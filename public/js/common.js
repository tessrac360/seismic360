$(document).ready(function () {
    $('.sweet-alert').find('h2').hide();
    var baseURL = $('#baseURL').val();
    $(document).on('click', '#btn_datatableSearch', function () {
        var posturl = $('#hidden_datatableUrl').val();
        var postkeyword = $('#txt_datatablesearch').val();
        //console.log(baseURL +posturl+ '?sd='+postkeyword);
        window.location.href = baseURL + posturl + '?sd=' + postkeyword;
    });
    $(document).on('click', '.buttonsAction', function () {
        var menuid = $(this).attr('menuid')
        $.ajax({
            type: 'POST',
            url: baseURL + 'menu/deleteTempMenu',
            dataType: 'json',
            data: {menuid: menuid},
            success: function (data) {
                if (data.status == 'true') {
                    $(".buttonMenu_" + menuid).remove();
                } else {

                }
            }
        });
    });
    $('.onoffactiontoggle').bootstrapToggle({
        on: 'Active',
        off: 'Deactive',
        size: 'mini',
        onstyle: 'success',
        offstyle: 'danger'
    });
    var table = $('.fixedheader').DataTable({
        "ordering": false,
        "bInfo": false,
        "bPaginate": false,
        "fixedHeader": true,
        "searching": false
    });


    $("#txt_datatablesearch").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#btn_datatableSearch").click();
        }
    });

    //Popover script Code goes here

    $(".pop").popover({trigger: "manual", html: true, animation: false})
            .on("mouseenter", function () {
                var _this = this;
                $(this).popover("show");
                $(".popover").on("mouseleave", function () {
                    $(_this).popover('hide');
                });
            }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
    });
});


