$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'services/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
	
    $('.gateway_type').on('change', function () {
        var value = this.value;
		//alert(value);
		if(value == 1){
			$("#command").show();
		}else{
			$("#command").hide();
		}
    });

});

$(document).on('click', '.isDelete', function () {
	var baseURL = $('#baseURL').val();
	service_uuid = $(this).attr('service_uuid');
	// status = $(this).attr('status');
	swal({
		title: "Are you sure?",
		text: "you want to delete ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		cancelButtonText: "No",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function (isConfirm) {
		if (isConfirm) {
			window.location.href = baseURL + 'Services/deleteService/' + service_uuid;
		}
	});

});

$(document).on('change', '.services', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var gateway_type = $("#gateway_type").val();
	var value = this.value;
	$.ajax({
		type: 'POST',
		url: baseURL + '/services/ajax_checkUniqueServiceName',
		dataType: 'json',
		data: {table: table, field: field, value: value, gateway_type:gateway_type},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " Service already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {
				
			}
		}
	});
	
});


  
