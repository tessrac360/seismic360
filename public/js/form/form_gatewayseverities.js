$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'gateways/ajax_changeSeverityStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
	
	      $('#severity').on('change', function () {
        //$('#errmsg').html('').hide();
        var table = $(this).attr('tableName');
        var field = $(this).attr('tableField');
        var value = this.value;
        //var old_email = $('#old_email').val();
        var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + '/severities/ajax_checkUniqueSeverityName',
            dataType: 'json',
            data: {table: table, field: field, value: value},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                    $('#' + field).val('');
                    $('#alias').val('');
                    $('#' + field).focus();
                } else {

                }
            }
        });
    });
});
 $("form[name='frmSeverity']").validate({
        // Specify validation rules
        rules: {
			severity_id: "required"
			//event_handler_enabled: "required",
			
        },
        // Specify validation error messages
        messages: {
			severity_id: "Please select actual severity"
			//event_handler_enabled: "Please enter 0 or 1(0 for disable and 1 for enable)",
        },
		
  });
  
