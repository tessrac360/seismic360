$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/gateway/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
	
 $('#admin_client').on('change', function () {
	var baseURL = $('#baseURL').val();
	$("#gateway_location").html("");
	var client_ids = this.value;
	var split = client_ids.split("#");
	var client_id = split[0];
	var client_uuid = split[1];	
	$.ajax({
			type: 'POST',
			url: baseURL + 'gateways/ajax_locations_list',
			data: {'client_uuid': client_uuid},
			success: function (response) {
					$("#gateway_location").removeClass('edited');
					$("#gateway_location").html(response);
					$("#gateway_location").addClass('edited');
				}
		});
});		

});

$(document).on('click', '.isdelete', function () {
	var baseURL = $('#baseURL').val();
	gateway_id = $(this).attr('gateway_id');
	//alert(gateway_id);
	// status = $(this).attr('status');
	
	//alert(baseURL + 'gateways/delete/' + gateway_id);
	//console.log(baseURL + 'gateways/delete/' + gateway_id);
	swal({
		title: "Are you sure?",
		text: "you want to delete ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		cancelButtonText: "No",
		closeOnConfirm: true,
		closeOnCancel: true
	},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = baseURL + 'gateways/delete/' + gateway_id;
				}
			});

});	

$(document).on('change', '.ip_address', function () {
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	$.ajax({
		type: 'POST',
		url: baseURL + 'gateways/ajax_checkUniqueIpAddress',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " Ip Address already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {
				
			}
		}
	});
});


 $("form[name='frmGateway']").validate({
        // Specify validation rules
        rules: {
            client_id: "required",
			title: "required",
			ip_address: "required",
			gateway_type: "required"
			//event_handler_enabled: "required",
			
        },
        // Specify validation error messages
        messages: {
            client_id: "Please select client",
			title: "Please enter gateway title",
			ip_address: "Please enter ip address",
			gateway_type: "Please select gateway type"
			//event_handler_enabled: "Please enter 0 or 1(0 for disable and 1 for enable)",
        },
		
  });
  

  
  
