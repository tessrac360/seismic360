$(document).on('change', '.username', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	//var reference_uuid = $(this).attr('reference_uuid');
	var value = this.value;
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/EmailAccountSettings/ajax_checkUniqueUsername',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {
				
			}
		}
	});
});

