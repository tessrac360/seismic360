$(document).ready(function () {
    var baseURL = $('#baseURL').val();
	var chart_duration = $('#duration_chart').value;
    $(document).on('click', '#chartMothlyReportclosed', function (e) {
        $('#chartMothlyReportdiv').hide();
        $('#chartMothlyReportHour').hide();
        serialMonthlyChart($('#duration_chart').val());
    });
	
	$('#duration_chart').on('change', function() {
		$('#chartMothlyReportdiv').hide();
        $('#chartMothlyReportHour').hide();
        serialMonthlyChart(this.value);
		if (serialMonthlyChart(this.value)="Today")
			$('#chartMothlyReportdiv').hide();
	});
	
 /*    openEventsStatusLive();
    setInterval(openEventsStatusLive, 60000);
    setInterval(openBreachedEventsLive, 60000); */
    serialMonthlyChart();    
    //pieAcknowledgmentStatus();
    //openBreachedEventsLive();
    function serialMonthlyChart(chart_duration = "MTD") {
      // alert("hiii");
        $.ajax({
            type: 'POST',
            url: baseURL + 'dashboard/ajax_chartMonthwise_event/'+chart_duration,
			dataType: 'json',
            success: function (response) {
                if (response.status == 'true') {
                    
                    var chartSerial = AmCharts.makeChart("chartMothlyReport", {
                        "type": "serial",
                        "theme": "light",
                        "fontFamily": 'Open Sans',
                        "color": '#888888',
                        "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
                        "dataProvider": response.result,
                        
                        "balloon": {
                            "cornerRadius": 6
                        },
                        "valueAxes": [{
                                "event_count": "",
                                "event_countUnits": {
                                    // "hh": "h ",
                                    // "mm": "min"
                                },
                                "axisAlpha": 0
                            }],
                        "graphs": [{
                                "bullet": "square",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 1,
                                "fillAlphas": 0.3,
                                "fillColorsField": "lineColor",
                                "legendValueText": "[[value]]",
                                "lineColorField": "lineColor",
                                "title": "Event Count",
                                "valueField": "event_count"
                            }],
                        "dataDateFormat": "YYYY-MM-DD",
                        "categoryField": "date",
                        "categoryAxis": {
                            "dateFormats": [{
                                    "period": "DD",
                                    "format": "DD"
                                }, {
                                    "period": "WW",
                                    "format": "MMM DD"
                                }, {
                                    "period": "MM",
                                    "format": "MMM"
                                }, {
                                    "period": "YYYY",
                                    "format": "YYYY"
                                }],
                            "parseDates": true,
                            "autoGridCount": false,
                            "axisColor": "#555555",
                            "gridAlpha": 0,
                            "gridCount": 50
                        },
                    });
                    var graph = new AmCharts.AmGraph();
                    chartSerial.addListener("clickGraphItem", ChangePan, $(this));
                   
                }
            }
        });
    }
    function ChangePan(event) {
        var datetime = event.item.dataContext.date;
        $.ajax({
            type: 'POST',
            url: baseURL + 'dashboard/ajax_chartDaywise_event',
            dataType: 'json',
            data: {'datetime': datetime},
            success: function (response) {
                console.log(response);
                if (response.status == 'true') {
                    $('#chartMothlyReportdiv').show();
                    $('#chartMothlyReportHour').show();
                    var chartSerialHourWise = AmCharts.makeChart("chartMothlyReport", {
                        "type": "serial",
                        "theme": "light",
                        "fontFamily": 'Open Sans',
                        "color": '#888888',
                        "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
                        "dataProvider": response.result,
                        "balloon": {
                            "cornerRadius": 6
                        },
                        "valueAxes": [{
                                "event_count": "",
                                "event_countUnits": {
                                    // "hh": "h ",
                                    // "mm": "min"
                                },
                                "axisAlpha": 0
                            }],
                        "graphs": [{
                                "bullet": "square",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 1,
                                "fillAlphas": 0.3,
                                "fillColorsField": "lineColor",
                                "legendValueText": "[[value]]",
                                "lineColorField": "lineColor",
                                "title": "Event Count",
                                "valueField": "event_count"
                            }],
                        "dataDateFormat": "YYYY-MM-DD",
                        "categoryField": "hour",
                        "categoryAxis": {
                            "dateFormats": [{"period": "hh", "format": "JJ:NN"},
                                {"period": "mm", "format": "JJ:NN"}]

                        },
                    });
                    var graph12 = new AmCharts.AmGraph();
                    chartSerialHourWise.addListener("clickGraphItem", ChangeHourWise, $(this));
                }
            }
        });
    }
    function ChangeHourWise(event) {       
        if (event.item.dataContext.event_count > 0) {
            var date = event.item.dataContext.date;
            var hour = event.item.dataContext.hour;
            window.location.href = baseURL + 'event/history?date=' + date + '&hour=' + hour;
        }
    }
/*     function pieAcknowledgmentStatus(chart_duration = "MTD") {
        $.ajax({
            type: 'POST',
            url: baseURL + 'dashboard/ajax_acknowledgmentStatus_event/'+chart_duration,
            dataType: 'json',
            success: function (response) {
                var chartPie = AmCharts.makeChart("acknowledgmentStatus", {
                    "type": "pie",
                    "labelText": "[[percents]]",
                    "theme": "light",
                    "color": '#fff',
                    "autoMargins": false,
                    "innerRadius": 20,
                    "marginTop": 10,
                    "marginBottom": 0,
                    "marginLeft": 0,
                    "marginRight": 0,
                    "pullOutRadius": 0,
                    "addClassNames": true,
                    "labelRadius": -25,
                    "labelText": "[[value]]",
                    "legend": {
                        "position": "bottom",
                        "marginRight": 0,
                        "valueWidth": 0,
                        "verticalGap": 0,
                        "autoMargins": false
                    },
                    "dataProvider": response.result,
                    "valueField": "counts",
                    "titleField": "type",
                    "exportConfig": {
                        menuItems: [{
                                icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                                format: 'png'
                            }]
                    },
                    "listeners": [{
                            "event": "clickSlice",
                            "method": function (e) {
                                console.log(e.dataItem.dataContext.type);
                                var acktype = e.dataItem.dataContext.type;
                                if (acktype == 'Acknowledged') {
                                    window.location.href = baseURL + 'event/history?acktype=acknowledged';
                                } else if (acktype == 'Unacknowledged') {
                                    window.location.href = baseURL + 'event/history?acktype=unacknowledged';
                                } else if (acktype == 'Suppressed') {
                                    window.location.href = baseURL + 'event/suppress';
                                }

                            }
                        }]
                });
            }
        });
    } */
    
/*     //Start Open Events Status: Live
     function openEventsStatusLive(){
        $.ajax({
            type: 'POST',
            url: baseURL + 'dashboard/ajax_open_events_status_live_event',  
            success: function (data) {
                console.log(data);
                $('.eventTable tbody').html("");
                if (data.status == 'true') {
                    $('.Open-Events-Status-Live tbody').html(data);

                } else {
                    $('.Open-Events-Status-Live tbody').html(data);
                }
            }
        });
    }
    //End Open Events Status: Live
    
    
    //Start Breached Events: Live
     function openBreachedEventsLive(){
        $.ajax({
            type: 'POST',
            url: baseURL + 'dashboard/ajax_breached_events_live_event',          
            success: function (data) {               
                $('.eventTable tbody').html("");
                if (data.status == 'true') {
                    $('.breached-events-live tbody').html(data);

                } else {
                    $('.breached-events-live tbody').html(data);
                }
            }
        });
    }
    //End Breached Events: Live */
});


