$(document).ready(function () {

	
	var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
		var table = $(this).attr('mytable');
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/devices/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'id': id, 'table': table},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
	
	var baseURL = $('#baseURL').val();	
    $('#country_id').on('change', function () {
		
        var value = this.value;
		//alert(value);
        $.ajax({
            type: 'POST',
            url: baseURL + 'location/ajax_State',
            dataType: 'json',
            data: { value: value},
            success: function (data) {
				var options = "";
				var obj = JSON.stringify(data);
				options = '<option value=""> </option>';
				$.each(JSON.parse(obj), function(i, item) {					
					options+= '<option value="'+item.id+'">'+item.name+'</option>';
				})				
				$("#state_id").html(options);
            }
        });
    });
	
	
	var baseURL = $('#baseURL').val();	
    $('#state_id').on('change', function () {
		
        var value = this.value;
		//alert(value);
        $.ajax({
            type: 'POST',
            url: baseURL + 'location/ajax_Zone',
            dataType: 'json',
            data: { value: value},
            success: function (data) {
				var options = "";
				var obj = JSON.stringify(data);
				options = '<option value=""> </option>';
				$.each(JSON.parse(obj), function(i, item) {					
					options+= '<option value="'+item.id+'">'+item.name+'</option>';
				})				
				$("#zone_id").html(options);
            }
        });
    });
	
	var baseURL = $('#baseURL').val();	
    $('#zone_id').on('change', function () {
		
        var value = this.value;
		//alert(value);
        $.ajax({
            type: 'POST',
            url: baseURL + 'location/ajax_Cluster',
            dataType: 'json',
            data: { value: value},
            success: function (data) {
				var options = "";
				var obj = JSON.stringify(data);
				options = '<option value=""> </option>';
				$.each(JSON.parse(obj), function(i, item) {					
					options+= '<option value="'+item.id+'">'+item.name+'</option>';
				})				
				$("#cluster_id").html(options);
            }
        });
    });

	 // validate the form when it is submitted
    $("#frmLocation").validate();
	
	/* Country Form Validation */
	
    $("#country_name").rules("add", {
        required:true,
        messages: {
            required: "Please enter country name"
        }
    });
     
    $("#country_short_name").rules("add", {
        required:true,
        messages: {
            required: "Please enter country short name"
        }
    });
	
	/* State Form Validation */
	
	$("#country_id").rules("add", {
        required:true,
        messages: {
            required: "Please select country"
        }
    });

	$("#state_id").rules("add", {
        required:true,
        messages: {
            required: "Please select state"
        }
    });
	  
	
	$("#state_name").rules("add", {
        required:true,
        messages: {
            required: "Please enter state name"
        }
    });
	
	/* Circle Form Validation */
	
	$("#circle_name").rules("add", {
        required:true,
        messages: {
            required: "Please enter circle name"
        }
    });
	
     
    $("#circle_short_name").rules("add", {
        required:true,
        messages: {
            required: "Please enter circle short name"
        }
    });
	
	$("#zone_name").rules("add", {
        required:true,
        messages: {
            required: "Please enter zone name"
        }
    });
	
	$("#cluster_name").rules("add", {
        required:true,
        messages: {
            required: "Please enter cluster name"
        }
    });
	
     
    $("#cluster_short_name").rules("add", {
        required:true,
        messages: {
            required: "Please enter cluster short name"
        }
    });
	

});
function status_val(value,table){
		var baseURL = $('#baseURL').val();
		$.ajax({
            type: 'POST',
			dataType: 'json',
            url: baseURL + 'location/statusChange',
			data: { value: value, table: table},
            success: function (data) {
					
            }
        });	
}

