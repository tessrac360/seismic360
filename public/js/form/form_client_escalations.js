$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
		var escalations_assign_group_uuid = $(this).attr('escalations_assign_group_uuid');
		var escalation_group_uuid = $(this).attr('escalation_group_uuid');
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/ClientEscalations/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'escalations_assign_group_uuid': escalations_assign_group_uuid,   'escalation_group_uuid':escalation_group_uuid},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
 
});

$(document).on('click', '.isDelete', function () {
	var baseURL = $('#baseURL').val();
	escalations_assign_group_uuid = $(this).attr('escalations_assign_group_uuid');
	// status = $(this).attr('status');
	swal({
		title: "Are you sure?",
		text: "you want to delete ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		cancelButtonText: "No",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function (isConfirm) {
		if (isConfirm) {
			window.location.href = baseURL + 'client/ClientEscalations/deleteEscalationGroup/' + escalations_assign_group_uuid;
		}
	});

});
