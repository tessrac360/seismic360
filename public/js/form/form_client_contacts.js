$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
		var contact_uuid = $(this).attr('contact_uuid');
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/contacts/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'contact_uuid': contact_uuid},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
	


	   
});

$(document).on('click', '.isDeleteClientContacts', function () {
	var baseURL = $('#baseURL').val();
	contactId = $(this).attr('contactId');
	// status = $(this).attr('status');
	swal({
		title: "Are you sure?",
		text: "you want to delete ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		cancelButtonText: "No",
		closeOnConfirm: true,
		closeOnCancel: true
	},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = baseURL + 'client/contacts/deleteContact/' + contactId;
				}
			});

});
	$(document).on('change', '.phone_number', function () {
        var checkarray ={};
	    var i=0;
		$(".phone_number").each(function() {
			var phone_number = $(this).val();
			var phone_country_code = $(this).parent().parent().find('.phone_country_code').val();
			//alert("'"+phone_country_code+phone_number+"'");
						
			if(checkarray["'"+phone_country_code+phone_number+"'"] == undefined ){					
				checkarray["'"+phone_country_code+phone_number+"'"]= new Array();	
				checkarray["'"+phone_country_code+phone_number+"'"][i] = 'ok';				  
				i++;
			}else{
				swal("Oops!!!", phone_number + " already exists!", "warning");
				$(this).val('');
				
			}

		});
		//alert(checkarray);
	    //console.log(checkarray);
		var phonethis = $(this);
		var baseURL = $('#baseURL').val();
        //$('#errmsg').html('').hide();
        var table = $(this).attr('tableName');
        var field = $(this).attr('tableField');
        var value = this.value;
		var phone_country_code = $(this).parent().parent().find('.phone_country_code').val();
		//alert(phone_country_code);
        //var old_email = $('#old_email').val();
		//alert(value);
        var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/contacts/ajax_checkUniquePhone',
            dataType: 'json',
            data: {table: table, field: field, value: value,phone_country_code :phone_country_code},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                   // $('#' + field).val('');
                    phonethis.val('');
                    phonethis.focus();
                } else {

                }
            }
        });
    });
	$(document).on('change', '.email_address', function () {
		var checkarray ={};
	    var i=0;
		$(".email_address").each(function() {
			var email_address = $(this).val();
							
			if(checkarray[email_address] == undefined ){					
				checkarray[email_address]= new Array();	
				checkarray[email_address][i] = 'ok';				  
				i++;
			}else{
				swal("Oops!!!", email_address + " already exists!", "warning");
				$(this).val('');
				
			}

		});

		var baseURL = $('#baseURL').val();
		var emailthis = $(this);
        //$('#errmsg').html('').hide();
        var table = $(this).attr('tableName');
        var field = $(this).attr('tableField');
        var value = this.value;
        //var old_email = $('#old_email').val();
        var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/contacts/ajax_checkUniqueEmail',
            dataType: 'json',
            data: {table: table, field: field, value: value},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                    //$('#' + field).val('');
                    emailthis.val('');
                    emailthis.focus();
                } else {

                }
            }
        });
    });
$(document).on('click', '.isPrimaryContact', function () {

	var baseURL = $('#baseURL').val();
	var thisd = $(this);

	// status = $(this).attr('status');
	//$(this).prop('checked', false);
	swal({
		title: "Primary Contact is already set",
		text: "Do you want to set this contact as primary ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		cancelButtonText: "No",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function (isConfirm) {
		if (isConfirm) {
			

		}else{
			//alert('else');
		 thisd.prop('checked', false);	
		}
	});

});

$("#first_name").keyup(function () {
    $('#contact_name').val($(this).val()+' '+$("#last_name").val());
    $('#full_name').val($(this).val()+' '+$("#last_name").val());

});
$("#last_name").keyup(function () {
    $('#contact_name').val($("#first_name").val()+' '+$(this).val());
    $('#full_name').val($("#first_name").val()+' '+$(this).val());
});
$("#second_name").keyup(function () {
    $('#full_name').val($("#first_name").val()+' '+$(this).val()+' '+$("#last_name").val());
});
