var baseURL = $('#baseURL').val();


var Login = function () {

    var handleLogin = function () {
        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },

            messages: {
                username: {
                    required: "Username is required."
                },
                password: {
                    required: "Password is required."
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function () {
                var dataString = $("#login-form").serialize();
                $.ajax({
                    type: 'POST',
                    url: baseURL + 'auth/do_login',
                    dataType: 'json',
                    data: dataString,
                    success: function (data) {
                        console.log(data);//return false;
                        if (data.status == 'true') {
                            //window.location.href = baseURL+'auth-group';
                            window.location.href = baseURL + data.pageurl;
                        } else {

                            $('.alert-danger', $('.login-form')).show();
                            $('.alert-danger #errorLoginMessage').text(data.message);
                            //$('#error_message').text(data.message);
                            $('#password').val("");
                            $('#username').val("");

                        }

                    }
                });
                //form.submit();
            }
        });


    }

    var handleForgetPassword = function () {
        $('.forget-frm').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },

            messages: {
                email: {
                    required: "Email is required."
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   

            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {
				$(':input[type="submit"]').prop('disabled', true);
                var dataString = $("#forget-frm").serialize();
                $.ajax({
                    type: 'POST',
                    url: baseURL + 'auth/do_checkmail',
                    dataType: 'json',
                    data: dataString,
                    success: function (data) {
                        //console.log(data);return false;
                        if (data.status == 'true') {
                            //window.location.href = baseURL+'auth-group';
							$(':input[type="submit"]').prop('disabled', false);
                            window.location.href = baseURL + data.pageurl;
                        } else {
							$(':input[type="submit"]').prop('disabled', false);
                            $('#email').val("");
                            alert(data.message);
                            //$('#error_message').text(data.message);                            


                        }

                    }
                });
            }
        });

        $('.forget-frm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-frm').validate().form()) {
                    $('.forget-frm').submit();
                }
                return false;
            }
        });



    }

    var handleRegister = function () {

        function format(state) {
            if (!state.id) {
                return state.text;
            }
            var $state = $(
                    '<span><img src="public/assets/global/img/flags/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
                    );
            console.log($state);

            return $state;
        }

        if (jQuery().select2 && $('#country_list').size() > 0) {
            $("#country_list").select2({
                placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
                templateResult: format,
                templateSelection: format,
                width: 'auto',
                escapeMarkup: function (m) {
                    return m;
                }
            });


            $('#country_list').change(function () {
                $('.register-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        }


        $('.register-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {

                fullname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                address: {
                    required: true
                },
                city: {
                    required: true
                },
                country: {
                    required: true
                },

                username: {
                    required: true
                },
                password: {
                    required: true
                },
                rpassword: {
                    equalTo: "#register_password"
                },

                tnc: {
                    required: true
                }
            },

            messages: {// custom messages for radio buttons and checkboxes
                tnc: {
                    required: "Please accept TNC first."
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   

            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
                    error.insertAfter($('#register_tnc_error'));
                } else if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function (form) {
                form.submit();
            }
        });

        $('.register-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.register-form').validate().form()) {
                    $('.register-form').submit();
                }
                return false;
            }
        });


    }

    var handleResertPassword = function () {
        $('.reset-frm').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                password: {
                    required: true
                },
                cpassword: {
                    required: true,
                    equalTo: "#password"
                },
            },

            messages: {
                password: {
                    required: "Password is required."
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   

            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {

                var dataString = $("#reset-frm").serialize();
                $.ajax({
                    type: 'POST',
                    url: baseURL + 'auth/do_resetPassword',
                    dataType: 'json',
                    data: dataString,
                    success: function (data) {
                        //console.log(data);return false;
                        if (data.status == 'true') {                            
                           // window.location.href = baseURL + data.pageurl;
                            $('.alert-success').show();
                            $('.alert-success #errorLoginMessage').text(data.message);
                            $('#password').val("");
                            $('#cpassword').val("");
                        } else {
                            $('#password').val("");
                            $('#cpassword').val("");
                            alert(data.message);
                            //$('#error_message').text(data.message);                            
                        }

                    }
                });
            }
        });

        $('.reset-frm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.reset-frm').validate().form()) {
                    $('.reset-frm').submit();
                }
                return false;
            }
        });



    }

    return {
        //main function to initiate the module
        init: function () {

            handleLogin();
            handleForgetPassword();
            handleRegister();
            handleResertPassword();
            // init background slide images
            $.backstretch([
              // baseURL + "public/assets/pages/media/bg/secure.jpg"
               //baseURL + "public/assets/pages/media/bg/5.jpg"                
            ], {
                fade: 1000,
                duration: 8000
            }
            );
        }
    };

}();

jQuery(document).ready(function () {
    $(".mypassword").keyup(function (event) {
//alert("hiiii");	
        if (event.keyCode == 13) {
            $('.login-form').trigger('submit');
        }
    });

    Login.init();
});