$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/category/ajax_changeStatusCategory',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
    
    $('.subcatdevicestatus').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/category/ajax_changeStatusSubCategory',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
});
$("form[name='frmdevcategory']").validate({
    // Specify validation rules
    rules: {
        assign_group_id: "required",
        category: "required",       
    },   
});

$("form[name='frmdevsubcategory']").validate({
    // Specify validation rules
    rules: {
        device_sub_category_id: "required",
        subcategory: "required",       
    },   
});

$(document).on('change', '.category', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/category/ajax_checkUniqueCategoryName',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " Category already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {
				
			}
		}
	});
	
});

$(document).on('change', '.subcategory', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var device_sub_category_id = $("#device_sub_category_id").val();
	var value = this.value;
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/category/ajax_checkUniqueSubCategoryName',
		dataType: 'json',
		data: {table: table, field: field, value: value, device_sub_category_id: device_sub_category_id},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " Category already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {
				
			}
		}
	});
	
});
