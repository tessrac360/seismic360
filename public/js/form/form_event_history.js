$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.client').multiselect({
        includeSelectAllOption: true,
        //enableCaseInsensitiveFiltering: true,
       // enableFiltering: true,
        nonSelectedText: 'Partner',
        selectAllText: ' Select all',
        maxHeight: 300,        
    });


    $('.device').multiselect({
        includeSelectAllOption: true,
        //enableCaseInsensitiveFiltering: true,
        //enableFiltering: true,
        nonSelectedText: 'Host Name',
        selectAllText: ' Select all',
        maxHeight: 300

    });

    $('#service').multiselect({
        includeSelectAllOption: true,
        //enableCaseInsensitiveFiltering: true,
        //enableFiltering: true,
        nonSelectedText: 'Event Type',
        selectAllText: ' Select all',
        maxHeight: 300
    });

    $('#severity').multiselect({
        includeSelectAllOption: true,
        //enableCaseInsensitiveFiltering: true,
        //enableFiltering: true,
        nonSelectedText: 'Severity',
        selectAllText: ' Select all',
        maxHeight: 300
    });

    $(document).on('click', '#resetEventhistory', function () {
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_eventHistoryReset',
            dataType: 'json',
            success: function (data) {
                if (data.status == 'true') {
                    window.location.href = baseURL + 'event/history';
                }
            }
        });
    });


    /* start suppress module */
    $(document).on('click', '.reactivateSuppress', function () {
        var ack_type = $(this).attr('ack_type');
        var eventSlug = $(this).attr('eventSlug');
        var ackId = $(this).attr('ackId');
        var evtid = $(this).attr('evtid');
        if (ack_type == 'T') {
            swal({
                title: "Are you sure you want to reactivate Temporary Suppress (" + eventSlug + ") ?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: 'POST',
                                dataType: 'json',
                                url: baseURL + 'event/ajax_reactivateSuppress',
                                data: {evtid: evtid, ackId: ackId, ack_type: ack_type},
                                success: function (response) {
                                    if (response.status == 'true') {
                                        console.log(response);
                                        $('.event_' + evtid).remove();
                                        swal("", response.message, "success");
                                    } else if (response.status == 'false') {
                                        $(".errormsg").html(response.msg);
                                    }
                                }
                            });
                        }
                    });
        } else if (ack_type == 'P') {
            swal({
                title: "Are you sure you want to reactivate Permanent Suppress (" + eventSlug + ") ?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: 'POST',
                                dataType: 'json',
                                url: baseURL + 'event/ajax_reactivateSuppress',
                                data: {evtid: evtid, ackId: ackId, ack_type: ack_type},
                                success: function (response) {
                                    if (response.status == 'true') {
                                        //$('.event_' + response.eventId).remove();
                                        $('.event_' + evtid).remove();
                                        swal("", response.message, "success");
                                    } else if (response.status == 'false') {
                                        $(".errormsg").html(response.msg);
                                    }
                                }
                            });
                        }
                    });
        }
    });

    /* start suppress module */

});


