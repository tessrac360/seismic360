


$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    setInterval(get_fb, 30000);
    get_eventHeader();
    get_fb();


//    var url_string = document.URL;
//    var url = new URL(url_string);
//    var param_severity = url.searchParams.get("severity");
//    alert(param_severity);
//    $('.severity').prop('checked', true);

    function get_fb() {

        var severity = []
        $("input[name='severity[]']:checked").each(function ()
        {
            severity.push(parseInt($(this).val()));
        });

        var tools = []
        $("input[name='tools[]']:checked").each(function ()
        {
            tools.push(parseInt($(this).val()));
        });

        var device_name = []
        $("input[name='device_name[]']:checked").each(function ()
        {
            device_name.push(parseInt($(this).val()));
        });

        var clients = []
        $("input[name='subClients[]']:checked").each(function ()
        {
            clients.push(parseInt($(this).val()));
        });

        var service = []
        $("input[name='service[]']:checked").each(function ()
        {
            service.push(parseInt($(this).val()));
        });

        var state = []
        $("input[name='state[]']:checked").each(function ()
        {
            state.push(parseInt($(this).val()));
        });

        var eventId = []
        $("input[name='event_id[]']:checked").each(function ()
        {
            eventId.push(parseInt($(this).val()));
        });

        var url_string = document.URL;
        var url = new URL(url_string);
        var param_severity = url.searchParams.get("severity");

        param_severitypost = "";
        if (!isNaN(param_severity)) {
            param_severitypost = param_severity;
        } else {
            //alert("Invalid Input")
            window.location.href = baseURL + "event";
        }

        var type = url.searchParams.get("type");
        var acktype = url.searchParams.get("acktype");


        var eventSearch = $('#eventSearch').val();
        var trhighlight = $('#trhighlight').val();
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_eventLoad',
            dataType: 'json',
            data: {tools:tools,acktype: acktype, type: type, param_severitypost: param_severitypost, severity: severity, device_name: device_name, eventSearch: eventSearch, clients: clients, service: service, state: state, eventId: eventId, trhighlight: trhighlight},
            success: function (data) {
                $('.eventTable tbody').html("");
                if (data.status == 'true') {
                    $('.eventTable tbody').html(data.html);

                } else {
                    $('.eventTable tbody').html(data.html);
                }
            }
        });
    }

    function get_eventHeader() {
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_eventHeaderLoad',
            success: function (data) {
                $('.eventTable thead').html(data);
            }
        });
    }

    $("#eventSearch").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#btn_eventSearch").click();
        }
    });
    $(document).on('click', '#btn_eventSearch', function () {
        get_fb();
    });
    $(document).on('change', '.service', function () {
        get_fb();
    });
    $(document).on('change', '.subClients', function () {
        get_fb();
    });
    $(document).on('change', '.severity', function () {
        get_fb();
    });
    $(document).on('change', '.device_name', function () {
        get_fb();
    });
    $(document).on('change', '.state', function () {
        get_fb();
    });
    $(document).on('change', '.tools', function () {
        get_fb();
    });



    $(document).on('click', '.acknowlegementDetails', function () {
        var eventId = $(this).attr('value');
        $(".parent").removeClass('col-md-12');
        $(".parent").addClass('col-md-9');
        $(".acknowledgeDetails").show();
        $(".right_block").hide();
        $(".answer").hide();
        $('.coupon_question').removeAttr('checked');
        $('tr').removeClass("highlight");


        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_acknowlegementDetails',
            dataType: 'json',
            data: {eventId: eventId},
            success: function (data) {
                if (data.status == 'true') {
                    $('.acknowledgeDetails .appendAckList').html(data.html);
                }
            }
        });
    });

    $(document).on('click', '.coupon_question', function () {
        var eventId = $(this).attr('evtid');

        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_openAcknowledgeDiv',
            data: {eventId: eventId},
            success: function (data1) {
                $('#myModal').modal();
                $('#load-data').html(data1);
            }
        });

    });

    $(document).on('dblclick', '.eventtr', function () {
        var eventId = $(this).attr('rowId');
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_openAcknowledgeDiv',
            data: {eventId: eventId},
            success: function (data1) {
                $('#myModal').modal();
                $('#load-data').html(data1);
            }
        });

    });


    $(document).on('click', '.suppress_action', function () {
        var eventId = $(this).attr('evtid');
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_openSuppressPopup',
            data: {eventId: eventId},
            success: function (data1) {
                $('#actionmodal').modal();
                $('#load-data-suppress').html(data1);
            }
        });

    });



    $(".topMoveable tbody").sortable();
    $(document).on('click', '#btnSorting', function () {
        var position = new Array();
        $('#sortable-row tbody tr td').each(function () {
            position.push($(this).attr("id"));
        });
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_eventSorting',
            dataType: 'json',
            data: {position: position},
            success: function (data) {
                if (data.status == 'true') {
                    window.location.href = baseURL + 'event';
//                    get_eventHeader();
//                    get_fb();
                }
            }
        });
    });

    $(document).on('click', '#btnmanualack', function (e) {
        e.preventDefault();
        $('#msg_manualack').html('');
        if ($.trim($('#txtmanualack').val()) == "") {
            $('#msg_manualack').html('Please enter acknowledge comments !!').css("color", "red");
            return false;
        } else {
            var eventId = [];
            var client_id = [];
            $("input[name='event_id[]']:checked").each(function () {
                eventId.push(parseInt($(this).val()));
                client_id.push(parseInt($(this).attr('clientVal')));
            });

            var data = {
                'eventId': eventId,
                'client_id': client_id,
                'ack_status_id': '2'
            };
            data = $('#frm_manual').serialize() + '&' + $.param(data);
            $.ajax({
                type: 'POST',
                url: baseURL + 'event/ajax_manualAcknowledge',
                dataType: 'json',
                data: data,
                async: false,
                success: function (response) {
                    $('.coupon_question').removeAttr('checked');
                    $(".parent").removeClass('col-md-9');
                    $(".parent").addClass('col-md-12');
                    $(".answer").hide();
                    $(".right_block").hide();
                    if (response.status == 'true') {
                        for (var i = 0; i < response.eventId.length; i++) {


                            $("#ack_" + response.eventId[i]).html('<a href="javascript:void()" class="acknowlegementDetails" value="' + response.eventId[i] + '">Manual Acknowledged</a>');
                        }
                        swal("", response.message, "success");
                    } else if (response.status == 'false') {
                        $(".errormsg").html(response.msg);
                    }
                }
            });
            //return false;
        }
    });

    $(document).on('click', '#btnsurpress', function (e) {
        e.preventDefault();
        console.log($('#datetimeSupress').val());
        $('#msg_txtsurpress').html('');
        $('#msg_datetimeSupress').html('');
        var datetimeSupress = new Date($('#datetimeSupress').val().replace(/-/g, "/"));
        var currentDate = new Date();
        if ($.trim($('#datetimeSupress').val()) == "Y-m-d H:i" || $.trim($('#datetimeSupress').val()) == "") {
            $('#msg_datetimeSupress').html('Please enter suppress date time !!').css("color", "red");
            return false;
        } else if (datetimeSupress < currentDate) {
            $('#msg_datetimeSupress').html("Suppress Date Time Should be greater than Current Date Time").css("color", "red");
            return false;
        } else {
            data = $('#frm_Supress').serialize();
            $.ajax({
                type: 'POST',
                url: baseURL + 'event/ajax_surpressAcknowledge',
                dataType: 'json',
                data: data,
                async: false,
                success: function (response) {
                    if (response.status == 'true') {
                        $('#myModal').modal('hide');
                        $('#actionmodal').modal('hide');
                        $('.event_' + response.eventId).remove();
                        swal("", response.message, "success");
                    } else if (response.status == 'false') {
                        $(".errormsg").html(response.msg);
                    }
                }
            });
            //return false;
        }
    });





    $(document).on('click', '#btnsurpresspermanent', function (e) {
        e.preventDefault();
        data = $('#frm_Supress_permanent').serialize();
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_surpressPermanent',
            dataType: 'json',
            data: data,
            async: false,
            success: function (response) {
                if (response.status == 'true') {
                    $('#myModal').modal('hide');
                    $('#actionmodal').modal('hide');
                    $('.event_' + response.eventId).remove();
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
        return false;

    });

    // start Acknowledgement 

    $(document).on('click', '#btnack', function (e) {
        e.preventDefault();
        //alert($('#txtacknowledgement').val());
        if ($.trim($('#txtacknowledgement').val()) == "") {
            $('#msg_manualack').html('Please enter acknowledge comments !!').css("color", "red");
            return false;
        } else {
            data = $('#frm_acknowledgement').serialize();
            console.log(data);
            $.ajax({
                type: 'POST',
                url: baseURL + 'event/ajax_acknowledgement',
                dataType: 'json',
                data: data,
                async: false,
                success: function (response) {
                    if (response.status == 'true') {
                        $('#myModal').modal('hide');
                        $('#actionmodal').modal('hide');
                        swal("", response.message, "success");
                    } else if (response.status == 'false') {
                        $(".errormsg").html(response.msg);
                    }
                }
            });
        }

    });


    $(document).on('click', '#btnackpupup', function (e) {
        e.preventDefault();
        //alert($('#txtacknowledgement').val());
        if ($.trim($('#txtacknowledgement').val()) == "") {
            $('#msg_manualack').html('Please enter acknowledge comments !!').css("color", "red");
            return false;
        } else {
            data = $('#frm_acknowledgement_popup').serialize();
            console.log(data);
            $.ajax({
                type: 'POST',
                url: baseURL + 'event/ajax_acknowledgement',
                dataType: 'json',
                data: data,
                async: false,
                success: function (response) {
                    if (response.status == 'true') {
                        $('#myModal').modal('hide');
                        $('#actionmodal').modal('hide');
                        swal("", response.message, "success");
                    } else if (response.status == 'false') {
                        $(".errormsg").html(response.msg);
                    }
                }
            });
        }

    });

    // End Acknowledgement 


//=================================

    $(document).on('click', '#btnsurpresspupup', function (e) {

        e.preventDefault();
        console.log($('#datetimeSupress').val());
        $('#msg_txtsurpress').html('');
        $('#msg_datetimeSupress').html('');
        //alert("hiiii");
        var datetimeSupress = new Date($('#datetimeSupress').val().replace(/-/g, "/"));
        var currentDate = new Date();
        if ($.trim($('#datetimeSupress').val()) == "Y-m-d H:i" || $.trim($('#datetimeSupress').val()) == "") {
            $('#msg_datetimeSupress').html('Please enter suppress date time !!').css("color", "red");
            return false;
        } else if (datetimeSupress < currentDate) {
            $('#msg_datetimeSupress').html("Suppress Date Time Should be greater than Current Date Time").css("color", "red");
            return false;
        } else {
            data = $('#frm_Supress_pupup').serialize();
            $.ajax({
                type: 'POST',
                url: baseURL + 'event/ajax_surpressAcknowledge',
                dataType: 'json',
                data: data,
                async: false,
                success: function (response) {
                    if (response.status == 'true') {
                        $('#myModal').modal('hide');
                        $('#actionmodal').modal('hide');
                        $('.event_' + response.eventId).remove();
                        swal("", response.message, "success");
                    } else if (response.status == 'false') {
                        $(".errormsg").html(response.msg);
                    }
                }
            });
            //return false;
        }
    });





    $(document).on('click', '#btnsurpresspermanentpupup', function (e) {
        e.preventDefault();
        data = $('#frm_Supress_permanent_pupup').serialize();
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_surpressPermanent',
            dataType: 'json',
            data: data,
            async: false,
            success: function (response) {
                if (response.status == 'true') {
                    $('#myModal').modal('hide');
                    $('#actionmodal').modal('hide');
                    $('.event_' + response.eventId).remove();
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
        return false;

    });


    $(document).on('click', '.suppress_reactivate', function () {
        var suppress_event_id = $(this).attr('suppress_event_id');
        var suppress_ack_id = $(this).attr('suppress_ack_id');
        //return false;
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_suppressReactivate',
            dataType: 'json',
            data: {suppress_ack_id: suppress_ack_id, suppress_event_id: suppress_event_id},
            success: function (data) {
                if (data.status == 'true') {
                    $('.coupon_question').removeAttr('checked');
                    $(".parent").removeClass('col-md-9');
                    $(".parent").addClass('col-md-12');
                    $(".answer").hide();
                    $(".right_block").hide();
                    swal("", data.message, "success");
                    $("#ack_" + data.event_id).html('');
                }
            }
        });
    });


    $(document).on('click', '.manual_ticket', function () {
        var eventId = $(this).attr('evtid');
        swal({
            title: "Are you sure you want to generate manual ticket?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: 'GET',
                            url: baseURL + 'users/auto_tickets/manual',
                            data: {eid: eventId},
                            success: function (data) {
                                if (data == 1) {
                                    $('.mticket_' + eventId).remove();
                                    swal("", 'Ticket is created successfully !!', "success");
                                }
                            }
                        });
                    }
                });





    });



});



