$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    var table = $('.report_tables').val();
    var save_id = $('#save_id').val();
    var url_string = document.URL;
    var url = new URL(url_string);
    var action = url.searchParams.get("action");

    $(document).on('click', '.addmore', function (event) {
        event.preventDefault();
        var condition = $(this).val();
        var countDiv = $('.add_bar').length;
        if (countDiv >= 1) {
            $('.topclosebtn').hide();
        }
        var formData = $('#form_filter').serializeArray();

        formData.push({name: "countDiv", value: countDiv});
        formData.push({name: "condition", value: condition});
        $.ajax({
            type: 'POST',
            url: baseURL + 'reports/ajax_managecondition',
            data: formData,
            success: function (response) {
                $('.addMoreDiv').append(response);
            }
        });
    });


    $(document).on('click', '.save-report', function (event) {
        //alert("hiiii");
        event.preventDefault();
        var condition = $(this).val();
        var countDiv = $('.add_bar').length;
        if (countDiv >= 1) {
            $('.topclosebtn').hide();
        }
        table_fields = [];
        $("#lstBox2 option").each(function () {
            table_fields.push($(this).val());
        });
        choose_field = [];
        $(".choose_field").each(function () {
            choose_field.push($(this).val());
        });
        oper = [];
        $(".oper").each(function () {
            oper.push($(this).val());
        });
        conditiontextbox = [];
        $(".conditiontextbox").each(function () {
            conditiontextbox.push($(this).val());
        });
        var save_id = $('.save_id').val();
        var tables = $('.report_tables').val();
        var client = $('.client').val();
        var tool = $('.tool').val();
        var field_name = table_fields;
        var start_date = $('.start_date').val();
        var end_date = $('.end_date').val();
        var choose_field = choose_field;
        var operator = oper;
        var conditionsearch = conditiontextbox;

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseURL + 'reports/ajax_edit_save_report',
            data: {save_id: save_id, field_name: field_name, tables: tables, client: client, tool: tool, start_date: start_date, end_date: end_date, choose_field: choose_field, operator: operator, conditionsearch: conditionsearch},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                    $('#responsive').modal('hide');
                } else {
                    alert(response.message);
                }
            }
        });




    });

    var wrapper = $('.field_wrapper'); //Input field wrapper
    $(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
        var countDiv = $('.add_bar').length;
        if (countDiv <= 2) {
            $('.topclosebtn').show();
        }
        e.preventDefault();
        $(this).parent().parent('div').remove();

    });

    $('#panel1').on('show.bs.collapse hidden.bs.collapse', function (e) {
        if (e.type == 'hidden') {
            $('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
            $(window).bind('resize', function () {
                $('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
                //alert('resized');
            });
        } else {
            $('.dataTables_scrollBody').css({'height': '210px'});
        }
        $(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');


    });







//    if (action = 'edit')
//    {
    $.ajax({
        type: 'POST',
        url: baseURL + 'reports/ajax_table_header',
        data: {save_id: save_id},
        success: function (response) {
            $('.saveReportDatatable').html(response);
            if (table == '3#macd') {
                $(".tool-start .fs-wrap").css("pointer-events", "none");
                $(".tool-start .fs-label-wrap").css("background", "#ccc");
            } else {
                $(".tool-start  .fs-wrap").css("pointer-events", "");
                $(".tool-start .fs-label-wrap").css("background", "#fff");
            }
        }
    });
    //}

    //alert(action);


    $.ajax({
        type: 'POST',
        url: baseURL + 'reports/ajax_columnList_save',
        data: {tableId: table, saveid: save_id},
        success: function (data) {
            $('#bind_field').html(data);
        }
    });


    $(document).on('change', '.choose_field', function () {

        var myval = $(this).val().split('#');
        var datatype = myval['2'];
        if (datatype == 'datetime') {
            $(".datepicker12").datepicker();
            $(this).parent().find('.conditiontextbox').val("");
        } else {
            $('.datepicker12').datepicker('remove');
            $(this).parent().find('.conditiontextbox').val("");
        }


    });

    $(document).on('change', '.report_tables', function () {
        $(".fs-wrap").addClass("costum-tools");
        tableId = $(this).val();
        if (tableId == '3#macd') {
            $(".tool-start .fs-wrap").css("pointer-events", "none");
            $(".tool-start .fs-label-wrap").css("background", "#ccc");

        } else {
            $(".tool-start  .fs-wrap").css("pointer-events", "");
            $(".tool-start .fs-label-wrap").css("background", "#fff");
        }
        if (tableId) {
            $.ajax({
                type: 'POST',
                url: baseURL + 'reports/ajax_columnList_save',
                data: {tableId: tableId},
                success: function (data) {
                    $('.add_bar').remove();
                    $('#bind_field').html(data);
                }
            });
        }
    });
    $('.client').fSelect();
    $('.tool').fSelect();
    $(document).on('change', '.choose_field,.oper', function (e) {
        if ($(this).val()) {
            $(this).css({
                "border": "",
                "background": ""
            });
        } else {
            $(this).css({
                "border": "1px solid red",
                "background": "#f8efef"
            });
        }
    });

    $(document).on('keyup', '.conditiontextbox', function () {
        if ($(this).val()) {
            $(this).css({
                "border": "",
                "background": ""
            });
        } else {
            $(this).css({
                "border": "1px solid red",
                "background": "#f8efef"
            });
        }
    });

    $(".filterSubmit").click(function (event) {
        event.preventDefault();
        var isValid = true;
        $('.choose_field,.oper,.conditiontextbox').each(function () {
            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    "background": "#f8efef"
                });
            } else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });

        if (isValid) {
            table_fields = [];
            $("#lstBox2 option").each(function () {
                var myval = $(this).val().split('#');
                table_fields.push(myval[1]);
            });
            if (table_fields != "") {
                var formData = $('#form_filter').serializeArray();

                formData.push({name: "table_fields", value: table_fields});
                $.ajax({
                    type: 'POST',
                    url: baseURL + 'reports/ajax_filters',
                    data: formData,
                    success: function (response) {
                        //$('#is_page_load_status').val(0);
                        $('.saveReportDatatable').html(response);

                    }
                });
            } else {
                bootbox.alert({
                    message: "Selected Columns should not be Blank",
                    className: "combobox-select"
                });

                return false;
            }
        }

    });



    //Start Save Report par


    $(document).on('click', '.save_report', function (event) {
        event.preventDefault();
        var formData = $('#form_save_report').serializeArray();
        $.ajax({
            type: 'POST',
            url: baseURL + 'reports/ajax_fetch_savereport_list',
            data: formData,
            success: function (response) {
                $('.saveReportDatatable').html(response);
            }
        });
    });



    $(document).on('click', '.report-delete', function () {

        //$(this).closest('.dash-drag').remove();
        save_id = $(this).attr('save_id');
        key = $(this).attr('keyid');

        // status = $(this).attr('status');
        swal({
            title: "Are you sure?",
            text: "you want to delete ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: baseURL + 'reports/ajax_delete_report',
                            data: {save_id: save_id},
                            success: function (response) {
                                if (response.status == 'true') {
                                    // $(this).closest('.dash-drag').remove();
                                    $('.deleterep_' + key).remove();
                                    swal("", response.message, "success");
                                } else {
                                    alert(response.message);
                                }
                            }
                        });
                    }
                });

    });


    $(document).on('click', '.report-view', function (event) {
        event.preventDefault();
        save_id = $(this).attr('save_id');
        key = $(this).attr('keyid');
        $('.bg1').removeClass('active');
        $(this).parent().addClass("active");
        $.ajax({
            type: 'POST',
            url: baseURL + 'reports/ajax_table_header',
            data: {save_id: save_id},
            success: function (response) {
                $('.saveReportDatatable').html(response);
            }
        });
    });


});



