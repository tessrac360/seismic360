$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
		var client_holidays_uuid = $(this).attr('client_holidays_uuid');
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/ClientHolidays/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'client_holidays_uuid': client_holidays_uuid},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
 
});

$(document).on('click', '.isDelete', function () {
	var baseURL = $('#baseURL').val();
	uuid = $(this).attr('uuid');
	// status = $(this).attr('status');
	swal({
		title: "Are you sure?",
		text: "you want to delete ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		cancelButtonText: "No",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function (isConfirm) {
		if (isConfirm) {
			window.location.href = baseURL + 'client/Contracts/deleteContract/' + uuid;
		}
	});

});

$(document).on('change', '.alert', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = $('#validity_uuid').val();
	var alertvalue = parseInt(this.value);
	
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/Contracts/ajax_getValidityDuration',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			var validity =parseInt(data.status);
			var less2 =parseInt(2);
			//alert(validity);
			if(alertvalue >= validity-less2)
			{
				swal("Oops!!!", " Expiry Alert value should less than validity period!", "warning");
				this.value = '';
				//$('#' + alertvalue).val('');
				$('#' + alertvalue).focus(); 
			}
			/* if (data.status == 'true') {
				swal("Oops!!!", value + " Holiday already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus(); 
			} else {
				
			}*/
		}
	});
	
});
