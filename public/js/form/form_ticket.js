$('.onchange').on('change', function () {
	var baseURL = $('#baseURL').val();
	$("#siddhu").html("");
	var clientId = $('#id').val();
	var status_code = $('#status_code').val();
	var ticket_cate_id = $('#ticket_cate_id').val();
	if($(this).attr("id") == 'ticket_cate_id')
	{
		ticket_sub_cate(ticket_cate_id);
	}
	
	var ticket_sub_cate_id = $('#ticket_sub_cate_id').val();
	var ticket_type_id = $('#ticket_type_id').val();
	var queue_id = $('#queue_id').val();
	var user_id = $('#user_id').val();
	//var client_id = $('#client_id').val();
	var severity_id = $('#severity_id').val();
	var priority_id = $('#priority_id').val();
	var date = $('#date').val();
	var user = $('#user').val();
	var myincidents = $('#myincidents').val();

	$.ajax({
			type: 'POST',
			url: baseURL + 'event/ticket/ajax_client_incidents',
			data: {'id':clientId, 'status_code':status_code, 'ticket_cate_id':ticket_cate_id, 'ticket_sub_cate_id':ticket_sub_cate_id, 'ticket_type_id':ticket_type_id, 'queue_id':queue_id, 'user_id':user_id,'severity_id':severity_id, 'priority_id':priority_id, 'date':date, 'user':user, 'myincidents':myincidents},
			success: function (response) {
				//alert(response);
				  if(response)
				  {
					//alert(response);
					  $("#siddhu").replaceWith(response);
				  }
					
				}
		});
		
});

function ticket_sub_cate(ticket_cate_id)
{
	var baseURL = $('#baseURL').val();
	
	$.ajax({
		type: 'POST',
		url: baseURL + 'event/ticket/ajaxGetAllSubCategories',
		data: {'ticket_cate_id':ticket_cate_id},
		success: function (response) {
			  if(response)
			  {
				  $("#ticket_sub_cate_id").html(response);
			  }
				
			}
	});
		
}

$(document).on('click', '#btn_incidentSearch', function () {
	var baseURL = $('#baseURL').val();	
	$("#siddhu").html("");
	var incidentSearch = $('#incidentSearch').val();
	var user = $('#user').val();
	var myincidents = $('#myincidents').val();
	var clientId = $('#id').val();
	//alert(user_id);
	$.ajax({
			type: 'POST',
			url: baseURL + 'event/ticket/ajax_incidents_search',
			data: {'incidentSearch':incidentSearch, 'user':user, 'myincidents':myincidents, 'id':clientId},
			success: function (response) {
				//alert(response);
				  if(response)
				  {
					//alert(response);
					  $("#siddhu").replaceWith(response);
				  }
					
				}
		});
});