$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'settings/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
    $('#contentspan').html('Email id Value');
    $('.setting_type').change(function () {
        var setting_type = $(this).val();
        if (setting_type == 2) {
            $('#contentspan').html('SMS Value');
        } else {
            $('#contentspan').html('Email id Value');
        }
    });

    $('#content').summernote({height: 200,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['fontname', ['fontname']],
            ['table', ['table']],
            ['link', ['link']],
            ['codeview', ['codeview']]            
        ]
    });
    $('#client').multiselect({
        //includeSelectAllOption: true,
        //nonSelectedText:'Services'
    });
    $("form[name='frmSettings']").validate({
        rules: {
            client: "required",
            slug: "required",
            subject: "required",
        },
        // Specify validation error messages
        messages: {
            client: "Please select Client",
            slug: "Please enter Unique SLUG",
            subject: "Please enter Email Subject",
        },

        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            var code = $('#content').val();
            if (code == " " || code == '<p><br></p>') {
                swal("Oops!!!", "Email content is required", "warning");
            } else {
                form.submit();
            }
        }
    });
    $('#slug').on('change', function () {
        $('#errmsg').html('').hide();
        var value = this.value;
        $.ajax({
            type: 'POST',
            url: baseURL + 'settings/ajax_checkUnique',
            dataType: 'json',
            data: {value: value},
            success: function (data) {
                if (data.status == 'true') {
                    swal("Oops!!!", "Slug is already exists!", "warning");
                    $('#slug').val('');
                    $('#slug').focus();
                }
            }
        });
    });
    $("form[name='frmSettingssmsandemail']").validate({
        rules: {

            slug: "required",
            subject: "required",
            content: "required",
        },
        // Specify validation error messages
        messages: {

            slug: "Please enter Unique SLUG",
            subject: "Please enter Email Subject",
            content: "Please enter values"
        },

        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            var code = $('#content').val();
            $('#clienerror').hide();
            if ($("select[name='client[]']").val() == null || $("select[name='client[]']").val() == "") {
                $('#clienerror').show();
            } else {
                form.submit();
            }
        }
    });

});