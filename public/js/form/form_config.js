jQuery.validator.addMethod("twointergerval", function(value, element) {
// allow any non-whitespace characters as the host part
return this.optional( element ) || /0|1$/.test( value );
}, 'Please enter a proper letters.');
 $("form[name='frmHost']").validate({
        // Specify validation rules
        rules: {
            device_name: "required",
			device_template_id: "required",
			address: "required",
			service_template_id: "required",		
			host_display_name : "required",
			host_parents : "required",
			host_hourly_value : "required",
			host_devicegroups : "required",
			host_check_command : "required",
			host_initial_state : "required",
			host_max_check_attempts: {
				required: true,
				number: true
			},				
			host_check_interval: {
				required: true,
				number: true
			},	
			host_retry_interval: {
				required: true,
				number: true
			},
			host_active_checks_enabled: {
				required: true,
				twointergerval: true
			},
			host_passive_checks_enabled: {
				required: true,
				twointergerval: true
			},			
			host_check_period : "required",
			host_obsess_over_device: {
				required: true,
				twointergerval: true
			},
			host_check_freshness: {
				required: true,
				twointergerval: true
			},
			host_freshness_threshold: {
				required: true,
				number: true
			},			
			host_event_handler : "required",
			host_event_handler_enabled : "required",
			host_low_flap_threshold : "required",
			host_high_flap_threshold : "required",
			host_flap_detection_enabled : "required",
			host_flap_detection_options : "required",
			host_process_perf_data : "required",
			host_retain_status_information : "required",
			host_retain_nonstatus_information : "required",
			host_contacts : "required",
			host_contact_groups : "required",
			host_notification_interval: {
				required: true,
				number: true
			},	
			host_first_notification_delay: {
				required: true,
				number: true
			},			
			host_notification_period : "required",
			host_notification_options : "required",
			host_notifications_enabled: {
				required: true,
				twointergerval: true
			},			
			host_stalking_options : "required",
			host_notes : "required",
			host_notes_url : "required",
			host_action_url : "required",
			host_icon_image : "required",
			host_icon_image_alt : "required",
			host_vrml_image : "required",
			host_statusmap_image : "required",
			host_2d_coords : "required",
			host_3d_coords : "required",			
			//event_handler_enabled: "required",
			
        },
        // Specify validation error messages
        messages: {
            device_name: "Please enter host name",
			device_template_id: "Please select host template",
			address: "Please enter ip address",
			service_template_id: "Please select service template",
			
			host_display_name : "Please enter  display name",
			host_parents : "Please enter host parents",
			host_hourly_value : "Please enter hourly value",
			host_devicegroups : "Please enter devicegroups",
			host_check_command : "Please enter check command",
			host_initial_state : "Please enter initial state",
			host_max_check_attempts: {
				required:  "Please enter max check attempts",
				number:  "Please enter numeric value"
			},			
			host_check_interval: {
				required:  "Please enter check interval",
				number:  "Please enter numeric value"
			},	
			host_retry_interval: {
				required:  "Please enter retry interval",
				number:  "Please enter numeric value"
			},	
			host_active_checks_enabled: {
				required: "Please enter active checks enabled" ,
				twointergerval: "Please enter 0 or 1"
			},	
			host_passive_checks_enabled: {
				required: "Please enter passive checks enabled" ,
				twointergerval: "Please enter 0 or 1"
			},				
			host_check_period : "Please enter check period",
			host_obsess_over_device: {
				required: "Please enter obsess over device",
				twointergerval: "Please enter 0 or 1"
			},
			host_check_freshness: {
				required: "Please enter check freshness",
				twointergerval: "Please enter 0 or 1"
			},
			host_freshness_threshold: {
				required: "Please enter freshness threshold",
				number: "Please enter numeric value"
			},			
			host_event_handler : "Please enter event handler",
			host_event_handler_enabled : "Please enter event handler enabled",
			host_low_flap_threshold : "Please enter low flap threshold",
			host_high_flap_threshold : "Please enter high flap threshold",
			host_flap_detection_enabled : "Please enter flap detection enabled",
			host_flap_detection_options : "Please enter flap detection options",
			host_process_perf_data : "Please enter process perf data",
			host_retain_status_information : "Please enter retain status information",
			host_retain_nonstatus_information : "Please enter retain nonstatus information",
			host_contacts : "Please enter contacts",
			host_contact_groups : "Please enter contact groups",
			host_notification_interval: {
				required: "Please enter notification interval",
				number: "Please enter numeric value"
			},	
			host_first_notification_delay: {
				required: "Please enter first notification delay",
				number: "Please enter numeric value"
			},				
			host_notification_period : "Please enter notification period",
			host_notification_options : "Please enter notification options",
			host_notifications_enabled: {
				required: "Please enter notifications enabled",
				twointergerval: "Please enter 0 or 1"
			},			
			host_stalking_options : "Please enter stalking_options",
			host_notes : "Please enter notes",
			host_notes_url : "Please enter notes url",
			host_action_url : "Please enter action url",
			host_icon_image : "Please enter icon image",
			host_icon_image_alt : "Please enter icon image alt",
			host_vrml_image : "Please enter vrml image",
			host_statusmap_image : "Please enter statusmap image",
			host_2d_coords : "Please enter 2d coords",
			host_3d_coords : "Please enter 3d coords",
			
			//event_handler_enabled: "Please enter 0 or 1(0 for disable and 1 for enable)",
        },
		
  });
  
   $("form[name='frmService']").validate({
        // Specify validation rules
        rules: {
            service_template_id: "required",
			service_description: "required",
			check_command: "required",
			
			//event_handler_enabled: "required",
			
        },
        // Specify validation error messages
        messages: {
            service_template_id: "Please select service template",
			service_description: "Please enter service description",
			check_command: "Please enter command",
			
			//event_handler_enabled: "Please enter 0 or 1(0 for disable and 1 for enable)",
        },
		
  });
  
  $("#device_name").keyup(function () {
        $('#alias').val($(this).val());
		//$("#alias").prop("readonly", true);
    });

   
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}  

  
$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'users/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });



 

    $('#device_template_id').on('change', function () {
        $("#host_template_list").html("");
        var templateId = this.value;
		$.ajax({
				type: 'POST',
				url: baseURL + 'nagiosmanagements/nagiosmanagements/ajax_host_template',
				data: {'device_template_id': templateId},
				success: function (response) {
						$("#host_template_list").html(response);
                    }
            });
			
    });
	

    $('#service_template_id').on('change', function () {
        $("#service_template_list").html("");
        var templateId = this.value;
		$.ajax({
				type: 'POST',
				url: baseURL + 'nagiosmanagements/nagiosmanagements/ajax_service_template',
				data: {'service_template_id': templateId},
				success: function (response) {
						$("#service_template_list").html(response);
                    }
            });
    });
    $('#client').on('change', function () {
        $("#role_id").empty();
        var clientId = this.value;
        console.log(clientId);//alert(clientId);
        $.ajax({
            type: 'POST',
            url: baseURL + 'users/admin/ajax_getRoles',
            dataType: 'json',
            data: {'clientId': clientId},
            success: function (response) {
                if (response.status == 'true') {
                    $.each(response.resultSet, function (key, value) {
                        $("#role_id").append("<option value=" + value.id + ">" + value.title + "</option>");
                    });
                } else {
                    $("#role_id").append("<option value=''>No Role Found</option>");
                }
            }
        });
    });


    $(document).on('click', '.status_islogin', function () {
        id = $(this).attr('callid');
        status = $(this).attr('status');
        swal({
            title: "Are you sure?",
            text: "you want to manually logout",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: baseURL + 'users/ajax_logout',
                            data: {'status': status, 'id': id},
                            success: function (response) {
                                if (response.status == 'true') {
                                    // console.log(response);
                                    $(".islogin_" + id).html(response.html);
                                    swal("Logged Out!", "", "success");
                                }
                            }
                        });


                    } else {

                        swal("Cancelled", "", "error");
                    }
                });

    });

    $("#email_id").keyup(function () {
        if ($(this).val().length >= 1 && $(this).val().length <= 2) {
            $('#errmsg').html('').hide();
        }
        $('#user_name').val($(this).val());
    });

    $("#user_name").prop("readonly", true);
    $.validator.addMethod("needsSelection", function (value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });


    $("form[name='frmUser']").validate({
        // Specify validation rules
        rules: {
            first_name: "required",
            last_name: "required",
            email_id: {
                required: true,
                email: true
            },
            phone: {
                required: true,
            },
            password: {
                required: true,
                minlength: 5
            },
            role_id: {
                required: {
                    depends: function (element) {
                        return $("#role_id").val() == "";
                    }
                }
            },
            client: "required",
        },
        // Specify validation error messages
        messages: {
            first_name: "Please enter your First name",
            last_name: "Please enter your lastname",
            role_id: "Please enter Role Type",
            phone: {
                required: "Please enter your Phone number",
            },
            email_id: "Please enter a valid email address",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            client: "Please select Client",

        },

        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            $("#group_id_error").text("");
            $("#skill_error").text("");
            $("#notification_error").text("");
            var Domains = $('#frmUser input[name="group_title[]"]:checked').size();
            var skill = $('#frmUser input[name="skill[]"]:checked').size();
            var notification = $('#frmUser .notification:checked').size();
            console.log(Domains + '-----' + skill + '---------' + notification);

            //return false;
            if (Domains == "0") {
                $("#group_id_error").text('Please select atleast one domains').css('color', 'red');
                return false;
            } else if (skill == "0") {
                $("#skill_error").text('Please select atleast one skill').css('color', 'red');
                return false;
            } else if (notification == "0") {
                $("#notification_error").text('Please select atleast one notification').css('color', 'red');
                return false;
            } else {
                form.submit();
            }
        }
    });

    $("#frmUser").submit(function () {
        var valu;
        valu = $("input[name='user_type']:checked").val();
        if (valu == 'S') {
            var checkedVals = $('.groupcheck:checkbox:checked').map(function () {
                return this.value;
            }).get();
            var data = checkedVals.join(",");
            if (data == '') {
                $("#group_id_error").text('Please select atleast one group');
                $("#group_id_error").css('color', 'red');
                return false;
            } else {
                $("#group_id_error").text('');
            }
        } else {

        }
    });





    $("form[name='frmEditUser']").validate({
        // Specify validation rules
        rules: {
            first_name: "required",
            last_name: "required",
            email_id: {
                required: true,
                email: true
            },
            phone: {
                required: true,
            },
            password: {

                minlength: 5
            },
            role_id: {
                required: {
                    depends: function (element) {
                        return $("#role_id").val() == "";
                    }
                }
            },
            client: "required",
        },
        // Specify validation error messages
        messages: {
            first_name: "Please enter your First name",
            last_name: "Please enter your lastname",
            role_id: "Please enter Role Type",
            phone: {
                required: "Please enter your Phone number",
            },
            email_id: "Please enter a valid email address",
            password: {

                minlength: "Your password must be at least 5 characters long"
            },
            client: "Please select Client",

        },

        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {

            $("#group_id_error").text("");
            $("#skill_error").text("");
            $("#notification_error").text("");
            var Domains = $('#frmEditUser input[name="group_title[]"]:checked').size();
            var skill = $('#frmEditUser input[name="skill[]"]:checked').size();
            var notification = $('#frmEditUser .notification:checked').size();
            console.log(Domains + '-----' + skill + '---------' + notification);
            if (Domains == "0") {
                $("#group_id_error").text('Please select atleast one domains').css('color', 'red');
                return false;
            } else if (skill == "0") {
                $("#skill_error").text('Please select atleast one skill').css('color', 'red');
                return false;
            } else {
                form.submit();
            }


        }
    });





    $('#device_name').on('change', function () {
        //$('#errmsg').html('').hide();
        var table = $(this).attr('tableName');
        var field = $(this).attr('tableField');
		var gateway_id = $(this).attr('gateway_id');
        var value = this.value;
        //var old_email = $('#old_email').val();
        var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + 'nagiosmanagements/nagiosmanagements/ajax_checkUniqueHostName',
            dataType: 'json',
            data: {table: table, field: field, value: value, gateway_id: gateway_id},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                    $('#' + field).val('');
                    $('#alias').val('');
                    $('#' + field).focus();
                } else {

                }
            }
        });
    });
	
	
    $('#address').on('change', function () {
        //$('#errmsg').html('').hide();
        var table = $(this).attr('tableName');
        var field = $(this).attr('tableField');
		var gateway_id = $(this).attr('gateway_id');
        var value = this.value;
        //var old_email = $('#old_email').val();
        var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + 'nagiosmanagements/nagiosmanagements/ajax_checkUniqueHostAddress',
            dataType: 'json',
            data: {table: table, field: field, value: value,gateway_id: gateway_id},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                    $('#' + field).val('');
                    //$('#alias').val('');
                    $('#' + field).focus();
                } else {

                }
            }
        });
    });
   
   


   $('.groupcheck').click(function () {
        check_val = $(this).prop('checked');
        value = $(this).attr('id');
        id_val = 'locations_' + value;
        if (check_val) {
            $("#group_id_error").text('');
            $("#" + id_val).css("display", "block");
        } else {
            $("#" + id_val).css("display", "none");
        }
    });

    // Start change password 

    $("form[name='frmUserChangepassword']").validate({
        // Specify validation rules
        rules: {
            old_password: {
                required: true,
                minlength: 5
            },
            password: {
                required: true,
                minlength: 5
            },
            password_confirm: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            old_password: {
                required: "Enter Old Password",
            },
            password: {
                required: "Enter Password",
            },
            password_confirm: {
                required: "Enter Confirm Password",
                equalTo: " Enter Confirm Password Same as Password"
            },

        },

        submitHandler: function (form) {
            form.submit();
        }
    });


    $(document).on('click', '.isdeleteAdminUsers', function () {
       userId = $(this).attr('userId');
        // status = $(this).attr('status');
        swal({
            title: "",
            text: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = baseURL + 'users/admin/delete/' + userId;
                    }
                });

    });


    $(document).on('click', '.isdeleteUser', function () {
        userId = $(this).attr('userId');
        // status = $(this).attr('status');
        swal({
            title: "Are you sure?",
            text: "you want to delete ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = baseURL + 'users/delete/' + userId;
                    }
                });

    });
    
    
    
// End change password   


$("form[name='frmChangeprofile']").validate({
        // Specify validation rules
        rules: {
            first_name: "required",
            last_name: "required",
            email_id: {
                required: true,
                email: true
            },
            phone: {
                required: true,
            }
        },
        // Specify validation error messages
        messages: {
            first_name: "Please enter your First name",
            last_name: "Please enter your lastname",           
            phone: {
                required: "Please enter your Phone number",
            }
        },

        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
             form.submit();
        }
    });

});

