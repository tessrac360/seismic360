
$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $(document).on('change', '.onoffactiontoggle', function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'users/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });

    $('.partner').on('change', function () {
        $("#role_id").empty();
        $("#subClients_list").html("");
        var clientId = this.value;
        var user_id = $("#user_id").val();
        // console.log(clientId);
        //alert(user_id);

        $.ajax({
            type: 'POST',
            url: baseURL + 'users/partner/ajax_getRoles',
            dataType: 'json',
            data: {'clientId': clientId},
            success: function (response) {
                if (response.status == 'true') {
                    $.each(response.resultSet, function (key, value) {
                        $("#role_id").append("<option value=" + value.id + ">" + value.title + "</option>");
                    });
                } else {
                    $("#role_id").append("<option value=''>No Role Found</option>");
                }
            }
        });



    });


    $('#client').on('change', function () {
        $("#role_id").empty();
        var clientId = this.value;
        console.log(clientId);//alert(clientId);
        $.ajax({
            type: 'POST',
            url: baseURL + 'users/admin/ajax_getRoles',
            dataType: 'json',
            data: {'clientId': clientId},
            success: function (response) {
                if (response.status == 'true') {
                    $.each(response.resultSet, function (key, value) {
                        $("#role_id").append("<option value=" + value.id + ">" + value.title + "</option>");
                    });
                } else {
                    $("#role_id").append("<option value=''>No Role Found</option>");
                }
            }
        });
    });


    $(document).on('click', '.status_islogin', function () {
        id = $(this).attr('callid');
        status = $(this).attr('status');
        swal({
            title: "Are you sure?",
            text: "you want to manually logout",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: baseURL + 'users/ajax_logout',
                            data: {'status': status, 'id': id},
                            success: function (response) {
                                if (response.status == 'true') {
                                    // console.log(response);
                                    $(".islogin_" + id).html(response.html);
                                    swal("Logged Out!", "", "success");
                                }
                            }
                        });


                    } else {

                        swal("Cancelled", "", "error");
                    }
                });

    });

    $("#email_id").keyup(function () {
        if ($(this).val().length >= 1 && $(this).val().length <= 2) {
            $('#errmsg').html('').hide();
        }
        $('#user_name').val($(this).val());
    });

    $("#user_name").prop("readonly", true);
    $.validator.addMethod("needsSelection", function (value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });


    $("form[name='frmUser']").validate({
        // Specify validation rules
        rules: {
            first_name: "required",
            last_name: "required",
            email_id: {
                required: true,
                email: true
            },

            password: {
                required: true,
                minlength: 5
            },
            client: "required",
        },
        // Specify validation error messages
        messages: {
            first_name: "Please enter your First name",
            last_name: "Please enter your Last name",
            email_id: "Please enter a valid email address",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            client: "Please select Client",
        },

        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            $("#skill_error").text("");
            $("#role_error").text("");
            $("#assign_error").text("");
            var skill = $('#frmUser input[name="skill[]"]:checked').size();
            var role_id = $("#role_id").val();
            var assign_group = $("#assign_group_id :selected").length;
            if (role_id == "0") {
                $("#role_error").text('Please select Role').css('color', 'red');
                return false;
            } else if (skill == "0") {
                $("#skill_error").text('Please select atleast one skill').css('color', 'red');
                return false;
            } else if (assign_group == "0") {
                $("#assign_error").text('Please select atleast one group').css('color', 'red');
                return false;
            } else {
                form.submit();
            }
        }
    });


    $("#frmUser").submit(function () {
        var valu;
        valu = $("input[name='user_type']:checked").val();
        if (valu == 'S') {
            var checkedVals = $('.groupcheck:checkbox:checked').map(function () {
                return this.value;
            }).get();
            var data = checkedVals.join(",");
            if (data == '') {
                $("#group_id_error").text('Please select atleast one group');
                $("#group_id_error").css('color', 'red');
                return false;
            } else {
                $("#group_id_error").text('');
            }
        } else {

        }
    });





    $("form[name='frmEditUser']").validate({
        // Specify validation rules
        rules: {
            first_name: "required",
            last_name: "required",
            email_id: {
                required: true,
                email: true
            },
            password: {

                minlength: 5
            },
            client: "required",
        },
        // Specify validation error messages
        messages: {
            first_name: "Please enter your First name",
            last_name: "Please enter your lastname",
            email_id: "Please enter a valid email address",
            password: {

                minlength: "Your password must be at least 5 characters long"
            },
            client: "Please select Client",
        },

        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            $("#assign_error").text("");
            var assign_group = $("#assign_group_id :selected").length;
            if (assign_group == "0") {
                $("#assign_error").text('Please select atleast one group').css('color', 'red');
                return false;
            } else {
                form.submit();
            }
        }
    });





    $('#email_id').on('change', function () {
        $('#errmsg').html('').hide();
        var table = $(this).attr('tableName');
        var field = $(this).attr('tableField');
        var value = this.value;
        var old_email = $('#old_email').val();
        var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + 'users/ajax_checkUnique',
            dataType: 'json',
            data: {table: table, field: field, value: value, id: id, old_email: old_email},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                    $('#' + field).val('');
                    $('#user_name').val('');
                    $('#' + field).focus();
                } else {

                }
            }
        });
    });
    $('.groupcheck').click(function () {
        check_val = $(this).prop('checked');
        value = $(this).attr('id');
        id_val = 'locations_' + value;
        if (check_val) {
            $("#group_id_error").text('');
            $("#" + id_val).css("display", "block");
        } else {
            $("#" + id_val).css("display", "none");
        }
    });

    // Start change password 

    $("form[name='frmUserChangepassword']").validate({
        // Specify validation rules
        rules: {
            old_password: {
                required: true,
                minlength: 5
            },
            password: {
                required: true,
                minlength: 5
            },
            password_confirm: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            old_password: {
                required: "Enter Old Password",
            },
            password: {
                required: "Enter Password",
            },
            password_confirm: {
                required: "Enter Confirm Password",
                equalTo: " Enter Confirm Password Same as Password"
            },

        },

        submitHandler: function (form) {
            form.submit();
        }
    });


    $(document).on('click', '.isdeleteAdminUsers', function () {
        userId = $(this).attr('userId');
        // status = $(this).attr('status');
        swal({
            title: "",
            text: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = baseURL + 'users/admin/delete/' + userId;
                    }
                });

    });


    $(document).on('click', '.isdeleteUser', function () {
        userId = $(this).attr('userId');
        // status = $(this).attr('status');
        swal({
            title: "Are you sure?",
            text: "you want to delete ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = baseURL + 'users/delete/' + userId;
                    }
                });

    });



// End change password   


    $("form[name='frmChangeprofile']").validate({
        // Specify validation rules
        rules: {
            first_name: "required",
            last_name: "required",
            email_id: {
                required: true,
                email: true
            },
            phone: {
                required: true,
            }
        },
        // Specify validation error messages
        messages: {
            first_name: "Please enter your First name",
            last_name: "Please enter your Last name",
            phone: {
                required: "Please enter your Phone number",
            }
        },

        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });



    $("form[name='frmpartner']").validate({
        // Specify validation rules
        rules: {
            first_name: "required",
            last_name: "required",
            email_id: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            client: "required",
            role_id: {
                required: {
                    depends: function (element) {
                        return $("#role_id").val() == "";
                    }
                }
            },
        },
        // Specify validation error messages
        messages: {
            first_name: "Please enter your First name",
            last_name: "Please enter your Last name",
            email_id: "Please enter a valid email address",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            client: "Please select Client",
            role_id: "Please enter Role Type",
        },

        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            $("#assign_error").text("");
            var assign_group = $("#assign_group_id :selected").length;
            if (assign_group == "0") {
                $("#assign_error").text('Please select atleast one group').css('color', 'red');
                return false;
            } else {
                form.submit();
            }
        }
    });

});

