$(document).ready(function () {
   
    $(document).on('click', '.addmore', function (event) {
        event.preventDefault();
        var condition = $(this).val();
        var countDiv = $('.add_bar').length;
        if (countDiv >= 1) {
            $('.topclosebtn').hide();
        }
        var formData = $('#form_filter').serializeArray();
		
        formData.push({name: "countDiv", value: countDiv});
        formData.push({name: "condition", value: condition});
        $.ajax({
            type: 'POST',
            url: baseURL + 'reports/ajax_managecondition',
            data: formData,
            success: function (response) {
                $('.addMoreDiv').append(response);
            }
        });
    });


    $(document).on('click', '.save-report', function (event) {
        event.preventDefault();
        var title = $.trim($('#title').val());        
        var report_status =  $('input[name=report_status]:checked').val();
        if (title == "") {
            $('#error_report').html('Title should not blank');
            return false;
        } else {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: baseURL + 'reports/ajax_save_report',
                data: {title: title,reportStatus:report_status},
                success: function (response) {
                    if (response.status == 'true') {
                        swal("", response.message, "success");
                        $('#title').val("");
                        $('#responsive').modal('hide');
                    } else {
                        alert(response.message);
                    }
                }
            });



        }

    });

    var wrapper = $('.field_wrapper'); //Input field wrapper
    $(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
        var countDiv = $('.add_bar').length;
        if (countDiv <= 2) {
            $('.topclosebtn').show();
        }
        e.preventDefault();
        $(this).parent().parent('div').remove();

    });

    $('#panel1').on('show.bs.collapse hidden.bs.collapse', function (e) {
        if (e.type == 'hidden') {
            $('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
            $(window).bind('resize', function () {
                $('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
                //alert('resized');
            });
        } else {
            $('.dataTables_scrollBody').css({'height': '210px'});
        }
        $(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');


    });

    var baseURL = $('#baseURL').val();
    $.ajax({
        type: 'POST',
        url: baseURL + 'reports/ajax_columnList',
        data: {tableId: '1#events'},
        success: function (data) {
            //$(".fs-wrap").addClass("cost-tools");
            $('#bind_field').html(data);
            table_fields = [];
            $("#lstBox2 option").each(function () {
                var myval = $(this).val().split('#');
                table_fields.push(myval[1]);
            });
            var formData = $('#form_filter').serializeArray();
			
            formData.push({name: "table_fields", value: table_fields});
            $.ajax({
                type: 'POST',
                url: baseURL + 'reports/ajax_filters',
                data: formData,
                success: function (response) {
                    $('.resultSetFilter').html(response);
                }
            });
        }
    });


    $(document).on('change', '.choose_field', function () {

        var myval = $(this).val().split('#');
        var datatype = myval['2'];
        if (datatype == 'datetime') {
            $(".datepicker12").datepicker();
            $(this).parent().find('.conditiontextbox').val("");
        } else {
            $('.datepicker12').datepicker('remove');
            $(this).parent().find('.conditiontextbox').val("");
        }


    });

    $(document).on('change', '.report_tables', function () {
        //$(".fs-wrap").addClass("costum-tools");
        tableId = $(this).val();
        if(tableId == '3#macd'){
           $(".tool-start .fs-wrap").css("pointer-events", "none");
           $(".tool-start .fs-label-wrap").css("background", "#ccc");
        }else{
          $(".tool-start  .fs-wrap").css("pointer-events", "");
          $(".tool-start .fs-label-wrap").css("background", "#fff");
        }
        if (tableId) {
            $.ajax({
                type: 'POST',
                url: baseURL + 'reports/ajax_columnList',
                data: {tableId: tableId},
                success: function (data) {
                    $('.add_bar').remove();       
                    $('#bind_field').html(data);
                }
            });
        }
    });
      $('.client').fSelect();
      $('.tool').fSelect();
    
//    $('#client').multiselect({
//        includeSelectAllOption: true,
//        nonSelectedText: 'Clients',
//        selectAllText: ' Select all',
//        maxHeight: 150,
//        buttonWidth: '100%'
//    });
    
//    $('#tool').multiselect({
//        includeSelectAllOption: true,
//        nonSelectedText: 'Tool',
//        selectAllText: ' Select all',
//        maxHeight: 150,
//        buttonWidth: '100%'
//    });
    
    
    $(document).on('change', '.choose_field,.oper', function (e) {
        if ($(this).val()) {
            $(this).css({
                "border": "",
                "background": ""
            });
        } else {
            $(this).css({
                "border": "1px solid red",
                "background": "#f8efef"
            });
        }
    });

    $(document).on('keyup', '.conditiontextbox', function () {
        if ($(this).val()) {
            $(this).css({
                "border": "",
                "background": ""
            });
        } else {
            $(this).css({
                "border": "1px solid red",
                "background": "#f8efef"
            });
        }
    });

    $(".filterSubmit").click(function (event) {
        event.preventDefault();
        var isValid = true;
        $('.choose_field,.oper,.conditiontextbox').each(function () {
            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    "background": "#f8efef"
                });
            } else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });

        if (isValid) {
            table_fields = [];
            $("#lstBox2 option").each(function () {
                var myval = $(this).val().split('#');
                table_fields.push(myval[1]);
            });
            if (table_fields != "") {
                var formData = $('#form_filter').serializeArray();

                formData.push({name: "table_fields", value: table_fields});
                $.ajax({
                    type: 'POST',
                    url: baseURL + 'reports/ajax_filters',
                    data: formData,
                    success: function (response) {
                        $('#is_page_load_status').val(0);
                        $('.resultSetFilter').html(response);

                    }
                });
            } else {
                bootbox.alert({
                    message: "Selected Columns should not be Blank",
                    className: "combobox-select"
                });

                return false;
            }
        }

    });



    //Start Save Report par


    $(document).on('click', '.save_report', function (event) {
        event.preventDefault();
        var formData = $('#form_save_report').serializeArray();
        $.ajax({
            type: 'POST',
            url: baseURL + 'reports/ajax_fetch_savereport_list',
            data: formData,
            success: function (response) {
                $('.saveReportDatatable').html(response);
            }
        });
    });


});



