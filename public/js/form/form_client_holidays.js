$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
		var client_holidays_uuid = $(this).attr('client_holidays_uuid');
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/ClientHolidays/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'client_holidays_uuid': client_holidays_uuid},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
 
});

$(document).on('click', '.isDelete', function () {
	var baseURL = $('#baseURL').val();
	client_holidays_uuid = $(this).attr('client_holidays_uuid');
	// status = $(this).attr('status');
	swal({
		title: "Are you sure?",
		text: "you want to delete ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		cancelButtonText: "No",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function (isConfirm) {
		if (isConfirm) {
			window.location.href = baseURL + 'client/ClientHolidays/deleteHolidays/' + client_holidays_uuid;
		}
	});

});

$(document).on('change', '.holiday', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var prev = $(this).data('current-date');
	var uuid = $(this).attr('uuid');
	var value = this.value;
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/ClientHolidays/ajax_checkUniqueDate',
		dataType: 'json',
		data: {table: table, field: field, value: value,uuid:uuid},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " Holiday already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {
				
			}
		}
	});
	
});


/* $(document).on('change', '.holiday_occasion', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	var date = $("#holiday_date").val();
	var id = "";
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/ClientHolidays/ajax_checkUniqueOccasion',
		dataType: 'json',
		data: {table: table, field: field, value: value, date: date},
		success: function (data) {
			if (data.status == 'true') {
				//$('#errmsg').html(value + ' already exists!').show();
	//                    bootbox.alert(value + ' already exists!', function () {
	//                    });
				swal("Oops!!!", value + " already exists!", "warning");
				$('#' + field).val('');
				$('#alias').val('');
				$('#' + field).focus();
			} else {

			}
		}
	});
}); */

