
$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/ContractValidations/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
});

$(document).on('change', '.validity_type', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	var id = "";
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/ContractValidations/ajax_checkUniqueValidityType',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {

			}
		}
	});
});

$(document).on('change', '.validity_duration', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	var id = "";
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/ContractValidations/ajax_checkUniqueValidityDuration',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {

			}
		}
	});
});


$(document).on('click', '.isDelete', function () {
	var baseURL = $('#baseURL').val();
	uuid = $(this).attr('uuid');
	// status = $(this).attr('status');
	swal({
		title: "Are you sure?",
		text: "you want to delete ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		cancelButtonText: "No",
		closeOnConfirm: true,
		closeOnCancel: true
	},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = baseURL + 'client/ContractValidations/deleteContractValidity/' + uuid;
				}
			});

});

$(document).on('click', '.isDefault', function () {

	var baseURL = $('#baseURL').val();
	var thisd = $(this);

	// status = $(this).attr('status');
	//$(this).prop('checked', false);
	swal({
		title: "Primary Contact is already set",
		text: "Do you want to set this contract as default ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		cancelButtonText: "No",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function (isConfirm) {
		if (isConfirm) {
			

		}else{
			//alert('else');
		 thisd.prop('checked', false);	
		}
	});

});


$(document).on('change', '.contract_period_type', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/contractperiod/ajax_checkUniqueContractType',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " Contract Period Type already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {
				
			}
		}
	});
	
});

$(document).on('change', '.contract_period', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/contractperiod/ajax_checkUniqueContractPeriod',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " Contract Period already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {
				
			}
		}
	});
	
});

