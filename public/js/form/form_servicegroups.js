$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'nagiosmanagements/ajaxChangeServiceTemplateStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
    $('#gateway_type_uuid').on('change', function () {
        $("#services_list").html("");
        var gatewaytype_uuid = this.value;
		var gateway_type_uuid = this.value;
		var device_category_id = $('#device_category_id').val();				
		$.ajax({
				type: 'POST',
				url: baseURL + 'nagiosmanagements/ajax_services_list',
				data: {'gatewaytype_uuid': gatewaytype_uuid},
				success: function (response) {
						$("#services_list").html(response);
                    }
        });
		
		$.ajax({
			type: 'POST',
			url: baseURL + 'nagiosmanagements/ajax_default_service_groups_check',
			dataType: 'json',
			data: {'device_category_id': device_category_id,'gateway_type_uuid':gateway_type_uuid},
			success: function (response) {
				if (response.status == 'true') {
					$('#default_group').attr('checked', false);				
				} else if (response.status == 'false') {
					$('#default_group').attr('checked', true);
				}
			}
		});			
    });	
	      $('#name').on('change', function () {
        //$('#errmsg').html('').hide();
        var table = $(this).attr('tableName');
        var field = $(this).attr('tableField');
        var value = this.value;
		var gateway_id = $(this).attr('gateway_id');
        //var old_email = $('#old_email').val();
        var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + '/nagiosmanagements/ajax_checkUniqueServiceTemName',
            dataType: 'json',
            data: {table: table, field: field, value: value,gateway_id:gateway_id},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                    $('#' + field).val('');
                    $('#alias').val('');
                    $('#' + field).focus();
                } else {

                }
            }
        });
    });
	
/*	$('#device_category_id,#gateway_type_uuid').change(function (e) {
		var category_id = $('#device_category_id').val();
		var gateway_type_id  = $('#gateway_type_uuid').val();
		if(category_id !='' && gateway_type_id !=''){
			$.ajax({
				type: 'POST',
				url: baseURL + '/nagiosmanagements/ajax_checkDefaultTemplate',
				dataType: 'json',
				data: {category_id: category_id, gateway_type_id: gateway_type_id},
				success: function (data) {
					if (data.status == 'true') {
						//swal("Oops!!!",  " already exists!", "warning");

					} else {

					}
				}
			});	
		}
		
	});*/
	
});
 $("form[name='frmServiceGroup']").validate({
        // Specify validation rules
        rules: {
			service_group_name: "required",
			device_category_id : "required",
			gateway_type_uuid : "required"
			//event_handler_enabled: "required",
			
        },
        // Specify validation error messages
        messages: {
			service_group_name: "Please enter service group name",
			device_category_id: "Please select device category",
			gateway_type_uuid: "Please select gateway type"
			//event_handler_enabled: "Please enter 0 or 1(0 for disable and 1 for enable)",
        },
		submitHandler: function (form) {
			
			var isValid = true;
			if ($(".services:checked").length < 1) 
			{ 
				 $("#serviceslisterror").text("Please select atleast one service"); 
				// cancel submit
				isValid = false;
			}
			$(".services:checked").each(function () {
			var id = $(this).attr("value");
			var services_temp_id = 'services_'+id;
			var services_temp_id_val = $('#'+services_temp_id).val();
					//alert(services_temp_id_val);
					if(services_temp_id_val != undefined){
						if(services_temp_id_val == ''){
							$('#'+services_temp_id).css({
							"border": "1px solid red",
							"background": "#f8efef"
							});
							isValid = false; 
						}else{
							$('#'+services_temp_id).css({
							"border": "",
							"background": ""
							});						
						}
					}
			});

					
			if (isValid) {
				//salert('submitted');
				//return false;
				form.submit();
			}

        }
		
  });
$(document).on('change', '.servicetemp, .services', function (e) {
	var tagnam = $(this).prop("tagName");
	
	if(tagnam === 'SELECT'){
	
		if ($(this).val()) {
			$(this).css({
			"border": "",
			"background": ""
			});		

		} else {
			$(this).css({
			"border": "1px solid red",
			"background": "#f8efef"
			});					
		}
	}else if(tagnam === 'INPUT'){
		if ($(this).val()) {
			$('#serviceslisterror').text(""); 		

		} else {
			$('#serviceslisterror').text("Please select atleast one service"); 				
		}					
	}
});

