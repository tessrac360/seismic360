
$(document).ready(function () {
	
    var baseURL = $('#baseURL').val();
	
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'sla/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
	
	$('.inc').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'sla/ajax_changeStatusInc',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
	$('#priority_id').change(function () {
		var request_type = '<div class="col-md-3 "><div class="form-group form-md-line-input form-md-floating-label"><select class="form-control" id="request_type" name="request_type" required><option></option><option value="move" > Move </option><option value="add"> Add </option><option value="change"> Change </option><option value="delete"> Disconnect/Delete </option></select><label for="form_control_1">Request Type</label></div></div>';		
		
		var priority_id = $(this).val();
		if( priority_id == 4){
			//alert('ok');
			$("#request_typediv").html(request_type);
		}else{
			$("#request_typediv").html('');	
		}
		
	});

 	$( ".postageyes" ).on( "change", function() {
	
	
	  var b = $('input[name=bussiness_hrs]:checked').val();
	  if(b == '1')
	  {
		  $("#shift_start_time").val('00:00');
		  $("#shift_start_time").prop('readonly', true);
		  $("#shift_start_time").val('00:00');	  
		  $("#shift_end_time").val('23:59');
		  $("#shift_end_time").prop('readonly', true);
	  }else{
			$("#shift_start_time").val('');
			$("#shift_start_time").prop('readonly', false);		  
			$("#shift_end_time").val('');
			$("#shift_end_time").prop('readonly', false);		  
	  }

	}); 

});


$(document).on('change', '.severity', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var client_id = $(this).attr('client_id');
	//alert(client_id);
	var editval = $(this).attr('editval');
	var value = this.value;
	$.ajax({
		type: 'POST',
		url: baseURL + '/sla/ajax_checkUniqueSlaSeverity',
		dataType: 'json',
		data: {table: table, field: field, value: value, client_id:client_id,editval:editval},
		success: function (data) {
			if (data.status == 'true') {
				swal("Oops!!!", value + " Sla Severity already exists!", "warning");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {
				
			}
		}
	});
	
});

 $("form[name='frmGateway']").validate({
        submitHandler: function (form) {
			$("#day_error").text("");
			if ($(".days:checked").length < 1){
				$("#day_error").text('Please select atleast one working day').css('color', 'red');
                return false;
			}else {
                form.submit();
            } 
        }
    });



	
	
    
