$(document).ready(function () {
    var baseURL = $('#baseURL').val();
   

    $(document).on('click', '.coupon_question', function () {
        var eventId = $(this).attr('evtid');

        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_openAcknowledgeDiv',
            data: {eventId: eventId},
            success: function (data1) {
                $('#myModal').modal();
                $('#load-data').html(data1);
            }
        });

    });

    
    
    
    $(document).on('click', '.suppress_action', function () {
        var eventId = $(this).attr('evtid');
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_openSuppressPopup',
            data: {eventId: eventId},
            success: function (data1) {
                $('#actionmodal').modal();
                $('#load-data-suppress').html(data1);
            }
        });

    });



  
    $(document).on('click', '#btnSorting', function () {
        var position = new Array();
        $('#sortable-row tbody tr td').each(function () {
            position.push($(this).attr("id"));
        });
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_eventSorting',
            dataType: 'json',
            data: {position: position},
            success: function (data) {
                if (data.status == 'true') {
                    window.location.href = baseURL + 'event';
//                    get_eventHeader();
//                    get_fb();
                }
            }
        });
    });

    $(document).on('click', '#btnmanualack', function (e) {
        e.preventDefault();
        $('#msg_manualack').html('');
        if ($.trim($('#txtmanualack').val()) == "") {
            $('#msg_manualack').html('Please enter acknowledge comments !!').css("color", "red");
            return false;
        } else {
            var eventId = [];
            var client_id = [];
            $("input[name='event_id[]']:checked").each(function () {
                eventId.push(parseInt($(this).val()));
                client_id.push(parseInt($(this).attr('clientVal')));
            });

            var data = {
                'eventId': eventId,
                'client_id': client_id,
                'ack_status_id': '2'
            };
            data = $('#frm_manual').serialize() + '&' + $.param(data);
            $.ajax({
                type: 'POST',
                url: baseURL + 'event/ajax_manualAcknowledge',
                dataType: 'json',
                data: data,
                async: false,
                success: function (response) {
                    $('.coupon_question').removeAttr('checked');
                    $(".parent").removeClass('col-md-9');
                    $(".parent").addClass('col-md-12');
                    $(".answer").hide();
                    $(".right_block").hide();
                    if (response.status == 'true') {
                        for (var i = 0; i < response.eventId.length; i++) {


                            $("#ack_" + response.eventId[i]).html('<a href="javascript:void()" class="acknowlegementDetails" value="' + response.eventId[i] + '">Manual Acknowledged</a>');
                        }
                        swal("", response.message, "success");
                    } else if (response.status == 'false') {
                        $(".errormsg").html(response.msg);
                    }
                }
            });
            //return false;
        }
    });

    $(document).on('click', '#btnsurpress', function (e) {
        e.preventDefault();
        console.log($('#datetimeSupress').val());
        $('#msg_txtsurpress').html('');
        $('#msg_datetimeSupress').html('');
        var datetimeSupress = new Date($('#datetimeSupress').val().replace(/-/g, "/"));
        var currentDate = new Date();
        if ($.trim($('#datetimeSupress').val()) == "Y-m-d H:i") {
            $('#msg_datetimeSupress').html('Please enter surpress datetime !!').css("color", "red");
            return false;
        } else if (datetimeSupress < currentDate) {
            $('#msg_datetimeSupress').html("Supress Date Time Should be greater than Current Date Time").css("color", "red");
            return false;
        } else {
            data = $('#frm_Supress').serialize();
            $.ajax({
                type: 'POST',
                url: baseURL + 'event/ajax_surpressAcknowledge',
                dataType: 'json',
                data: data,
                async: false,
                success: function (response) {
                    if (response.status == 'true') {
                        $('#myModal').modal('hide');
                        $('#actionmodal').modal('hide');
                        $('.event_' + response.eventId).remove();
                        swal("", response.message, "success");
                    } else if (response.status == 'false') {
                        $(".errormsg").html(response.msg);
                    }
                }
            });
            //return false;
        }
    });
    
    
   


    $(document).on('click', '#btnsurpresspermanent', function (e) {
        e.preventDefault();
        data = $('#frm_Supress_permanent').serialize();
        $.ajax({
            type: 'POST',
            url: baseURL + 'event/ajax_surpressPermanent',
            dataType: 'json',
            data: data,
            async: false,
            success: function (response) {
                if (response.status == 'true') {
                    $('#myModal').modal('hide');
                    $('#actionmodal').modal('hide');
                    $('.event_' + response.eventId).remove();
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
        

    });



    
    
});



