$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $(document).on('click', '.client_status', function () {
        var status = $(this).attr('status');
//console.log(status);        
        var id = $(this).attr('myval');
        if (status == 'Y') {
            $.ajax({
                type: 'POST',
                url: baseURL + 'client/ajax_changeStatus',
                dataType: 'json',
                data: {'status': status, 'id': id},
                success: function (response) {
                    if (response.status == 'true') {
                        swal("", response.message, "success");
                        $('.status_active_' + id).html(response.html);
                        $('.hideClientDelete_' + id).remove();
                    } else if (response.status == 'infoUP') {
                        swal("", response.message, "info");
                    } else if (response.status == 'infoDOWN') {
                        swal("", response.message, "info");
                    } else if (response.status == 'false') {
                        $(".errormsg").html(response.msg);
                    }
                }
            });
        } else if (status == 'N') {
            swal({
                title: "Do you agree to the Terms and Conditions (Deactivate)?",
                text: "a. All the child clients inside the client should be inactive/disabled.<br><br> b.  If active child clients are present, you can move this child clients to other clients before disabling this parent client or make them inactive/disable.<br><br> c.After 90 days, the inactive client can be manually deleted.<br><br>d.If the client is inactive for 180 days it will be deleted permanently. Alert mails for same will be sent to administrative email group before 30 days of automatic deleting daily.<br><br>e.If client is not made active/enabled, the client will be deleted permanently and all it’s records will also be deleted.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, I agree",
                cancelButtonText: "No, I disagree",
                showCancelButton: true,
                closeOnConfirm: false,
                html: true
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: 'POST',
                                url: baseURL + 'client/ajax_changeStatus',
                                dataType: 'json',
                                data: {'status': status, 'id': id},
                                success: function (response) {
                                    if (response.status == 'true') {
                                        swal("", response.message, "success");
                                        $('.status_active_' + id).html(response.html);
                                    } else if (response.status == 'infoUP') {
                                        swal("", response.message, "info");
                                    } else if (response.status == 'infoDOWN') {
                                        swal("", response.message, "info");
                                    } else if (response.status == 'false') {
                                        $(".errormsg").html(response.msg);
                                    }
                                }
                            });
                        }
                    });
        }
    });



    $(document).on('click', '.isdelete', function () {
        clientId = $(this).attr('clientId');
        console.log(clientId);
        swal({
            title: "Do you agree to the Terms and Conditions (Delete)?",
            text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = baseURL + 'client/delete/' + clientId;
                    }
                });

    });
	
	$(document).on('change', '.servicetemp, .services', function (e) {
		var tagnam = $(this).prop("tagName");
		
		if(tagnam === 'SELECT'){
		
			if ($(this).val()) {
				$(this).css({
				"border": "",
				"background": ""
				});		

			} else {
				$(this).css({
				"border": "1px solid red",
				"background": "#f8efef"
				});					
			}
		}else if(tagnam === 'INPUT'){
			if ($(this).val()) {
				$('#serviceslisterror').text(""); 		

			} else {
				$('#serviceslisterror').text("Please select atleast one service"); 				
			}					
		}
	});	
});
$('#client_title').on("keyup", function(e){ 

	var field = "client_title";
	var parent_client_id = $("#client_title").attr("partner");
	var table =  "client";
	var value = $(this).val();
	var baseURL = $('#baseURL').val();
	$.ajax({
			type: 'POST',
			url: baseURL + 'client/ajax_checkUniqueClient',
			dataType: 'json',
			data: {table: table, field: field, parent_client_id: parent_client_id,value: value},
			success: function (data) {
				if (data.status == 'true') {
					//$('#errmsg').html(value + ' already exists!').show();
	//                    bootbox.alert(value + ' already exists!', function () {
	//                    });
					swal("Oops!!!", value + " already exists!", "warning");
					//alert(value+' client already exists');
					//$('#client_title').val('');
					//$('#client_display_name').val('');					
					$('#client_title').focus();
					$("#savebutton").attr("type","button");

				} else {
					$("#savebutton").attr("type","submit");

				}
			}
	});
	
});