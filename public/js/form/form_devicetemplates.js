$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'nagiosmanagements/ajaxChangeDeviceTemplateStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
	
	      $('#name').on('change', function () {
        //$('#errmsg').html('').hide();
        var table = $(this).attr('tableName');
        var field = $(this).attr('tableField');
        var value = this.value;
		var gateway_id = $(this).attr('gateway_id');
        //var old_email = $('#old_email').val();
        var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + '/nagiosmanagements/ajax_checkUniqueDeviceTemName',
            dataType: 'json',
            data: {table: table, field: field, value: value,gateway_id:gateway_id},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                    $('#' + field).val('');
                    $('#alias').val('');
                    $('#' + field).focus();
                } else {

                }
            }
        });
    });
});
jQuery.validator.addMethod("twointergerval", function(value, element) {
// allow any non-whitespace characters as the host part
return this.optional( element ) || /0|1$/.test( value );
}, 'Please enter a proper letters.');
 $("form[name='frmDeviceTemplate']").validate({
        // Specify validation rules
        rules: {
			name: "required",
			//event_handler_enabled: "required",
			host_check_period : "required",
			host_check_interval: {
				required: true,
				number: true
			},	
			host_retry_interval: {
				required: true,
				number: true
			},
			host_max_check_attempts: {
				required: true,
				number: true
			},	
			host_check_command : "required",
			host_notification_period : "required",			
			host_notification_interval: {
				required: true,
				number: true
			},
			host_notification_options : "required",
			
			

			host_passive_checks_enabled: {
				required: true,
				twointergerval: true
			},
			host_process_perf_data : "required",
			host_active_checks_enabled: {
				required: true,
				twointergerval: true
			},
			host_retain_nonstatus_information : "required"				
			
	
			//event_handler_enabled: "required",			
        },
        // Specify validation error messages
        messages: {
			name: "Please enter device template name",
			host_check_period : "Please enter check period",
			host_check_interval: {
				required:  "Please enter check interval",
				number:  "Please enter numeric value"
			},
			host_retry_interval: {
				required:  "Please enter retry interval",
				number:  "Please enter numeric value"
			},
			host_max_check_attempts: {
				required:  "Please enter max check attempts",
				number:  "Please enter numeric value"
			},	
			host_check_command : "Please enter check command",			
			host_notification_period : "Please enter notification period",
			host_notification_interval: {
				required: "Please enter notification interval",
				number: "Please enter numeric value"
			},
			host_notification_options : "Please enter notification options",
			
			
			
			host_passive_checks_enabled: {
				required: "Please enter passive checks enabled" ,
				twointergerval: "Please enter 0 or 1"
			},
			host_process_perf_data : "Please enter process perf data",
			host_active_checks_enabled: {
				required: "Please enter active checks enabled" ,
				twointergerval: "Please enter 0 or 1"
			},
			host_retain_nonstatus_information : "Please enter retain nonstatus information"	
			
		
			//event_handler_enabled: "Please enter 0 or 1(0 for disable and 1 for enable)",
        },
		
  });
  
