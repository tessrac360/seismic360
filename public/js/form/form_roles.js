$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'roles/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });

    $('.isUsedRoleByUser').click(function () {
        swal("", "Oops You can't delete because role is already used somewhere", "warning");
    });

    $('.isUsedRoleByUserstatus').click(function () {
        swal("", "Oops You can't deactivate because role is already used somewhere", "warning");
    });


    $(document).on('click', '.isdelete', function () {
        roleId = $(this).attr('roleId');
        clientId = $(this).attr('clientId');
        // status = $(this).attr('status');
        swal({
            title: "",
            text: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = baseURL + 'roles/delete/' + roleId + '/' + clientId;
                    }
                });

    });


    $.validator.addMethod("serviceRegex", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9&.,\-\s]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");

    $("form[name='frmrole']").validate({
        rules: {
            title: {
                required: true,
                serviceRegex: true,
            },
            client_id: "required",
            level: {
                required: {
                    depends: function (element) {
                        return $("#level").val() == "";
                    }
                }
            },
        },
        messages: {
            title: {
                required: "Role name is Required",
                serviceRegex: "Please enter alphanumerical characters"
            },
            client_id: "Please select client",
            level: "Level is Required"
        },
        submitHandler: function (form) {
            var moduleCount = $('#frmrole input[type="checkbox"]:checked').size();
            if (moduleCount >= 1) {
                form.submit();
            } else {
                swal("Oops!!!", "Please select the permission", "error");
            }
        }
    });



    $('.multicheck').change(function () { //".checkbox" change 
        //uncheck "select all", if one of the listed checkbox item is unchecked
        id = $(this).val();
        status = $('.' + id).is(':checked');
        var arr = id.split('_');
        checkId = arr[1];
        if ($('.multi_' + checkId + ':checked').length > 1) {
            if (status == "false") { //if this item is unchecked
                $(".checkall_" + checkId)[0].checked = false; //change "select all" checked status to false
            }

//            //check "select all" if all checkbox items are checked
            //console.log($('.multi_' + checkId + ':checked').length);
            // console.log($('.multi_' + checkId).length);

            if ($('.multi_' + checkId + ':checked').length == $('.multi_' + checkId).length) {
                $(".checkall_" + checkId)[0].checked = true; //change "select all" checked status to true
            }
        }
    });




    $('.chkpermission').change(function () {

        permissionVal = $(this).val();
        console.log(permissionVal);
        if ($(this).is(':checked')) {

            $('.hid_' + permissionVal).val('1');
        } else {

            $('.hid_' + permissionVal).val('0');
        }
    });
    $(document).on('click', '.allcheck', function () {
        id = $(this).attr('id');
        status = $(this).is(':checked');

        if (status == "true") {

            $('.hid_chkadd_' + id).val('1');
            $('.hid_chkedit_' + id).val('1');
            $('.hid_chkview_' + id).val('1');
            $('.hid_chkactive_' + id).val('1');
            $('.hid_chkdelete_' + id).val('1');
            $('.hid_chkconfig_' + id).val('1');
            $('.chkadd_' + id).prop('checked', true);
            $('.chkedit_' + id).prop('checked', true);
            $('.chkview_' + id).prop('checked', true);
            $('.chkactive_' + id).prop('checked', true);
            $('.chkdelete_' + id).prop('checked', true);
            $('.chkconfig_' + id).prop('checked', true);

        } else {
            $('.hid_chkadd_' + id).val('0');
            $('.hid_chkedit_' + id).val('0');
            $('.hid_chkview_' + id).val('0');
            $('.hid_chkactive_' + id).val('0');
            $('.hid_chkdelete_' + id).val('0');
            $('.hid_chkconfig_' + id).val('0');
            $('.chkadd_' + id).prop('checked', false);
            $('.chkedit_' + id).prop('checked', false);
            $('.chkview_' + id).prop('checked', false);
            $('.chkactive_' + id).prop('checked', false);
            $('.chkdelete_' + id).prop('checked', false);
            $('.chkconfig_' + id).prop('checked', false);
        }
    });
});