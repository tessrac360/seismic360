$(document).on('click', '.isdeleteQueue', function () {
		var baseURL = $('#baseURL').val();
        queueId = $(this).attr('queueId');
        // status = $(this).attr('status');
		//alert(queueId);
         swal({	title: "Are you sure?",
				text: "you want to delete ?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = baseURL + 'event/queue/delete/' + queueId;
				}
			});
});
  				
 $('#queue_name').on('change', function () {
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	var id = "";
	$.ajax({
		type: 'POST',
		url: baseURL + '/event/queue/ajax_checkUniqueQueueName',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			if (data.status == 'true') {
				//$('#errmsg').html(value + ' already exists!').show();
	//                    bootbox.alert(value + ' already exists!', function () {
	//                    });
				swal("Oops!!!", value + " already exists!", "warning");
				$('#' + field).val('');
				$('#alias').val('');
				$('#' + field).focus();
			} else {

			}
		}
	});
});

	$("form[name='frmQueue']").validate({
        // Specify validation rules
        rules: {
			queue_name: "required",
			severity_id: "required",
			skill_id: "required",
			ticket_status_id: "required"
			//event_handler_enabled: "required",
			
        },
        // Specify validation error messages
        messages: {
			queue_name: "Please enter Queue Name",
			severity_id: "Please select severity",
			skill_id: "Please select priority",
			ticket_status_id: "Please select incident status"
			//event_handler_enabled: "Please enter 0 or 1(0 for disable and 1 for enable)",
        },
		
		submitHandler: function (form) {
            $("#group_id_error").text("");
           
            var severity_id = $('#frmQueue input[name="severity_id[]"]:checked').size();
            var skill_id = $('#frmQueue input[name="skill_id[]"]:checked').size();
            var ticket_status_id = $('#frmQueue input[name="ticket_status_id[]"]:checked').size();
            //var notification = $('#frmUser .notification:checked').size();
            console.log(severity_id + '-----' + skill_id + '---------' + ticket_status_id);

            //return false;
            if (severity_id == "0" && skill_id == "0" && ticket_status_id == "0" ) {
                $("#group_id_error").text('Please select atleast one check box from Severities or Priorities or Incident Status').css('color', 'red');
                return false;
            
            } else {
                form.submit();
            }
        }
		
  });

 /*  
 $.validator.addMethod('require-one', function(value) {
    return $('.require-one:checked').size() > 0;
}, 'Please check at least one box.');

var checkboxes = $('.require-one');
var checkbox_names = $.map(checkboxes, function(e, i) {
    return $(e).attr("name")
}).join(" ");

$("#frmQueue").validate({
    groups: {
        checks: checkbox_names
    },
    errorPlacement: function(error, element) {
        if (element.attr("type") == "checkbox") error.insertAfter(checkboxes.last());
        else error.insertAfter(element);
    }
});
 */
