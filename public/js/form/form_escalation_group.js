
$(document).ready(function () {
    var baseURL = $('#baseURL').val();
    $('.onoffactiontoggle').change(function () {
        var status = $(this).prop('checked');
        console.log(status);
        var id = $(this).attr('myval');
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/EscalationGroups/ajax_changeStatus',
            dataType: 'json',
            data: {'status': status, 'id': id},
            success: function (response) {
                if (response.status == 'true') {
                    swal("", response.message, "success");
                } else if (response.status == 'false') {
                    $(".errormsg").html(response.msg);
                }
            }
        });
    });
});

$(document).on('change', '.escalation_group_name', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	var id = "";
	$.ajax({
		type: 'POST',
		url: baseURL + '/client/EscalationGroups/ajax_checkUniqueGroupName',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			if (data.status == 'true') {
				//$('#errmsg').html(value + ' already exists!').show();
	//                    bootbox.alert(value + ' already exists!', function () {
	//                    });
				swal("Oops!!!", value + " already exists!", "warning");
				$('#' + field).val('');
				$('#alias').val('');
				$('#' + field).focus();
			} else {

			}
		}
	});
});

$("form[name='frmEscalationGroup']").validate({
	// Specify validation rules
	rules: {
		escalation_group_name: "required",
	},
	// Specify validation error messages
	messages: {
		escalation_group_name: "Please enter group name",
	},

	
});

$(document).on('click', '.isDelete', function () {
	var baseURL = $('#baseURL').val();
	escalation_group_uuid = $(this).attr('escalation_group_uuid');
	// status = $(this).attr('status');
	swal({
		title: "Are you sure?",
		text: "you want to delete ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		cancelButtonText: "No",
		closeOnConfirm: true,
		closeOnCancel: true
	},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = baseURL + 'client/EscalationGroups/deleteEscalationGroup/' + escalation_group_uuid;
				}
			});

});
