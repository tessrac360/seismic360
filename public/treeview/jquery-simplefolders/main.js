$(document).delegate('.expander','click',function(){
  $(this).toggleClass('expanded')
    .nextAll('ul:first').toggleClass('expanded');
  return true;
});

$(document).ready(function () {
$('.expander').toggleClass('expanded')
   .nextAll('ul').toggleClass('expanded');
});