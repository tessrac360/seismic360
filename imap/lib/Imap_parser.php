<?php

/* * *******************************************
 * #### Imap_parser.php v0.0.1 ####
 * IMAP mailbox parser using PHP.
 * Return Array or JSON.
 * Coded by @bachors 2016.
 * https://github.com/bachors/Imap_parser.php
 * Updates will be posted to this site.

  - data:
  - email:
  - hostname
  - username
  - password
  - pagination:
  - sort
  - limit
  - offset

  - result:
  - status
  - email
  - count
  - inbox:
  - id
  - subject
  - from
  - email
  - date
  - message
  - image
  - pagination
  - sort
  - limit
  - offset
  - back
  - next

 * ******************************************* */

class Imap_parser {

    function inboxVistara($data) {

        $result = array();

        $imap = imap_open($data['email']['hostname'], $data['email']['username'], $data['email']['password']) or die('Cannot connect to yourdomain.com: ' . imap_last_error());

        if ($imap) {

            $result['status'] = 'success';
            $result['email'] = $data['email']['username'];

            $read = imap_search($imap, 'UNSEEN');
            //$read = imap_search($imap, 'ALL');

            if (!empty($read)) {
                if ($data['pagination']['sort'] == 'DESC') {
                    sort($read);
                }

//                echo '<pre>';
//                print_r($read);
//                echo '</pre>';

                $num = count($read);
                $result['count'] = $num;
                $stop = $data['pagination']['limit'] + $data['pagination']['offset'];

                if ($stop > $num) {
                    $stop = $num;
                }

                for ($i = $data['pagination']['offset']; $i < $stop; $i++) {
                    $message = "";
                    $overview = imap_fetch_overview($imap, $read[$i], 0);
                    // $message = imap_body($imap, $read[$i], 0); 
                    $structure = imap_fetchstructure($imap, $read[$i]);
//                    //$message = imap_fetchbody($imap, $read[$i], 1);
                    if (isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
                        $part = $structure->parts[0];
                        $messageBefore = imap_fetchbody($imap, $read[$i], 1); // convert UNSEEN to SEEN
                        if ($part->encoding == 4) {
                            $message = imap_qprint($messageBefore);
                        } else {
                            $message = $messageBefore;
                        }
                    }

                    /* get mesage body */
                    //$message =   imap_fetchbody($imap, $read[$i], 1);;                     
                    $aDataTableHeaderHTML = array();
                    $pos = strpos($message, '</table>');
                    $outputcontent = substr($message, $pos);
                    $htmlContent = preg_replace('/td/', 'th', $outputcontent);
                    $DOM = new DOMDocument();
                    libxml_use_internal_errors(true);
                    $DOM->loadHTML($htmlContent);
                    $Header = $DOM->getElementsByTagName('th');

                    //#Get header name of the table
                    foreach ($Header as $NodeHeader) {
                        $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
                    }


                    $final = array();
                    $j = 0;
                    for ($j = 0; $j < sizeof($aDataTableHeaderHTML); $j += 2) {
                        $final[$aDataTableHeaderHTML[$j]] = $aDataTableHeaderHTML[$j + 1];
                    }

                    $result['inbox'][] = array(
                        'eid' => $read[$i],
                        'subject' => strip_tags($overview[0]->subject),
                        'date' => $overview[0]->date,
                        'message' => $final,
                            //'structure' => $structure->parts[0],
                    );

                    $result['pagination'] = array(
                        'sort' => $data['pagination']['sort'],
                        'limit' => $data['pagination']['limit'],
                        'offset' => array(
                            'back' => ($data['pagination']['offset'] == 0 ? null : $data['pagination']['offset'] - $data['pagination']['limit']),
                            'next' => ($data['pagination']['offset'] < $num ? $data['pagination']['offset'] + $data['pagination']['limit'] : null)
                        )
                    );
                }
                $result['status'] = 'true';
                imap_close($imap);
            } else {
                $result['status'] = 'false';
            }
        } else {
            $result['status'] = 'false';
        }

        return $result;
    }

    public function convertDatetimeVistara($datetime) {     
        $words = explode(' ', $datetime);       
        if (count($words) >= 2) {
            $words = array_slice($words, 0, count($words) - 1);
        }
        $dateWithTimeZone = implode(' ', $words);
        $time = strtotime($dateWithTimeZone);
        
        return date("Y-m-d H:i:s", $time);
    }

    public function convertDatetimeNectar($datetime) {
        $words = explode(' ', $datetime);
        if (count($words) >= 2) {
            $words = array_slice($words, 0, count($words) - 2);
        }
        $dateWithTimeZone = implode(' ', $words);
        $time = strtotime($dateWithTimeZone);
        return date("Y-m-d H:i:s", $time);
    }

    function inboxNectar($data) {


        $result = array();

        $imap = imap_open($data['email']['hostname'], $data['email']['username'], $data['email']['password']) or die('Cannot connect to yourdomain.com: ' . imap_last_error());

        if ($imap) {

            $result['status'] = 'success';
            $result['email'] = $data['email']['username'];

            $read = imap_search($imap, 'UNSEEN');
            //$read = imap_search($imap, 'ALL');

            if (!empty($read)) {
                if ($data['pagination']['sort'] == 'DESC') {
                    sort($read);
                }

                $num = count($read);

                $result['count'] = $num;

                $stop = $data['pagination']['limit'] + $data['pagination']['offset'];

                if ($stop > $num) {
                    $stop = $num;
                }

                for ($i = $data['pagination']['offset']; $i < $stop; $i++) {
                    $message = "";
                    //print_r($read[$i]);
                    $overview = imap_fetch_overview($imap, $read[$i], 0);
                    $structure = imap_fetchstructure($imap, $read[$i]);




                    if (isset($structure->parts) && is_array($structure->parts)) {
                        $part = $structure->parts[0];
                        $messageBefore = imap_fetchbody($imap, $read[$i], 1); // convert UNSEEN to SEEN
                        if ($part->encoding == 4) {
                            $message = imap_qprint($messageBefore);
                        } else {
                            $message = $messageBefore;
                        }
                    }
                    

                    $header = imap_headerinfo($imap, $read[$i], 0);
                    
                    /* get mesage body */

                    //$message = imap_body($imap, $read[$i],2);
                    $mail = $header->from[0]->mailbox . '@' . $header->from[0]->host;



                    $aDataTableDetailHTML = array();
                    $aDataTableHeaderHTML = array();
                    $htmlContent = preg_replace('/td/', 'th', $message, 16);
                    $DOM = new DOMDocument();
                    libxml_use_internal_errors(true);
                    $DOM->loadHTML($htmlContent);
                    $Header = $DOM->getElementsByTagName('th');
                    $Detail = $DOM->getElementsByTagName('td');
                    //#Get header name of the table
                    foreach ($Header as $NodeHeader) {
                        $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
                    }

                    #Get row data/detail table without header name as key
                    $k = 0;
                    $j = 0;




                    foreach ($Detail as $sNodeDetail) {
                        $aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
                        $k = $k + 1;
                        $j = $k % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
                    }

                    krsort($aDataTableDetailHTML);

                    if (strpos($overview[0]->subject, '(') !== false) {
                        $subj1 = strstr($overview[0]->subject, '(', true);
                    } else {
                        $subj1 = $overview[0]->subject;
                    }
                    $subj1 = strstr($overview[0]->subject, '(', true);
                    $subj2 = str_replace("Server", "", $subj1);
                    $subjectfinal = str_replace("Condition:", "", $subj2);

                   
                    preg_match("/(.*?)\((.*?)$/i", $overview[0]->subject, $output_array);
                    $exp_clientCode = explode(' ', $output_array[2]);
                    $clientCode = str_replace(")", "", $exp_clientCode[0]);




                    $result['inbox'][] = array(
                        'id' => $read[$i],
                        'client_name' => trim($subjectfinal),
                        'email' => $mail,
                        ///'subject' => $overview[0]->subject,
                        'client_code' => $clientCode,
                         'message' => $aDataTableDetailHTML,
//                        'str' => $structure->parts
                    );



                    $result['pagination'] = array(
                        'sort' => $data['pagination']['sort'],
                        'limit' => $data['pagination']['limit'],
                        'offset' => array(
                            'back' => ($data['pagination']['offset'] == 0 ? null : $data['pagination']['offset'] - $data['pagination']['limit']),
                            'next' => ($data['pagination']['offset'] < $num ? $data['pagination']['offset'] + $data['pagination']['limit'] : null)
                        )
                    );
                }

                $result['status'] = 'true';
                imap_close($imap);
            } else {
                $result['status'] = 'false';
            }
        } else {
            $result['status'] = 'false';
        }

        return $result;
    }
    
    function inboxVistaraDemo($data) {

        
        $result = array();

        $imap = imap_open($data['email']['hostname'], $data['email']['username'], $data['email']['password']) or die('Cannot connect to yourdomain.com: ' . imap_last_error());

        if ($imap) {

            $result['status'] = 'success';
            $result['email'] = $data['email']['username'];

            $read = imap_search($imap, 'UNSEEN');
            //$read = imap_search($imap, 'ALL');
           
            if (!empty($read)) {
                if ($data['pagination']['sort'] == 'DESC') {
                    sort($read);
                }

                $num = count($read);

                $result['count'] = $num;

                $stop = $data['pagination']['limit'] + $data['pagination']['offset'];

                if ($stop > $num) {
                    $stop = $num;
                }

                for ($i = $data['pagination']['offset']; $i < $stop; $i++) {
                    $message = "";
                    //print_r($read[$i]);
                    $overview = imap_fetch_overview($imap, $read[$i], 0);
                    $structure = imap_fetchstructure($imap, $read[$i]);
                    
                    
                    if (isset($structure->parts) && is_array($structure->parts)) {
                        $part = $structure->parts[0];
                        $messageBefore = imap_fetchbody($imap, $read[$i], 1); // convert UNSEEN to SEEN
                        if ($part->encoding == 4) {
                            $message = imap_qprint($messageBefore);
                        } else {
                            $message = $messageBefore;
                        }
                    }else{
                        $messageBefore = imap_fetchbody($imap, $read[$i], 1);
                        $message = imap_qprint($messageBefore);
                        //$message = imap_fetchbody($imap, $read[$i], 1);
                    }
                    //echo $message;exit;

                    $header = imap_headerinfo($imap, $read[$i], 0);

                    /* get mesage body */

                    //$message = imap_body($imap, $read[$i],2);
                    $mail = $header->from[0]->mailbox . '@' . $header->from[0]->host;



                    $aDataTableDetailHTML = array();
                    $aDataTableHeaderHTML = array();
                    $htmlCon = preg_match_all('/\<div(.*?)\<\/div\>/ims', $message,$arraylist);                    
                    //print_r($arraylist[0][1]) ;exit;
                    $htmlCon1 = preg_match_all('/\<table(.*?)\<\/table\>/ims', $arraylist[0][1],$arraylist2);
                    //print_r($arraylist2[0][1]) ;exit;
                    $message= $arraylist2[0][1];
                    $htmlContent = preg_replace('/td/', 'td', $message, 16);                       
                    $DOM = new DOMDocument();
                    libxml_use_internal_errors(true);
                    $DOM->loadHTML($htmlContent);
                    
                    $Header = $DOM->getElementsByTagName('th');
                    $Detail = $DOM->getElementsByTagName('td');
                    //#Get header name of the table
                    foreach ($Header as $NodeHeader) {                        
                        $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
                    }
                  
                    #Get row data/detail table without header name as key
                    $k = 0;
                    $j = 0;
                    
                    foreach ($Detail as $sNodeDetail) {
                        //print_r($sNodeDetail->textContent);
                        $aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
                        $k = $k + 1;
                        $j = $k % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
                        //$j =  $j + 1;
                    }
                   
                    krsort($aDataTableDetailHTML);
                    

                    $result['inbox'][] = array(
                        'id' => $read[$i],  
                        'subject' => strip_tags($overview[0]->subject),
                        'email' => $mail,
                        'message' => $aDataTableDetailHTML,
//                        'str' => $structure->parts
                    );



                    $result['pagination'] = array(
                        'sort' => $data['pagination']['sort'],
                        'limit' => $data['pagination']['limit'],
                        'offset' => array(
                            'back' => ($data['pagination']['offset'] == 0 ? null : $data['pagination']['offset'] - $data['pagination']['limit']),
                            'next' => ($data['pagination']['offset'] < $num ? $data['pagination']['offset'] + $data['pagination']['limit'] : null)
                        )
                    );
                }

                $result['status'] = 'true';
                imap_close($imap);
            } else {
                $result['status'] = 'false';
            }
        } else {
            $result['status'] = 'false';
        }
        //print_r($result);exit;

        return $result;
    }

}

?>
