<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function deleteTempMenu() {

        $menuid = $this->input->post('menuid');
        $this->load->model('Menus');
        $status = $this->Menus->deleteMenu($menuid);
        echo json_encode($status);
        exit;
    }

}
