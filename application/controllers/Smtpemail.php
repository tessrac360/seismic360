<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Smtpemail extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->load->library('email');

        $this->email->from('kketan2000@gmail.com', 'Your Name');
        $this->email->to('kkc1909@gmail.com');
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');

        $this->email->send();



        exit;
    }

    public function smpt_setup() {
        //$this->load->library('email');
//SMTP & mail configuration
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'smtp.eventedge@gmail.com',
            'smtp_pass' => 'nspire@123',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );

        /* $config = array(
          'protocol' => 'smtp',
          'smtp_host' => 'tls://10.10.32.52',
          'smtp_port' => 587,
          //'smtp_port' => 993,
          'smtp_user' => 'admin@seismic360.com',
          'smtp_pass' => '123456',
          'mailtype' => 'html',
          'charset' => 'utf-8'
          ); */

        /* $config = array(
          'protocol' => 'smtp',
          //'smtp_host' => 'tls://10.10.32.52',
          'smtp_host' => '10.10.32.52',
          'smtp_port' => 587,
          //'smtp_port' => 993,
          'smtp_user' => 'admin@seismic360.com',
          'smtp_pass' => '123456',
          'mailtype' => 'html',
          'charset' => 'utf-8'
          ); */

        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

//Email content
        $htmlContent = '<h1>Sending email via SMTP server</h1>';
        $htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';

        $this->email->to('kkc1909@gmail.com');
        //$this->email->bcc('veeru89.k@gmail.com,venkataprasadbuddala@gmail.com');
        //$this->email->from('kketan2000@gmail.com', 'MyWebsite');
        $this->email->subject('How to send email via SMTP server in CodeIgniter');
        $this->email->message($htmlContent);

//Send email
        $this->email->send();
        echo "hiiii";exit;
    }

    function sendmail() {
        //$this->load->library('email'); // load email library
        $htmlContent = '<h1>Sending email via SMTP server</h1>';
        $htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';
        $this->email->to('kkc1909@gmail.com');
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('veeru89.k@gmail.com,venkataprasadbuddala@gmail.com');
        $this->email->from('kketan2000@gmail.com', 'MyWebsite');
        $this->email->subject('How to send email via SMTP server in CodeIgniter');
        $this->email->message($htmlContent);
        if ($this->email->send())
            echo "Mail Sent!";
        else
            echo "There is error in sending mail!";
    }

    public function tempDesign() {
        // echo "hiiii";exit;
        $emilListing = helperEmailContent('EMAIL_FORGOT_PASSWORD');
        // pr($emilListing);exit;
        if (!empty($emilListing)) {
            $subject = $emilListing->subject;
            $url = base_url();
            $link = "<a href='$url'>Click here</a>";
            echo $message = email_forgetPassword($emilListing->content, $link);
            $fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');
            $toEmail = 'kkc1909@gmail.com';
            exit;
            // $isSend = $isSend = helperSendMail('kkc1909@gmail.com', $fromEmailID, $subject, $message, SOCIETY_NAME_EMAIL_SLUG);
            // $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, 'seismic360');
        }
    }

    public function createMacd() {
        // echo "hiiii";exit;
        $emilListing = helperEmailContent('EMAILID_MACD_SUBMITTED');
        if (!empty($emilListing)) {
            $this->db->select('mac.macd_request_id as macd_id,mac.firstname_from as requester_name,mac.request_type,ass_usr.first_name as name,ass_usr.user_name'); //ass_usr.first_name
            $this->db->from('macd mac');
            $this->db->join('users as urs', 'urs.id = mac.created_by');
            $this->db->join('client as cl', 'cl.id = mac.client_id');
            $this->db->join('users as ass_usr', 'ass_usr.id = urs.reporting_manager_id', 'LEFT');
            $this->db->where('cl.is_sentmail', 'Y');
            $this->db->where('mac.macd_id', '174');
            $query = $this->db->get();
//            echo $this->db->last_query();
//            exit;
            if ($query->num_rows() > 0) {
                $data = $query->row();
                pr($data);
                echo $subject = tempSubject($emilListing->subject, $data);

                echo $message = tempContent($emilListing->content, $data);
                $fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');
                $toEmail = $data->user_name;
                $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, 'seismic360');
                exit;
            }
        }
    }

    public function macdStatusChange() {
//        echo helpler_getCompanyId();
//        pr($_SESSION);exit;
        $emilListing = helperEmailContent('EMAIL_MACD_RESPONSE_EMAIL_FROM_APPROVED');
        if (!empty($emilListing)) {
            $this->db->select('mac.firstname_from as name,mac.macd_request_id as macd_id,macsts.status_name as status,ass_usr.first_name as requester_name,ass_usr.user_name as reply_mail,urs.user_name'); //ass_usr.first_name
            $this->db->from('macd mac');
            $this->db->join('macd_queue as madque', 'madque.request_id = mac.macd_id');
            $this->db->join('macd_status as macsts', 'macsts.status_id = madque.macd_status');
            $this->db->join('users as urs', 'urs.id = mac.created_by');
            $this->db->join('client as cl', 'cl.id = mac.client_id');
            $this->db->join('users as ass_usr', 'ass_usr.id = urs.reporting_manager_id', 'LEFT');
            $this->db->where('cl.is_sentmail', 'Y');
            $this->db->where('mac.macd_id', '176');
            $query = $this->db->get();
            echo $this->db->last_query();
//            exit;
            if ($query->num_rows() > 0) {
                $data = $query->row();
                $subject = tempSubject($emilListing->subject, $data);
                $message = tempContent($emilListing->content, $data);
                //$fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');
                $fromEmailID = $data->reply_mail;
                $toEmail = $data->user_name;
                $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, 'seismic360');
            }
        }
    }

    public function ssss() {
        $username = 'inspiredge';
        $password = 'inspiredge@123';

        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Basic " . base64_encode("$username:$password")
            )
        ));
        $url = "http://10.10.32.52/eventedge2/webapi/createMacd/macd_id/176";
        $data = file_get_contents($url, false, $context);
        $array = json_decode($data, true);
        pr($array);
        if ($array['status'] == 'true') {
            
        }
    }

}
