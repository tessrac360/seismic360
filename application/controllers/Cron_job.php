<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_job extends MX_Controller {

    public function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        $current_date = $this->config->item('datetime');
        $this->current_date = $this->config->item('datetime');
        $this->load->model('event/Auto_assign_model', 'auto_assign');
        $this->load->model('event/Model_ticket');
        $this->load->model('event/Model_event');
    }

    /*
      |--------------------------------------------------------------------------
      | Auto Assign (Author: SIDDHARTHA)
      |--------------------------------------------------------------------------
      |	This Controller for Auto assigning incidents to Engineer(based on engineer skills) using Round Robin method.
      | 	First Get All Online Engineer's where is_login = 1 in user table.
      |	Get All Incidents which is not assigned to any Engineer where assigned_to = 0 in tickets table.
      |	Here $defult_set = 4; which means assign incidents limit, So we can assign 4 incidents only per Engineer.
      |	Now, we have to check the user skills for assigning incident. if Engineer match that skill level match then
      |	only assign theIncident.
      |	As well as assgin Incidents if Engineer's same Client only. Related that Engineer's Only.
      |	Here if incident not closed means,engineer still working on incident.
      |   So we consider incident will not add until he not match Defult_set
      |	user table skill = priorty in ticket table
     */

    public function auto_assign_incidents() {
        $defult_set = 4;
        $users = $this->auto_assign->get_login_users(); // Get online users
        $incidents = $this->auto_assign->getAllIncidents(); // Get un-assigned incidents
        //pr($incidents); exit; // check current unassign incidents
        $online_users = array();
        $online_users_details = array();
        if (count($users) > 0) {
            foreach ($users as $k => $user) {
                $online_users[] = $user['id'];
                $online_users_details[$user['id']]['user_id'] = $user['id'];
                $online_users_details[$user['id']]['username'] = $user['username'];
                $online_users_details[$user['id']]['incidents_count'] = $user['incidents_count'];
                $online_users_details[$user['id']]['subclient_ids'] = $user['subclient_ids'];
                $online_users_details[$user['id']]['skill'] = $user['skill'];
            }
        }
        //pr($online_users_details); exit; // check user details

        if ((count($incidents['resultSet']) > 0) && (count($online_users) > 0)) {
            foreach ($incidents['resultSet'] as $incident) {
                $user = current($online_users); // it will tell Q method to assign incident. ex: A,B,C 	
                next($online_users);




                if ($online_users_details[$user]['incidents_count'] < $defult_set) { // its not allow if user working greater than $defult_set
                    $skills = $online_users_details[$user]['skill'];
                    $s = explode(',', $skills);
                    if (in_array($incident['priority'], $s)) {// Check engineer skill to assign incident. if engineer not match incident will not assign 
                        $clients = $online_users_details[$user]['subclient_ids'];
                        $c = explode(',', $clients);
                        if (in_array($incident['client_id'], $c)) {// Check engineer client to assign incident. if engineer not match client will not assign 
                            if ($this->chkEngineerCurrentWrkInc($user, $defult_set)) {// check presnt current working incidents count
                                //auto assign update
                                $updateTicket = array("assigned_to" => $user, 'status_code' => 1);
                                $this->auto_assign->updateTicketDetails($updateTicket, $incident['ticket_id']);
                                // update last end time
                                $lastTicketActivityID = $this->Model_ticket->getLastTicketActivityID($incident['ticket_id']);
                                $lastActiveUpdate = array('end_time' => date('Y-m-d H:i:s'));
                                $updateLastActiveTicket = $this->Model_ticket->updateLastActiveTicket($lastActiveUpdate, array('activity_id' => $lastTicketActivityID['activity_id']));
                                // add activity
                                $activityData = array('start_time' => date('Y-m-d H:i:s'),
                                    'notes' => "System Auto Assigned Incident",
                                    'state_status' => 1,
                                    'ticket_id' => $incident['ticket_id'],
                                    'user_id' => 1);

                                $insertTicketActivity = $this->Model_ticket->insertTicketActivity($activityData);
                            }
                        }
                    }

                    $assign_to_users[][$user] = $incident['ticket_id'];
                }


                $c = current($online_users);
                if (!$c) {
                    reset($online_users); // it will reset form user array to start  ex: A,B,C,A,B,C 	
                }
            }
        }
    }

    function chkEngineerCurrentWrkInc($userId = NULL, $defult_set) {
        if ($userId != NULL) {
            $user = $this->auto_assign->getLoginCurrentUsersCount($userId);
            if ($user['current_work_incidents'] < $defult_set) {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }

    public function cronSuppressedActive() {
        $this->db->select('event_id,(SELECT event_suppress_endtime FROM `eventedge_event_acknowledgements` WHERE event_id=eventedge_events.event_id ORDER by ack_id DESC LIMIT 1) as suppress_end_time');
        $this->db->from('events');
        $this->db->where('is_suppressed', 'Y');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                if ($value->suppress_end_time != '0000-00-00 00:00:00') {
                    if ($value->suppress_end_time <= $this->current_date) {
                        $this->db->where('event_id', $value->event_id);
                        $this->db->update('events', array('is_suppressed' => 'N'));
                    }
                }
            }
        }
        exit;
    }

}
