<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class webapi extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    function client_get() {
        $clientId = $this->get('id');
        if ((int) $clientId) {
            $this->db->select('private_key');
            $this->db->from('client');
            $this->db->where('id', $clientId);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $return['status'] = 'true';
                $return['resultSet'] = $query->row();
            } else {
                $return['status'] = 'false';
                $return['message'] = "Something went wrong";
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        }

        $this->response($return, 200);
    }

    function masterkey_get() {
        $this->db->select('masterkey');
        $this->db->from('masterkey');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return = $query->row();
        } else {
            $return = array();
        }
        $this->response($return);
    }

    function getIncidents_get() {
        $clientId = $this->get('client_id');
        $severity_id = $this->get('severity_id');
		$timestamp = date('Y-m-d H:i', $this->get('date'));
		//echo $timestamp."<br>";
		$macd_action = $this->get('macd_action');		
        if ($clientId != "" && $severity_id != "" && $timestamp != "") {
            $this->db->select('shift_start_time,shift_end_time,response_sla,mtrs_sla,mttr_sla,working_day,hours,days');
            $this->db->from('incident_sla');
            $this->db->where('client_id', $clientId);
            $this->db->where('priority_id', $severity_id);
			if($macd_action !=""){
				$this->db->where('request_type', $macd_action);
			}
            $query = $this->db->get();
            //echo $this->db->last_query();exit;
            if ($query->num_rows() > 0) {
                $newArray = array();
                $data = $query->row();
                $eventDate = strtotime(date('Y-m-d', strtotime($timestamp)));
                $eventTime = HRToSec(date('H:i', strtotime($timestamp)));
                $shift_start_time = HRToSec(date('H:i', strtotime($data->shift_start_time)));
				//echo $data->shift_start_time."<br>";
				//echo $data->shift_end_time;//exit;
                $shift_end_time = HRToSec(date('H:i', strtotime($data->shift_end_time)));
                $weekdays = explode(",", $data->working_day);
                $holidays = array();
                $sla_time = $data->response_sla;
                $mtrs_time = $data->mtrs_sla;
                $mttr_sla = $data->mttr_sla;
                $sla = $this->calculateSLADueDate($eventDate, $eventTime, $weekdays, $shift_start_time, $shift_end_time, $sla_time, $holidays);
                $datetime['slaDueTime'] = date('Y-m-d H:i', $sla);
                $mtrs = $this->calculateSLADueDate($eventDate, $eventTime, $weekdays, $shift_start_time, $shift_end_time, $mtrs_time, $holidays);
                $datetime['mtrsDueTime'] = date('Y-m-d H:i', $mtrs);
                $mttr = $this->calculateSLADueDate($eventDate, $eventTime, $weekdays, $shift_start_time, $shift_end_time, $mttr_sla, $holidays);
                $datetime['mttrDueTime'] = date('Y-m-d H:i', $mttr);
                $return['status'] = 'true';
                $return['resultSet'] = $datetime;
				//pr($return);exit;
            } else {
                $return['status'] = 'false';
                $return['message'] = "Something went wrong";
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        }
        $this->response($return, 200);
    }

    function getSLAtime_get() {
        $clientId = $this->get('client_id');
        $priority_id = $this->get('priority_id');
        $timestamp = date('Y-m-d H:i', $this->get('date'));
        if ($clientId != "" && $priority_id != "" && $timestamp != "") {
            $this->db->select('shift_start_time,shift_end_time,incident_generated_sla,mtrs_sla,mttr_sla,working_day,hours,days');
            $this->db->from('events_sla');
            $this->db->where('client_id', $clientId);
            $this->db->where('priority_id', $priority_id);
            $query = $this->db->get();
            //echo $this->db->last_query();
            if ($query->num_rows() > 0) {
                $newArray = array();
                $data = $query->row();
                $eventDate = strtotime(date('Y-m-d', strtotime($timestamp)));
                $eventTime = HRToSec(date('H:i', strtotime($timestamp)));
                $shift_start_time = HRToSec(date('H:i', strtotime($data->shift_start_time)));
                $shift_end_time = HRToSec(date('H:i', strtotime($data->shift_end_time)));
                $weekdays = explode(",", $data->working_day);
                $holidays = array();
                $sla_time = $data->incident_generated_sla;
                $mtrs_time = $data->mtrs_sla;
                $mttr_sla = $data->mttr_sla;
                $sla = $this->calculateSLADueDate($eventDate, $eventTime, $weekdays, $shift_start_time, $shift_end_time, $sla_time, $holidays);
                $datetime['slaDueTime'] = date('Y-m-d H:i', $sla);
                $mtrs = $this->calculateSLADueDate($eventDate, $eventTime, $weekdays, $shift_start_time, $shift_end_time, $mtrs_time, $holidays);
                $datetime['mtrsDueTime'] = date('Y-m-d H:i', $mtrs);
                $mttr = $this->calculateSLADueDate($eventDate, $eventTime, $weekdays, $shift_start_time, $shift_end_time, $mttr_sla, $holidays);
                $datetime['mttrDueTime'] = date('Y-m-d H:i', $mttr);
                $return['status'] = 'true';
                $return['resultSet'] = $datetime;
            } else {
                $return['status'] = 'false';
                $return['message'] = "Something went wrong";
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        }
        $this->response($return, 200);
    }

    function calculateSLADueDate($eventStartDay, $eventStartTime, $weekdays = array(), $slaStartTime, $slaEndTime, $slaDuration, $holidays = array()) {

        //echo $eventStartDay;exit;
        /*
          The function calculates a due date for an event or ticket by considering business hours
          and excluding non working days and holidays
         */

        $DueDate = $eventStartDay; // Default SLA Due date
        $SLAperiod = $slaDuration;

        // echo "<br/> SLA for the day '" . date('Y-m-d', $DueDate) ."'\t Day of the week : ". date('N', $DueDate); // for debug

        /* checks if an event day is in holiday list or not */
        $isHoliday = false;
        $hDayCount = sizeof($holidays);
        if ($hDayCount > 0) {

            $isHoliday = $this->isHoliday($holidays, $DueDate);
        }

        /* check if an event day falls within working days and not a holiday. */

        if ((in_array(date('N', $DueDate), $weekdays)) && (!($isHoliday))) {


            if (($eventStartTime >= $slaStartTime) && ($eventStartTime <= $slaEndTime)) {

                /* if event time falls with in business hours */
                $duration = $slaEndTime - $eventStartTime;
                if ($SLAperiod <= $duration) {

                    $DueDate += $eventStartTime + $SLAperiod;
                    $SLAperiod = 0;
                } else {

                    $DueDate += 86400 + $slaStartTime;
                    $SLAperiod -= $duration;
                }

                // echo "<br/> SLA1 : " . ($slaDuration - $SLAperiod) ; // for debug
            } else if ($eventStartTime < $slaStartTime) {

                /* if event time is before start of business hours */

                $DueDate += $slaStartTime;
                $duration = $slaEndTime - $slaStartTime;
                if ($SLAperiod <= $duration) {

                    $DueDate += $SLAperiod;
                    $SLAperiod = 0;
                } else {

                    $DueDate += 86400;
                    $SLAperiod -= $duration;
                }

                //echo "<br/> SLA2 :" . ($slaDuration - $SLAperiod ) ; // for debug
            } else if ($eventStartTime > $slaEndTime) {

                /* if event time is after end of business hours */
                $DueDate += 86400 + $slaStartTime;

                //echo "<br/> SLA3 :" . ($slaDuration - $SLAperiod)  ; // for debug
            }
        } else { /* runs if event time not within business hours or it's a holiday */

            $DueDate += 86400 + $slaStartTime;
            //echo "<br/> SLA4 :" . ($slaDuration - $SLAperiod ) ; // for debug
        }


        while ($SLAperiod > 0) {

            /* checks if an due date day is in holiday list or not */
            $isHoliday = false;
            if ($hDayCount > 0) {

                $isHoliday = $this->isHoliday($holidays, $DueDate);
            }

            /*  check if an Due date falls within working days and not a holiday. */
            if ((in_array(date('N', $DueDate), $weekdays)) && (!($isHoliday))) {

                $duration = $slaEndTime - $slaStartTime;
                if ($SLAperiod <= $duration) {

                    $DueDate += $SLAperiod;
                    $SLAperiod = 0;
                } else {

                    $DueDate += 86400;
                    $SLAperiod -= $duration;
                }
                //echo "<br/> SLA6 :" . ($slaDuration - $SLAperiod)  ; // for debug
            } else { /* if Due date not within business days or it's a holiday */

                $DueDate += 86400;
                //echo "<br/> SLA7 :" . ($slaDuration - $SLAperiod)  ; // for debug
            }
        }

        return $DueDate;
    }

    function isHoliday($holidays, $DueDate) {

        /* Checks if a given date is holiday list or not */

        $flag_Holiday = false;
        $tempDay = strtotime(date('Y-m-d', $DueDate));

        if (in_array($tempDay, $holidays)) {

            //echo "<br/> Excluding holiday :" . date('Y-m-d H:i', $tempDay) ; // for debug
            $flag_Holiday = true;
        }
        return $flag_Holiday;
    }

    public function createMacd_get() {
        $macd_id = $this->get('macd_id');
        $primary_client_Id = $this->get('primary_client_Id');
        if ($macd_id) {
            $emilListing = helperEmailContent('EMAILID_MACD_SUBMITTED', $primary_client_Id);
            if (!empty($emilListing)) {
                $name = "(SELECT first_name FROM `eventedge_users` WHERE id = (case when (urs.reporting_manager_id = '0') THEN urs.id ELSE ass_usr.id END)) as name";
                $email = "(SELECT user_name FROM `eventedge_users` WHERE id = (case when (urs.reporting_manager_id = '0') THEN urs.id ELSE ass_usr.id END)) as user_name";
                $this->db->select("mac.macd_request_id as macd_id,mac.firstname_from as requester_name,mac.request_type,$email,$name"); //ass_usr.first_name
                $this->db->from('macd mac');
                $this->db->join('users as urs', 'urs.id = mac.created_by');
                $this->db->join('client as cl', 'cl.id = mac.client_id');
                $this->db->join('users as ass_usr', 'ass_usr.id = urs.reporting_manager_id', 'LEFT');
                $this->db->where('cl.is_sentmail', 'Y');
                $this->db->where('mac.macd_id', $macd_id);
                $query = $this->db->get();
                //echo $this->db->last_query();exit;
                if ($query->num_rows() > 0) {
                    $data = $query->row();
                    $subject = tempSubject($emilListing->subject, $data);
                    $message = tempContent($emilListing->content, $data);
                    $fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');
                    $toEmail = $data->user_name;
                    $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, 'Seismic360');
                    if ($isSend) {
                        $return['status'] = 'true';
                        $return['message'] = 'email is sent successfully';
                    } else {
                        $return['status'] = 'false';
                        $return['message'] = 'Email error';
                    }
                } else {
                    $return['status'] = 'false';
                    $return['message'] = 'Macd ID not found';
                }
            } else {
                $return['status'] = 'false';
                $return['message'] = 'email format not found';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        }
        $this->response($return, 200);
    }

    public function macdStatusChange_get() {
        $macd_id = $this->get('macd_id');
        $primary_client_Id = $this->get('primary_client_Id');
        if ($macd_id) {
            $emilListing = helperEmailContent('EMAIL_MACD_RESPONSE_EMAIL_FROM_APPROVED', $primary_client_Id);
            if (!empty($emilListing)) {
                $this->db->select('mac.firstname_from as name,mac.macd_request_id as macd_id,macsts.status_name as status,ass_usr.first_name as requester_name,ass_usr.user_name as reply_mail,urs.user_name'); //ass_usr.first_name
                $this->db->from('macd mac');
                $this->db->join('macd_queue as madque', 'madque.request_id = mac.macd_id');
                $this->db->join('macd_status as macsts', 'macsts.status_id = madque.macd_status');
                $this->db->join('users as urs', 'urs.id = mac.created_by');
                $this->db->join('client as cl', 'cl.id = mac.client_id');
                $this->db->join('users as ass_usr', 'ass_usr.id = urs.reporting_manager_id', 'LEFT');
                $this->db->where('cl.is_sentmail', 'Y');
                $this->db->where('mac.macd_id', $macd_id);
                $query = $this->db->get();
                echo $this->db->last_query();exit;
                if ($query->num_rows() > 0) {
                    $data = $query->row();
                    $subject = tempSubject($emilListing->subject, $data);
                    $message = tempContent($emilListing->content, $data);
                    //$fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');
                    $fromEmailID = $data->reply_mail;
                    $toEmail = $data->user_name;
                    $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, 'Seismic360');
                    if ($isSend) {
                        $return['status'] = 'true';
                        $return['message'] = 'email is sent successfully';
                    } else {
                        $return['status'] = 'false';
                        $return['message'] = 'Email error';
                    }
                } else {
                    $return['status'] = 'false';
                    $return['message'] = 'Macd ID not found';
                }
            } else {
                $return['status'] = 'false';
                $return['message'] = 'email format not found';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        }
        $this->response($return, 200);
    }

    public function createTicket_get() {
        $macd_id = $this->get('macd_id');
        $primary_client_Id = $this->get('primary_client_Id');
        if ($macd_id) {
            $emilListing = helperEmailContent('EMAIL_TASK_CREATION_EMAIL_BUSINESS_HOURS', $primary_client_Id);
            if (!empty($emilListing)) {
                $this->db->select('tic.incident_id as task_id, mac.firstname_from as name,mac.macd_request_id as macd_id,ass_usr.user_name as reply_mail,urs.user_name'); //ass_usr.first_name
                $this->db->from('macd mac');
                $this->db->join('tickets as tic', 'tic.ticket_id = mac.ticket_id');
                $this->db->join('users as urs', 'urs.id = mac.created_by');
                $this->db->join('client as cl', 'cl.id = mac.client_id');
                $this->db->join('users as ass_usr', 'ass_usr.id = urs.reporting_manager_id', 'LEFT');
                $this->db->where('cl.is_sentmail', 'Y');
                $this->db->where('mac.macd_id', $macd_id);
                $query = $this->db->get();
                //echo $this->db->last_query();exit;
                if ($query->num_rows() > 0) {
                    $data = $query->row();
                    $subject = tempSubject($emilListing->subject, $data);
                    $message = tempContent($emilListing->content, $data);
                    //$fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');
                    $fromEmailID = $data->reply_mail;
                    $toEmail = $data->user_name;
                    $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, 'Seismic360');
                    if ($isSend) {
                        $return['status'] = 'true';
                        $return['message'] = 'email is sent successfully';
                    } else {
                        $return['status'] = 'false';
                        $return['message'] = 'Email error';
                    }
                } else {
                    $return['status'] = 'false';
                    $return['message'] = 'Macd ID not found';
                }
            } else {
                $return['status'] = 'false';
                $return['message'] = 'email format not found';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        }
        $this->response($return, 200);
    }

    public function assignEngineer_get() { //'7'
        $ticket_id = $this->get('ticket_id');
        $primary_client_Id = $this->get('primary_client_Id');
        if ($ticket_id) {
            $emilListing = helperEmailContent('EMAIL_TASK_ASSIGNMENT_EMAIL_TO_ENGINEER', $primary_client_Id);
            if (!empty($emilListing)) {
                $this->db->select('tic.incident_id as task_id,urs.first_name as name,urs.user_name'); //ass_usr.first_name
                $this->db->from('tickets as tic');
                $this->db->join('users as urs', 'urs.id = tic.assigned_to');
                $this->db->join('client as cl', 'cl.id = tic.client_id');
                $this->db->where('cl.is_sentmail', 'Y');
                $this->db->where('tic.ticket_id', $ticket_id);
                $query = $this->db->get();
                // echo $this->db->last_query();exit;
                if ($query->num_rows() > 0) {
                    $data = $query->row();
                    $subject = tempSubject($emilListing->subject, $data);
                    $message = tempContent($emilListing->content, $data);
                    // exit;
                    $fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');

                    $toEmail = $data->user_name;
                    $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, 'Seismic360');
                    if ($isSend) {
                        $return['status'] = 'true';
                        $return['message'] = 'email is sent successfully';
                    } else {
                        $return['status'] = 'false';
                        $return['message'] = 'Email error';
                    }
                } else {
                    $return['status'] = 'false';
                    $return['message'] = 'Macd ID not found';
                }
            } else {
                $return['status'] = 'false';
                $return['message'] = 'email format not found';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        }
        $this->response($return, 200);
    }

    public function updateStatasForTask_get() { //'7'
        $ticket_id = $this->get('ticket_id');
        $primary_client_Id = $this->get('primary_client_Id');
        $ticket_status = $this->get('ticket_status');
        if ($ticket_id) {
            $emilListing = helperEmailContent('EMAIL_TASK_STATUS_CHANGE', $primary_client_Id);
            if (!empty($emilListing)) {
               
                $this->db->select('tic.incident_id as task_id,urs.first_name as name,ticsta.status,urs.user_name,ass_usr.user_name as reply_mail'); //ass_usr.first_name
                $this->db->from('macd mac');
                $this->db->join('tickets as tic', 'tic.ticket_id = mac.ticket_id');
                $this->db->join('ticket_status as ticsta', 'ticsta.status_code = tic.status_code');
                $this->db->join('users as urs', 'urs.id = mac.created_by');
                $this->db->join('client as cl', 'cl.id = mac.client_id');
                $this->db->join('users as ass_usr', 'ass_usr.id = urs.reporting_manager_id', 'LEFT');
                $this->db->where('cl.is_sentmail', 'Y');
                $this->db->where('tic.ticket_id', $ticket_id);
                $this->db->where("FIND_IN_SET('" . $ticket_status . "', cl.ticket_status)");
                $query = $this->db->get();
                //echo $this->db->last_query();exit;
                if ($query->num_rows() > 0) {
                    $data = $query->row();
                    $subject = tempSubject($emilListing->subject, $data);
                    $message = tempContent($emilListing->content, $data);
                    $fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');
                    $toEmail = $data->user_name;
                    $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, 'Seismic360');
                    if ($isSend) {
                        $return['status'] = 'true';
                        $return['message'] = 'email is sent successfully';
                    } else {
                        $return['status'] = 'false';
                        $return['message'] = 'Email error';
                    }
                } else {
                    $return['status'] = 'false';
                    $return['message'] = 'Macd ID not found';
                }
            } else {
                $return['status'] = 'false';
                $return['message'] = 'email format not found';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        }
        $this->response($return, 200);
    }

    public function updateStatasForTaskResolved_get() {
        $ticket_id = $this->get('ticket_id');
        $primary_client_Id = $this->get('primary_client_Id');
        $ticket_status = $this->get('ticket_status');
        if ($ticket_id) {
            $emilListing = helperEmailContent('EMAIL_TASK_STATUS_CHANGE_RESOLVED', $primary_client_Id);
            if (!empty($emilListing)) {
                $this->db->select('tic.incident_id as task_id,urs.first_name as name,ticsta.status,urs.user_name,ass_usr.user_name as reply_mail'); //ass_usr.first_name
                $this->db->from('macd mac');
                $this->db->join('tickets as tic', 'tic.ticket_id = mac.ticket_id');
                $this->db->join('ticket_status as ticsta', 'ticsta.status_code = tic.status_code');
                $this->db->join('users as urs', 'urs.id = mac.created_by');
                $this->db->join('client as cl', 'cl.id = mac.client_id');
                $this->db->join('users as ass_usr', 'ass_usr.id = urs.reporting_manager_id', 'LEFT');
                $this->db->where('cl.is_sentmail', 'Y');
                $this->db->where('tic.ticket_id', $ticket_id);
                $this->db->where("FIND_IN_SET('" . $ticket_status . "', cl.ticket_status)");
                $query = $this->db->get();
                //echo $this->db->last_query();//exit;
                if ($query->num_rows() > 0) {
                    $data = $query->row();
                    // pr($data);exit;
                    $subject = tempSubject($emilListing->subject, $data);
                    $message = tempContent($emilListing->content, $data);
                    $fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');
                    $toEmail = $data->user_name;
                    $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, 'Seismic360');
                    if ($isSend) {
                        $return['status'] = 'true';
                        $return['message'] = 'email is sent successfully';
                    } else {
                        $return['status'] = 'false';
                        $return['message'] = 'Email error';
                    }
                } else {
                    $return['status'] = 'false';
                    $return['message'] = 'Macd ID not found';
                }
            } else {
                $return['status'] = 'false';
                $return['message'] = 'email format not found';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        }
        $this->response($return, 200);
    }

    public function updateStatasForTaskClosed_get() {
        $ticket_id = $this->get('ticket_id');
        $primary_client_Id = $this->get('primary_client_Id');
        $ticket_status = $this->get('ticket_status');
        if ($ticket_id) {
            $emilListing = helperEmailContent('EMAIL_EMAIL_TASK_STATUS_CHANGE_CLOSED', $primary_client_Id);
            if (!empty($emilListing)) {
                $this->db->select('tic.incident_id as task_id,urs.first_name as name,ticsta.status,urs.user_name,ass_usr.user_name as reply_mail'); //ass_usr.first_name
                $this->db->from('macd mac');
                $this->db->join('tickets as tic', 'tic.ticket_id = mac.ticket_id');
                $this->db->join('ticket_status as ticsta', 'ticsta.status_code = tic.status_code');
                $this->db->join('users as urs', 'urs.id = mac.created_by');
                $this->db->join('client as cl', 'cl.id = mac.client_id');
                $this->db->join('users as ass_usr', 'ass_usr.id = urs.reporting_manager_id', 'LEFT');
                $this->db->where('cl.is_sentmail', 'Y');
                $this->db->where('tic.ticket_id', $ticket_id);
                $this->db->where("FIND_IN_SET('" . $ticket_status . "', cl.ticket_status)");
                $query = $this->db->get();
                //echo $this->db->last_query();//exit;
                if ($query->num_rows() > 0) {
                    $data = $query->row();
                    $subject = tempSubject($emilListing->subject, $data);
                    $message = tempContent($emilListing->content, $data);
                    $fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');
                    $toEmail = $data->user_name;
                    $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, 'Seismic360');
                    if ($isSend) {
                        $return['status'] = 'true';
                        $return['message'] = 'email is sent successfully';
                    } else {
                        $return['status'] = 'false';
                        $return['message'] = 'Email error';
                    }
                } else {
                    $return['status'] = 'false';
                    $return['message'] = 'Macd ID not found';
                }
            } else {
                $return['status'] = 'false';
                $return['message'] = 'email format not found';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        }
        $this->response($return, 200);
    }

}
