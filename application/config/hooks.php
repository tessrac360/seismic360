<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | Hooks
  | -------------------------------------------------------------------------
  | This file lets you define "hooks" to extend CI without hacking the core
  | files.  Please see the user guide for info:
  |
  |	https://codeigniter.com/user_guide/general/hooks.html
  |
 */

$hook['pre_controller'] = array(
    'class' => 'App_auth',
    'function' => 'menuTab',
    'filename' => 'App_auth.php',
    'filepath' => 'hooks',
    'params' => array()
);


$hook['post_controller_constructor'] = array(
    'class' => 'App_auth',
    'function' => 'index',
    'filename' => 'App_auth.php',
    'filepath' => 'hooks',
    'params' => array()
);

//$hook['post_controller_constructor'] = array(
//    'class' => 'App_auth',
//    'function' => 'islogincheckHook',
//    'filename' => 'App_auth.php',
//    'filepath' => 'hooks',
//    'params' => array()
//);


