<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends MX_Controller {

    private $tablename, $userId, $roleId, $user_type, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'client';
        $this->company_id = $this->session->userdata('company_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $this->current_date = $this->config->item('datetime');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    /**
     * Login user on the site
     *
     * @return void
     */
    public function login() {

//        $this->load->model('Auth_model');
//        $data['getPart'] = $this->Auth_model->getModule();
        $data['pageTitle'] = 'Login';
        $this->load->view('login_form',$data);
    }

    function do_login() {
        //pr($_REQUEST);exit;
        $this->load->model('Auth_model');
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
        } else {
            $data['uname'] = $this->input->post('username');
            $data['psw'] = $this->input->post('password');
            $login_details = $this->Auth_model->login_admin($data);
            if ($login_details['status'] == 'true') {
                //pr($login_details);
                $this->session->set_userdata("is_login", 'Y');
                $this->session->set_userdata("id", $login_details['resultSet']['id']);
                $this->session->set_userdata("role_id", $login_details['resultSet']['role_id']);
                $this->session->set_userdata("company_id", $login_details['resultSet']['company_id']);
                $this->session->set_userdata("user_type", $login_details['resultSet']['user_type']);
                $this->session->set_userdata("client_id", $login_details['resultSet']['client']);
                $this->session->set_userdata("role_type", $login_details['resultSet']['role_type']);
                $this->session->set_userdata("assign_client_type", $login_details['resultSet']['assign_client_type']);
                $this->session->set_userdata("user_uuid", $login_details['resultSet']['uuid']);
                $this->session->set_userdata("role_uuid", $login_details['resultSet']['role_uuid']);
                $this->session->set_userdata("client_uuid", $login_details['resultSet']['client_uuid']);
                $this->Auth_model->logMaintainLoginAndLogout($login_details['resultSet']['id'], $login_details['resultSet']['uuid'], $login_details['resultSet']['client_uuid'], 'LOGIN');
            }
            unset($login_details['resultSet']);
            //unset($login_details['resultSet']);
            echo json_encode($login_details);
        }
        exit;
    }

    function forgot_password() {
        $data['pageTitle'] = 'Forgot Password';
        $this->load->view('forgotpassward_form',$data);        
    }
    
    public function reset_password($userUUID = "") {
        $this->load->model('Auth_model');
        $isCheckedUUID = $this->Auth_model->isCheckValidUUID($userUUID);
           if ($isCheckedUUID['status'] == 'true') {
               $data['uuid'] = $userUUID;
               $data['pageTitle'] = 'Reset Password';
               $this->load->view('reset_password', $data);
           }else{
               redirect('login');
           }
    }
    
    public function do_resetPassword() { 
        $this->load->model('Auth_model');
        $isValidUUID = $this->Auth_model->isValidUUID($_POST['uuid']);
       // pr($isValidUUID);
        if ($isValidUUID['status'] == 'true') {
            //pr($_POST);exit;
            $data = array(
                'is_mail_send' => 'N',
                'password' => md5($_POST['password'])
            );
            $this->db->where('uuid', $_POST['uuid']);
            if($this->db->update('users', $data)){
                $status = array('status' => 'true', "message" => "Password is reset successfully");
            }else{
                $status = array('status' => 'false', "message" => "Something wents wrong !");
            }

        } else {
            $status = array('status' => 'false', "message" => "Password is already reset!!");
        }
//        pr($status);
//        exit;
        echo json_encode($status);
    }
    
    
    public function do_checkmail() { 
        $this->load->model('Auth_model');
        $forgotDetails = $this->Auth_model->forgotPassword($_POST['email']);

        if ($forgotDetails['status'] == 'true') {            
            $data = array(
                'is_mail_send' => 'Y',                
            );
            $this->db->where('id', $forgotDetails['resultSet']->id);
            $this->db->update('users', $data);
            $url = base_url() . "auth/reset_password/" . $forgotDetails['resultSet']->uuid;
            $link = "<a href='$url'>Click here</a>";

            $emilListing = helperEmailContent('EMAIL_FORGOT_PASSWORD');
            if (!empty($emilListing)) {
                
                $message = tempContent($emilListing->content, $forgotDetails['resultSet'], $link);
                //echo $fromEmailID = helperReplayEmailID('REPLAY_MAILID');
                $toEmail = $forgotDetails['resultSet']->email;
                $isSend = helperSendMail($toEmail, 'noply@eventedge.in', $emilListing->subject, $message, 'Seismic360');
                if ($isSend == 1) {
                    $status = array('error' => 0, "message" => "Reset Password Link has  sent to your email ID");
                } else {
                    $status = array('error' => 1, "message" => "Error in Mail Send");
                }
            }
        } else {
            $status = array('status' => 'false', "message" => "Email not Found");
        }

        echo json_encode($status);
    }

    public function auth_group() {
        if ($this->input->post()) {
            $groupPost = $this->input->post();
            $this->load->model('Auth_model');
            $return = $this->Auth_model->updateDefaultGroupId($groupPost['group_id']);
            if ($return['status'] == 'true') {

                if ($_SESSION['role_type'] == 'M') {
                    redirect('dashboard/incident');
                } else {
                    redirect('dashboard');
                }
            } else {
                redirect('auth-group');
            }
        } else {
            $getCompanyId = helpler_getCompanyId();
            if ($getCompanyId == 1) {
                if ($_SESSION['role_type'] == 'M') {
                    redirect('dashboard/incident');
                } else {
                    redirect('dashboard');
                }
            } else {
                $this->load->model('Auth_model');
                $isDefaultGroupCheck = $this->Auth_model->isDefaultGroupCheck();
                if ($isDefaultGroupCheck['status'] == 'true') {
                    if ($isDefaultGroupCheck['groupCount'] > '0' && $isDefaultGroupCheck['groupCount'] == '1') {
                        if ($_SESSION['role_type'] == 'M') {
                            redirect('dashboard/incident');
                        } else {
                            redirect('dashboard');
                        }
                    } else {

                        $data['groupData'] = $isDefaultGroupCheck;

                        $this->load->view('view_group', $data);
                    }
                } else {
                    if ($_SESSION['role_type'] == 'M') {
                        redirect('dashboard/incident');
                    } else {
                        redirect('dashboard');
                    }
                }
            }
        }
    }

    public function change_auth_group() {

//        $returnSet = helper_getUserLocationAllocate();
//        //pr($returnSet);
//        if ($returnSet['status'] == "true") {
//            helper_isInArray($returnSet['resultSet'], 'unique_slug', 'ZO');
//        }

        if ($this->input->post()) {
            $groupPost = $this->input->post();
            $this->load->model('Auth_model');
            $return = $this->Auth_model->updateDefaultGroupId($groupPost['group_id']);
            if ($return['status'] == 'true') {
                redirect('dashboard');
            }
        } else {
            $this->load->model('Auth_model');
            $data['groupData'] = $this->Auth_model->isGroupCount();
            if ($data['groupData']['status'] == 'true') {
                $data['file'] = 'change_group';
                $this->load->view('template/front_template', $data);
            } else {
                redirect('dashboard');
            }
        }
    }

    public function logout() {
        $data['is_login'] = 'N';
        $this->db->where('id', $this->userId);
        $update_status = $this->db->update('users', $data);
        /* last activity log */
        $user_log['log_out'] = $this->current_date;
        $this->db->where('id', $this->userId);
        $this->db->update('users_last_activity_log', $user_log);
        /* last activity log */
        $this->load->model('Auth_model');
        $this->Auth_model->logMaintainLoginAndLogout($this->session->userdata('id'), $this->session->userdata('user_uuid'), $this->session->userdata('client_uuid'), 'LOGOUT');
        $this->session->sess_destroy();
       // redirect('auth/sessionout/' . encode_url(time()));
        redirect('login');
    }

    public function sessionout($param = "") {
        // echo "hii";exit;
        $this->load->view('sessionout');
    }

    public function api_auth() {
        $this->load->model('event/Model_event');
        $dataResult = $this->Model_event->getMasterKey();
        $context = stream_context_create(array(
            'http' => array(
                'header' => 'Authorization: Basic ' . base64_encode("ketan:123456")
            )
        ));
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/authkey/auth.json', json_encode($dataResult['resultSet']));
        //$this->response($message, 200); 
    }

    public function ssss() {
        $context = stream_context_create(array(
            'http' => array(
                'header' => 'Authorization: Basic ' . base64_encode("ketan:123456")
            )
        ));
        $data = file_get_contents('http://10.10.32.51/authkey/auth.json', false, $context);
        $array = json_decode($data);
        pr($array);

        exit;
    }

}
