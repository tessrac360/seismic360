
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase">Change Domain</span>
                </div>                
            </div>
            <div class="portlet-body form">

                <form role="form" name="frmUser" id="frmUser" method="post" action="">
                    <div class="form-body">
                       <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                           <div class="form-group form-md-radios">
                            <div class="md-radio-inline">  
                                <?php                               
                                if ($groupData['status'] == 'true') {
                                    foreach ($groupData['resultSet'] as $key=>$value) {
                                        ?>
                                        <div class="md-radio">
                                            <input id="radio<?php echo $key; ?>"  <?php
                                                    if ($value->domain_id == $groupData['defaultGroup']) {
                                                        echo 'checked';
                                                    }
                                                    ?>  name="group_id" value="<?php echo $value->domain_id; ?>" class="md-radiobtn" type="radio">
                                            <label for="radio<?php echo $key; ?>">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span><?php echo $value->title; ?></label>
                                        </div> 
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                                </div>
                            </div>
                       </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Change</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('dashboard'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>