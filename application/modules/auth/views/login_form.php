<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
       <title> Seismic360 |  <?php echo isset($pageTitle) ? $pageTitle : $this->router->fetch_class().'-'.$this->router->fetch_method(); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="#1 selling multi-purpose bootstrap admin theme sold in themeforest marketplace packed with angularjs, material design, rtl support with over thausands of templates and ui elements and plugins to power any type of web applications including saas and admin dashboards. Preview page of Theme #2 for "
            name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() . "public/" ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() . "public/" ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url() . "public/" ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url() . "public/" ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo base_url() . "public/" ?>assets/pages/css/login-4.min.css" rel="stylesheet" type="text/css" />
		
        <link href="<?php echo base_url() . "public/" ?>assets/layouts/layout2/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
		<style>
			.login .logo{text-align:left;}
			.login .content{margin:inherit; background:#fff; border-radius: 3px !important; width:auto;
			-webkit-box-shadow: 11px 5px 48px -12px rgba(0,0,0,0.75);
			-moz-box-shadow: 11px 5px 48px -12px rgba(0,0,0,0.75);
			box-shadow: 11px 5px 48px -12px rgba(0,0,0,0.75);
			}
			.banner_title{
				font-size: 40px;
				font-weight: 600;
				color: #fff;
				min-height: 333px;
				display: flex;
				align-items: center;
			}
			.login .copyright{    width: 1024px; bottom:10px; border-top:1px solid rgba(255, 255, 255, 0.2); position:absolute;}
			.login .content .form-title{text-align: center; font-size: 22px; color:#333; margin-bottom:29px;}
			.login .form-group{margin-bottom:20px;}
			.login .content .form-control{border: 0; border-bottom: 1px solid #ccc; padding-left:0;}
			.login .input-icon>i{right:0; left:auto;}
			.login .content .form-actions .btn{margin-top:20px; background:#0d4ea0;  border-radius:20px !important; border:0; padding:6px 20px; width:110px;}
			.login .content .forget-password{text-align: center; font-size: 13px; font-weight: 600;}
			canvas {
			  display: block;
			  vertical-align: bottom;
			}

			/* ---- particles.js container ---- */

			#particles-js {
			  position: absolute;
			  width: 100%;
			  height: 100%;
			  background-image: url("");
			  background-repeat: no-repeat;
			  background-size: cover;
			  background-position: 50% 50%;
			  opacity:0.8;
			}

			/* ---- stats.js ---- */

			.count-particles{
			  background: #000022;
			  position: absolute;
			  top: 48px;
			  left: 0;
			  width: 80px;
			  color: #13E8E9;
			  font-size: .8em;
			  text-align: left;
			  text-indent: 4px;
			  line-height: 14px;
			  padding-bottom: 2px;
			  font-family: Helvetica, Arial, sans-serif;
			  font-weight: bold;
			}

			.js-count-particles{
			  font-size: 1.1em;
			}

			#stats,
			.count-particles{
			  -webkit-user-select: none;
			}

			#stats{
			  border-radius: 3px 3px 0 0;
			  overflow: hidden;
			}

			.count-particles{
			  border-radius: 0 0 3px 3px;
			}	
			@media (min-width: 1200px){
				.container {
					width: 1024px;
				}
			}
		</style>
		
		</head>
    <!-- END HEAD -->

    <body class=" login">
	
	<div id="particles-js"></div>
	
	<div class="container">
        <div class="row">
			<!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.html">
                <img src="<?php echo base_url() . "public/" ?>assets/layouts/layout2/img/logo-default.png" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <div class="row">
			<div class="col-md-8 banner_title">
				Transformative<br />Business Solutions


			</div>
			<div class="col-md-4">
				<!-- BEGIN LOGIN -->
				<div class="content">
					<!-- BEGIN LOGIN FORM -->
					<form class="login-form" method="post" id="login-form">
						<h3 class="form-title">Login To Your Accounts</h3>
						<div class="alert alert-danger display-hide">
							<button class="close" data-close="alert"></button>
							<span id="errorLoginMessage"></span>
						</div>
						<div class="form-group">
							<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
							<label class="control-label visible-ie8 visible-ie9">Username</label>
							<div class="input-icon">
								<i class="fa fa-user"></i>
								<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"  id="username"/> </div>
						</div>
						<div class="form-group">
							<label class="control-label visible-ie8 visible-ie9">Password</label>
							<div class="input-icon">
								<i class="fa fa-lock"></i>
								<input class="form-control placeholder-no-fix mypassword"  type="password" autocomplete="off" placeholder="Password" name="password" id="password"/> </div>
						</div>
						<div class="form-actions text-center">
							<button type="submit" class="btn green"> Login </button>
						</div>
						
						<div class="forget-password">
							<a href="<?php echo base_url('forgot-password');?>" id="forget-password"> Forgot your password ? </a>
						</div>
					   
					</form>
					<!-- END LOGIN FORM -->
				   
				   
				</div>
				<input type="hidden" id="baseURL" value="<?php echo base_url(); ?>"/>
				<!-- END LOGIN -->
			</div>
		</div>
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> 
            Copyright &copy; 2017-<?php echo date('Y')?> Seismic LLC. All rights reserved.             
        </div>
        <!-- END COPYRIGHT -->
		</div>
	</div>		
        <!--[if lt IE 9]>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url() . "public/" ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url() . "public/" ?>js/form/form_auth.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        
		<script src='https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js'></script>
		
		<script>
		/* ---- particles.js config ---- */
		particlesJS("particles-js", {
		  "particles": {
			"number": {
			  "value": 120,
			  "density": {
				"enable": true,
				"value_area": 1000
			  }
			},
			"color": {
			  "value": "#ffffff"
			},
			"shape": {
			  "type": "circle",
			  "stroke": {
				"width": 0,
				"color": "#000000"
			  },
			  "polygon": {
				"nb_sides": 5
			  },
			  "image": {
				"src": "img/github.svg",
				"width": 100,
				"height": 100
			  }
			},
			"opacity": {
			  "value": 0.5,
			  "random": false,
			  "anim": {
				"enable": false,
				"speed": 1,
				"opacity_min": 0.1,
				"sync": false
			  }
			},
			"size": {
			  "value": 3,
			  "random": true,
			  "anim": {
				"enable": false,
				"speed": 40,
				"size_min": 0.1,
				"sync": false
			  }
			},
			"line_linked": {
			  "enable": true,
			  "distance": 150,
			  "color": "#ffffff",
			  "opacity": 0.4,
			  "width": 1
			},
			"move": {
			  "enable": true,
			  "speed": 3,
			  "direction": "none",
			  "random": false,
			  "straight": false,
			  "out_mode": "out",
			  "bounce": false,
			  "attract": {
				"enable": false,
				"rotateX": 600,
				"rotateY": 1200
			  }
			}
		  },
		  "interactivity": {
			"detect_on": "canvas",
			"events": {
			  "onhover": {
				"enable": true,
				"mode": "grab"
			  },
			  "onclick": {
				"enable": true,
				"mode": "push"
			  },
			  "resize": true
			},
			"modes": {
			  "grab": {
				"distance": 140,
				"line_linked": {
				  "opacity": 1
				}
			  },
			  "bubble": {
				"distance": 400,
				"size": 40,
				"duration": 2,
				"opacity": 8,
				"speed": 3
			  },
			  "repulse": {
				"distance": 200,
				"duration": 0.4
			  },
			  "push": {
				"particles_nb": 4
			  },
			  "remove": {
				"particles_nb": 2
			  }
			}
		  },
		  "retina_detect": true
		});


		/* ---- stats.js config ---- */

		var count_particles, stats, update;
		stats = new Stats;
		stats.setMode(0);
		stats.domElement.style.position = 'absolute';
		stats.domElement.style.left = '0px';
		stats.domElement.style.top = '0px';
		document.body.appendChild(stats.domElement);
		count_particles = document.querySelector('.js-count-particles');
		update = function() {
		  stats.begin();
		  stats.end();
		  if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
			count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
		  }
		  requestAnimationFrame(update);
		};
		requestAnimationFrame(update);
		
		</script>
      
    </body>
</html>