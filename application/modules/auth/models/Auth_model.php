<?php

class Auth_model extends CI_Model {

    private $current_date, $userId;

    function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->userId = $this->session->userdata('id');
    }

    public function getIsloginStatus() {
        $this->db->select('is_login');
        $this->db->from('users');
        $this->db->where('id', $this->userId);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $is_login = $row['is_login'];
        } else {
            $is_login = 'N';
        }
        return $is_login;
    }

    public function forgotPassword($emalId = "") {
        $status = array('error' => 1);
        $this->db->select('first_name as name ,id,uuid,email_id as email');
        $this->db->from('users');
        $this->db->where('email_id', $emalId);
        $this->db->where('active_status', 'Y');
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {
            $status['resultSet'] = $query->row();
            $status['status'] = 'true';
            
        } else {
            $status['status'] = 'false';
        }
        return $status;
    }
    
     public function isCheckValidUUID($userUUID = "") {        
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('uuid', $userUUID);
        $this->db->where('active_status', 'Y');
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {            
            $status['status'] = 'true';            
        } else {
            $status['status'] = 'false';
        }
        return $status;
    }
    
    public function isValidUUID($userUUID = "") {        
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('uuid', $userUUID);
        $this->db->where('active_status', 'Y');
        $this->db->where('is_mail_send', 'Y');
        $query = $this->db->get(); 
      //   echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {            
            $status['status'] = 'true';            
        } else {
            $status['status'] = 'false';
        }
        return $status;
    }

    public function login_admin($postValue = array()) {
        $this->db->select('id,role_id,is_login,active_status,user_type,company_id,default_domains,client,is_deleted,multi_signon_allow,role_type,assign_client_type, uuid, role_uuid, client_uuid');
        $this->db->from('users');
        $this->db->where('password', md5($postValue['psw']));
        $this->db->where("(email_id = '" . $postValue['uname'] . "' OR user_name = '" . $postValue['uname'] . "')");
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            if ($row['is_deleted'] == 'N') {
                if ($row['active_status'] == 'Y') {
                    if ($row['is_login'] == 'N') {
                        //pr($row);//exit;
                        $status['status'] = 'true';
                        $status['message'] = "Successfull Login";
                        $status['role_type'] = $row['role_type'];
                        $status['pageurl'] = helper_getDashBoardStatus($row['role_id']);
                        $status['resultSet'] = $row;

                        if ($row['multi_signon_allow'] == '0') {
                            $this->updateIsLogin($row['id']);
                        }
                        $this->InsertLastActivietyLogin($row['id']);
                        //pr($user_log);
                    } else {
                        $status['status'] = 'false';
                        $status['resultSet'] = array();
                        $status['message'] = "User credential is used by another user please contact with administrator";
                    }
                } else {
                    $status['status'] = 'false';
                    $status['resultSet'] = array();
                    $status['message'] = "Your Account is deactivated";
                }
            } else {
                $status['status'] = 'false';
                $status['resultSet'] = array();
                $status['message'] = "Your Account is deleted";
            }
        } else {
            $status['status'] = 'false';
            $status['resultSet'] = array();
            $status['message'] = "Please enter correct Email and password";
        }
        //pr($status);
        return $status;
    }

    public function logMaintainLoginAndLogout($userId = '', $userUUID = '', $clientUUID = '', $type = '') {
        $postData = array(
            'user_id' => $userId,
            'sid' => session_id(),
            'date' => date("Y-m-d"),
            'time' => date("H:i:s"),
            'ipAddress' => get_client_ip(),
            'type' => $type,
            'company_id' => helpler_getCompanyId(),
            'created_on' => $this->current_date,
            'user_uuid' => $userUUID,
            'client_uuid' => $clientUUID,
        );
        $this->db->insert('user_log', $postData);
    }

    private function updateIsLogin($userId = "") {
        $data['is_login'] = 'Y';
        $this->db->where('id', $userId);
        $update_status = $this->db->update('users', $data);
    }

    private function InsertLastActivietyLogin($userId = "") {
        $this->db->select('id');
        $this->db->from('users_last_activity_log');
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $user_log['browser'] = $this->input->user_agent();
            $user_log['ipAddress'] = $this->input->ip_address();
            $user_log['log_on'] = $this->current_date;
            $user_log['user_id'] = $userId;
            $this->db->where('id', $row['id']);
            $update_status = $this->db->update('users_last_activity_log', $user_log);
        } else {
            $user_log['browser'] = $this->input->user_agent();
            $user_log['ipAddress'] = $this->input->ip_address();
            $user_log['log_on'] = $this->current_date;
            $user_log['user_id'] = $userId;
            $this->db->insert('users_last_activity_log', $user_log);
        }
    }

    public function isDefaultGroupCheck() {
        $this->db->select('default_domains');
        $this->db->from('users');
        $this->db->where('id', $this->userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            if ($row['default_domains'] == 0) {
                $this->db->select('ugl.domain_id,grp.title');
                $this->db->from('users_domain as ugl');
                $this->db->join('group as grp', 'grp.id = ugl.domain_id');
                $this->db->where('grp.status', 'Y');
                $this->db->where('ugl.user_id', $this->userId);
                $queryCount = $this->db->get();
                if ($queryCount->num_rows() > 0 && $queryCount->num_rows() == 1) {
                    $resultSet = $queryCount->row_array();
                    $this->updateDefaultGroupId($resultSet['group_id']);
                    $data['groupCount'] = $queryCount->num_rows();
                } else {
                    $data['groupCount'] = $queryCount->num_rows();
                    $data['resultSet'] = $queryCount->result();
                }
                $data['status'] = 'true';
            } else {
                $data['status'] = 'false';
            }
//            pr($data);
//            exit;
            return $data;
        }
    }

    public function updateDefaultGroupId($group_id = "") {
        $data = array('default_domains' => $group_id);
        $this->db->where('id', $this->userId);
        if ($this->db->update('users', $data)) {
            $data['status'] = 'true';
        } else {
            $data['status'] = 'false';
        }
        return $data;
    }

    public function getDefaultGroup() {
        $this->db->select('default_domains');
        $this->db->from('users');
        $this->db->where('id', $this->userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->default_domains;
        } else {
            return 0;
        }
    }

    public function getGroupLocationAllocate() {
        $currentGroup = $this->getDefaultGroup();
        $this->db->select('location_serialization as location');
        $this->db->from('users_domain');
        $this->db->where('group_id', $currentGroup);
        $this->db->where('user_id', $this->userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $locationarray = unserialize($row->location);
            $this->db->select('id,location,url,unique_slug');
            $this->db->from('group_location');
            $this->db->where('status', 'Y');
            $this->db->where('groupid', $currentGroup);
            $this->db->where_in('id', $locationarray);
            $queryGroup = $this->db->get();
            if ($queryGroup->num_rows() > 0) {
                $data['status'] = 'true';
                $data['resultSet'] = $queryGroup->result_array();
            } else {
                $data['status'] = 'false';
            }
        } else {
            $data['status'] = 'false';
        }
        return $data;
    }

    public function isGroupCount() {
        $this->db->select('ugl.domain_id,grp.title');
        $this->db->from('users_domain as ugl');
        $this->db->join('group as grp', 'grp.id = ugl.domain_id');
        $this->db->where('grp.status', 'Y');
        $this->db->where('ugl.user_id', $this->userId);
        $queryCount = $this->db->get();
        if ($queryCount->num_rows() >= 2) {
            $data['status'] = 'true';
            $data['resultSet'] = $queryCount->result();
            $data['defaultGroup'] = $this->getDefaultGroup();
        } else {
            $data['status'] = 'false';
        }
        return $data;
    }

}

?>