<?php

class Model_dashboard extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    private $finall;

    public function __construct() {
        parent::__construct();
        $this->tablename = 'client';
        $this->users = 'users';
        $this->temp_role = 'temp_role';
        $this->temp_role_permission = 'temp_role_permission';
        $this->role = 'role';
        $this->role_permission = 'role_permission';
        $this->master_columns = 'event_column';
        $this->rename_columns = 'event_column_rename';
        $this->company_id = $this->session->userdata('company_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->accessLevelClient = helper_getAccessLevelClient();
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    private function currentMonth() {
        $list = array();
        $month = date('m');
        $year = date('Y');
        $day = date('d');
        for ($d = 1; $d <= 31; $d++) {
            $time = mktime(12, 0, 0, $month, $d, $year);

            if (date('m', $time) == $month)
                $list[] = date('Y-m-d', $time);
            if ($d == $day) {
                break;
            }
        }
        return $list;
    }

    public function getTotalAlarms() {
        $filter = "";
        if ($this->accessLevelClient['status'] == 'true') {
            $x = implode(",", $this->accessLevelClient['resultSet']);
            $filter = " and ev.client_id in(" . $x . ")";
        }
        $rs = $this->db->query('select count(ev.event_id) as cnt, sev.severity from eventedge_events ev, eventedge_severities sev where ev.severity_id=sev.id and ev.severity_id!=2 ' . $filter . 'group by `severity_id`');
        $array = $rs->result_array();
        $eventss = array();
        for ($i = 0; $i < count($array); $i++) {
            $eventss[$array[$i]['severity']] = $array[$i]['cnt'];
        }
        return $eventss;
    }

    public function getSla() {
        $filter = "";
        if ($this->accessLevelClient['status'] == 'true') {
            $x = implode(",", $this->accessLevelClient['resultSet']);
            $filter = " and e.client_id in(" . $x . ")";
        }
        $rs = $this->db->query('Select count(counter) from (SELECT *,  TIME_TO_SEC(TIMEDIFF(end_time, event_start_time)) as timediff, case when TIME_TO_SEC(TIMEDIFF(end_time, event_start_time))<=slatkt then "1" else "0" end as counter from (SELECT e.event_id, e.client_id, e.event_start_time, t.status_code, t.priority, t.ticket_id,  (SELECT CASE WHEN ticket_id is not null THEN t.created_on WHEN ticket_id="" and status_code = "7" THEN e.event_end_time WHEN ticket_id="" THEN NOW() END FROM `eventedge_ticket_activity` WHERE ticket_id = t.ticket_id ORDER by activity_id DESC LIMIT 1) as end_time, (SELECT status FROM `eventedge_ticket_status` WHERE status_code = t.status_code) as ticket_status, (SELECT sla.incident_generated_sla from eventedge_events_sla sla where e.client_id=sla.client_id and sla.priority_id=e.priority_id limit 1) as slatkt, t.created_on FROM `eventedge_tickets` t, `eventedge_events` e where e.event_id=t.event_id  ' . $filter . '  AND EXTRACT(YEAR_MONTH FROM e.event_start_time) = EXTRACT(YEAR_MONTH FROM CURDATE()) limit 1) as db) as main where counter=1
');
        $array = $rs->result_array();
        $sla_cnt = count($array);

        $rs1 = $this->db->query('select count(e.event_id) as cnt  from eventedge_events e where EXTRACT(YEAR_MONTH FROM e.event_start_time) = EXTRACT(YEAR_MONTH FROM CURDATE())  ' . $filter . ' ');
        $array1 = $rs1->result_array();
        $per_sla = 0;
        if ($array1[0]['cnt'] > 0 && $sla_cnt > 0) {
            $per_sla = round(( $sla_cnt / $array1[0]['cnt'] ) * 100);
        }
        //pr($array1);exit;
        //echo $this->db->last_query();exit;
        //pr($array);exit;
        return $per_sla;
    }

    public function getMttr() {
        $filter = "";
        if ($this->accessLevelClient['status'] == 'true') {
            $x = implode(",", $this->accessLevelClient['resultSet']);
            $filter = " and e.client_id in(" . $x . ")";
        }

        //$rs = $this->db->query('select avg(TIME_TO_SEC(TIMEDIFF(`event_end_time`,`created_on`))) as mttr, `created_on`, `event_end_time` from eventedge_events where event_end_time!="0000-00-00 00:00:00" ' . $filter);
        $rs = $this->db->query('select avg(in_Sec) as mttr from (select *, TIME_TO_SEC(TIMEDIFF(end_time, event_start_time)) as in_Sec from (SELECT e.event_id, e.event_start_time, t.status_code, (case when e.ticket_id =0 then now() when (select status_code from `eventedge_ticket_activity` WHERE ticket_id = t.ticket_id ORDER by activity_id DESC LIMIT 1)=7 then (SELECT end_time FROM `eventedge_ticket_activity` WHERE ticket_id = t.ticket_id and status_code = "7" and end_time!="0000-00-00 00:00:00" ORDER by activity_id DESC LIMIT 1) else NOW() end) as end_time, (SELECT status FROM `eventedge_ticket_status` WHERE status_code = t.status_code) as ticket_status FROM `eventedge_tickets` t, `eventedge_events` e where e.event_id=t.event_id  ' . $filter . '  AND EXTRACT(YEAR_MONTH FROM e.event_start_time) = EXTRACT(YEAR_MONTH FROM CURDATE())) as db ) as main
');
        $array = $rs->result_array();
        //echo $this->db->last_query();exit;
        return $array;
    }

    public function getMtrs() {
        $filter = "";
        if ($this->accessLevelClient['status'] == 'true') {
            $x = implode(",", $this->accessLevelClient['resultSet']);
            $filter = " and e.client_id in(" . $x . ")";
        }

        $rs = $this->db->query('select avg(diff_in_Sec) as mtrs from (select e.event_id, e.event_start_time, case when e.ticket_id="" and aout.state_status=7 then TIME_TO_SEC(TIMEDIFF( e.event_end_time,e.event_start_time)) when e.ticket_id="" then TIME_TO_SEC(TIMEDIFF( NOW(),e.event_start_time)) when e.ticket_id!="" then TIME_TO_SEC(TIMEDIFF( t.created_on,e.event_start_time)) end  as diff_in_Sec, t.ticket_id, t.created_on, (SELECT a.start_time FROM `eventedge_ticket_activity` a WHERE a.`ticket_id` = t.ticket_id and a.state_status=7 order by a.activity_id DESC LIMIT 1) as end_start_time, (SELECT a.state_status FROM `eventedge_ticket_activity` a WHERE a.`ticket_id` = t.ticket_id order by a.activity_id DESC LIMIT 1 ) as latest_status from eventedge_tickets t, eventedge_events e, `eventedge_ticket_activity` aout  where t.event_id=e.event_id  ' . $filter . '  AND EXTRACT(YEAR_MONTH FROM e.event_start_time) = EXTRACT(YEAR_MONTH FROM CURDATE()) and aout.ticket_id=t.ticket_id order by t.ticket_id and aout.activity_id ) as main');
        $array = $rs->result_array();
        //echo $this->db->last_query();exit;
        return $array;
    }

    public function MTTRSeverity($Severity = "") {
//        $this->db->select("ticket_id,created_on,due_date, (SELECT `start_time` FROM `eventedge_ticket_activity` WHERE `ticket_id` = eventedge_tickets.ticket_id AND `state_status` = '7') as `start_time`");
//        $this->db->from('tickets');
//        $this->db->where('priority', $Severity);
//        $query = $this->db->get();
//       // echo $this->db->last_query();//exit;
//        $Met = 0;
//        $missed = 0;
//        if ($query->num_rows() > 0) {
//
//            foreach ($query->result() as $value) {  
//                if ($value->start_time != "") {
//                    if (strtotime($value->start_time) <= strtotime($value->due_date)) {
//                        $Met++;
//                    } else {
//                        $missed++;
//                    }
//                } else {
//                    $missed++;
//                }
//            }
//            $array = array(
//                0 => array(
//                    'type' => 'Met',
//                    'count' => $Met,
//                    'color' => '#7ec34e'
//                ),
//                1 => array(
//                    'type' => 'Missed',
//                    'count' => $missed,
//                    'color' => '#ff6600'
//                )
//            );
//        } else {
//            $array = array();
//        }
        $array = array();
//        pr($array);
//        exit;
        return json_encode($array);
        exit;
    }

    public function MTTRSeverityMACD($Severity = "") {
        $this->db->select("tic.mttr_due_time as due_date, (SELECT `start_time` FROM `eventedge_ticket_activity` WHERE `ticket_id` = mac.ticket_id AND `state_status` = '7') as `start_time`");
        $this->db->from('macd as mac');
        $this->db->join('tickets as tic', 'tic.ticket_id=mac.ticket_id');
        $this->db->where('tic.priority', $Severity);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $Met = 0;
        $missed = 0;
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $value) {
                if ($value->start_time != "") {
                    if (strtotime($value->start_time) <= strtotime($value->due_date)) {
                        $Met++;
                    } else {
                        $missed++;
                    }
                } else {
                    $missed++;
                }
            }
            $array = array(
                0 => array(
                    'type' => 'Met',
                    'count' => $Met,
                    'color' => '#7ec34e'
                ),
                1 => array(
                    'type' => 'Missed',
                    'count' => $missed,
                    'color' => '#ff6600'
                )
            );
        } else {
            $array = array();
        }
//        pr($array);
//        exit;
        return json_encode($array);
        exit;
    }

    //For Pie chart : Priority wise MTTR, MTRS

    public function MTRSSeverity($Severity = "") {
        $this->db->select('ticket_id,created_on,resolved_on');
        $this->db->from('tickets');
        $this->db->where('priority', $Severity);
        $query = $this->db->get();
        //echo $this->db->last_query();
        $Met = 0;
        $missed = 0;
        $st = array(5, 7);
        if ($query->num_rows() > 0) {
            $dataTicket = $query->result_array();
            //pr($dataTicket);
            foreach ($query->result() as $value) {
                $this->db->select('start_time');
                $this->db->from('ticket_activity');
                $this->db->where('ticket_id', $value->ticket_id);
                $this->db->where_in('state_status', $st);
                $this->db->order_by('start_time', 'asc');
                $queryActivity = $this->db->get();

                if ($queryActivity->num_rows() > 0) {
                    $activity = $queryActivity->row();
                    //pr($activity)
//                    echo "<br>";
//                    echo $value->ticket_id.'------'.$activity->start_time .'-------------'.$value->due_date."<br>";   
                    //pr($activity->start_time); 
                    //pr($value->resolved_on); exit;
                    if (($activity->start_time) - ($value->created_on) <= 3600) {
                        $Met++;
                    } else {
                        $missed++;
                    }
                } else {
                    $missed++;
                }
            }
            $array = array(
                0 => array(
                    'type' => 'Met',
                    'count' => $Met,
                    'color' => '#7ec34e'
                ),
                1 => array(
                    'type' => 'Missed',
                    'count' => $missed,
                    'color' => '#ff6600'
                )
            );
        } else {
            $array = array();
        }
        return json_encode($array);
        exit;
    }

    public function MTRSSeverityMACD($Severity = "") {
        $this->db->select("tic.mtrs_due_time as due_date, (SELECT `start_time` FROM `eventedge_ticket_activity` WHERE `ticket_id` = mac.ticket_id AND `state_status` = '5'  order by activity_id desc limit 1) as `start_time`,tic.status_code");
        $this->db->from('macd as mac');
        $this->db->join('tickets as tic', 'tic.ticket_id=mac.ticket_id');
        $this->db->where('tic.priority', $Severity);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        $Met = 0;
        $missed = 0;
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $value) {
                if ($value->start_time != "" && $value->status_code == 5) {
                    if (strtotime($value->start_time) <= strtotime($value->due_date)) {
                        $Met++;
                    } else {
                        $missed++;
                    }
                } else {
                    $missed++;
                }
            }
            $array = array(
                0 => array(
                    'type' => 'Met',
                    'count' => $Met,
                    'color' => '#7ec34e'
                ),
                1 => array(
                    'type' => 'Missed',
                    'count' => $missed,
                    'color' => '#ff6600'
                )
            );
        } else {
            $array = array();
        }
//        pr($array);
//        exit;
        return json_encode($array);
        exit;
    }

    public function getTicketsCount() {
        $filter = "";
//        if ($this->accessLevelClient['status'] == 'true') {
//            $x = implode(",", $this->accessLevelClient['resultSet']);
//            $filter = " and tick.client_id in(" . $x . ")";
//        }
        //$rs = $this->db->query('select count(tick.ticket_id) as cnt, ticks.status,ticks.status_code 
        //
        //from eventedge_tickets tick, eventedge_ticket_status ticks 
        //where tick.ticket_type_id=1 and tick.status_code=ticks.status_code ' . $filter . 'group by tick.`status_code`');
        //$array = $rs->result_array();
        //echo $this->db->last_query();exit;
        //pr($this->accessLevelClient);
        $this->db->select("count(tick.ticket_id) as cnt, ticks.status,ticks.status_code");
        $this->db->from('eventedge_tickets as tick');
        $this->db->join('eventedge_ticket_status as ticks', 'tick.status_code=ticks.status_code');
        if ($this->accessLevelClient['status'] == 'true') {
            $this->db->where_in('tick.client_id', $this->accessLevelClient['resultSet']);
        }
        $this->db->group_by("tick.`status_code");
        $query = $this->db->get();
        //echo $this->db->last_query();//exit;
        $eventss = array();
        if ($query->num_rows() > 0) {
            $array = $query->result_array();
            //pr($array);
            for ($i = 0; $i < count($array); $i++) {
                $eventss[$array[$i]['status_code']] = $array[$i]['cnt'];
            }
        }





//        pr($eventss);
//        exit;
        return $eventss;
    }

    public function getMTTRincident() {

        $filter = "";
        if ($this->accessLevelClient['status'] == 'true') {
            $x = implode(",", $this->accessLevelClient['resultSet']);
            $filter = " and t.client_id in(" . $x . ")";
        }

        $rs = $this->db->query('select avg(in_Sec) as secons from (select *, TIME_TO_SEC(TIMEDIFF(end_time, created_on)) as in_Sec from (SELECT t.created_on, t.status_code, (SELECT CASE WHEN end_time!="0000-00-00 00:00:00" THEN end_time ELSE NOW() END FROM `eventedge_ticket_activity` WHERE ticket_id = t.ticket_id ORDER by activity_id DESC LIMIT 1) as end_time, (SELECT status FROM `eventedge_ticket_status` WHERE status_code = t.status_code) as ticket_status FROM `eventedge_tickets` t where status_code = "7"  ' . $filter . ' AND EXTRACT(YEAR_MONTH FROM t.created_on) = EXTRACT(YEAR_MONTH FROM CURDATE())) as db ) as main');
        $array = $rs->row();
        return $array;
    }

    public function getMTRSincident() {
        $rs = $this->db->query('select avg(in_Sec) as secons from (select *, TIME_TO_SEC(TIMEDIFF(end_time, created_on)) as in_Sec from (SELECT t.created_on, t.status_code, (SELECT end_time FROM `eventedge_ticket_activity` WHERE ticket_id = t.ticket_id ORDER by activity_id DESC LIMIT 1) as end_time, (SELECT status FROM `eventedge_ticket_status` WHERE status_code = t.status_code) as ticket_status FROM `eventedge_tickets` t where status_code in (6,7) AND EXTRACT(YEAR_MONTH FROM t.created_on) = EXTRACT(YEAR_MONTH FROM CURDATE())) as db) as main');
        $array = $rs->row();
        return $array;
    }

    public function getSLAincident() {
        $rs = $this->db->query('SELECT ((COUNT(timediffSec)*100)/(SELECT count(ticket_id) FROM `eventedge_tickets` WHERE EXTRACT(YEAR_MONTH FROM created_on) = EXTRACT(YEAR_MONTH FROM CURDATE()))) as main FROM (SELECT t.created_on, (SELECT start_time FROM `eventedge_ticket_activity` WHERE ticket_id = t.ticket_id ORDER by activity_id DESC LIMIT 1) as start_time, TIME_TO_SEC(TIMEDIFF((SELECT start_time FROM `eventedge_ticket_activity` WHERE ticket_id = t.ticket_id ORDER by activity_id DESC LIMIT 1),t.created_on)) as timediffSec FROM `eventedge_tickets` t WHERE t.status_code = 2 and EXTRACT(YEAR_MONTH FROM t.created_on) = EXTRACT(YEAR_MONTH FROM CURDATE())) as db where timediffSec <= 900');
        $array = $rs->row();
        return $array;
    }

    public function getAlartypeCount() {
        $filter = "";
        if ($this->accessLevelClient['status'] == 'true') {
            $x = implode(",", $this->accessLevelClient['resultSet']);
            $filter = " and ev.client_id in(" . $x . ")";
        }
        $rs = $this->db->query('select count(ev.`event_id`) as cnt,ev.service_id, serv.service_description,AVG(TIME_TO_SEC(TIMEDIFF(tick.last_updated_on,tick.created_on))) AS timediff from eventedge_events ev, eventedge_services serv,`eventedge_tickets` tick where tick.status_code =7 and  serv.service_id = ev.service_id and ev.event_id = tick.event_id and tick.ticket_type_id=1 and ev.ticket_id !=0 ' . $filter . 'group by ev.`service_id` order by cnt desc');
        $array = $rs->result_array();
        //echo $this->db->last_query();exit;
        //pr($array);exit;
        $eventss = array();
        $events = array();
        $ii = 0;
        for ($i = 0; $i < count($array); $i++) {

            if (count($eventss) == 5) {
                break;
            }
            if ($array[$i]['service_description'] != '') {
                if (!isset($eventss[$array[$i]['service_description']])) {
                    $secondsInAMinute = 60;
                    $secondsInAnHour = 60 * $secondsInAMinute;
                    $secondsInADay = 24 * $secondsInAnHour;
                    $avgal = round($array[$i]['timediff'] / $secondsInADay, 2);
                    $events['name'][] = $array[$i]['service_description'];
                    $events['cnt'][] = $array[$i]['cnt'];
                    $events['avgal'][] = $avgal;
                    $events['service_id'][] = $array[$i]['service_id'];
                    $eventss[$array[$i]['service_description']] = 'ok';
                    $ii++;
                }
            }
        }
        //pr($events);exit;
        return $events;
    }

    public function getTotalClients() {
        $filter = "";
        if ($this->accessLevelClient['status'] == 'true') {
            $x = implode(",", $this->accessLevelClient['resultSet']);
            $filter = " and tick.client_id in(" . $x . ")";
        }
        $rs = $this->db->query('SELECT count(tick.ticket_id) as cnt,cli.client_title,tick.client_id,AVG(TIME_TO_SEC(TIMEDIFF(tick.last_updated_on,tick.created_on))) AS timediff FROM `eventedge_tickets` tick ,eventedge_client cli WHERE  cli.id = tick.client_id and tick.ticket_type_id=1  ' . $filter . ' group by tick.client_id ORDER BY cnt DESC limit 5');
        $array = $rs->result_array();
        //pr($array);exit;
        //=echo $this->db->last_query();exit;
        return $array;
    }

    public function getLastUpdatedBucket() {
        $AgeBucket = array(array("Min" => "0", "Max" => "1"), array("Min" => "2", "Max" => "4"), array("Min" => "5", "Max" => "10"), array("Min" => "11", "Max" => "700"));
        $array_ret = array();
        $lastArrayCount = count($AgeBucket) - 1;
        foreach ($AgeBucket as $key => $value) {
            $actendtime = "(SELECT end_time  FROM `eventedge_incident_activity_log` WHERE `ticket_id` = tick.ticket_id ORDER by activity_log_id DESC LIMIT 1)";
            $this->db->select("COUNT(ticket_id) as total_ticket");
            $this->db->from('tickets as tick');
            $this->db->where('DATEDIFF(CURDATE(), ' . $actendtime . ') >=', $value['Min']);
            $this->db->where('DATEDIFF(CURDATE(), ' . $actendtime . ') <=', $value['Max']);
            $this->db->where('tick.status_code !=', '7');
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('tick.client_id', $this->accessLevelClient['resultSet']);
            }
            $query = $this->db->get();
            //echo $this->db->last_query().'<br>';
            // exit;
            if ($query->num_rows() > 0) {
                $data = $query->row();
                if ($lastArrayCount == $key) {
                    $array_ret[] = array('days' => ($value['Min']) . ' +Days', 'count' => $data->total_ticket, 'flag' => base64_encode($value['Min'] . '-' . $value['Max']));
                } else {
                    $array_ret[] = array('days' => $value['Min'] . '-' . $value['Max'] . ' Days', 'count' => $data->total_ticket, 'flag' => base64_encode($value['Min'] . '-' . $value['Max']));
                }
            }
        }
        /////pr($array_ret);exit;
        return json_encode($array_ret);
        exit;
    }

    public function getAgeBucket() {
        $AgeBucket = array(array("Min" => "0", "Max" => "5"), array("Min" => "6", "Max" => "10"), array("Min" => "11", "Max" => "15"), array("Min" => "16", "Max" => "700"));
        $array_ret = array();
        $lastArrayCount = count($AgeBucket) - 1;
        foreach ($AgeBucket as $key => $value) {
            $this->db->select("COUNT(ticket_id) as total_ticket");
            $this->db->from('tickets');
            $this->db->where('DATEDIFF(CURDATE(), created_on) >=', $value['Min']);
            $this->db->where('DATEDIFF(CURDATE(), created_on) <=', $value['Max']);
            $this->db->where('status_code !=', '7');
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('client_id', $this->accessLevelClient['resultSet']);
            }
            $query = $this->db->get();
            //echo $this->db->last_query().'<br>';
            if ($query->num_rows() > 0) {
                $data = $query->row();
                if ($lastArrayCount == $key) {
                    $array_ret[] = array('days' => $value['Min'] . ' +Days', 'count' => $data->total_ticket, 'flag' => base64_encode($value['Min'] . '-' . $value['Max']));
                } else {
                    $array_ret[] = array('days' => $value['Min'] . '-' . $value['Max'] . ' Days', 'count' => $data->total_ticket, 'flag' => base64_encode($value['Min'] . '-' . $value['Max']));
                }
            }
        }
        //exit;
        return json_encode($array_ret);
        exit;
    }

    private function currentDayHours() {
        $iTimestamp = mktime(1, 0, 0, 1, 1, 2011);
        for ($i = 0; $i < 24; $i++) {
            $list[] = date('H', $iTimestamp);
            $iTimestamp += 3600;
        }

        sort($list);
        return $list;
    }

    public function getChartCurrentDate($date = "") {
        $hours = $this->currentDayHours();
        if (!empty($date)) {
            $permalink = array(2);
            $this->db->select("count(event_id) as event_count, HOUR(event_start_time) as date");
            $this->db->from('events');
            $this->db->where('DATE(event_start_time)', $date);
            $this->db->order_by("event_start_time", "ASC");
            $this->db->where_not_in('severity_id', $permalink);
            $this->db->group_by("HOUR(event_start_time)");
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('client_id', $this->accessLevelClient['resultSet']);
            }
            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
            if ($query->num_rows() > 0) {
                $data = $query->result_array();
                $i = 0;
                foreach ($hours as $value) {
                    if (in_array($value, array_column($data, 'date'))) {
                        $arrayIndex = array_column($data, 'event_count');
                        $myArray[] = array('event_count' => $arrayIndex[$i], 'hour' => $value . ':00', 'date' => $date);
                        $i++;
                    } else {
                        $myArray[] = array('event_count' => '0', 'hour' => $value . ':00', 'date' => $date);
                    }
                }

                $return['result'] = $myArray;
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getChartCurrentMonth() {
        $day = $this->currentMonth();
        if (!empty($day)) {
            $permalink = array(2);
            $this->db->select("count(event_id) as event_count,DATE_FORMAT(event_start_time, '%Y-%m-%d') as date");
            $this->db->from('events');
            $this->db->where_in('DATE(event_start_time)', $day);
            $this->db->group_by('DATE(event_start_time)');
            $this->db->order_by("event_start_time", "ASC");
            $this->db->where_not_in('severity_id', $permalink);
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('client_id', $this->accessLevelClient['resultSet']);
            }
            $query = $this->db->get();
            //echo $this->db->last_query();exit;
            if ($query->num_rows() > 0) {
                $data = $query->result_array();
                $myArray = array();
                $i = 0;
                foreach ($day as $value) {
                    if (in_array($value, array_column($data, 'date'))) {
                        $arrayIndex = array_column($data, 'event_count');
                        $myArray[] = array('event_count' => $arrayIndex[$i], 'date' => $value);
                        $i++;
                    } else {
                        $myArray[] = array('event_count' => '0', 'date' => $value);
                    }
                }
                $return['result'] = $myArray;
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getChartAcknowledgmentStatus() {
        $permalink = array(2);
        $suppress = array();
        $this->db->select('event_id');
        $this->db->from('events');
        $this->db->where('is_suppressed', 'Y');
        $this->db->where_not_in('severity_id', $permalink);
        $queryTemp = $this->db->get();
        if ($this->accessLevelClient['status'] == 'true') {
            $this->db->where_in('client_id', $this->accessLevelClient['resultSet']);
        }
        //echo $this->db->last_query();
        if ($queryTemp->num_rows() > 0) {
            $suppress = $queryTemp->result_array();
        }

        $countSuppress = count($suppress);
        $notInTT = array();
        if (!empty($suppress)) {
            $notInTT = array_column($suppress, 'event_id');
        }

        $this->db->select('event_id');
        $this->db->from('events');
        $this->db->where('ticket_id !=', '0');
        $this->db->where_not_in('severity_id', $permalink);
        if (!empty($notInTT)) {
            $this->db->where_not_in('event_id', $notInTT);
        }
        if ($this->accessLevelClient['status'] == 'true') {
            $this->db->where_in('client_id', $this->accessLevelClient['resultSet']);
        }
        $queryTicket = $this->db->get();
        //echo $this->db->last_query();
        if ($queryTicket->num_rows() > 0) {
            $countTicket = $queryTicket->num_rows();
            $dataTicket = $queryTicket->result_array();
            $notInNonAck = array_column($dataTicket, 'event_id');
        } else {
            $countTicket = 0;
        }

        $this->db->select('event_id');
        $this->db->from('events');
        $this->db->where('ticket_id', '0');
        $this->db->where_not_in('severity_id', $permalink);
        if (!empty($notInTT)) {
            $this->db->where_not_in('event_id', $notInTT);
        }
        if ($this->accessLevelClient['status'] == 'true') {
            $this->db->where_in('client_id', $this->accessLevelClient['resultSet']);
        }
        $queryNonACK = $this->db->get();
        if ($queryNonACK->num_rows() > 0) {
            $countNonACK = $queryNonACK->num_rows();
        } else {
            $countNonACK = 0;
        }
        $array = array(
            0 => array(
                'type' => 'Acknowledged',
                'counts' => $countTicket
            ),
            1 => array(
                'type' => 'Suppressed',
                'counts' => $countSuppress
            ),
            2 => array(
                'type' => 'Unacknowledged',
                'counts' => $countNonACK
            )
        );
        //pr($array);exit;
        $return['result'] = $array;
        $return['status'] = 'true';
        return $return;
    }

    public function getOpenEventsStatusLive() {
        $filter = "";
        $permalink = array(2);
        if ($this->accessLevelClient['status'] == 'true') {
            $x = implode(",", $this->accessLevelClient['resultSet']);
            $filter = " and evt.client_id in(" . $x . ")";
        }


        //$querySuppressNotFinised = "(SELECT sum(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(ack.event_suppress_starttime)) as duration FROM `eventedge_events` as `evt` JOIN `eventedge_event_acknowledgements` as `ack` ON `ack`.`event_id`=`evt`.`event_id` WHERE `evt`.`severity_id` = ev.severity_id AND `evt`.`event_end_time` = '0000-00-00 00:00:00' AND `ack`.`is_history` = 'N' AND `ack`.`event_suppress_endtime` >= now()  AND `ack`.`ack_type` = 'T' $filter) as suppressNotFinisedTime";
        $querySuppressFinised = "(SELECT sum(ack.duration) as duration FROM `eventedge_events` as `evt` JOIN `eventedge_event_acknowledgements` as `ack` ON `ack`.`event_id`=`evt`.`event_id` WHERE `evt`.`severity_id` = ev.severity_id AND `evt`.`event_end_time` = '0000-00-00 00:00:00' AND `ack`.`is_history` = 'N' AND `ack`.`event_suppress_endtime` <= now() AND `ack`.`ack_type` = 'T' $filter) as suppressFinisedTime";
        //$this->db->select("COUNT(ev.event_id) as cnt ,sev.id as severity_id,sev.severity, sum(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(ev.event_start_time)) as timev,$querySuppressNotFinised,$querySuppressFinised");
        $this->db->select("COUNT(ev.event_id) as cnt ,sev.id as severity_id,sev.severity, sum(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(ev.event_start_time)) as timev,$querySuppressFinised");
        $this->db->from('events as ev');
        $this->db->join('severities as sev', 'ev.severity_id=sev.id');
        $this->db->where_not_in('ev.severity_id', $permalink);
        $this->db->where('ev.event_end_time', '0000-00-00 00:00:00');
        $this->db->where('ev.is_suppressed', 'N');
        if ($this->accessLevelClient['status'] == 'true') {
            $this->db->where_in('ev.client_id', $this->accessLevelClient['resultSet']);
        }
        $this->db->group_by("ev.severity_id");
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $events = array();
        if ($query->num_rows() > 0) {
            $array = $query->result_array();
            for ($i = 0; $i < count($array); $i++) {
                //$FinalTime = $array[$i]['timev'] - ($array[$i]['suppressNotFinisedTime'] + $array[$i]['suppressFinisedTime']);
                $FinalTime = $array[$i]['timev'] - $array[$i]['suppressFinisedTime'];
                $seconds = $FinalTime / $array[$i]['cnt'];
                $mins = floor($seconds / 60);
                $secs = floor($seconds % 60);
                $resultFinalTime = $mins . ":" . $secs;
                $events[$array[$i]['severity']][] = $array[$i]['cnt'];
                $events[$array[$i]['severity']][] = $resultFinalTime;
            }
        }
//        pr($events);exit;
        return $events;
    }

    public function getTasksCount() {
        $filter = "";
        $permalink = array(2);
        if ($this->accessLevelClient['status'] == 'true') {
            $x = implode(",", $this->accessLevelClient['resultSet']);
            $filter = " and evt.client_id in(" . $x . ")";
        }


        $rs = $this->db->query("SELECT `t`.`ticket_id`, `t`.`incident_id`, `t`.`created_on`, `t`.`requestor`, `t`.`subject`, `t`.`last_updated_on`, `cl`.`client_title` as `client`, CONCAT_WS(' ', `uc`.`first_name`, uc.last_name) as user, CONCAT_WS(' ', `ua`.`first_name`, ua.last_name) as assigned_to, `ts`.`status` as `state`, `p`.`name` as `priority`, `s`.`title` as `severity`, `tt`.`name` as `ticket_type`, `dc`.`category` as `category`, `sr`.`service_description` as `sub_category`
		FROM `eventedge_tickets` `t`
		JOIN `eventedge_client` as `cl` ON `cl`.`id` = `t`.`client_id`
		JOIN `eventedge_users` as `uc` ON `uc`.`id` = `t`.`created_by`
		LEFT JOIN `eventedge_users` as `ua` ON `ua`.`id` = `t`.`assigned_to`
		JOIN `eventedge_ticket_status` as `ts` ON `ts`.`status_code` = `t`.`status_code`
		LEFT JOIN `eventedge_ticket_urgency` as `p` ON `p`.`urgency_id` = `t`.`priority`
		LEFT JOIN `eventedge_events` as `e` ON `e`.`event_id` = `t`.`event_id`
		LEFT JOIN `eventedge_devices` as `d` ON `d`.`device_id` = `e`.`device_id`
		LEFT JOIN `eventedge_skill_set` as `s` ON `s`.`skill_id` = `d`.`priority_id`
		LEFT JOIN `eventedge_ticket_types` as `tt` ON `tt`.`ticket_type_id` = `t`.`ticket_type_id`
		LEFT JOIN `eventedge_device_categories` as `dc` ON `dc`.`id` = `d`.`device_category_id`
		LEFT JOIN `eventedge_services` as `sr` ON `sr`.`service_id` = `e`.`service_id`
		WHERE `t`.`ticket_type_id` = 2
		AND `ts`.`status_code` NOT IN(5, 7)
		AND `t`.`assigned_to` in(" . $this->userId . ")");
        echo $this->db->last_query();
        $array = $rs->result_array();

        return count($array);
    }

    public function getBreachedEventsLive() {
        $events = array();
        $permalink = array(2);
        $this->db->select("COUNT(ev.event_id) as cnt ,sev.id as severity_id,sev.severity");
        $this->db->from('events as ev');
        $this->db->join('severities as sev', 'ev.severity_id=sev.id');
        $this->db->where_not_in('ev.severity_id', $permalink);
        $this->db->where('ev.event_end_time', '0000-00-00 00:00:00');
        $this->db->where('ev.is_suppressed', 'N');
        //$this->db->where('ev.severity_id', '1');
        if ($this->accessLevelClient['status'] == 'true') {
            $this->db->where_in('ev.client_id', $this->accessLevelClient['resultSet']);
        }
        $this->db->group_by("severity_id");
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $valueMain) {
                //pr($value);
                $shift_start_time = "(SELECT  `shift_start_time` FROM `eventedge_events_sla` WHERE `client_id` = ev.client_id AND `priority_id` = dev.priority_id) as shift_start_time";
                $shift_end_time = "(SELECT  `shift_end_time` FROM `eventedge_events_sla` WHERE `client_id` = ev.client_id AND `priority_id` = dev.priority_id) as shift_end_time";
                $weekdays = "(SELECT  `working_day` FROM `eventedge_events_sla` WHERE `client_id` = ev.client_id AND `priority_id` = dev.priority_id) as working_day";
                $holidays = array();
                $this->db->select("dev.priority_id,ev.client_id,ev.event_id,ev.event_start_time,ev.ticket_id, (case when (ev.ticket_id != 0) THEN tkt.created_on ELSE now() END) as ticketCreatedtime,$shift_start_time,$shift_end_time,$weekdays");
                $this->db->from('events as ev');
                $this->db->where_not_in('ev.severity_id', $permalink);
                $this->db->join('tickets as tkt', 'tkt.ticket_id=ev.ticket_id', 'left');
                $this->db->join('devices as dev', 'dev.device_id=ev.device_id');
                $this->db->where('ev.event_end_time', '0000-00-00 00:00:00');
                $this->db->where('ev.severity_id', $valueMain->severity_id);
                $this->db->where('ev.is_suppressed', 'N');
                if ($this->accessLevelClient['status'] == 'true') {
                    $this->db->where_in('ev.client_id', $this->accessLevelClient['resultSet']);
                }
                $querySLA = $this->db->get();
//                echo $this->db->last_query();
                if ($querySLA->num_rows() > 0) {
                    $slatime = "";
                    $myarray = array();
                    foreach ($querySLA->result() as $value) {
                        //pr($value);
                        $eventStartDay = strtotime(date('Y-m-d', strtotime($value->event_start_time)));
                        $eventStartTime = HRToSec(date('H:i', strtotime($value->event_start_time)));
                        $holidays = array();
                        $eventEndDay = strtotime(date('Y-m-d', strtotime($value->ticketCreatedtime)));
                        $eventEndTime = HRToSec(date('H:i', strtotime($value->ticketCreatedtime)));
                        $weekdays = explode(",", $value->working_day);
                        $shift_start_time = HRToSec(date('H:i', strtotime($value->shift_start_time)));
                        $shift_end_time = HRToSec(date('H:i', strtotime($value->shift_end_time)));
                        $slatime += $this->calculateSLA($eventStartDay, $eventStartTime, $eventEndDay, $eventEndTime, $weekdays, $shift_start_time, $shift_end_time, $holidays);
                    }
                    $seconds = $slatime / $valueMain->cnt;
                    $mins = floor($seconds / 60);
                    $secs = floor($seconds % 60);
                    $resultFinalTime = $mins . ":" . $secs;
                    $valueMain->myval = $resultFinalTime;
                }

                $array[] = $valueMain;
            }
            $events = array();
            for ($i = 0; $i < count($array); $i++) {
                $events[$array[$i]->severity][] = $array[$i]->cnt;
                $events[$array[$i]->severity][] = $array[$i]->myval;
            }
        }
        //pr($events);exit;
        return $events;
    }

    function calculateSLA($eventStartDay, $eventStartTime, $eventEndDay, $eventEndTime, $weekdays = array(), $slaStartTime, $slaEndTime, $holidays = array()) {

        /*
          This function calculates SLA  between two given date and time values and
          by excluding on-business hours, non-working days and holidays
         */
        $sla = 0;

        /* checks if an event day is in holiday list or not */
        $isHoliday = false;
        $hDayCount = sizeof($holidays);
        if ($hDayCount > 0) {

            $isHoliday = $this->isHoliday($holidays, $eventEndDay);
        }

        if ($eventEndDay != $eventStartDay) {
            /*
              If event start and End date are not on same, Loops through the days and calculates
              SLA by excluding non business hours, non working days and holidays.
             */

            //echo "<br/> SLA for the day '" . date('Y-m-d', $eventStartDay) ."'\t Day of the week : ". date('N', $eventStartDay); // for debug
            if ((in_array(date('N', $eventStartDay), $weekdays)) && (!($isHoliday))) {

                if (($eventStartTime >= $slaStartTime) && ($eventStartTime <= $slaEndTime)) {

                    $sla = $slaEndTime - $eventStartTime;
                    //echo "<br/>SLA1 :" . $sla ; // for debug
                } else if ($eventStartTime < $slaStartTime) {

                    $sla = $slaEndTime - $slaStartTime;
                    //echo "<br/>SLA2 :" . $sla ; // for debug
                }
            }

            $tempEventDay = $eventStartDay + 86400;
            do {

                // echo "<br/> SLA for the day '" . date('Y-m-d', $tempEventDay) ."'\t Day of the week : ". date('N', $tempEventDay); // for debug

                /* checks if an event day is in holiday list or not */
                $isHoliday = false;
                if ($hDayCount > 0) {

                    $isHoliday = $this->isHoliday($holidays, $tempEventDay);
                }

                if ((in_array(date('N', $tempEventDay), $weekdays)) && (!($isHoliday))) {

                    if ($eventEndDay == $tempEventDay) {

                        if (($eventEndTime >= $slaStartTime) && ($eventEndTime <= $slaEndTime)) {

                            $sla += $eventEndTime - $slaStartTime;
                            // echo "<br/> SLA3 :" . $sla ; // for debug
                        } elseif (($eventEndTime >= $slaStartTime) && ($eventEndTime > $slaEndTime)) {

                            $sla += $slaEndTime - $slaStartTime;
                            // echo "<br/> SLA4 :" . $sla ; // for debug
                        }
                    } else {

                        $sla += $slaEndTime - $slaStartTime;
                        // echo "<br/> SLA5 :" . $sla ; // for debug
                    }
                }

                $tempEventDay += 86400;
            } while ($tempEventDay <= $eventEndDay);
        } else {

            /*
              If event start and End date are on same day, calculates	SLA by excluding non business hours, non working days and holidays.
             */

            //echo "<br/> SLA for the day '" . date('Y-m-d', $eventStartDay) ."'\t Day of the week : ". date('N', $eventStartDay); // for debug
            if ((in_array(date('N', $eventStartDay), $weekdays)) && (!($isHoliday))) {

                if (($eventStartTime >= $slaStartTime) && ($eventStartTime <= $slaEndTime)) {

                    If ($eventEndTime < $slaEndTime) {

                        $sla = $eventEndTime - $eventStartTime;
                        // echo "<br/>SLA6 :" . $sla ; // for debug
                    } else {

                        $sla = $slaEndTime - $eventStartTime;
                        // echo "<br/>SLA7 :" . $sla ; // for debug
                    }
                } elseif ($eventStartTime < $slaStartTime) {

                    If ($eventEndTime > $slaStartTime) {

                        If ($eventEndTime <= $slaEndTime) {

                            $sla = $eventEndTime - $slaStartTime;
                            // echo "<br/>SLA8 :" . $sla ; // for debug
                        } else {

                            $sla = $slaEndTime - $slaStartTime;
                            // echo "<br/>SLA9 :" . $sla ; // for debug
                        }
                    }
                }
            }
        }

        return $sla;
    }

    function isHoliday($holidays, $DueDate) {

        /* Checks if a given date is holiday list or not */

        $flag_Holiday = false;
        $tempDay = strtotime(date('Y-m-d', $DueDate));

        if (in_array($tempDay, $holidays)) {

            //echo "<br/> Excluding holiday :" . date('Y-m-d H:i', $tempDay) ; // for debug
            $flag_Holiday = true;
        }
        return $flag_Holiday;
    }

}

?>