<style>
    a:hover{text-decoration: none;}
    .remove-pad-right{padding-right:0;}
    .remove-pad-left{padding-left: 0;}
    .dashboard-stat2{padding: 6px 9px 5px;}
    .dashboard-stat2 .display .number small{font-size: 14px;font-weight: 800;text-transform: capitalize;}
    .dashboard-stat2{margin-bottom: 5px; padding:7px 9px 5.5px;}
    .dashboard-stat2 .display{margin-bottom: 4.5px;}
    .dashboard-stat{margin-bottom: 6px;}
    .amcharts-title{color:#000; fill:black;}

    .dashboard-stat.dashboard-stat-v2 .visual {
        padding-top: 0px;
        margin-bottom: 0;
    }
    .dashboard-stat .details{padding-right: 0;}
    .dashboard-stat .details .number{font-size: 22px;padding-top:4px; line-height: 25px;}
    .dashboard-stat .details .desc{font-size: 13px;}
    .dashboard-stat.yellow-gold {background-color: #E87E04;}
    .dashboard-stat.asign{background-color: #25ad5f;}
    .dashboard-stat.green.schedule{background-color: #b69059 !important;}
    .dashboard-stat.blue-cham .details .number, .dashboard-stat.blue-cham .details .desc{color: #fff;}    
    .dashboard-stat.blue-cham .visual>i { opacity: .1; filter: alpha(opacity=10);}    
    .dashboard-stat.blue-cham {background-color: #2C3E50;}
    .dashboard-stat .visual{height: 63px;}
    .dashboard-stat .details{position: relative;}
    .dashboard-stat .details .desc{position: absolute; left: 28px; font-weight:600;}
    .dashboard-stat.green{background-color:#24a286;}

    .dashboard-stat2 hr{margin: 6px 0 0 0;}
    .dashboard-stat2 h3 { margin-top: 0px; margin-bottom: 2px; font-size:31px; font-weight:600;}
    .dashboard-stat2 .progress-info .status{text-transform: capitalize;}
    .dashboard-stat2 .display.green small{color:#2ab4c0;}
    .dashboard-stat2 .display.red  small{color:#f36a5a;}
    .dashboard-stat2 .display.blue  small{color:#5c9bd1;}
    .dashboard-stat2 .display.green h3{color:#2ab4c0 !important;position: relative; z-index: 2; }
    .dashboard-stat2 .display.red  h3{color:#f36a5a !important; position: relative; z-index: 2; }
    .dashboard-stat2 .display.blue  h3{color:#5c9bd1 !important;position: relative; z-index: 2; }
    .mttr_blocks .dashboard-stat2{overflow:hidden; position:relative;    display: block;}
    .mttr_blocks .dashboard-stat2 i{position: absolute; font-size: 100px !important;     color: rgba(0, 0, 0, 0.05) !important; top: 7px; right: -16px;}
    .table-scrollable{margin:0 !important;}
    .events_Live_status .table>tbody>tr>td{padding: 7.9px 6px 9px 6px;}
    .events_Live_status .table>tbody>tr>td a{font-weight:600;}
    .events_Live_status .table>thead>tr>th{font-weight:600;}


    .events_Live_status .table-advance td.highlight div.Critical { border-left: 2px solid #e7505a; }
    .events_Live_status .table-advance td.highlight div.Major { border-left: 2px solid #8e44ad; }
    .events_Live_status .table-advance td.highlight div.Warning { border-left: 2px solid #e87e04; }
    .events_Live_status .table-advance td.highlight div.Minor { border-left: 2px solid #24a286; }
    .events_Live_status .table-advance td.highlight div.Unknown { border-left: 2px solid #3598dc; }

    .span-padd{padding:0px 4px; color:#fff; min-width:30px; text-align:center; display: inline-block; position: relative; top: 1px;}
    .critical-span-bg{background:#e7505a;}
    .major-span-bg{background:#8e44ad;}
    .warning-span-bg{background:#e87e04;}
    .minor-span-bg{background:#24a286;}
    .unknown-span-bg{background:#3598dc;}

    .table-advance div.Critical, .table-advance div.Major, .table-advance div.Warning, .table-advance div.Minor, .table-advance div.Unknown {
        position: absolute; margin-top: -5px; float: left; width: 2px; height: 30px; margin-right: 20px!important; }
    .portlet { margin-bottom: 6px;}
    .portlet.light>.portlet-title { min-height: 33px; margin-bottom: 0;}

    .dashboard-stat .visual { height: 51.8px;}
    .dashboard-stat .details .desc{bottom:auto;}
    .portlet.light>.portlet-title>.caption>.caption-subject {
        font-size: 14px;
        text-transform: capitalize !important;
        font-weight: 600 !important;
    }

    .portlet.light>.portlet-title>.nav-tabs>li>a { padding: 0 9px 9px;top: 5px;}
    .tab-content{height:auto !important;}
    .portlet.light { padding: 5px 10px 10px 10px;}

    #mttr .amcharts-legend-marker, #mtrs .amcharts-legend-marker{d: path('M -4.5 4.5 L 4.5 4.5 L 4.5 -4.5 L -4.5 -4.5 Z');}

    .page-title {
        background: rgba(204, 204, 204, 0.22);
        padding: 6px 9px;
        margin: 0 0 10px;
    }.page-title{font-size: 14px; font-weight: 600 !important; letter-spacing:-0.5px !important ;}	
    .table-advance tr td.highlight:first-child a {margin-left: 6px;}
    .remove-pad-right .portlet.light>.portlet-title>.actions, .remove-pad-left .portlet.light>.portlet-title>.actions{display:none;}
</style>
<div class="full-height-content full-height-content-scrollable">
    <h1 class="page-title"> Dashboard</h1>
    <div style="position: absolute; top: 8px; right:8px;font-size: 12px;">
        <div class="page-toolbar form-inline"  style="padding-top:0;">
            Period : 
            <select class="form-control" style="height: 23px; padding: 0; font-size: 11px;">
                <option>Till Date</option>               
            </select>
        </div>
    </div>
    <div class="full-height-content-body">
        <div class="row">

            <div class="col-md-10 parent remove-pad-right">

                <div class="row">
                    <div class="col-md-4  remove-pad-right">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase font-dark">Top 5 Priorities</span>														
                                </div>
                            </div>
                            <div class="portlet-body events_Live_status">
                                <div class="table-scrollable">
                                    <table class="Open-Events-Status-Live table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <i class="fa fa-bell" aria-hidden="true"></i> Tickets# </th>
                                                <th class="text-center">
                                                    <i class="fa fa-check-circle" aria-hidden="true"></i> Sev </th>
                                                <th class="text-center">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i> Time Left <small>(mm:ss)</small> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="highlight">
                                                    <div class="success"></div>
                                                    <a href="#"> INC-000000000000009 </a>
                                                </td>

                                                <td class="text-center">
                                                    Sev1</td>
                                                <td class="text-center">56:32 </td>
                                            </tr>

                                            <tr>
                                                <td class="highlight">
                                                    <div class="success"></div>
                                                    <a href="#"> INC-000000000000009 </a>
                                                </td>

                                                <td class="text-center">
                                                    Sev2</td>
                                                <td class="text-center">56:32 </td>
                                            </tr>
                                            <tr>
                                                <td class="highlight">
                                                    <div class="success"></div>
                                                    <a href="#"> INC-000000000000009 </a>
                                                </td>

                                                <td class="text-center">
                                                    Sev3</td>
                                                <td class="text-center">56:32 </td>
                                            </tr>

                                            <tr>
                                                <td class="highlight">
                                                    <div class="success"></div>
                                                    <a href="#"> INC-000000000000009 </a>
                                                </td>

                                                <td class="text-center">
                                                    Sev4</td>
                                                <td class="text-center">56:32 </td>
                                            </tr>
                                            <tr>
                                                <td class="highlight">
                                                    <div class="success"></div>
                                                    <a href="#"> INC-000000000000009 </a>
                                                </td>

                                                <td class="text-center">
                                                    Sev4</td>
                                                <td class="text-center">56:32 </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="col-md-4 remove-pad-right">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase font-dark">Open Cases: Live</span>														
                                </div>
                            </div>
                            <div class="portlet-body events_Live_status">
                                <div class="table-scrollable">
                                    <table class="Open-Events-Status-Live table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <i class="fa fa-bell" aria-hidden="true"></i> Incidents </th>
                                                <th class="text-center">
                                                    <i class="fa fa-check-circle" aria-hidden="true"></i> Count </th>
                                                <th class="text-center">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i> Age <small>(hh:mm)</small> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="highlight">
                                                    <div class="success"></div>
                                                    <a href="#"> Critical </a>
                                                </td>

                                                <td class="text-center">
                                                    <a href="#">
                                                        <span class="span-padd critical-span-bg">7</span>  </a></td>
                                                <td class="text-center">56:32 </td>
                                            </tr>

                                            <tr>
                                                <td class="highlight">
                                                    <div class="Major"> </div>
                                                    <a href="#"> Major </a>
                                                </td>
                                                <td class="text-center">  
                                                    <a href="#"> 
                                                        <span class="span-padd major-span-bg">0</span>  
                                                    </a>
                                                </td>
                                                <td class="text-center">00:00  </td>
                                            </tr>
                                            <tr>
                                                <td class="highlight">
                                                    <div class="Warning"> </div>
                                                    <a href="#"> Warning </a>
                                                </td>
                                                <td class="text-center"> 
                                                    <a href="#">
                                                        <span class="span-padd warning-span-bg">3</span>
                                                    </a>
                                                </td>
                                                <td class="text-center"> 44:00 </td>
                                            </tr>

                                            <tr>
                                                <td class="highlight">
                                                    <div class="Minor"> </div>
                                                    <a href="#"> Minor </a>
                                                </td>
                                                <td class="text-center">  
                                                    <a href="#">
                                                        <span class="span-padd minor-span-bg">0 </span>
                                                    </a>
                                                </td>
                                                <td class="text-center">00:00  </td>
                                            </tr>
                                            <tr>
                                                <td class="highlight">
                                                    <div class="Unknown"> </div>
                                                    <a href="#"> Tasks </a>
                                                </td>
                                                <td class="text-center"> 
                                                    <a href="#">
                                                        <span class="span-padd unknown-span-bg">0 </span>
                                                    </a>
                                                </td>
                                                <td class="text-center">52:00   </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>  

                    <div class="col-md-4">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase font-dark">Breached Cases: Live</span>														
                                </div>
                            </div>
                            <div class="portlet-body events_Live_status">
                                <div class="table-scrollable">
                                    <table class="Open-Events-Status-Live table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <i class="fa fa-bell" aria-hidden="true"></i> Incidents </th>
                                                <th class="text-center">
                                                    <i class="fa fa-check-circle" aria-hidden="true"></i> Count </th>
                                                <th class="text-center">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i> Age <small>(hh:mm)</small> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="highlight">
                                                    <div class="success"></div>
                                                    <a href="#"> Critical </a>
                                                </td>

                                                <td class="text-center">
                                                    <a href="#">
                                                        <span class="span-padd critical-span-bg">7</span>  </a></td>
                                                <td class="text-center">56:32 </td>
                                            </tr>

                                            <tr>
                                                <td class="highlight">
                                                    <div class="Major"> </div>
                                                    <a href="#"> Major </a>
                                                </td>
                                                <td class="text-center">  
                                                    <a href="#"> 
                                                        <span class="span-padd major-span-bg">0</span>  
                                                    </a>
                                                </td>
                                                <td class="text-center">00:00  </td>
                                            </tr>
                                            <tr>
                                                <td class="highlight">
                                                    <div class="Warning"> </div>
                                                    <a href="#"> Warning </a>
                                                </td>
                                                <td class="text-center"> 
                                                    <a href="#">
                                                        <span class="span-padd warning-span-bg">3</span>
                                                    </a>
                                                </td>
                                                <td class="text-center"> 44:00 </td>
                                            </tr>

                                            <tr>
                                                <td class="highlight">
                                                    <div class="Minor"> </div>
                                                    <a href="#"> Minor </a>
                                                </td>
                                                <td class="text-center">  
                                                    <a href="#">
                                                        <span class="span-padd minor-span-bg">0 </span>
                                                    </a>
                                                </td>
                                                <td class="text-center">00:00  </td>
                                            </tr>
                                            <tr>
                                                <td class="highlight">
                                                    <div class="Unknown"> </div>
                                                    <a href="#"> Tasks </a>
                                                </td>
                                                <td class="text-center"> 
                                                    <a href="#">
                                                        <span class="span-padd unknown-span-bg">0 </span>
                                                    </a>
                                                </td>
                                                <td class="text-center">52:00   </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 remove-pad-right">
                        <div class="portlet light">
                            <div class="portlet-title tabbable-line">
                                <div class="caption">
                                    <i class=" icon-social-twitter font-dark hide"></i>
                                    <span class="caption-subject font-dark bold uppercase">Ageing</span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li>
                                        <a href="#mttr" data-toggle="tab" aria-expanded="true"> Last Updated Bucket </a>
                                    </li >
                                    <li  class="active">
                                        <a href="#mtrs" data-toggle="tab" aria-expanded="false"> Age Bucket </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <div class="tab-pane " id="mttr">
                                        <div class="row">
                                            <div class="col-md-12 remove-pad-left">

                                                <div id="last-updated-bucket" style="height:201px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane active" id="mtrs">
                                        <div class="col-md-12 remove-pad-left">
                                            <div id="age-bucket-records" style="height:201px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                        <div class="portlet light" style="padding-top: 5px;">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase font-dark">Events Trend <span style="display: none" id="chartMothlyReportHour">/ Hour</span></span>
                                </div>
                            </div>
                            <div class="portlet-body  events_Live_status" style="position:relative;">
                                <div class="row">
                                    <div id="chartMothlyReport" style="height:201px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <div class="col-md-2 parent remove-pad-left">
                <div class="row">
                    <div class="col-md-12 remove-pad-left">
                        <a class="dashboard-stat dashboard-stat-v2 red" style="background:#5e5eab;" href="<?php echo base_url() . "event/ticket?status_id=" . base64_encode(0); ?>">
                            <div class="visual">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number"> 
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets[0])) ? $getTickets[0] : "0" ?>"><?php echo (isset($getTickets[0])) ? $getTickets[0] : "0" ?></span></div>
                                <div class="desc"> New </div>
                            </div>
                        </a>
                        <a class="dashboard-stat dashboard-stat-v2 yellow-gold asign" href="<?php echo base_url() . "event/ticket?status_id=" . base64_encode(1); ?>">
                            <div class="visual">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets[1])) ? $getTickets[1] : "0" ?>"><?php echo (isset($getTickets[1])) ? $getTickets[1] : "0" ?></span></div>
                                <div class="desc"> Assigned </div>
                            </div>
                        </a>
                        <a class="dashboard-stat dashboard-stat-v2 yellow-gold" href="<?php echo base_url() . "event/ticket?status_id=" . base64_encode(2); ?>">
                            <div class="visual">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets[2])) ? $getTickets[2] : "0" ?>"><?php echo (isset($getTickets[2])) ? $getTickets[2] : "0" ?></span></div>
                                <div class="desc"> In-Progress </div>
                            </div>
                        </a>
                        <a class="dashboard-stat dashboard-stat-v2 red" href="<?php echo base_url() . "event/ticket?status_id=" . base64_encode(3); ?>">
                            <div class="visual">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number"> 
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets[3])) ? $getTickets[3] : "0" ?>"><?php echo (isset($getTickets[3])) ? $getTickets[3] : "0" ?></span></div>
                                <div class="desc"> Pending Customer </div>
                            </div>
                        </a>
                        <a class="dashboard-stat dashboard-stat-v2 purple" href="<?php echo base_url() . "event/ticket?status_id=" . base64_encode(8); ?>">
                            <div class="visual">
                                <i class="fa fa-bar-chart-o"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets[8])) ? $getTickets[8] : "0" ?>"><?php echo (isset($getTickets[8])) ? $getTickets[8] : "0" ?></span>
                                </div>
                                <div class="desc"> Pending Vendor </div>
                            </div>
                        </a>
                        <a class="dashboard-stat dashboard-stat-v2 green" href="<?php echo base_url() . "event/ticket?status_id=" . base64_encode(9); ?>">
                            <div class="visual">
                                <i class="fa fa-download" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets[9])) ? $getTickets[9] : "0" ?>"><?php echo (isset($getTickets[9])) ? $getTickets[9] : "0" ?></span>
                                </div>
                                <div class="desc"> Pending Work Order </div>
                            </div>
                        </a>
                        <a class="dashboard-stat dashboard-stat-v2 green schedule" href="<?php echo base_url() . "event/ticket?status_id=" . base64_encode(4); ?>">
                            <div class="visual">
                                <i class="fa fa-download" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets[4])) ? $getTickets[4] : "0" ?>"><?php echo (isset($getTickets[4])) ? $getTickets[4] : "0" ?></span>
                                </div>
                                <div class="desc"> Schedule </div>
                            </div>
                        </a>
                        <a class="dashboard-stat dashboard-stat-v2 yellow-gold" style="background:#353535;" href="<?php echo base_url() . "event/ticket?status_id=" . base64_encode('11,12'); ?>">
                            <div class="visual">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <?php
                                    $MonitoringHold = 0;
                                    if ((isset($getTickets[11])) || (isset($getTickets[12]))) {
                                        $MonitoringHold = (isset($getTickets[11]) ? $getTickets[11] : 0) + (isset($getTickets[12]) ? $getTickets[12] : 0);
                                    }
                                    ?>
                                    <span data-counter="counterup" data-value="<?php echo $MonitoringHold ?>"><?php echo $MonitoringHold ?></span></div>
                                <div class="desc"> Monitoring/Hold </div>
                            </div>
                        </a>
                        <a class="dashboard-stat dashboard-stat-v2 blue" href="<?php echo base_url() . "event/ticket?status_id=" . base64_encode('5,7'); ?>">
                            <div class="visual">
                                <i class="fa fa-bar-chart-o"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <?php
                                    $ResolvedClosed = 0;
                                    if ((isset($getTickets[5])) || (isset($getTickets[7]))) {
                                        $ResolvedClosed = (isset($getTickets[5]) ? $getTickets[5] : 0) + (isset($getTickets[7]) ? $getTickets[7] : 0);
                                    }
                                    ?>
                                    <span data-counter="counterup" data-value="
                                          <?php echo $ResolvedClosed; ?>">  <?php echo $ResolvedClosed; ?></span></div>
                                <div class="desc"> Resolved/Closed </div>
                            </div>
                        </a>

                    </div>
                </div>
            </div>

            <div class="right_block col-md-3" style="display:none;">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">Revenue</span>
                        </div>

                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-cloud-upload"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-wrench"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-trash"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="close fa fa-close"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script src="<?php echo base_url() . "public/" ?>js/form/form_dashboardincedent.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        var chart1 = AmCharts.makeChart("mttr-ser1", {
            "type": "pie",
            "labelText": "[[percents]]%",
            "theme": "light",
            "color": '#fff',
            "autoMargins": false,
            "innerRadius": 45,
            "marginTop": 5,
            "marginBottom": 5,
            "marginLeft": 0,
            "marginRight": 0,
            "pullOutRadius": 0,
            "addClassNames": true,
            "labelRadius": -15,
            "labelText": "[[value]]",
            "legend": {
                "position": "bottom",
                "marginRight": 0,
                "valueWidth": 0,
                "verticalGap": 0,
                "autoMargins": false

            },

            "dataProvider": [{"type": "Met", "count": 4, "color": "#7ec34e"}, {"type": "Missed", "count": 0, "color": "#ff6600"}],
            "titleField": "type",
            "valueField": "count",
            "colorField": "color",

            "exportConfig": {
                menuItems: [{
                        icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                        format: 'png'
                    }]
            }
        });

        AmCharts.checkEmptyData_mttrSer1 = function (chart) {

            // check if data is mepty
            if (0 == chart.dataProvider.length) {
                // add some bogus data
                var dp = {};

                dp[chart.titleField] = "";
                dp[chart.valueField] = 1;
                chart.dataProvider.push(dp)



                // disable slice labels
                chart.labelsEnabled = false;
                chart.legend = false;
                // add label to let users know the chart is empty          
                chart.addLabel('50%', '50%', '0 Incidents under Sev-1', 'middle', 12, 'black');
                // dim the whole chart
                chart.alpha = 0.3;
                chart.validateNow();
            }

        };

        AmCharts.checkEmptyData_mttrSer1(chart1);



        var chart2 = AmCharts.makeChart("mttr-ser2", {
            "type": "pie",
            "labelText": "[[percents]]%",
            "theme": "light",
            "color": '#fff',
            "autoMargins": false,
            "innerRadius": 45,
            "marginTop": 5,
            "marginBottom": 5,
            "marginLeft": 0,
            "marginRight": 0,
            "pullOutRadius": 0,
            "addClassNames": true,
            "labelRadius": -15,
            "labelText": "[[value]]",
            "legend": {
                "position": "bottom",
                "marginRight": 0,
                "valueWidth": 0,
                "verticalGap": 0,
                "autoMargins": false

            },

            "dataProvider": [{"type": "Met", "count": 667, "color": "#7ec34e"}, {"type": "Missed", "count": 18, "color": "#ff6600"}],
            "titleField": "type",
            "valueField": "count",
            "colorField": "color",
            "exportConfig": {
                menuItems: [{
                        icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                        format: 'png'
                    }]
            }
        });

        AmCharts.checkEmptyData_mttrSer2 = function (chart) {

            // check if data is mepty
            if (0 == chart.dataProvider.length) {
                // add some bogus data
                var dp = {};

                dp[chart.titleField] = "";
                dp[chart.valueField] = 1;
                chart.dataProvider.push(dp)



                // disable slice labels
                chart.labelsEnabled = false;
                chart.legend = false;
                // add label to let users know the chart is empty          
                chart.addLabel('50%', '50%', '0 Incidents under Sev-2', 'middle', 12, 'black');
                // dim the whole chart
                chart.alpha = 0.3;
                chart.validateNow();
            }

        };

        AmCharts.checkEmptyData_mttrSer2(chart2);

        var chart3 = AmCharts.makeChart("mttr-ser3", {
            "type": "pie",
            "labelText": "[[percents]]%",
            "theme": "light",
            "color": '#fff',
            "autoMargins": false,
            "innerRadius": 45,
            "marginTop": 5,
            "marginBottom": 5,
            "marginLeft": 0,
            "marginRight": 0,
            "pullOutRadius": 0,
            "addClassNames": true,
            "labelRadius": -15,
            "labelText": "[[value]]",
            "legend": {
                "position": "bottom",
                "marginRight": 0,
                "valueWidth": 0,
                "verticalGap": 0,
                "autoMargins": false

            },

            "dataProvider": [{"type": "Met", "count": 9, "color": "#7ec34e"}, {"type": "Missed", "count": 1, "color": "#ff6600"}],
            "titleField": "type",
            "valueField": "count",
            "colorField": "color",
            "exportConfig": {
                menuItems: [{
                        icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                        format: 'png'
                    }]
            }
        });

        AmCharts.checkEmptyData_mttrSer3 = function (chart) {

            // check if data is mepty
            if (0 == chart.dataProvider.length) {
                // add some bogus data
                var dp = {};

                dp[chart.titleField] = "";
                dp[chart.valueField] = 1;
                chart.dataProvider.push(dp)



                // disable slice labels
                chart.labelsEnabled = false;
                chart.legend = false;
                // add label to let users know the chart is empty          
                chart.addLabel('50%', '50%', '0 Incidents under Sev-3', 'middle', 12, 'black');
                // dim the whole chart
                chart.alpha = 0.3;
                chart.validateNow();
            }

        };

        AmCharts.checkEmptyData_mttrSer3(chart3);

        var chart4 = AmCharts.makeChart("mttr-ser4", {
            "type": "pie",
            "labelText": "[[percents]]%",
            "theme": "light",
            "color": '#fff',
            "autoMargins": false,
            "innerRadius": 45,
            "marginTop": 5,
            "marginBottom": 5,
            "marginLeft": 0,
            "marginRight": 0,
            "pullOutRadius": 0,
            "addClassNames": true,
            "labelRadius": -15,
            "labelText": "[[value]]",
            "legend": {
                "position": "bottom",
                "marginRight": 0,
                "valueWidth": 0,
                "verticalGap": 0,
                "autoMargins": false

            },

            "dataProvider": [{"type": "Met", "count": 179, "color": "#7ec34e"}, {"type": "Missed", "count": 12, "color": "#ff6600"}],
            "titleField": "type",
            "valueField": "count",
            "colorField": "color",
            "exportConfig": {
                menuItems: [{
                        icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                        format: 'png'
                    }]
            }
        });
        AmCharts.checkEmptyData_mttrSer4 = function (chart) {

            // check if data is mepty
            if (0 == chart.dataProvider.length) {
                // add some bogus data
                var dp = {};

                dp[chart.titleField] = "";
                dp[chart.valueField] = 1;
                chart.dataProvider.push(dp)



                // disable slice labels
                chart.labelsEnabled = false;
                chart.legend = false;
                // add label to let users know the chart is empty          
                chart.addLabel('50%', '50%', '0 Task under Sev-4', 'middle', 12, 'black');
                // dim the whole chart
                chart.alpha = 0.3;
                chart.validateNow();
            }

        };

        AmCharts.checkEmptyData_mttrSer4(chart4);

        var chartMtrs1 = AmCharts.makeChart("mtrs-sev1", {
            "type": "pie",
            "labelText": "[[percents]]%",
            "theme": "light",
            "color": '#fff',
            "autoMargins": false,
            "innerRadius": 45,
            "marginTop": 5,
            "marginBottom": 5,
            "marginLeft": 0,
            "marginRight": 0,
            "pullOutRadius": 0,
            "addClassNames": true,
            "labelRadius": -15,
            "labelText": "[[value]]",
            "legend": {
                "position": "bottom",
                "marginRight": 0,
                "valueWidth": 0,
                "verticalGap": 0,
                "autoMargins": false

            },

            "dataProvider": [{"type": "Met", "count": 4, "color": "#7ec34e"}, {"type": "Missed", "count": 0, "color": "#ff6600"}],
            "valueField": "count",
            "titleField": "type",
            "colorField": "color",
            "exportConfig": {
                menuItems: [{
                        icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                        format: 'png'
                    }]
            }
        });

        AmCharts.checkEmptyData_mtrsSer1 = function (chart) {

            // check if data is mepty
            if (0 == chart.dataProvider.length) {
                // add some bogus data
                var dp = {};

                dp[chart.titleField] = "";
                dp[chart.valueField] = 1;
                chart.dataProvider.push(dp)



                // disable slice labels
                chart.labelsEnabled = false;
                chart.legend = false;
                // add label to let users know the chart is empty          
                chart.addLabel('50%', '50%', '0 Incidents under Sev-1', 'middle', 12, 'black');
                // dim the whole chart
                chart.alpha = 0.3;
                chart.validateNow();
            }

        };

        AmCharts.checkEmptyData_mtrsSer1(chartMtrs1);


        var chartMtrs2 = AmCharts.makeChart("mtrs-sev2", {
            "type": "pie",
            "labelText": "[[percents]]%",
            "theme": "light",
            "color": '#fff',
            "autoMargins": false,
            "innerRadius": 45,
            "marginTop": 5,
            "marginBottom": 5,
            "marginLeft": 0,
            "marginRight": 0,
            "pullOutRadius": 0,
            "addClassNames": true,
            "labelRadius": -15,
            "labelText": "[[value]]",
            "legend": {
                "position": "bottom",
                "marginRight": 0,
                "valueWidth": 0,
                "verticalGap": 0,
                "autoMargins": false

            },

            "dataProvider": [{"type": "Met", "count": 634, "color": "#7ec34e"}, {"type": "Missed", "count": 51, "color": "#ff6600"}],
            "valueField": "count",
            "titleField": "type",
            "colorField": "color",
            "exportConfig": {
                menuItems: [{
                        icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                        format: 'png'
                    }]
            }
        });
        AmCharts.checkEmptyData_mtrsSer2 = function (chart) {

            // check if data is mepty
            if (0 == chart.dataProvider.length) {
                // add some bogus data
                var dp = {};

                dp[chart.titleField] = "";
                dp[chart.valueField] = 1;
                chart.dataProvider.push(dp)



                // disable slice labels
                chart.labelsEnabled = false;
                chart.legend = false;
                // add label to let users know the chart is empty          
                chart.addLabel('50%', '50%', '0 Incidents under Sev-2', 'middle', 12, 'black');
                // dim the whole chart
                chart.alpha = 0.3;
                chart.validateNow();
            }

        };

        AmCharts.checkEmptyData_mtrsSer2(chartMtrs2);


        var chartMtrs3 = AmCharts.makeChart("mtrs-sev3", {
            "type": "pie",
            "labelText": "[[percents]]%",
            "theme": "light",
            "color": '#fff',
            "autoMargins": false,
            "innerRadius": 45,
            "marginTop": 5,
            "marginBottom": 5,
            "marginLeft": 0,
            "marginRight": 0,
            "pullOutRadius": 0,
            "addClassNames": true,
            "labelRadius": -15,
            "labelText": "[[value]]",
            "legend": {
                "position": "bottom",
                "marginRight": 0,
                "valueWidth": 0,
                "verticalGap": 0,
                "autoMargins": false

            },

            "dataProvider": [{"type": "Met", "count": 9, "color": "#7ec34e"}, {"type": "Missed", "count": 1, "color": "#ff6600"}],
            "valueField": "count",
            "titleField": "type",
            "colorField": "color",
            "exportConfig": {
                menuItems: [{
                        icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                        format: 'png'
                    }]
            }
        });
        AmCharts.checkEmptyData_mtrsSer3 = function (chart) {

            // check if data is mepty
            if (0 == chart.dataProvider.length) {
                // add some bogus data
                var dp = {};

                dp[chart.titleField] = "";
                dp[chart.valueField] = 1;
                chart.dataProvider.push(dp)



                // disable slice labels
                chart.labelsEnabled = false;
                chart.legend = false;
                // add label to let users know the chart is empty          
                chart.addLabel('50%', '50%', '0 Incidents under Sev-3', 'middle', 12, 'black');
                // dim the whole chart
                chart.alpha = 0.3;
                chart.validateNow();
            }

        };

        AmCharts.checkEmptyData_mtrsSer3(chartMtrs3);


        var chartMtrs4 = AmCharts.makeChart("mtrs-sev4", {
            "type": "pie",
            "labelText": "[[percents]]%",
            "theme": "light",
            "color": '#fff',
            "autoMargins": false,
            "innerRadius": 45,
            "marginTop": 5,
            "marginBottom": 5,
            "marginLeft": 0,
            "marginRight": 0,
            "pullOutRadius": 0,
            "addClassNames": true,
            "labelRadius": -15,
            "labelText": "[[value]]",
            "legend": {
                "position": "bottom",
                "marginRight": 0,
                "valueWidth": 0,
                "verticalGap": 0,
                "autoMargins": false

            },

            "dataProvider": [{"type": "Met", "count": 183, "color": "#7ec34e"}, {"type": "Missed", "count": 8, "color": "#ff6600"}],
            "valueField": "count",
            "titleField": "type",
            "colorField": "color",
            "exportConfig": {
                menuItems: [{
                        icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                        format: 'png'
                    }]
            }
        });
        AmCharts.checkEmptyData_mtrsSer4 = function (chart) {

            // check if data is mepty
            if (0 == chart.dataProvider.length) {
                // add some bogus data
                var dp = {};
                dp[chart.titleField] = "";
                dp[chart.valueField] = 1;
                chart.dataProvider.push(dp)

                // disable slice labels
                chart.labelsEnabled = false;
                chart.legend = false;
                // add label to let users know the chart is empty          
                chart.addLabel('50%', '50%', '0 Task under Sev-4', 'middle', 12, 'black');
                // dim the whole chart
                chart.alpha = 0.3;
                chart.validateNow();
            }

        };

        AmCharts.checkEmptyData_mtrsSer4(chartMtrs4);



        var records = AmCharts.makeChart("age-bucket-records", {
            "type": "pie",
            "labelText": "[[percents]]",
            "theme": "light",
            "color": '#fff',
            "autoMargins": false,
            "innerRadius": 40,
            "marginTop": 0,
            "marginBottom": 0,
            "marginLeft": 0,
            "marginRight": 0,
            "pullOutRadius": 0,
            "addClassNames": true,
            "labelRadius": -25,
            "labelText": "[[value]]",
            "legend": {
                "position": "right",
                "marginRight": 0,
                "marginLeft": 0,
                "verticalGap": 0,
                "autoMargins": true
            },

            "dataProvider": <?php echo $countAgeingBucket; ?>,
            "valueField": "count",
            "titleField": "days",
            "exportConfig": {
                menuItems: [{
                        icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                        format: 'png'
                    }]
            },
            "listeners": [{
                    "event": "clickSlice",
                    "method": function (e) {
                        //alert("hiii");
                        console.log(e.dataItem.dataContext.flag);
                        //console.log(e.dataItem.dataContext.type);
                        var flag = e.dataItem.dataContext.flag;
                        window.location.href = "<?php echo base_url(); ?>event/ticket?ageingbucket="+flag;
                    }
                }]
        });

        AmCharts.checkEmptyData_ageing_records = function (chart) {

            // check if data is mepty
            if (0 == chart.dataProvider.length) {
                // add some bogus data
                var dp = {};

                dp[chart.titleField] = "";
                dp[chart.valueField] = 1;
                chart.dataProvider.push(dp)
                // disable slice labels
                chart.labelsEnabled = false;
                chart.legend = false;
                // add label to let users know the chart is empty          
                chart.addLabel('50%', '50%', 'No Data Found', 'middle', 15, 'black');
                // dim the whole chart
                chart.alpha = 0.3;
                chart.validateNow();
            }

        };

        AmCharts.checkEmptyData_ageing_records(records);



        var records = AmCharts.makeChart("last-updated-bucket", {
            "type": "pie",
            "labelText": "[[percents]]",
            "theme": "light",
            "color": '#fff',
            "autoMargins": false,
            "innerRadius": 40,
            "marginTop": 0,
            "marginBottom": 0,
            "marginLeft": 0,
            "marginRight": 0,
            "pullOutRadius": 0,
            "addClassNames": true,
            "labelRadius": -25,
            "labelText": "[[value]]",
            "legend": {
                "position": "right",
                "marginRight": 10,
                "verticalGap": 0,
                "autoMargins": true
            },

            "dataProvider": <?php echo $countLastUpdatedBucket; ?>,
            "valueField": "count",
            "titleField": "days",
            "exportConfig": {
                menuItems: [{
                        icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                        format: 'png'
                    }]
            },
            "listeners": [{
                    "event": "clickSlice",
                    "method": function (e) {
                        //alert("hiii");
                        console.log(e.dataItem.dataContext.flag);
                        //console.log(e.dataItem.dataContext.type);                        
                        var flag = e.dataItem.dataContext.flag;
                        window.location.href = "<?php echo base_url(); ?>event/ticket?lastupdatedbucket="+flag;

                    }
                }]
        });
    });
</script>

