<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//comit
class Reports extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->userId = $this->session->userdata('id');
        $this->client_id = $this->session->userdata('client_id');
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->client_id = $this->session->userdata('client_id');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->load->library('form_validation');
        $this->load->model("Model_reports");
        $this->load->model('event/Model_event');
    }

    public function exportexcel() {
        // pr($_SESSION);exit;
        $sql = $_SESSION['report']['query'];
        $lastSpacePosition = strrpos($sql, 'ORDER BY');
        if ($lastSpacePosition) {
            $sql = substr($sql, 0, $lastSpacePosition);
        }
        $result = array();
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }   
       // pr($result);exit;
        $this->load->library("excel");
        $heading = explode(',', $_SESSION['report']['header']);
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $rowNumberH = 1;
        $colH = 'A';
        foreach ($heading as $h) {
            $objPHPExcel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
            $objPHPExcel->getActiveSheet()->getStyle($colH . $rowNumberH)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension($colH)->setWidth(30);
            $colH++;
        }
        $objPHPExcel->getActiveSheet(0)->setTitle("Reports");
        $maxrow = count($result) + 1;
        $rowID = 2;

        if (is_array($result) && !empty($result) && count($result) > 0) {

            foreach ($result as $rowArray) {
               if(isset($rowArray['description'])) {
                   $rowArray['description'] = html_entity_decode(strip_tags($rowArray['description']));
               }
                if(isset($rowArray['activity_desc'])) {
                   $rowArray['activity_desc'] = html_entity_decode(strip_tags($rowArray['activity_desc']));
               }
                //pr($rowArray);
                
                $objPHPExcel->getActiveSheet()->fromArray($rowArray, null, 'A' . $rowID++);
            }
        } else {
            $objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A2', 'No data found', PHPExcel_Cell_DataType::TYPE_STRING);
        }
        //exit;
        $objPHPExcel->getActiveSheet()->freezePane('A2');
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        //$objPHPExcel->getActiveSheet()->setAutoFilter('A1:E1');
        //$objPHPExcel->getActiveSheet()->getStyle('A1:E' . $maxrow)->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->getStyle('A2:A' . $maxrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        if (ob_get_contents())
            ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Reports : ' . date('Y-M-d') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        exit();
    }

    public function index($client_id = "") {
        $data['client'] = $this->Model_reports->getClientFilter();
        $data['tool'] = $this->Model_reports->getGatewayTypes();
        $data['getReportTable'] = $this->Model_reports->getReportTable();
        $data['file'] = 'view_reports';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_columnList() {
        if ($_POST['tableId'] != "") {
            $tableId = $_POST['tableId'];
            $data['getReportColumn'] = $this->Model_reports->getReportColumn($tableId);
            $this->load->view('reportColumn', $data);
        }
    }

    public function ajax_filters() {
        $table_fields = explode(',', $_POST['table_fields']);

        $_SESSION['report']['header'] = $_POST['table_fields'];
        foreach ($table_fields as $value) {
            $arrayAliasName[] = $value;
        }
        $data['arrayColumnName'] = $arrayAliasName;
        $this->load->view('report_datatable', $data);
    }

    public function ajax_list() {
        if ($this->input->post('is_page_load_status') != 1) {
            $list = $this->Model_reports->get_datatables();
            $data = array();
            foreach ($list as $customers) {
                $data[] = array_values($customers);
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->Model_reports->count_all(),
                "recordsFiltered" => $this->Model_reports->count_filtered(),
                "data" => $data,
            );
        } else {
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => array(),
            );
        }
        echo json_encode($output);
        exit;
    }

    public function ajax_managecondition() {
        $tableId = $_POST['report_tables'];
        $data['countDiv'] = $_POST['countDiv'];
        $data['condition'] = $_POST['condition'];
        $data['conditions'] = array('=' => "Is", '!=' => "Is not", 'LIKE' => "Contains", 'NOT LIKE' => "Does not contain", '>' => "Greater Than", '<' => "Less Than", '>=' => "Greater Than equal", '<=' => "Less Than equal");
        $data['getConditionColumn'] = $this->Model_reports->getAndOrConditionColumn($tableId);
        $this->load->view('managecondition', $data);
    }

    public function ajax_save_report() {
        $title = $_POST['title'];
        if ($title != "") {
            $returnSet = $this->Model_reports->report_save();
            if ($returnSet['status'] == 'true') {
                $return['status'] = 'true';
                $return['message'] = "Report is saved successfully !!";
            } else {
                $return['status'] = 'false';
                $return['message'] = "something wents wrong !!";
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = "Enter Valid Title";
        }
        echo json_encode($return);
    }

    public function save() {
        //pr($_SESSION);
        $data['getSaveReport'] = $this->Model_reports->fetchMySaveReport();
        $data['getGlobalSaveReport'] = $this->Model_reports->fetchGlobalSaveReport();
        $data['file'] = 'save_reports/save_reports';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_fetch_savereport_list() {
        $getSaveReportsStatus = $this->Model_reports->getSaveReportBasedOnID();
        if ($getSaveReportsStatus['status'] == 'true') {
            $table_fields = explode(',', $getSaveReportsStatus['resultSet']->column_alias);
            foreach ($table_fields as $value) {
                $arrayAliasName[] = $value;
            }
            $data['saveReportId'] = $getSaveReportsStatus['resultSet']->id;
            $data['arrayColumnName'] = $arrayAliasName;
            $this->load->view('save_report_datatable', $data);
        }
    }

  

    public function edit_reports() {                
        $saveId = $_GET['sid'];
        if ($saveId != "") {
            $saveId = decode_url($saveId);
            $data['client'] = $this->Model_event->getClientFilter();
            $data['tool'] = $this->Model_reports->getGatewayTypes();
            $data['getReportTable'] = $this->Model_reports->getReportTable();
            $data['getsaveAttr'] = $this->Model_reports->getSaveAttr($saveId);
            //pr($data['getsaveAttr']);
            //pr($data['getsaveAttr']['attr']->tables);exit;
            $data['conditions'] = array('=' => "Is", '!=' => "Is not", 'LIKE' => "Contains", 'NOT LIKE' => "Does not contain", '>' => "Greater Than", '<' => "Less Than", '>=' => "Greater Than equal", '<=' => "Less Than equal");
            $data['getConditionColumn'] = $this->Model_reports->getAndOrConditionColumn($data['getsaveAttr']['attr']->tables);
            $data['file'] = 'save_reports/edit_save_reports';
            //pr($data);exit;
            $this->load->view('template/front_template', $data);
        }
    }

    public function ajax_columnList_save() {
        if ($_POST['tableId'] != "") {
            $tableId = $_POST['tableId'];
            $data['getReportColumn'] = $this->Model_reports->getReportColumn_save($tableId);
            $this->load->view('save_reports/reportColumn', $data);
        }
    }

    public function ajax_edit_save_report() {
        $returnSet = $this->Model_reports->edit_report();
        if ($returnSet['status'] == 'true') {
            $return['status'] = 'true';
            $return['message'] = "Report is updated successfully !!";
        } else {
            $return['status'] = 'false';
            $return['message'] = "something wents wrong !!";
        }
        echo json_encode($return);
    }

    public function ajax_delete_report() {
        $returnSet = $this->Model_reports->delete_report();
        if ($returnSet['status'] == 'true') {
            $return['status'] = 'true';
            $return['message'] = "Report is Deleted successfully !!";
        } else {
            $return['status'] = 'false';
            $return['message'] = "something wents wrong !!";
        }
        echo json_encode($return);
    }

    public function ajax_table_header() {       
        $saveId = $_POST['save_id'];
        if ($saveId != "") {
            $saveId = decode_url($saveId);         
            $resultSet = $this->Model_reports->getSaveAttr($saveId);
            $field_name = $resultSet['attr']->field_name;
            foreach ($field_name as $value) {
                $explode_field = explode('#', $value);                
                $arrayAliasName[] = $explode_field[1];
            }      
            $data['saveReportId'] = $saveId;
            $data['arrayColumnName'] = $arrayAliasName;
            $this->load->view('save_reports/save_report_datatable', $data);
        }
    }
    
      public function ajax_save_report_list() {
        $saveId = $_POST['save_id'];
        $list = $this->Model_reports->get_datatables_saveReport();
        $data = array();
        foreach ($list as $customers) {
            $data[] = array_values($customers);
        }
        $output = array(
            "draw" => $_POST['draw'],
            //"recordsTotal" => $this->Model_reports->count_all_saveReport(),
           "recordsFiltered" => $this->Model_reports->count_filtered_saveReport(),          
            "data" => $data,
        );





        echo json_encode($output);
        exit;
    }

}
