<style>
    .dataTables_scrollBody{height:210px;}    
</style>
<table id="table" class="table table-striped table-bordered" cellspacing="0">
    <thead style="background: #17C4BB !important; color:white;">
        <tr>
            <?php foreach ($arrayColumnName as $key => $value) { ?>
                <th><?php echo $value; ?></th>
            <?php } ?>
        </tr>

    </thead>
    <tbody>
    </tbody>
</table>
<script type="text/javascript">
    $(".dataTables_scrollHeadInner").css({"width": "100%"});
    $(".table").css({"width": "100%"});
    var table = $('#table').DataTable({
        scrollX: "100%",
        "scrollCollapse": true,
        "bStateSave": false,
        "lengthMenu": [[100, 300, 500], [100, 300, 500,]],
        "bLengthChange" : false,
        "language": {                
            "infoFiltered": ""
        },
        searching: false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.              
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('reports/ajax_save_report_list') ?>",
            "type": "POST",
            "data": function (data) {
                data.save_id = <?php echo $saveReportId;?>;               
            },
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0], //first column / numbering column
                "orderable": true, //set not orderable
            },
        ],
    });


</script>

