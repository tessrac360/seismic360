<style>
    .bottom-btns .btn {
        border: 1 !important;
    }
    .subject-info-box-1,
    .subject-info-box-2 {
        float: left;
        width: 45%;

        select {
            height: 100px;
            padding: 0;
            option {
                padding: 4px 10px 4px 10px;
            }
            option:hover {
                background: #EEEEEE;
            }
        }
    }
    .subject-info-arrows {
        float: left;
        width: 10%;
        margin-top: 50px;
        input {
            width: 30%;
            margin-bottom: 5px;
        }
    }

    select#lstBox1, select#lstBox2 {
        height: 211px;

    }
    input#btnAllRight,  input#btnRight ,input#btnLeft,input#btnAllLeft  {
        width: 40px;
        border-radius:0px !important;
        color: #000;
        background: #fdfdfd;
        font-weight: bold;
    }
</style>


<script>
    $('#btnRight').click(function (e) {
        var selectedOpts = $('#lstBox1 option:selected');
        if (selectedOpts.length == 0) {
            bootbox.alert({
                message: "Please select available Columns",
                className: "combobox-select"
            });

            e.preventDefault();
        }
        $('#lstBox2').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });
    $('#btnAllRight').click(function (e) {
        var selectedOpts = $('#lstBox1 option');
        if (selectedOpts.length == 0) {
            bootbox.alert({
                message: "Please select available Columns",
                className: "combobox-select"
            });
            e.preventDefault();
        }
        $('#lstBox2').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });
    $('#btnLeft').click(function (e) {
        var selectedOpts = $('#lstBox2 option:selected');
        if (selectedOpts.length == 0) {
            bootbox.alert({
                message: "Please select available Columns",
                className: "combobox-select"
            });
            e.preventDefault();
        }
        $('#lstBox1').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });
    $('#btnAllLeft').click(function (e) {
        var selectedOpts = $('#lstBox2 option');
        if (selectedOpts.length == 0) {
            bootbox.alert({
                message: "Please select available Columns",
                className: "combobox-select"
            });
            e.preventDefault();
        }
        $('#lstBox1').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });
</script>
<div class="subject-info-box-1">
    <label for="firstname" class="control-label">Available Columns </label>
    <select multiple="multiple" id='lstBox1' class="form-control">
        <?php
        if (!empty($getReportColumn['available'])) {
            foreach ($getReportColumn['available'] as $value) {
               
                    ?>
                    <option value="<?php echo $value->column_name . '#' . $value->alias_name . '#' . $value->f_id ?>"><?php echo $value->alias_name ?></option>
                    <?php
                
            }
        }
        ?>
    </select>
</div>
<div class="subject-info-arrows text-center">
    <input type="button" id="btnAllRight" value=">>" class="btn " /><br />
    <input type="button" id="btnRight" value=">" class="btn btn-default" /><br />
    <input type="button" id="btnLeft" value="<" class="btn btn-default" /><br />
    <input type="button" id="btnAllLeft" value="<<" class="btn btn-default" />
</div>
<div class="subject-info-box-2">
    <label for="firstname" class="control-label">Selected Columns </label>
    <select multiple="multiple" id='lstBox2' selected="selected" class="form-control field_name" name="report_column[]">
        <?php
        if (!empty($getReportColumn['selected'])) {
            foreach ($getReportColumn['selected'] as $value) {
               
                    ?>
                    <option value="<?php echo $value->column_name . '#' . $value->alias_name . '#' . $value->f_id ?>"><?php echo $value->alias_name ?></option>
                    <?php
                
            }
        }
        ?>
    </select>
</div>
<div class="clearfix"></div>

