 <?php $level = helper_getLevelOfUser($_SESSION['role_id']); ?>
<style>
    a:hover{text-decoration: none;}
    .remove-pad-right{padding-right:0;}
    .remove-pad-left{padding-left: 0;}

    .page-title {
        background: rgba(204, 204, 204, 0.22);
        padding: 6px 9px;
        margin: 0 0 10px;
    }.page-title{font-size: 14px; font-weight: 600 !important; letter-spacing:-0.5px !important ;}	
    .portlet.light>.portlet-title>.actions{right:0;}
    .reports-save-tabs{list-style-type:none; margin:0;padding:0;}
    .reports-save-tabs  li a {font-size: 12.5px; font-weight: 600;  padding:6px; display:block; border-bottom:1px solid #f0f0f0;}
    .dash-drag .btn-sm{padding: 1px 10px;}
    .bg1{background-color: #fff; padding: 5px 5px 3px 5px; border-bottom: 1px solid #f0f0f0; font-size: 13px;}
    .bg1:hover, .bg1.active{background: #e7fdff; text-decoration: none;}
    .bor-bot{border-bottom:1px solid #f0f0f0;}
    .dataTables_wrapper .dataTables_paginate .paginate_button:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{background: #337ab7 !important; color: #fff !important;
                                                                                                                                                                                                        border-color: #337ab7 !important;}
    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover{background: transparent !important; border:1px solid #ddd !important;}
    .dataTables_scroll{margin-bottom: 0;}
    div.dataTables_wrapper div.dataTables_paginate{margin-bottom: 3px;}
    table tr th, table tr td{max-width:200px; overflow-x: hidden; text-overflow: ellipsis; white-space: nowrap;}

    .no-records{text-align: center; height: 78.5vh; margin-bottom: 0;}
    .no-records img{margin: 0 auto;    position: relative; top: 50%; margin-top: -120px;}
    .save_reports_block{background: #fff; height:78.5vh ; padding:0;}
    .save_reports_block h2{margin: 0; font-size: 15px; padding: 8px 6px; font-weight: 600; border-bottom: 1px solid #eaeaea;}
    .main-title h3{margin: 0;  font-size: 13px; padding: 8px 6px; font-weight: 600; background: #d8dde3; border-bottom: 1px solid #eaeaea;}
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{bottom: 2px;}
    table.dataTable{margin-top: 0 !important;}
    .saveReportDatatable{background: #fff;}
    .header-right-btn{position: absolute; top: 8px; right:8px;font-size: 12px;}
    .header-right-btn a{font-size: 12px; padding: 2px 8px;}
</style>
<div>
    <h1 class="page-title"> View / Run Reports</h1>
    <div class="header-right-btn">
        <div class="page-toolbar form-inline" style="padding-top:0;">
            <a href="<?php echo base_url() . 'reports' ?>"  class="btn btn-primary"> Create/Run Report</a>
        </div>
    </div>


    <div>
        <div class="row save_block">

            <div class="col-md-6">
                <div class="row">
                    <div class="main-title">
                        <h3>Global Reports <i class="fa fa-file-text pull-right" aria-hidden="true"></i></h3>
                    </div>
                </div>
                <div class="row table-scroll scroller save_right_block" style="padding-right:0;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                    <div class="col-md-12 save_reports_block">
                        <div>
                            <div class="reports-save-tabs">
                                <?php
                                if ($getGlobalSaveReport['status'] == 'true') {
                                    foreach ($getGlobalSaveReport['resultSet'] as $key => $value) {
                                        ?>
                                        <div class="dash-drag deleterep_<?php echo $value->id; ?>"  >
                                            <div class="col-xs-12 bg1">
                                                <?php
                                                
                                                if ($level == 'Y') { ?>
                                                    <a href="<?php echo base_url() . 'reports/edit_reports?sid=' . encode_url($value->id); ?>" style="color: #000;word-wrap: break-word;text-decoration: none" class="col-md-9" keyid ="<?php echo $key; ?>" save_id="<?php echo encode_url($value->id); ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $value->report_name ?> </a>
                                                <?php } else { ?>
                                                    <a href="#" onclick="bootbox.alert('Unauthorized access!');" style="color: #000;word-wrap: break-word;text-decoration: none" class="col-md-9" keyid ="<?php echo $key; ?>" save_id="<?php echo encode_url($value->id); ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $value->report_name ?> </a>
                                                <?php } ?>
                                                <?php if ($level == 'Y') { ?>
                                                    <div class=" col-md-3 padr0">
                                                        <div class="btn-group btn-group-circle pull-right">
                                                            <a href="<?php echo base_url() . 'reports/edit_reports?sid=' . encode_url($value->id) . "&action=edit"; ?>"  class="btn btn-outline green btn-sm"><i class="fa fa-edit"></i></a>
                                                            <button type="button" class="btn btn-outline red btn-sm report-delete" keyid ="<?php echo $value->id; ?>" save_id="<?php echo encode_url($value->id); ?>"><i class="fa fa-trash-o"></i></button>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                            </div>                         
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="dash-drag"  >
                                        <div class="col-xs-12 bg1">

                                            <a href="#" style="color: #000;word-wrap: break-word;text-decoration: none" class="col-md-9" >No Global Reports Found </a>


                                        </div>                         
                                    </div>  
                                <?php }
                                ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="main-title">
                        <h3>My Reports <i class="fa fa-file-text pull-right" aria-hidden="true"></i></h3>
                    </div>
                </div>
                <div class="row table-scroll scroller save_right_block" style="padding-right:0;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                    <div class="col-md-12 save_reports_block">
                        <div>
                            <div class="reports-save-tabs">
                                <?php
                                if ($getSaveReport['status'] == 'true') {
                                    foreach ($getSaveReport['resultSet'] as $key => $value) {
                                        ?>
                                        <div class="dash-drag deleterep_<?php echo $value->id; ?>"  >
                                            <div class="col-xs-12 bg1">

                                                <a href="<?php echo base_url() . 'reports/edit_reports?sid=' . encode_url($value->id); ?>" style="color: #000;word-wrap: break-word;text-decoration: none" class="col-md-9" keyid ="<?php echo $key; ?>" save_id="<?php echo encode_url($value->id); ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $value->report_name ?> </a>


                                                <div class=" col-md-3 padr0">
                                                    <div class="btn-group btn-group-circle pull-right">
                                                        <a href="<?php echo base_url() . 'reports/edit_reports?sid=' . encode_url($value->id); ?>"  class="btn btn-outline green btn-sm"><i class="fa fa-edit"></i></a>
                                                        <button type="button" class="btn btn-outline red btn-sm report-delete" keyid ="<?php echo $value->id; ?>" save_id="<?php echo encode_url($value->id); ?>"><i class="fa fa-trash-o"></i></button>
                                                    </div>
                                                </div>
                                            </div>                         
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="dash-drag"  >
                                        <div class="col-xs-12 bg1">

                                            <a href="#" style="color: #000;word-wrap: break-word;text-decoration: none" class="col-md-9" >No Reports Found </a>


                                        </div>                         
                                    </div>  
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 



        </div>
    </div>
</div>

<script>
    $(document).on('click', '.report-delete', function () {

        //$(this).closest('.dash-drag').remove();
        save_id = $(this).attr('save_id');
        key = $(this).attr('keyid');

        // status = $(this).attr('status');
        swal({
            title: "Are you sure?",
            text: "you want to delete ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: '<?php echo base_url('reports/ajax_delete_report') ?>',
                            data: {save_id: save_id},
                            success: function (response) {
                                if (response.status == 'true') {

                                    $('.deleterep_' + key).remove();
                                    swal("", response.message, "success");
                                } else {
                                    alert(response.message);
                                }
                            }
                        });
                    }
                });

    });
</script>
