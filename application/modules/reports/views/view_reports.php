<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.css">
<style>
    .custom-search .portlet.light{padding:0;}
    .fields-block, .column-block{border-right:1px solid #e5e5e5;}

    .fields-block{padding:10px 12px 10px 5px;}
    .column-block{padding: 0px 10px 10px 12px;}

    .pad0{padding:0 !important;}
    .add-btns{border-bottom:1px solid #e5e5e5;}
    .add-content{min-height:120px; padding-left: 3px; padding-right: 3px;}
    .bottom-btns{border-top:0px solid #e5e5e5; padding-top:4px;}
    .fields-block:after, .column-block:after{ width:23px; height:23px; content:""; position:absolute; top:50%; margin-top:-8px; right:-11.5px; z-index:999;}
    .custom-search .form-group { margin-bottom: 3px; height:56px;}
    .custom-search .control-label{color:#5d5d5d; font-size:13px; font-weight:600;}
    .custom-search .form-control{color:#5d5d5d; font-size:13px;}
    .ms-container{width: 100% !important; height: 252px;background-position: 50% 44% !important;}
    .ms-container .ms-list{height: 195px !important;}
    .ms-container {
        width: 100% !important;
        height: 160px !important;;
        background-position: 50% 44% !important;
    }

    .ms-selectable:after{content:"Available Columns :"; position:absolute; bottom:15px; width: 21%; font-weight: bold; text-align: center; font-size:13px;top: 0px;}
    .ms-selection:after{content:"Selected Columns :"; position:absolute; bottom:15px; width: 21%;font-weight: bold; text-align: center; font-size:13px;top: 0px;}

    .small-title{font-size:12.5px; line-height: 25px;}
    .add-btns button{font-size:12px; color:#4f4f4f; background:#e7e7e7; border:1px solid #d2d2d2; font-weight:600; padding:3px 11px; border-radius:15px !important;}
    .add-btns{padding: 3px 5px 3px 3px;}
    .add-btns p{margin:0;}
    .add_bar{background:#f1f2f4; margin:3px 0 3px 0; padding:4px; font-size:12.5px;}
    .add_bar .form-control{height:24px;padding: 0px 2px;width: 17%}
    .add_bar .btn{height:24px;padding: 0px 6px;font-size: 11px;}
    .add_bar .form-inline  select.form-control.first-secetion{width:39%;}
    .bottom-btns .btn{font-size: 12px;color: #4f4f4f;background: #e7e7e7;border: 1px solid #d2d2d2;font-weight: 600; padding: 3px 11px;border-radius: 15px !important; color:#fff; width:75px; border:0;}
    .bottom-btns .btn-save{background:#1dc3ba;}
    .bottom-btns .btn-run{background:#3897db;}
    .bottom-btns .btn-export .buttons-csv{background:#fc9527;}
    #table th, table td { white-space: nowrap;}
    input.form-control.conditiontextbox {width: 155px; }
    label.condition_lebel {width: 30px;}
    .multiselect.dropdown-toggle.btn{text-align: left;}
    .multiselect.dropdown-toggle.btn .caret{ position:absolute; right:10px; top:50%;}
    .multiselect.dropdown-toggle.btn span{ overflow: hidden; width: 92%; display: block;}
    .multiselect.dropdown-toggle.btn-default:hover{border-color: #ccc;}
    a.dt-button.buttons-csv.buttons-html5{text-align:center;font-size:11px;font-weight:700;padding:3px 11px;border-radius:15px!important;color:#fff;width:75px;border:0;background:#fc9527;margin-top:5px}
    a.dt-button.buttons-pdf.buttons-html5 {text-align:center;font-size:11px;font-weight:700;padding:3px 11px;border-radius:15px!important;color:#fff;width:75px;border:0;background:#e90a18;margin-top:5px}
    div#table_length{padding:8px 0 0 8px}
    .input-daterange { width: 100% !important;}
    .panel-default {
        border-color: #fff;
    }
    .more-less {
        float: right;
        color: #212121;
        font-size: 11px;
    }
    button.btn.btn-export {
        background-color: #fc9527;
    }
    span#error_report {
        color: red;
        font-size: 12px;
        margin-left: 5px;
    }
    .panel-group{margin-bottom:0px;}
    .panel-body{padding:0px;}
    .panel-heading{padding: 4px 9px;}


    .combobox-select .modal-footer {
        display: none;
    }
    .combobox-select .modal-body {
        background-color: #f8c8c8;
    }
    td.dataTables_empty {
        text-transform: uppercase;
    }
    .panel-default>.panel-heading{background:#fff;}
    #bind_field{padding-top: 6px;}
    .btn-save-bottom{border-top: 1px solid #e5e5e5 !important;}
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{    bottom: 2px;}
    a.paginate_button, span.ellipsis {
        position: relative !important;
        padding: 3px 8px !important;
        line-height: 1.42857 !important;
        text-decoration: none !important;
        color: #337ab7 !important;
        background-color: #fff !important;
        border: 1px solid #ddd !important;
        margin-left: -1px !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{background: #337ab7 !important; color: #fff !important;
                                                                                                                                                                                                        border-color: #337ab7 !important;}
    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover{background: transparent !important; border:1px solid #ddd !important;}
    .dataTables_scroll{margin-bottom: 0;}
    div.dataTables_wrapper div.dataTables_paginate{margin-bottom: 3px;}
/*    table tr th, table tr td{max-width:200px; overflow-x: hidden; text-overflow: ellipsis; white-space: nowrap;}*/
    input#from_date{background:url("./public/images/calendar.png") no-repeat 10px center  ; }
    input#to_date{background:url("./public/images/calendar.png") no-repeat 10px center  ; }
    .fs-wrap {width: 100%;}
    .fs-label-wrap .fs-label { padding: 10px 22px 10px 8px;}
    /*.fs-dropdown { width: 90%;}*/
    
      table tr th{max-width:900px; white-space: nowrap;}
     table tr td{max-width:900px; white-space: normal; overflow: auto; vertical-align: top !important;}
     table tr td table tr td{font-size: 12px !important;}
</style>

<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-md-12 custom-search">            
        <div class="portlet light" style="margin:8px 4px 0 4px;overflow: visible;">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1">
                                <i class="more-less glyphicon glyphicon-minus"></i>Reports
                            </a>
                        </h4>
                    </div>
                    <div id="panel1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="portlet-body pad0">                                
                                <form name="form_filter" id="form_filter" method="post" action="<?php echo base_url('reports/exportexcel'); ?>">
                                    <div class="row">
                                        <div class="col-md-3 fields-block">
                                            <div class="form-group">
                                                <label for="firstname" class="control-label">Select Data Table : </label>
                                                <div>
                                                    <select name="report_tables"  id="select2-single-input-sm" class="form-control input-sm select2-multiple report_tables">                                      
                                                        <?php
                                                        if ($getReportTable['status'] == 'true') {
                                                            foreach ($getReportTable['resultSet'] as $value) {
                                                                ?>
                                                                <option value="<?php echo $value->t_id . '#' . $value->table_name ?>"><?php echo $value->alias ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="firstname" class="control-label">Partner : </label>
                                                <div >


                                                    <select class="form-control client sd" name="report_clients[]" id="client" multiple="multiple">
<!--                                                    <select name="report_clients[]" id="client"  multiple="multiple" class="client form-control input-sm multiselect" name="client[]">-->
                                                        <?php
                                                        if ($client['status'] == 'true') {
                                                            foreach ($client['resultSet'] as $value) {
                                                                ?>
                                                                <option value="<?php echo $value->id; ?>"><?php echo $value->client_title; ?></option>  
                                                                <?php
                                                            }
                                                        }
                                                        ?> 

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group" style="width:49%;float:left;">
                                                <label for="firstname"  class="control-label">Tool : </label>
                                                <div class="tool-start">
                                                    <select class="form-control tool sd" name="report_tool[]" id="tool" disabled="disabled" multiple="multiple">                                                    
                                                        <?php
                                                        if ($tool['status'] == 'true') {
                                                            foreach ($tool['resultSet'] as $value) {
                                                                ?>
                                                                <option value="<?php echo $value->gatewaytype_uuid; ?>"><?php echo $value->title; ?></option>  
                                                                <?php
                                                            }
                                                        }
                                                        ?> 

                                                    </select>
                                                </div>
                                            </div>												
                                            <div class="form-group" style="width:49%;float:right;">
                                                <label for="firstname" class="control-label">View Type : </label>
                                                <div>
                                                    <select class="form-control">
                                                        <option>List</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label class="control-label"><strong>Date Range</strong></label>
                                                    <div class="input-group input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
                                                        <input id="from_date" type="text" autocomplete="off"  class="form-control datepick start_date" placeholder="Start Date"  name="from_date">
                                                        <span class="input-group-addon"> To </span>
                                                        <input id="to_date" type="text" autocomplete="off"  class="form-control datepick end_date" placeholder="End Date"  name="to_date"> 
                                                    </div>             
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="col-md-4  column-block">
                                            <div class="bottom-btns">
                                                <div id="bind_field"></div>
                                            </div>

                                        </div>

                                        <div class="col-md-5 more-block pad0">
                                            <div class="add-btns">
                                                <button class="addmore" value="and">Add Filter Condition</button>
                                                <button class="addmore" value="or">Add OR Clause</button>
                                                <p class="pull-right small-title">
                                                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> All of these conditions must be met
                                                </p>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="add-content scroller addMoreDiv field_wrapper" style="height: 193px;" data-always-visible="1" data-rail-visible="0">
                                            </div>                                           
                                            <div class="bottom-btns text-center btn-save-bottom">
                                                <button type="submit" class="btn btn-run filterSubmit"><i class="fa fa-search"></i> Search</button>                                                                            
                                                <span id="hide_btn_pageload" style="display: none">
                                                    <button type="submit" class="btn btn-export"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</button>
                                                    <a class="btn btn-save btn-outline sbold" data-toggle="modal" href="#responsive"><i class="fa fa-save"></i> Save </a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="is_page_load_status" value="1">
                                </form>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 custom-search">
        <div class="portlet light" style="margin:4px 4px 0 4px;">
            <div class="col-md-12 resultSetFilter">
                <!--<div class="col-md-12 resultSetFilter" style="overflow: auto; padding: 0px; float: left; height: 144px;" id="style-3">-->

            </div>
        </div>
    </div>	

</div>
<div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Save Reports</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" id="title" placeholder="Enter your Title" class="col-md-12 form-control"> 
                    </div>
                    <span id="error_report"></span>                  
                    <?php
                    $level = helper_getLevelOfUser($_SESSION['role_id']);
                    if ($level == 'Y') {
                        ?>
                        <div class="col-md-12">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="report_status" value="G"/> Global Reports
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="report_status" checked value="M"/> My Reports
                                    <span></span>
                                </label>	
                            </div>                        
                        </div> 
<?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="button" class="btn green save-report">Save changes</button>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .multiselect-container {
        width: 100% !important;
    }
    .portlet.light .dataTables_wrapper .dt-buttons {
        margin-top: 0 !important;
    }
</style>
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.js" type="text/javascript"></script>	
<script src="<?php echo base_url() . "public/" ?>js/form/form_report.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>