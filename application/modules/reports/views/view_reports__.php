<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />



<style>
    .ui-widget.ui-widget-content {
        border: 0px solid #cccccc;
    }
    .ui-widget-content {
        border: 0px solid #0000 !important;
        background: top repeat-x !important;
    }
</style>
<!-- END PAGE HEADER-->
<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Reports</span>
            </div>
            <div class="page-toolbar">
                <ul class="page-breadcrumb breadcrumb custom-bread">
                    <li>
                        <i class="fa fa-cog"></i>
                        <span>Report</span>                       
                    </li>                   
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">

                <form role="form" name="frmUser" id="frmUser" method="post" action="" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="select2-single-input-sm" class="control-label">col-md-3</label>
                                <select id="select2-single-input-sm" class="form-control input-sm select2-multiple">
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>                                   
                                </select>
                            </div>
                            <div class="col-md-3" >
                                <select multiple="multiple" class="multi-select" id="my_multi_select1" name="my_multi_select1[]">
                                    <option>Dallas Cowboys</option>
                                    <option>New York Giants</option>
                                    <option selected>Philadelphia Eagles</option>
                                    <option selected>Washington Redskins</option>
                                    <option>Chicago Bears</option>
                                    <option>Detroit Lions</option>
                                    <option>Green Bay Packers</option>
                                    <option>Minnesota Vikings</option>
                                    <option selected>Atlanta Falcons</option>
                                    <option>Carolina Panthers</option>
                                    <option>New Orleans Saints</option>
                                    <option>Tampa Bay Buccaneers</option>
                                    <option>Arizona Cardinals</option>
                                    <option>St. Louis Rams</option>
                                    <option>San Francisco 49ers</option>
                                    <option>Seattle Seahawks</option>
                                </select>

                            </div>
                            <div class="col-md-3" >

                            </div>
                        </div>
                        <div class="form-actions noborder">

                            <button type="submit" class="btn green">Filter</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('users'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .optionGroup {
        font-weight: bold;        
    }
    .optionChild {
        padding-left: 15px;
    }
</style>  
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/pages/scripts/components-multi-select.min.js" type="text/javascript"></script>

