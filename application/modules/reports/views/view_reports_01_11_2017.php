<style>
    .custom-search .portlet.light{padding:0;}
    .fields-block, .column-block{border-right:1px solid #e5e5e5;}

    .fields-block{padding:10px 22px 10px 10px;}
    .column-block{padding: 10px 10px 10px 28px;}

    .pad0{padding:0 !important;}
    .add-btns{border-bottom:1px solid #e5e5e5;}
    .add-content{min-height:120px; padding-left: 3px; padding-right: 3px;}
    .bottom-btns{border-top:1px solid #e5e5e5; padding-top:4px;}

    .fields-block:after, .column-block:after{background:url(public/images/icon-arrow-left.png) no-repeat center center; width:23px; height:23px; content:""; position:absolute; top:50%; margin-top:-8px; right:-11.5px; z-index:999;}
    .custom-search .form-group { margin-bottom: 3px; height:66px;}
    .custom-search .control-label{color:#5d5d5d; font-size:13px; font-weight:600;}
    .custom-search .form-control{color:#5d5d5d; font-size:13px;}
    .ms-container{width: 370px !important; height: 252px;background-position: 50% 44% !important;}
    .ms-container .ms-list{height: 220px !important;}

    .ms-selectable:after{content:"Available"; position:absolute; bottom:15px; width: 40%; text-align: center; font-size:13px;}
    .ms-selection:after{content:"Selected"; position:absolute; bottom:15px; width: 40%; text-align: center; font-size:13px;}

    .small-title{font-size:12.5px;}
    .add-btns a{font-size:12px; color:#4f4f4f; background:#e7e7e7; border:1px solid #d2d2d2; font-weight:600; padding:3px 11px; border-radius:15px !important;}
    .add-btns{padding: 8px 6px;}
    .add-btns p{margin:0;}
    .add_bar{background:#f1f2f4; margin:3px 0 3px 0; padding:4px; font-size:12.5px;}
    .add_bar .form-control{height:24px;padding: 0px 2px;}
    .add_bar .btn{height:24px;padding: 0px 6px;font-size: 11px;}
    .add_bar .form-inline  select.form-control.first-secetion{width:215px;}
    .bottom-btns .btn{font-size: 12px;color: #4f4f4f;background: #e7e7e7;border: 1px solid #d2d2d2;font-weight: 600; padding: 3px 11px;border-radius: 15px !important; color:#fff; width:75px; border:0;}
    .bottom-btns .btn-save{background:#1dc3ba;}
    .bottom-btns .btn-run{background:#3897db;}
    .bottom-btns .btn-export{background:#fc9527;}
    table#role th, table#role td { white-space: nowrap;}
    input.form-control.conditiontextbox {width: 155px; }
    label.condition_lebel {width: 30px;}
</style>
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="http://127.0.0.1/eventedge/SOURCECODE/public/assets/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-md-12 custom-search">
        <div class="portlet light" style="margin:8px 4px 0 4px;">
            <div class="portlet-body pad0">
                <div class="row">
                    <div class="col-md-3 fields-block">
                        <div class="form-group">
                            <label for="firstname" class="control-label">Select Data Table : </label>
                            <div>
                                <select id="select2-single-input-sm" class="form-control input-sm select2-multiple">
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>                                   
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="control-label">Client : </label>
                            <div>
                                <select id="client" multiple="multiple" class="client form-control input-sm" name="client[]">
                                    <option value="0">Common1</option>
                                    <option value="1">Common2</option>
                                    <option value="2">Common3</option>
                                    <option value="3">Common4</option>
                                    <option value="0">Common1</option>
                                    <option value="1">Common2</option>
                                    <option value="2">Common3</option>
                                    <option value="3">Common4</option>
                                    <option value="0">Common1</option>
                                    <option value="1">Common2</option>
                                    <option value="2">Common3</option>
                                    <option value="3">Common4</option>
                                    <option value="0">Common1</option>
                                    <option value="1">Common2</option>
                                    <option value="2">Common3</option>
                                    <option value="3">Common4</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <!--                            <label for="firstname" class="control-label">Type : </label>
                                                        <div>
                                                            <select class="form-control">
                                                                <option>-- Select Type --</option>
                                                                <option>-- Select Type --</option>
                                                                <option>-- Select Type --</option>
                                                                <option>-- Select Type --</option>
                                                            </select>
                                                        </div>-->
                        </div>
                        <div class="form-group">
                            <!--                            <label for="firstname" class="control-label">Group By : </label>
                                                        <div>
                                                            <select class="form-control">
                                                                <option>-- Select Group By --</option>
                                                                <option>-- Select Group By --</option>
                                                                <option>-- Select Group By --</option>
                                                                <option>-- Select Group By --</option>
                                                            </select>
                                                        </div>-->
                        </div> 
                    </div>
                    <div class="col-md-4 column-block">
                        <div>
                            <label for="firstname" class="control-label">Select Columns : </label>
                            <div>
                                <select multiple="multiple" class="multi-select" id="my_multi_select1" name="my_multi_select1[]">
                                    <option>Dallas Cowboys</option>
                                    <option>New York Giants</option>
                                    <option selected>Philadelphia Eagles</option>
                                    <option selected>Washington Redskins</option>
                                    <option>Chicago Bears</option>
                                    <option>Detroit Lions</option>
                                    <option>Green Bay Packers</option>
                                    <option>Minnesota Vikings</option>
                                    <option selected>Atlanta Falcons</option>
                                    <option>Carolina Panthers</option>
                                    <option>New Orleans Saints</option>
                                    <option>Tampa Bay Buccaneers</option>
                                    <option>Arizona Cardinals</option>
                                    <option>St. Louis Rams</option>
                                    <option>San Francisco 49ers</option>
                                    <option>Seattle Seahawks</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 more-block pad0">
                        <div class="add-btns">
                            <a href="">Add Filter Condition</a>
                            <a href="">Add OR Clause</a>
                            <p class="pull-right small-title">
                                <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> All of these conditions must be met
                            </p>
                            <div class="clearfix"></div>
                        </div>
                        <div class="add-content scroller" style="height: 228px;" data-always-visible="1" data-rail-visible="0">
                            <div class="add_bar">
                                <div class="form-inline">
                                    <label class="condition_lebel"></label>
                                    <select class="form-control first-secetion">
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                    </select>
                                    <select class="form-control">
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                    </select>                                   
                                    <input type="text" class="form-control conditiontextbox">                                  
                                </div>
                            </div>
                            <div class="add_bar">
                                <div class="form-inline">
                                    <label class="condition_lebel">And : </label>
                                    <select class="form-control first-secetion">
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                    </select>
                                    <select class="form-control">
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                    </select>
                                    <input type="text" class="form-control conditiontextbox">    
                                    <button class="btn btn-default"><i class="fa fa-times" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <div class="add_bar">
                                <div class="form-inline">
                                    <label class="condition_lebel">OR : </label>
                                    <select class="form-control first-secetion">
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                        <option>-- Choose Field -- </option>
                                    </select>
                                    <select class="form-control">
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                        <option>-- Oper -- </option>
                                    </select>
                                    <input type="text" class="form-control conditiontextbox">    
                                    <button class="btn btn-default"><i class="fa fa-times" aria-hidden="true"></i></button>
                                </div>
                            </div>

                        </div>
                        <div class="bottom-btns text-center">
                            <a href="" class="btn btn-save">Save</a>
                            <a href="" class="btn btn-run">Run</a>
                            <a href="" class="btn btn-export">Export</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 custom-search">
        <div class="portlet light" style="margin:8px 4px 0 4px;">
            <div class="col-md-12" style="overflow: auto; padding: 0px; float: left; height: 144px;" id="style-3">
                <table class="table table-striped table-bordered table-hover order-column" id="role">
                    <thead style="background: #17C4BB !important; color:white;">
                        <tr>
                            <th style="width:12%">Incident Number</th>
                            <th style="width:12%">Incident Date &amp; Time</th>
                            <th>Client</th>
                            <th>Requestor</th>
                            <th>Created By</th>
                            <th>Summary</th>
                            <th>Assigned To</th>
                            <th>Queue</th>
                            <th>State</th>
                            <th>Priority</th>
                            <th>Severity</th>
                            <th>Ticket Type</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Last Updated</th>
                            <th>Closed On</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="http://10.10.32.52/eventedge2/event/ticket/editTicket/lDDrXMlqT9vE1XxJSEu~knHYbFM0sPwYsGBXaN~l4.OQFOshi0W8mDVhAcJfmoPOprTXHWKBg5tEDUU0Gd2dFg--">INC-000000000001781</a></td>
                            <td>2017-10-27 11:40:28</td>
                            <td>Nectar</td>
                            <td>Eventedge</td>
                            <td>System</td>								
                            <td>Host name: Trunk_Group_366-366_Connecticut_StAlarm Type: Host CheckSeverity: UNKNOWN</td>
                            <td></td>
                            <td>General</td>
                            <td>Assigned</td>
                            <td>Sev-2</td>
                            <td>UNKNOWN</td>
                            <td>Incident</td>
                            <td>Avaya</td>
                            <td>Avaya Aura Conferencing</td>
                            <td>2017-10-27 11:56:40</td>
                            <td> - </td>
                        </tr>
                    </tbody>


                </table>
            </div>
        </div>
    </div>	

</div>

<script>
    $(function ()
    {
        $('#client').multiselect({
            includeSelectAllOption: true,
            nonSelectedText: 'Clients',
            selectAllText: ' Select all',
            maxHeight: 150,
            buttonWidth: '290px'
                    //includeSelectAllOption: true,
                    //nonSelectedText:'Services'
        });
        $('#style-3').css({'height': (($(window).height()) - 400) + 'px'});

        $(window).bind('resize', function () {
            $('#style-3').css({'height': (($(window).height()) - 400) + 'px'});
            //alert('resized');
        });
    });
</script>
<style type="text/css">
    .multiselect-container {
        width: 100% !important;
    }
</style>
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>