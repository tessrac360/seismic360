<?php
if ($countDiv == 0) {
    ?>
    <div class="add_bar">
        <div class="form-inline">
            <label class="condition_lebel"></label>
            <select class="form-control first-secetion choose_field" name="choose_field[]">
                <option value=""> -- choose field --</option>
                <?php
                if ($getConditionColumn['status'] == 'true') {
                    foreach ($getConditionColumn['resultSet'] as $value) {
                        ?>
                        <option value="<?php echo $value->condition_search_field . '#AND' . '#' . $value->field_datatype . '#' . $value->f_id ?>"> <?php echo $value->alias_name ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <select class="form-control oper">
                <option value=""> -- oper --</option>
                <?php
                foreach ($conditions as $key => $value) {
                    ?>
                    <option value="<?php echo $key; ?>"> <?php echo $value; ?></option>
                <?php }
                ?>
            </select>
            <!--<input type="text" class="form-control" id="datepicker" >-->
            <input type="text" placeholder="--Value--"  class="form-control conditiontextbox datepicker12" id="datep" data-validation="date" data-validation-format="mm-dd-yyyy" data-validation-has-keyup-event="true">            
            <button class="btn btn-default remove_button topclosebtn"><i class="fa fa-times" aria-hidden="true"></i></button>
        </div>
    </div>  
<?php } else if ($condition == 'and') { ?>
    <div class="add_bar">
        <div class="form-inline">
            <label class="condition_lebel">And : </label>
            <select class="form-control first-secetion choose_field" name="choose_field[]">
                <option value=""> -- choose field --</option>

                <?php
                if ($getConditionColumn['status'] == 'true') {
                    foreach ($getConditionColumn['resultSet'] as $value) {
                        ?>
                        <option value="<?php echo $value->condition_search_field . '#AND' . '#' . $value->field_datatype. '#' . $value->f_id; ?>"> <?php echo $value->alias_name ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <select class="form-control oper">
                <option value=""> -- oper --</option>
                <?php
                foreach ($conditions as $key => $value) {
                    ?>
                    <option value="<?php echo $key; ?>"> <?php echo $value; ?></option>
                <?php }
                ?>
            </select>                                 
            <input type="text" placeholder="--Value--" class="form-control conditiontextbox datepicker12">   
            <button class="btn btn-default remove_button"><i class="fa fa-times" aria-hidden="true"></i></button>
        </div>
    </div>
<?php } else if ($condition == 'or') { ?>
    <div class="add_bar">
        <div class="form-inline">
            <label class="condition_lebel">OR : </label>
            <select class="form-control first-secetion choose_field" name="choose_field[]">
                <option value=""> -- choose field --</option>
                <?php
                if ($getConditionColumn['status'] == 'true') {
                    foreach ($getConditionColumn['resultSet'] as $value) {
                        ?>
                        <option value="<?php echo $value->condition_search_field . '#OR' . '#' . $value->field_datatype. '#' . $value->f_id ?>"> <?php echo $value->alias_name ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <select class="form-control oper">
                <option value=""> -- oper --</option>
                <?php
                foreach ($conditions as $key => $value) {
                    ?>
                    <option value="<?php echo $key; ?>"> <?php echo $value; ?></option>
                <?php }
                ?>
            </select>                                  
            <input type="text" placeholder="--Value--" class="form-control conditiontextbox datepicker12">   
            <button class="btn btn-default remove_button"><i class="fa fa-times" aria-hidden="true"></i></button>
        </div>
    </div> 
<?php } ?>