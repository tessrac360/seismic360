<?php

class Model_reports extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'temp_role';
        $this->client = 'client';
        $this->gateway_types = 'gateway_types';
        $this->company_id = $this->session->userdata('company_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $this->client_id = $this->session->userdata('client_id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $this->userId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $this->userId);
        $this->accessLevelClient = helper_getAccessLevelClient();
    }

    public function getClientFilter() {
        $accessLevelClient = helper_getAccessClientDropDown();
        if ($accessLevelClient['status'] == 'true') {
            $this->db->select('id,client_title');
            $this->db->from('client');
            $this->db->where_in('id', $accessLevelClient['resultSet']);
            $query = $this->db->get();
            // echo $this->db->last_query();exit;       
            if ($query->num_rows() > 0) {
                $return['status'] = 'true';
                $return['resultSet'] = $query->result();
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    public function getReportTable() {
        $this->db->select('t_id,table_name,alias');
        $this->db->from('report_table');
        $this->db->where('is_status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }//pr($return);exit;
        return $return;
    }

    public function explodeVal($input = "") {
        return explode('#', $input);
    }

    public function getReportColumn($tableId = "") {
        $arrayRet = $this->explodeVal($tableId);
        $t_id = $arrayRet['0'];
        $this->db->select('column_name,alias_name,is_selected_column,f_id');
        $this->db->from('report_fields');
        $this->db->where('is_status', 'Y');
        $this->db->where('t_id', $t_id);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $data = $query->result();
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }//pr($return);exit;
        return $return;
    }

    /* start datatable */

    private function _get_datatables_query() {
        // pr($_POST);
        $tablesArr = $this->explodeVal($_POST['tables']);
        $tableName = $tablesArr['1'];
        foreach ($_POST['field_name'] as $value) {
            $ret = $this->explodeVal($value);
            $arrayColumnName[] = $ret[0];
        }
        $searchArray = array();
        if (isset($_POST['choose_field'])) {
            for ($i = 0; $i < count($_POST['choose_field']); $i++) {
                if (!empty($_POST['choose_field'][$i]) && !empty($_POST['operator'][$i]) && !empty($_POST['conditionsearch'][$i])) {
                    $_field = $this->explodeVal($_POST['choose_field'][$i]);
                    $choose_field = $_field[0];
                    $conditions = $_field[1];
                    $searchArray[] = array("choose_field" => $choose_field, "conditions" => $conditions, "operator" => $_POST['operator'][$i], "conditionsearch" => $_POST['conditionsearch'][$i]);
                }
            }
        }

        $permalink = array(2);
        $columnName = implode(',', $arrayColumnName);
        $_SESSION['report']['column_name'] = $columnName;
        $column_order = $arrayColumnName; //set column field database for datatable orderable        

        if ($tableName == 'events') {
            $order = array('ev.event_id' => 'desc'); // default order 
            if ($this->input->post('client')) {
                $this->db->where_in('ev.partner_id', $this->input->post('client'));
            }
            if ($this->input->post('tool')) {
                $this->db->where_in('gt.gatewaytype_uuid', $this->input->post('tool'));
            }
            if ($this->input->post('start_date') != "" && $this->input->post('end_date') != "") {
                $this->db->where('DATE(ev.event_start_time) >=', $this->input->post('start_date'));
                $this->db->where('DATE(ev.event_start_time) <=', $this->input->post('end_date'));
            }
            $this->db->select($columnName);
            $this->db->from('events as ev');
            $this->db->join('client as clnt', 'clnt.id = ev.client_id');
            $this->db->join('client as part', 'part.id = ev.partner_id');
            $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
            $this->db->join('services as serv', 'serv.service_id = ev.service_id');
            $this->db->join('severities as severity', 'severity.id = ev.severity_id');
            $this->db->join('group as group', 'group.id = ev.domain_id');
            $this->db->join('client_gateways as cg', 'cg.id = ev.gateway_id');
            $this->db->join('gateway_types as gt', 'gt.gatewaytype_uuid = cg.gateway_type');
            $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');
            $this->db->where('ev.event_end_time', '0000-00-00 00:00:00');
            $this->db->join('tickets tkt', 'tkt.ticket_id = ev.ticket_id', 'LEFT');
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('ev.client_id', $this->accessLevelClient['resultSet']);
            }

            foreach ($searchArray as $value) {
                if ($value['conditions'] == 'OR') {
                    $this->db->or_where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                } else {
                    $this->db->where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                }
            }

            //$this->db->where_not_in('ev.severity_id', $permalink);
        } else if ($tableName == 'tickets') {
            $order = array('t.ticket_id' => 'desc'); // default order 
            if ($this->input->post('client')) {
                $this->db->where_in('t.patner_id', $this->input->post('client'));
            }
            if ($this->input->post('tool')) {
                $this->db->where_in('gt.gatewaytype_uuid', $this->input->post('tool'));
            }
            if ($this->input->post('start_date') != "" && $this->input->post('end_date') != "") {
                $this->db->where('DATE(t.created_on) >=', $this->input->post('start_date'));
                $this->db->where('DATE(t.created_on) <=', $this->input->post('end_date'));
            }
            $this->db->select($columnName);
            $this->db->from('tickets t');
            $this->db->join('client as cl', 'cl.id = t.client_id');
            $this->db->join('client as cn', 'cn.id = t.patner_id');
            $this->db->join('contract_types as contrtType', 'contrtType.uuid = t.contract_types_uuid', 'left');
            $this->db->join('ticket_status as ts', 'ts.status_code = t.status_code', 'left');
            $this->db->join('ticket_queue as tq', 'tq.queue_id = t.queue_id', 'left');
            $this->db->join('skill_set as sk', 'sk.skill_id = t.priority', 'left');
            $this->db->join('incident_activity_log as incactlog', 'incactlog.ticket_id = t.ticket_id', 'left');
            $this->db->join('activity_type as actType', 'actType.activity_type_id = incactlog.activity_type_id', 'left');
            $this->db->join('device_categories as divCat', 'divCat.id = t.ticket_cate_id', 'left');
            $this->db->join('device_sub_categories as divSubCat', 'divSubCat.id = t.ticket_sub_cate_id', 'left');
            $this->db->join('eventedge_ticket_types as tti', 'tti.ticket_type_id = t.ticket_type_id', 'left');
            $this->db->join('eventedge_tickets_category as ttc', 'ttc.ticket_cate_id = t.ticket_cate_id', 'left');
            $this->db->join('eventedge_ticket_sub_category as tsc', 'tsc.ticket_sub_cate_id = t.ticket_sub_cate_id', 'left');
            $this->db->join('contacts as conts', 'conts.contact_uuid = t.requestor', 'left');
            $this->db->join('contact_emails as contsemail', 'contsemail.reference_uuid = conts.contact_uuid', 'left');
            $this->db->join('client_locations as clntLoc', 'clntLoc.location_uuid = t.client_loaction', 'left');
            $this->db->join('users as usr', 'usr.id = t.created_by', 'left');
            $this->db->join('users as us', 'us.id = t.assigned_to', 'left');
            $this->db->join('devices as devi', 'devi.device_id = t.device_id', 'left');
            $this->db->join('client_gateways as cg', 'cg.id = devi.gateway_id', 'left');
            $this->db->join('gateway_types as gt', 'gt.gatewaytype_uuid = cg.gateway_type', 'left');
            $this->db->join('severities as SV', 'SV.id = t.severity_id', 'left');
            $this->db->where('t.ticket_type_id', '1');
            $this->db->group_start();
            $this->db->or_where('incactlog.is_revision', 'N');
            $this->db->or_where('incactlog.is_revision  IS NULL');
            $this->db->group_end();
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('t.client_id', $this->accessLevelClient['resultSet']);
            }

            foreach ($searchArray as $value) {

                $isIntegerCheck = $this->isIntegerCheck($value['conditionsearch']);
                //pr($value);

                if ($isIntegerCheck == 'Y') {
                    if ($value['conditions'] == 'OR') {
                        $this->db->or_where($value['choose_field'] . ' ' . $value['operator'] . ' `' . $value['conditionsearch'] . '`');
                    } else {
                        $this->db->where($value['choose_field'] . ' ' . $value['operator'] . ' ' . $value['conditionsearch'] . ' ');
                    }
                } else {
                    if ($value['conditions'] == 'OR') {
                        if ($value['operator'] == 'LIKE') {
                            $this->db->or_like($value['choose_field'], $value['conditionsearch']);
                        } else {
                            $this->db->or_where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                        }
                    } else {
                        if ($value['operator'] == 'LIKE') {
                            $this->db->like($value['choose_field'], $value['conditionsearch']);
                        } else {
                            $this->db->where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '" ');
                        }
                    }
                }
            }
        } else if ($tableName == 'macd') {
            $order = array('tkt.ticket_id' => 'desc'); // default order 
            if ($this->input->post('client')) {
                $this->db->where_in('tkt.patner_id', $this->input->post('client'));
            }

            if ($this->input->post('start_date') != "" && $this->input->post('end_date') != "") {
                $this->db->where('DATE(mac.created_on) >=', $this->input->post('start_date'));
                $this->db->where('DATE(mac.created_on) <=', $this->input->post('end_date'));
            }

            $this->db->select($columnName);
            $this->db->from('macd mac');
            $this->db->join('client as cl', 'cl.id = mac.client_id');
            $this->db->join('macd_queue as madque', 'madque.request_id = mac.macd_id', 'LEFT');
            //$this->db->join('macd_status as macsts', 'macsts.status_id = madque.macd_status', 'LEFT');
            $this->db->join('users as usr', 'usr.id = madque.reqested_by', 'LEFT');
            $this->db->join('tickets as tkt', 'tkt.ticket_id = mac.ticket_id');
            $this->db->join('client as part', 'part.id = tkt.patner_id');
            $this->db->join('ticket_status as ts', 'ts.status_code = tkt.status_code');
            $this->db->join('incident_activity_log as incactlog', 'incactlog.ticket_id = tkt.ticket_id', 'left');
            $this->db->join('users as ass_usr', 'ass_usr.id = tkt.assigned_to', 'LEFT');
            $this->db->join('client_locations as cntloc_to', 'cntloc_to.location_uuid = mac.location_uuid_to', 'LEFT');
            $this->db->where('tkt.ticket_type_id', '2');
            $this->db->group_start();
            $this->db->or_where('incactlog.is_revision', 'N');
            $this->db->or_where('incactlog.is_revision  IS NULL');
            $this->db->group_end();
			if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('tkt.client_id', $this->accessLevelClient['resultSet']);
            }
            foreach ($searchArray as $value) {
                if ($value['conditions'] == 'OR') {
                    $this->db->or_where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                } else {
                    $this->db->where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                }
            }
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($order)) {
            $order = $order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function isIntegerCheck($int) {
        if (filter_var($int, FILTER_VALIDATE_INT)) {
            return 'Y';
        } else {
            return 'N';
        }
    }

    public function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $save_array = array('tables' => $_POST['tables'], 'client' => $_POST['client'], 'tool' => $_POST['tool'], 'field_name' => $_POST['field_name'], 'start_date' => $_POST['start_date'], 'end_date' => $_POST['end_date'], 'choose_field' => (isset($_POST['choose_field'])) ? $_POST['choose_field'] : '', 'operator' => (isset($_POST['operator'])) ? $_POST['operator'] : '', 'conditionsearch' => (isset($_POST['conditionsearch'])) ? $_POST['conditionsearch'] : '');
        //pr($save_array);exit;

        $_SESSION['report']['save_attr'] = json_encode($save_array);
        //echo $this->db->last_query();
        // exit;
        $_SESSION['report']['query'] = $this->db->last_query();
        return $query->result_array();
    }

    public function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $permalink = array(2);
        $tablesArr = $this->explodeVal($_POST['tables']);
        $tableName = $tablesArr['1'];
        if ($tableName == 'events') {
            $this->db->from('events as ev');
            $this->db->join('client as clnt', 'clnt.id = ev.client_id');
            $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
            $this->db->join('services as serv', 'serv.service_id = ev.service_id');
            $this->db->join('severities as severity', 'severity.id = ev.severity_id');
            $this->db->join('group as group', 'group.id = ev.domain_id');
            $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');
            $this->db->where('ev.event_end_time', '0000-00-00 00:00:00');
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('ev.client_id', $this->accessLevelClient['resultSet']);
            }
            // $this->db->where_not_in('ev.severity_id', $permalink);
        } else if ($tableName == 'tickets') {
            $this->db->from('tickets t');
            $this->db->join('client as cl', 'cl.id = t.client_id');
            $this->db->join('ticket_status as ts', 'ts.status_code = t.status_code');
            $this->db->join('ticket_queue as tq', 'tq.queue_id = t.queue_id');
            $this->db->join('skill_set as sk', 'sk.skill_id = t.priority');
            $this->db->join('eventedge_ticket_types as tti', 'tti.ticket_type_id = t.ticket_type_id');
            $this->db->join('eventedge_tickets_category as ttc', 'ttc.ticket_cate_id = t.ticket_cate_id');
            $this->db->join('eventedge_ticket_sub_category as tsc', 'tsc.ticket_sub_cate_id = t.ticket_sub_cate_id');
            $this->db->join('users as usr', 'usr.id = t.created_by');
            $this->db->join('users as us', 'us.id = t.assigned_to', 'left');
            $this->db->join('events as e', 'e.event_id = t.event_id');
            $this->db->join('severities as SV', 'SV.id = e.severity_id');
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('t.client_id', $this->accessLevelClient['resultSet']);
            }
        } else if ($tableName == 'macd') {
            $this->db->from('macd mac');
            $this->db->join('client as cl', 'cl.id = mac.client_id');
            $this->db->join('macd_queue as madque', 'madque.request_id = mac.macd_id', 'LEFT');
            $this->db->join('macd_status as macsts', 'macsts.status_id = madque.macd_status', 'LEFT');
            $this->db->join('users as usr', 'usr.id = madque.reqested_by', 'LEFT');
            $this->db->join('tickets as tkt', 'tkt.ticket_id = mac.ticket_id');
            $this->db->join('users as ass_usr', 'ass_usr.id = tkt.assigned_to', 'LEFT');
            $this->db->join('client_locations as cntloc_to', 'cntloc_to.location_uuid = mac.location_uuid_to', 'LEFT');
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('mac.client_id', $this->accessLevelClient['resultSet']);
            }
        }

        return $this->db->count_all_results();
    }

    public function getAndOrConditionColumn($tableId = "") {
        $arrayRet = $this->explodeVal($tableId);
        $t_id = $arrayRet['0'];
        $this->db->select('condition_search_field,alias_name,field_datatype,f_id');
        $this->db->from('report_fields');
        $this->db->where('is_status', 'Y');
        $this->db->where('is_condition_column', 'Y');
        $this->db->where('t_id', $t_id);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $data = $query->result();
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }//pr($return);exit;
        return $return;
    }

    public function report_save() {
        $postData['report_name'] = $_POST['title'];
        $postData['report_status'] = isset($_POST['reportStatus']) ? $_POST['reportStatus'] : 'M';
        $postData['save_attr'] = $_SESSION['report']['save_attr'];
        $postData['client_id'] = $_SESSION['client_id'];
        $postData['created_by'] = $_SESSION['id'];
        $this->db->insert('report_save', $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function fetchMySaveReport() {
        //pr($_SESSION)
        //pr($this->accessLevelClient['resultSet']);exit;

        $this->db->select('report_name,id');
        $this->db->from('report_save');
        $this->db->where('is_status', 'Y');
        if ($this->accessLevelClient['status'] == 'true') {
            $this->db->where_in('client_id', $this->accessLevelClient['resultSet']);
        }
        $this->db->where('report_status', 'M');
        $this->db->where('created_by', $this->userId);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        //echo $this->db->last_query();exit

        if ($query->num_rows() > 0) {
            $data = $query->result();
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function fetchGlobalSaveReport() {
        $this->db->select('report_name,id');
        $this->db->from('report_save');
        $this->db->where('is_status', 'Y');
        $this->db->where('report_status', 'G');
        if ($this->accessLevelClient['status'] == 'true') {
            $this->db->where_in('client_id', $this->accessLevelClient['resultSet']);
        }
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data = $query->result();
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getSaveReportBasedOnID() {
        if ($_POST['savereportsid']) {
            $this->db->select('id,column_alias');
            $this->db->from('report_save');
            $this->db->where('is_status', 'Y');
            $this->db->where('id', $_POST['savereportsid']);
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('client_id', $this->accessLevelClient['resultSet']);
            }
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $data = $query->row();
                $return['status'] = 'true';
                $return['resultSet'] = $data;
            } else {
                $return['status'] = 'false';
            }
            return $return;
        }
    }

    private function _get_datatables_saveReport() {
        $save_id = $_POST['save_id'];
        $getAttr = $this->getSaveAttr($save_id);

        $rep_attr = $getAttr['attr'];

        //pr($rep_attr);
        $tablesArr = $this->explodeVal($rep_attr->tables);
        $tableName = $tablesArr['1'];
        foreach ($rep_attr->field_name as $value) {
            $ret = $this->explodeVal($value);
            $arrayColumnName[] = $ret[0];
        }

        $searchArray = array();
        if (isset($rep_attr->choose_field)) {
            for ($i = 0; $i < count($rep_attr->choose_field); $i++) {
                if (!empty($rep_attr->choose_field[$i]) && !empty($rep_attr->operator[$i]) && !empty($rep_attr->conditionsearch[$i])) {
                    $_field = $this->explodeVal($rep_attr->choose_field[$i]);
                    $choose_field = $_field[0];
                    $conditions = $_field[1];
                    $searchArray[] = array("choose_field" => $choose_field, "conditions" => $conditions, "operator" => $rep_attr->operator[$i], "conditionsearch" => $rep_attr->conditionsearch[$i]);
                }
            }
        }

        $permalink = array(2);
        $columnName = implode(',', $arrayColumnName);
        $_SESSION['report']['column_name'] = $columnName;
        $column_order = $arrayColumnName; //set column field database for datatable orderable        

        if ($tableName == 'events') {
            $order = array('ev.event_id' => 'desc'); // default order 
            if ($rep_attr->client) {
                $this->db->where_in('ev.partner_id', $rep_attr->client);
            }

            if ($rep_attr->tool) {
                $this->db->where_in('gt.gatewaytype_uuid', $rep_attr->tool);
            }
            if ($rep_attr->start_date != "" && $rep_attr->end_date != "") {
                $this->db->where('DATE(ev.event_start_time) >=', $rep_attr->start_date);
                $this->db->where('DATE(ev.event_start_time) <=', $rep_attr->end_date);
            }


            $this->db->select($columnName);
            $this->db->from('events as ev');
            $this->db->join('client as clnt', 'clnt.id = ev.client_id');
            $this->db->join('client as part', 'part.id = ev.partner_id');
            $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
            $this->db->join('services as serv', 'serv.service_id = ev.service_id');
            $this->db->join('severities as severity', 'severity.id = ev.severity_id');
            $this->db->join('group as group', 'group.id = ev.domain_id');
            $this->db->join('client_gateways as cg', 'cg.id = ev.gateway_id');
            $this->db->join('gateway_types as gt', 'gt.gatewaytype_uuid = cg.gateway_type');
            $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');
            $this->db->where('ev.event_end_time', '0000-00-00 00:00:00');
            $this->db->join('tickets tkt', 'tkt.ticket_id = ev.ticket_id', 'LEFT');
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('ev.client_id', $this->accessLevelClient['resultSet']);
            }

            foreach ($searchArray as $value) {
                if ($value['conditions'] == 'OR') {
                    if ($value['operator'] == 'LIKE') {
                        $this->db->or_like($value['choose_field'], $value['conditionsearch']);
                    } else {
                        $this->db->or_where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                    }
                } else {
                    if ($value['operator'] == 'LIKE') {
                        $this->db->like($value['choose_field'], $value['conditionsearch']);
                    } else {
                        $this->db->where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                    }
                }
            }
        } else if ($tableName == 'tickets') {
            $order = array('t.ticket_id' => 'desc'); // default order 
            if ($rep_attr->client) {
                $this->db->where_in('t.patner_id', $rep_attr->client);
            }
            if ($rep_attr->tool) {
                $this->db->where_in('gt.gatewaytype_uuid', $rep_attr->tool);
            }

            if ($rep_attr->start_date != "" && $rep_attr->end_date != "") {
                $this->db->where('DATE(t.created_on) >=', $rep_attr->start_date);
                $this->db->where('DATE(t.created_on) <=', $rep_attr->end_date);
            }

            $this->db->select($columnName);
            $this->db->from('tickets t');
            $this->db->join('client as cl', 'cl.id = t.client_id');
            $this->db->join('client as cn', 'cn.id = t.patner_id');
            $this->db->join('contract_types as contrtType', 'contrtType.uuid = t.contract_types_uuid', 'left');
            $this->db->join('ticket_status as ts', 'ts.status_code = t.status_code', 'left');
            $this->db->join('ticket_queue as tq', 'tq.queue_id = t.queue_id', 'left');
            $this->db->join('skill_set as sk', 'sk.skill_id = t.priority', 'left');
            $this->db->join('incident_activity_log as incactlog', 'incactlog.ticket_id = t.ticket_id', 'left');
            $this->db->join('activity_type as actType', 'actType.activity_type_id = incactlog.activity_type_id', 'left');
            $this->db->join('device_categories as divCat', 'divCat.id = t.ticket_cate_id', 'left');
            $this->db->join('device_sub_categories as divSubCat', 'divSubCat.id = t.ticket_sub_cate_id', 'left');
            $this->db->join('eventedge_ticket_types as tti', 'tti.ticket_type_id = t.ticket_type_id', 'left');
            $this->db->join('eventedge_tickets_category as ttc', 'ttc.ticket_cate_id = t.ticket_cate_id', 'left');
            $this->db->join('eventedge_ticket_sub_category as tsc', 'tsc.ticket_sub_cate_id = t.ticket_sub_cate_id', 'left');
            $this->db->join('contacts as conts', 'conts.contact_uuid = t.requestor', 'left');
            $this->db->join('contact_emails as contsemail', 'contsemail.reference_uuid = conts.contact_uuid', 'left');
            $this->db->join('client_locations as clntLoc', 'clntLoc.location_uuid = t.client_loaction', 'left');
            $this->db->join('users as usr', 'usr.id = t.created_by', 'left');
            $this->db->join('users as us', 'us.id = t.assigned_to', 'left');
            $this->db->join('devices as devi', 'devi.device_id = t.device_id', 'left');
            $this->db->join('client_gateways as cg', 'cg.id = devi.gateway_id', 'left');
            $this->db->join('gateway_types as gt', 'gt.gatewaytype_uuid = cg.gateway_type', 'left');
            $this->db->join('severities as SV', 'SV.id = t.severity_id', 'left');
            $this->db->where('t.ticket_type_id', '1');
            $this->db->group_start();
            $this->db->or_where('incactlog.is_revision', 'N');
            $this->db->or_where('incactlog.is_revision  IS NULL');
            $this->db->group_end();
            if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('t.client_id', $this->accessLevelClient['resultSet']);
            }

            foreach ($searchArray as $value) {
                if ($value['conditions'] == 'OR') {
                    if ($value['operator'] == 'LIKE') {
                        $this->db->or_like($value['choose_field'], $value['conditionsearch']);
                    } else {
                        $this->db->or_where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                    }
                } else {
                    if ($value['operator'] == 'LIKE') {
                        $this->db->like($value['choose_field'], $value['conditionsearch']);
                    } else {
                        $this->db->where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                    }
                }
            }
        } else if ($tableName == 'macd') {
            $order = array('tkt.ticket_id' => 'desc'); // default order 
            if ($rep_attr->client) {
                $this->db->where_in('tkt.patner_id', $rep_attr->client);
            }

            if ($rep_attr->start_date != "" && $rep_attr->end_date != "") {
                $this->db->where('DATE(mac.created_on) >=', $rep_attr->start_date);
                $this->db->where('DATE(mac.created_on) <=', $rep_attr->end_date);
            }


            $this->db->select($columnName);
            $this->db->from('macd mac');
            $this->db->join('client as cl', 'cl.id = mac.client_id');
            $this->db->join('macd_queue as madque', 'madque.request_id = mac.macd_id', 'LEFT');
            //$this->db->join('macd_status as macsts', 'macsts.status_id = madque.macd_status', 'LEFT');
            $this->db->join('users as usr', 'usr.id = madque.reqested_by', 'LEFT');
            $this->db->join('tickets as tkt', 'tkt.ticket_id = mac.ticket_id');
            $this->db->join('client as part', 'part.id = tkt.patner_id');
            $this->db->join('ticket_status as ts', 'ts.status_code = tkt.status_code');
            $this->db->join('incident_activity_log as incactlog', 'incactlog.ticket_id = tkt.ticket_id', 'left');
            $this->db->join('users as ass_usr', 'ass_usr.id = tkt.assigned_to', 'LEFT');
            $this->db->join('client_locations as cntloc_to', 'cntloc_to.location_uuid = mac.location_uuid_to', 'LEFT');
            $this->db->where('tkt.ticket_type_id', '2');
            $this->db->group_start();
            $this->db->or_where('incactlog.is_revision', 'N');
            $this->db->or_where('incactlog.is_revision  IS NULL');
            $this->db->group_end();
			if ($this->accessLevelClient['status'] == 'true') {
                $this->db->where_in('tkt.client_id', $this->accessLevelClient['resultSet']);
            }
            foreach ($searchArray as $value) {
                if ($value['conditions'] == 'OR') {
                    if ($value['operator'] == 'LIKE') {
                        $this->db->or_like($value['choose_field'], $value['conditionsearch']);
                    } else {
                        $this->db->or_where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                    }
                } else {
                    if ($value['operator'] == 'LIKE') {
                        $this->db->like($value['choose_field'], $value['conditionsearch']);
                    } else {
                        $this->db->where($value['choose_field'] . ' ' . $value['operator'] . ' "' . $value['conditionsearch'] . '"');
                    }
                }
            }
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($order)) {
            $order = $order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables_saveReport() {
        $this->_get_datatables_saveReport();
        //pr($_POST);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        // echo $this->db->last_query(); exit;   
        $_SESSION['report']['query'] = $this->db->last_query();

        return $query->result_array();
    }

    public function count_filtered_saveReport() {
        $this->_get_datatables_saveReport();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_saveReport() {
        $permalink = array(2);
        $this->db->from('events as ev');
        $this->db->join('client as clnt', 'clnt.id = ev.client_id');
        $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
        $this->db->join('services as serv', 'serv.service_id = ev.service_id');
        $this->db->join('severities as severity', 'severity.id = ev.severity_id');
        $this->db->join('group as group', 'group.id = ev.domain_id');
        $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');
        $this->db->where('ev.event_end_time', '0000-00-00 00:00:00');
        // $this->db->where_not_in('ev.severity_id', $permalink);


        return $this->db->count_all_results();
    }

    public function getGatewayTypes() {
        $this->db->select('gatewaytype_uuid,title');
        $this->db->from($this->gateway_types);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
//        echo $this->db->last_query();
//        exit;
        if ($query->num_rows() > 0) {
            $data = $query->result();
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }//pr($return);exit;
        return $return;
    }

    public function getSaveAttr($saveId = "") {
        $this->db->select('report_name,save_attr,id');
        $this->db->from('report_save');
        $this->db->where('is_status', 'Y');
        $this->db->where('id', $saveId);
        if ($this->accessLevelClient['status'] == 'true') {
            $this->db->where_in('client_id', $this->accessLevelClient['resultSet']);
        }
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data = $query->row();
            // pr($data);exit;
            $return['status'] = 'true';
            $return['save_id'] = $data->id;
            $return['report_name'] = $data->report_name;
            $return['attr'] = json_decode($data->save_attr);
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getReportColumn_save($tableId = "") {
        $arrayRet = $this->explodeVal($tableId);
        $t_id = $arrayRet['0'];
        // pr($_POST);
        $saveSelectColumn = array();
        if (isset($_POST['saveid'])) {
            $saveId = decode_url($_POST['saveid']);
            $this->db->select('save_attr');
            $this->db->from('report_save');
            $this->db->where('is_status', 'Y');
            $this->db->where('id', $saveId);
            $this->db->order_by('id', 'DESC');
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                $data = $query->row();
                $attr = json_decode($data->save_attr);

                foreach ($attr->field_name as $key => $value) {
                    $x = $this->explodeVal($value);
                    $saveSelectColumn[] = $x[2];
                }
            }
        }
        $this->db->select('column_name,alias_name,is_selected_column,f_id');
        $this->db->from('report_fields');
        $this->db->where('is_status', 'Y');
        $this->db->where('t_id', $t_id);
        if (!empty($saveSelectColumn)) {
            $this->db->where_in('f_id', $saveSelectColumn);
        } else {
            $this->db->where('is_selected_column', 'Y');
        }
        $querySelected = $this->db->get();


        $this->db->select('column_name,alias_name,is_selected_column,f_id');
        $this->db->from('report_fields');
        $this->db->where('is_status', 'Y');
        $this->db->where('t_id', $t_id);
        if (!empty($saveSelectColumn)) {
            $this->db->where_not_in('f_id', $saveSelectColumn);
        } else {
            $this->db->where('is_selected_column', 'N');
        }
        $queryAvailable = $this->db->get();

        $return['available'] = array();
        $return['selected'] = array();
        if ($queryAvailable->num_rows() > 0) {
            $return['available'] = $queryAvailable->result();
        }
        if ($querySelected->num_rows() > 0) {
            $return['selected'] = $querySelected->result();
        }


        return $return;
    }

    public function edit_report() {
        $save_id = decode_url($_POST['save_id']);
        $save_array = array('tables' => $_POST['tables'], 'client' => $_POST['client'], 'tool' => $_POST['tool'], 'field_name' => $_POST['field_name'], 'start_date' => $_POST['start_date'], 'end_date' => $_POST['end_date'], 'choose_field' => (isset($_POST['choose_field'])) ? $_POST['choose_field'] : '', 'operator' => (isset($_POST['operator'])) ? $_POST['operator'] : '', 'conditionsearch' => (isset($_POST['conditionsearch'])) ? $_POST['conditionsearch'] : '');
        $save_attr = json_encode($save_array);
        $data = array('save_attr' => $save_attr);
        $this->db->where('id', $save_id);
        if ($this->db->update('report_save', $data)) {
            $return['status'] = "true";
        } else {
            $return['status'] = "false";
        }
        return $return;
    }

    public function delete_report() {
        $save_id = decode_url($_POST['save_id']);
        $this->db->where('id', $save_id);
        if ($this->db->delete('report_save')) {
            $return['status'] = "true";
        } else {
            $return['status'] = "false";
        }
        return $return;
    }

}

?>