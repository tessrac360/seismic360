<!-- END PAGE HEADER-->
<style>
.input-group input + span.error{
	position: absolute;
    right: 0;
    z-index: 9;
    text-align: center;
    top: 34px;
    background: #af6d6d;
    color: #fff;
    padding: 2px 6px;
    width: 65%;
    font-size: 12px;
}
.input-group input + span.error:before {
    content: '';
    position: absolute;
    border-style: solid;
    border-width:0 5px 5px;
    border-color: #af6d6d transparent;
    display: block;
    width: 0;
    z-index: 1;
    top: -5px;
    right: 55px;
}
</style>
<div class="row">
    <div>
		
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Device Template</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('nagiosmanagements/devicetemplates') ?>">Device Template</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmDeviceTemplate" id="frmDeviceTemplate" method="post" action="">
                    <div class="form-body">
                        <div class="row">
						<div class="col-md-6">
						<div class="form-group form-md-line-input form-md-floating-label">
						<input class="form-control" id="name" name="name" type="text" tableName='devices_templates' tableField='name' gateway_id="<?php //echo $gateway_id?>">
						<label for="form_control_1">Template Name</label>                                               
						</div>
						</div>
						<div class="col-md-12">
							<div class="caption font-dark" style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px;">
								<span class="caption-subject bold uppercase"> Template Data</span>
							</div>
						</div>						
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="check_period" id="check_period" checked>
									<span>Check Period</span>
								</label>
								<input type="text" class="form-control" name="host_check_period" id="host_check_period"  value="24x7"> 
							</div>
						</div>					
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked value="check_interval" id="check_interval">
									<span>Check Interval</span>
								</label>
								<input type="text" class="form-control" name="host_check_interval" id="host_check_interval"  value="5"> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked value="retry_interval" id="retry_interval">
									<span>Retry Interval</span>
								</label>
								<input type="text" class="form-control" name="host_retry_interval" id="host_retry_interval"  value="1"> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" checked name="hostoptions[]" value="max_check_attempts" id="max_check_attempts">
									<span>Max Check Attempts</span>
								</label>
								<input type="text" class="form-control" name="host_max_check_attempts" id="host_max_check_attempts"  value="10"> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked value="check_command" id="check_command">
									<span>Check Command</span>
								</label>
								<input type="text" class="form-control" name="host_check_command" id="host_check_command"  value="check-host-alive"> 
							</div>
						</div>	

						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" checked name="hostoptions[]" value="notification_period" id="notification_period">
									<span>Notification Period</span>
								</label>
								<input type="text" class="form-control" name="host_notification_period" id="host_notification_period"  value="workhours"> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked value="notification_interval" id="notification_interval">
									<span>Notification Interval</span>
								</label>
								<input type="text" class="form-control" name="host_notification_interval" id="host_notification_interval"  value="120"> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked value="notification_options" id="notification_options">
									<span>Notification Options</span>
								</label>
								<input type="text" class="form-control" name="host_notification_options" id="host_notification_options"  value="d,u,r"> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="process_perf_data">
									<span>Process perf data</span>
								</label>
								<input type="text" class="form-control" name="host_process_perf_data" id="host_process_perf_data" disabled value="0"> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="retain_nonstatus_information">
									<span>Retain nonstatus information</span>
								</label>
								<input type="text" class="form-control" name="host_retain_nonstatus_information" id="host_retain_nonstatus_information" disabled value="0"> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="active_checks_enabled">
									<span>Active checks enabled</span>
								</label>
								<input type="text" class="form-control" name="host_active_checks_enabled" id="host_active_checks_enabled" disabled value="0"> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="passive_checks_enabled">
									<span>Passive checks enabled</span>
								</label>
								<input type="text" class="form-control" name="host_passive_checks_enabled" id="host_passive_checks_enabled" disabled value="0"> 
							</div>
						</div>						

						
						
						
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('nagiosmanagements/devicetemplates'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $('.hostoptions').click(function(){
			$id = this.value;
            if($(this).is(":checked")){
				$("#host_"+$id).prop("disabled", false);
            }
            else if($(this).is(":not(:checked)")){

				$("#host_"+$id).prop("disabled", true);
            }
        });
		$('#check_period').click(function(){ $("#host_check_period").prop("disabled", false);return false; });
		$('#check_interval').click(function(){ $("#host_check_interval").prop("disabled", false);return false; });
		$('#retry_interval').click(function(){ $("#host_retry_interval").prop("disabled", false);return false; });
		$('#max_check_attempts').click(function(){ $("#host_max_check_attempts").prop("disabled", false);return false; });
		$('#check_command').click(function(){ $("#host_check_command").prop("disabled", false);return false; });
		$('#notification_period').click(function(){ $("#host_notification_period").prop("disabled", false);return false; });
		$('#notification_interval').click(function(){ $("#host_notification_interval").prop("disabled", false);return false; });
		$('#notification_options').click(function(){ $("#host_notification_options").prop("disabled", false);return false; });
		//$('#check_period').click(function(){ $("#host_check_period").prop("disabled", false);return false; });
		//$('#max_check_attempts').click(function(){ $("#host_max_check_attempts").prop("disabled", false);return false; });		
    });
</script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_devicetemplates.js" type="text/javascript"></script>