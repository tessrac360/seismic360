<style>
.template_data .input-group-addon{width:70%; white-space: inherit; text-align:left;background-color: #f5f4f4;}
.template_data .input-group-addon input[type="checkbox"]{top: 2px; position: relative;}
</style>

<div class="col-md-12">
	<div class="caption font-dark" style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px;">
		<span class="caption-subject bold uppercase"> Template Data</span>
	</div>
</div>
<?php foreach($ajax_host_template as $key => $value){?>
<?php if(!empty($value)){
	if($key =="devicegroups"){
		$key = "hostgroups";
	}
	?>
<div class="col-md-4 template_data">
	<div class="input-group">
		<label class="input-group-addon">
			<input type="checkbox" class="hostoptions" name="hostoptions[]" value="<?php echo $key;?>">
			<span><?php echo ucwords(str_replace('_',' ',$key));?></span>
		</label>
		<input type="text" class="form-control" name="host_<?php echo $key;?>" id="host_<?php echo $key;?>" disabled value="<?php echo $value;?>"> 
	</div>
</div>
<?php }?>
<?php }?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.hostoptions').click(function(){
			$id = this.value;
            if($(this).is(":checked")){
				$("#host_"+$id).prop("disabled", false);
            }
            else if($(this).is(":not(:checked)")){

				$("#host_"+$id).prop("disabled", true);
            }
        });
    });
</script>