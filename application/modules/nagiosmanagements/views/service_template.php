<style>
.template_data .input-group-addon{width:80%; white-space: inherit; text-align:left;background-color: #f5f4f4;}
</style>

<div class="col-md-12">
	<div class="caption font-dark" style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px;">
		<span class="caption-subject bold uppercase"> Template Data</span>
	</div>
</div>
<?php foreach($ajax_service_template as $key => $value){?>
<?php if(!empty($value)){?>
<div class="col-md-4 template_data">
	<div class="input-group">
		<label class="input-group-addon">
			<input type="checkbox" class ="serviceoptions" name="serviceoptions[]" value="<?php echo $key;?>">
			<span><?php echo ucwords(str_replace('_',' ',$key));?></span>
		</label>
		<input type="text" class="form-control" id="service_<?php echo $key;?>" name="service_<?php echo $key;?>" disabled value="<?php echo $value;?>"> 
	</div>
</div>
<?php }?>
<?php }?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.serviceoptions').click(function(){
			$id = this.value;
            if($(this).is(":checked")){
				$("#service_"+$id).prop("disabled", false);
            }
            else if($(this).is(":not(:checked)")){

				$("#service_"+$id).prop("disabled", true);
            }
        });
    });
</script>
