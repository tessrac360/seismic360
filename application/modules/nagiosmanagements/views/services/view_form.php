<?php
$accessAdd = helper_fetchPermission('51', 'add');
$accessEdit = helper_fetchPermission('51', 'edit');
$accessStatus = helper_fetchPermission('51', 'active');
?>
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Service Configurations</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php //echo base_url().'client/gateway/getGateway/'.encode_url($hosts['resultSet']['gateway_id']); ?>">Gateways</a>                            
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php //echo base_url('client') ?>">Host Configurations</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<a href="<?php //echo base_url('client') ?>">Service Configurations</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
											
					<li>
						<span>List</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('nagiosmanagements/createService/'.encode_url($gateway_id).'/'.encode_url($device_id)); ?>"> Add New Service
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
								
                            <!--<div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php //echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php //echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>			
                

                <table class="table table-striped table-bordered table-hover tree" id="role">
                    <thead>
						<tr>
							<th> Service Description </th>
							<th> Command </th>
							<th> Action </th> 

							<!--<th> Actions </th>-->
											   
						</tr>
					</thead>
					<tbody>
                    <?php 
                    if ($services['status'] == 'true') {
                        foreach ($services['resultSet'] as $service_data) {
                            //pr($file);exit;
                    ?>
                    <tr>
						<td><?php echo $service_data['service_description'];?></td>
						<td><?php echo $service_data['check_command'];?></td>

						<td>
							<!--<a href="<?php //echo base_url() . 'nagiosmanagements/view/' . encode_url($host_data['id']).'/'. encode_url($gateway_id); ?>" class="btn btn-xs  green" data-toggle="tooltip" title="View">
                                <i class="fa fa-eye"></i>
                            </a>
							<a href="<?php //echo base_url() . 'nagiosmanagements/edit/' . encode_url($host_data['id']).'/'. encode_url($gateway_id); ?>" class="btn btn-xs  blue" data-toggle="tooltip" title="Edit">
                                <i class="fa fa-edit"></i>
                            </a>
						</td>
						 <!--<td>
							<a href="<?php //echo base_url() . 'nagiosmanagements/nagios_config/' . encode_url($value['id']) ; ?>">
								<button class="btn blue" style=" padding: 2px 2px;font-size:12px;" id="addsubclient" type="button">Manage Services</button>
							</a>													
						</td>-->
                    </tr>		
						<?php
						}
					}else{
						?>
                    <tr>
                        <td colspan="5"> No Record Found </td>
                    </tr> 
					<?php } ?>
					</tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <!--<?php //if($search ==''){echo $this->pagination->create_links();}  ?>-->
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_logs.js" type="text/javascript"></script>