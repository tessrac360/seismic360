<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Service</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <a href="<?php echo base_url('client') ?>">Client</a>                           
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <a href="<?php //echo base_url('client/gateway/getGateway') ?>">Gateways</a>                           
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <a href="<?php //echo base_url('users/admin') ?>">Host Configurations</a>                           
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <a href="<?php //echo base_url('users/admin') ?>">Service Configurations</a>                           
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
				<!-- Host Form -->
				<?php if($allservices == 'n'){?>
                <form role="form" name="frmService" id="frmService" method="post" action="" autocomplete="off">
                    <div class="form-body">
					
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption font-dark" style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px;">
                                    <span class="caption-subject bold uppercase"> Services Information</span>
                                </div>
                            </div>							
							<div class="form-group" >
							<?php
							foreach ($unique_services['resultSet'] as $value){
								if(!isset($gatewayservicelist[$value['service_id']])){
									if($value['service_description']!="Host Check"){
							?>	
								<div class="col-sm-3" >

										<label class="mt-checkbox mt-checkbox-outline">
											<input type="checkbox" class='groupcheck' name ="serviceslist[]" value="<?php echo $value['service_id']; ?>" ><?php echo $value['service_description']; ?>
											<span></span>
										</label>										
								
								</div>	
							<?php }}} ?>								
								
							</div>
							
							<div class="clearfix"></div>
							<div class="col-md-12">
                                <div class="caption font-dark" style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px;">
                                    <span class="caption-subject bold uppercase"> Services Templates</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="service_template_id" name="service_template_id" >
                                        <option value=""></option>
                                        <?php
                                        if ($service_templates['status'] == 'true') {
                                            foreach ($service_templates['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['service_template_id']; ?>"><?php echo $value['name']; ?></option>  
                                                <?php
                                            }
                                       }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Select Service Templates<span class="required" aria-required="true">*</span></label>
                                </div>
                            </div>													                                             							
							<div id="service_template_list"></div>
							
                        </div>

                        <div class="form-actions noborder">
                            <input type="hidden" name="type" value="service_from" >
                            <button type="submit" class="btn green">Save</button>

                        </div>
						</div>

                    </div>
                </form>
				<?php }else{?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption font-dark" style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px;">
                                    <span class=""> All Services Assigned </span>
									<a href="<?php echo base_url('nagiosmanagements/service_config/'. encode_url($gateway_id).'/'. encode_url($device_id)); ?>">Back</a>
                                </div>
                            </div>	
						</div>							
				<?php }?>
				
            </div>
        </div>
    </div>
</div>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}
</script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_config.js" type="text/javascript"></script>