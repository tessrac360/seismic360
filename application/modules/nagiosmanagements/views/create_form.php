<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-ipaddress-master/bootstrap-ipaddress.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-ipaddress-master/bootstrap-ipaddress.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<style>
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
	margin:10px;
}
.accordion::after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}
.accordion.active::after {
    content: "\2212";
}

.accordion.active, .accordion:hover {
    background-color: #ddd; 
}

div.panel {
    padding: 0 18px;
    display: none;
    background-color: white;
}
</style>
<style>
.input-group input + span.error{
	position: absolute;
    right: 0;
    z-index: 9;
    text-align: center;
    top: 34px;
    background: #af6d6d;
    color: #fff;
    padding: 2px 6px;
    width: 65%;
    font-size: 12px;
}
.input-group input + span.error:before {
    content: '';
    position: absolute;
    border-style: solid;
    border-width:0 5px 5px;
    border-color: #af6d6d transparent;
    display: block;
    width: 0;
    z-index: 1;
    top: -5px;
    right: 55px;
}
</style>
<div class="row">
    <div>
	
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Device</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <a href="<?php echo base_url('client') ?>">Client</a>                           
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <a href="<?php //echo base_url('client/gateway/getGateway') ?>">Gateways</a>                           
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <a href="<?php //echo base_url('users/admin') ?>">Device Configurations</a>                           
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
				
				<!-- Device Form -->
                <form role="form" name="frmHost" id="frmHost" method="post" action="" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption font-dark"style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px;">
                                    <span class="caption-subject bold uppercase"> Device Information</span>
                                </div>
                            </div>
							<div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="device_name" name="device_name" type="text" tableName='devices' tableField='device_name' gateway_id="<?php echo $gateway_id;?>">
                                    <label for="form_control_1">Device Name<span class="required" aria-required="true" >*</span></label>  
                                    <!--<span id="errmsg" class="error" style="display: none;"></span>-->
                                </div>
                            </div>
							<div class="col-md-4 ">
                                <div class="form-group form-md-line-input form-md-floating-label" id="IpDemo">
                                    <input class="form-control" id="address" name="address" type="text" tableName='devices' tableField='address' gateway_id="<?php echo $gateway_id;?>" value="">
                                    <label for="form_control_1">Device Address<span class="required"  aria-required="true">*</span></label>                                               
                                </div>
                            </div>	
							<div class="col-md-4 ">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  class="form-control" id="alias" name="alias" type="text" >
                                    <label for="form_control_1">Device Alias Name<span class="required" aria-required="true"></span></label>                                               
                                </div>
                            </div>							
							
							<div class="clearfix"></div>
							
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="device_parent_id" name="device_parent_id" >
										<option value=""></option>
                                        <?php
                                        if ($deviceslist['status'] == 'true') {
                                            foreach ($deviceslist['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['device_id']; ?>"><?php echo $value['device_name']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Device Parent</label>
                                </div>
                            </div>
							
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="device_category_id" name="device_category_id" >
										<option value=""></option>
                                        <?php
                                        if ($categories['status'] == 'true') {
                                            foreach ($categories['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['device_category_id']; ?>"><?php echo $value['category']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Device Category</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="priority_id" name="priority_id" >
										<option value=""></option>
                                        <?php
                                        if ($priorities['status'] == 'true') {
                                            foreach ($priorities['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['skill_id']; ?>"><?php echo $value['title']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Device Priority</label>
                                </div>
                            </div>						
							<div class="clearfix"></div>
							
							<div class="col-md-12">
                                <div class="caption font-dark"style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px; margin-top:15px;">
                                    <span class="caption-subject bold uppercase">Device Template</span>
                                </div>
                            </div>
							
	                        <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="device_template_id" name="device_template_id" >
                                        <option value=""></option>
                                        <?php
                                        if ($host_templates['status'] == 'true') {
                                            foreach ($host_templates['resultSet'] as $value) {
												//if($value['use_temp'] !=''){
                                                ?>
                                               <option value="<?php echo $value['device_template_id']; ?>"><?php echo $value['name']; ?></option>  
                                                <?php
												//}
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Select Device Template<span class="required" aria-required="true">*</span></label>
                                </div>
                            </div>						
						<div class="col-md-12" style="margin-top: 10px;margin-bottom:0px;">	
						<div id="host_template_list"></div>
						</div>
						 <div class="col-md-8 " style="margin-top: 10px;">
                        <div class="form-actions noborder">
                            <input type="hidden" name="type" value="service_from" >
						
                            <button type="submit" class="btn green">Save</button>


                        </div>	
						</div>						
                        </div>

                    </div>
                </form>
				
            </div>
        </div>
    </div>
</div>

<script>
  $(document).ready(function () {
     //$('#IpDemo').ipaddress();
     $('input[name=address]').ipaddress();
     $('#IpDemo').ipaddress();
});  
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}
</script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_config.js" type="text/javascript"></script>