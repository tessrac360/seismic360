
<!-- END PAGE HEADER-->

<div class="row">
    <div>
		
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Service Template</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php //echo base_url('nagiosmanagements/devicetemplates') ?>">Service Template</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmServiceTemplate" id="frmServiceTemplate" method="post" action="">
                    <div class="form-body">
                        <div class="row">
						<div class="col-md-6">
						<div class="form-group form-md-line-input form-md-floating-label">
						<input class="form-control" id="name" name="name" type="text" tableName='services_templates' tableField='name' gateway_id="<?php //echo $gateway_id?>" value="<?php echo $name?>">
						<label for="form_control_1">Template Name</label>                                               
						</div>
						</div>
						<div class="col-md-12">
							<div class="caption font-dark" style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px;">
								<span class="caption-subject bold uppercase"> Template Data</span>
							</div>
						</div>						
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" id="active_checks_enabled" name="hostoptions[]" value="active_checks_enabled" checked>
									<span>Active Checks Enabled</span>
								</label>
								<input type="text" class="form-control"  name="host_active_checks_enabled" id="host_active_checks_enabled"  <?php if(isset($selected_values['active_checks_enabled'])){ ?> value="<?php echo $selected_values['active_checks_enabled'];?>" <?php }else{?>  value="1" <?php }?>> 
							</div>
						</div>					
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked value="passive_checks_enabled" id="passive_checks_enabled">
									<span>passive_checks_enabled</span>
								</label>
								<input type="text" class="form-control" name="host_passive_checks_enabled" id="host_passive_checks_enabled"  <?php if(isset($selected_values['passive_checks_enabled'])){ ?> value="<?php echo $selected_values['passive_checks_enabled'];?>" <?php }else{?>  value="1" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked value="parallelize_check" id="parallelize_check">
									<span>parallelize_check</span>
								</label>
								<input type="text" class="form-control" name="host_parallelize_check" id="host_parallelize_check"  <?php if(isset($selected_values['parallelize_check'])){ ?> value="<?php echo $selected_values['parallelize_check'];?>" <?php }else{?>  value="1" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" checked name="hostoptions[]" value="obsess_over_service" id="obsess_over_service">
									<span>obsess_over_service</span>
								</label>
								<input type="text" class="form-control" name="host_obsess_over_service" id="host_obsess_over_service"  <?php if(isset($selected_values['obsess_over_service'])){ ?> value="<?php echo $selected_values['obsess_over_service'];?>" <?php }else{?>  value="1" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked value="check_freshness" id="check_freshness">
									<span>check_freshness</span>
								</label>
								<input type="text" class="form-control" name="host_check_freshness" id="host_check_freshness"   <?php if(isset($selected_values['check_freshness'])){ ?> value="<?php echo $selected_values['check_freshness'];?>" <?php }else{?>  value="0" <?php }?>> 
							</div>
						</div>	

						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" checked name="hostoptions[]" value="notifications_enabled" id="notifications_enabled">
									<span>notifications_enabled</span>
								</label>
								<input type="text" class="form-control" name="host_notifications_enabled" id="host_notifications_enabled"  <?php if(isset($selected_values['notifications_enabled'])){ ?> value="<?php echo $selected_values['notifications_enabled'];?>" <?php }else{?>  value="1" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked value="event_handler_enabled" id="event_handler_enabled">
									<span>event_handler_enabled</span>
								</label>
								<input type="text" class="form-control" name="host_event_handler_enabled" id="host_event_handler_enabled"   <?php if(isset($selected_values['event_handler_enabled'])){ ?> value="<?php echo $selected_values['event_handler_enabled'];?>" <?php }else{?>  value="1" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked value="flap_detection_enabled" id="flap_detection_enabled">
									<span>flap_detection_enabled</span>
								</label>
								<input type="text" class="form-control" name="host_flap_detection_enabled" id="host_flap_detection_enabled"  <?php if(isset($selected_values['flap_detection_enabled'])){ ?> value="<?php echo $selected_values['flap_detection_enabled'];?>" <?php }else{?>  value="1" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="process_perf_data" <?php if(isset($selected_values['process_perf_data'])) echo "checked"?>>
									<span>Process perf data</span>
								</label>
								<input type="text" class="form-control" name="host_process_perf_data" id="host_process_perf_data" <?php if(isset($selected_values['process_perf_data'])){ ?> value="<?php echo $selected_values['process_perf_data'];?>" <?php }else{?> disabled value="1" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="retain_status_information" <?php if(isset($selected_values['retain_status_information'])) echo "checked"?>>
									<span>retain_status_information</span>
								</label>
								<input type="text" class="form-control" name="host_retain_status_information" id="host_retain_status_information" <?php if(isset($selected_values['retain_status_information'])){ ?> value="<?php echo $selected_values['retain_status_information'];?>" <?php }else{?> disabled value="1" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="retain_nonstatus_information" <?php if(isset($selected_values['retain_nonstatus_information'])) echo "checked"?>>
									<span>retain_nonstatus_information</span>
								</label>
								<input type="text" class="form-control" name="host_retain_nonstatus_information" id="host_retain_nonstatus_information" <?php if(isset($selected_values['retain_nonstatus_information'])){ ?> value="<?php echo $selected_values['retain_nonstatus_information'];?>" <?php }else{?> disabled value="1" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="is_volatile" <?php if(isset($selected_values['is_volatile'])) echo "checked"?>>
									<span>is_volatile</span>
								</label>
								<input type="text" class="form-control" name="host_is_volatile" id="host_is_volatile" <?php if(isset($selected_values['is_volatile'])){ ?> value="<?php echo $selected_values['is_volatile'];?>" <?php }else{?> disabled value="0" <?php }?>> 
							</div>
						</div>

						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" checked id ="check_period" value="check_period">
									<span>check_period</span>
								</label>
								<input type="text" class="form-control" name="host_check_period" id="host_check_period"<?php if(isset($selected_values['check_period'])){ ?> value="<?php echo $selected_values['check_period'];?>" <?php }else{?>  value="24x7" <?php }?>> 
							</div>
						</div>

						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="max_check_attempts" checked id ="max_check_attempts">
									<span>max_check_attempts</span>
								</label>
								<input type="text" class="form-control" name="host_max_check_attempts" id="host_max_check_attempts"  <?php if(isset($selected_values['max_check_attempts'])){ ?> value="<?php echo $selected_values['max_check_attempts'];?>" <?php }else{?>  value="3" <?php }?>> 
							</div>
						</div>

						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="check_interval" <?php if(isset($selected_values['check_interval'])) echo "checked"?>>
									<span>check_interval</span>
								</label>
								<input type="text" class="form-control" name="host_check_interval" id="host_check_interval"<?php if(isset($selected_values['check_interval'])){ ?> value="<?php echo $selected_values['check_interval'];?>" <?php }else{?> disabled value="10" <?php }?>> 
							</div>
						</div>

						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="retry_interval" <?php if(isset($selected_values['retry_interval'])) echo "checked"?>>
									<span>retry_interval</span>
								</label>
								<input type="text" class="form-control" name="host_retry_interval" id="host_retry_interval" <?php if(isset($selected_values['retry_interval'])){ ?> value="<?php echo $selected_values['retry_interval'];?>" <?php }else{?> disabled value="2" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="notification_options">
									<span>notification_options</span>
								</label>
								<input type="text" class="form-control" name="host_notification_options" id="host_notification_options" <?php if(isset($selected_values['notification_options'])){ ?> value="<?php echo $selected_values['notification_options'];?>" <?php }else{?> disabled value="w,u,c,r" <?php }?>> 
							</div>
						</div>	
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="notification_interval" <?php if(isset($selected_values['notification_interval'])) echo "checked"?>>
									<span>notification_interval</span>
								</label>
								<input type="text" class="form-control" name="host_notification_interval" id="host_notification_interval"  <?php if(isset($selected_values['notification_interval'])){ ?> value="<?php echo $selected_values['notification_interval'];?>" <?php }else{?> disabled value="60" <?php }?>> 
							</div>
						</div>
						<div class="col-md-4 template_data">
							<div class="input-group">
								<label class="input-group-addon">
									<input type="checkbox" class="hostoptions" name="hostoptions[]" value="notification_period" <?php if(isset($selected_values['notification_period'])) echo "checked"?>>
									<span>notification_period</span>
								</label>
								<input type="text" class="form-control" name="host_notification_period" id="host_notification_period"  <?php if(isset($selected_values['notification_period'])){ ?> value="<?php echo $selected_values['notification_period'];?>" <?php }else{?> disabled value="24x7" <?php }?>> 
							</div>
						</div>						
						

						
						
						
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('severities'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $('.hostoptions').click(function(){
			$id = this.value;
            if($(this).is(":checked")){
				$("#host_"+$id).prop("disabled", false);
            }
            else if($(this).is(":not(:checked)")){

				$("#host_"+$id).prop("disabled", true);
            }
        });
		$('#active_checks_enabled').click(function(){ $("#host_active_checks_enabled").prop("disabled", false);return false; });
		$('#passive_checks_enabled').click(function(){ $("#host_passive_checks_enabled").prop("disabled", false);return false; });
		$('#parallelize_check').click(function(){ $("#host_parallelize_check").prop("disabled", false);return false; });
		$('#obsess_over_service').click(function(){ $("#host_obsess_over_service").prop("disabled", false);return false; });
		$('#check_freshness').click(function(){ $("#host_check_freshness").prop("disabled", false);return false; });
		$('#notifications_enabled').click(function(){ $("#host_notifications_enabled").prop("disabled", false);return false; });
		$('#event_handler_enabled').click(function(){ $("#host_event_handler_enabled").prop("disabled", false);return false; });
		$('#flap_detection_enabled').click(function(){ $("#host_flap_detection_enabled").prop("disabled", false);return false; });
		$('#check_period').click(function(){ $("#host_check_period").prop("disabled", false);return false; });
		$('#max_check_attempts').click(function(){ $("#host_max_check_attempts").prop("disabled", false);return false; });
		
		//$('#process_perf_data').click(function(){ $("#host_process_perf_data").prop("disabled", false);return false; });
		//$('#retain_status_information').click(function(){ $("#host_retain_status_information").prop("disabled", false);return false; });
		//$('#retain_nonstatus_information').click(function(){ $("#host_retain_nonstatus_information").prop("disabled", false);return false; });
    });
</script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_servicetemplates.js" type="text/javascript"></script>