<style>
.template_data .input-group-addon{width:50%; white-space: inherit; text-align:left;background-color: #f5f4f4;}
</style>
<div style="height: 300px;overflow: auto;" >
<div class="col-md-12">
	<div class="caption font-dark" style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px;">
		<span class="caption-subject bold uppercase"> Services List</span>
	</div>
</div>

<div id="serviceslisterror" style="color:red;margin:5px;"></div>	
<?php
if ($ajax_services_list['status'] == 'true') {
	foreach ($ajax_services_list['resultSet'] as $value) {
?>

<div class="col-md-6  template_data">
	<div class="input-group col-md-12">
		<label class="input-group-addon">
			<input type="checkbox" class ="services" name="services[]" value="<?php echo $value['service_id'];?>">
			<span><?php echo $value['service_description'];?></span>
		</label>
		<?php if($gateway_type == 1){?>
		<select class="form-control  servicetemp" id="services_<?php echo $value['service_id'];?>" name="services_<?php echo $value['service_id'];?>" >
			<option value=""></option>
			<?php
			if ($ajax_service_templates['status'] == 'true') {
				foreach ($ajax_service_templates['resultSet'] as $value) {
					?>
				   <option value="<?php echo $value['service_template_id']; ?>"><?php echo $value['name']; ?></option>  
					<?php
				}
			}
			?> 
		</select> 
		<?php }?>
	</div>
</div>
<?php }} else{?>
No Services Found


<?php }?>	
</div>