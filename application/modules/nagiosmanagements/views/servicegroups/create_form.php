
<!-- END PAGE HEADER-->

<div class="row">
    <div>
		
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Service Group</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('nagiosmanagements/servicegroups/'); ?>">Service Group</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmServiceGroup" id="frmServiceGroup" method="post" action="">
                    <div class="form-body">
                        <div class="row"> 						

                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="device_category_id" name="device_category_id" >
										<option value=""></option>
                                        <?php
                                        if ($categories['status'] == 'true') {
                                            foreach ($categories['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['id']; ?>"><?php echo $value['category']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Device Category</label>
                                </div>
                            </div>							
							<div class="col-md-6">
								<div class="form-group form-md-line-input form-md-floating-label">
									<select class="form-control" id="gateway_type_uuid" name="gateway_type_uuid" >
										<option value=""></option>
										<?php
										if ($gatewayTypes['status'] == 'true') {
											foreach ($gatewayTypes['resultSet'] as $value) {
												if($value['title'] !='NA'){
												?>
											   <option value="<?php echo $value['gatewaytype_uuid']; ?>"><?php echo $value['title']; ?></option>  
												<?php
												}
											}
										}
										?> 
									</select> 								
									<label for="form_control_1">Gateway Type</label>                                               
								</div>
							</div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="service_group_name" name="service_group_name" type="text" tableName='severities' tableField='severity'>
                                    <label for="form_control_1">Service Group Name</label>                                               
                                </div>
                            </div>	                            
                            <div class="col-sm-6">
                                
                               
                                    <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                        <input type="checkbox" id="default_group"  name ="default_group" value ="1"> Default Group For Device Category
                                        <span></span>
                                    </label>
                               
                            </div>							
							<div class="clearfix"></div>
							<div id="services_list" ></div>							
	
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('nagiosmanagements/servicegroups/'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
				
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script src="<?php echo base_url() . "public/" ?>js/form/form_servicegroups.js" type="text/javascript"></script>

<script>
$('#device_category_id').on('change', function () {
	var device_category_id = this.value;
	var gateway_type_uuid = $('#gateway_type_uuid').val();
	var baseURL = $('#baseURL').val();

	$.ajax({
		type: 'POST',
		url: baseURL + 'nagiosmanagements/ajax_default_service_groups_check',
		dataType: 'json',
		data: {'device_category_id': device_category_id,'gateway_type_uuid':gateway_type_uuid},
		success: function (response) {
			if (response.status == 'true') {
				$('#default_group').attr('checked', false);				
			} else if (response.status == 'false') {
				$('#default_group').attr('checked', true);
			}
		}
	});	

});	


</script>





