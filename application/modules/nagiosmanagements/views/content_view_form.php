<?php //echo $getStatus['file_content'];?>
<div class="row">
    <div>
		
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">View File</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Nagios</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('nagiosmanagements/nagios_config'. encode_url($gateway_id)) ?>">Nagios Config</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>View</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmFile" id="frmFile" method="post" action="">
                    <div class="form-body">
                        <div class="row">                                                                            
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
									<label for="form_control_1">Config File Content:</label>
                                    <pre><?php echo $getStatus['file_content'] ; ?></pre>
								</div>
                            </div>
							
						</div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>




