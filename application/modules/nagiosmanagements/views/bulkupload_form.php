<div class="row">
    <div>
		<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<span class="caption-subject font-green-steel bold uppercase">Devices Bulk Upload</span>
		</div>
		<div class="page-toolbar">
			 <ul class="page-breadcrumb breadcrumb custom-bread">
				<li>
					<i class="fa fa-cog"></i>
					<span>Device Bulk Upload</span>  
					<i class="fa fa-angle-right"></i>
				</li>
			</ul>
		</div>
		<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmClient" id="frmClient" method="post" action="" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <label for="form_control_1">1) Column names must be Device Title,Client,Gateway,Device Category,Service Group</label>                                               
                                </div>
                            </div>	
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <label for="form_control_1">2) Uploaded file must be in csv format</label>                                               
                                </div>
                            </div>							
							<div class="col-md-12">
							<div class="form-actions noborder">
								<input type="file" name="userfile" ><br><br>
								<input type="submit" name="submit" value="UPLOAD" class="btn btn-primary">
								
							</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
	</div>
</div>
