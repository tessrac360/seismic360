<?php
//error_reporting(0);
error_reporting(E_ALL);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class NagiosManagements extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
		$this->getDomainId = helper_getUserDefaultGroup();
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_nagios_config");
        $this->load->library('form_validation');
		$this->load->library('Csvimport');
    }

    public function index() {
		$context = new ZMQContext();
		//  Socket to talk to server
		$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
		$requester->connect("tcp://10.10.32.35:5555");
		$send_req = array("msg"=>"Get Nagios Response files","data"=>"");
		$send_json_req = json_encode($send_req);
		$requester->send("Get Nagios Response files");
		$reply = $requester->recv();
		$str = array();
		$str = json_decode($reply,true);
		foreach($str as $nagiosfiles){
			//print_r($str);
			$resultData = $this->Model_nagios_config->insertConfigfiles($nagiosfiles);
			if ($resultData['status'] == 'true') {
				
			}
		}
    }
	public function nagios_config($gateway_id = null) {
		$gateway_id = decode_url($gateway_id);
		$data['gateway_id'] = $gateway_id;
		$hostlist = array();
		$data['hosts'] = $this->Model_nagios_config->getDevices($gateway_id);
		if($data['hosts']['status'] == 'true'){
			foreach($data['hosts']['resultSet'] as $host_data){
				$hostlist [$host_data['device_id']] = $host_data['device_name'];
			}
			$data['host_ids'] = $hostlist;
		}
		//pr($data['hosts']);exit;
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }
    public function create($gateway_id = null){
		$gateway_id = decode_url($gateway_id);		
		$res = $this->Model_nagios_config->getGatewayDetails($gateway_id);	
		$client_id = $res['resultSet']['client_id'];
		$context = new ZMQContext();
		//  Socket to talk to server
		$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
		$requester->connect("tcp://".$res['resultSet']['ip_address'].":5560");	
		
		$data['host_templates'] = $this->Model_nagios_config->getHostTemplates(array('gateway_id'=>$gateway_id));
		$data['service_templates'] = $this->Model_nagios_config->getServiceTemplates(array('gateway_id'=>$gateway_id));
		$data['groups'] = $this->Model_nagios_config->getGroups($gateway_id);
		
		
		//pr($data['templates']);exit();
		if ($this->input->post()) {
			$postHost = $this->input->post();
			//pr($postHost);exit();
			$this->form_validation->set_rules('device_template_id', 'Use Template', 'required');
            $this->form_validation->set_rules('device_name', 'Host Name', 'required');
            $this->form_validation->set_rules('address', 'Host Address', 'required');
            //$this->form_validation->set_rules('devicegroup_id', 'Host Group', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
			if ($this->form_validation->run() == TRUE) { 
			    $resHosttName = $this->Model_nagios_config->getDeviceTemplateName($postHost['device_template_id'],$gateway_id);	
				//exit;
			    $resHostgName = $this->Model_nagios_config->getDeviceGroupName($postHost['devicegroup_id']);
				
			    //$resServicetName = $this->Model_nagios_config->getServiceTemplateName($postHost['service_template_id']);			
				$hostTemplateName = $resHosttName['resultSet']['name'];
				$hostGroupName = $resHostgName['resultSet']['devicegroup_name'];
				//$serviceTemplateName = $resServicetName['resultSet']['name'];	

				$file_path = '/usr/local/nagios/etc/objects/all.cfg';
					
				$hostData['gateway_id'] =$gateway_id;
				$hostData['device_template_id'] = $postHost['device_template_id'];
				$hostData['device_name'] = $postHost['device_name'];
				$hostData['alias'] = $postHost['alias'];
				$hostData['address'] = $postHost['address'];				
				$hostData['device_category_id'] = $postHost['device_category_id'];
				$hostData['device_parent_id'] = $postHost['device_parent_id'];
				$hostData['priority_id'] = $postHost['priority_id'];				
				$hostData['client_id'] = $client_id;
				$hostData['domain_id'] = "1";
				//$hostData['ports'] = $postHost['ports'];
				//$hostData['devicegroup_id'] = $postHost['devicegroup_id'];
				$hostData['file_path'] = $file_path;
				
				$config_template_data = "define host{\n";
				$config_template_data .= "\tuse\t".$hostTemplateName."\n";
				$config_template_data .= "\thost_name\t".$postHost['device_name']."\n";
				$config_template_data .= "\talias\t".$postHost['alias']."\n";
				$config_template_data .= "\taddress\t".$postHost['address']."\n";	
				if($hostGroupName !=""){
					$config_template_data .= "\thostgroups\t".$hostGroupName."\n";							
				}
				$hostoptions = array();
				if(isset($postHost['hostoptions'])){
					for($i=0;$i<count($postHost['hostoptions']);$i++){
						if($postHost['hostoptions'][$i] =="hostgroups"){
							$hostData['devicegroups'] = $postHost['host_'.$postHost['hostoptions'][$i]];
						}else{
							$hostData[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
						}
						$hostoptions[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
						
						$config_template_data .= "\t".$postHost['hostoptions'][$i]."\t".$postHost['host_'.$postHost['hostoptions'][$i]]."\n";	
					}
				}
				$hostData['selected_values'] = serialize($hostoptions);
				$config_template_data .= "\t}\n";
				//pr($postHost);
				//echo "<pre>";
				//echo $config_template_data;
				//echo "</pre>";exit;
				
			    //pr($hostData);exit;
				$resultData = $this->Model_nagios_config->insertDevices($hostData);	
				if ($resultData['status'] == 'true') {
					$lastInsertId = $resultData['lastId'];	
					//echo $lastInsertId;				
					//echo "<pre>";					
					//echo $config_template_data;
					$send_req = array("msg"=>"Adding Hosts","data"=>$config_template_data,"file_path"=>$file_path);
					$send_json_req = json_encode($send_req);
					$requester->send($send_json_req);						
					//echo "</pre>";
					//exit;
					$this->session->set_flashdata("success_msg", "Host added successfully in database and nagios server");
					redirect('nagiosmanagements/nagios_config/'.encode_url($gateway_id));
				}
			}else{
				$this->session->set_flashdata("error_msg", "Some thing went wrong");
				//redirect('nagiosmanagements/nagios_config');
			}
		}	
		//pr($data['file_edit_content']);exit;
		$data['gateway_id'] = $gateway_id;
		$data['categories'] = $this->Model_nagios_config->getDeviceCategories($client_id);		
		$data['deviceslist'] = $this->Model_nagios_config->getDevices($gateway_id);		
		$data['priorities'] = $this->Model_nagios_config->getPriorities();
		//pr($data['deviceslist']);exit;		
		$data['file'] = 'create_form';
        $this->load->view('template/front_template', $data);
	}
	
	public function service_config($gateway_id = null, $device_id = null) {
		$device_id = decode_url($device_id);
		$data['device_id'] = $device_id;
		$gateway_id = decode_url($gateway_id);
		$data['gateway_id'] = $gateway_id;		
		$data['services'] = $this->Model_nagios_config->getServices($device_id);
		//pr($data['services']);exit;
        $data['file'] = 'services/view_form';
        $this->load->view('template/front_template', $data);
    }
    public function createService($gateway_id = null,$device_id = null){
		$device_id = decode_url($device_id);
		$gateway_id = decode_url($gateway_id);
		$res_devices = $this->Model_nagios_config->getDeviceDetails($device_id );
		if($res_devices['status'] == 'true'){
			$file_path = $res_devices['resultSet']['file_path'];
		}
		$res = $this->Model_nagios_config->getGatewayDetails($gateway_id);
		$client_id = $res['resultSet']['client_id'];
		$gateway_type = $res['resultSet']['gateway_type'];
		$context = new ZMQContext();
		//  Socket to talk to server
		$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
		$requester->connect("tcp://".$res['resultSet']['ip_address'].":5560");
		$data['service_templates'] = $this->Model_nagios_config->getServiceTemplates(array('gateway_id'=>$gateway_id));
		//pr($data['templates']);exit();
		 if ($this->input->post()) {
			$postHost = $this->input->post();
			//pr($postHost);exit();
			$this->form_validation->set_rules('service_template_id', 'Use Template', 'required');
            //$this->form_validation->set_rules('service_description', 'Service Description', 'required');
            //$this->form_validation->set_rules('check_command', 'Command', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
			if ($this->form_validation->run() == TRUE) { 
			    //$resHosttName = $this->Model_nagios_config->getDeviceTemplateName($postHost['device_template_id']);	
			    //$resHostgName = $this->Model_nagios_config->getDeviceGroupName($postHost['devicegroup_id']);	
			    $resServicetName = $this->Model_nagios_config->getServiceTemplateName($postHost['service_template_id'],$gateway_id);	
				//$hostTemplateName = $resHosttName['resultSet']['name'];
				//$hostGroupName = $resHostgName['resultSet']['devicegroup_name'];
				$config_template_data ="";
				foreach($postHost['serviceslist'] as $service_id){
					$resUniqueService = $this->Model_nagios_config->getUniqueServiceDetail($service_id);
					//pr($resUniqueService);exit;
					$serviceTemplateName = $resServicetName['resultSet']['name'];				
					$serviceData['gateway_id'] =$gateway_id;
					$serviceData['service_template_id']= $postHost['service_template_id'];
					$serviceData['client_id'] = $client_id;
					$serviceData['service_id'] = $service_id;
					$serviceData['device_id']= $device_id;
					$serviceData['domain_id'] = "1";
					$serviceData['service_description']= $resUniqueService['resultSet']['service_description'];
					$serviceData['check_command']= $resUniqueService['resultSet']['check_command_with_arg'];
					$serviceData['file_path'] = $file_path;
					
					$config_template_data .= "define service{\n";
					$config_template_data .= "\tuse\t".$serviceTemplateName."\n";
					$config_template_data .= "\thost_name\t".$res_devices['resultSet']['device_name']."\n";
					$config_template_data .= "\tservice_description\t".$resUniqueService['resultSet']['service_description']."\n";
					$config_template_data .= "\tcheck_command\t".$resUniqueService['resultSet']['check_command_with_arg']."\n";
					$serviceoptions = array();
					if(isset($postHost['serviceoptions'])){
						for($k=0;$k<count($postHost['serviceoptions']);$k++){
							$serviceData[$postHost['serviceoptions'][$k]] = $postHost['service_'.$postHost['serviceoptions'][$k]];
							$config_template_data .= "\t".$postHost['serviceoptions'][$k]."\t".$postHost['service_'.$postHost['serviceoptions'][$k]]."\n";
							$serviceoptions[$postHost['serviceoptions'][$k]] = $postHost['service_'.$postHost['serviceoptions'][$k]];
						}
					}
					$config_template_data .= "\t}\n";
					$serviceData['selected_values'] = serialize($serviceoptions);
					$result = $this->Model_nagios_config->insertService($serviceData);
				}				
				//echo "<pre>";					
				//echo $config_template_data;exit;

				$send_req = array("msg"=>"Adding Hosts","data"=>$config_template_data,"file_path"=>$file_path);
				$send_json_req = json_encode($send_req);
				$requester->send($send_json_req);						
				//echo "</pre>";
				//exit;
				$this->session->set_flashdata("success_msg", "Services added successfully in database and nagios server");
				redirect('nagiosmanagements/service_config/'.encode_url($gateway_id).'/'.encode_url($device_id));
			}else{
				$this->session->set_flashdata("error_msg", "Some thing went wrong");
				redirect('nagiosmanagements/service_config');
			}
		}	
		//pr($data['file_edit_content']);exit; 
		$data['device_id'] = $device_id;
		$data['allservices'] = "n";
		$gatewayservicelist =array();
		$data['unique_services'] = $this->Model_nagios_config->getUniqueServices($gateway_type);
		$data['services'] = $this->Model_nagios_config->getServices($device_id);
		if($data['services']['status'] == 'true'){
			foreach($data['services']['resultSet'] as $service_data){
				$gatewayservicelist [$service_data['service_id']] = 'ok';
			}			
		}
		if($data['unique_services']['status'] == 'true' && $data['services']['status'] == 'true' ){
		   	if(count($data['unique_services']['resultSet']) == count($data['services']['resultSet'])+1 ){
				$data['allservices'] = "y";	
			}
		}
		$data['gateway_id'] = $gateway_id;
		$data['device_id'] = $device_id;
		$data['gatewayservicelist'] = $gatewayservicelist;
		$data['file'] = 'services/create_form';
        $this->load->view('template/front_template', $data);
	}	


    public function edit($postId = NULL,$gateway_id = null){
		$device_id = decode_url($postId);
		$gateway_id = decode_url($gateway_id);		
		$res = $this->Model_nagios_config->getGatewayDetails($gateway_id);	
		$client_id = $res['resultSet']['client_id'];
		$context = new ZMQContext();
		//  Socket to talk to server
		$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
		$requester->connect("tcp://".$res['resultSet']['ip_address'].":5560");			
		$data['host_templates'] = $this->Model_nagios_config->getHostTemplates(array('gateway_id'=>$gateway_id));
		$data['service_templates'] = $this->Model_nagios_config->getServiceTemplates(array('gateway_id'=>$gateway_id));
		$data['groups'] = $this->Model_nagios_config->getGroups($gateway_id);
		$data['device_details'] = $this->Model_nagios_config->getDeviceDetails($device_id);		
		$templateDatabyId = $this->Model_nagios_config->getHostTemplates(array('device_template_id'=>$data['device_details']['resultSet']['device_template_id']));
		$data['ajax_host_template'] = array();
		$data['selected_values'] = array();
		if (isset($templateDatabyId['resultSet'][0])) {
			foreach ($templateDatabyId['resultSet'][0] as $key => $rowVal) {
				$unset = array('device_template_id','name','use_temp','alias','display_name','address','client_id','domain_id','gateway_id','file_path','register');
				if(!in_array($key,$unset))
				{
					$data['ajax_host_template'][$key] = $rowVal;	
				}
			}
		}
		$data['selected_values'] = unserialize($data['device_details']['resultSet']['selected_values']);
		//pr($data['selected_values']);exit;
		$file_path = $data['device_details']['resultSet']['file_path'];	
	
		if ($this->input->post()) {
			$postHost = $this->input->post();
			//pr($postHost);exit();
			$this->form_validation->set_rules('device_template_id', 'Use Template', 'required');
            $this->form_validation->set_rules('device_name', 'Host Name', 'required');
            $this->form_validation->set_rules('address', 'Host Address', 'required');
            //$this->form_validation->set_rules('devicegroup_id', 'Host Group', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
			if ($this->form_validation->run() == TRUE) {
				$hostGroupName = "";
			    $resHosttName = $this->Model_nagios_config->getDeviceTemplateName($postHost['device_template_id'],$gateway_id);	
				//exit;
			    $resHostgName = $this->Model_nagios_config->getDeviceGroupName($data['device_details']['resultSet']['selected_values']);
				
			    //$resServicetName = $this->Model_nagios_config->getServiceTemplateName($postHost['service_template_id']);			
				$hostTemplateName = $resHosttName['resultSet']['name'];
				if ($resHostgName['status'] == 'true') {
					$hostGroupName = $resHostgName['resultSet']['devicegroup_name'];
				}
				//$serviceTemplateName = $resServicetName['resultSet']['name'];	
				//$file_path = '/usr/local/nagios/etc/objects/all.cfg';
					
				$hostData['gateway_id'] =$gateway_id;
				$hostData['device_template_id'] = $postHost['device_template_id'];
				$hostData['device_name'] = $postHost['device_name'];
				$hostData['alias'] = $postHost['alias'];
				$hostData['address'] = $postHost['address'];				
				$hostData['device_category_id'] = $postHost['device_category_id'];
				$hostData['device_parent_id'] = $postHost['device_parent_id'];
				$hostData['priority_id'] = $postHost['priority_id'];				
				$hostData['client_id'] = $client_id;
				$hostData['domain_id'] = "1";
				//$hostData['ports'] = $postHost['ports'];
				//$hostData['devicegroup_id'] = $postHost['devicegroup_id'];
				$hostData['file_path'] = $file_path;
				$hostoptions = array();
				if(isset($postHost['hostoptions'])){
					for($i=0;$i<count($postHost['hostoptions']);$i++){
						if($postHost['hostoptions'][$i] =="hostgroups"){
							$hostData['devicegroups'] = $postHost['host_'.$postHost['hostoptions'][$i]];
						}else{
							$hostData[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
						}
						$hostoptions[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];					
					}
				}
				$hostData['selected_values'] = serialize($hostoptions);

				//pr($postHost);
				//echo "<pre>";
				//echo $config_template_data;
				//echo "</pre>";exit;
				
			    //pr($hostData);exit;
				//exit;
				$resultData = $this->Model_nagios_config->updateDevice($hostData,$device_id);	
				if ($resultData['status'] == 'true') {	
					//echo $lastInsertId;	
					$config_template_data = $this->get_cfg_template($gateway_id,$file_path);
					//echo "<pre>";
					//echo $config_template_data;
					$send_req = array("msg"=>"edit cfg file","data"=>$config_template_data,"file_path"=>$file_path);
					$send_json_req = json_encode($send_req);
					$requester->send($send_json_req);						
					//echo "</pre>";
					//exit;
					$this->session->set_flashdata("success_msg", "Host updated successfully in database and nagios server");
					redirect('nagiosmanagements/nagios_config/'.encode_url($gateway_id));
				}
			}else{
				$this->session->set_flashdata("error_msg", "Some thing went wrong");
				//redirect('nagiosmanagements/nagios_config');
			}
		}	
		
		$data['gateway_id'] = $gateway_id;
		$data['categories'] = $this->Model_nagios_config->getDeviceCategories($client_id);		
		$data['deviceslist'] = $this->Model_nagios_config->getDevices($gateway_id);		
		$data['priorities'] = $this->Model_nagios_config->getPriorities();
		//pr($data['deviceslist']);exit;		
		$data['file'] = 'update_form';
        $this->load->view('template/front_template', $data);
	}
	
	public function get_cfg_template($gateway_id ="",$file_path = "") {
		$config_template_data ="";
		$HostsData = $this->Model_nagios_config->getHostsCfg($gateway_id,$file_path);
		if ($HostsData['status'] == 'true') {	
			foreach($HostsData['resultSet'] as $HostData){
				$config_template_data .= "define host{\n";
				$config_template_data .= "\tuse\t".$HostData['use']."\n";
				$config_template_data .= "\thost_name\t".$HostData['host_name']."\n";
				$config_template_data .= "\talias\t".$HostData['alias']."\n";
				$config_template_data .= "\taddress\t".$HostData['address']."\n";
				$host_selected = unserialize($HostData['selected_values']);
				foreach($host_selected as $key=>$value){
					
					$config_template_data .= "\t".$key."\t".$value."\n";	
				}
				$config_template_data .= "\t}\n";				
				
			}
		}
		$ServicesData = $this->Model_nagios_config->getSevicesCfg($gateway_id,$file_path);
		//pr($ServicesData);exit;
		if ($ServicesData['status'] == 'true') {	
			foreach($ServicesData['resultSet'] as $ServicesData){

				$config_template_data .= "define service{\n";
				$config_template_data .= "\tuse\t".$ServicesData['use']."\n";
				$config_template_data .= "\thost_name\t".$ServicesData['host_name']."\n";
				$config_template_data .= "\tservice_description\t".$ServicesData['service_description']."\n";
				$config_template_data .= "\tcheck_command\t".$ServicesData['check_command']."\n";
				$service_selected = unserialize($ServicesData['selected_values']);
				if($service_selected){
					foreach($service_selected as $key=>$value){					
						$config_template_data .= "\t".$key."\t".$value."\n";	
					}	
				}
				$config_template_data .= "\t}\n";				
				
			}
		}		
		return $config_template_data;		
	}
	
	
	
	public function view($postId = NULL,$gateway_id = null){
		$gateway_id = decode_url($gateway_id);
		$postId = decode_url($postId);
		$data['getStatus'] = $this->Model_nagios_config->getFileEdit($postId);
		//pr($data['getStatus']);exit;
		if ($this->input->post()){
			$postFile = $this->input->post();
			//pr($postFile);exit();
			$this->form_validation->set_rules('file_content', 'File Content', 'required');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
			if ($this->form_validation->run() == TRUE) { 
				$data['file_edit_content'] = $this->Model_nagios_config->updateConfigfiles($postFile,$postId);
			}else{
				redirect('nagiosmanagements/nagios_config');
			}
		}	
		//pr($data['file_edit_content']);exit;
		$data['gateway_id'] = $gateway_id;
		$data['file'] = 'content_view_form';
        $this->load->view('template/front_template', $data);
	}
	
	public function fetch($gateway_id = NULL) {
		
		$gateway_id = decode_url($gateway_id);
		//$this->Model_nagios_config->deleteGatewayConfig($gateway_id);
		$res = $this->Model_nagios_config->getGatewayDetails($gateway_id);	
		$client_id = $res['resultSet']['client_id'];		
		$context = new ZMQContext();
		//  Socket to talk to server
		$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
		$requester->connect("tcp://".$res['resultSet']['ip_address'].":5560");
		$send_req = array("msg"=>"Get Nagios Response files","data"=>"");
		$send_json_req = json_encode($send_req);
		$requester->send($send_json_req);
		$reply = $requester->recv();
		$str = array();
		$str = json_decode($reply,true);
		$devices_array = array();		
		$devices_templates_array = array();
		$devicegroups_array = array();
		$sevices_array = array();
		$services_template_array = array();
		$commands_array = array();
		
		$count_devices= 0;
		$count_devicegroups= 0;
		$count_devices_templates = 0;
		$count_sevices = 0;
		$count_services_template = 0;
		$count_commands = 0;		
		$is_device = true;
		$is_device_template = true;			
		$is_service = true;
		$is_service_template = true;	
		
		
		foreach($str as $nagiosfiles){
			$extension = "";
			$array = explode('.', $nagiosfiles['file_path']);
			$extension = trim(end($array));
			if($extension == "cfg" ){
				if( preg_match_all ("/define\ host\{(.*?)\}/ims", $nagiosfiles['file_content'], $pat_array_host)){
					foreach($pat_array_host[1] as $hostdata){
						if(preg_match_all ("/(.*?)\n/i",$hostdata,$hostbyline)){						    
							foreach($hostbyline[0] as $eachline){
								$eachline = trim($eachline);								
									//echo $eachline."<br>";
									$device_values = explode(";",trim($eachline));
									if(trim($device_values[0])!=""){
										
										$device_valuess = preg_split ("/\s/", trim($device_values[0]));
										//echo $device_valuess[0]
										$device_field_valuess ="";
										for($i=1;$i<count($device_valuess);$i++){
											$device_field_valuess .= $device_valuess[$i]." ";	
										}	
										$device_field_valuess = trim($device_field_valuess);
										//echo "<pre>";print_r($device_valuess);echo "</pre>";										
										//echo $device_valuess[0]." - ".$device_field_valuess."<br>";
										if($device_valuess[0] == 'host_name'){
											$devices_array[$count_devices]['device_name'] = $device_field_valuess;		
										}elseif($device_valuess[0] == 'hostgroups'){
											$devices_array[$count_devices]['devicegroups'] = $device_field_valuess;
										}elseif($device_valuess[0] == 'use'){
											$devices_array[$count_devices]['use_temp'] = $device_field_valuess;
										}else{
											$devices_array[$count_devices][$device_valuess[0]] = $device_field_valuess;
										}											
										
									}
								
							}							
							$devices_array[$count_devices]['file_path'] = $nagiosfiles['file_path'];
							$devices_array[$count_devices]['client_id'] = $client_id;
							$devices_array[$count_devices]['gateway_id'] = $gateway_id;	
							$devices_array[$count_devices]['domain_id'] = "1";
														
							//$resultData = $this->Model_nagios_config->insertDevices($devices_array);							
							//echo pr($name_array);
							//echo "<hr>";
							
						}
						$count_devices++;
					}
					//echo "<pre>";print_r($pat_array_host[1]);	
				}
				
				if( preg_match_all ("/define\ service\{(.*?)\}/ims", $nagiosfiles['file_content'], $pat_array_host)){
					foreach($pat_array_host[1] as $hostdata){
						if(preg_match_all ("/(.*?)\n/i",$hostdata,$hostbyline)){
						    
							foreach($hostbyline[0] as $eachline){
								$eachline = trim($eachline);								
								//echo $eachline."<br>";
								$device_values = explode(";",trim($eachline));
								if(trim($device_values[0])!=""){
									
									$device_valuess = preg_split ("/\s/", trim($device_values[0]));
									//echo $device_valuess[0]
									$device_field_valuess ="";
									for($i=1;$i<count($device_valuess);$i++){
										$device_field_valuess .= $device_valuess[$i]." ";	
									}	
									$device_field_valuess = trim($device_field_valuess);
									//echo "<pre>";print_r($device_valuess);echo "</pre>";										
									//echo $device_valuess[0]." - ".$device_field_valuess."<br>";
									if($device_valuess[0] == 'host_name'){
										$sevices_array[$count_sevices]['device_name'] = $device_field_valuess;		
									}elseif($device_valuess[0] == 'hostgroups'){
										$sevices_array[$count_sevices]['devicegroups'] = $device_field_valuess;
									}elseif($device_valuess[0] == 'use'){
										$sevices_array[$count_sevices]['use_temp'] = $device_field_valuess;
									}else{
										$sevices_array[$count_sevices][$device_valuess[0]] = $device_field_valuess;
									}											

								}
								
							}
							
							$sevices_array[$count_sevices]['file_path'] = $nagiosfiles['file_path'];
							$sevices_array[$count_sevices]['client_id'] = $client_id;
							$sevices_array[$count_sevices]['gateway_id'] = $gateway_id;
							$sevices_array[$count_sevices]['domain_id'] = "1";							
							//$resultData = $this->Model_nagios_config->insertDevices($devices_array);							
							//echo pr($name_array);
							//echo "<hr>";
							
						}
						$count_sevices++;
					}
				}
				if( preg_match_all ("/define\ hostgroup\{(.*?)\}/ims", $nagiosfiles['file_content'], $pat_array_host)){
					foreach($pat_array_host[1] as $hostdata){
						if(preg_match_all ("/(.*?)\n/i",$hostdata,$hostbyline)){					    
							foreach($hostbyline[0] as $eachline){
								$eachline = trim($eachline);
								
									//echo $eachline."<br>";
									$device_values = explode(";",trim($eachline));
									if(trim($device_values[0])!=""){
										
										$device_valuess = preg_split ("/\s/", trim($device_values[0]));
										//echo $device_valuess[0]
										$device_field_valuess ="";
										for($i=1;$i<count($device_valuess);$i++){
											$device_field_valuess .= $device_valuess[$i]." ";	
										}
										$device_field_valuess = trim($device_field_valuess);
										//echo "<pre>";print_r($device_valuess);echo "</pre>";										
										//echo $device_valuess[0]." - ".$device_field_valuess."<br>";
										if($device_valuess[0] == 'hostgroup_name'){
											$devicegroups_array[$count_devicegroups]['devicegroup_name'] = $device_field_valuess;		
										}elseif($device_valuess[0] == 'hostgroup_members'){
											$devicegroups_array[$count_devicegroups]['devicegroup_members'] = $device_field_valuess;
										}else{
											$devicegroups_array[$count_devicegroups][$device_valuess[0]] = $device_field_valuess;
										}											

									}
								
							}
							
							$devicegroups_array[$count_devicegroups]['file_path'] = $nagiosfiles['file_path'];
							$devicegroups_array[$count_devicegroups]['client_id'] = $client_id;
							$devicegroups_array[$count_devicegroups]['gateway_id'] = $gateway_id;															
							//$resultData = $this->Model_nagios_config->insertDevices($devices_array);							
							//echo pr($name_array);
							//echo "<hr>";
							
						}
						$count_devicegroups++;
					}
				}

				if( preg_match_all ("/define\ command\{(.*?)\}/ims", $nagiosfiles['file_content'], $pat_array_host)){
					foreach($pat_array_host[1] as $hostdata){
						if(preg_match_all ("/(.*?)\n/i",$hostdata,$hostbyline)){					    
							foreach($hostbyline[0] as $eachline){
								$eachline = trim($eachline);
								
									//echo $eachline."<br>";
									$device_values = explode(";",trim($eachline));
									if(trim($device_values[0])!=""){
										
										$device_valuess = preg_split ("/\s/", trim($device_values[0]));
										//echo $device_valuess[0]
										$device_field_valuess ="";
										for($i=1;$i<count($device_valuess);$i++){
											$device_field_valuess .= $device_valuess[$i]." ";	
										}	
										$device_field_valuess = trim($device_field_valuess);
										//echo "<pre>";print_r($device_valuess);echo "</pre>";										
										//echo $device_valuess[0]." - ".$device_field_valuess."<br>";
										if($device_valuess[0] == 'hostgroup_name'){
											$commands_array[$count_commands]['devicegroup_name'] = $device_field_valuess;		
										}elseif($device_valuess[0] == 'hostgroup_members'){
											$commands_array[$count_commands]['devicegroup_members'] = $device_field_valuess;
										}else{
											$commands_array[$count_commands][$device_valuess[0]] = $device_field_valuess;
										}											

									}
								
							}
							
							$commands_array[$count_commands]['file_path'] = $nagiosfiles['file_path'];
							$commands_array[$count_commands]['client_id'] = $client_id;
							$commands_array[$count_commands]['gateway_id'] = $gateway_id;															
							//$resultData = $this->Model_nagios_config->insertDevices($devices_array);							
							//echo pr($name_array);
							//echo "<hr>";
							
						}
						$count_commands++;
					}
				}				
				//echo "<pre>";print_r($pat_array_service[1]);echo "</pre>";
				//$resultData = $this->Model_nagios_config->insertConfigfiles($nagiosfiles); 
			}

		}
		
		for($i=0;$i<count($commands_array);$i++){		
			//if(isset($commands_array[$i]['name'])){				
				$resultData = $this->Model_nagios_config->insertCommands($commands_array[$i]);	
			//}
		}
		//pr($commands_array);exit;
		for($i=0;$i<count($devicegroups_array);$i++){			
			if(isset($devicegroups_array[$i]['devicegroup_name'])){				
				$resultData = $this->Model_nagios_config->insertDeviceGroups($devicegroups_array[$i]);
			}
		}
		
		for($i=0;$i<count($devices_array);$i++){		
			if(isset($devices_array[$i]['name'])){				
				$resultData = $this->Model_nagios_config->insertDeviceTemplates($devices_array[$i]);	
			}
		}
			
		for($i=0;$i<count($devices_array);$i++){			
			if(isset($devices_array[$i]['device_name'])){
				if(isset($devices_array[$i]['devicegroups'])){
				$res_groups = $this->Model_nagios_config->getDeviceGroupId($devices_array[$i]['devicegroups'],$gateway_id);
					if ($res_groups['status'] == 'true') {
					$devices_array[$i]['devicegroup_id'] =	$res_groups['resultSet']['devicegroup_id'];
					}				
				}
				$res_template = $this->Model_nagios_config->getDeviceTemplateId($devices_array[$i]['use_temp'],$gateway_id);
				if ($res_template['status'] == 'true') {
					$devices_array[$i]['device_template_id'] = $res_template['resultSet']['device_template_id'];
				}
				$res_device = $this->Model_nagios_config->getDeviceId($devices_array[$i]['device_name']);
				if ($res_device['status'] == 'true') {
					$updateData = $this->Model_nagios_config->updateDevice($devices_array[$i],$res_device['resultSet']['device_id']);					
				}else{					
					$resultData = $this->Model_nagios_config->insertDevices($devices_array[$i]);
				}				
			}
		}		

		for($i=0;$i<count($sevices_array);$i++){		
			if(isset($sevices_array[$i]['name'])){				
				$resultData = $this->Model_nagios_config->insertServiceTemplates($sevices_array[$i]);	
			}
		}
				
		for($i=0;$i<count($sevices_array);$i++){	
			$uniqueservices_array = array();
			if(isset($sevices_array[$i]['device_name'])){
				
				$res_device = $this->Model_nagios_config->getDeviceId($sevices_array[$i]['device_name']);
				if ($res_device['status'] == 'true') {
					$sevices_array[$i]['device_id'] = $res_device['resultSet']['device_id'];
				}											
				$res_template = $this->Model_nagios_config->getServiceTemplateId($sevices_array[$i]['use_temp'],$gateway_id);
				if ($res_template['status'] == 'true') {
					$sevices_array[$i]['service_template_id'] = $res_template['resultSet']['service_template_id'];
				}
				unset($sevices_array[$i]['device_name']);
				unset($sevices_array[$i]['use_temp']);
								
				$service_command  = explode("!",$sevices_array[$i]['check_command']);
				$service_only_command = trim($service_command[0]);
								
				$res_unique_service = $this->Model_nagios_config->getUniqueServiceId($sevices_array[$i]['service_description']);
				if ($res_unique_service['status'] == 'true') {	
					//echo "tets1";
					$sevices_array[$i]['service_id'] =  $res_unique_service['resultSet']['service_id'];					
				}else{
					//echo "tets2";
					$uniqueservices_array['check_command'] = $service_only_command;
					$uniqueservices_array['check_command_with_arg'] = $sevices_array[$i]['check_command'];
					$uniqueservices_array['service_description'] = $sevices_array[$i]['service_description'];					
					$resultData = $this->Model_nagios_config->insertUniqueService($uniqueservices_array);
					//pr($resultData);
					$sevices_array[$i]['service_id'] = $resultData['lastId'];										
				}
				
				//echo $sevices_array[$i]['service_id'];	
				//pr($sevices_array[$i]);exit;
				$resultData = $this->Model_nagios_config->insertService($sevices_array[$i]);
				
			}
		}
		
		$url = 'nagiosmanagements/nagios_config/'.encode_url($gateway_id);
		echo'
		<script>
		window.location.href = "'.base_url().$url.'";
		</script>
		'; 
    }
    public function refetch($gateway_id = null) {
		exit;
		
		$gateway_id = decode_url($gateway_id);
		//$this->Model_nagios_config->deleteGatewayConfig($gateway_id);
		$res = $this->Model_nagios_config->getGatewayDetails($gateway_id);		
		$context = new ZMQContext();
		//  Socket to talk to server
		$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
		$requester->connect("tcp://".$res['resultSet']['ip_address'].":5560");
		$requester->send("Get Nagios Response files");
		$reply = $requester->recv();
		$str = array();
		$str = json_decode($reply,true);
		$devices_array = array();		
		$devices_templates_array = array();
		$devicegroups_array = array();
		$sevices_array = array();
		$services_template_array = array();
		
		$count_devices= 0;
		$count_devicegroups= 0;
		$count_devices_templates = 0;
		$count_sevices = 0;
		$count_services_template = 0;	
		$is_device = true;
		$is_device_template = true;			
		$is_service = true;
		$is_service_template = true;		
		
		foreach($str as $nagiosfiles){
			$extension = "";
			$array = explode('.', $nagiosfiles['file_path']);
			$extension = trim(end($array));
			if($extension == "cfg" ){
				if( preg_match_all ("/define\ host\{(.*?)\}/ims", $nagiosfiles['file_content'], $pat_array_host)){
					foreach($pat_array_host[1] as $hostdata){
						if(preg_match_all ("/(.*?)\n/i",$hostdata,$hostbyline)){						    
							foreach($hostbyline[0] as $eachline){
								$eachline = trim($eachline);								
									//echo $eachline."<br>";
									$device_values = explode(";",trim($eachline));
									if(trim($device_values[0])!=""){
										
										$device_valuess = preg_split ("/\s/", trim($device_values[0]));
										//echo $device_valuess[0]
										$device_field_valuess ="";
										for($i=1;$i<count($device_valuess);$i++){
											$device_field_valuess .= $device_valuess[$i];	
										}																											
										//echo "<pre>";print_r($device_valuess);echo "</pre>";										
										//echo $device_valuess[0]." - ".$device_field_valuess."<br>";
										if($device_valuess[0] == 'host_name'){
											$devices_array[$count_devices]['device_name'] = $device_field_valuess;		
										}elseif($device_valuess[0] == 'hostgroups'){
											$devices_array[$count_devices]['devicegroups'] = $device_field_valuess;
										}elseif($device_valuess[0] == 'use'){
											$devices_array[$count_devices]['use_temp'] = $device_field_valuess;
										}else{
											$devices_array[$count_devices][$device_valuess[0]] = $device_field_valuess;
										}											
										
									}
								
							}							
							$devices_array[$count_devices]['file_path'] = $nagiosfiles['file_path'];
							$devices_array[$count_devices]['client_id'] = $res['resultSet']['client_id'];
							$devices_array[$count_devices]['gateway_id'] = $res['resultSet']['id'];	
														
							//$resultData = $this->Model_nagios_config->insertDevices($devices_array);							
							//echo pr($name_array);
							//echo "<hr>";
							
						}
						$count_devices++;
					}
					//echo "<pre>";print_r($pat_array_host[1]);	
				}


				if( preg_match_all ("/define\ service\{(.*?)\}/ims", $nagiosfiles['file_content'], $pat_array_host)){
					foreach($pat_array_host[1] as $hostdata){
						if(preg_match_all ("/(.*?)\n/i",$hostdata,$hostbyline)){
						    
							foreach($hostbyline[0] as $eachline){
								$eachline = trim($eachline);
								
									//echo $eachline."<br>";
									$device_values = explode(";",trim($eachline));
									if(trim($device_values[0])!=""){
										
										$device_valuess = preg_split ("/\s/", trim($device_values[0]));
										//echo $device_valuess[0]
										$device_field_valuess ="";
										for($i=1;$i<count($device_valuess);$i++){
											$device_field_valuess .= $device_valuess[$i];	
										}																											
										//echo "<pre>";print_r($device_valuess);echo "</pre>";										
										//echo $device_valuess[0]." - ".$device_field_valuess."<br>";
										if($device_valuess[0] == 'host_name'){
											$sevices_array[$count_sevices]['device_name'] = $device_field_valuess;		
										}elseif($device_valuess[0] == 'hostgroups'){
											$sevices_array[$count_sevices]['devicegroups'] = $device_field_valuess;
										}elseif($device_valuess[0] == 'use'){
											$sevices_array[$count_sevices]['use_temp'] = $device_field_valuess;
										}else{
											$sevices_array[$count_sevices][$device_valuess[0]] = $device_field_valuess;
										}											

									}
								
							}
							
							$sevices_array[$count_sevices]['file_path'] = $nagiosfiles['file_path'];
							$sevices_array[$count_sevices]['client_id'] = $res['resultSet']['client_id'];
							$sevices_array[$count_sevices]['gateway_id'] = $res['resultSet']['id'];															
							//$resultData = $this->Model_nagios_config->insertDevices($devices_array);							
							//echo pr($name_array);
							//echo "<hr>";
							
						}
						$count_sevices++;
					}
				}
				if( preg_match_all ("/define\ hostgroup\{(.*?)\}/ims", $nagiosfiles['file_content'], $pat_array_host)){
					foreach($pat_array_host[1] as $hostdata){
						if(preg_match_all ("/(.*?)\n/i",$hostdata,$hostbyline)){					    
							foreach($hostbyline[0] as $eachline){
								$eachline = trim($eachline);
								
									//echo $eachline."<br>";
									$device_values = explode(";",trim($eachline));
									if(trim($device_values[0])!=""){
										
										$device_valuess = preg_split ("/\s/", trim($device_values[0]));
										//echo $device_valuess[0]
										$device_field_valuess ="";
										for($i=1;$i<count($device_valuess);$i++){
											$device_field_valuess .= $device_valuess[$i];	
										}																											
										//echo "<pre>";print_r($device_valuess);echo "</pre>";										
										//echo $device_valuess[0]." - ".$device_field_valuess."<br>";
										if($device_valuess[0] == 'hostgroup_name'){
											$devicegroups_array[$count_devicegroups]['devicegroup_name'] = $device_field_valuess;		
										}elseif($device_valuess[0] == 'hostgroup_members'){
											$devicegroups_array[$count_devicegroups]['devicegroup_members'] = $device_field_valuess;
										}else{
											$devicegroups_array[$count_devicegroups][$device_valuess[0]] = $device_field_valuess;
										}											

									}
								
							}
							
							$devicegroups_array[$count_devicegroups]['file_path'] = $nagiosfiles['file_path'];
							$devicegroups_array[$count_devicegroups]['client_id'] = $res['resultSet']['client_id'];
							$devicegroups_array[$count_devicegroups]['gateway_id'] = $res['resultSet']['id'];															
							//$resultData = $this->Model_nagios_config->insertDevices($devices_array);							
							//echo pr($name_array);
							//echo "<hr>";
							
						}
						$count_devicegroups++;
					}
				}				
				//echo "<pre>";print_r($pat_array_service[1]);echo "</pre>";
				//$resultData = $this->Model_nagios_config->insertConfigfiles($nagiosfiles); 
			}

		}

		for($i=0;$i<count($devicegroups_array);$i++){			
			if(isset($devicegroups_array[$i]['devicegroup_name'])){				
				$resultData = $this->Model_nagios_config->insertDeviceGroups($devicegroups_array[$i]);
			}
		}
		
		for($i=0;$i<count($devices_array);$i++){		
			if(isset($devices_array[$i]['name'])){				
				$resultData = $this->Model_nagios_config->insertDeviceTemplates($devices_array[$i]);	
			}
		}
			
		for($i=0;$i<count($devices_array);$i++){			
			if(isset($devices_array[$i]['device_name'])){
				if(isset($devices_array[$i]['devicegroups'])){
				$res_groups = $this->Model_nagios_config->getDeviceGroupId($devices_array[$i]['devicegroups']);
					if ($res_groups['status'] == 'true') {
					$devices_array[$i]['devicegroup_id'] =	$res_groups['resultSet']['devicegroup_id'];
					}				
				}
				$res_template = $this->Model_nagios_config->getDeviceTemplateId($devices_array[$i]['use_temp']);
				if ($res_template['status'] == 'true') {
					$devices_array[$i]['device_template_id'] = $res_template['resultSet']['device_template_id'];
				}
				$resultData = $this->Model_nagios_config->insertDevices($devices_array[$i]);				
			}
		}		

		for($i=0;$i<count($sevices_array);$i++){		
			if(isset($sevices_array[$i]['name'])){				
				$resultData = $this->Model_nagios_config->insertServiceTemplates($sevices_array[$i]);	
			}
		}
				
		for($i=0;$i<count($sevices_array);$i++){			
			if(isset($sevices_array[$i]['device_name'])){
				
				$res_device = $this->Model_nagios_config->getDeviceId($sevices_array[$i]['device_name']);
				if ($res_device['status'] == 'true') {
					$sevices_array[$i]['device_id'] = $res_device['resultSet']['device_id'];
				}				
								
				$res_template = $this->Model_nagios_config->getServiceTemplateId($sevices_array[$i]['use_temp']);
				if ($res_template['status'] == 'true') {
					$sevices_array[$i]['service_template_id'] = $res_template['resultSet']['service_template_id'];
				}
				unset($sevices_array[$i]['device_name']);
				unset($sevices_array[$i]['use_temp']);
				$resultData = $this->Model_nagios_config->insertService($sevices_array[$i]);
				
			}
		}
		
		pr($devices_array);
		echo "<hr>";
		pr($sevices_array);
		exit;
		$url = 'nagiosmanagements/nagios_config/'.encode_url($gateway_id);
		echo'
		<script>
		window.location.href = "'.base_url().$url.'";
		</script>
		'; 
    }

	
	public function ajax_host_template() {
		$device_template_id = $_POST['device_template_id'];
		$templateDatabyId = $this->Model_nagios_config->getHostTemplates(array('device_template_id'=>$device_template_id));
		$data['ajax_host_template'] = array();
		if (isset($templateDatabyId['resultSet'][0])) {
			foreach ($templateDatabyId['resultSet'][0] as $key => $rowVal) {
				$unset = array('device_template_id','name','use_temp','alias','display_name','address','client_id','domain_id','gateway_id','file_path','register');
				if(!in_array($key,$unset))
				{
					$data['ajax_host_template'][$key] = $rowVal;	
				}
			}
		}
		echo $this->load->view('host_template', $data);
	}
	
	public function ajax_service_template() {
		$service_template_id = $_POST['service_template_id'];
		$templateDatabyId = $this->Model_nagios_config->getServiceTemplates(array('service_template_id'=>$service_template_id));
		$data['ajax_service_template'] = array();
		if (isset($templateDatabyId['resultSet'][0])) {
			foreach ($templateDatabyId['resultSet'][0] as $key => $rowVal) {
				$unset = array('service_template_id','name','use_temp','devicegroup_name','display_name','address','client_id','domain_id','gateway_id','file_path','service_description','register');
				if(!in_array($key,$unset))
				{
					$data['ajax_service_template'][$key] = $rowVal;	
				}
			}
		}
		echo $this->load->view('service_template', $data);
	}	
	public function ajax_checkUniqueHostName() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
		$gateway_id = $this->input->post('gateway_id');
        $recordCount = $this->db->get_where($table, array($field => $value,'gateway_id'=>$gateway_id))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	
	public function ajax_checkUniqueHostAddress() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
		$gateway_id = $this->input->post('gateway_id');
        $recordCount = $this->db->get_where($table, array($field => $value,'gateway_id'=>$gateway_id))->num_rows();
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
    public function deviceTemplates() {
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		$config['base_url'] = base_url() . 'nagiosmanagements/devicetemplates/';
        $config['total_rows'] = $this->Model_nagios_config->getDeviceTemPagination($search);
		$config['uri_segment'] = 3;		      
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;      
        $data['deviceTemplates'] = $this->Model_nagios_config->getDeviceTemPagination($search, $config['per_page'], $page);
		//pr($data['deviceTemplates']);exit;
		$data['hiddenURL'] = 'nagiosmanagements/devicetemplates/';
		//pr($data['clientTitles']);exit;
        $data['file'] = 'devicetemplates/view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }
    public function ajaxChangeDeviceTemplateStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Model_nagios_config->updateDeviceTempStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Device Template is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Device Template is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function devicetemplateCreate() {
		//echo $gateway_id;exit;
		//echo "tcp://".$res['resultSet']['ip_address'].":5560";exit;
		//pr($res);exit;
 		/* $context = new ZMQContext();
		//  Socket to talk to server
		$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
		$requester->connect("tcp://".$res['resultSet']['ip_address'].":5560"); */ 		
		$roleAccess = helper_fetchPermission('67', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('name', 'Template Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {

                    $postHost = $this->input->post();
					//pr($postHost);exit;
					$file_path = '/usr/local/nagios/etc/objects/templates.cfg';
					$hostData['name'] = $postHost['name'];
					$hostData['file_path'] = $file_path;
					$hostoptions = array();
					//$config_template_data = "define host{\n";
					//$config_template_data .= "\tname\t".$postHost['name']."\n";
					if(isset($postHost['hostoptions'])){
						for($i=0;$i<count($postHost['hostoptions']);$i++){
							if($postHost['hostoptions'][$i] =="hostgroups"){
								$hostData['devicegroups'] = $postHost['host_'.$postHost['hostoptions'][$i]];
							}else{
								$hostData[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
							}
							$hostoptions[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
							
							//$config_template_data .= "\t".$postHost['hostoptions'][$i]."\t".$postHost['host_'.$postHost['hostoptions'][$i]]."\n";	
						}
					}
					$hostData['selected_values'] = serialize($hostoptions);	
					//$config_template_data .= "\tregister\t0\n";
					//$config_template_data .= "\t}\n";					
					$hostData = array_merge($hostData, $this->cData);
					//pr($hostData);
					//echo "<pre>";echo $config_template_data;exit;
					$resultData = $this->Model_nagios_config->insertDeviceTemplate($hostData);
					
					//pr($resultData);exit;
					if (!empty($resultData)) {
						//echo $lastInsertId;				
						//echo "<pre>";					
						//echo $config_template_data;
						/* $config_template_data = $this->get_cfg_template_host_service($gateway_id,$file_path);
						//echo "<pre>";
						//echo $config_template_data;exit;
 						$send_req = array("msg"=>"edit cfg file","data"=>$config_template_data,"file_path"=>$file_path);
						$send_json_req = json_encode($send_req);
						$requester->send($send_json_req);  */						
						//echo "</pre>";
						//exit;						
						$this->session->set_flashdata("success_msg", "Device Template is created successfully ..!!");                        
						redirect('nagiosmanagements/devicetemplates/');								
					} else {
						$this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('nagiosmanagements/devicetemplates/');                      
					}					

                } else {					
					//$data['severitiesall'] = $this->Model_gateways->getSeverities();
					//pr($data['severitiesall']);exit;
                    $data['file'] = 'devicetemplates/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {		
				//$data['severitiesall'] = $this->Model_gateways->getSeverities();
				//pr($data['severitiesall']);exit;
                $data['file'] = 'devicetemplates/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }
	
    public function devicetemplateEdit($device_template_id = NULL) {
		$device_template_id = decode_url($device_template_id);
		//echo "tcp://".$res['resultSet']['ip_address'].":5560";exit;
		//pr($res);exit;
 		/* $context = new ZMQContext();
		//  Socket to talk to server
		$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
		$requester->connect("tcp://".$res['resultSet']['ip_address'].":5560"); */ 	
		$data['devicetemp_details'] = $this->Model_nagios_config->getDeviceTempDetails($device_template_id);
		$data['selected_values'] = unserialize($data['devicetemp_details']['resultSet']['selected_values']);
		$data['name'] = $data['devicetemp_details']['resultSet']['name'];
		
		$roleAccess = helper_fetchPermission('67', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('name', 'Template Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {

                    $postHost = $this->input->post();
					//pr($postHost);exit;
					$file_path = '/usr/local/nagios/etc/objects/templates.cfg';
					$hostData['name'] = $postHost['name'];
					$hostData['file_path'] = $file_path;
					$hostoptions = array();
					//$config_template_data = "define host{\n";
					//$config_template_data .= "\tname\t".$postHost['name']."\n";
					if(isset($postHost['hostoptions'])){
						for($i=0;$i<count($postHost['hostoptions']);$i++){
							if($postHost['hostoptions'][$i] =="hostgroups"){
								$hostData['devicegroups'] = $postHost['host_'.$postHost['hostoptions'][$i]];
							}else{
								$hostData[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
							}
							$hostoptions[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
							
							//$config_template_data .= "\t".$postHost['hostoptions'][$i]."\t".$postHost['host_'.$postHost['hostoptions'][$i]]."\n";	
						}
					}
					$hostData['selected_values'] = serialize($hostoptions);	
					//$config_template_data .= "\tregister\t0\n";
					//$config_template_data .= "\t}\n";					
					$hostData = array_merge($hostData, $this->cData);
					//pr($hostData);
					//echo "<pre>";echo $config_template_data;exit;
					$resultData = $this->Model_nagios_config->updateDeviceTemplate($hostData,$device_template_id);
					
					//pr($resultData);exit;
					if ($resultData['status'] == 'true') {
						//echo $lastInsertId;				
						//echo "<pre>";					
						//echo $config_template_data;
						/* $config_template_data = $this->get_cfg_template_host_service($gateway_id,$file_path);
						//echo "<pre>";
						//echo $config_template_data;exit;
 						$send_req = array("msg"=>"edit cfg file","data"=>$config_template_data,"file_path"=>$file_path);
						$send_json_req = json_encode($send_req);
						$requester->send($send_json_req); */ 						
						//echo "</pre>";
						//exit;						
						$this->session->set_flashdata("success_msg", "Device Template is updated successfully ..!!");                        
						redirect('nagiosmanagements/devicetemplates/');								
					} else {
						$this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('nagiosmanagements/devicetemplates/');                      
					}					

                } else {					
                    $data['file'] = 'devicetemplates/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {		
                $data['file'] = 'devicetemplates/update_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }	

    public function serviceTemplates() {
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		$config['base_url'] = base_url() . 'nagiosmanagements/servicetemplates/';
        $config['total_rows'] = $this->Model_nagios_config->getServiceTemPagination($search);
		$config['uri_segment'] = 3;		      
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;      
        $data['serviceTemplates'] = $this->Model_nagios_config->getServiceTemPagination($search, $config['per_page'], $page);
		//pr($data['deviceTemplates']);exit;
		$data['hiddenURL'] = 'nagiosmanagements/servicetemplates/';
		//pr($data['clientTitles']);exit;
        $data['file'] = 'servicetemplates/view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }
    public function ajaxChangeServiceTemplateStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Model_nagios_config->updateServiceTempStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Service Template is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Service Template is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function servicetemplateCreate() {
		//echo $gateway_id;exit;
		//echo "tcp://".$res['resultSet']['ip_address'].":5560";exit;
		//pr($res);exit;
		/* $context = new ZMQContext();
		//  Socket to talk to server
		$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
		$requester->connect("tcp://".$res['resultSet']['ip_address'].":5560"); */		
		$roleAccess = helper_fetchPermission('67', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('name', 'Template Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {

                    $postHost = $this->input->post();
					//pr($postHost);exit;
					$file_path = '/usr/local/nagios/etc/objects/templates.cfg';
					$hostData['name'] = $postHost['name'];
					$hostData['file_path'] = $file_path;
					$hostoptions = array();
					//$config_template_data = "define service{\n";
					//$config_template_data .= "\tname\t".$postHost['name']."\n";
					if(isset($postHost['hostoptions'])){
						for($i=0;$i<count($postHost['hostoptions']);$i++){
							if($postHost['hostoptions'][$i] =="hostgroups"){
								$hostData['devicegroups'] = $postHost['host_'.$postHost['hostoptions'][$i]];
							}else{
								$hostData[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
							}
							$hostoptions[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
							
							//$config_template_data .= "\t".$postHost['hostoptions'][$i]."\t".$postHost['host_'.$postHost['hostoptions'][$i]]."\n";	
						}
					}
					$hostData['selected_values'] = serialize($hostoptions);	
					//$config_template_data .= "\tregister\t0\n";
					//$config_template_data .= "\t}\n";					
					$hostData = array_merge($hostData, $this->cData);
					//pr($hostData);
					//echo "<pre>";echo $config_template_data;exit;
					$resultData = $this->Model_nagios_config->insertServiceTemplate($hostData);
					
					//pr($resultData);exit;
					if (!empty($resultData)) {
						//echo $lastInsertId;				
						//echo "<pre>";					
						//echo $config_template_data;
						/* $config_template_data = $this->get_cfg_template_host_service($gateway_id,$file_path);
						//echo "<pre>";
						//echo $config_template_data;exit;
						$send_req = array("msg"=>"edit cfg file","data"=>$config_template_data,"file_path"=>$file_path);
						$send_json_req = json_encode($send_req);
						$requester->send($send_json_req); */						
						//echo "</pre>";
						//exit;						
						$this->session->set_flashdata("success_msg", "Service Template is created successfully ..!!");                        
						redirect('nagiosmanagements/servicetemplates/');								
					} else {
						$this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('nagiosmanagements/servicetemplates/');                      
					}					

                } else {					
                    $data['file'] = 'servicetemplates/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {		
                $data['file'] = 'servicetemplates/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }	
	public function get_cfg_template_host_service($gateway_id ="",$file_path = "") {
		$config_template_data ="";
		$config_template_data .= "define contact{\n";
		$config_template_data .= "\tname\tgeneric-contact\n";
		$config_template_data .= "\tservice_notification_period\t24x7\n";
		$config_template_data .= "\thost_notification_period \t24x7\n";
		$config_template_data .= "\tservice_notification_options\tw,u,c,r,f,s\n";
		$config_template_data .= "\thost_notification_options\td,u,r,f,s\n";
		$config_template_data .= "\tservice_notification_commands\tnotify-service-by-email\n";
		$config_template_data .= "\thost_notification_commands\tnotify-host-by-email\n";
		$config_template_data .= "\tregister\t0\n";
		$config_template_data .= "\t}\n";		
		$HostsData = $this->Model_nagios_config->getHostsTempCfg($gateway_id,$file_path);
		if ($HostsData['status'] == 'true') {	
			foreach($HostsData['resultSet'] as $HostData){
				$config_template_data .= "define host{\n";
				$config_template_data .= "\tname\t".$HostData['name']."\n";
				$host_selected = unserialize($HostData['selected_values']);
				if($host_selected){
					foreach($host_selected as $key=>$value){
						
						$config_template_data .= "\t".$key."\t".$value."\n";	
					}
				}
				$config_template_data .= "\tregister\t0\n";
				$config_template_data .= "\t}\n";				
				
			}
		}
		$ServicesData = $this->Model_nagios_config->getSevicesTempCfg($gateway_id,$file_path);
		//pr($ServicesData);exit;
		if ($ServicesData['status'] == 'true') {	
			foreach($ServicesData['resultSet'] as $ServicesData){

				$config_template_data .= "define service{\n";
				$config_template_data .= "\tname\t".$ServicesData['name']."\n";
				$service_selected = unserialize($ServicesData['selected_values']);
				if($service_selected){
					foreach($service_selected as $key=>$value){					
						$config_template_data .= "\t".$key."\t".$value."\n";	
					}	
				}
				$config_template_data .= "\tregister\t0\n";
				$config_template_data .= "\t}\n";				
				
			}
		}		
		return $config_template_data;		
	}
	public function ajax_checkUniqueDeviceTemName() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
		$gateway_id = $this->input->post('gateway_id');
        $recordCount = $this->db->get_where($table, array($field => $value,'gateway_id'=>$gateway_id))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	public function ajax_checkUniqueServiceTemName() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
		$gateway_id = $this->input->post('gateway_id');
        $recordCount = $this->db->get_where($table, array($field => $value,'gateway_id'=>$gateway_id))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }	
    public function servicetemplateEdit($service_template_id = NULL) {
		$service_template_id = decode_url($service_template_id);
		//echo "tcp://".$res['resultSet']['ip_address'].":5560";exit;
		//pr($res);exit;
 		/* $context = new ZMQContext();
		//  Socket to talk to server
		$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
		$requester->connect("tcp://".$res['resultSet']['ip_address'].":5560"); */ 	
		$data['servicetemp_details'] = $this->Model_nagios_config->getServiceTempDetails($service_template_id);
		$data['selected_values'] = unserialize($data['servicetemp_details']['resultSet']['selected_values']);
		$data['name'] = $data['servicetemp_details']['resultSet']['name'];
		
		$roleAccess = helper_fetchPermission('67', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('name', 'Template Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {

                    $postHost = $this->input->post();
					//pr($postHost);exit;
					$file_path = '/usr/local/nagios/etc/objects/templates.cfg';
					$hostData['name'] = $postHost['name'];
					$hostData['file_path'] = $file_path;
					$hostoptions = array();
					//$config_template_data = "define host{\n";
					//$config_template_data .= "\tname\t".$postHost['name']."\n";
					if(isset($postHost['hostoptions'])){
						for($i=0;$i<count($postHost['hostoptions']);$i++){
							if($postHost['hostoptions'][$i] =="hostgroups"){
								$hostData['devicegroups'] = $postHost['host_'.$postHost['hostoptions'][$i]];
							}else{
								$hostData[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
							}
							$hostoptions[$postHost['hostoptions'][$i]] = $postHost['host_'.$postHost['hostoptions'][$i]];
							
							//$config_template_data .= "\t".$postHost['hostoptions'][$i]."\t".$postHost['host_'.$postHost['hostoptions'][$i]]."\n";	
						}
					}
					$hostData['selected_values'] = serialize($hostoptions);	
					//$config_template_data .= "\tregister\t0\n";
					//$config_template_data .= "\t}\n";					
					$hostData = array_merge($hostData, $this->cData);
					//pr($hostData);
					//echo "<pre>";echo $config_template_data;exit;
					$resultData = $this->Model_nagios_config->updateServiceTemplate($hostData,$service_template_id);
					
					//pr($resultData);exit;
					if ($resultData['status'] == 'true') {
						//echo $lastInsertId;				
						//echo "<pre>";					
						//echo $config_template_data;
						/* $config_template_data = $this->get_cfg_template_host_service($gateway_id,$file_path);
						//echo "<pre>";
						//echo $config_template_data;exit;
 						$send_req = array("msg"=>"edit cfg file","data"=>$config_template_data,"file_path"=>$file_path);
						$send_json_req = json_encode($send_req);
						$requester->send($send_json_req); */ 						
						//echo "</pre>";
						//exit;						
						$this->session->set_flashdata("success_msg", "Service Template is updated successfully ..!!");                        
						redirect('nagiosmanagements/servicetemplates/');								
					} else {
						$this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('nagiosmanagements/servicetemplates/');                      
					}					

                } else {					
					//$data['severitiesall'] = $this->Model_gateways->getSeverities();
					//pr($data['severitiesall']);exit;
                    $data['file'] = 'servicetemplates/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {		
				//$data['severitiesall'] = $this->Model_gateways->getSeverities();
				//pr($data['severitiesall']);exit;
                $data['file'] = 'servicetemplates/update_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }
    public function serviceGroups() {		
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		$config['base_url'] = base_url() . 'nagiosmanagements/servicegroups/';
        $config['total_rows'] = $this->Model_nagios_config->getServiceGroupsPagination($search);
		$config['uri_segment'] = 3;		      
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;      
        $data['servicegroups'] = $this->Model_nagios_config->getServiceGroupsPagination($search, $config['per_page'], $page);
		//pr($data['servicegroups']);exit;
		$data['hiddenURL'] = 'nagiosmanagements/servicegroups/';
		//pr($data['clientTitles']);exit;
        $data['file'] = 'servicegroups/view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }	
    public function serviceGroupCreate() {		
		$roleAccess = helper_fetchPermission('67', 'add');
		$resultGatewayTypes = $this->Model_nagios_config->getGatewayTypes();
		$data['gatewayTypes'] = $resultGatewayTypes;  
		$data['categories'] = $this->Model_nagios_config->deviceCategories();		
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('service_group_name', 'Service Group Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
					$serviceGroupPost = $this->input->post();
					$serviceGroupData = array();
					$serviceGroupMapped = array();
					$service_group_uuid = generate_uuid('sgp_');
					$serviceGroupData['service_group_uuid'] = $service_group_uuid;
					$serviceGroupData['gateway_type_uuid'] = $serviceGroupPost['gateway_type_uuid'];
					$serviceGroupData['device_category_id'] = $serviceGroupPost['device_category_id'];	
					if(isset($serviceGroupPost['default_group']) && $serviceGroupPost['default_group']!=''){
						$resultDataUpate = $this->Model_nagios_config->updateServiceGroupDefault($serviceGroupPost['gateway_type_uuid'],$serviceGroupPost['device_category_id']);
						$serviceGroupData['default_group'] = $serviceGroupPost['default_group'];
					}					
									
					$serviceGroupData['service_group_name'] = $serviceGroupPost['service_group_name'];					
					$serviceGroupData = array_merge($serviceGroupData, $this->cData);
					//pr($serviceGroupData);exit;
					$resultData = $this->Model_nagios_config->insertServiceGroup($serviceGroupData);
					
					if(isset($serviceGroupPost['services'])){
						for($k=0;$k<count($serviceGroupPost['services']);$k++){							
							$serviceGroupMapped['service_group_uuid'] = $service_group_uuid;
							$serviceGroupMapped['service_id'] = $serviceGroupPost['services'][$k];
							if($serviceGroupPost['gateway_type_uuid'] ==1 ){
								$serviceGroupMapped['service_template_id'] = $serviceGroupPost['services_'.$serviceGroupPost['services'][$k]];	
							}
							$serviceGroupMapped = array_merge($serviceGroupMapped, $this->cData);	
							$resultDataGrouped = $this->Model_nagios_config->insertGroupedServices($serviceGroupMapped);
						}
					}
					//pr($resultData);exit;
	                if (!empty($resultData)) {
                        $this->session->set_flashdata("success_msg", "Service Group is created successfully ..!!");                        
						redirect('nagiosmanagements/servicegroups/');								
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('nagiosmanagements/servicegroupCreate/');                      
                    }
                } else {					
                    $data['file'] = 'servicegroups/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {		
                $data['file'] = 'servicegroups/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }
	public function ajax_services_list() {
		$gateway_type = $_POST['gatewaytype_uuid'];
		$data['gateway_type'] = $gateway_type;
		$data['ajax_services_list']= $this->Model_nagios_config->getServicesList($gateway_type);
		$data['ajax_service_templates']= $this->Model_nagios_config->getServiceTemplatesList($gateway_type);
		echo $this->load->view('servicegroups/services_list', $data);
	}	
    function devicesUpload() {
		//echo $client_id;exit;
		ini_set('auto_detect_line_endings',TRUE);
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';
        $this->load->library('upload', $config); 
        // If upload failed, display error
        if (!$this->upload->do_upload()) {		
            $data['error'] = $this->upload->display_errors();
			//exit;
			$data['file'] = 'bulkupload_form';
			$this->load->view('template/front_template', $data);
        } else {
            $file_data = $this->upload->data();
            $file_path =  './uploads/'.$file_data['file_name']; 
            if ($this->csvimport->get_array($file_path)) {				
                $csv_array = $this->csvimport->get_array($file_path);
				//pr($csv_array);exit;
                foreach ($csv_array as $row) {
					$client_id = "";
					$gateway_id = "";
					$device_category_id = "";
					$service_group_id = "";
					$gateway_type = "";
					$ip_address ="";
					$client_res = $this->Model_nagios_config->getClientId($row['Client']);
					if($client_res['status'] == 'true'){
						$client_id = $client_res['resultSet']['id'];
					}
					
					$gateway_res = $this->Model_nagios_config->getGatewayId($row['Gateway']);
					if($gateway_res['status'] == 'true'){
						$gateway_id = $gateway_res['resultSet']['id'];
						$gateway_type = $gateway_res['resultSet']['gateway_type'];
					}
					
					$device_category_res = $this->Model_nagios_config->getDeviceCategoryId($row['Device Category']);
					if($device_category_res['status'] == 'true'){
						$device_category_id = $device_category_res['resultSet']['id'];
						if(isset($row['Service Group']) && $row['Service Group'] == ''){
							$service_group_res = $this->Model_nagios_config->getServiceGroupIdFrServicesG($device_category_id,$gateway_type);
							if($service_group_res['status'] == 'true'){
								$service_group_id = $service_group_res['resultSet']['service_group_uuid'];
							}							
						}else{
							$service_group_res = $this->Model_nagios_config->getServiceGroupId($row['Service Group']);
							if($service_group_res['status'] == 'true'){
								$service_group_id = $service_group_res['resultSet']['service_group_uuid'];
							}							
						}
					}
					if(isset($row['IP Address']) && $row['IP Address'] != ''){
							$ip_address = $row['IP Address'];
					}						
					//pr($client_res);
					//pr($gateway_res);
					//pr($device_category_res);
					//pr($service_group_res);
					//exit;
					if($client_id!=''){
						$insert_data = array(
							'device_name' =>$row['Device Title'],						
							'client_id' =>$client_id,
							'gateway_id' =>$gateway_id,
							'device_category_id' =>$device_category_id,							
							'service_group_uuid' =>$service_group_id,
							'address' =>$ip_address,
							'created_on' => $this->current_date,
							'created_by' => $this->current_user						
							);
						$this->Model_nagios_config->insertDevices($insert_data);
					}
                }
                $this->session->set_flashdata('success_msg', 'Csv Data Imported Succesfully');
                redirect(base_url().'nagiosmanagements/devicesupload/');
            } else{ 				
                $data['error'] = "Error occured";
				$data['file'] = 'bulkupload_form';
				$this->load->view('template/front_template', $data);
            }
 
        } 
	}	
	public function ajax_checkDefaultTemplate() {
        $category_id = $this->input->post('category_id');
        $gateway_type_id = $this->input->post('gateway_type_id');
        $value = $this->input->post('value');
		$gateway_id = $this->input->post('gateway_id');
        $recordCount = $this->db->get_where('service_groups', array('default_group' =>'1','device_category_id' => $category_id,'gateway_type_uuid'=>$gateway_type_id))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	
	public function ajax_default_service_groups_check() {
        $device_category_id = $this->input->post('device_category_id');
        $gateway_type_uuid = $this->input->post('gateway_type_uuid');
        $recordCount = $this->db->get_where('service_groups', array('default_group' =>'1','device_category_id' => $device_category_id,'gateway_type_uuid'=>$gateway_type_uuid))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }		
}
?>