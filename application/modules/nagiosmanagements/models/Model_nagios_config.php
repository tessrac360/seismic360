<?php

class Model_nagios_config extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    private $finall;

    public function __construct() {
        parent::__construct();
        $this->tablename = 'nagios_config_files';
		$this->devices_templates = 'devices_templates';
		$this->device_groups = 'device_groups';
		$this->devices = 'devices';
		$this->device_categories = 'device_categories';
		$this->client_device_categories = 'client_device_categories';
		$this->skill_set = 'skill_set';		
		$this->commands = 'commands';		
		$this->services_templates = 'services_templates';
		$this->services = 'services';
		$this->gateway_services = 'gateway_services';
		$this->gateway_types = 'gateway_types';
		$this->grouped_services = 'grouped_services';
		$this->client ='client';
		
		$this->tablename_client_gateways = 'client_gateways';
		$this->service_groups = 'service_groups';		
		$this->gateway = 'client_gateways';
        $this->company_id = $this->session->userdata('company_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }


    public function insertConfigfiles($postData = array()) {

        $this->db->insert($this->tablename, $postData);
        //echo $this->db->last_query();exit;
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
    public function insertDeviceTemplates($postData = array()) {

        $this->db->insert($this->devices_templates, $postData);
        //echo $this->db->last_query();exit;
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
    public function insertDeviceGroups($postData = array()) {

        $this->db->insert($this->device_groups, $postData);
        //echo $this->db->last_query();exit;
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function insertCommands($postData = array()) {

        $this->db->insert($this->commands, $postData);
        //echo $this->db->last_query();exit;
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }	
    public function getDeviceTemplateId($title = "",$gateway_id ="") {
        $this->db->select('device_template_id');
        $this->db->from($this->devices_templates);
        $this->db->where('name', $title);
		$this->db->where('gateway_id', $gateway_id);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function getDeviceGroupId($title = "",$gateway_id ="") {
        $this->db->select('devicegroup_id');
        $this->db->from($this->device_groups);
        $this->db->where('devicegroup_name', $title);
		$this->db->where('gateway_id', $gateway_id);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function getDeviceId($title = "") {
        $this->db->select('device_id');
        $this->db->from($this->devices);
        $this->db->where('device_name', $title);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function getUniqueServiceId($title = "") {
        $this->db->select('service_id');
        $this->db->from($this->services);
        $this->db->where('service_description', $title);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }	
    public function getServiceTemplateId($title = "",$gateway_id ="") {
        $this->db->select('service_template_id');
        $this->db->from($this->services_templates);
        $this->db->where('name', $title);
		$this->db->where('gateway_id', $gateway_id);		
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }


    public function getDeviceGroupName($id = "",$gateway_id ="") {
        $this->db->select('devicegroup_name');
        $this->db->from($this->device_groups);
        $this->db->where('devicegroup_id', $id);
		$this->db->where('gateway_id', $gateway_id);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

    public function getServiceTemplateName($id = "",$gateway_id ="") {
        $this->db->select('name');
        $this->db->from($this->services_templates);
        $this->db->where('service_template_id', $id);
		$this->db->where('gateway_id', $gateway_id);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
	
    public function getDeviceTemplateName($id = "",$gateway_id ="") {
        $this->db->select('name');
        $this->db->from($this->devices_templates);
        $this->db->where('device_template_id', $id);
		$this->db->where('gateway_id', $gateway_id);		
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }	
    public function insertDevices($postData = array()) {

        $this->db->insert($this->devices, $postData);
        //echo $this->db->last_query();exit;
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateDevice($postData = array(), $device_id) {
        $this->db->where('device_id', $device_id);
        $update_status = $this->db->update($this->devices, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }	

    public function insertServiceTemplates($postData = array()) {

        $this->db->insert($this->services_templates, $postData);
        //echo $this->db->last_query();exit;
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }	

    public function insertService($postData = array()) {

        $this->db->insert($this->gateway_services, $postData);
        //echo $this->db->last_query();exit;
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
    public function insertUniqueService($postData = array()) {

        $this->db->insert($this->services, $postData);
        //echo $this->db->last_query();exit;
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    } 	
	
	public function selectConfigfiles($gateway_id = "") {

        $this->db->select('*');
		$this->db->from($this->tablename);
		$this->db->where('gateway_id', $gateway_id);
		$query = $this->db->get();
		$result = $query->result_array();
		//pr($result);
		//exit;
        //echo $this->db->last_query();exit;
        
        return $result;
    }
	
	public function getFileEdit($id) {

		$this->db->select('*');
        $this->db->from($this->tablename);
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
	public function updateConfigfiles($postData = array(),$id){
		//pr($postData);exit;
		$this->db->where('id', $id);
        $update_status = $this->db->update($this->tablename,$postData);
		//echo $this->db->last_query();exit;
		 if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
    public function deleteGatewayConfig($gateway_id = "") {
        if ($gateway_id) {
            $this->db->where('gateway_id', $gateway_id);
            if ($this->db->delete($this->tablename)) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }	

    public function getGatewayDetails($gateway_id = "") {
        $this->db->select('id,title,ip_address,client_id,gateway_type');
        $this->db->from($this->tablename_client_gateways);
        $this->db->where('id', $gateway_id);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }	
	
	public function getHostTemplates($params = array()) {
		$this->db->select('*');
		$this->db->from($this->devices_templates);
		if(is_array($params) && count($params) > 0)
		{
			$this->db->where($params);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	public function getServiceTemplates($params = array()) {
		$this->db->select('*');
		$this->db->from($this->services_templates);
		if(is_array($params) && count($params) > 0)
		{
			$this->db->where($params);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }

	
	public function getGroups($gateway_id = NULL) {
		$this->db->select('*');
		$this->db->from($this->device_groups);
		$this->db->where('gateway_id',$gateway_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }

	public function getDevices($gateway_id = NULL) {
		$this->db->select('*');
		$this->db->from($this->devices);
		$this->db->where('gateway_id',$gateway_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	public function getDeviceDetails($device_id = NULL) {
		$this->db->select('*');
		$this->db->from($this->devices);
		$this->db->where('device_id',$device_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->row_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	public function getServices($device_id = NULL) {
		$this->db->select('se.service_id,se.service_description,se.check_command_with_arg as check_command');
		$this->db->from($this->gateway_services." as gse");
		$this->db->where('device_id',$device_id);
		$this->db->join('eventedge_services as se', 'gse.service_id = se.service_id');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	public function getUniqueServices($gateway_type = NULL) {
		$this->db->select('*');
		$this->db->from($this->services);
		$this->db->where('gateway_type',$gateway_type);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	public function getUniqueServiceDetail($service_id = NULL) {
		$this->db->select('*');
		$this->db->from($this->services);
		$this->db->where('service_id',$service_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->row_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }	
	
	public function getDeviceCategories($client_id = NULL) {
		$this->db->select('cdec.device_category_id,dec.category');
		$this->db->from($this->client_device_categories. ' as cdec');
		$this->db->join('eventedge_device_categories as dec', 'dec.id = cdec.device_category_id');
		$this->db->where('cdec.client_id',$client_id);
		$this->db->where('cdec.status','Y');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	public function getPriorities() {
		$this->db->select('*');
		$this->db->from($this->skill_set);
		$this->db->where('status','Y');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }	
	public function getHostsCfg($gateway_id = NULL,$file_path= NULL) {
				
		$this->db->select('de.device_name as host_name,det.name as use,de.alias,de.address,de.selected_values');
		$this->db->from($this->devices . ' as de');
		$this->db->join($this->devices_templates.' as det', 'det.device_template_id = de.device_template_id');		
		$this->db->where('de.gateway_id',$gateway_id);
		$this->db->where('de.file_path',$file_path);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	public function getSevicesCfg($gateway_id = NULL,$file_path= NULL) {
				
		$this->db->select('de.device_name as host_name,set.name as use,se.service_description,se.check_command_with_arg as check_command,gse.selected_values');
		$this->db->from($this->gateway_services . ' as gse');
		$this->db->join($this->services_templates.' as set', 'set.service_template_id = gse.service_template_id');
		$this->db->join($this->services.' as se', 'se.service_id = gse.service_id');	
		$this->db->join($this->devices.' as de', 'de.device_id = gse.device_id');
		$this->db->where('gse.gateway_id',$gateway_id);
		$this->db->where('gse.file_path',$file_path);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
    public function getDeviceTemPagination($search = '', $limit = 0, $start = 0) {
        $this->db->select('*');
        $this->db->from($this->devices_templates);
		//$this->db->where('status', 'Y');
		//$this->db->where('gateway_id', $gateway_id);
        if ($search != "") {
            $this->db->like('name', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
       
        return $gateway->result_array();
    }
    public function updateDeviceTempStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('device_template_id', $id);
            $this->db->update($this->devices_templates, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
    public function getGatewayEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->gateway);
        $this->db->where('id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function insertDeviceTemplate($postData = array()){
		$this->db->insert($this->devices_templates, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }	
    public function updateDeviceTemplate($postData = array(), $device_template_id) {
        $this->db->where('device_template_id', $device_template_id);
        $update_status = $this->db->update($this->devices_templates, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }
	public function getDeviceTempDetails($device_template_id = NULL) {
		$this->db->select('*');
		$this->db->from($this->devices_templates);
		$this->db->where('device_template_id',$device_template_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->row_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }	
    public function getServiceTemPagination($search = '', $limit = 0, $start = 0) {
        $this->db->select('*');
        $this->db->from($this->services_templates);
		//$this->db->where('status', 'Y');
        if ($search != "") {
            $this->db->like('name', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
       
        return $gateway->result_array();
    }
    public function insertServiceTemplate($postData = array()){
		$this->db->insert($this->services_templates, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
    public function updateServiceTempStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('service_template_id', $id);
            $this->db->update($this->services_templates, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	public function getHostsTempCfg($gateway_id = NULL,$file_path= NULL) {
				
		$this->db->select('name,selected_values');
		$this->db->from($this->devices_templates);		
		$this->db->where('gateway_id',$gateway_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	public function getSevicesTempCfg($gateway_id = NULL,$file_path= NULL) {
				
		$this->db->select('name,selected_values');
		$this->db->from($this->services_templates );
		$this->db->where('gateway_id',$gateway_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	public function getServiceTempDetails($service_template_id = NULL) {
		$this->db->select('*');
		$this->db->from($this->services_templates);
		$this->db->where('service_template_id',$service_template_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->row_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
    
    public function updateServiceTemplate($postData = array(), $service_template_id= NULL) {
        $this->db->where('service_template_id', $service_template_id);
        $update_status = $this->db->update($this->services_templates, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }
    public function getServiceGroupsPagination($search = '', $limit = 0, $start = 0) {
				
        $this->db->select('sg.*,dc.category,gt.title');
        $this->db->from($this->service_groups.' as sg');		
		$this->db->join($this->device_categories.' as dc', 'dc.id = sg.device_category_id');
		$this->db->join($this->gateway_types.' as gt', 'gt.gatewaytype_uuid = sg.gateway_type_uuid');
		
		//$this->db->where('status', 'Y');
        if ($search != "") {
            $this->db->or_like('sg.service_group_name', $search);
			$this->db->or_like('gt.title', $search);
			$this->db->or_like('dc.category', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
		$this->db->order_by('sg.service_group_name', "desc");
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $gateway->result_array();
    }	
    public function getGatewayTypes() {
        $this->db->select('*');
        $this->db->from($this->gateway_types);
		$this->db->where('status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function deviceCategories() {
        $this->db->select('*');
        $this->db->from($this->device_categories);
		$this->db->where('status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
	public function getServicesList($gateway_type = NULL) {
				
		$this->db->select('*');
		$this->db->from($this->services);
		$this->db->where('gateway_type',$gateway_type);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	public function getServiceTemplatesList($gateway_type = NULL) {
				
		$this->db->select('*');
		$this->db->from($this->services_templates);
		//$this->db->where('gateway_type',$gateway_type);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }	
	public function insertServiceGroup($postData = array()){
		$this->db->insert($this->service_groups, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
	public function insertGroupedServices($postData = array()){
		$this->db->insert($this->grouped_services, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
    function insertClients($data) {
        $this->db->insert($this->devices, $data);
		//echo $this->db->last_query();exit;
    }
    public function getClientId($clientTitle = "") {
        $this->db->select('id');
        $this->db->from($this->client);
        $this->db->where('client_title', $clientTitle);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function getGatewayId($gateway_title = "") {
        $this->db->select('id,gateway_type');
        $this->db->from($this->gateway);
        $this->db->where('title', $gateway_title);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function getDeviceCategoryId($devicec_title = "") {
        $this->db->select('id');
        $this->db->from($this->device_categories);
        $this->db->where('category', $devicec_title);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function getServiceGroupId($service_group_name = "") {
        $this->db->select('service_group_uuid');
        $this->db->from($this->service_groups);
        $this->db->where('service_group_name', $service_group_name);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function getServiceGroupIdFrServicesG($device_category_id = NULL,$gateway_type = NULL) {
        $this->db->select('service_group_uuid');
        $this->db->from($this->service_groups);
        $this->db->where('device_category_id', $device_category_id);
        $this->db->where('gateway_type_uuid', $gateway_type);
        $this->db->where('default_group', '1');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }	
	
    public function updateServiceGroupDefault($gateway_type_uuid, $device_category_id) {
        $this->db->where('gateway_type_uuid', $gateway_type_uuid);
		$this->db->where('device_category_id', $device_category_id);
		$postData = array("default_group"=>"0");
        $update_status = $this->db->update($this->service_groups, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }	
}

?>