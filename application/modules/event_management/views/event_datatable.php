<style>
    .dataTables_scrollBody{height:360px;}    
</style>
<table id="table" class="table table-striped table-bordered" cellspacing="0">
    <thead style="background: #17C4BB !important; color:white;">
        <tr>
            <?php foreach ($arrayColumnName as $key => $value) { ?>
                <th><?php echo $value; ?></th>
            <?php } ?>
        </tr>

    </thead>
    <tbody>
    </tbody>
</table>
<style>
    /*   table.dataTable th{
            width: 20px;
            max-width: 20px;
            word-break: break-all;
            white-space: pre-line;
        }
    
        table.dataTable td{
            width: 20px;
            max-width: 20px;
            word-break: break-all;
            white-space: pre-line;
        }*/
</style>
<script type="text/javascript">
    var d = new Date();
    table_fields = [];
//    $("#lstBox2 option").each(function () {
//        table_fields.push($(this).val());
//    });
//    choose_field = [];
//    $(".choose_field").each(function () {
//        choose_field.push($(this).val());
//    });
//    oper = [];
//    $(".oper").each(function () {
//        oper.push($(this).val());
//    });
//    conditiontextbox = [];
//    $(".conditiontextbox").each(function () {
//        conditiontextbox.push($(this).val());
//    });


    $(".dataTables_scrollHeadInner").css({"width": "100%"});
    $(".table").css({"width": "100%"});
    // $('#table').DataTable().draw();
    var table = $('#table').DataTable({
//        fixedHeader: {
//            header: true
//        },
//        fixedHeader: {
//            headerOffset: 50
//        },


        scrollX: "100%",
        //"scrollY": "200px",
        "scrollCollapse": true,
        "bStateSave": false,
        "lengthMenu": [[100, 300, 500], [100, 300, 500, ]],
        "bLengthChange": false,
        "language": {                
            "infoFiltered": ""
        },
        searching: true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.              
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('event_management/ajax_list') ?>",
            "type": "POST",
            "data": function (data) {
//                data.tables = $('.report_tables').val();
//                data.client = $('.client').val();
//		data.tool = $('.tool').val();
//                data.field_name = table_fields;
//                data.start_date = $('.start_date').val();
//                data.end_date = $('.end_date').val();
//                data.is_page_load_status = $('#is_page_load_status').val();
//                data.choose_field = choose_field;
//                data.operator = oper;
//                data.conditionsearch = conditiontextbox;
            },
            dataSrc: function (response) {
                if (response.recordsFiltered > 0) {
                    $('#hide_btn_pageload').show();

                } else {
                    $('#hide_btn_pageload').hide();
                    $('div#table_info').hide();
                    $('div#table_paginate').hide();
                }
                $("#table").val(response.recordsTotal);
                return response.data;
            }

        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0], //first column / numbering column
                "orderable": true, //set not orderable
            },
        ],
    });


</script>

