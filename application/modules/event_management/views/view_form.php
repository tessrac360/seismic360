<style type="text/css">
    body{font-size:13px;}
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 3px 6px;
        font-size: 12px !important;
    }

    .form-search .control-label{font-size:13px; text-align:right;}
    .form-search .form-control{height:24px; padding:0px 8px; font-size:13px;}
    .form-search .form-group{height: 16px;margin-bottom: 12px;}

    .page-toolbar .input-group{width: 245px;}
    .page-toolbar .input-group .btn.btn-default{padding: 4px 12px;}
    .page-toolbar .input-group .form-control{padding: 4px 12px !important; height: 30px;}
    table thead tr th, table tbody tr td{white-space: nowrap;}
    .dataTables_scrollHeadInner table.display.dataTable.no-footer tr th{ white-space: nowrap; font-weight:400 !important; font-size:12px !important; padding:3px 6px !important;}
    table tbody tr td { white-space: nowrap; font-weight:400 !important; font-size:12px !important; padding:3px 6px !important;}
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{bottom:3px;}
    table.dataTable, .dataTables_scroll{margin-top:0 !important; margin-bottom:0;}
    input[type="search"]{border: 1px solid #c2cad8 !important;}
      .dataTables_wrapper .dataTables_paginate .paginate_button:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{background: #337ab7 !important; color: #fff !important;
                                                                                                                                                                                                        border-color: #337ab7 !important;}
    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover{background: transparent !important; border:1px solid #ddd !important;}
    .dataTables_scroll{margin-bottom: 0;}
    div.dataTables_wrapper div.dataTables_paginate{margin-bottom: 3px;}
/*    table tr th, table tr td{max-width:200px; overflow-x: hidden; text-overflow: ellipsis; white-space: nowrap;}*/
    input#from_date{background:url("./public/images/calendar.png") no-repeat 10px center  ; }
    input#to_date{background:url("./public/images/calendar.png") no-repeat 10px center  ; }
    .fs-wrap {width: 100%;}
    .fs-label-wrap .fs-label { padding: 10px 22px 10px 8px;}
    /*.fs-dropdown { width: 90%;}*/
    
      table tr th{max-width:900px; white-space: nowrap;}
     table tr td{max-width:900px; white-space: normal; overflow: auto; vertical-align: top !important;}
     table tr td table tr td{font-size: 12px !important;}
</style>
<?php //pr($status_id);exit;?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/css/fSelect.css">
<!-- END PAGE HEADER-->
<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Event Table</span>
            </div>





            <div class="clearfix"></div>
        </div>


        <form class="form-search" style="padding:10px 0; background: rgba(255, 255, 255, 0.78);">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Partner</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange patner_id" id="patner_id" name="patner_id[]" multiple="multiple" onchange="getClients(this.value)">
                                <option value="">Select Option</option>
                                <?php
                                if ($partners['status'] == 'true') {
                                    foreach ($partners['resultSet'] as $value) {
                                        ?>
                                        <option value="<?php echo $value['id']; ?>" <?php
                                        if (isset($_GET['client_id']) && $_GET['client_id'] == $value['id']) {
                                            echo "selected";
                                        }
                                        ?>><?php echo $value['client_title']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Client</label>
                        <div class="col-md-8">
                            <select  class="form-control client_id" id="id" name="id[]" multiple="multiple">
                                <option value="">Select Option</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Tools</label>
                        <div class="col-md-8">

                            <select  class="form-control onchange tools" id="requestor" name="tools[]" multiple="multiple">
                                <option value="">Select Option</option>
                                <?php
                                //pr($status_ids);exit

                                if ($tools['status'] == 'true') {
                                    foreach ($tools['resultSet'] as $value) {
                                        ?>
                                        <option value="<?php echo $value->id; ?>" ><?php echo $value->title; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Host Name</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange status_code" id="status_code" name="status_code[]" multiple="multiple">
                                <option value="">Select Option</option>
                                <?php
                                if ($hostName['status'] == 'true') {
                                    foreach ($hostName['resultSet'] as $value) {
                                        ?>
                                        <option value="<?php echo $value->device_id ?>" ><?php echo $value->device_name; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Event Type</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange ticket_cate_id" id="ticket_cate_id" name="ticket_cate_id[]" multiple="multiple" onchange="getSubCategories(this.value)">
                                <option value="">Select Option</option>
                                <?php
                                if ($service['status'] == 'true') {
                                    foreach ($service['resultSet'] as $value) {
                                        ?>
                                        <option value="<?php echo $value->service_id ?>" ><?php echo $value->service_description; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Severity</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange severity_id" id="severity_id" name="severity_id[]" multiple="multiple">
                                <option value="">Select Option</option>
                                <?php
                                if ($severity['status'] == 'true') {
                                    foreach ($severity['resultSet'] as $value) {
                                        ?>
                                        <option value="<?php echo $value->id ?>" ><?php echo $value->severity; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">State</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange priority_id" id="priority_id" name="priority_id[]" multiple="multiple">
                                <option value="">Select Option</option>
                                <?php
                                if ($state['status'] == 'true') {
                                    foreach ($state['resultSet'] as $value) {
                                        ?>
                                        <option value="<?php echo $value->id ?>" ><?php echo $value->event_state; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>


            <!--<div><button class="btn btn-green green pull-right" style="padding: 1px 4px; font-size: 13px; margin-right: 11px;">Advance Search</button></div>-->
            <div class="clearfix"></div>
        </form>

        <div class="clearfix"></div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="col-md-12 custom-search">
                <div class="portlet light" style="margin:4px 4px 0 4px;">
                    <div class="col-md-12 resultSetFilter">                       

                    </div>
                </div>
            </div>        
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>




<script>

    $(document).ready(function () {
        var baseURL = $('#baseURL').val();
        $('.patner_id').fSelect();
        $('.client_id').fSelect();
        $('.tools').fSelect();
        $('.status_code').fSelect();
        //$('.ticket_cate_id').fSelect();
        $('.ticket_sub_cate_id').fSelect();
        $('.user_id').fSelect();
        $('.severity_id').fSelect();
        $('.priority_id').fSelect();
        $.ajax({
                type: 'POST',
                url: baseURL + 'event_management/ajax_filters',
                //data: formData,
                success: function (response) {
                    $('.resultSetFilter').html(response);
                }
            });
    });
    function getClients(val)
    {
        var baseURL = $('#baseURL').val();
        var value = $('#patner_id').val();
        $('.client_id').html('').trigger('change');
        $('.requestor').html('').trigger('change');
        $('.user_id').html('').trigger('change');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseURL + 'event_management/ajax_getAllClientDetails',
            data: {'value': value},
            success: function (msg) {
                $('.client_id').html(msg.clientData).trigger('change');
                recall();
            },
        });
    }

    function recall()
    {
        $('.client_id').fSelect('destroy');
        $('.client_id').fSelect('create');
        $('.requestor').fSelect('destroy');
        $('.requestor').fSelect('create');
        $('.user_id').fSelect('destroy');
        $('.user_id').fSelect('create');
    }






</script>



<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/datatable-scroller-server-side/js/jquery.dataTables.js" type="text/javascript"></script>	
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/datatable-scroller-server-side/js/dataTables.scroller.js" type="text/javascript"></script>	
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/fSelect.js" type="text/javascript"></script>	