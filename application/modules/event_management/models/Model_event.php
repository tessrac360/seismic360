<?php

class Model_event extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'temp_role';
        $this->client = 'client';
        $this->company_id = $this->session->userdata('company_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $this->client_id = $this->session->userdata('client_id');

        $this->cData = array('created_on' => $current_date, 'created_by' => $this->userId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $this->userId);
    }

    public function getAllPartner($ids = array()) {
        $this->db->select('cnt.id,cnt.client_title,cnt.client_uuid,cn.client_title as parent_name');
        $this->db->from('client as cnt');
        $this->db->join('client as cn', 'cn.id = cnt.parent_client_id');
        $this->db->where_in('cnt.id', $ids);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getClientsDetails($partnerId = "") {
        $this->db->select('id,client_title');
        $this->db->from('client');
        $this->db->where('status', 'Y');
        $this->db->where('client_type', 'C');
        $this->db->where_in('parent_client_id', $partnerId);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;	
        if ($query->num_rows() > 0) {
            $data['status'] = 'true';
            $data = $query->result_array();
            //pr($data);exit;
            // $rows = implode(",", array_column($data, 'client_id'));
            //$return['status'] = 'true';
            $return['resultSet'] = $data;
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getToolFilter() {
        $this->db->select('gatewaytype_uuid as id,title');
        $this->db->from('gateway_types');
        $query = $this->db->get();
        // echo $this->db->last_query();exit;       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    public function getHostName() {
        $accessLevelClient = helper_getAccessLevelClient();
        $this->db->select('device_id,device_name');
        $this->db->from('devices');
        $this->db->where('device_name !=', 'NA');
        if ($accessLevelClient['status'] == 'true') {
            $this->db->where_in('client_id', $accessLevelClient['resultSet']);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getService() {
        $this->db->select('service_id,service_description');
        $this->db->from('eventedge_services');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getSeverity() {
        $permalink = array(2, 3);
        $this->db->select('id, severity');
        $this->db->from('severities');
        $this->db->where_not_in('id', $permalink);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getState() {
        $this->db->select('id,event_state');
        $this->db->from('event_states');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getEventColumn() {
        $this->db->select('ecs.sort_id,ecr.rename_column,ec.column_id,ec.is_filter,ec.filter_function,ec.table_column_name');
        $this->db->from('event_column_rename ecr');
        $this->db->join('event_column_sort as ecs', 'ecs.rename_id = ecr.rename_id');
        $this->db->join('event_column as ec', 'ec.column_id = ecr.column_id');
        $this->db->where('ecr.status', 'Y');
        $this->db->order_by('ecs.position', 'asc');
        $this->db->where('ecs.user_id', $this->userId);
        $this->db->where('ecs.client_id', $this->client_id);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    private function _get_datatables_query() {
        $accessLevelClient = helper_getAccessLevelClient();
        //pr($eventFilter);exit;
        $type = "";
        if (!empty($eventFilter)) {
            if ($eventFilter['param_severitypost'] != "") {
                $eventparamSeveritypost = $eventFilter['param_severitypost'];
            }

            if ($eventFilter['eventSearch'] != "") {
                $eventSearch = $eventFilter['eventSearch'];
            }

            if (isset($eventFilter['severity'])) {
                $severity = $eventFilter['severity'];
            }
            if (isset($eventFilter['device_name'])) {
                $device_name = $eventFilter['device_name'];
            }
            if (isset($eventFilter['clients'])) {
                $clients = $eventFilter['clients'];
            }

            if (isset($eventFilter['service'])) {
                $service = $eventFilter['service'];
            }

            if (isset($eventFilter['state'])) {
                $state = $eventFilter['state'];
            }

            if (isset($eventFilter['type'])) {
                $type = $eventFilter['type'];
            }

            if (isset($eventFilter['acktype'])) {
                $acktype = $eventFilter['acktype'];
            }

            if (isset($eventFilter['tools'])) {
                $tools = $eventFilter['tools'];
            }
        }
        $permalink = array(2, 3);
        $ticket_id = "(SELECT ticket_id FROM `eventedge_tickets` WHERE `ticket_id` = ev.ticket_id) as ticket_id";
        $incident_id = "(SELECT incident_id FROM `eventedge_tickets` WHERE `ticket_id` = ev.ticket_id) as incident_id";
        $this->db->select("$incident_id,$ticket_id,gettype.title as getway_title,ev.client_name,ev.event_id,ev.event_invoice,ev.event_text,SUBSTR(event_text, 1, 30) as event_text_small,ev.event_start_time,ev.event_end_time,ev.event_created_time,ev.event_description,ev.client_id,clnt.client_title,devi.device_name,serv.service_description as service_name,severity.severity,group.title as domain_name,eventsta.event_state,TIMEDIFF(NOW(), ev.event_start_time) as Duration,severity.color_code,ev.is_suppressed");
        $this->db->from('events as ev');
        $this->db->join('client as clnt', 'clnt.id = ev.client_id');
        $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
        $this->db->join('services as serv', 'serv.service_id = ev.service_id');
        $this->db->join('severities as severity', 'severity.id = ev.severity_id');
        $this->db->join('group as group', 'group.id = ev.domain_id');
        $this->db->join('client_gateways as cgat', 'cgat.id = ev.gateway_id');
        $this->db->join('gateway_types as gettype', 'gettype.gatewaytype_uuid = cgat.gateway_type');
        $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');      
        $this->db->where_not_in('ev.severity_id', $permalink);

        if ($accessLevelClient['status'] == 'true') {
            $this->db->where_in('ev.client_id', $accessLevelClient['resultSet']);
        }
        //$this->db->where_in('ev.priority_id', $skill);
        if (isset($_POST['search']['value'])) {
            $this->db->group_start();
            $this->db->like('clnt.client_title', $_POST['search']['value']);
            $this->db->or_like('group.title', $_POST['search']['value']);
            $this->db->or_like('devi.device_name', $_POST['search']['value']);
            $this->db->or_like('serv.service_description', $_POST['search']['value']);
            $this->db->or_like('severity.severity', $_POST['search']['value']);
            $this->db->or_like('eventsta.event_state', $_POST['search']['value']);
            $this->db->or_like('ev.event_invoice', $_POST['search']['value']);
            $this->db->or_like('ev.client_name', $_POST['search']['value']);
            $this->db->or_like('gettype.title', $_POST['search']['value']);
            //$this->db->or_like($incident_id, $_POST['search']['value']);
            $this->db->group_end();
        }

        if (isset($eventparamSeveritypost)) {
            $this->db->where('ev.severity_id', $eventparamSeveritypost);
        }

        if (isset($tools)) {
            $this->db->where_in('cgat.gateway_type', $tools);
        }

        if (isset($severity)) {
            $this->db->where_in('ev.severity_id', $severity);
        }
        if (isset($device_name)) {
            $this->db->where_in('ev.device_id', $device_name);
        }
        if (isset($clients)) {
            $this->db->where_in('ev.client_id', $clients);
        }
        if (isset($service)) {
            $this->db->where_in('ev.service_id', $service);
        }
        if (isset($state)) {
            $this->db->where_in('ev.event_state_id', $state);
        }
        
         if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;

        $data = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                if ($row->is_suppressed == 'N') {
                    // pr($row);
                    $showTickeicon = "";
                    if ($row->ticket_id == "") {
                        $showTickeicon = '    <a title="manual ticket" class="manual_ticket mticket_' . $row->event_id . '" evtid=' . $row->event_id . '  href="javascript:void(0);"><i class="fa fa-ticket" aria-hidden="true"></i></a>';
                    }
                    $row->event_suppress_action = '<a title="Suppress" href="javascript:void(0);" class="suppress_action" evtid=' . $row->event_id . '><i class="fa fa-clock-o" aria-hidden="true"></i></a>' . $showTickeicon . '</span>';
                    $event_description = json_decode($row->event_description, 'true');
                    $state_str = (isset($event_description['state_str'])) ? $event_description['state_str'] : '';
                    $event_description = $state_str . '  -  ' . $event_description['service_description'] . '  -  ' . $event_description['output'];
                    $row->service_name = '<span title="' . $row->service_name . '">' . wordlimit($row->service_name, 20) . '</span>';
                    $row->event_description = '<span title="' . $event_description . '">' . wordlimit($event_description, 10) . '</span>';
                    $row->event_text = '<span  title="' . $row->event_text . '">' . $row->event_text_small . '</span>';
                    $row->severity = '<span class="severityEvent" style="background-color: ' . $row->color_code . '; ">' . $row->severity . '</span>';
                    $row->ticketDetails = '<a href="' . base_url('event/ticket/editTicket/' . encode_url($row->ticket_id)) . '">' . $row->incident_id . '</a>';
                    $data[] = $row;
                }
            }
        }
//        pr($data);exit;
//        exit;
        return $data;
    }

    public function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>