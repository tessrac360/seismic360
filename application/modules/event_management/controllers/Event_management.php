<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Event_management extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->userId = $this->session->userdata('id');
        $this->client_id = $this->session->userdata('client_id');
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->client_id = $this->session->userdata('client_id');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->load->library('form_validation');
        $this->load->model('Model_event');
    }

    public function index() {
        $userClients = helper_getAccessClientDropDown();
        if (isset($userClients['resultSet']) && (count($userClients['resultSet']) > 0)) {
            $data['partners'] = $this->Model_event->getAllPartner($userClients['resultSet']);
        }
        $data['hostName'] = $this->Model_event->getHostName();
        $data['tools'] = $this->Model_event->getToolFilter();
        $data['service'] = $this->Model_event->getService();
        $data['severity'] = $this->Model_event->getSeverity();
        $data['state'] = $this->Model_event->getState();
        ///$data['eventColumn'] = $this->Model_event->getEventColumn();       
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_filters() {
        // $table_fields = explode(',', $_POST['table_fields']);
        $data['eventColumn'] = $this->Model_event->getEventColumn();
        //pr($data['eventColumn']);exit;

        foreach ($data['eventColumn']['resultSet'] as $value) {
            $arrayAliasName[] = $value->rename_column;
        }
        // pr($arrayAliasName);exit;
        $data['arrayColumnName'] = $arrayAliasName;
        $this->load->view('event_datatable', $data);
    }

    public function ajax_list() {
        $eventColumn = $this->Model_event->getEventColumn();
         //pr($eventColumn);exit;       
        $col_1 = $eventColumn['resultSet'][1]->table_column_name;
        $col_2 = $eventColumn['resultSet'][2]->table_column_name;
        $col_3 = $eventColumn['resultSet'][3]->table_column_name;
        $col_4 = $eventColumn['resultSet'][4]->table_column_name;
        $col_5 = $eventColumn['resultSet'][5]->table_column_name;
        $col_6 = $eventColumn['resultSet'][6]->table_column_name;
        $col_7 = $eventColumn['resultSet'][7]->table_column_name;
        $col_8 = $eventColumn['resultSet'][8]->table_column_name;
        $col_9 = $eventColumn['resultSet'][9]->table_column_name;
        $col_10 = $eventColumn['resultSet'][10]->table_column_name;
        $col_11 = $eventColumn['resultSet'][11]->table_column_name;
        $col_12 = $eventColumn['resultSet'][12]->table_column_name;
        
        $list = $this->Model_event->get_datatables();
        //pr($list);exit;
        $data = array();
        foreach ($list as $customers) {
            $row = array();   
            $row[] = '<a href="javascript:void(0);" class="coupon_question" evtid=' . $customers->event_id . ' >' . $customers->event_invoice . '</a>';
            $row[] = $customers->$col_1;
            $row[] = $customers->$col_2;
            $row[] = $customers->$col_3;
            $row[] = $customers->$col_4;
            $row[] = $customers->$col_5;
            $row[] = $customers->$col_6;
            $row[] = $customers->$col_7;
            $row[] = $customers->$col_8;
            $row[] = $customers->$col_9;
            $row[] = $customers->$col_10;
            $row[] = $customers->$col_11;
            $row[] = $customers->$col_12;

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => 0,
            "recordsFiltered" => $this->Model_event->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
        exit;
    }

    public function ajax_getAllClientDetails() {
        $requesterData = '';
        $usersData = '<option value="">Select Option</option>';
        $clientData = '';
        if (isset($_POST['value']) && !empty($_POST['value'])) {
            $partner_ids = $_POST['value'];
            //get Cleints
            $getClients = $this->Model_event->getClientsDetails($partner_ids);
            if (isset($getClients['resultSet']) && (count($getClients['resultSet']) > 0)) {
                foreach ($getClients['resultSet'] as $client_data) {
                    $clientData .= '<option value=' . $client_data['id'] . '>' . $client_data['client_title'] . '</option>';
                }
            }
        }
        $return['clientData'] = $clientData;
        echo json_encode($return);
    }

}
