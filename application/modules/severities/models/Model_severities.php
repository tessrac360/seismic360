<?php

class Model_severities extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->severities = 'severities';
        $this->users = 'users';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }


    public function insertSeverity($postData = array()){
		$this->db->insert($this->severities, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }

    public function updateSeverity($postData = array(), $id) {
		$this->db->where('id', $id);
        $update_status = $this->db->update($this->severities, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getSeveritiesPagination($search = '', $limit = 0, $start = 0) {
        $this->db->select('*');
        $this->db->from($this->severities);
		//$this->db->where('status', 'Y');
        if ($search != "") {
            $this->db->like('severity', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $gateway->result_array();
    }


    public function getSeverityEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->severities);
        $this->db->where('id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

	public function isDeleteSeverity($postId) {
        if ($postId != "") {
            $this->db->where('id', $postId);
            $this->db->delete($this->severities);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('id', $id);
            $this->db->update($this->severities, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	
}

?>