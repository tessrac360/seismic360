<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Severities extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_severities");
       // $this->load->model('client/Model_client');	
        $this->load->library('form_validation');
    }

    public function index() {
		
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		$config['base_url'] = base_url() . 'severities/index/';
        $config['total_rows'] = $this->Model_severities->getSeveritiesPagination($search);
		$config['uri_segment'] = 3;		      
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;      
        $data['severities'] = $this->Model_severities->getSeveritiesPagination($search, $config['per_page'], $page);
		$data['hiddenURL'] = 'severities/index/';
		//pr($data['clientTitles']);exit;
        $data['file'] = 'view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }
	
    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Model_severities->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Severity is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Severity is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }	

    public function create() {
       $roleAccess = helper_fetchPermission('67', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('severity', 'Severity Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $severityPost = $this->input->post();				                   
                    $severityPost = array_merge($severityPost, $this->cData);
                    $resultData = $this->Model_severities->insertSeverity($severityPost);
					//pr($resultData);exit;
	                if (!empty($resultData)) {
                        $this->session->set_flashdata("success_msg", "Severity is created successfully ..!!");                        
						redirect('severities/');								
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('severities/create/');                      
                    }
                } else {					
                    $data['file'] = 'create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {		
                $data['file'] = 'create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($postId = NULL) {			
		//$client_id = decode_url($client_id);
        $roleAccess = helper_fetchPermission('67', 'edit');
		//echo $roleAccess;exit;
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId);
                if ($this->input->post()) {
                    $postSeverity = $this->input->post();				
					$this->form_validation->set_rules('severity', 'Severity Name', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
                        $postSeverity = array_merge($postSeverity, $this->uData); //pr($postUsers);exit;
                        $updateStatus = $this->Model_severities->updateSeverity($postSeverity, $postId);
						//pr($updateStatus);exit;
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "Severity is Updated successfully ..!!");
							redirect('severities/');								
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
							redirect('severities/edit/'.encode_url($postId));							
                        }
                    } else {
                        $data['getSeverity'] = $this->Model_severities->getSeverityEdit($postId);						
                        $data['file'] = 'update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
                    $data['getSeverity'] = $this->Model_severities->getSeverityEdit($postId);					
                    $data['file'] = 'update_form';
                    $this->load->view('template/front_template', $data);
                }
            
        } else {
            redirect('unauthorized');
        }
    }

	public function ajax_checkUniqueSeverityName() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $recordCount = $this->db->get_where($table, array($field => $value))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
		
	public function delete($postId = "") {
        $postId = decode_url($postId);

        if ($postId != "") {
            $deleteRole = $this->Model_severities->isDeleteSeverity($postId);
			//pr($deleteRole);exit;
            if ($deleteRole['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "Severity is deleted successfully..!!");
                redirect('severities/');
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('severities/');
            }
        } else {
            redirect('severities/');
        }
    }


}
?>