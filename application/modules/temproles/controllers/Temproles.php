<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Temproles extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model('Module_temproles');
        $this->load->model('Web_module/Module_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $roleAccess = helper_fetchPermission('50', 'view');
        if ($roleAccess == 'Y') {
            $this->getrole();
        } else {
            redirect('unauthorized');
        }
    }
    
    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Module_temproles->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                   $return['message'] = 'User Account is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'User Account is Deactivated successfully';
                }
                
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function getrole() {
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'roles/getrole/';
        $config['total_rows'] = count($this->Module_temproles->getRolesPagin());
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['role'] = $this->Module_temproles->getRolesPagin("", $config['per_page'], $page);
        $data['hiddenURL'] = 'temproles/searchData';
        $data['search'] = '';
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }

    public function searchdata() {
        $search = $this->input->get_post('sd');
        if ($search == "") {
            redirect('roles');
        }
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'roles/searchdata/';
        $config['total_rows'] = count($this->Module_temproles->getSearchData($search));
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $config['first_url'] = base_url() . "roles/searchdata/0?sd=" . $search;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['role'] = $this->Module_temproles->getSearchData($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'temproles/searchData';
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }

    public function create() {
        $roleAccess = helper_fetchPermission('50', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('title', 'Role', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $group = $this->input->post();
                    $creaedby = $this->session->userdata('uid');
                    //$createdDate = date('Y-m-d H:i:s');
                    $title = $this->input->post('title');
                    $checkRole = $this->Module_temproles->CheckRoleGroupData($title);
                    if ($checkRole['status'] == 'true') {
                        $createRole = $this->Module_temproles->createRole($group);
                        if ($createRole['status'] == 'true') {
                            $roleId = $createRole['roleid'];

                            for ($i = 0; $i < count($group['roleid']); $i++) {
                                $moduleid = $group['roleid'][$i];

                                if (!empty($group['chkadd_' . $moduleid]) || !empty($group['chkedit_' . $moduleid]) || !empty($group['chkconfig_' . $moduleid]) || !empty($group['chkview_' . $moduleid]) || !empty($group['chkactive_' . $moduleid]) || !empty($group['chkdelete_' . $moduleid]))
                                    if ($group['chkadd_' . $moduleid] != "") {
                                        if ($group['chkadd_' . $moduleid]) {
                                            $chkadd = "Y";
                                        } else {
                                            $chkadd = "N";
                                        }

                                        if ($group['chkedit_' . $moduleid]) {
                                            $chkedit = "Y";
                                        } else {
                                            $chkedit = "N";
                                        }

                                        if ($group['chkview_' . $moduleid]) {
                                            $chkview = "Y";
                                        } else {
                                            $chkview = "N";
                                        }

                                        if ($group['chkactive_' . $moduleid]) {
                                            $chkactive = "Y";
                                        } else {
                                            $chkactive = "N";
                                        }

                                        if ($group['chkdelete_' . $moduleid]) {
                                            $chkdelete = "Y";
                                        } else {
                                            $chkdelete = "N";
                                        }

                                        $permissionDetails[] = array("role_id" => $roleId, 'module_id' => $moduleid, "add" => $chkadd, "edit" => $chkedit, "view" => $chkview, "active" => $chkactive, "delete" => $chkdelete);
                                    }
                            }

                            $CreatePermission = $this->Module_temproles->insertPermission($permissionDetails);
                            if ($CreatePermission['status'] == 'true') {
                                $this->session->set_flashdata("success_msg", "Role Created successfully..!!");
                                redirect('temproles');
                            } else {
                                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                                redirect('temproles/create');
                            }
                        } else {
                            $this->session->set_flashdata("error_msg", "Role cannot be inserted");
                            redirect('temproles/create');
                        }
                    } else {
                        $this->session->set_flashdata("error_msg", "Role title already exists");
                        redirect('temproles/create');
                    }
                } else {
                    $data['priv_data'] = $this->input->post();
                    $data['pageTitle'] = 'Your page title';
                    $data['getRecords'] = $this->Module_model->getModule();
                    $data['file'] = 'create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['pageTitle'] = 'Your page title';
                $data['getRecords'] = $this->Module_model->getModule();
                $data['file'] = 'create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($postId = NULL) {
        $roleAccess = helper_fetchPermission('50', 'view');
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId);
            if ($this->input->post()) {
                $this->form_validation->set_rules('title', 'Role', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $group = $this->input->post();
                    // pr($group);exit;
                    $creaedby = $this->session->userdata('uid');
                    //$createdDate = date('Y-m-d H:i:s');
                    $title = $this->input->post('title');
                    $hid_title = $this->input->post('hid_title');
                    $checkRole = $this->Module_temproles->CheckRoleGroupDataForEdit($title, $hid_title);
                    //pr($checkRole);
                    if ($checkRole['status'] == 'true') {
                        $updateRole = $this->Module_temproles->updateRole($group, $postId);
                        if ($updateRole['status'] == 'true') {
                            $isDelete = $this->Module_temproles->deleteRoleById($postId);
                            if ($isDelete['status'] == 'true') {
                                $roleId = $postId;

                                for ($i = 0; $i < count($group['roleid']); $i++) {
                                    $moduleid = $group['roleid'][$i];

                                    if (!empty($group['chkadd_' . $moduleid]) || !empty($group['chkedit_' . $moduleid]) || !empty($group['chkconfig_' . $moduleid]) || !empty($group['chkview_' . $moduleid]) || !empty($group['chkactive_' . $moduleid]) || !empty($group['chkdelete_' . $moduleid]))
                                        if ($group['chkadd_' . $moduleid] != "") {
                                            if ($group['chkadd_' . $moduleid]) {
                                                $chkadd = "Y";
                                            } else {
                                                $chkadd = "N";
                                            }

                                            if ($group['chkedit_' . $moduleid]) {
                                                $chkedit = "Y";
                                            } else {
                                                $chkedit = "N";
                                            }

                                            if ($group['chkview_' . $moduleid]) {
                                                $chkview = "Y";
                                            } else {
                                                $chkview = "N";
                                            }

                                            if ($group['chkactive_' . $moduleid]) {
                                                $chkactive = "Y";
                                            } else {
                                                $chkactive = "N";
                                            }

                                            if ($group['chkdelete_' . $moduleid]) {
                                                $chkdelete = "Y";
                                            } else {
                                                $chkdelete = "N";
                                            }

                                            $permissionDetails[] = array("role_id" => $roleId, 'module_id' => $moduleid, "add" => $chkadd, "edit" => $chkedit, "view" => $chkview, "active" => $chkactive, "delete" => $chkdelete);
                                        }
                                }

                                $CreatePermission = $this->Module_temproles->insertPermission($permissionDetails);
                                if ($CreatePermission['status'] == 'true') {
                                    $this->session->set_flashdata("success_msg", "Role Updated successfully..!!");
                                    redirect('temproles');
                                } else {
                                    $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                                    redirect('temproles/edit/' . encode_url($postId));
                                }
                            }
                        } else {
                            $this->session->set_flashdata("error_msg", "Something wents wrongs");
                            redirect('temproles/edit/' . encode_url($postId));
                        }
                        exit;
                    } else {
                        $this->session->set_flashdata("error_msg", "Role title already exists");
                        redirect('temproles/edit/' . encode_url($postId));
                    }
                } else {
                    $data['priv_data'] = $this->input->post();
                    $data['pageTitle'] = 'Your page title';
                    $data['getRecords'] = $this->Module_model->getModule();
                    $data['fetchRole'] = $this->Module_temproles->fetchRoleEdit($postId);
                    $data['file'] = 'update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['pageTitle'] = 'Your page title';
                $data['getRecords'] = $this->Module_model->getModule();
                $data['fetchRole'] = $this->Module_temproles->fetchRoleEdit($postId);
                $data['file'] = 'update_form';
                // pr($data);exit;
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function delete() {
        $data['file'] = 'delete_form';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_status() {
        $data['file'] = 'delete_form';
        $this->load->view('template/front_template', $data);
    }

}
