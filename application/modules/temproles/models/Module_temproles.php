<?php

class Module_temproles extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'temp_role';
        $this->company_id = $this->session->userdata('company_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');

        $this->cData = array('created_on' => $current_date, 'created_by' => $this->userId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $this->userId);
    }

    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            $this->db->where('id', $id);
            $this->db->update($this->tablename, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

    public function fetchRoleEdit($Id) {
        $this->db->select('id,title');
        $this->db->from($this->tablename);
        $this->db->where('id', $Id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->row();    // For executing single row records            
        }
        //pr($data);exit;
        return $data;
    }

    public function getCompanyId_helper() {
        if ($this->user_type == 'S') {
            $data = $this->userId;
        } else if ($this->user_type == 'A') {
            $data = 1;
        } else if ($this->user_type == 'O') {
            $data = $this->company_id;
        }

        return $data;
    }

    /*
     * Objective: insert Permission
     * parameters: Null
     * Return: Null
     */

    public function insertPermission($postPermission) {
        $insertid = $this->db->insert_batch('temp_role_permission', $postPermission);
        if ($insertid != '') {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function fetchPermission($roleID, $moduleID) {
        $this->db->from('temp_role_permission');
        $this->db->where('role_id', $roleID);
        $this->db->where('module_id', $moduleID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data = $row;
            }
        } else {
            $data = array();
        }
        return $data;
    }

    /*
     * Objective: Left side menu show using role_id
     * parameters: Null
     * Return: Null
     */

    public function getPermissionForLogin() {
        $roleId = $this->session->userdata('role_id');
        $this->db->select('rp.module_id,m.title,m.icons,m.link,m.full_link,m.is_linkshow,m.status,m.primary_menuid,m.menu_level');
        $this->db->from('temp_role_permission as rp');
        $this->db->join('module as m', 'm.id = rp.module_id');
        $this->db->where('rp.role_id', $roleId);
        $this->db->where('m.menu_level', '0');
        $this->db->where('m.status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                $this->db->select('rp.module_id,m.title,m.icons,m.link,m.full_link,m.is_linkshow,m.status,m.primary_menuid,m.menu_level');
                $this->db->from('temp_role_permission as rp');
                $this->db->join('module as m', 'm.id = rp.module_id');
                $this->db->where('m.primary_menuid', $value->module_id);
                $this->db->where('rp.role_id', $roleId);
                $this->db->where('m.menu_level', '1');
                $this->db->where('m.status', 'Y');
                $querySubCaterory = $this->db->get();
                //echo $this->db->last_query();
                if ($querySubCaterory->num_rows() > 0) {
                    $subcat = array();
                    foreach ($querySubCaterory->result() as $valuesub) {
                        $this->db->select('rp.module_id,m.title,m.icons,m.link,m.full_link,m.is_linkshow,m.status,m.primary_menuid,m.menu_level');
                        $this->db->from('temp_role_permission as rp');
                        $this->db->join('module as m', 'm.id = rp.module_id');
                        $this->db->where('m.primary_menuid', $valuesub->module_id);
                        $this->db->where('rp.role_id', $roleId);
                        $this->db->where('m.menu_level', '2');
                        $this->db->where('m.status', 'Y');
                        $querySubSubCaterory = $this->db->get();
                        if ($querySubSubCaterory->num_rows() > 0) {
                            $subcatlevel3 = array();
                            foreach ($querySubSubCaterory->result() as $valuesubsub) {
                                $subcatlevel3[] = $valuesubsub;
                            }
                            $valuesub->menu_second_level = $subcatlevel3;
                        } else {
                            $valuesub->menu_second_level = array();
                        }

                        $subcat[] = $valuesub;
                    }

                    $value->menu_one_level = $subcat;
                } else {
                    $value->menu_one_level = array();
                }

                $returnSet[] = $value;
            }
            // For executing single row records
            $return['status'] = 'true';
            $return['resultSet'] = $returnSet;
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Records not found';
        }
        return $return;
    }

    public function CheckRoleGroupData($title) {
        //$createdby = $_SESSION['uid'];
        $return = array();
        $this->db->from('temp_role');
        $this->db->where('title', $title);       
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'false';
        } else {
            $return['status'] = 'true';
        }
        return $return;
    }

    public function CheckRoleGroupDataForEdit($title, $hidtitle) {
        $getCompanyId = helpler_getCompanyId();
        $return = array();
        $this->db->from('temp_role');
        $this->db->where('title', $title);
        $this->db->where_not_in('title', $hidtitle);
        //$this->db->where('company_id', $getCompanyId);
        $query = $this->db->get();
        echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $return['status'] = 'false';
        } else {
            $return['status'] = 'true';
        }
        return $return;
    }

    /*
     * Objective: Role Create
     * parameters: Null
     * Return: Null
     */

    public function createRole($groupPost) {
        $group['title'] = $groupPost['title'];
        
        $this->db->insert('temp_role', $group);
        $lastInsertId = $this->db->insert_id();
        if ($lastInsertId != '') {
            $return['status'] = 'true';
            $return['roleid'] = $lastInsertId;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateRole($groupPost, $id) {
        unset($rolePost['hid_title']);
        $group['title'] = $groupPost['title'];
        //$group = array_merge($group, $this->uData);
        //$group['company_id'] = helpler_getCompanyId();
        $this->db->where('id', $id);
        $getdata = $this->db->update('temp_role', $group);
        if ($getdata != '') {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function deleteRoleById($roleId) {
        $this->db->where('role_id', $roleId);
        $deleteStaus = $this->db->delete('temp_role_permission');
        if ($deleteStaus != '') {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    /*
     * Objective: Pagination of Role module
     * parameters: Null
     * Return: Null
     */

    public function getRolesPagin($whr = "", $limit = "", $start = "") {
        $getCompanyId = helpler_getCompanyId();        
        $this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get($this->tablename);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

    /*
     * Objective: Pagination of Role moudle with Searching
     * parameters: Null
     * Return: Null
     */

    public function getSearchData($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {
        $getCompanyId = helpler_getCompanyId();
        if ($search != "") {
            $this->db->or_like('title', $search);
            //$this->db->or_like('', $searchTerm2);
            //$this->db->or_like('list.description', $searchTerm3);
        }       
        $this->db->order_by($order_by, $order_type);
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get($this->tablename);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

    public function fetchMethods($moduleID, $method) {
        $user = $this->session->userdata('id');
        $roleId = $this->session->userdata('role_id');
        $this->db->select('`' . $method . '` as rstatus');
        $this->db->from('temp_role_permission');
        $this->db->where_in('role_id', $roleId);
        $this->db->where('module_id', $moduleID);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data = $row->rstatus;
            }
        } else {
            $data = 'N';
        }
        return $data;
    }

}

?>