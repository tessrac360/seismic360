<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_log extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->setting = 'setting';
        $this->client = 'client';
        $this->company_id = $this->session->userdata('company_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->client_id = $this->session->userdata('client_id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $this->cData = array('created_on' => $current_date, 'created_by' => $this->userId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $this->userId);
    }

    public function LogFetchRecord($tableName = "", $key, $value = "") {
        $this->db->select('clientC.*,clientP.client_title as parentClientName');
        $this->db->from($tableName . ' as clientC');
        $this->db->join($tableName . ' as clientP', 'clientP.id = clientC.parent_client_id');
        $this->db->where('clientC.' . $key, $value);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $rows = $query->row_array();    // For executing single row records
            $return['status'] = 'true';
            $return['log'] = serialize($rows);
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function createLogActions($logTableName = "", $clientId = "", $actions = "", $oldValue = "", $newValue = "") {
        if ($clientId != '') {
            if ($oldValue == "") {
                $data = array(
                    'client_id' => $clientId,
                    'action' => $actions,
                    'new_value' => $newValue
                );
            } else if ($newValue == "") {
                $data = array(
                    'client_id' => $clientId,
                    'action' => $actions,
                    'old_value' => $oldValue,
                );
            } else {
                $data = array(
                    'client_id' => $clientId,
                    'action' => $actions,
                    'old_value' => $oldValue,
                    'new_value' => $newValue
                );
            }

            $data = array_merge($data, $this->cData);
            // pr($data);exit;
            $this->db->insert($logTableName, $data);
            $lastInsertId = $this->db->insert_id();
            if ($lastInsertId != '') {
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function LogFetchRecordUser($tableName = "", $key, $value = "") {
        $this->db->select('user.*,role.title as roleTitle,client.client_title as clientName,group_concat(DISTINCT usrsubman.client_id) as clientaccess,group_concat(DISTINCT usrdom.domain_id) as domainids');
        $this->db->from($tableName . ' as user');
        $this->db->join('role as role', 'user.role_id = role.id');
        $this->db->join('client as client', 'client.id = user.client');
        $this->db->join('users_subclient_manage as usrsubman', 'usrsubman.user_id = user.id');
        $this->db->join('users_domain as usrdom', 'usrdom.user_id = user.id');
        $this->db->where('user.' . $key, $value);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $rows = $query->row_array();    // For executing single row records
            $this->db->select('group_concat(title) as skilltitles');
            $this->db->from('skill_set');
            $skillIds = explode(",", $rows['skill']);
            $this->db->where_in('skill_id', $skillIds);
            $query = $this->db->get();
            $row = $query->row_array();
            $rows['skilltitles'] = $row['skilltitles'];
            $this->db->select('group_concat(client_title) as clientAccessTitles');
            $this->db->from('client');
            $clientIds = explode(",", $rows['clientaccess']);
            $this->db->where_in('id', $clientIds);
            $query = $this->db->get();
            $row = $query->row_array();
            $rows['clientAccessTitles'] = $row['clientAccessTitles'];
            $this->db->select('group_concat(title) as domainTitles');
            $this->db->from('group');
            $domainIds = explode(",", $rows['domainids']);
            $this->db->where_in('id', $domainIds);
            $query = $this->db->get();
            $row = $query->row_array();
            $rows['domainTitles'] = $row['domainTitles'];
            $return['status'] = 'true';
            $return['log'] = serialize($rows);           
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

}
