<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class SmsTemp extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model('Module_settings');
        $this->load->model('users/Model_users');
        $this->load->library('form_validation');
    }

   

}
