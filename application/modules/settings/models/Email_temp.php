<?php

class Email_temp extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getEmailContent($slugName = "", $pclientId = "") {
        $this->db->select('subject,content');
        $this->db->from('setting');
        $this->db->where('type', '1');
        $this->db->where('status', 'Y');
        $this->db->where('slug', $slugName);
        if ($pclientId != "") {
            $this->db->where_in('company_id', $pclientId);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data = $row;
            }
            return $data;
        }
        return false;
    }

    public function getReplayEmailID($slugName = "", $clientId = "") {
        $this->db->select('content');
        $this->db->from('setting');
        $this->db->where('type', '2');
        $this->db->where('status', 'Y');
        $this->db->where('slug', $slugName);
        if ($clientId != "") {
            $this->db->where_in('client', $clientId);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data = $row->content;
            }
            return $data;
        }
        return false;
    }

}
?>

