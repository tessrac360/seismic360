<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model('Module_settings');
        $this->load->model('users/Model_users');
        $this->load->library('form_validation');
    }

    function slog_create($str) {
        return strtoupper(str_replace(" ", "_", trim($str)));
    }

    public function create() {
        $roleAccess = helper_fetchPermission('54', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {               
                $this->form_validation->set_rules('slug', 'Slug', 'required|is_unique[setting.slug]');
                $this->form_validation->set_rules('subject', 'Subject', 'required');
                $this->form_validation->set_rules('content', 'Content', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $settingPost = $this->input->post();
                    unset($settingPost['files']);
                    $settingPost = array_merge($settingPost, $this->cData);
                    $settingPost['slug'] = 'EMAIL_' . $this->slog_create($settingPost['slug']);                    
                    //pr($settingPost);exit;
                    $resultData = $this->Module_settings->insertEmail($settingPost);
                    if ($resultData['status'] == 'true') {
                        $lastInsertId = $resultData['lastId'];
                        $this->session->set_flashdata("success_msg", "Email Template is created successfully ..!!");
                        redirect('settings');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('settings/create');
                    }
                } else {
                    $data['clientMacro'] = $this->Module_settings->getMacroInfo();
                    $data['client'] = $this->Model_users->getClient();
                    $data['file'] = 'view_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['clientMacro'] = $this->Module_settings->getMacroInfo();
                $data['file'] = 'createemail_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function index() {
        $roleAccess = helper_fetchPermission('54', 'view');
        if ($roleAccess == 'Y') {
            $this->load->library('pagination');
            $config['base_url'] = base_url() . 'settings/index/';
            $config['total_rows'] = count($this->Module_settings->getSettingPagination());
            $config['per_page'] = 10;
            $config['uri_segment'] = 3;
            $config['display_pages'] = TRUE;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['settings'] = $this->Module_settings->getSettingPagination($config['per_page'], $page);
            $data['hiddenURL'] = 'settings/searchdata';
            $data['search'] = '';
            $data['file'] = 'view_form';
            //pr($data);exit;
            $this->load->view('template/front_template', $data);
        } else {
            redirect('unauthorized');
        }
    }

    public function searchdata() {
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'settings/searchData/';
        $config['total_rows'] = count($this->Module_settings->getSettingSearchData($search));
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        //$config['first_url'] = base_url() . "client/searchdata/0?sd=" . $search;
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['settings'] = $this->Module_settings->getSettingSearchData($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'settings/searchdata';
        $data['file'] = 'view_form';
        //pr($data);exit;
        $this->load->view('template/front_template', $data);
    }

    public function edit($id = "") {
        $roleAccess = helper_fetchPermission('54', 'add');
        if ($roleAccess == 'Y') {
            $id = decode_url($id); //exit;
            if ($this->input->post()) {               
                $this->form_validation->set_rules('subject', 'Subject', 'required');
                $this->form_validation->set_rules('content', 'Content', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $settingPost = $this->input->post();
                    unset($settingPost['files']);
                    $settingPost = array_merge($settingPost, $this->uData);                  
                    //pr($settingPost);exit;
                    $resultData = $this->Module_settings->updateEmail($settingPost, $id);
                    if ($resultData['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "Email Template is Updated successfully ..!!");
                        redirect('settings');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('settings/edit');
                    }
                } else {
                    $data['clientMacro'] = $this->Module_settings->getMacroInfo();
                    $data['client'] = $this->Model_users->getClient();
                    $data['getEmailSettings'] = $this->Module_settings->getEmailSettingsEdit($id);
                    $data['file'] = 'updateemail_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['clientMacro'] = $this->Module_settings->getMacroInfo();
                $data['client'] = $this->Model_users->getClient();
                $data['getEmailSettings'] = $this->Module_settings->getEmailSettingsEdit($id);
                $data['file'] = 'updateemail_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Module_settings->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Email is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Email is Deactivated successfully';
                }
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function ajax_checkUnique() {
        $value = $this->input->post('value');
        $field = 'slug';
        $recordCount = $this->db->get_where('setting', array($field => $value))->num_rows();
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

    public function smsandothers() {
        $roleAccess = helper_fetchPermission('54', 'view');
        if ($roleAccess == 'Y') {
            $this->load->library('pagination');
            $config['base_url'] = base_url() . 'settings/index/';
            $config['total_rows'] = count($this->Module_settings->getSettingSMSPagination());
            $config['per_page'] = PAGINATION_PERPAGE;
            $config['uri_segment'] = 3;
            $config['display_pages'] = TRUE;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['settings'] = $this->Module_settings->getSettingSMSPagination($config['per_page'], $page);
            $data['hiddenURL'] = 'settings/searchdatasetting';
            $data['search'] = '';
            $data['file'] = 'smsandothers';
            //pr($data);exit;
            $this->load->view('template/front_template', $data);
        } else {
            redirect('unauthorized');
        }
    }

    public function searchdatasetting() {
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'settings/searchdatasetting/';
        $config['total_rows'] = count($this->Module_settings->getSettingSearchDataSMS($search));
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        //$config['first_url'] = base_url() . "client/searchdata/0?sd=" . $search;
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['settings'] = $this->Module_settings->getSettingSearchDataSMS($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'settings/searchdatasetting';
        $data['file'] = 'smsandothers';
        //pr($data);exit;
        $this->load->view('template/front_template', $data);
    }

    public function create_smsandothers() {
        $roleAccess = helper_fetchPermission('55', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('client[]', 'Client', 'required');
                $this->form_validation->set_rules('slug', 'Slug', 'required|is_unique[setting.slug]');
                $this->form_validation->set_rules('subject', 'Subject', 'required');
                $this->form_validation->set_rules('content', 'Content', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $settingPost = $this->input->post();
                    unset($settingPost['files']);
                    $settingPost = array_merge($settingPost, $this->cData);
                    $settingPost['slug'] = ($settingPost['setting_type'] == 2) ? 'SMS_' . $this->slog_create($settingPost['slug']) : 'EMAILID_' . $this->slog_create($settingPost['slug']);
                    $settingPost['client'] = implode(",", $settingPost['client']);
                    $settingPost['type'] = '2';
                    //pr($settingPost);exit;
                    $resultData = $this->Module_settings->insertEmail($settingPost);
                    if ($resultData['status'] == 'true') {
                        $lastInsertId = $resultData['lastId'];
                        $this->session->set_flashdata("success_msg", ($settingPost['setting_type'] == 2) ? "SMS is created successfully ..!!" : "EmailID is created successfully ..!!");
                        redirect('settings/smsandothers');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('settings/create_smsandothers');
                    }
                } else {
                    $data['client'] = $this->Model_users->getClient();
                    $data['file'] = 'create_smsandothers';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['client'] = $this->Model_users->getClient();
                $data['file'] = 'create_smsandothers';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit_smsandothers($id = "") {

        $roleAccess = helper_fetchPermission('54', 'add');
        if ($roleAccess == 'Y') {
            $id = decode_url($id); //exit;
            if ($this->input->post()) {
                $this->form_validation->set_rules('client[]', 'Client', 'required');
                $this->form_validation->set_rules('subject', 'Subject', 'required');
                $this->form_validation->set_rules('content', 'Content', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $settingPost = $this->input->post();

                    unset($settingPost['files']);
                    $settingPost = array_merge($settingPost, $this->uData);
                    $settingPost['client'] = implode(",", $settingPost['client']);
                    //pr($settingPost);
                    //exit;
                    $resultData = $this->Module_settings->updateEmail($settingPost, $id);
                    if ($resultData['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", ($settingPost['setting_type'] == 2) ? "SMS is updated successfully ..!!" : "EmailID is updated successfully ..!!");
                        redirect('settings/smsandothers');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('settings/edit_smsandothers');
                    }
                } else {
                    $data['client'] = $this->Model_users->getClient();
                    $data['getEmailSettings'] = $this->Module_settings->getEmailSettingsEdit($id);
                    $data['file'] = 'edit_smsandothers';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['client'] = $this->Model_users->getClient();
                $data['getEmailSettings'] = $this->Module_settings->getEmailSettingsEdit($id);
                $data['file'] = 'edit_smsandothers';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function tempDesign() {
        // echo "hiiii";exit;
        $emilListing = helperEmailContent('EMAIL_FORGOT_PASSWORD');
        //pr($emilListing);exit;
        if (!empty($emilListing)) {
            $subject = $emilListing->subject;

            $url = base_url();
            $link = "<a href='$url'>Click here</a>";
            echo $message = email_forgetPassword($emilListing->content, $link);
            $fromEmailID = helperReplayEmailID('EMAILID_NO_REPLY');
            //pr($fromEmailID);


            exit;
            // $isSend = $isSend = helperSendMail('kkc1909@gmail.com', $fromEmailID, $subject, $message, SOCIETY_NAME_EMAIL_SLUG);
            $isSend = helperSendMail($toEmail, $fromEmailID, $subject, $message, SOCIETY_NAME_EMAIL_SLUG);
        }
    }

}
