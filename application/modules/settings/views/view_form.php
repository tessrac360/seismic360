<?php
$AccessAdd = helper_fetchPermission('54', 'add');
$AccessEdit = helper_fetchPermission('54', 'edit');
$AccessStatus = helper_fetchPermission('54', 'active');
?>

<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Settings</span>
                </div>  
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Settings</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <span">Email Template</span>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>List</span>                            
                        </li>
                    </ul>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-9">
                            <?php if ($AccessAdd == 'Y') { ?> 
                                <div class="btn-group">
                                    <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('settings/create') ?>"> Add Email Template
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="col-md-3">
                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search...." id="txt_datatablesearch" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="role">
                    <thead>
                        <tr>                            
                            <th> Subject </th>                           
                            <th> Slug Key </th>
                            <!--<th> Created By </th>-->
                            <?php if ($AccessStatus == 'Y') { ?> 
                                <th> Status </th>
                            <?php } ?>
                            <?php if ($AccessEdit == 'Y') { ?> 
                                <th> Action </th>
                            <?php } ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($settings)) {
                            foreach ($settings as $value) {                               
                                ?>
                                <tr>
                                    <td> <?php echo $value->subject ?> </td>                                                                                                   
                                    <td> <?php echo $value->slug; ?>  </td>    
                                    <!--<td> <?php echo $value->username; ?>  </td>-->       
                                     <?php if($AccessStatus=='Y'){  ?> 
                                    <td>
                                        <input type="checkbox" <?php
                                        if ($value->status == 'Y') {
                                            echo 'checked';
                                        }
                                        ?>  id="onoffactiontoggle" class="onoffactiontoggle" myval="<?php echo encode_url($value->id); ?>">
                                    </td>
                                    <?php } ?>
                                      <?php if($AccessEdit=='Y'){  ?> 
                                    <td>
                                        <a href="<?php echo base_url() . 'settings/edit/' . encode_url($value->id); ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>                                        
                                    </td>
                                    <?php } ?>
                                </tr> 
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="7"> No Record Found </td>
                            </tr> 
<?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_settings.js" type="text/javascript"></script>