<?php
//pr($getEmailSettings);
$getResult = $getEmailSettings['resultSet'];
?>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Create SMS & Others</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Settings</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo base_url('settings/smsandothers') ?>">SMS & Email ID Config</a>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>Edit</span>                            
                        </li>
                    </ul>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" name="frmSettingssmsandemail" id="frmSettingssmsandemail" method="post" action="" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4 clientdiv" style="margin-top: -20px;">
                                <div class="form-group form-md-line-input form-md-floating-label">                                    
                                    <label for="form_control_1">Client<span class="required" aria-required="true">*</span></label> <br>
                                    <select id="client" multiple="multiple" class="client" name="client[]">
                                        <?php
                                         $client_array = explode(',', $getResult->client);
                                        //pr($client_array);
                                        if ($client['status'] == 'true') {
                                            foreach ($client['resultSet'] as $value) {
                                                ?>
                                                <option value="<?php echo $value->id; ?>" <?php if (in_array($value->id, $client_array)) {
                                        echo "selected";
                                    } ?>><?php echo $value->client_title; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                        <option value="0" <?php if (in_array('0', $client_array)) {
                                        echo "selected";
                                    } ?>>Common</option>
                                    </select>
                                    <span id="clienerror" class="other_errors" style="display: none;">Please choose client</span>
                                        <!--<span id="errmsg" class="error" ></span>-->
                                </div>
                            </div>

                            <div class="col-md-4" style="margin-top: 15px;">
                                <div class="form-group form-md-radios">
                                    <div class="md-radio-inline">  

                                        <div class="md-radio">
                                            <input id="radio2"  disabled <?php
                                                    if ($getResult->setting_type == 2) {
                                                        echo 'checked';
                                                    }
                                                    ?> value="2" class="md-radiobtn" type="radio">
                                            <label for="radio2">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span><?php echo 'SMS' ?></label>
                                        </div> 
                                        <div class="md-radio">
                                            <input id="radio3"  disabled <?php
                                                    if ($getResult->setting_type == 3) {
                                                        echo 'checked';
                                                    }
                                                    ?> value="3" class="md-radiobtn" type="radio">
                                            <label for="radio3">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span><?php echo 'EMAIL ID' ?></label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="setting_type" value="<?php echo  $getResult->setting_type?>">
                                </div>

                            </div>

                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" type="text" readonly="readonly" value="<?php echo ($getResult->slug) ? $getResult->slug : ''; ?>" style="text-transform:uppercase">
                                    <label for="form_control_1">Unique Slug<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="subject" value="<?php echo ($getResult->subject) ? $getResult->subject : ''; ?>" name="subject" type="text">
                                    <label for="form_control_1">Title</label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control"  name="content" type="text" value="<?php echo ($getResult->content) ? $getResult->content : ''; ?>">
                                    <label for="form_control_1"> <?php echo ($getResult->setting_type==2)?'SMS Value':'Email id Value'; ?></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
                        </div>
                        <div class="form-actions noborder">                           
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('settings/smsandothers'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_settings.js" type="text/javascript"></script>