<?php

class Module_model extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'users';
        $this->company_id = $this->session->userdata('company_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    private function getPermissionData($menuLevel = "") {
        $this->db->select('rp.module_id');
        $this->db->from('role_permission as rp');
        $this->db->join('module as m', 'm.id = rp.module_id');
        $this->db->where('rp.role_id', $this->roleId);
        $this->db->where('m.menu_level', $menuLevel);
        $this->db->where('m.status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['status'] = 'true';
            $data = $query->result_array();
            $rows = implode(",", array_column($data, 'module_id'));
            $return['status'] = 'true';
            $return['resultSet'] = explode(',', $rows);
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    public function getModule($client_id="") {
        //echo $client_id;//exit;
        $menu = $this->getPermissionData(0);
        $menuOne = $this->getPermissionData(1);
        $menutTwo = $this->getPermissionData(2);
        $this->db->select('id,title,menu_level,is_linkshow');
        $this->db->from('module');
        $this->db->where('menu_level', '0');
        $this->db->where('status', 'Y');         
        if ($this->getCompanyId != 1) {
            if ($menu['status'] == 'true') {
                $this->db->where_in('id', $menu['resultSet']);
            }
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                $this->db->select('id,title,menu_level,is_linkshow');
                $this->db->from('module');
                $this->db->where('primary_menuid', $value->id);
                $this->db->where('menu_level', '1');              
                $this->db->where_not_in('id', 52);                
                $this->db->where('status', 'Y'); 
                if ($this->getCompanyId != 1) {
                    if ($menuOne['status'] == 'true') {
                        $this->db->where_in('id', $menuOne['resultSet']);
                    }
                }
                $querySubCaterory = $this->db->get();
                if ($querySubCaterory->num_rows() > 0) {
                    $subcat = array();
                    foreach ($querySubCaterory->result() as $valuesub) {
                        $this->db->select('id,title,menu_level');
                        $this->db->from('module');
                        $this->db->where('primary_menuid', $valuesub->id);
                        $this->db->where('menu_level', '2');
                        $this->db->where('status', 'Y'); 
                        if ($this->getCompanyId != 1) {
                            if ($menutTwo['status'] == 'true') {
                                $this->db->where_in('id', $menutTwo['resultSet']);
                            }
                        }
                        $querySubSubCaterory = $this->db->get();
                        if ($querySubSubCaterory->num_rows() > 0) {
                            $subcatlevel3 = array();
                            foreach ($querySubSubCaterory->result() as $valuesubsub) {
                                $subcatlevel3[] = $valuesubsub;
                            }
                            $valuesub->menu_second_level = $subcatlevel3;
                        } else {
                            $valuesub->menu_second_level = array();
                        }

                        $subcat[] = $valuesub;
                    }
                    $value->menu_one_level = $subcat;
                } else {
                    $value->menu_one_level = array();
                }

                $returnSet[] = $value;
            }
            $status['resultSet'] = $returnSet;
            $status['status'] = TRUE;
        } else {
            $status['status'] = FALSE;
            $status['message'] = "Record not found";
        }
        //pr($status);exit;
        return $status;
    }

}

?>