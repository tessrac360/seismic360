<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <span>Role Permission</span>
        </li>
    </ul>

</div>
<!-- END PAGE HEADER-->

<div class="row">
    <div class="col-md-12">
      <div class="portlet light ">
        <div class="form-group">
            <label>Role Name</label>
            <input class="form-control spinner" placeholder="Role Name" type="text"> 
        </div>
        <!-- BEGIN PORTLET-->
        <label class="error" id="error_module" style="display: none;">Please select module permission</label>
        <form name="frmrole" id="frmrole" method="post">
            <div class="box-body"><div class="mt-checkbox-list">
                    <table class="table table-condensed">
                        <tr>
                            <th></th>
                            <th>All</th>
                            <th>Add</th>
                            <th>Edit</th>
                            <th>View</th>
                            <th>Status</th>
                            <th>Delete</th>
                        </tr>
                        <?php
                        if (!empty($getRecords['resultSet'])) {
                            foreach ($getRecords['resultSet'] as $role) {
                                //pr($role);
                                if ($role->is_linkshow == 'N') {
                                    ?>
                                    <tr>
                                        <td><?php echo '<strong>' . $role->title . '<strong>'; ?>

                                        </td>
                                        <td>
                                            <div class="mt-checkbox-list">
                                                <!--                                            <label class="mt-checkbox mt-checkbox-outline">
                                                                                                <input type="checkbox"  id="<?php echo $role->id; ?>" class="minimal allstatus allcheck checkall_<?php echo $role->id; ?>">
                                                                                               
                                                                                                <span></span>
                                                                                            </label> --><input type="hidden" name="roleid[]" value="<?php echo $role->id; ?>" class="minimal">
                                            </div>

                                        </td>
                                        <td>
                                            <input type="hidden" name="chkadd_<?php echo $role->id; ?>" value="0" class="hid_chkadd_<?php echo $role->id; ?>">
                                            <input type="hidden" name="chkedit_<?php echo $role->id; ?>" value="0" class="hid_chkedit_<?php echo $role->id; ?>">
                                            <input type="hidden" name="chkactive_<?php echo $role->id; ?>" class="hid_chkactive_<?php echo $role->id; ?>" value="0">
                                            <input type="hidden" name="chkdelete_<?php echo $role->id; ?>" class="hid_chkdelete_<?php echo $role->id; ?>" value="0">
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="hidden" name="chkview_<?php echo $role->id; ?>" value="0" class="hid_chkview_<?php echo $role->id; ?>">
                                                    <input type="checkbox" value="chkview_<?php echo $role->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td><?php echo '<strong>' . $role->title . '<strong>'; ?></td>
                                        <td>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox"  id="<?php echo $role->id; ?>" class="minimal allstatus allcheck checkall_<?php echo $role->id; ?>">
                                                    <input type="hidden" name="roleid[]" value="<?php echo $role->id; ?>" class="minimal">
                                                    <span></span>
                                                </label>
                                            </div>



                                        </td>
                                        <td>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="hidden" name="chkadd_<?php echo $role->id; ?>" value="0" class="hid_chkadd_<?php echo $role->id; ?>">
                                                    <input type="checkbox" value="chkadd_<?php echo $role->id; ?>" class="minimal chkpermission allstatus chkadd_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="hidden" name="chkedit_<?php echo $role->id; ?>" value="0" class="hid_chkedit_<?php echo $role->id; ?>">
                                                    <input type="checkbox" value="chkedit_<?php echo $role->id; ?>"  class="minimal chkpermission allstatus chkedit_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="hidden" name="chkview_<?php echo $role->id; ?>" value="0" class="hid_chkview_<?php echo $role->id; ?>">
                                                    <input type="checkbox" value="chkview_<?php echo $role->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="hidden" name="chkactive_<?php echo $role->id; ?>" class="hid_chkactive_<?php echo $role->id; ?>" value="0">
                                                    <input type="checkbox" value="chkactive_<?php echo $role->id; ?>"  class="minimal chkpermission allstatus chkactive_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="hidden" name="chkdelete_<?php echo $role->id; ?>" class="hid_chkdelete_<?php echo $role->id; ?>" value="0">
                                                    <input type="checkbox" value="chkdelete_<?php echo $role->id; ?>"   class="minimal chkpermission allstatus chkdelete_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                <?php
                                if (!empty($role->menu_one_level)) {
                                    foreach ($role->menu_one_level as $menulevelone) {
                                        if ($menulevelone->is_linkshow == 'N') {
                                            ?>
                                            <tr>
                                                <td><?php echo $role->title . ' / <strong>' . $menulevelone->title . '</strong>'; ?></td>
                                                <td>
                                                    <input type="hidden" name="roleid[]" value="<?php echo $menulevelone->id; ?>" class="minimal">
                                                </td>
                                                <td>

                                                </td>
                                                <td>

                                                </td>
                                                <td>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                            <input type="hidden" name="chkview_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkview_<?php echo $menulevelone->id; ?>">
                                                            <input type="checkbox" value="chkview_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="chkadd_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkadd_<?php echo $menulevelone->id; ?>">
                                                    <input type="hidden" name="chkedit_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkedit_<?php echo $menulevelone->id; ?>">
                                                    <input type="hidden" name="chkactive_<?php echo $menulevelone->id; ?>" class="hid_chkactive_<?php echo $menulevelone->id; ?>" value="0">
                                                    <input type="hidden" name="chkdelete_<?php echo $menulevelone->id; ?>" class="hid_chkdelete_<?php echo $menulevelone->id; ?>" value="0">
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <td><?php echo $role->title . ' / <strong>' . $menulevelone->title . '</strong>'; ?></td>
                                                <td>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                            <input type="checkbox" id="<?php echo $menulevelone->id; ?>" class="minimal allstatus allcheck checkall_<?php echo $menulevelone->id; ?>">
                                                            <input type="hidden" name="roleid[]" value="<?php echo $menulevelone->id; ?>" class="minimal">
                                                            <span></span>
                                                        </label>
                                                    </div>



                                                </td>
                                                <td>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                            <input type="hidden" name="chkadd_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkadd_<?php echo $menulevelone->id; ?>">
                                                            <input type="checkbox" value="chkadd_<?php echo $menulevelone->id; ?>" class="minimal chkpermission allstatus chkadd_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                            <input type="hidden" name="chkedit_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkedit_<?php echo $menulevelone->id; ?>">
                                                            <input type="checkbox" value="chkedit_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkedit_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                            <input type="hidden" name="chkview_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkview_<?php echo $menulevelone->id; ?>">
                                                            <input type="checkbox" value="chkview_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                            <input type="hidden" name="chkactive_<?php echo $menulevelone->id; ?>" class="hid_chkactive_<?php echo $menulevelone->id; ?>" value="0">
                                                            <input type="checkbox" value="chkactive_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkactive_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="mt-checkbox-list">
                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                            <input type="hidden" name="chkdelete_<?php echo $menulevelone->id; ?>" class="hid_chkdelete_<?php echo $menulevelone->id; ?>" value="0">
                                                            <input type="checkbox" value="chkdelete_<?php echo $menulevelone->id; ?>"   class="minimal chkpermission allstatus chkdelete_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php }
                                        ?>



                                        <?php
                                        if (!empty($menulevelone->menu_second_level)) {
                                            foreach ($menulevelone->menu_second_level as $menuleveltwo) {
                                                //pr($menuleveltwo);
                                                ?>
                                                <tr>
                                                    <td><?php echo $role->title . ' / ' . $menulevelone->title . ' / <strong>' . $menuleveltwo->title . '</strong>'; ?></td>
                                                    <td>
                                                        <div class="mt-checkbox-list">
                                                            <label class="mt-checkbox mt-checkbox-outline">
                                                                <input type="checkbox" id="<?php echo $menuleveltwo->id; ?>" class="minimal allstatus allcheck checkall_<?php echo $menuleveltwo->id; ?>">
                                                                <input type="hidden" name="roleid[]" value="<?php echo $menuleveltwo->id; ?>" class="minimal">
                                                                <span></span>
                                                            </label>
                                                        </div>



                                                    </td>
                                                    <td>
                                                        <div class="mt-checkbox-list">
                                                            <label class="mt-checkbox mt-checkbox-outline">
                                                                <input type="hidden" name="chkadd_<?php echo $menuleveltwo->id; ?>" value="0" class="hid_chkadd_<?php echo $menuleveltwo->id; ?>">
                                                                <input type="checkbox" value="chkadd_<?php echo $menuleveltwo->id; ?>" class="minimal chkpermission allstatus chkadd_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="mt-checkbox-list">
                                                            <label class="mt-checkbox mt-checkbox-outline">
                                                                <input type="hidden" name="chkedit_<?php echo $menuleveltwo->id; ?>" value="0" class="hid_chkedit_<?php echo $menuleveltwo->id; ?>">
                                                                <input type="checkbox" value="chkedit_<?php echo $menuleveltwo->id; ?>"  class="minimal chkpermission allstatus chkedit_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="mt-checkbox-list">
                                                            <label class="mt-checkbox mt-checkbox-outline">
                                                                <input type="hidden" name="chkview_<?php echo $menuleveltwo->id; ?>" value="0" class="hid_chkview_<?php echo $menuleveltwo->id; ?>">
                                                                <input type="checkbox" value="chkview_<?php echo $menuleveltwo->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="mt-checkbox-list">
                                                            <label class="mt-checkbox mt-checkbox-outline">
                                                                <input type="hidden" name="chkactive_<?php echo $menuleveltwo->id; ?>" class="hid_chkactive_<?php echo $menuleveltwo->id; ?>" value="0">
                                                                <input type="checkbox" value="chkactive_<?php echo $menuleveltwo->id; ?>"  class="minimal chkpermission allstatus chkactive_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="mt-checkbox-list">
                                                            <label class="mt-checkbox mt-checkbox-outline">
                                                                <input type="hidden" name="chkdelete_<?php echo $menuleveltwo->id; ?>" class="hid_chkdelete_<?php echo $menuleveltwo->id; ?>" value="0">
                                                                <input type="checkbox" value="chkdelete_<?php echo $menuleveltwo->id; ?>"   class="minimal chkpermission allstatus chkdelete_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>


                                        <?php
                                    }
                                }
                                ?>

                                <tr style="border-top: 2px solid #383D3D !important;">
                                    <td colspan="7"></td>
                                </tr>

                                <?php
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary groupsubmit">Save</button>
                &nbsp; &nbsp; <a href="<?php echo base_url() . 'role'; ?>" class="btn btn-danger">cancel</a>					
            </div>
        </form>
        <!-- END PORTLET-->
      </div>
    </div>
</div>







<script type="text/javascript">
    $(document).ready(function () {
        $('.multicheck').change(function () { //".checkbox" change 
            //uncheck "select all", if one of the listed checkbox item is unchecked
            id = $(this).val();
            status = $('.' + id).is(':checked');
            var arr = id.split('_');
            checkId = arr[1];
            if ($('.multi_' + checkId + ':checked').length > 1) {
                if (status == "false") { //if this item is unchecked
                    $(".checkall_" + checkId)[0].checked = false; //change "select all" checked status to false
                }

//            //check "select all" if all checkbox items are checked
                console.log($('.multi_' + checkId + ':checked').length);
                console.log($('.multi_' + checkId).length);

                if ($('.multi_' + checkId + ':checked').length == $('.multi_' + checkId).length) {
                    $(".checkall_" + checkId)[0].checked = true; //change "select all" checked status to true
                }
            }
        });




        $('.chkpermission').change(function () {

            permissionVal = $(this).val();
            console.log(permissionVal);
            if ($(this).is(':checked')) {

                $('.hid_' + permissionVal).val('1');
            } else {

                $('.hid_' + permissionVal).val('0');
            }
        });
        $(document).on('click', '.allcheck', function () {
            id = $(this).attr('id');
            status = $(this).is(':checked');

            if (status == "true") {

                $('.hid_chkadd_' + id).val('1');
                $('.hid_chkedit_' + id).val('1');
                $('.hid_chkview_' + id).val('1');
                $('.hid_chkactive_' + id).val('1');
                $('.hid_chkdelete_' + id).val('1');
                $('.hid_chkconfig_' + id).val('1');
                $('.chkadd_' + id).prop('checked', true);
                $('.chkedit_' + id).prop('checked', true);
                $('.chkview_' + id).prop('checked', true);
                $('.chkactive_' + id).prop('checked', true);
                $('.chkdelete_' + id).prop('checked', true);
                $('.chkconfig_' + id).prop('checked', true);

            } else {
                $('.hid_chkadd_' + id).val('0');
                $('.hid_chkedit_' + id).val('0');
                $('.hid_chkview_' + id).val('0');
                $('.hid_chkactive_' + id).val('0');
                $('.hid_chkdelete_' + id).val('0');
                $('.hid_chkconfig_' + id).val('0');
                $('.chkadd_' + id).prop('checked', false);
                $('.chkedit_' + id).prop('checked', false);
                $('.chkview_' + id).prop('checked', false);
                $('.chkactive_' + id).prop('checked', false);
                $('.chkdelete_' + id).prop('checked', false);
                $('.chkconfig_' + id).prop('checked', false);
            }
        });

    });
</script>