<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Web_module extends MX_Controller {

    function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
    }

    public function index() {
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }

    public function createlevelone() {

        $data['pageTitle'] = 'Your p44age title';
        $data['file'] = 'create_form';
        $this->load->view('template/front_template', $data);
    }

    public function updatelevelone() {
        $data['file'] = 'update_form';
        $this->load->view('template/front_template', $data);
    }

    public function deletelevelone() {
        $data['file'] = 'delete_form';
        $this->load->view('template/front_template', $data);
    }

    public function indexleveltwo() {
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }

    public function createleveltwo() {

        //$data['pageTitle'] = 'Your p44age title';
        $data['file'] = 'create_form';
        $this->load->view('template/front_template', $data);
    }

    public function updateleveltwo() {
        $data['file'] = 'update_form';
        $this->load->view('template/front_template', $data);
    }

    public function deleteleveltwo() {
        $data['file'] = 'delete_form';
        $this->load->view('template/front_template', $data);
    }

    public function indexlevelthree() {
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }

    public function createlevelthree() {

        //$data['pageTitle'] = 'Your p44age title';
        $data['file'] = 'create_form';
        $this->load->view('template/front_template', $data);
    }

    public function updatelevelthree() {
        $data['file'] = 'update_form';
        $this->load->view('template/front_template', $data);
    }

    public function deletelevelthree() {
        $data['file'] = 'delete_form';
        $this->load->view('template/front_template', $data);
    }

    public function migrate() {
        $this->db->select('id,client_uuid');
        $this->db->from('client');
        $this->db->where_not_in('id', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                $client_uuid = $value->client_uuid;
                $this->db->select('*');
                $this->db->from('company_locations');
                $this->db->where('location_company', $value->id);
                $queryLocation = $this->db->get();
                if ($queryLocation->num_rows() > 0) {
                    //pr($queryLocation->result());
                    foreach ($queryLocation->result() as $locationval) {
                        $location['location_name'] = $locationval->location_name;
                        $location['referrence_uuid'] = $client_uuid;
                        $location['location_uuid'] = generate_uuid('loca_');
                        $location = array_merge($location, $this->cData);
                        $locationAddress['referrence_uuid'] = $location['location_uuid'];
                        $locationAddress['address_uuid'] = generate_uuid('addr_');
                        $locationAddress['full_address'] = $locationval->location_address1;
                        $locationAddress['city'] = $locationval->location_city;
                        $locationAddress['state'] = $locationval->location_state;
                        $locationAddress['postal_code'] = $locationval->location_zip;
                        $locationAddress = array_merge($locationAddress, $this->cData);
                        if ($this->db->insert('client_locations', $location)) {
                            $this->db->insert('address', $locationAddress);
                        }
                    }
                }
            }
        } else {
            
        }
    }

    public function migrateUser() {
        $this->db->select('cnt.id as client_id,cnt.client_uuid,usr.user_username,usr.user_password,con.contact_first_name,con.contact_last_name,con.contact_email,con.contact_phone');
        $this->db->from('usersacure as usr');
        $this->db->join('contactsacure as con', 'usr.user_contact=con.contact_id');
        $this->db->join('client as cnt', 'cnt.id=con.contact_company');
        //$this->db->where('usr.user_contact=con.contact_id');
        $this->db->where_in('con.contact_company', array('50', '14', '92', '19', '10', '75', '62', '91', '80', '58', '89'));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                $value->uuid = generate_uuid('eeur_');
                $value->role_id = '297';
                $current_date = $this->config->item('datetime');
                $current_user = $this->session->userdata('id');

                $eventedge_users = array(
                    'uuid' => $value->uuid,
                    'first_name' => $value->contact_first_name,
                    'last_name' => $value->contact_last_name,
                    'phone' => $value->contact_phone,
                    'email_id' => $value->contact_email,
                    'user_name' => $value->user_username,
                    'password' => $value->user_password,
                    'role_id' => $value->role_id,
                    'role_uuid' => '',
                    'created_on' => $current_date,
                    'created_by' => $current_user,
                    'company_id' => 1,
                    'user_type' => 'U',
                    'client' => $value->client_id,
                    'client_uuid' => $value->client_uuid,
                    'skill' => '1,2,3,4',
                    'multi_signon_allow' => '1',
                    'is_mac_access' => 'E',
                    'is_approval' => 'N',
                    'assign_client_type' => 'C',
                    'partner_id' => '1'
                );
                if ($this->db->insert('users', $eventedge_users)) {
                    
                }
            }
        } else {
            
        }
    }

    public function migrateCat() {
        $this->db->select('lookup_value');
        $this->db->from('lookup_values');
        $this->db->where('lookup_list', 'HelpDeskCategory');
        $this->db->group_by('lookup_value');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                $list['category'] = $value->lookup_value;
                $list['device_category_uuid'] = generate_uuid('dcat_');
                $list = array_merge($list, $this->cData);
                if ($this->db->insert('device_categories', $list)) {
                    $listsub['device_category_id'] = $this->db->insert_id();
                    $listsub['device_category_uuid'] = $list['device_category_uuid'];
                    $listsub['uuid'] = generate_uuid('dsca_');
                    $listsub = array_merge($listsub, $this->cData);
                    $this->db->select('lookup_value');
                    $this->db->from('lookup_values');
                    $this->db->where('lookup_list', 'HelpDeskCallType');
                    $this->db->group_by('lookup_value');
                    $querySubcat = $this->db->get();
                    if ($querySubcat->num_rows() > 0) {
                        foreach ($querySubcat->result() as $val) {
                            $listsub['subcategory'] = $val->lookup_value;
                            $this->db->insert('device_sub_categories', $listsub);
                        }
                    }
                }
            }
        }
    }

}
