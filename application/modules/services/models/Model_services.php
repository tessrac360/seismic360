<?php

class Model_services extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->services = 'services';
        $this->gateway_types = 'gateway_types';
        $this->users = 'users';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function isExitGateway($id) {
        $this->db->select('id');
        $this->db->from($this->gateway);
        $this->db->where('id', $id);
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function insertService($postData = array()){
		$this->db->insert($this->services, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }

    public function updateService($postData = array(), $service_uuid) {
		$this->db->where('uuid', $service_uuid);
        $update_status = $this->db->update($this->services, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getServicesPagination($search = '', $limit = 0, $start = 0) {
        $this->db->select('ser.*,gt.title');
        $this->db->from($this->services. ' as ser');
        $this->db->join('gateway_types as gt', 'gt.gatewaytype_uuid = ser.gateway_type');
		$this->db->where('is_deleted', '0');
		$this->db->where('gateway_type!=', '5');
        if ($search != "") {
            $this->db->like('ser.service_description', $search);
            $this->db->or_like('ser.check_command_with_arg', $search);
            $this->db->or_like('gt.title', $search);
        }		
        $this->db->order_by('service_id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $service = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $service->num_rows();
        }
        //pr($service->result_array());exit;
        return $service->result_array();
    }

    public function getServiceEdit($service_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->services);
        $this->db->where('service_id', $service_uuid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
	
	public function isDeleteService($service_uuid) {
        if ($service_uuid != "") {
            $data = array('is_deleted' => '1');
            $this->db->where('uuid', $service_uuid);
            $this->db->update($this->services, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('is_disabled' => '0');
            } else if ($status == 'false') {
                $data = array('is_disabled' => '1');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('service_id', $id);
            $this->db->update($this->services, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	public function getClientName($clientId = "") {
        $this->db->select('client_title');
        $this->db->from('client');
        $this->db->where('id', $clientId);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }	
	
	public function getGatewayTypes()
	{
		$this->db->select('*');
        $this->db->from('gateway_types');
		$this->db->where('gatewaytype_uuid!=', '5');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->result_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
}

?>