
<!-- END PAGE HEADER-->

<div class="row">
    <div>
		
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Service</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Account Mgmt</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('services') ?>">Services</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <div class="portlet light padd0">
		<?php
            if ($getService['status'] = 'true') {
               
                $getResult = $getService['resultSet'];
                //pr($getResult);exit;
                ?>
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmSeverity" id="frmSeverity" method="post" action="">
                    <div class="form-body">
                        <div class="row"> 	
							<div class="col-md-12">
								<div class="form-group form-md-line-input form-md-floating-label">
									<select class="form-control edited gateway_type" name="gateway_type" id="gateway_type" data-validation="required" data-validation-error-msg="Please select gateway type" disabled>
									<option value="">--Select--</option>
									<?php if($gatewayTypes['status'] == true){
										foreach($gatewayTypes['resultSet'] as $value){?>
										<option value="<?php echo $value['gatewaytype_uuid'] ?>"  <?= ($getResult->gateway_type == $value['gatewaytype_uuid']) ? 'selected' : ''; ?>><?php echo $value['title'] ?></option>
										<?php }
									}
									?>
									</select>
									<label for="form_control_1">Gateway Type<span class="required" aria-required="true">*</span></label>
								</div>
							</div>
							<?php if($getResult->gateway_type == 1){?>
                            <div class="col-md-12" style='display:block;' id="command">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="check_command_with_arg" name="check_command_with_arg" type="text" tableName='services' tableField='check_command_with_arg' data-validation="required" data-validation-error-msg="Please enter check command" value="<?php echo $getResult->check_command_with_arg;?>">
                                    <label for="form_control_1">Check Command With Arg<span class="required" aria-required="true">*</span></label>                                        
                                </div>
                            </div>
							
							<?php }?>
							<div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control services" id="service_description" name="service_description" type="text" tableName='services' tableField='service_description' data-validation="required" data-validation-error-msg="Please enter service description" value="<?php echo $getResult->service_description;?>">
                                    <label for="form_control_1">Service Descripton<span class="required" aria-required="true">*</span></label>                                        
                                </div>
                            </div>							

                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('services'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
			<?php } ?>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script src="<?php echo base_url() . "public/" ?>js/form/form_services.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });
</script>