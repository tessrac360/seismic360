<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Services extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_services");
       // $this->load->model('client/Model_client');	
        $this->load->library('form_validation');
    }

    public function index() {
		
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		$config['base_url'] = base_url() . 'services/index/';
        $config['total_rows'] = $this->Model_services->getServicesPagination($search);
		$config['uri_segment'] = 3;		      
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;      
        $data['services'] = $this->Model_services->getServicesPagination($search, $config['per_page'], $page);
		$data['hiddenURL'] = 'services/index/';
		//pr($data['clientTitles']);exit;
        $data['file'] = 'view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }
	
    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = $_POST['id'];
        if ($id != "") {
            $Status = $this->Model_services->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Service is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Service is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }	

    public function create() {
       $roleAccess = helper_fetchPermission('84', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('gateway_type', 'Gateway Type', 'required');
                $this->form_validation->set_rules('service_description', 'Service Description', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $servicePost = $this->input->post();
					//pr($servicePost);exit;	
					$servicePost['uuid'] = generate_uuid('eesv_');
					$servicePost = array_merge($servicePost, $this->cData);
					$resultData = $this->Model_services->insertService($servicePost);
	                if ($resultData['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "Service is created successfully ..!!");                        
						redirect('services/');								
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('services/create/');                      
                    }
                } else {	
					$data['gatewayTypes'] = $this->Model_services->getGatewayTypes();				
                    $data['file'] = 'create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				$data['gatewayTypes'] = $this->Model_services->getGatewayTypes();				
                $data['file'] = 'create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($service_uuid = NULL) {			
		//$client_id = decode_url($client_id);
        $roleAccess = helper_fetchPermission('84', 'edit');
		//echo $roleAccess;exit;
		$service_uuid = decode_url($service_uuid);
        if ($roleAccess == 'Y') {
                if ($this->input->post()) {
                    //$this->form_validation->set_rules('gateway_type', 'Gateway Type', 'required');
					$this->form_validation->set_rules('service_description', 'Service Description', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
					if ($this->form_validation->run() == TRUE) {
						$servicePost = $this->input->post();
						//pr($servicePost);exit;	
						$servicePost = array_merge($servicePost, $this->uData);
						$updateStatus = $this->Model_services->updateService($servicePost,$service_uuid);
						
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "Service is Updated successfully ..!!");
							redirect('services/');								
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
							redirect('services/edit/'.$service_uuid);							
                        }
                    } else {
						$data['gatewayTypes'] = $this->Model_services->getGatewayTypes();	
                        $data['getService'] = $this->Model_services->getServiceEdit($service_uuid);
                        $data['file'] = 'update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['gatewayTypes'] = $this->Model_services->getGatewayTypes();	
                    $data['getService'] = $this->Model_services->getServiceEdit($service_uuid);
                    $data['file'] = 'update_form';
                    $this->load->view('template/front_template', $data);
                }
            
        } else {
            redirect('unauthorized');
        }
    }

	public function ajax_checkUniqueServiceName() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = trim($this->input->post('value'));
        $gateway_type = $this->input->post('gateway_type');
        $recordCount = $this->db->get_where($table, array($field => $value,'gateway_type' =>$gateway_type))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

	public function deleteService($service_uuid = "") {
        //$service_uuid = $_POST['service_uuid'];
		if ($service_uuid != "") {
            $deleteServices = $this->Model_services->isDeleteService($service_uuid);
			//pr($deleteServices);exit;
            if ($deleteServices['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "Severity is deleted successfully..!!");
                redirect('services/');
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('services/');
            }
        } else {
            redirect('services/');
        }
    }
	
}
?>