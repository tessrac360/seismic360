<?php

class Model_login_account_types extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->login_account_types = 'login_account_types';
        $this->users = 'users';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }



    public function insertlogin_account_types($postData = array()){
		$this->db->insert($this->login_account_types, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }

    public function updatelogin_account_type($postData = array(), $id) {
		//exit;
		$this->db->where('credentials_uuid', $id);
        $update_status = $this->db->update($this->login_account_types, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getAccountPagination($search = '', $limit = 0, $start = 0) {
        $this->db->select('*');
        $this->db->from($this->login_account_types);
		$this->db->where('is_deleted', '0');
        if ($search != "") {
            $this->db->like('login_account_type', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $gateway->result_array();
    }




    public function getlogin_account_typeEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->login_account_types);
        $this->db->where('credentials_uuid', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

	public function isDeletelogin_account_type($postId) {
        if ($postId != "") {
			$data = array('is_deleted' => '1');
            $this->db->where('credentials_uuid', $postId);
			$this->db->update($this->login_account_types, $data);
			//echo $this->db->last_query();exit;
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('is_disabled' => '0');
            } else if ($status == 'false') {
                $data = array('is_disabled' => '1');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('credentials_uuid', $id);
            $this->db->update($this->login_account_types, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	public function getClientName($clientId = "") {
        $this->db->select('client_title');
        $this->db->from('client');
        $this->db->where('id', $clientId);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }	
}

?>