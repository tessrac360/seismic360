<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class LoginAccountTypes extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_login_account_types", "acctyp");
       // $this->load->model('client/Model_client');	
        $this->load->library('form_validation');
    }

    public function index() {
		
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		$config['base_url'] = base_url() . 'LoginAccountTypes/index/';
        $config['total_rows'] = $this->acctyp->getAccountPagination($search);
		$config['uri_segment'] = 3;		      
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;      
        $data['account_types'] = $this->acctyp->getAccountPagination($search, $config['per_page'], $page);
		//pr($data['account_types']);exit;
		$data['hiddenURL'] = 'LoginAccountTypes/index/';
		//pr($data['clientTitles']);exit;
        $data['file'] = 'view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }
	
    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->acctyp->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Login Account Type is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Login Account Type is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }	

    public function create() {
       $roleAccess = helper_fetchPermission('67', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('login_account_type', 'login_account_type Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $login_account_typesPost = $this->input->post();
					$login_account_typesPost['credentials_uuid'] =  generate_uuid('loac_');
                    $login_account_typesPost = array_merge($login_account_typesPost, $this->cData);
                    $resultData = $this->acctyp->insertlogin_account_types($login_account_typesPost);
					//pr($resultData);exit;
	                if (!empty($resultData)) {
                        $this->session->set_flashdata("success_msg", "Login Account Type is created successfully ..!!");                        
						redirect('LoginAccountTypes/');								
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('LoginAccountTypes/create/');                      
                    }
                } else {					
                    $data['file'] = 'create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {		
                $data['file'] = 'create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($postId = NULL) {	
		
		//$client_id = decode_url($client_id);
        $roleAccess = helper_fetchPermission('67', 'edit');
		//echo $roleAccess;exit;
        if ($roleAccess == 'Y') {

            $postId = decode_url($postId);
                if ($this->input->post()) {

                    $postlogin_account_type = $this->input->post();	
				
					$this->form_validation->set_rules('login_account_type', 'login_account_type Name', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
						
                        $postlogin_account_type = array_merge($postlogin_account_type, $this->uData); //pr($postUsers);exit;
                        $updateStatus = $this->acctyp->updatelogin_account_type($postlogin_account_type, $postId);
						//pr($updateStatus);exit;
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "Login Account Type is Updated successfully ..!!");
							redirect('LoginAccountTypes/');								
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
							redirect('LoginAccountTypes/edit/'.encode_url($postId));							
                        }
                    } else {
                        $data['getlogin_account_type'] = $this->acctyp->getlogin_account_typeEdit($postId);						
                        $data['file'] = 'update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {

                    $data['getlogin_account_type'] = $this->acctyp->getlogin_account_typeEdit($postId);					
                    $data['file'] = 'update_form';
                    $this->load->view('template/front_template', $data);
                }
            
        } else {
            redirect('unauthorized');
        }
    }

	public function ajax_checkUniqueAccountName() {
		$table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
		$deleted_field = 'is_deleted';
		$array = array($field => $value, $deleted_field => '0');
		$array = (isset($value) && $value != "")?array_merge($array,array('credentials_uuid!='=>$value)):$array;
        $recordCount = $this->db->get_where($table, $array)->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	
	
	public function delete($postId = "") {
        $postId = decode_url($postId);
		//echo $postId;exit;
		//exit;

        if ($postId != "") {
            $deleteRole = $this->acctyp->isDeletelogin_account_type($postId);
			//pr($deleteRole);exit;
            if ($deleteRole['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "Login Account Type is deleted successfully..!!");
                redirect('LoginAccountTypes/');
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('LoginAccountTypes/');
            }
        } else {
            redirect('LoginAccountTypes/');
        }
    }

}
?>