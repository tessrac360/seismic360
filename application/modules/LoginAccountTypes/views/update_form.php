<div class="row">
    <div>
	
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Edit Login Account Types</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('LoginAccountTypes') ?>">Login Account Types</a>                            
						<i class="fa fa-angle-right"></i>
					</li>							
					<li>
						<span>Edit</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmSeverity" id="frmSeverity" method="post" action="">
                    <div class="form-body">
                        <div class="row">    					
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="login_account_type" name="login_account_type" type="text" data-validation="required" data-validation-error-msg="Please enter login account name" tableName='login_account_types' tableField='login_account_type' value="<?php echo ($getlogin_account_type['resultSet']->login_account_type) ? $getlogin_account_type['resultSet']->login_account_type : ''; ?>">
                                    <label for="form_control_1">login_account_type Name</label>                                               
                                </div>
                            </div>
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('LoginAccountTypes'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
  $(function() {
    // setup validate
    $.validate();
  });
</script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_login_account_types.js" type="text/javascript"></script>