
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Client</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Managements</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo base_url('client') ?>">Client</a>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>List</span>                            
                        </li>
                    </ul>
    

</div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('client/create') ?>"> Add New Client
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover" id="role">
                    <thead>
                        <tr>
                            <th> Client Name </th>
                            <th> Remarks </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($client)) {
                            foreach ($client as $value) {
                                ?>
                                <tr>
                                    <td> <?php echo $value->client_title?> </td>
                                     <td>
                                        <input type="checkbox" <?php if ($value->status == 'Y') {
                            echo 'checked';
                        } ?>  id="onoffactiontoggle" class="onoffactiontoggle" myval="<?php echo encode_url($value->id); ?>">
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url() . 'client/edit/'.encode_url($value->id); ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                      
                                    </td>
                                </tr> 
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="4"> No Record Found </td>
                            </tr> 
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>

