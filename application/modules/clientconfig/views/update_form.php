<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase">Edit Client</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Managements</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo base_url('client') ?>">Client</a>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>Edit</span>                            
                        </li>
                    </ul>

                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" name="frmClient" id="frmClient" method="post" action="">
                    <div class="form-body">
                        <div class="row">                                                                            
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_title" name="client_title" type="text" value="<?php echo ($getClient['resultSet']->client_title) ? $getClient['resultSet']->client_title : ''; ?>">
                                    <label for="form_control_1">Client Name</label>                                               
                                </div>
                            </div>

                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>




