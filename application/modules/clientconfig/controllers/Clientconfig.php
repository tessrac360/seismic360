<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clientconfig extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user, 'company_id' => $this->getCompanyId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user, 'company_id' => $this->getCompanyId);
        $this->load->model("Model_clientconfig");
        $this->load->library('form_validation');
    }



    public function index() {
        ///helper_getActiveClient();
        $roleAccess = helper_fetchPermission('19', 'view');
        if ($roleAccess == 'Y') {
            $this->getClient();
        } else {
            redirect('unauthorized');
        }
    }

    public function getClient() {
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'client/getclient/';
        $config['total_rows'] = count($this->Model_client->getClientAssignToUserPagination());
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['client'] = $this->Model_client->getClientAssignToUserPagination($config['per_page'], $page);
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }
    
    public function create() {
        $roleAccess = helper_fetchPermission('51', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('client_title', 'Client Name', 'required');
               $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $clientPost = $this->input->post();
                    $clientPost = array_merge($clientPost, $this->cData);
                    $resultData = $this->Model_client->insertClient($clientPost);
                    if ($resultData['status'] == 'true') {
                        $lastInsertId = $resultData['lastId'];
                        $this->session->set_flashdata("success_msg", "Client is created successfully ..!!");
                        redirect('client');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('client/create');
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['pageTitle'] = 'Your p44age title';
                    $data['file'] = 'create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['user_type'] = $this->user_type;
                $data['pageTitle'] = 'Your p44age title';
                $data['file'] = 'create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($postId = NULL) {
        $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId);
            $getStatus = $this->Model_client->isExitCleint($postId);
            //$getStatus['status'] = 'true';
            if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
                    $postClient = $this->input->post();
                    $this->form_validation->set_rules('client_title', 'Client Name', 'required');
                   $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
                        //unset($postUsers['id']);
                        $postClient = array_merge($postClient, $this->uData); //pr($postUsers);exit;
                        $updateStatus = $this->Model_client->updateClient($postClient, $postId);
                        if ($updateStatus['status'] == 'true') {

                            $this->session->set_flashdata("success_msg", "Client is Updated successfully ..!!");
                            redirect('client');
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/edit');
                        }
                    } else {
                        $data['user_type'] = $this->user_type;
                        $data['getClient'] = $this->Model_client->getClientEdit($postId);
                        $data['pageTitle'] = 'Your p44age title';
                        $data['file'] = 'update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['getClient'] = $this->Model_client->getClientEdit($postId);
                    $data['pageTitle'] = 'Your p44age title';
                    $data['file'] = 'update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('/users');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function delete() {
        $data['file'] = 'delete_form';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Model_client->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'User Account is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'User Account is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

}
