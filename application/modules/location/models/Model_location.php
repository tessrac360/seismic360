<?php

class Model_location extends CI_Model {

  private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'temp_countries';
        $this->tablename_countries = 'countries';
        $this->tablename_cirlce = 'circles';
        $this->tablename_states = 'temp_states';
        $this->tablename_states_actual = 'states';
        $this->tablename_zones = 'zones';
        $this->company_id = $this->session->userdata('company_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }
    
    
	// Country Model

    public function isExitCountry($id) {
        $this->db->select('id');
        $this->db->from($this->tablename);
        $this->db->where('id', $id);
        if ($this->company_id != 0) {
            $this->db->where('company_id', $this->getCompanyId);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function insertCountry($postData = array()) {
		echo $this->tablename;
        $this->db->insert($this->tablename, $postData);		
        $insert_id = $this->db->insert_id();
		$params['CO']=$insert_id;
		$code=$this->code_generate('CO',$params);
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;            
        } else {
            $return['status'] = 'false';
        }
		$this->db->set('code',$code);
        $this->db->where('id',$insert_id);
        $this->db->update($this->tablename);
		//echo $this->db->last_query();exit;
        if ($insert_id) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
      
        return $return;
    }

    public function updateCountry($postData = array(), $id) {
        $this->db->where('id', $id);
        $update_status = $this->db->update($this->tablename, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
       
        return $return;
    }

    public function getCountryPagination($limit = "", $start = "") {
       $this->db->where('company_id', $this->getCompanyId);
       $this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get($this->tablename);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

    /*
     * Objective: Pagination of Role moudle with Searching
     * parameters: Null
     * Return: Null
     */

    public function getCountrySearchData($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {        
        if ($search != "") {
            $this->db->or_like('name', $search);         
        }
        $this->db->where('company_id', $this->getCompanyId);
        $this->db->order_by($order_by, $order_type);
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get($this->tablename);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

    public function getCountryEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $this->db->where('id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
	
	
	public function getCountryListPagination($limit = "", $start = "") {
       $this->db->where('company_id', $this->getCompanyId);
       $this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get($this->tablename_countries);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

    /*
     * Objective: Pagination of Role moudle with Searching
     * parameters: Null
     * Return: Null
     */

    public function getCountryListSearchData($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {        
        if ($search != "") {
            $this->db->or_like('name', $search);         
        }
        $this->db->where('company_id', $this->getCompanyId);
        $this->db->order_by($order_by, $order_type);
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get($this->tablename_countries);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

    	
    public function getCountryofcompany($company_id) {
       $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->tablename_countries);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }		
	public function countriesMigration($country_id,$company_id) {
		$this->db->where('id', $country_id);
		$this->db->order_by('id', "asc");
		$query = $this->db->get($this->tablename);
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$row->company_id = $company_id;
				$this->db->insert($this->tablename_countries,$row);
			}	
		} else {

		}
		$this->db->where('country_id', $country_id);
		$this->db->order_by('id', "asc");
		$query = $this->db->get($this->tablename_states);
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$row->company_id = $company_id;  
				$this->db->insert($this->tablename_states_actual,$row);
			}	
		} else {

		}	
	}	

    public function countriesMigrationEdit($country_id,$company_id) {

		$this->db->where('company_id', $company_id);
		$this->db->delete($this->tablename_states_actual);

		$this->db->where('company_id', $company_id);
		$this->db->delete($this->tablename_countries); 

		$this->db->where('id', $country_id);
		$this->db->order_by('id', "asc");
		$query = $this->db->get($this->tablename);
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$row->company_id = $company_id;
				$this->db->insert($this->tablename_countries,$row);
			}	
		} else {

		}
		$this->db->where('country_id', $country_id);
		$this->db->order_by('id', "asc");
		$query = $this->db->get($this->tablename_states);
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$row->company_id = $company_id;  
				$this->db->insert($this->tablename_states_actual,$row);
			}	
		} else {

		}	
    }

	//State Model
	
	 public function isExitState($id) {
        $this->db->select('id');
        $this->db->from($this->tablename_states);
        $this->db->where('id', $id);
        if ($this->company_id != 0) {
            $this->db->where('company_id', $this->getCompanyId);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function insertState($postData = array()) {
		echo $this->tablename_states;
        $this->db->insert($this->tablename_states, $postData);		
        $insert_id = $this->db->insert_id();
		$params['CO']=$postData['country_id'];
		$params['ST']=$insert_id;
		$code=$this->code_generate('CO',$params);
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;            
        } else {
            $return['status'] = 'false';
        }
		$this->db->set('code',$code);
        $this->db->where('id',$insert_id);
        $this->db->update($this->tablename_states);
		//echo $this->db->last_query();exit;
        if ($insert_id) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
      
        return $return;
    }
	
	 public function updateState($postData = array(), $id) {
		$params['CO']=$postData['country_id'];
		$params['ST']=$id;
		$code=$this->code_generate('CO',$params);	
		$postData['code'] =$code;
        $this->db->where('id', $id);
        $update_status = $this->db->update($this->tablename_states, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
       
        return $return;
    }
	
	public function getStatePagination($limit = "", $start = "") {
		$this->db->select($this->tablename_states.'.*,'.$this->tablename.'.name as country_name');
		$this->db->from($this->tablename_states);
		$this->db->join($this->tablename, $this->tablename.'.id = '.$this->tablename_states.'.country_id');
		$this->db->where(array($this->tablename_states.'.company_id' => $this->getCompanyId));

		
		
		if ($start >= 0 and $limit > 0) {
		   $this->db->limit($limit, $start);
		}	
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$data = $query->result();
		} else {
			$data = array();
		}
		return $data;
	}

	public function getStateSearchData($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {

		$this->db->select($this->tablename_states.'.*,'.$this->tablename.'.name as country_name');
		$this->db->from($this->tablename_states);
		$this->db->join($this->tablename, $this->tablename.'.id = '.$this->tablename_states.'.country_id');
        if ($search != "") {
            $this->db->or_like($this->tablename_states.'.name', $search);         
        }		
		$this->db->where(array($this->tablename_states.'.company_id' => $this->getCompanyId));
        $this->db->order_by($this->tablename_states.'.'.$order_by, $this->tablename_states.'.'.$order_type);
				
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

	public function getStateEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->tablename_states);
        $this->db->where('id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
	
	
	public function getStateListPagination($limit = "", $start = "") {
		$this->db->select($this->tablename_states_actual.'.*,'.$this->tablename_countries.'.name as country_name');
		$this->db->from($this->tablename_states_actual);
		$this->db->join($this->tablename_countries, $this->tablename_countries.'.id = '.$this->tablename_states_actual.'.country_id');
		$this->db->where(array($this->tablename_states_actual.'.company_id' => $this->getCompanyId));
		if ($start >= 0 and $limit > 0) {
			$this->db->limit($limit, $start);
		}	
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$data = $query->result();
		} else {
			$data = array();
		}
		return $data;
    }

    /*
     * Objective: Pagination of Role moudle with Searching
     * parameters: Null
     * Return: Null
     */

    public function getStateListSearchData($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {        
        $this->db->select($this->tablename_states_actual.'.*,'.$this->tablename_countries.'.name as country_name');
		$this->db->from($this->tablename_states_actual);
		$this->db->join($this->tablename_countries, $this->tablename_countries.'.id = '.$this->tablename_states_actual.'.country_id');
        if ($search != "") {
            $this->db->or_like($this->tablename_states_actual.'.name', $search);         
        }		
		$this->db->where(array($this->tablename_states_actual.'.company_id' => $this->getCompanyId));
        $this->db->order_by($this->tablename_states_actual.'.'.$order_by, $this->tablename_states_actual.'.'.$order_type);
				
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

	
	public function updateStatus($status = NULL, $id = NULL, $table = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            $this->db->where('id', $id);
            $this->db->update($table, $data);
			//echo $this->db->last_query();exit;
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	
	public function code_generate($title,$params) {
        $str= $title."#".implode("-",$params);
        return $str;
    }
	

}

?>