<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Location extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user, 'company_id' => $this->getCompanyId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user, 'company_id' => $this->getCompanyId);
		$this->load->model("Model_location");
        $this->load->library('form_validation');
		//$this->output->enable_profiler();
    }

    /*
     * Objective: Start Cournty module 
     */

    /*
     * Objective: Listing cournty
     * parameters: NULL
     * Return: Null
     * UserID: 1530
     * Created On: 17/05/17
     */

    public function country() {
        //$this->getUser();
        $roleAccess = helper_fetchPermission('17', 'view');
        if ($roleAccess == 'Y') {
			$this->load->library('pagination');
			$config['base_url'] = base_url() . 'location/country/';
			$config['total_rows'] = count($this->Model_location->getCountryPagination());
			$config['per_page'] = PAGINATION_PERPAGE;
			$config['uri_segment'] = 3;
			$config['display_pages'] = TRUE;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['country'] = $this->Model_location->getCountryPagination($config['per_page'], $page);
			$data['hiddenURL'] = 'location/searchCountry';
			$data['search'] = '';
			$data['file'] = 'country/view_form';
			$this->load->view('template/front_template', $data);		
        } else {
            redirect('unauthorized');
        }		

    }
	
	public function countrylist() {
        //$this->getUser();
        $roleAccess = helper_fetchPermission('19', 'view');
        if ($roleAccess == 'Y') {
			$this->load->library('pagination');
			$config['base_url'] = base_url() . 'location/countrylist/';
			$config['total_rows'] = count($this->Model_location->getCountryListPagination());
			$config['per_page'] = PAGINATION_PERPAGE;
			$config['uri_segment'] = 3;
			$config['display_pages'] = TRUE;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['countrylist'] = $this->Model_location->getCountryListPagination($config['per_page'], $page);
			$data['hiddenURL'] = 'location/searchCountryList';
			$data['search'] = '';
			$data['file'] = 'country/country_list_form';
			$this->load->view('template/front_template', $data);		
        } else {
            redirect('unauthorized');
        }		
    }

    /*
     * Objective: Add cournty
     * parameters: Null
     * Return: Null
     * UserID: 1530
     * Created On: 17/05/17
     */

    public function addcountry() {
		
        $roleAccess = helper_fetchPermission('17', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
				$this->form_validation->set_rules('name', 'Country Name', 'required');
				$this->form_validation->set_rules('short_name', 'Short Name', 'required');
               $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $countryPost = $this->input->post();
                    $countryPost = array_merge($countryPost, $this->cData);
					echo "<pre>";
					print_r($countryPost);
					//die();
                    $resultData = $this->Model_location->insertCountry($countryPost);
                    if ($resultData['status'] == 'true') {
                        $lastInsertId = $resultData['lastId'];
                        $this->session->set_flashdata("success_msg", "Country is created successfully ..!!");
                        redirect('location/country');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('location/addcountry');
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['pageTitle'] = 'Your page title';
                    $data['file'] = 'country/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['user_type'] = $this->user_type;
                $data['pageTitle'] = 'Your page title';
                $data['file'] = 'country/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }		

    }

    /*
     * Objective: Edit cournty
     * parameters: $postId
     * Return: Null
     * UserID: 1530
     * Created On: 17/05/17
     */

    public function editcountry($postId = NULL) {
        $roleAccess = helper_fetchPermission('17', 'view');
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId);
            $getStatus = $this->Model_location->isExitCountry($postId);
            //$getStatus['status'] = 'true';
            if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
                    $postCountry = $this->input->post();
					$this->form_validation->set_rules('name', 'Country Name', 'required');
					$this->form_validation->set_rules('short_name', 'Short Name', 'required');
				   $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {

                        //unset($postUsers['id']);
                        $postCountry = array_merge($postCountry, $this->uData); //pr($postUsers);exit;
                        $updateStatus = $this->Model_location->updateCountry($postCountry, $postId);
                        if ($updateStatus['status'] == 'true') {

                            $this->session->set_flashdata("success_msg", "Country is Updated successfully ..!!");
                            redirect('location/country');
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('location/editcountry');
                        }
                    } else {
                        $data['user_type'] = $this->user_type;
                        $data['getCountry'] = $this->Model_location->getCountryEdit($postId);                       
                        $data['pageTitle'] = 'Your page title';
                        $data['file'] = 'country/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['getCountry'] = $this->Model_location->getCountryEdit($postId);
                    $data['pageTitle'] = 'Your page title';
                    $data['file'] = 'country/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				//echo "test";
				//exit;
                redirect('/location/country');
            }
        } else {
            redirect('unauthorized');
        }
    }
	
	 /*
     * Objective: Search country
     * parameters: $postId
     * Return: Null
     * UserID: 1530
     * Created On: 17/05/17
     */

    public function searchCountry() {
        $search = $this->input->get_post('sd');
        if ($search == "") {
            redirect('location/country/');
        }
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'location/country/searchCountry/';
        $config['total_rows'] = count($this->Model_location->getCountrySearchData($search));
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $config['first_url'] = base_url() . "location/country/searchCountry/0?sd=" . $search;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['country'] = $this->Model_location->getCountrySearchData($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'location/country/searchCountry/';
        $data['file'] = 'country/view_form';
        $this->load->view('template/front_template', $data);
    }	
	
	public function searchCountryList() {
        $search = $this->input->get_post('sd');
        if ($search == "") {
            redirect('location/countrylist/');
        }
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'location/countrylist/searchCountryList/';
        $config['total_rows'] = count($this->Model_location->getCountryListSearchData($search));
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $config['first_url'] = base_url() . "location/countrylist/searchCountryList/0?sd=" . $search;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['countrylist'] = $this->Model_location->getCountryListSearchData($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'location/countrylist/searchCountryList/';
        $data['file'] = 'country/country_list_form';
        $this->load->view('template/front_template', $data);
    }	
	
	
    /*
     * Objective: End Cournty module 
     */

	 
    /*
     * Objective: Start State module 
     */

    public function state() {
        //$this->getUser();
        $roleAccess = helper_fetchPermission('18', 'view');
        if ($roleAccess == 'Y') {
			$this->load->library('pagination');
			$config['base_url'] = base_url() . 'location/state/';
			$config['total_rows'] = count($this->Model_location->getStatePagination());
			$config['per_page'] = PAGINATION_PERPAGE;
			$config['uri_segment'] = 3;
			$config['display_pages'] = TRUE;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['states'] = $this->Model_location->getStatePagination($config['per_page'], $page);
			$data['hiddenURL'] = 'location/searchState';
			$data['search'] = '';
			$data['file'] = 'state/view_form';
			$this->load->view('template/front_template', $data);		
        } else {
            redirect('unauthorized');
        }		
    }
	
	public function statelist() {
        //$this->getUser();
        $roleAccess = helper_fetchPermission('19', 'view');
        if ($roleAccess == 'Y') {
			$this->load->library('pagination');
			$config['base_url'] = base_url() . 'location/statelist/';
			$config['total_rows'] = count($this->Model_location->getStateListPagination());
			$config['per_page'] = PAGINATION_PERPAGE;
			$config['uri_segment'] = 3;
			$config['display_pages'] = TRUE;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['statelist'] = $this->Model_location->getStateListPagination($config['per_page'], $page);
			$data['hiddenURL'] = 'location/searchStateList';
			$data['search'] = '';
			$data['file'] = 'state/state_list_form';
			$this->load->view('template/front_template', $data);		
        } else {
            redirect('unauthorized');
        }		
    }
	
	/*
     * Objective: Add state
     * parameters: Null
     * Return: Null
     * UserID: 1320
     * Created On: 18/05/17
     */

    public function addstate() {

        $roleAccess = helper_fetchPermission('18', 'add');
        if ($roleAccess == 'Y') {
			$data['countries'] = $this->Model_location->getCountryPagination();
            if ($this->input->post()) {
				$this->form_validation->set_rules('country_id', 'Country', 'required');
				$this->form_validation->set_rules('name', 'State Name', 'required');
               $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $countryPost = $this->input->post();
                    $countryPost = array_merge($countryPost, $this->cData);
					echo "<pre>";
					print_r($countryPost);
					//die();
                    $resultData = $this->Model_location->insertState($countryPost);
                    if ($resultData['status'] == 'true') {
                        $lastInsertId = $resultData['lastId'];
                        $this->session->set_flashdata("success_msg", "State is created successfully ..!!");
                        redirect('location/state');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('location/state');
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['pageTitle'] = 'Your page title';
                    $data['file'] = 'state/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['user_type'] = $this->user_type;
                $data['pageTitle'] = 'Your page title';
                $data['file'] = 'state/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }		

    }

	
	
    /*
     * Objective: Edit state
     * parameters: $postId
     * Return: Null
     * UserID: 1320
     * Created On: 18/05/17
     */
    public function editstate($postId = NULL) {
        $roleAccess = helper_fetchPermission('18', 'view');
        if ($roleAccess == 'Y') {
			$data['countries'] = $this->Model_location->getCountryPagination();
            $postId = decode_url($postId);
            $getStatus = $this->Model_location->isExitState($postId);
            //$getStatus['status'] = 'true';
            if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
                    $postCountry = $this->input->post();
					$this->form_validation->set_rules('country_id', 'Country', 'required');
					$this->form_validation->set_rules('name', 'State Name', 'required');
				   $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {

                        //unset($postUsers['id']);
                        $postCountry = array_merge($postCountry, $this->uData); //pr($postUsers);exit;
                        $updateStatus = $this->Model_location->updateState($postCountry, $postId);
                        if ($updateStatus['status'] == 'true') {

                            $this->session->set_flashdata("success_msg", "State is Updated successfully ..!!");
                            redirect('location/state');
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('location/editstate');
                        }
                    } else {
                        $data['user_type'] = $this->user_type;
                        $data['getState'] = $this->Model_location->getStateEdit($postId);                       
                        $data['pageTitle'] = 'Your page title';
                        $data['file'] = 'state/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['getState'] = $this->Model_location->getStateEdit($postId);
                    $data['pageTitle'] = 'Your page title';
                    $data['file'] = 'state/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				//echo "test";
				//exit;
                redirect('/location/state');
            }
        } else {
            redirect('unauthorized');
        }
    }

	 /*
     * Objective: Search state
     * parameters: Null
     * Return: Null
     * UserID: 1320
     * Created On: 18/05/17
     */
	 
	 
	public function searchState() {
        $search = $this->input->get_post('sd');
        if ($search == "") {
            redirect('location/state/');
        }
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'location/state/searchState/';
        $config['total_rows'] = count($this->Model_location->getStateSearchData($search));
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $config['first_url'] = base_url() . "location/state/searchState/0?sd=" . $search;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['states'] = $this->Model_location->getStateSearchData($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'location/state/searchState/';
        $data['file'] = 'state/view_form';
        $this->load->view('template/front_template', $data);
    }	
	
	
	public function searchStateList() {
        $search = $this->input->get_post('sd');
        if ($search == "") {
            redirect('location/statelist/');
        }
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'location/statelist/searchStateList/';
        $config['total_rows'] = count($this->Model_location->getStateListSearchData($search));
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $config['first_url'] = base_url() . "location/statelist/searchStateList/0?sd=" . $search;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['statelist'] = $this->Model_location->getStateListSearchData($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'location/statelist/searchStateList/';
        $data['file'] = 'state/state_list_form';
        $this->load->view('template/front_template', $data);
    }	
	
	
	public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
		$table = decode_url($_POST['table']);
        if ($id != "") {
            $Status = $this->Model_location->updateStatus($status, $id, $table);
            if ($Status['status'] == 'true') {
				if($table =='temp_countries'){
					if ($status == 'true') {
						
						$return['message'] = 'Country is activated successfully';
					} else if ($status == 'false') {
						$return['message'] = 'Country is Deactivated successfully';
					}					
				}elseif($table =='temp_states'){
					if ($status == 'true') {
						
						$return['message'] = 'State is activated successfully';
					} else if ($status == 'false') {
						$return['message'] = 'State is Deactivated successfully';
					}					
				}
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

}
