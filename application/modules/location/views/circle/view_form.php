<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
		<i class="icon-home"></i>
		<a href="<?php echo base_url() ?>">Home</a>
		<i class="fa fa-angle-right"></i>
		</li>
		<li>
		<a href="#">Managements</a>
		<i class="fa fa-angle-right"></i>
		</li>
		<li>
		<a href="#">Client Onboard</a>
		<i class="fa fa-angle-right"></i>
		</li>
		<li>
		<a href="#">Circles</a>			   
		</li>
	</ul>

</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Circles</span>
                </div>                                  
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('location/addcircle') ?>"> Add New Circle
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">

                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search RoleName" id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="role">
                    <thead>
                        <tr>
                            <th> S.No </th>
                            <th> Circle Name </th>
							<th> Country Name</th>
                            <th> Short Name </th>
                            <th> Description </th>
                            <th> Status </th>
                            <th> Action </th>  
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($circles)) {
							$i=0;
                            foreach ($circles as $value) {
							$i++;
                                ?>
                                <tr>
									<td> <?php echo $i ; ?> </td>
                                    <td> <?php echo $value->circle_name ; ?> </td>
									<td> <?php echo $value->name ; ?> </td>
                                    <td> <?php echo $value->short_name; ?>  </td>
                                    <td> <?php echo $value->description; ?>  </td>
                                    <td> 
                                        <div class="islogin_<?php echo $value->id ?>">
                                            <?php if ($value->status == 'Y') { ?>
                                                	Active
                                            <?php } else { ?>                                            
													Inactive
                                            <?php } ?> 
                                        </div>
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url() . 'location/editcircle/'.encode_url($value->id); ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0);" class="btn btn-xs  red">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr> 
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="4"> No Record Found </td>
                            </tr> 
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>



