
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Countries</span>
                </div> 
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-cog"></i>
							<a href="<?php echo base_url() ?>">Managements</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Click Onboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Countries</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">List</a>
				   		</li>
					</ul>
				</div>				
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('location/addcountry') ?>"> Add New Country
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search Country Name" id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-condensed table-bordered table-hover fixedheader" id="role">
                    <thead>
                        <tr>
                            <th> Country Name </th>
                            <th> Short Name </th>
                            <th> Serial Key </th>
                            <th> Status </th>
                            <th> Action </th>  
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($country)) {
							foreach ($country as $value) {
							    ?>
                                <tr>
									<td> <?php echo $value->name ; ?> </td>
                                    <td> <?php echo $value->short_name; ?>  </td>
                                    <td> <?php echo $value->code; ?>  </td>
                                    <td>
                                        <input type="checkbox" <?php if ($value->status == 'Y') {
											echo 'checked';
										} ?>  id="onoffactiontoggle" class="onoffactiontoggle" myval="<?php echo encode_url($value->id); ?>" mytable="<?php echo encode_url('temp_countries'); ?>">
                                    </td>
									<td>
                                        <a href="<?php echo base_url() . 'location/editcountry/'.encode_url($value->id); ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr> 
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="4"> No Record Found </td>
                            </tr> 
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_location.js" type="text/javascript"></script>


