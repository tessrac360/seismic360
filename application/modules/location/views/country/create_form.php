
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Create Country</span>
                </div>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-cog"></i>
							<a href="<?php echo base_url() ?>">Managements</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Click Onboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Countries</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Form</a>
						</li>
					</ul>
				</div>
            </div>
            <div class="portlet-body form">
                <form role="form" name="frmLocation" id="frmLocation" method="post" action="">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="country_name" name="name" type="text">
                                    <label for="form_control_1">Country Name<span class="required" aria-required="true">*</span></label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="country_short_name" name="short_name" type="text" onkeyup="this.value = this.value.replace(/[^A-Z/g, '')" minlength = "3" maxlength = "3">
                                    <label for="form_control_1">Short Name<span class="required"  aria-required="true">*</span></label>                                               
                                </div>
                            </div>
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('location/country'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_location.js" type="text/javascript"></script>




