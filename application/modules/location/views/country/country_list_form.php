
<div class="tabbable-line">
	<ul class="nav nav-tabs ">
		<?php
		helper_LocationTab();
		?>
	</ul>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Countries</span>
                </div> 
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-cog"></i>
							<a href="<?php echo base_url() ?>">Managements</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Click Onboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Countries</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">List</a>
				   		</li>
					</ul>
				</div>				
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
						<div class="col-md-9">
                            <div class="btn-group">
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search Country Name" id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="role">
                    <thead>
                        <tr>
                            <th> Country Name </th>
                            <th> Short Name </th>
                            <th> Serial Key </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($countrylist)) {
							foreach ($countrylist as $value) {
							    ?>
                                <tr>
									<td> <?php echo $value->name ; ?> </td>
                                    <td> <?php echo $value->short_name; ?> </td>
                                    <td> <?php echo $value->code; ?>  </td>
                                </tr> 
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="4"> No Record Found </td>
                            </tr> 
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_location.js" type="text/javascript"></script>


