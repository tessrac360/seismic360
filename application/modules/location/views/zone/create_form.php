<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <span>Create Zone</span>
        </li>
    </ul>

</div>
<!-- END PAGE HEADER-->

<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Create Zone</span>
                </div>
            </div>
            <div class="portlet-body form">

                <form role="form" name="frmLocation" id="frmLocation" method="post" action="">
                    <div class="form-body">
                        <div class="row">
						
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control edited" name="country_id" id="country_id">
                                        <option value=""></option>
                                        <?php
                                            foreach ($countries as $value) {
                                                ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>  
                                                <?php
                                            }                                        
                                        ?>                                      
                                    </select>
                                    <label for="form_control_1">Country<span class="required" aria-required="true">*</span></label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control edited" name="state_id" id="state_id">
                                        <option value=""></option>                                    
                                    </select>
                                    <label for="form_control_1">State<span class="required" aria-required="true">*</span></label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="zone_name" name="name" type="text">
                                    <label for="form_control_1">Zone Name<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>



                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                                    <label for="form_control_1">Description</label>                                               
                                </div>
                            </div>
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('location/zone'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_location.js" type="text/javascript"></script>