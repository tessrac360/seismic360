
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Create State</span>
                </div>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-cog"></i>
							<a href="<?php echo base_url() ?>">Managements</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Click Onboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">States</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Form</a>
						</li>
					</ul>
				</div>
            </div>
            <div class="portlet-body form">
                <form role="form" name="frmLocation" id="frmLocation" method="post" action="">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                               <div class="form-group form-md-line-input form-md-floating-label">
                                   <select class="form-control edited" name="country_id" id="country_id">
                                       <option value=""></option>
                                       <?php
                                           foreach ($countries as $value) {
                                               ?>
                                               <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>  
                                               <?php
                                           }                                        
                                       ?>                                      
                                   </select>
                                   <label for="form_control_1">Country<span class="required" aria-required="true">*</span></label>
                               </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="state_name" name="name" type="text">
                                    <label for="form_control_1">State Name<span class="required"  aria-required="true">*</span></label>                                               
                                </div>
                            </div>
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('location/state'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_location.js" type="text/javascript"></script>




