<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gateways extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
		//pr($this->session->userdata());exit;
        $this->getCompanyId = helpler_getCompanyId();
		$this->getDomainId = helper_getUserDefaultGroup();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_gateways");
        $this->load->model('client/Model_client');	
        $this->load->library('form_validation');
    }

    public function index($client_id = NULL) {
		//$client_id  = decode_url($client_id);
		//echo $client_id;exit;
		$client_page = true;
		if(is_numeric($client_id)){
		   	$client_page = false;
		}elseif($client_id == NULL){
			$client_page = false;
		}
		if($client_page){
			$client_id  = decode_url($client_id);
			$client_ids[] = $client_id;
			$clientRes = $this->Model_gateways->getClientName($client_id);
			$client_titles[$client_id] =$clientRes['resultSet']['client_title'];
			
		}else{
			$data['clients'] = $this->Model_client->getClientsgat($this->client_id);		
			$client_ids =array();
			$client_titles =array();
			for($i=0;$i<count($data['clients']);$i++){
				$client_ids[]= $data['clients'][$i]['id'];
				$client_titles[$data['clients'][$i]['id']]= $data['clients'][$i]['client_title'];
			}			
		}
		//pr($client_ids);exit;		
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		if($client_page){
			$config['base_url'] = base_url() . 'gateways/index/'.encode_url($client_id );
		}else{
			$config['base_url'] = base_url() . 'gateways/index/';
		}
        $config['total_rows'] = count($this->Model_gateways->getGatewaySearchData($search,$client_ids ));
		if($client_page){
			$config['uri_segment'] = 4;
		}else{
			$config['uri_segment'] = 3;
		}		
       
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		//$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}
        $this->pagination->initialize($config);
		if($client_page){
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		}else{
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		}		
        

        $data['gateway'] = $this->Model_gateways->getGatewayPagination($search, $client_ids, $config['per_page'], $page);
		//pr($data['gateway']);exit;
        //$data['client_id'] = $client_id;
		if($client_page){
			$data['hiddenURL'] = 'gateways/index/'.encode_url($client_id );
		}else{
			$data['hiddenURL'] = 'gateways/index/';
		}		
        if($client_page){
			$data['clientName'] = $this->Model_gateways->getClientName($client_id);
			$data['client_page'] = true;
			$data['client_id'] = $client_id;
			$data['client_title'] = $data['clientName']['resultSet']['client_title'];
			$data['client_type'] = $data['clientName']['resultSet']['client_type'];
			
		}else{
			$data['client_page'] = false;
		}
		$data['clientTitles'] = $client_titles;
		//pr($data['clientTitles']);exit;
        $data['file'] = 'view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }

	public function searchdata($client_id = NULL) {
        
		$data['clients'] = $this->Model_client->getClients($this->client_id);		
		$client_ids =array();
		$client_titles =array();
		for($i=0;$i<count($data['clients']);$i++){
			$client_ids[]= $data['clients'][$i]['id'];
			$client_titles[$data['clients'][$i]['id']]= $data['clients'][$i]['client_title'];
		}
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'gateways/searchData/';
        $config['total_rows'] = count($this->Model_gateways->getGatewaySearchData($search,$client_ids ));
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		//$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['gateway'] = $this->Model_gateways->getGatewaySearchData($search, $client_ids , $config['per_page'], $page);
		$data['clientTitles'] = $client_titles;
        $data['search'] = $search;
        $data['hiddenURL'] = 'gateways/searchData/';
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }
    public function getGateway($client_id = NULL) {
		$client_id  = decode_url($client_id);
		//echo $client_id;exit;
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'client/gateway/getgateway/'.encode_url($client_id );
        $config['total_rows'] = $this->Model_gateways->getGatewayPagination($search, $client_id);
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['uri_segment'] = 5;
        $config['display_pages'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

        $data['gateway'] = $this->Model_gateways->getGatewayPagination($search, $client_id, $config['per_page'], $page);
		$data['clientName'] = $this->Model_gateways->getClientName($client_id);
		//pr($data['gateway']);exit;
        $data['client_id'] = $client_id;
        $data['hiddenURL'] = 'client/gateway/searchdata/'.encode_url($client_id );
        $data['file'] = 'gateway/view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }


    public function getClientLocations() {
		$client_id = $_POST['client_id'];
		$resultGatewayTypes = $this->Model_gateways->getClientLocations($client_id);
		echo '<option value=""></option>';
		if ($resultGatewayTypes['status'] == 'true') {
			foreach($resultGatewayTypes['resultSet'] as $value){
				echo '<option value="'.$value['location_uuid'].'">'.$value['location_name'].'</option>';
			}

		}
	}
    public function create($client_id = NULL) {
		$client_page = false;
		if($client_id != NULL){
			$client_id = decode_url($client_id);
			$rescli = $this->Model_gateways->getClientName($client_id);			
			$client_uuid = $rescli['resultSet']['client_uuid'];											
			$client_page = true;
		}		
		//pr($client_id);exit;
	   $resultGatewayTypes = $this->Model_gateways->getGatewayTypes();
	   $data['gatewayTypes'] = $resultGatewayTypes;
   
	   //pr($resultGatewayTypes);exit;
        //$level = (isset($_GET['lvl'])) ? decode_url($_GET['lvl']) : 1;
       $roleAccess = helper_fetchPermission('62', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
				if(!$client_page){
					$this->form_validation->set_rules('client_id', 'Client', 'required');
				}
                $this->form_validation->set_rules('title', 'Gateway Name', 'required');
                $this->form_validation->set_rules('ip_address', 'IP Address', 'required|is_unique[client_gateways.ip_address]');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $gatewayPost = $this->input->post();
					
					if($client_page){
						$gatewayPost['client_id'] = $client_id;
						$gatewayPost['client_uuid'] = $client_uuid;											
					}else{
						$client_ids  = explode("#",$gatewayPost['client_id']);
						$gatewayPost['client_id'] = $client_ids[0];
						$gatewayPost['client_uuid'] = $client_ids[1];
					}
					//pr($gatewayPost);exit;
					
					
					$gatewayPost['gateway_uuid'] = generate_uuid('eecg_');					
					$gatewayPost['private_key'] = createSecretKey($gatewayPost['gateway_uuid']);
					//pr($gatewayPost);exit;
                    $gatewayPost = array_merge($gatewayPost, $this->cData);
					$accept_email = array();
					$reject_email = array();
					if(isset($gatewayPost['accept_email']) && $gatewayPost['accept_email'][0] !=""){
						$accept_email = $gatewayPost['accept_email'];
					}
					if(isset($gatewayPost['reject_email']) && $gatewayPost['reject_email'][0] !=""){
						$reject_email = $gatewayPost['reject_email'];
					}	

					unset($gatewayPost['accept_email']);
					unset($gatewayPost['reject_email']);

						
                    $resultData = $this->Model_gateways->insertGateway($gatewayPost);
					
					
					//pr($resultData);exit;
	                if (!empty($resultData)) {
						$gateway_id = $resultData['lastId'];
												
						$gateway_accept_list = array();
						if($accept_email[0] !=""){
							foreach($accept_email as $email_id){
								$uuid = generate_uuid('gacl_');
								$gateway_accept_list[] = array('uuid'=>$uuid ,'email_id'=>$email_id, 'accept_type'=>'0','gateway_id'=>$gateway_id,'gateway_uuid'=>$gatewayPost['gateway_uuid']);
							}
							
						}
						if($reject_email[0] !=""){
							foreach($reject_email as $email_id){
								$uuid = generate_uuid('gacl_');
								$gateway_accept_list[] = array('uuid'=>$uuid ,'email_id'=>$email_id, 'accept_type'=>'1','gateway_id'=>$gateway_id,'gateway_uuid'=>$gatewayPost['gateway_uuid']);
							}
							
						}
						if(count($gateway_accept_list)>0){
							 $resultDataAcceptList = $this->Model_gateways->insertGatewayAcceptList($gateway_accept_list);
						}
						
                        $this->session->set_flashdata("success_msg", "Gateway is created successfully ..!!");                        
						if($client_page){
							redirect('gateways/index/'.encode_url($client_id));	
						}else{
							redirect('gateways/');
						}
						
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
						if($client_page){
							redirect('gateways/create/'.encode_url($client_id));	
						}else{
							redirect('gateways/create/');
						}						
                       
                    }
                } else {
					//
					$data['client'] = $this->treeClientDropdown();
					if($client_page){
						$data['clientName'] = $this->Model_gateways->getClientName($client_id);
						if($data['clientName']['status'] == 'true'){
							$data['client_title'] = $data['clientName']['resultSet']['client_title'];
							$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						}
						$data['client_page'] = true;
						$data['client_id'] = $client_id;
						$resultGetClientLocations = $this->Model_gateways->getClientLocations($client_uuid);
						$data['getClientLocations'] = $resultGetClientLocations;						
					}else{
						$data['client_page'] = false;
					}					
                    $data['file'] = 'create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				//$data['clientName'] = $this->Model_gateways->getClientName($client_id);
				$data['client'] = $this->treeClientDropdown();
								
				if($client_page){
					$data['clientName'] = $this->Model_gateways->getClientName($client_id);
					if($data['clientName']['status'] == 'true'){
						$data['client_title'] = $data['clientName']['resultSet']['client_title'];
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					}
					$data['client_page'] = true;
					$data['client_id'] = $client_id;
					$resultGetClientLocations = $this->Model_gateways->getClientLocations($client_uuid );
					$data['getClientLocations'] = $resultGetClientLocations;					
				}else{
					$data['client_page'] = false;
				}				
                $data['file'] = 'create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($client_id = NULL,$postId = NULL) {
		$client_page = true;
		if($postId == NULL){
			$postId = $client_id;
			$client_page = false;
		}		
		if($client_page){
			$client_id = decode_url($client_id);
			$rescli = $this->Model_gateways->getClientName($client_id);			
			$client_uuid = $rescli['resultSet']['client_uuid'];				
		}		
		//$client_id = decode_url($client_id);
        $roleAccess = helper_fetchPermission('62', 'edit');
		//echo $roleAccess;exit;
        if ($roleAccess == 'Y') {
			$resultGatewayTypes = $this->Model_gateways->getGatewayTypes();
			$data['gatewayTypes'] = $resultGatewayTypes;
			//exit;

            $postId = decode_url($postId);
                if ($this->input->post()) {
                    $postGateway = $this->input->post();
					if(!$client_page){
						$this->form_validation->set_rules('client_id', 'Client', 'required');
					}					
                    $this->form_validation->set_rules('title', 'Gateway Name', 'required');
                    $this->form_validation->set_rules('ip_address', 'IP Address', 'required');
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
                        $postGateway = array_merge($postGateway, $this->uData); 
						if($client_page){
										
						}else{
							$client_ids  = explode("#",$postGateway['client_id']);
							$postGateway['client_id'] = $client_ids[0];
							$postGateway['client_uuid'] = $client_ids[1];
						}						
						//pr($postGateway);exit;
                        $updateStatus = $this->Model_gateways->updateGateway($postGateway, $postId);
						//pr($updateStatus);exit;
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "Gateway is Updated successfully ..!!");
							if($client_page){
								redirect('gateways/index/'.encode_url($client_id));	
							}else{
								redirect('gateways/');
							}								
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
							if($client_page){
								redirect('gateways/edit/'.encode_url($client_id)."/".encode_url($postId));	
							}else{
								redirect('gateways/edit/'.encode_url($postId));
							}							
                        }
                    } else {
						//$data['clientName'] = $this->Model_gateways->getClientName($client_id);
                        $data['getGateway'] = $this->Model_gateways->getGatewayEdit($postId);
						//pr($data['getGateway']);exit;
						$resultGetClientLocations = $this->Model_gateways->getClientLocations($data['getGateway']['resultSet']->client_uuid );
						$data['getClientLocations'] = $resultGetClientLocations;
						//pr($data['getClientLocations']);exit;						
						$data['client'] = $this->treeClientDropdown();
						if($client_page){							
							$data['clientName'] = $this->Model_gateways->getClientName($client_id);
							if($data['clientName']['status'] == 'true'){
								$data['client_title'] = $data['clientName']['resultSet']['client_title'];
								$data['client_type'] = $data['clientName']['resultSet']['client_type'];
							}
							$data['client_page'] = true;
							$data['client_id'] = $client_id;
							
						}else{
							$data['client_page'] = false;
						}							
                        $data['file'] = 'update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					//$data['clientName'] = $this->Model_gateways->getClientName($client_id);
                    $data['getGateway'] = $this->Model_gateways->getGatewayEdit($postId);
					//pr($data['getGateway']);exit;
					$resultGetClientLocations = $this->Model_gateways->getClientLocations($data['getGateway']['resultSet']->client_uuid  );
					$data['getClientLocations'] = $resultGetClientLocations;
					//pr($data['getClientLocations']);exit;
					$data['client'] = $this->treeClientDropdown();
					if($client_page){						
						$data['clientName'] = $this->Model_gateways->getClientName($client_id);
						if($data['clientName']['status'] == 'true'){
							$data['client_title'] = $data['clientName']['resultSet']['client_title'];
							$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						}
						$data['client_page'] = true;
						$data['client_id'] = $client_id;
						
					}else{
						$data['client_page'] = false;
					}						
                    $data['file'] = 'update_form';
                    $this->load->view('template/front_template', $data);
                }
            
        } else {
            redirect('unauthorized');
        }
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);


        if ($id != "") {
            $Status = $this->Model_gateways->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Gateway is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Gateway is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }
	
	public function delete($client_id = "",$postId = "") {
		$client_page = false;
		if($postId ==""){
			
			$postId = $client_id;
		}else{
			$client_page = true;
		}
        $postId = decode_url($postId);

        if ($postId != "") {
            $deleteRole = $this->Model_gateways->isDeleteGateway($postId);
			//pr($deleteRole);exit;
            if ($deleteRole['status'] == 'true') {
				
				$this->session->set_flashdata("success_msg", "Gateway is deleted successfully..!!");
				if($client_page){
					redirect('gateways/index/'.$client_id);
				}else{
					redirect('gateways/');	
				}
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('gateways/');
            }
        } else {
            redirect('gateways/');
        }
    }
    public function treeClientDropdown() {
        $parent_client_id = $this->session->userdata('client_id');
        $clients = array();
		if($parent_client_id ==1){
			$resultSet = $this->Model_client->getClientTreeView($parent_client_id);
		}else{
			$resultSet = $this->Model_client->getClientTreeView($parent_client_id,1);
		}
        if (!empty($resultSet)) {
            foreach ($resultSet as $value) {
                //  pr($value);
                $client['id'] = $value['id'];
                $client['client_title'] = $value['client_title'];
                $client['parent_client_id'] = $value['parent_client_id'];
                $client['client_uuid'] = $value['client_uuid'];
                $client['client_type'] = $value['client_type'];
                $client['children'] = $this->getClientChildren($value['id']);
                $clients[] = $client;
            }
            return $clients;
        } else {
            return array();
        }
    }
    public function getClientChildren($parent_id) {
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeView($parent_id);
        foreach ($resultSet as $value) {
            $client['id'] = $value['id'];
            $client['client_title'] = $value['client_title'];
            $client['parent_client_id'] = $value['parent_client_id'];
            $client['client_uuid'] = $value['client_uuid'];
            $client['client_type'] = $value['client_type'];
            $client['children'] = $this->getClientChildren($value['id']);
            $clients[] = $client;
        }
        return $clients;
    }
	public function severities($gateway_id = NULL){
		$gateway_id = decode_url($gateway_id);
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		$config['base_url'] = base_url() . 'gateways/severities/'.encode_url($gateway_id).'/';
        $config['total_rows'] = $this->Model_gateways->getGatwaySeveritiesPagination($gateway_id,$search);
		$config['uri_segment'] = 4;		      
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;      
        $data['severities'] = $this->Model_gateways->getGatwaySeveritiesPagination($gateway_id,$search, $config['per_page'], $page);
		
		
		$data['hiddenURL'] = 'gateways/severities/'.encode_url($gateway_id).'/';
		//pr($data['clientTitles']);exit;
        $data['file'] = 'severities/view_form';
        $data['search'] = $search;
		$data['gateway_id'] = $gateway_id;
        $this->load->view('template/front_template', $data);		
		
	}
    public function severitiesCreate($gateway_id = NULL) {
		$gateway_id = decode_url($gateway_id);
		//echo $gateway_id;exit;
		$res = $this->Model_gateways->getGatewayEdit($gateway_id);	
		$client_id = $res['resultSet']->client_id;
		$roleAccess = helper_fetchPermission('67', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('severity_id', 'Actual Severity', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {

                    $severityPost = $this->input->post();	
					$severityPost['client_id'] = $client_id;
					$severityPost['gateway_id'] = $gateway_id;
					$severityPost['domain_id'] = $this->getDomainId;
					//pr($severityPost['domain_id']);exit;					
					$severity_str = explode("###",$severityPost['severity_id']);
					$severityPost['severity_id'] = $severity_str[0];
					$severityTitle = $severity_str[1];
					if($severityPost['gateway_severity'] == NULL){
						$severityPost['gateway_severity'] = $severityTitle;						
					}
					$resultCheck = $this->Model_gateways->checkUniqueSeverity($severityPost);
					//pr($resultCheck);exit;
					if($resultCheck['status'] == 'true'){
						$this->session->set_flashdata("error_msg", "Severity already exists");
						redirect('gateways/severitiescreate/'.encode_url($gateway_id));
					}else{					
						$severityPost = array_merge($severityPost, $this->cData);
						$resultData = $this->Model_gateways->insertGatewaySeverity($severityPost);
						//pr($resultData);exit;
						if (!empty($resultData)) {
							$this->session->set_flashdata("success_msg", "Gateway Severity is created successfully ..!!");                        
							redirect('gateways/severities/'.encode_url($gateway_id));								
						} else {
							$this->session->set_flashdata("error_msg", "Some thing went wrong");
							redirect('gateways/severitiescreate/'.encode_url($gateway_id));                      
						}					
					}
                } else {					
					$data['severitiesall'] = $this->Model_gateways->getSeverities();
					//pr($data['severitiesall']);exit;
					$data['gateway_id'] = $gateway_id;
                    $data['file'] = 'severities/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {		
                $data['severitiesall'] = $this->Model_gateways->getSeverities();
				//pr($data['severitiesall']);exit;
				$data['gateway_id'] = $gateway_id;
                $data['file'] = 'severities/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function severitiesEdit($gateway_id = NULL,$postId = NULL) {
		$gateway_id = decode_url($gateway_id);
		//$client_id = decode_url($client_id);
        $roleAccess = helper_fetchPermission('67', 'edit');
		//echo $roleAccess;exit;
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId);
			if ($this->input->post()) {
				$postSeverity = $this->input->post();				
				$this->form_validation->set_rules('severity_id', 'Actual Severity', 'required');
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
				if ($this->form_validation->run() == TRUE) {	
					$postSeverity['gateway_id'] = $gateway_id;
					$postSeverity['domain_id'] = $this->getDomainId;
					//pr($severityPost['domain_id']);exit;					
					$severity_str = explode("###",$postSeverity['severity_id']);
					$postSeverity['severity_id'] = $severity_str[0];
					$severityTitle = $severity_str[1];
					if($postSeverity['gateway_severity'] == ""){
						$postSeverity['gateway_severity'] = $severityTitle;						
					}						
					$postSeverity = array_merge($postSeverity, $this->uData); //pr($postUsers);exit;
					$updateStatus = $this->Model_gateways->updateGatewaySeverity($postSeverity, $postId);
					//pr($updateStatus);exit;
					if ($updateStatus['status'] == 'true') {
						$this->session->set_flashdata("success_msg", "Severity is Updated successfully ..!!");
						redirect('gateways/severities/'.encode_url($gateway_id));								
					} else {
						$this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('gateways/severitiesedit/'.encode_url($gateway_id)."/".encode_url($postId));							
					}
				} else {
					$data['getSeverity'] = $this->Model_gateways->getGatewaySeverityEdit($postId);
					$data['severitiesall'] = $this->Model_gateways->getSeverities();
					$data['gateway_id'] = $gateway_id;
					$data['file'] = 'severities/update_form';
					$this->load->view('template/front_template', $data);
				}
			} else {
				$data['getSeverity'] = $this->Model_gateways->getGatewaySeverityEdit($postId);
				$data['severitiesall'] = $this->Model_gateways->getSeverities();
				$data['gateway_id'] = $gateway_id;
				$data['file'] = 'severities/update_form';
				$this->load->view('template/front_template', $data);
			}
            
        } else {
            redirect('unauthorized');
        }
    }	
	public function severityDelete($gateway_id = NULL,$postId = "") {
		$gateway_id =  decode_url($gateway_id);
        $postId = decode_url($postId);

        if ($postId != "") {
            $deleteRole = $this->Model_gateways->isDeleteGatewaySeverity($postId);
			//pr($deleteRole);exit;
            if ($deleteRole['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "Severity is deleted successfully..!!");
                redirect('gateways/severities/'.encode_url($gateway_id));
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('gateways/severities/'.encode_url($gateway_id));
            }
        } else {
            redirect('severities/');
        }
    }
    public function ajax_changeSeverityStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Model_gateways->updateSeverityStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Severity is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Severity is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }	
	public function ajax_locations_list() {
		$client_uuid = $_POST['client_uuid'];
		$resultGetClientLocations = $this->Model_gateways->getClientLocations($client_uuid);
		echo '<option value="" ></option>';		
		if ($resultGetClientLocations['status'] == 'true'){
		foreach ($resultGetClientLocations['resultSet'] as $value) {

		   echo '<option value="'.$value['location_uuid'].'" >'.$value['location_name'].'</option>';  

		}
		 
		}		
				
	}
	
	public function ajax_checkUniqueIpAddress() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = trim($this->input->post('value'));
        $recordCount = $this->db->get_where($table, array($field => $value))->num_rows();
        //echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
}
?>