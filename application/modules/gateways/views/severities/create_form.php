
<!-- END PAGE HEADER-->

<div class="row">
    <div>
		
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Gateway Severity</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('gateways/severities/'.encode_url($gateway_id)); ?>">Gateway Severities</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmSeverity" id="frmSeverity" method="post" action="">
                    <div class="form-body">
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="severity_id" name="severity_id" >
										<option value=""></option>
                                        <?php
                                        if ($severitiesall['status'] == 'true') {
                                            foreach ($severitiesall['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['id']."###".$value['severity']; ?>"><?php echo $value['severity']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select> 								
                                    <label for="form_control_1">Severity Name</label>                                               
                                </div>
                            </div>							
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="gateway_severity" name="gateway_severity" type="text" tableName='severities' tableField='severity'>
                                    <label for="form_control_1">Rename Severity</label>                                               
                                </div>
                            </div>
						
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('gateways/severities/'.encode_url($gateway_id)); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script src="<?php echo base_url() . "public/" ?>js/form/form_gatewayseverities.js" type="text/javascript"></script>





