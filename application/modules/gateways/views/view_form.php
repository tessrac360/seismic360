
<!-- END PAGE HEADER-->
<div class="row">
    <div>
	
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Gateways</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					 <li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_page){
							if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }}?>
					<li>
						<span>Gateways</span>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>List</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo ($client_page)?base_url('gateways/create/').encode_url($client_id):base_url('gateways/create/'); ?>"> Add New Gateway
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">

                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover order-column" id="role">
                    <thead>
                        <tr>
                            <th style="width:20%; text-align:left;"> Gateway Name </th>   
							<th> Gateway Type </th>
                            <th> IP Address </th>
							<th> Client</th>
                            <th> Created By </th>  
                            <th  style="width:8%; text-align:left;"> Status </th>
                            <th  style="text-align:left;"> Action </th>  
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($gateway)) {
                            foreach ($gateway as $value) {
                                //  pr($value);
								//if($value['is_deleted'] == 'N'){
                                ?>
                                <tr>
                                    <td> <?php echo $value['title']; ?> </td>
									<td> <?php echo $value['type_title']; ?> </td>
									
									
                                    <td> <?php echo $value['ip_address']; ?>  </td>
									<td> <?php echo ($clientTitles[$value['client_id']])?$clientTitles[$value['client_id']]:''; ?>  </td>
									
										
                                    <td> <?php echo $value['name']; ?>  </td>
                                    
                                    <td>
                                        <input type="checkbox" <?php
                                        if ($value['status'] == 'Y') {
                                            echo 'checked';
                                        }
                                        ?>  id="onoffactiontoggle" class="onoffactiontoggle" myval="<?php echo encode_url($value['id']); ?>">
                                    </td>
									
                                    <td>
                                        <a href="<?php echo ($client_page)?base_url() . 'gateways/edit/'. encode_url($client_id)."/".encode_url($value['id']):base_url() . 'gateways/edit/'. encode_url($value['id']); ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>  
                                         <a href="javascript:void(0);"  gateway_id="<?php echo($client_page)?encode_url($client_id)."/".encode_url($value['id']):encode_url($value['id']);?>" class="btn btn-xs red isdelete" onlick=''>
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                        <a href="<?php echo base_url() . 'event/network/index/'.encode_url($value['id']); ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-tree" ></i>
                                        </a> 												
                                        <!--<a class="btn btn-xs  blue" data-toggle="tooltip" title="Manage Devices" href="<?php echo base_url() . 'nagiosmanagements/nagios_config/' . encode_url($value['id']) ; ?>">
                                           <i class="fa fa-laptop" aria-hidden="true" ></i>
                                        </a>-->										
										
                                        <a class="btn btn-xs  blue" data-toggle="tooltip" title="Manage Severities" href="<?php echo base_url() . 'gateways/severities/' . encode_url($value['id']) ; ?>">
                                           <i class="glyphicon glyphicon-warning-sign" aria-hidden="true" ></i>										
                                        </a>
										 <a  class="btn btn-xs  blue"  data-toggle="tooltip" title="Add Credentials" href="<?php echo base_url() . 'client/ClientSystemCredentials/view/' . $value['gateway_uuid'] ;?>">
										<i class="fa fa-cog" aria-hidden="true" ></i>
										</a>
										<?php if($value['gateway_type'] == '1'){?>
                                        <!--<a class="btn btn-xs  blue" data-toggle="tooltip" title="Manage Device Templates" href="<?php echo base_url() . 'nagiosmanagements/devicetemplates/' . encode_url($value['id']) ; ?>">
                                           <i class="fa fa-laptop" aria-hidden="true" ></i>										
                                        </a>
                                        <a class="btn btn-xs  blue"  data-toggle="tooltip" title="Manage Service Templates" href="<?php echo base_url() . 'nagiosmanagements/servicetemplates/' . encode_url($value['id']) ; ?>">
                                           <i class="fa fa-cog" aria-hidden="true" ></i>										
                                        </a>-->										
										<?php }?>
 										
                                    </td>
                                </tr> 
                                <?php
								//}
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9"> No Record Found </td>
                            </tr> 
<?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_gateways.js" type="text/javascript"></script>
