<style>
.marbt{margin-bottom:10px; margin-top:5px;}
.h5_title{ margin: 0; padding: 8px 18px; display: inline-block; background: #f9f9f9; }
.mt-repeater-delete { min-width: 20px !important;padding: 0;}
</style>
<!-- END PAGE HEADER-->

<div class="row">
    <div>
		
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Gateway</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_page){
							if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }}?>
					<li>
					<?php if(isset($client_id) && $client_id!=""){?>
						<a href="<?php echo base_url('gateways/index/'.encode_url($client_id)) ?>">Gateways</a>
						<i class="fa fa-angle-right"></i>
					<?php }else{?>
						<a href="<?php echo base_url('gateways') ?>">Gateways</a>
						<i class="fa fa-angle-right"></i>
					<?php }?>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmGateway" id="frmGateway" method="post" action="">
                    <div class="form-body">
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">

                                    <select class="form-control" id="admin_client" name="client_id" <?php echo ($client_page)?"disabled":"";?>  >
                                        
										<?php if($client_page){
											$client_type = ($clientName['resultSet']['client_type']== 'P')?"Partner":"Client";
											?>
											<option value="<?php echo $client_id."#".$clientName['resultSet']['client_uuid']; ?>"><?php echo $clientName['resultSet']['client_title']."(".$client_type.")";?></option>
										<?php }else{?>
										<option value=""></option>
                                        <?php if (!empty($client)) { ?>       
                                            <?php
                                            foreach ($client as $value) {
												$client_type = ($value['client_type']== 'P')?"Partner":"Client";
                                                $child = count($value['children']);
                                                ?>
                                                <option value="<?php echo $value['id']."#".$value['client_uuid']; ?>" <?php
                                                if ($child > 0) {
                                                    echo 'class="optionGroup"';
                                                }
                                                ?>><?php echo $value['client_title']."(".$client_type.")"; ?></option>
                                                        <?php
                                                        if ($child > 0) {
                                                            echo childTreeDropdown($value['children'], strlen($value['client_title']));
                                                        }
                                                    }
                                                    ?>
										<?php } }?>
                                    </select>                                    
                                    <label for="form_control_1">Client<span class="required" aria-required="true">*</span></label>
                                </div>
                            </div>	
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="gateway_type" name="gateway_type" >
										<option value=""></option>
                                        <?php
                                        if ($gatewayTypes['status'] == 'true') {
                                            foreach ($gatewayTypes['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['gatewaytype_uuid']; ?>"><?php echo $value['title']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select> 								
                                    <label for="form_control_1">Gateway Type</label>                                               
                                </div>
                            </div>							
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="gateway_location" name="gateway_location" >
										<option value=""></option>
                                        <?php
                                        if ($getClientLocations['status'] == 'true') {
                                            foreach ($getClientLocations['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['location_uuid']; ?>" ><?php echo $value['location_name']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 										
                                    </select> 								
                                    <label for="form_control_1">Gateway Location</label>                                               
                                </div>
                            </div>							
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="title" name="title" type="text">
                                    <label for="form_control_1">Gateway Name</label>                                               
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control ip_address" id="ip_address" name="ip_address" type="text" tableName="client_gateways" tableField="ip_address">
                                    <label for="form_control_1">IP Address</label>                                               
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="gateway_sync_port" name="gateway_sync_port" type="text">
                                    <label for="form_control_1">Gateway Sync Port</label>                                               
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="gateway_remote_port" name="gateway_remote_port" type="text">
                                    <label for="form_control_1">Gateway Remote Port</label>                                               
                                </div>
                            </div>
						</div>	
						<div id="email_block" > 
						
						</div>	

                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <!--<a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<?php

function childTreeDropdown($treeArray = array(), $strlenparent = '') {
    //echo $strlenparent;//exit;
    $strlenparentDot = str_repeat('&nbsp;', $strlenparent);
    ?>   
    <?php
    $child = "";
    foreach ($treeArray as $value) {
        ?>   
        <?php
        if (!empty($value['children'])) {
            $child = count($value['children']);
			
        }
		$client_type = ($value['client_type']== 'P')?"Partner":"Client";
        ?>
        <option value="<?php echo $value['id']."#".$value['client_uuid']; ?>" <?php if ($child > 0) {
            echo 'class="optionGroup"';
        } ?>><?php echo $strlenparentDot . $value['client_title']."(".$client_type.")"; ?></option>
        <?php
        if (!empty($value['children'])) {
            $countlength = $strlenparent + strlen($value['client_title']);
            echo childTreeDropdown($value['children'], $countlength);
        }
        ?>        
        <?php
    }
    ?>
    <?php
}?>
<script src="<?php echo base_url() . "public/" ?>js/form/form_gateways.js" type="text/javascript"></script>
<script type="text/javascript">

</script>
<script>
	var email_settings_div = '<div class="panel-group"><div class="panel panel-default custom-panel"><div class="panel-heading client-panel-heading"><h3 class="sub-head"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true"><span>Email Settings</span><i class="more-less glyphicon glyphicon-minus pull-right"></i></a></h3></div><div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style=""><div class="panel-body"><div class="row"><div class="col-md-12"><div class="fieldRow clearfix"><div class="col-md-6"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="gateway_hostname" name="gateway_hostname" type="text" ><label for="form_control_1">Gateway Hostname</label> </div></div><div class="col-md-6"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="gateway_email" name="gateway_email" type="text" value=""><label for="form_control_1">Email</label> </div></div><div class="col-md-6"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="gateway_password" name="gateway_password" type="password" value=""><label for="form_control_1">Password</label> </div></div></div></div><div class="col-md-6"><div class="accept_input_fields_wrap wrapping"><h5 class="h5_title">Accept Emails</h5><div class="acceptcountdivs"><div class="fieldRow clearfix"><div class="col-md-10"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control email_address" id="accept_email" name="accept_email[]" type="text" ><label for="form_control_1">Email</label> </div></div><div class="col-md-2"> <div id="div_id_stock_1_service" class="form-group"><label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_accept_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i> </a> </div></div></div></div></div></div><button class="add_accept_field_button btn blue btn-outline mt-repeater-add pull-right marbt">Add More</button><div class="clearfix"></div></div><div class="col-md-6"><div class="reject_input_fields_wrap wrapping"><h5 class="h5_title">Reject Emails</h5><div class="rejectcountdivs"><div class="fieldRow clearfix"><div class="col-md-10"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control email_address" id="reject_email" name="reject_email[]" type="text" ><label for="form_control_1">Email</label> </div></div><div class="col-md-2"> <div id="div_id_stock_1_service" class="form-group"><label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_reject_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i> </a> </div></div></div></div></div></div><button class="add_reject_field_button btn blue btn-outline mt-repeater-add pull-right marbt">Add More</button><div class="clearfix"></div></div></div></div></div></div></div>';
    $('#gateway_type').on('change', function () {
        $("#email_block").html('');
        var templateId = this.value;
		if(templateId != 1 && templateId != ''){
			$("#email_block").html(email_settings_div);
			addmore_tabs();
		}
			
    });
function addmore_tabs(){
	var max_fields      = 10; //maximum input boxes allowed
    var accept_wrapper         = $(".accept_input_fields_wrap"); //Fields wrapper
	
    var add_accept_button      = $(".add_accept_field_button"); //Add button ID
	var px = 0;
    px = $('.acceptcountdivs').length; //initlal text box count
	//alert(px);
	
	
	$(add_accept_button).click(function(e){ //on add input button click
        e.preventDefault();
        $(accept_wrapper).append('<div class="acceptcountdivs"><div class="fieldRow clearfix"><div class="col-md-10"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control email_address" id="accept_email" name="accept_email[]" type="text" ><label for="form_control_1">Email</label> </div></div><div class="col-md-2"> <div id="div_id_stock_1_service" class="form-group"><label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_accept_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i> </a> </div></div></div></div></div>'); //adding form
		px++;
		recall();
    });
   
   $(accept_wrapper).on("click",".remove_accept_field", function(e){ //user click on remove button
        e.preventDefault(); 
		$(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove();
		px--;	
		//alert(x);
    });
	
	var max_fields      = 10; //maximum input boxes allowed
    var reject_wrapper         = $(".reject_input_fields_wrap"); //Fields wrapper
	
    var add_reject_button      = $(".add_reject_field_button"); //Add button ID
	var px = 0;
    px = $('.rejectcountdivs').length; //initlal text box count
	//alert(px);
	
	
	$(add_reject_button).click(function(e){ //on add input button click
        e.preventDefault();
        $(reject_wrapper).append('<div class="rejectcountdivs"><div class="fieldRow clearfix"><div class="col-md-10"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control email_address" id="reject_email" name="reject_email[]" type="text" ><label for="form_control_1">Email</label> </div></div><div class="col-md-2"> <div id="div_id_stock_1_service" class="form-group"><label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_reject_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i> </a> </div></div></div></div></div>'); //adding form
		px++;
		recall();
    });
   
   $(reject_wrapper).on("click",".remove_reject_field", function(e){ //user click on remove button
        e.preventDefault(); 
		$(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove();
		px--;	
		//alert(x);
    });		
	
}	
$(document).on('change', '.email_address', function () {
		var checkarray ={};
	    var i=0;
		$(".email_address").each(function() {
			var email_address = $(this).val();
							
			if(checkarray[email_address] == undefined ){					
				checkarray[email_address]= new Array();	
				checkarray[email_address][i] = 'ok';				  
				i++;
			}else{
				swal("Oops!!!", email_address + " already exists!", "warning");
				$(this).val('');
				
			}

		});
});		
</script>