<?php

class Model_gateways extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->gateway = 'client_gateways';
		$this->gateway_severities = 'gateway_severities';	
		$this->gateway_accept_list = 'gateway_accept_list';	
		$this->severities = 'severities';		
        $this->tablename = 'client';
        $this->users = 'users';
		$this->gateway_types = 'gateway_types';
		$this->client_locations = 'client_locations';		
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function isExitGateway($id) {
        $this->db->select('id');
        $this->db->from($this->gateway);
        $this->db->where('id', $id);
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function insertGateway($postData = array()){
		$this->db->insert($this->gateway, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
	
    public function insertGatewayAcceptList($postData = array()){
		$this->db->insert_batch($this->gateway_accept_list, $postData);
		$return['status'] = 'true';
		return $return;
    }	

    public function updateGateway($postData = array(), $id) {
		$this->db->where('id', $id);
        $update_status = $this->db->update($this->gateway, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getGatewayPagination($search = '', $client_ids = 0, $limit = 0, $start = 0) {
        $this->db->select('g.*,gatwty.title as type_title, CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from($this->gateway . ' as g');
        $this->db->join('users as usr', 'usr.id = g.created_by');
		$this->db->join('gateway_types as gatwty', 'gatwty.gatewaytype_uuid = g.gateway_type');
        $this->db->where_in('g.client_id', $client_ids);
		$this->db->where('g.is_deleted', 'N');
        if ($search != "") {
            $this->db->like('g.title', $search);
        }		
        $this->db->order_by('g.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $gateway->result_array();
    }


    public function getGatewaySearchData($search, $client_ids = "",$limit = "", $start = "", $order_by = "id desc", $order_type = "") {
        $this->db->select('g.*,gatwty.title as type_title, CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from($this->gateway . ' as g');
        $this->db->join('users as usr', 'usr.id = g.created_by');
		$this->db->join('gateway_types as gatwty', 'gatwty.gatewaytype_uuid = g.gateway_type');
		$this->db->where_in('g.client_id', $client_ids);
		$this->db->where('g.is_deleted', 'N');
        if ($search != "") {
            $this->db->like('g.title', $search);
        }
        $this->db->order_by('g.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
		//echo $this->db->last_query();exit;

        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        } else {
            $data = array();
        }
		//pr($data);exit;

        return $data;
    }

    public function getGatewayEdit($postId = "",$type= "") {
        $this->db->select('*');
        $this->db->from($this->gateway);
		if($type !=""){
			$this->db->where('gateway_uuid', $postId);
		}else{
			$this->db->where('id', $postId);
		}
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

	public function isDeleteGateway($postId) {
        if ($postId != "") {
            $data = array('is_deleted' => 'Y');
            $this->db->where('id', $postId);
            $this->db->update($this->gateway, $data);
			//echo $this->db->last_query();exit; 
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('id', $id);
            $this->db->update($this->gateway, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	public function getClientName($clientId = "") {
        $this->db->select('client_title,client_uuid,client_type');
        $this->db->from('client');
        $this->db->where('id', $clientId);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
    public function getGatwaySeveritiesPagination($gateway_id = NULL,$search = '', $limit = 0, $start = 0) {
				
        $this->db->select('gs.*,s.severity');
        $this->db->from($this->gateway_severities.' as gs');
		$this->db->join($this->severities.' as s', 's.id = gs.severity_id');
		$this->db->where('gs.gateway_id', $gateway_id);
        if ($search != "") {
            $this->db->like('severity', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $gateway->result_array();
    }	
    public function getSeverities() {
        $this->db->select('*');
        $this->db->from($this->severities);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function getGatewayDetails($gateway_id = "") {
        $this->db->select('id,title,ip_address,client_id,gateway_type');
        $this->db->from($this->tablename_client_gateways);
        $this->db->where('id', $gateway_id);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    public function getGatewayTypes() {
        $this->db->select('*');
        $this->db->from($this->gateway_types);
		$this->db->where('status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }	
    public function insertGatewaySeverity($postData = array()){
		$this->db->insert($this->gateway_severities, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
    public function getGatewaySeverityEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->gateway_severities);
        $this->db->where('id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
	
    public function checkUniqueSeverity($postData = array()) {
        $this->db->select('*');
        $this->db->from($this->gateway_severities);
        $this->db->where($postData);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }	
    public function updateGatewaySeverity($postData = array(), $id) {
		$this->db->where('id', $id);
        $update_status = $this->db->update($this->gateway_severities, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	public function isDeleteGatewaySeverity($postId) {
        if ($postId != "") {
            $this->db->where('id', $postId);
            $this->db->delete($this->gateway_severities);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
    public function updateSeverityStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('id', $id);
            $this->db->update($this->gateway_severities, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	
    public function getClientLocations($client_uuid = NULL) {
        $this->db->select('*');
        $this->db->from($this->client_locations);
		$this->db->where('referrence_uuid', $client_uuid);
		$this->db->where('is_disabled', '0');
		//$this->db->where('is_deleted', '1');
		
        $query = $this->db->get();
		//echo $this->db->last_query();exit; 
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
}

?>