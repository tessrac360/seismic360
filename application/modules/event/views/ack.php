<link href="<?php echo base_url() . "public/" ?>datapicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<style>
    .xdsoft_noselect{z-index:99999 !important;}
    .modal-header{padding: 6px 10px !important;}
</style>
<?php //pr($events) ?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title" style="font-size:15px;"><b>Basic Info - E-<?php echo str_pad($event_id, 10, '0', STR_PAD_LEFT) ?></b></h3>
    </div>
    <div class="modal-body">

        <table class="table table-striped table-bordered table table-hover" id="tblGrid">
            <thead>
                <tr>
                    <th>CLIENT :</th>
                    <th>HOST NAME :</th>
                    <th>HOST IP :</th>

                </tr>
            </thead>
            <tbody class="bold-text">
                <tr>

                    <td class="normal-text"><?php echo $events['resultSet']['client_title'] ?></td>    
                    <td class="normal-text"><?php echo $events['resultSet']['device_name'] ?></td>
                    <td class="normal-text"><?php echo $events['resultSet']['ip_address'] ?></td>
                </tr>

            </tbody>
        </table>
        <div class="portlet"> 
            <div class="portlet-body">                                  
                <div>
                    <div class="portlet light ">									
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#eventhistory" data-toggle="tab" style="font-size:13px;color:#000;"><b>Event History </b></a>
                                </li>
                                <li>
                                    <a href="#monitorservices" data-toggle="tab" style="font-size:13px;color:#000;"><b>Service Status</b></a>
                                </li>
                                <li>
                                    <a href="#activitylog" data-toggle="tab" style="font-size:13px;color:#000;"><b> Suppress Action</b></a>
                                </li>
                                <li>
                                    <a href="#suppresshistory" data-toggle="tab" style="font-size:13px;color:#000;"><b> Suppress History </b></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                            <div class="tab-pane" id="suppresshistory">
                                    <table class="table table-striped table-bordered table table-hover" id="tblGrid">   
                                        <thead style="background:#17c4bb;">
                                            <tr>
                                                <th style="color:#fff;">Start Time</th>
                                                <th style="color:#fff;">End Time</th>
                                                <th style="color:#fff;">Duration</th>
                                                <th style="color:#fff;">Note</th> 
                                                <th style="color:#fff;">Type</th>
                                                <th style="color:#fff;">Created By</th>
                                                <th style="color:#fff;">Suppresed Date</th>                                              
                                            </tr>
                                        </thead>
                                        <tbody class="bold-text">
                                            <?php 
                                           // pr($eventsAck);
                                           if($eventsAck['status']=='true'){
                                            foreach ($eventsAck['resultSet'] as $Data) { ?>
                                                <tr>
                                                    <td class="normal-text1"><?php echo $Data['event_suppress_starttime'] ?></td>
                                                    <td class="normal-text1"><?php echo $Data['event_suppress_endtime'] ?></td>
                                                    <td class="normal-text1"><?php echo $Data['duration'] ?></td> 
                                                    <td class="normal-text1"><?php echo $Data['ack_notes'] ?></td> 
                                                    <td class="normal-text1"><?php echo $Data['ack_type'] ?></td> 
                                                    <td class="normal-text1"><?php echo $Data['name'] ?></td>
                                                    <td class="normal-text1"><?php echo $Data['created_on'] ?></td>                                              
                                                </tr>  
                                            <?php } 
                                            } ?>								 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane active" id="eventhistory">
                                    <table class="table table-striped table-bordered table table-hover" id="tblGrid">   
                                        <thead style="background:#17c4bb;">
                                            <tr>
                                                <th style="color:#fff;">Host Name</th>
                                                <th style="color:#fff;">Event Type</th>
                                                <th style="color:#fff;">Description</th>
                                                <th style="color:#fff;">Severity</th>
                                                <th style="color:#fff;">Event Start Time</th>
                                            </tr>
                                        </thead>
                                        <tbody class="bold-text">
                                            <?php foreach ($eventsHistory['resultSet'] as $historyData) { ?>
                                                <tr>
                                                    <td class="normal-text1"><?php echo $historyData['device_name'] ?></td>
                                                    <td class="normal-text1"><?php echo $historyData['service_name'] ?></td>
                                                    <td class="normal-text1"><?php echo $historyData['event_text'] ?></td>
                                                    <td class="normal-text1"><?php echo $historyData['severity'] ?></td>
                                                    <td class="normal-text1"><?php echo $historyData['event_start_time'] ?></td>


                                                </tr>  
                                            <?php } ?>								 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="monitorservices">
                                    <table class="table table-striped table-bordered table table-hover" id="tblGrid">   
                                        <thead style="background:#17c4bb;">
                                            <tr>
                                                <th style="color:#fff; width:6%;">Event Type</th>
                                                <th style="color:#fff;  width:23%;">Event Text</th>
                                                <th style="color:#fff; width:3%;">Severity</th>
                                                <th style="color:#fff;  width:5%;">Start Time</th>
                                                <th style="color:#fff;  width:1%;">Duration</th>
                                            </tr>
                                        </thead>
                                        <tbody class="bold-text">
                                            <?php foreach ($eventMonitorService['resultSet'] as $historyData) { ?>
                                                <tr>

                                                    <td class="normal-text1"><?php echo $historyData['service_name'] ?></td>
                                                    <td class="normal-text1"><?php echo $historyData['event_text'] ?></td>
                                                    <td class="normal-text1"><?php echo $historyData['severity'] ?></td>
                                                    <td class="normal-text1"><?php echo $historyData['event_start_time'] ?></td>
                                                    <td class="normal-text1"><?php echo $historyData['Duration'] ?></td>								


                                                </tr>  
                                            <?php } ?>								 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="activitylog">                         
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <ul class="nav nav-tabs tabs-left">
                                                    <li class="active">
                                                        <a href="#supress" data-toggle="tab" style="font-size:13px;color:#000;"><b>Temporary</b></a>
                                                    </li>
                                                    <li>
                                                        <a href="#permanent" data-toggle="tab" style="font-size:13px;color:#000;"><b>Permanent</b></a>
                                                    </li>
                                                    <!-- <li>
                                                        <a href="#ack" data-toggle="tab" style="font-size:13px;color:#000;"><b>Acknowledgement</b></a>
                                                    </li> -->

                                                </ul>
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <div class="tab-content">

                                                    <div class="tab-pane active" id="supress">
                                                        <form id ="frm_Supress" method="post" action="">

                                                            <div class="form-group col-md-12">
                                                                <label for="firstname" class="col-sm-2 control-label">Select Date :</label>
                                                                <div class="col-sm-5">
                                                                    <div class="input-group date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d" style="width:100%;">
                                                                        <input type="text" class="form-control datetimeSupress" name="datetimeSupress" id="datetimeSupress" style="width:100%;">
                                                                        <span id="msg_datetimeSupress"></span>
                                                                    </div>  
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-12">
                                                                <label for="firstname" class="col-sm-2 control-label">Comment :</label>
                                                                <div class="col-sm-5">
                                                                    <textarea style="width:100%;" class="form-control" id="txtsurpress" name="txtsurpress" placeholder="Comment !"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>

                                                            <div class="form-group col-md-12">
                                                                <label for="firstname" class="col-sm-2 control-label hidden-xs">&nbsp;</label>
                                                                <div class="col-sm-5">
                                                                    <input type="hidden" value="<?php echo $event_id; ?>" name="su_event_id">
                                                                    <input type="hidden" value="T" name="ack_type">
                                                                    <input type="hidden" value="<?php echo  $events['resultSet']['client_id'];?>" name="client_id">
                                                                    <button class="btn btn-success pull-right" style="padding:5px;" type="submit" id="btnsurpress" >Save</button>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-5">

                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div class="tab-pane fade" id="permanent">
                                                        <div class="col-sm-12">
                                                            <form id ="frm_Supress_permanent" method="post" action="">
                                                                                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-12">
                                                                <label for="firstname" class="col-sm-2 control-label">Comment :</label>
                                                                <div class="col-sm-5">
                                                                    <textarea style="width:100%;" class="form-control" id="txtsurpress" name="txtsurpress" placeholder="Comment !"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
															 <div class="form-group col-md-12">
                                                                <label for="firstname" class="col-sm-2 control-label hidden-xs">&nbsp;</label>
                                                                <div class="col-sm-5">
                                                                   <input type="hidden" value="<?php echo $event_id; ?>" name="su_event_id">                                                                
                                                                <input type="hidden" value="P" name="ack_type">
                                                                 <input type="hidden" value="<?php echo  $events['resultSet']['client_id'];?>" name="client_id">
                                                                <button class="btn btn-success pull-right" style="padding:5px;" id="btnsurpresspermanent">Save</button>
                                                                </div>
                                                            </div>
															
																
                                                            </form>
                                                        </div>
                                                    </div>  
                                                    <div class="tab-pane" id="ack">
                                                        <form id ="frm_acknowledgement" method="post" action="">                                                         
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-12">
                                                                <label for="firstname" class="col-sm-2 control-label">Acknowledgement :</label>
                                                                <div class="col-sm-5">
                                                                    <textarea style="width:100%;" class="form-control" id="txtacknowledgement" name="txtacknowledgement" placeholder="Comment !"></textarea>
                                                                     <span id="msg_manualack"></span>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group col-md-12">
                                                                <label for="firstname" class="col-sm-2 control-label hidden-xs">&nbsp;</label>
                                                                <div class="col-sm-5">
                                                                    <input type="hidden" value="<?php echo $event_id; ?>" name="su_event_id">
                                                                    <input type="hidden" value="A" name="ack_type">
                                                                    <input type="hidden" value="<?php echo  $events['resultSet']['client_id'];?>" name="client_id">
                                                                    <button class="btn btn-success pull-right" style="padding:5px;" type="submit" id="btnack" >Save</button>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-5">
                                                            </div>
                                                        </form>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>                        
                                </div>                                  
                            </div>                                 
                        </div>
                    </div>						
                </div>     
            </div>
        </div> 

    </div><!-- /.modal-content -->
    <script src="<?php echo base_url() . "public/" ?>datapicker/onejquery.datetimepicker.full.js"></script>
    <script>

        $('.datetimeSupress').datetimepicker({
           
            dayOfWeekStart: 1,
            lang: 'en',
            timepicker: true,
            showTimePicker: true,
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false,
            minDate: 0,
            step: 5
        });
    </script>