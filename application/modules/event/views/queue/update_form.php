<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url() . "public/" ?>treeview/treejs/js/jquery.tree.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>treeview/treejs/css/jquery.tree.css"/>
<script>
    $(document).ready(function () {
        $('#example-0 div').tree();
    });
</script>
<style>
    .ui-widget.ui-widget-content {
        border: 0px solid #cccccc;
    }
    .ui-widget-content {
        border: 0px solid #0000 !important;
        background: top repeat-x !important;
    }
</style>
<!-- END PAGE HEADER-->
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Queue</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					 <li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php //echo base_url('users') ?>">Queue</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php
            if ($getQueue['status'] = 'true') {
        ?>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">

                <form role="form" name="frmQueue" id="frmQueue" method="post" action="" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase"> Queue Information</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="queue_name" name="queue_name" type="text" value="<?php echo ($getQueue['resultSet']->queue_name) ? $getQueue['resultSet']->queue_name : ''; ?>" tableName='ticket_queue' tableField='queue_name'>
                                    <label for="form_control_1">Queue Name<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
                            
                            </div>
                            
                            <div id="groupdiv">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Severities</span>
                                    </div>
                                </div>
								<div id="subClients_list"></div>
                                <div class="form-group" >
                                    <div class="col-sm-12" style="padding: 10px 0 10px 15px">
                                        
                                        <?php
										$severity_id = explode(',', $getQueue['resultSet']->severity_id);
                                        if ($severity['status'] == 'true') {
                                        foreach ($severity['resultSet'] as $value) {
											//pr($value);exit;
                                            ?>	
                                            <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;margin-right: 25px;    margin-bottom: 6px;">
                                                <input type="checkbox" class='groupcheck' name ="severity_id[]" value="<?php echo $value['id']; ?>" <?php if (in_array($value['id'], $severity_id)) echo 'checked="checked"'; ?> id="<?php echo $value['id']; ?>"><?php echo $value['severity']; ?>
                                                <span></span>
                                            </label>
<?php } ?>
<?php } ?>
                                    </div>
                                    <!--<div class="col-sm-12" id="group_id_error"></div>-->
                                </div>

                                
                            </div>
                            <div id="groupdiv">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase">Priorities</span>
                                    </div>
                                </div>
								<div id="subClients_list"></div>
                                <div class="form-group" >
                                    <div class="col-sm-12" style="padding: 10px 0 10px 15px">
                                        
                                        <?php
										$skill = explode(',', $getQueue['resultSet']->skill_id);
										if ($priority['status'] == 'true') {
                                        foreach ($priority['resultSet'] as $value) {
										?>	
                                            <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;margin-right: 25px;    margin-bottom: 6px;">
                                                <input type="checkbox" class='groupcheck' name ="skill_id[]" value="<?php echo $value['skill_id']; ?>"  <?php if (in_array($value['skill_id'], $skill)) echo 'checked="checked"'; ?> id="<?php echo $value['skill_id']; ?>"><?php echo $value['title']; ?>
                                                <span></span>
                                            </label>
<?php } ?>
<?php } ?>
                                    </div>
                                    <!--<div class="col-sm-12" id="group_id_error"></div>-->
                                </div>


                            </div>
							
							<div id="groupdiv">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Incident Status</span>
                                    </div>
                                </div>
								<div id="subClients_list"></div>
                                <div class="form-group" >
                                    <div class="col-sm-12" style="padding: 10px 0 10px 15px">
                                        
                                        <?php
										$status = explode(',', $getQueue['resultSet']->ticket_status_id);
										if ($ticketState['status'] == 'true') {
                                        foreach ($ticketState['resultSet'] as $value) {
                                            ?>	
                                            <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;margin-right: 25px;    margin-bottom: 6px;">
                                                <input type="checkbox" class='groupcheck' name ="ticket_status_id[]" value="<?php echo $value['status_code']; ?>" <?php if (in_array($value['status_code'], $status)) echo 'checked="checked"'; ?> id="<?php echo $value['status_code']; ?>"><?php echo $value['status']; ?>
                                                <span></span>
                                            </label>
<?php } ?>
<?php } ?>
                                    </div>
                                    <div class="col-sm-12" id="group_id_error"></div>
                                </div>


                            </div>
							
                        </div>
                        <div class="form-actions noborder">
                            
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php //echo base_url('users'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
			 <?php } ?>
        </div>
    </div>
</div>

<script>
$(':checkbox[readonly]').click(function(){ return false; });
</script>     
<script src="<?php echo base_url() . "public/" ?>js/form/form_queue.js" type="text/javascript"></script>    
