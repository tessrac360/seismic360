<?php
$accessAdd = helper_fetchPermission('13', 'add');
$accessEdit = helper_fetchPermission('13', 'edit');
$accessStatus = helper_fetchPermission('13', 'active');
$accessDelete = helper_fetchPermission('13', 'delete');
?>

<!-- END PAGE HEADER-->
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Queue</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					 <li>
						<i class="icon-home"></i>
						<a href="<?php //echo base_url() ?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span>Queues</span>
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            
            <div class="portlet-body padd-top0">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
                            <?php if ($accessAdd == 'Y') { ?> 
                                <div class="btn-group">
                                    <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('event/queue/create') ?>"> Add New Queue
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">

                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover order-column" id="role">
                    <thead>
                        <tr>
                            <th> Name </th>                           
                            <th> Created By </th>   
							<?php //if ($accessEdit == 'Y' || $accessDelete == 'Y') { ?> 
                                <th> Action </th>
                            <?php //} ?>


                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($queue)) {
                            foreach ($queue as $value) {
                                //  pr($value);
                                ?>
                                <tr>
                                    <td> <?php echo $value['queue_name'] ?> </td>
                                    <td> <?php echo $value['username']; ?>  </td>  
                                    <?php //if ($accessEdit == 'Y' || $accessDelete == 'Y') { ?> 
                                        <td>
                                            <?php //if ($accessEdit == 'Y') { ?> 
                                                <a href="<?php echo base_url() . 'event/queue/edit/' . encode_url($value['queue_id']); ?>" class="btn btn-xs  blue">
                                                    <i class="fa fa-edit"></i>
                                                </a> 
                                            <?php //} ?>
                                            <?php //if ($accessDelete == 'Y') { ?>
                                                <a href="javascript:void(0);"  queueId="<?php echo encode_url($value['queue_id']); ?>" class="btn btn-xs red isdeleteQueue">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                                <?php
                                            //}
                                            ?>
                                        </td>
                                    <?php //} ?>

                                </tr> 
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9"> No Record Found </td>
                            </tr> 
                        <?php } ?>
                    </tbody>
                </table>
            </div>
           <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_queue.js" type="text/javascript"></script>  