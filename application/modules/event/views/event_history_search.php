<style>
    .inline-form-body div[class*='col-']{padding:0;}
    .inline-form-body div[class*='col-'] .btn-group, .inline-form-body div[class*='col-'] .multiselect.dropdown-toggle.btn, .inline-form-body div[class*='col-'] .input-group.input-medium.date-picker, .inline-form-body div[class*='col-'] button[type="submit"]{width:100% !important;}

    .inline-form-body div[class*='col-'] button[type="submit"]{width:50% !important;}
    .inline-form-body div[class*='col-'] button[type="reset"]{width:45% !important;}

    .inline-form-body div[class*='col-'] .btn-group, .inline-form-body div[class*='col-'] .multiselect.dropdown-toggle.btn{border-right:0;     text-align: left;}
    .inline-form-body div[class*='col-'] .multiselect.dropdown-toggle.btn .caret{ position:absolute; right:10px; top:50%;}
    .inline-form-body div[class*='col-'] .multiselect.dropdown-toggle.btn span{ overflow: hidden; width: 92%; display: block;}
    .inline-form-body div[class*='col-'] .multiselect.dropdown-toggle.btn-default:hover{border-color: #ccc;}
</style>

<?php
$opendiv = 0;
if (isset($_SESSION['eventHistorySession'])) {
    $filterPost = unserialize($_SESSION['eventHistorySession']);
//    pr($filterPost);//exit;

    if (!empty($filterPost)) {
        if (isset($filterPost['client'])) {
            $clientsFilter = $filterPost['client'];
            $opendiv = 1;
        }
        if (isset($filterPost['device'])) {
            $device_nameFilter = $filterPost['device'];
            $opendiv = 1;
        }
        if (isset($filterPost['service'])) {
            $serviceFilter = $filterPost['service'];
            $opendiv = 1;
        }
        if (isset($filterPost['severity'])) {
            $severityFilter = $filterPost['severity'];
            $opendiv = 1;
        }
        if ($filterPost['from_date'] != "") {
            $from_dateFilter = $filterPost['from_date'];
            $opendiv = 1;
        }
        if ($filterPost['to_date'] != "") {
            $to_dateFilter = $filterPost['to_date'];
        }
    }
}
?>
<!-- END PAGE HEADER-->
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Event</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>View</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('event/history') ?>"> Events </a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>List</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
<!--                <div id="costom_search" class="table-toolbar collapse <?php echo ($opendiv == 1) ? 'in' : ''; ?>">
                    <div class="row">
                        <form  role="form" method="post">
                            <div class="form-body inline-form-body">                            
                                <div>
                                    <div class="col-md-2">
                                        <select id="client" multiple="multiple" class="client" name="client[]">
                                            <?php
                                            if ($clients['status'] == 'true') {
                                                foreach ($clients['resultSet'] as $value) {
                                                    ?>
                                                    <option value="<?php echo $value->id; ?>"  <?php
                                                    if (isset($clientsFilter)) {
                                                        if (in_array($value->id, $clientsFilter)) {
                                                            echo "selected";
                                                        }
                                                    }
                                                    ?>><?php echo $value->client_title; ?></option>  
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select id="device" multiple="multiple" class="device" name="device[]">
                                            <?php
                                            if ($hostName['status'] == 'true') {
                                                foreach ($hostName['resultSet'] as $value) {
                                                    ?>
                                                    <option value="<?php echo $value->device_id; ?>" <?php
                                                    if (isset($device_nameFilter)) {
                                                        if (in_array($value->device_id, $device_nameFilter)) {
                                                            echo "selected";
                                                        }
                                                    }
                                                    ?>><?php echo $value->device_name; ?></option>  
        <?php
    }
}
?> 
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select id="service" multiple="multiple" class="service" name="service[]">
                                            <?php
                                            if ($service['status'] == 'true') {
                                                foreach ($service['resultSet'] as $value) {
                                                    ?>
                                                    <option value="<?php echo $value->service_id; ?>" <?php
                                                            if (isset($serviceFilter)) {
                                                                if (in_array($value->service_id, $serviceFilter)) {
                                                                    echo "selected";
                                                                }
                                                            }
                                                            ?>><?php echo $value->service_description; ?></option>  
        <?php
    }
}
?> 
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select id="severity" multiple="multiple" class="severity" name="severity[]">
                                            <?php
                                            if ($severity['status'] == 'true') {
                                                foreach ($severity['resultSet'] as $value) {
                                                    ?>
                                                    <option value="<?php echo $value->id; ?>" <?php
                                                            if (isset($severityFilter)) {
                                                                if (in_array($value->id, $severityFilter)) {
                                                                    echo "selected";
                                                                }
                                                            }
                                                            ?>><?php echo $value->severity; ?></option>  
        <?php
    }
}
?> 
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group input-medium date-picker input-daterange" data-date-format="yyyy-mm-dd">
                                            <input id="from_date" type="text" autocomplete="off" class="form-control datepick" value="<?php
                                                   if (isset($from_dateFilter)) {
                                                       echo $from_dateFilter;
                                                   }
?>" name="from_date">
                                            <span class="input-group-addon"> To </span>
                                            <input id="to_date" type="text" autocomplete="off" class="form-control datepick" value="<?php
                                                   if (isset($to_dateFilter)) {
                                                       echo $to_dateFilter;
                                                   }
?>" name="to_date"> 
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="submit" class="btn green"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        <button type="button" id="resetEventhistory" class="btn green"><i class="fa fa-refresh" aria-hidden="true"></i></button> 
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>-->
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="role">
                    <thead style="background: #17C4BB !important; color:#fff;">
                        <tr>
                            <th> Client </th>                           
                            <th> Host Name </th>
                            <th> Service </th>                                                     
                            <th> Severity </th>   
                            <th> Start date </th>  
                            <th> End Date </th>
                            <th> Duration </th>                           
                        </tr>
                    </thead>
                    <tbody>
<?php
if (!empty($eventHistory)) {
    foreach ($eventHistory as $value) {
        //  pr($value);
        ?>
                                <tr>
                                    <td> <?php echo $value->client_title; ?> </td>
                                    <td> <?php echo $value->device_name; ?>  </td>
                                    <td> <?php echo $value->service_name; ?>  </td>
                                    <td style="background-color: <?php echo $value->color_code ?>"> <?php echo $value->severity ?> </td>                                    
                                    <td> <?php echo $value->event_start_time; ?>  </td> 
                                    <td> <?php echo $value->event_end_time; ?>  </td> 
                                    <td> <?php echo $value->Duration; ?>  </td> 

                                </tr> 
                            <?php
                        }
                    } else {
                        ?>
                            <tr>
                                <td colspan="7"> No Record Found </td>
                            </tr> 
<?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
<?php //echo $this->pagination->create_links();  ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_event_history.js" type="text/javascript"></script>