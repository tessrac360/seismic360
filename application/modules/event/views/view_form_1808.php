<link href="<?php echo base_url() . "public/js/" ?>events/css/events_style.css" rel="stylesheet" type="text/css" />
<style>
    th {padding: 0}
    th > span {padding: 0 10px}
    .severityEvent {width: 100%;padding-top: 5px;height: 100%; z-index: 0;position:absolute; top: 0; left: 0};
</style>
<div class="full-height-content full-height-content-scrollable" style="background:#ffffff;">

    <div class="full-height-content-body"  style="overflow:auto !important;">
        <div class="row">
            <div class="parent">
                <div class="col-md-8" style="background:rgba(204, 204, 204, 0.22);">
                    <div class="row">
                        <h1 class="" style="padding: 6px 9px;margin:0px;font-size: 15px;font-weight: 400;letter-spacing: -1px;line-height: 26px;display: block;font-family: "Open Sans", sans-serif;"> Events Table </h1>
                    </div>
                </div>
                <div class="col-md-4" style="background:rgba(204, 204, 204, 0.22);">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="input-group add-on" style="margin-top:5px;"> 
                                <input class="form-control" placeholder="Search" name="eventSearch" id="eventSearch" type="text" style="padding:5px 12px !important; height: 31px;">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" id="btn_eventSearch" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="col-sm-6">
                                <div class="chkdropdowns chkdropdowns-right ">
                                    <i class="fa fa-arrows dropdown-toggle color-cogs" data-toggle="dropdown"></i>
                                    <ul class="column_sort_table">
                                        <section class="">
                                            <div class="column_sort_table_height containers">
                                                <input type = "hidden" name="row_order" id="row_order" />
                                                <table class="column_sort_table sticky-table topMoveable" id="sortable-row" style="border-spacing: 0;width:100%;">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <div>Column sorter &nbsp <button class="btn btn-warning save-btn" id="btnSorting">Save</button></div>
                                                            </th>    
                                                        </tr>
                                                    </thead>
                                                    <tbody class="ui-sortable column_sort_body">
                                                        <?php
                                                        if ($eventColumn['status'] == true) {

                                                            foreach ($eventColumn['resultSet'] as $value) {
                                                                if ($value->column_id != 1) {
                                                                    ?>
                                                                    <tr class="ui-sortable-handle">
                                                                        <td id=<?php echo $value->sort_id; ?>><i class="fa fa fa-arrows-v" aria-hidden="true"></i> <?php echo $value->rename_column ?></td>      
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>														
                                    </ul>
                                </div>	
                            </div>
                            <div class="col-md-6"> 
                                <!-- Bootstrap popover button -->
                                <div class="columnSelectorWrapper">
                                    <input id="colSelect1" type="checkbox" class="hidden">
                                    <label class="columnSelectorButton" for="colSelect1"><i class="fa fa-cog pull-right" aria-hidden="true"></i><div></div></label>
                                    <div id="columnSelector" class="columnSelector">
                                        <!-- this div is where the column selector is added -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="overflow:auto;height:530px; padding-top:3px;">	
                    <table id="demo-table" class="eventTable tablesorter custom-popup bordered cellpadding-0 cellspacing-0 table table-bordered table-hover">
                        <thead>
                            <tr class="table-filter">    
                                <?php
                                    if ($eventColumn['status'] == true) {
                                    foreach ($eventColumn['resultSet'] as $value) {
                                        if ($value->is_filter == 'Y') {
                                            $functionName = 'event_' . $value->filter_function;
                                            $functionVariableName = 'filter_' . $value->filter_function;
                                            $functionVariable = (eval('return $' . $functionVariableName . ';'));
                                            ?>
                                            <td><?php $functionName($functionVariable); ?></td>
                                            <?php
                                        } else {
                                            echo "<td></td>";
                                        }
                                    }
                                }
                                ?>
                            </tr>
                            <tr class=" dnd-moved color-scheme" class="table-filter">
                                <?php
                                if ($eventColumn['status'] == true) {
                                    foreach ($eventColumn['resultSet'] as $value) {
                                        ?>
                                        <th><?php echo $value->rename_column ?></th>

                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
            <div class="right_block answer col-md-3" style="display:none;">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">Revenue</span>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- EVENT MONITOR STARTS HERE -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script src='https://cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.js'></script>
<script src="<?php echo base_url() . "public/js/" ?>events/js/drag/dragndrop.table.columns.js" type="text/javascript"></script>		
<style>body.tablesorter-disableSelection { -ms-user-select: none; -moz-user-select: -moz-none;-khtml-user-select: none; -webkit-user-select: none; user-select: none; }.tablesorter-resizable-container { position: relative; height: 1px; }.tablesorter-resizable-handle { position: absolute; display: inline-block; width: 8px;top: 1px; cursor: ew-resize; z-index: 3; user-select: none; -moz-user-select: none; }</style>
<!-- Tablesorter: required -->
<script src="<?php echo base_url() . "public/js/" ?>events/js/jquery.tablesorter.js"></script>
<script src="<?php echo base_url() . "public/js/" ?>events/js/jquery.tablesorter.widgets.js"></script>
<script src="<?php echo base_url() . "public/js/" ?>events/js/widgets/widget-columnSelector.js"></script>
<script src="<?php echo base_url() . "public/js/" ?>events/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url() . "public/js/" ?>events/js/column_filter.js"></script>	
<script src="<?php echo base_url() . "public/js/" ?>events/js/rightbar.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_event.js" type="text/javascript"></script>