<link href="<?php echo base_url() . "public/js/" ?>events/css/events_style.css" rel="stylesheet" type="text/css" />
<style>
    th {padding: 0}
    th > span {padding: 0 10px}
    .severityEvent {width: 100%;padding-top: 5px;height: 100%; z-index: 0;position:absolute; top: 0; left: 0};
   
    .rightbar-th ul li a{color:#ffffff !important;background:#3598dc !important;}

  /*  #style-3::-webkit-scrollbar-track{-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);background-color: #F5F5F5;}
    #style-3::-webkit-scrollbar{width: 6px;background-color: #F5F5F5;}
    #style-3::-webkit-scrollbar-thumb{background-color: #000000;} */
    .modal-dialog1{width:50%;margin:0px auto;}
</style>



<style type="text/css">
    .highlight {
        background:#f3f4f6;
        border:2px solid #17C4BB;
    }

    .modal-dialog {width: 90%;margin: 30px auto;}
    .modal .modal-header{background:#17C4BB;color:#fff;font-weight:bold;}
    .bold-text{font-weight:bold;width:15%;}
    .normal-text{font-weight:400;}
    .bold-text1{font-weight:bold;width:15%;color:#000;}
    .normal-text1{font-weight:400;color:#000;}
	
	
	#actionmodal .modal-body, #myModal .modal-body{height: 85vh; overflow: auto;}
	.modal-body .nav-tabs>li a b{font-weight:600 !important;}
	.modal-body .nav-tabs>li a{padding: 6px 5px !important;}
	.modal-body .bold-text1{font-weight:600 !important; font-size:13px; text-transform:capitalize !important;}
	.modal-body hr{margin:3px 0 !important;}
	.modal-body .table{margin-bottom:0 !important;}
	.modal-body .form-group{margin-bottom:6px !important;}
	.modal-body .form-group .control-label{font-size:11px !important;}
	.modal-header{padding: 6px 10px !important;}
	.modal .modal-header .close { margin-top: 8px !important; }

    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
        color: #ffffff !important;
        background-color: #17c4bb;
        border: 1px solid #ddd;
        border-bottom-color: transparent;
        cursor: default;
        .modal-header{padding:5px 10px !important;}
    }
	.portlet{margin-bottom:0;}
	.portlet.light{
		padding:0;
	}
	.portlet.light>.portlet-title {
		min-height: 35px;
		margin-bottom: 0;
		background:rgba(231, 233, 236, 0.62) !important;
		padding: 3px 8px;
	}
	.page-bar{background-color: transparent;}
	.portlet.light>.portlet-title>.caption, .btn#btn_eventSearch {
		padding: 6px 0 4px 0;
	}
	.btn#btn_eventSearch {
		padding: 4px 12px;
	}
	.portlet.light>.portlet-title>.caption>.caption-subject {
		font-size: 13px;
		text-transform: capitalize !important;
	}
	.form-control{
		padding: 4px 12px !important;
		height: 30px;
	}

</style>

<div class="modal fade" id="myModal">
    <div class="modal-dialog"  id="load-data">
		
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<div class="modal fade" id="actionmodal" role="dialog">
    <div class="modal-dialog1 modal-md" id="load-data-suppress">
      
    </div>
</div>


<div class="full-height-content full-height-content-scrollable" style=";">
    <div class="">
        <div class="row portlet light">
            <div class="col-md-12 portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"> Events Table </span>
                </div>
                <div class="page-bar form-inline">
					<div class="input-group add-on"> 
						<input class="form-control" placeholder="Search" name="eventSearch" id="eventSearch" type="text">
						<div class="input-group-btn">
							<button class="btn btn-default hidden-xs" id="btn_eventSearch" type="submit"><i class="glyphicon glyphicon-search"></i></button>
						</div>
					</div>
					<div class="chkdropdowns chkdropdowns-right ">
						<button class="btn btn-green dropdown-toggle color-cogs"><i class="fa fa-arrows" data-toggle="dropdown"></i></button>
						<ul class="column_sort_table">
							<section class="">
								<div class="column_sort_table_height containers">
									<input type = "hidden" name="row_order" id="row_order" />
									<table class="column_sort_table sticky-table topMoveable" id="sortable-row" style="border-spacing: 0;width:100%; border-collapse: collapse;" >
										<thead>
											<tr>
												<th>
													<div>Column sorter &nbsp <button class="btn btn-warning save-btn" id="btnSorting">Save</button></div>
												</th>    
											</tr>
										</thead>
										<tbody class="ui-sortable column_sort_body">
											<?php
											if ($eventColumn['status'] == true) {

												foreach ($eventColumn['resultSet'] as $value) {
													if ($value->column_id != 1) {
														?>
														<tr class="ui-sortable-handle">
															<td id=<?php echo $value->sort_id; ?>><i class="fa fa fa-arrows-v" aria-hidden="true"></i> <?php echo $value->rename_column ?></td>      
														</tr>
														<?php
													}
												}
											}
											?>
										</tbody>
									</table>
								</div>
							</section>														
						</ul>
					</div>	
					
					
					<div class="chkdropdowns chkdropdowns-right column-selecter">
						<button class="btn btn-green dropdown-toggle color-cogs">
							<i class="fa fa-cog" aria-hidden="true"></i>
						</button>
						<ul class="column_sort_table">
							<section class="">
								<div class="column_sort_table_height containers">
									<input type = "hidden" name="row_order" id="row_order" />
									<div id="columnSelector" class="columnSelector">
										<!-- this div is where the column selector is added -->
									</div>
								</div>
							</section>														
						</ul>
					</div>	
                </div>
            </div>    
			<div>	
				<div class="col-md-12" style="overflow:auto;padding-top:3px;float:left;" id="style-3">	
                    <table id="demo-table" class="eventTable tablesorter custom-popup bordered cellpadding-0 cellspacing-0 table table-bordered table-hover">
                        <thead>

                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <input type="hidden" id="trhighlight" value="">
            </div>           
            <div class="answer col-md-3" id="acknowledgeDiv" style="display:none;">

            </div>

            <div class="acknowledgeDetails col-md-3" id="acknowledgeDetails" style="display:none;">
                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line" style="background:#3598dc;color:#ffffff !important;text-align:center;">
                        <h4 class="list-title">Acknowledge</h4>
                        <div class="actions">                            
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="close fa fa-close"></i>                                
                            </a>
                        </div>                       
                    </div>
                    <div class="portlet-body">
                        <div class="mt-element-list">

                            <div class="mt-list-container list-news" style="height: 450px;overflow-y: scroll;">
                                <ul class="appendAckList">                                   

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="right_block col-md-3" style="display:none;">
                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line" style="background:#3598dc;color:#ffffff !important;">
                        <div class="actions">
                            <!-- <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a> -->
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="close fa fa-close"></i>
                            </a>
                        </div>
                        <ul class="nav nav-tabs content-tab rightbar-th">
                            <li class="active">
                                <a href="#portlet_tabs1" data-toggle="tab" style="color:#ffffff !important;">Host Details</a>
                            </li>
                            <li>
                                <a href="#portlet_tabs2" data-toggle="tab" style="color:#ffffff !important;">Event History</a>
                            </li>
                            <li>
                                <a href="#portlet_tabs3" data-toggle="tab" style="color:#ffffff !important;">Monitor Services</a>
                            </li>
                            <li>
                                <a href="#portlet_tabs4" data-toggle="tab" style="color:#ffffff !important;">Activity Log</a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content eventDetails" >	
                            <div class="hostDetails tab-pane active" id="portlet_tabs1" >

                            </div>
                            <div class="eventHistory tab-pane" id="portlet_tabs2">

                            </div>
                            <div class="monitorServices tab-pane" id="portlet_tabs3">								

                            </div>
                            <div class="activityLog tab-pane" id="portlet_tabs4">								

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- EVENT MONITOR STARTS HERE -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>

<script src="<?php echo base_url() . "public/js/" ?>events/js/rightbar.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_event.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/assets/" ?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/assets/" ?>global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/assets/" ?>global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/assets/" ?>global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/assets/" ?>pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<link href="<?php echo base_url() . "public/assets/" ?>global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() . "public/assets/" ?>global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . "public/js/" ?>events/js/TTalert.js"></script>
<script>
$(function()
{
        $('#style-3') .css({'height': (($(window).height()) - 126)+'px'});
    
        $(window).bind('resize', function(){
            $('#style-3') .css({'height': (($(window).height()) - 126)+'px'});
            //alert('resized');
        });
});
</script>