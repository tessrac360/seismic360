

<tr class="table-filter">    
    <?php
    if ($eventColumn['status'] == true) {

        foreach ($eventColumn['resultSet'] as $value) {
            if ($value->is_filter == 'Y') {
                $functionName = 'event_' . $value->filter_function;
                $functionVariableName = 'filter_' . $value->filter_function;
                $functionVariable = (eval('return $' . $functionVariableName . ';'));
                ?>
                <td><?php $functionName($functionVariable); ?></td>
                <?php
            } else {
                echo "<td></td>";
            }
        }
    }
    ?>
</tr>
<tr class=" dnd-moved color-scheme" class="table-filter">
    <?php
    if ($eventColumn['status'] == true) {
        foreach ($eventColumn['resultSet'] as $key=>$value) {
            ?>
            <th id='column-header-<?php echo $key?>' data-priority="<?php echo $key?>"><?php echo $value->rename_column ?></th>

            <?php
        }
    }
    ?>
</tr>
<?php echo base_url() . "public/js/" ?>
<script src='https://cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.js'></script>
<script src="<?php echo base_url() . "public/js/" ?>events/js/drag/dragndrop.table.columns.js" type="text/javascript"></script>	
<!-- Tablesorter: required -->
<script src="<?php echo base_url() . "public/js/" ?>events/js/jquery.tablesorter.js"></script>
<script src="<?php echo base_url() . "public/js/" ?>events/js/jquery.tablesorter.widgets.js"></script>
<script src="<?php echo base_url() . "public/js/" ?>events/js/widgets/widget-columnSelector.js"></script>
<script src="<?php echo base_url() . "public/js/" ?>events/js/column_filter.js"></script>	

