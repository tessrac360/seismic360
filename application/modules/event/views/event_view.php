<?php
$CI =& get_instance();
$baseurl = $CI->config->item("base_url"); 
$asset_url = $CI->config->item("asset_url");
?>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>View detail</title>    
		<style>
		
		.logo_img{position:relative; top:5px;}
		.main-header {    background: #367FA9;}
		.sidebar-mini.sidebar-collapse .navbar {
			margin-left: 50px !important;
		}

		.sidebar-mini.sidebar-collapse .main-sidebar{
			min-width: 50px !important;
		}

		

		.arrow_click{z-index:999;}

		.alerts
		{
			border-left: 1px solid #a8a8a8;
			border-radius: 5px;	
		}

		.sidebar-menu li.active > .treeview-menu {
			display: block;
			position: relative;
		}.sidebar-menu li {
			display: block;
			position: relative;
		}

		ul.treeview-menu:first-child a:nth-child(2){right:0px !important;}
		/*ul{padding-left:5px !important;} ul.treeview-menu{padding-left:3px !important;} */
		.text-dark-blue{color:#1B99D8;}

		.skin-blue .sidebar-menu>li:hover>a, .skin-blue .sidebar-menu>li.active>a{background:#222d32 !important;}
		.treeview{background:#222d32;}
		.skin-blue .sidebar-menu>li:hover>a, .skin-blue .sidebar-menu>li.active>a{border-left-color:transparent !important;}
		.sidebar-collapse .all_1{display:none !important;}
		.sidebar-menu .treeview-menu {padding:0;}
		.skin-blue .sidebar-menu > li > .treeview-menu {margin:0;}
		/* a.hover_effects:hover{background:none !Important;} 

		.skin-blue .sidebar-menu>li:hover>a, .skin-blue .sidebar-menu>li.active>a {background:none !Important;} */

		@-moz-document url-prefix() { 
			.sidebar-menu, .main-sidebar .user-panel, .sidebar-menu > li.header {
				white-space: normal !important;
			}
		}

		table.event_table tr td:first-child{text-align:left !important; font-size:15px;}
		table.event_table th{text-align:center; background: #00bbfc; font-size:14px;padding:0px;}
		table.event_table tr{height:10px;}
		table.event_table tr:nth-child(odd){background: #b8d1f3;}
		table.event_table tr:nth-child(even){background: #dae5f4;}
		table.event_table tr td{vertical-align:middle !important; font-size:13px; border-bottom: 1pt solid silver;padding:0px 5px 0px 5px;}
		/* .sidebar-collapse .sidebar-menu{min-width:230px;} */

		.sidebar-mini.sidebar-collapse .main-sidebar .user-panel>.info, .sidebar-mini.sidebar-collapse .sidebar-form, .sidebar-mini.sidebar-collapse .sidebar-menu>li>a>span, .sidebar-mini.sidebar-collapse .sidebar-menu>li>.treeview-menu, .sidebar-mini.sidebar-collapse .sidebar-menu>li>a>.pull-right, .sidebar-mini.sidebar-collapse .sidebar-menu li.header{
			min-width:315px !important;
			display:inline-block;
		}

		/* .sidebar-mini.sidebar-collapse .sidebar-menu li a.arrow_click{
			pointer-events: none;
		} */

		.treeview-menu .treeview .fa-angle-down{padding-right:7px !important;}


		.fa-bell{color:lightgreen;}
		.columnFilter{border: none;background-color: red;}
		
		.fa-filter{color:black;}
                
                .btn-box-tool {color: #ffffff !important;}

	</style>
	<style>
		.dropbtn {
			/*background-color: #4CAF50;
			color: white;*/
			padding: 0px;
			font-size: 14px;
			cursor: pointer;
			background-color: Transparent;
			outline:none;
		}
		.tablesorter {
			text-transform: none;	
		}

		/*
		.dropbtn:hover, .dropbtn:focus {
			background-color: #3e8e41;
		}*/	

		.dropdown {
			position: relative;
			display: inline-block;
			padding: 0px;
		}

		.dropdown-content {
			display: none;
			position: absolute;
			background-color: #f9f9f9;
			min-width: 160px;
			overflow: auto;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			text-align: left;
		}

		.chkbox {
			color: black;
			padding: 5px 5px 5px 10px;
			display: block;
			
		}

		.chkbox:hover {background-color: #f1f1f1}

		.show {display:block;}
	</style>

        <link href="<?php echo $baseurl;?>public/assets/viewdetail/bootstrap.min.css" rel="stylesheet" type="text/css">
        <!-- Font Awesome Icons -->
        <link href="<?php echo $baseurl;?>public/assets/viewdetail/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Ionicons -->
        <link href="<?php echo $baseurl;?>public/assets/viewdetail/ionicons.min.css" rel="stylesheet" type="text/css"> 
        <link href="<?php echo $baseurl;?>public/assets/viewdetail/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">    
        <link href="<?php echo $baseurl;?>public/assets/viewdetail/AdminLTE.min.css" rel="stylesheet" type="text/css">		
		<script src="<?php echo $baseurl;?>public/assets/viewdetail/jQuery-2.1.4.min.js.download"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="<?php echo $baseurl;?>public/assets/viewdetail/bootstrap.min.js.download" type="text/javascript"></script>       
        <script src="<?php echo $baseurl;?>public/assets/viewdetail/app.min.js.download" type="text/javascript"></script>       
        <script src="<?php echo $baseurl;?>public/assets/viewdetail/moment.min.js.download" type="text/javascript"></script>       
        <script src="<?php echo $baseurl;?>public/assets/viewdetail/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>      
        <script src="<?php echo $baseurl;?>public/assets/viewdetail/ajaxTicket.js.download" type="text/javascript"></script>      
        <script src="<?php echo $baseurl;?>public/assets/viewdetail/script.js.download" type="text/javascript"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">   
		<script>
		//var baseurl = 'http://10.10.32.52/eventedge/index.php/';
		var dftEvent = '2795782';      
			   
		function activityTypeChanged()
		{
			
			if($('#activity_type').val() == 'RCA'){
				
				document.getElementById('RCA').style.display = 'block';
				getRCACategory($('#activity_scope').val());
			} else {
				
				document.getElementById('RCA').style.display = 'none';
			}
			
			document.getElementById("activity_category1").disabled = true;
			document.getElementById("activity_category2").disabled = true;
			document.getElementById("activity_category3").disabled = true;
		}

		function activityScopeChanged(id)
		{
			if(id==0){
				
				document.getElementById("activity_category1").selectedIndex = 0;
				document.getElementById("activity_category2").selectedIndex = 0;
				document.getElementById("activity_category3").selectedIndex = 0;
				while (document.getElementById("activity_category1").options.length > 1) {
					
					document.getElementById("activity_category1").options[document.getElementById("activity_category1").options.length - 1] =null;
				}
				
				while (document.getElementById("activity_category2").options.length > 1) {
					
					document.getElementById("activity_category2").options[document.getElementById("activity_category2").options.length - 1] =null;
				}
				while (document.getElementById("activity_category3").options.length > 1) {
					
					document.getElementById("activity_category3").options[document.getElementById("activity_category3").options.length - 1] =null;
				}	
				
				document.getElementById("activity_category1").disabled = true;
				document.getElementById("activity_category2").disabled = true;
				document.getElementById("activity_category3").disabled = true;
				
				getRCACategory($('#activity_scope').val());
			}
			if(id==1){
				
				
				document.getElementById("activity_category2").selectedIndex = 0;
				document.getElementById("activity_category3").selectedIndex = 0;
				while (document.getElementById("activity_category2").options.length > 1) {
					
					document.getElementById("activity_category2").options[document.getElementById("activity_category2").options.length - 1] =null;
					
				}
				while (document.getElementById("activity_category3").options.length > 1) {
					
					document.getElementById("activity_category3").options[document.getElementById("activity_category3").options.length - 1] =null;
					
				}
				
				document.getElementById("activity_category2").disabled = true;
				document.getElementById("activity_category3").disabled = true;
				
				if($('#activity_category1').val() !=""){
					
					getSubRCACategory(id,$('#activity_category1').val());
				}		
				
			}
			
			if(id==2){
				
				document.getElementById("activity_category3").selectedIndex = 0;
				while (document.getElementById("activity_category3").options.length > 1) {
					
					document.getElementById("activity_category3").options[document.getElementById("activity_category3").options.length - 1] =null;
					
				}
				document.getElementById("activity_category3").disabled = true;
				if($('#activity_category2').val() !=""){
					
					getSubRCACategory(id,$('#activity_category2').val());
				}
			}
		}




		function placeCall(phoneNumber, ticket, escLevel, jobDesc, userName, profileID)
		{
			
			$.ajax({
					url: baseurl+'events/connectCall?m='+phoneNumber+'&e='+ticket+'&le='+escLevel+'&jd='+jobDesc,
					type: "POST",
					success: function(uid){
						
						showAllUserEvents(uid, phoneNumber, ticket, escLevel, jobDesc, userName, profileID);
					}
			});
		}

		function downloadfile(url)
		{
			
			document.getElementById('my_iframe').src = url;
			//alert(url);		
		}

		function updateEvents(uid, escLevel)
		{
			var events = document.getElementsByName('flt-th-selected')[0].value
			if(events != ''){
				
				$.ajax({
					url: baseurl+'events/updateEventsActivity?events='+events+'&uid='+uid+'&level='+escLevel,
					type: "POST",
					success: function(data){
						
						window.close();
					}
				});
			} else {
				
				alert("Select at least one event to save");
			}
			
				
		}


		function getSubRCACategory(catType,subBkt)
		{
			//alert(subBkt);
			$.ajax({
				type: "POST",
				url: baseurl+'events/getSubRCACategory',
				dataType:'json',
				data:{catType:catType,subBkt:subBkt},
				success: function(data) { 
					catTemp = catType +1;
					var selElement = document.getElementById("activity_category"+catTemp);
					//alert(data);
					for (var key in data){
						
						var option = document.createElement("option");
						option.text = data[key];
						option.value = key;
						selElement.add(option);
					}	
					if (document.getElementById("activity_category"+catTemp).options.length > 1) {
					
						document.getElementById("activity_category"+catTemp).disabled = false;
					}
				}
			});	
		}


		function getRCACategory(subBkt)
		{
			//alert(subBkt);
			$.ajax({
				type: "POST",
				url: baseurl+'events/getMainRCA',
				dataType:'json',
				data:{subBkt:subBkt},
				success: function(data) { 
					
					var selElement = document.getElementById("activity_category1");
					//alert(data);
					for (var key in data){
						
						var option = document.createElement("option");
						option.text = data[key];
						option.value = key;
						selElement.add(option);
					}	
					if (document.getElementById("activity_category1").options.length > 1) {
					
						document.getElementById("activity_category1").disabled = false;
					}
				}
			});	
		}
				
		function addRCA()
		{
			
			document.getElementById("err_msg").innerHTML = '';
			var event_id = $('#event_id').val();
			var event_form = $('#event_form').val();
			var activity_created_time = $('#activity_created_time').val();
			var activity_type = $('#activity_type').val();
			var activity_mode = $('#activity_mode').val();
			var activity_start_time = $('#activity_start_time').val();
			var activity_end_time = $('#activity_end_time').val();
			var activity_description = $('#activity_description').val();
			var activity_scope = $('#activity_scope').val();
			var activity_category1 = $('#activity_category1').val();
			if(activity_category1.trim() != ""){
				activity_category1 = document.getElementById("activity_category1").options[document.getElementById("activity_category1").selectedIndex].text
			}
			var activity_category2 = $('#activity_category2').val();
			if(activity_category2.trim() != ""){
				activity_category2 = document.getElementById("activity_category2").options[document.getElementById("activity_category2").selectedIndex].text
			}
			var activity_category3 = $('#activity_category3').val();
			if(activity_category3.trim() != ""){
				activity_category3 = document.getElementById("activity_category3").options[document.getElementById("activity_category3").selectedIndex].text
			}
			if(activity_type.trim() != "" && activity_mode.trim() != ""  && activity_start_time.trim() != "" && activity_end_time.trim() != "" )
			{
				var flagSave = true;
				if(activity_type.trim() == "RCA" && activity_scope.trim() == "Infra" && activity_category1.trim() == ""){
					
					flagSave = false;
					document.getElementById("err_msg").innerHTML = '*** Select a valid reason for <b>Category1</b>';
				}
				if(activity_type.trim() == "Activity Log" && activity_description.trim() == "" ){
					
					flagSave = false;
					document.getElementById("err_msg").innerHTML = "*** Cann't leave <b>Description</b> blank for Activity Log";
				}
				
				if (flagSave){
					
					$.ajax({
						
						url: baseurl+'events/saveActivity/',
						type: "POST",
						data:  {event_id:event_id,activity_created_time:activity_created_time,activity_type:activity_type,activity_mode:activity_mode,activity_start_time:activity_start_time,activity_end_time:activity_end_time,activity_description:activity_description,activity_category1:activity_category1,activity_category2:activity_category2,activity_category3:activity_category3,activity_scope:activity_scope, event_form:event_form},
						
						success: function(strRCA){
							/*if(strRCA != ''){
								window.opener.document.getElementById("rca_"+event_id).innerHTML = strRCA;
							}*/
							window.location.reload();	
						}
				   });	
				} 
				
			}else
			{
				
				document.getElementById("err_msg").innerHTML = "*** please fill all mandatory fields";
			}		
		}

		function showAllUserEvents(uid, phoneNumber, ticket, escLevel, jobDesc, userName, profileID)
		{
			url = baseurl+'events/userEvents?mob='+phoneNumber+'&uid='+uid+'&ticket='+ticket+'&level='+escLevel+'&un='+userName+'&job='+jobDesc+'&proID='+profileID;
			newwindow = window.open(url, "Event Details", "width=auto,height=auto,scrollbars=yes,resizable=yes");
			if(window.focus)
			{
				newwindow.focus();
			}
			return false;
		}

		function viewdetail(event_id)
		{
			$('#tr'+event_id).css("font-weight", "normal");
			url = baseurl+'events/viewdetail?u='+event_id;
			newwindow = window.open(url, "Event Details", "width=auto,height=auto,scrollbars=yes,resizable=yes");
			if(window.focus)
			{
				newwindow.focus();
			}
			return false;
		}

		function filterRows()
		{
		/*
		will filter the rows in table based on seleted items for that column
		*/	
			var eleP = document.getElementsByName('th-priority');
			var eleD = document.getElementsByName('th-dgstatus');
			var eleA = document.getElementsByName('th-alarmtext');
			
			var rows = $('tbody tr');
				
			i=0;
			for (i = 0; i < rows.length; i++) {
				
				var row = rows[i];
				var bolShow = false;
				var k;
				var strtemp ="";
				var bolP = false;
				for (k = 0; k < eleP.length; k++) {
					
					if(eleP[k].checked){

						if(eleP[k].value ==  row.getElementsByClassName('priority')[0].innerHTML){ bolP = true; strtemp += "P-"+row.getElementsByClassName('priority')[0].innerHTML; }
					}
				} 
				k=0;
				var bolD = false;
				for (k = 0; k < eleD.length; k++) {
					
					if(eleD[k].checked){

						if(eleD[k].value ==  row.getElementsByClassName('DGStatus')[0].innerHTML){ bolD = true; strtemp +=  " D-"+  row.getElementsByClassName('DGStatus')[0].innerHTML}
					}
				} 
				k=0;
				var bolA = false;
				for (k = 0; k < eleA.length; k++) {
					
					if(eleA[k].checked){

						if(eleA[k].value ==  row.getElementsByClassName('AlarmText')[0].innerHTML){ bolA = true; strtemp +=  " A-"+row.getElementsByClassName('AlarmText')[0].innerHTML}
					}
				} 
				
				//alert( strtemp);
				if(bolP && bolD && bolA ){
					
					$('tr#'+row.getAttribute("id")).show();
				} else {
					
					$('tr#'+row.getAttribute("id")).hide();
				}
				
			}
		}

		window.onclick = function(event) {
			
		  if (!event.target.matches('.fa-filter') && !event.target.matches('.dropbtn') && !event.target.matches('.chkbox') && !event.target.matches('.chk') ) {

			var dropdowns = document.getElementsByClassName("dropdown-content");
			var i;
			for (i = 0; i < dropdowns.length; i++) {
			  var openDropdown = dropdowns[i];
			  if (openDropdown.classList.contains('show')) {
				openDropdown.classList.remove('show');
			  }
			}
			//searchby();
		  } 
		  if (event.target.matches('.dropbtn') ) {
			  
			 //alert(event.target.id);
			 document.getElementById(event.target.id+"-DD").classList.toggle("show");
		  }
		  if (event.target.matches('.fa-filter') ) {
			  
			 //alert(event.target.id);
			 document.getElementById('th-'+event.target.id+"-DD").classList.toggle("show");
		  }
		 
		  if (event.target.matches('.chkbox') ) {
			 
				var chkEle = document.getElementById("chk-"+document.getElementById(event.target.id).getAttribute("id"));
				if(chkEle.checked){
					
					chkEle.checked = false;
				} else {
					
					chkEle.checked = true;
				}
				var chkboxes = document.getElementsByName(chkEle.name);
				
				if((document.getElementsByName(chkEle.name)[0].id == chkEle.id)){
					
					var i;
					for (i = 1; i < chkboxes.length; i++) {
					 
						chkboxes[i].checked = chkboxes[0].checked;
					} 
					if(chkEle.checked){
						
						document.getElementsByName('ico-'+chkEle.name)[0].style.color = 'black';
					} else {
						
						chkboxes[1].checked = true;
						document.getElementsByName('ico-'+chkEle.name)[0].style.color = 'orange';
					} 
				} else {
					
					var chkCount =0;
					for (i = 1; i < chkboxes.length; i++) {
					  
						if (chkboxes[i].checked){
							
							chkCount++;
						}
					}
					if(chkCount == (chkboxes.length - 1)){
						
						//document.getElementsById('ico-'+chkEle.name).style.color = "black";
						document.getElementsByName('ico-'+chkEle.name)[0].style.color = 'black';
						chkboxes[0].checked = true;
					} else {
						
						chkboxes[0].checked = false;
						if(chkCount == 0){
							
							chkEle.checked =true;
						}
						document.getElementsByName('ico-'+chkEle.name)[0].style.color = 'orange';
					}
				} 
				filterRows();
		   }
		   
		   if (event.target.matches('.chk') ) {
			   
			   //var chkEle = document.getElementById(event.target.id).getAttribute("id");
			  // alert(event.target.checked);
			   var chkboxes = document.getElementsByName(event.target.name);
				
				if((chkboxes[0].id == event.target.id)){
					
					var i;
					for (i = 1; i < chkboxes.length; i++) {
					 
						chkboxes[i].checked = event.target.checked;
					} 
					if(event.target.checked){
						
						document.getElementsByName('ico-'+event.target.name)[0].style.color = 'black';
					} else {
						
						chkboxes[1].checked = true;
						document.getElementsByName('ico-'+event.target.name)[0].style.color = 'orange';
					}
					
				} else {
					
					var chkCount =0;
					for (i = 1; i < chkboxes.length; i++) {
					  
						if (chkboxes[i].checked){
							
							chkCount++;
						}
					}
					if(chkCount == (chkboxes.length - 1)){
						
						chkboxes[0].checked = true;
						document.getElementsByName('ico-'+event.target.name)[0].style.color = 'black';
					} else {
						
						chkboxes[0].checked = false;
						if(chkCount == 0){
							
							event.target.checked =true;
						}
						document.getElementsByName('ico-'+event.target.name)[0].style.color = 'orange';
					}
					
				}
				//alert(document.getElementsByName('flt-'+event.target.name)[0].value); 
				//searchby();
				filterRows();
		   }
		   
		   
			if (event.target.matches('.chk-selected') ) {
			   
				//var chkEle = document.getElementById(event.target.id).getAttribute("id");
				// alert(event.target.checked);
				var chkboxes = document.getElementsByName(event.target.name);
				
				if((event.target.name+'-0' == event.target.id)){
					
					var i;
					var chkValue = "";
					for (i = 1; i < chkboxes.length; i++) {
					 
						chkboxes[i].checked = event.target.checked;
						if(event.target.checked){
							if(chkValue == ""){
								
								chkValue = chkboxes[i].value;
							} else {
								
								chkValue += ", "+chkboxes[i].value;
							}					
						} 
					} 
											
					document.getElementsByName('flt-th-selected')[0].value = chkValue;
				} else {
					
					var chkCount =0;
					var chkValue = "";
					for (i = 1; i < chkboxes.length; i++) {
					  
						if (chkboxes[i].checked){
							
							chkCount++;
							if(chkValue == ""){
								
								chkValue = chkboxes[i].value;
							} else {
								
								chkValue += ", "+chkboxes[i].value;
							}
						}
					}
					document.getElementsByName('flt-th-selected')[0].value = chkValue;
					if(chkCount == (chkboxes.length - 1)){
						
						chkboxes[0].checked = true;
						
					} else {
						
						chkboxes[0].checked = false;
						
					}
				}
				
				//searchby();
			}
		}

		</script>
		
	</head> 
    <body class="register-page" style="overflow:auto">
    	<div class="">
            <div class="">
                <style>

.box_margin
{
	margin-bottom:3px !important;	
}
.dev_side
{
	padding-left:0px !important;
	padding-right:0px !important;	
}
.dev_side1
{
	padding-left:0px !important;
	padding-right:6px !important;	
}
h4
{
	margin:5px 0px -10px 13px !important;
}
.box-header
{
	padding:0px !important;	
}
</style>
<script>
function openActPop()
{
	
	$.ajax({
			url: baseurl+'events/openActPop/',
			type: "POST",
			data: {event_id:2795782,mode:"",event_form:'events'},
			success: function(result){
				$('#activity').html(result);
			}
	});
}


window.onload = function() {
	
	//alert("Hi");
	var chkboxes = document.getElementsByClassName("chk");
	//alert(chkboxes.length);
	for (i = 0; i < chkboxes.length; i++) {
			 
		chkboxes[i].checked = true;
	} 
		
}

</script>
<section class="content" style="padding:3px 20px 5px 19px !important">
<iframe id="my_iframe" style="display:none;" src="./viewdetail_files/saved_resource.html"></iframe>
<!--<div class="row">
	
	<div class="col-md-12" style="background: #00bbfc;font-size: 18px" >
		<div class="col-md-12" style="height:5px"></div>
    	<div class="col-md-4"><b>Client :&nbsp;&nbsp;&nbsp;</b>inspiredge </div>
   		<div class="col-md-4"><b>Host :&nbsp;&nbsp;&nbsp;</b>dcca</div>
    	<!--<div class="col-md-4"><b>Site Name :&nbsp;&nbsp;&nbsp;</b>dcca</div>-->
        <!--<div class="col-md-4"><b>Ticket No :&nbsp;&nbsp;&nbsp;</b>2795782</div>
		<div class="col-md-12" style="height:5px"></div>
		
	</div>
</div>-->

<div class="row">
    <div class="col-md-3 dev_side1">
      <div class="box box_margin">
      	<div class="box-header" style="background: #17C4BB;color:#ffffff;">
            <h4>Host Details
                <button class="btn btn-box-tool pull-right" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </h4>
         </div>
        
         <div class="box-body" style="height: 400px; overflow: scroll; display: block;">
            <table class="table">
             <tbody>
                <tr><th>Client :</th><td><?php echo $events['resultSet']['client_title'];?></td></tr>
                <tr><th>Host name :</th><td><?php echo $events['resultSet']['device_name'];?></td></tr>
                
             </tbody>
          </table>
        </div>
      </div>
	</div>
    
    <div class="col-md-9 dev_side">
            <div class="box box_margin">
             <div class="box-header" style="background: #17C4BB;color:#ffffff;">
                <h4>Event History
                    <button class="btn btn-box-tool pull-right" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </h4>
             </div>	
             <div class="box-body" style="height:180px;overflow:scroll;">
                 <table class="table table-bordered dataTable">
                    <thead>
                    	<tr>
							
					            <th>Host Name </th>
                                <th>Service</th>
                                <th>Event Text</th>
                                <th>Severity</th>
                                <th>Start Time</th>
                                <th>Duration</th>
                                                
						</tr>
						
					</thead>
                    <tbody>
                      	
						<?php foreach($eventsHistory['resultSet'] as $historyData){ ?>
						<tr>
					            <td><?php echo $historyData['device_name']?></td>
                                <td><?php echo $historyData['service_name']?></td>
                                <td><?php echo $historyData['event_text']?></td>
                                <td><?php echo $historyData['severity']?></td>
                                <td><?php echo $historyData['event_start_time']?></td>
                                <td><?php echo $historyData['Duration']?></td>
                                                
						</tr>  
						<?php }?>	
                    </tbody>
                </table>
            </div>
          </div>
		<div class="box box_margin">
			
			<div class="box-header" style="background: #17C4BB;color:#ffffff;">
				<h4>Monitor Services
					<button class="btn btn-box-tool pull-right" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</h4>
			</div>
			<div class="box-body" style="height:180px;overflow:scroll;">
				<table class="table table-bordered table-striped dataTable">
					<thead>
						<tr>
							
                                <th>Service</th>
                                <th>Event Text</th>
                                <th>Severity</th>
                                <th>Start Time</th>
                                <th>Duration</th>								
                                                
						</tr>
					</thead>
                    <tbody>
                      	
						<?php foreach($eventMonitorService['resultSet'] as $historyData){ ?>
						<tr>
	
                                <td><?php echo $historyData['service_name']?></td>
                                <td><?php echo $historyData['event_text']?></td>
                                <td><?php echo $historyData['severity']?></td>
                                <td><?php echo $historyData['event_start_time']?></td>
                                <td><?php echo $historyData['Duration']?></td>								

                                                
						</tr>  
						<?php }?>	
                    </tbody>					
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row"> 
	<div class="col-md-12 dev_side">
		<div class="box box_margin">
			<div class="box-header" style="background: #17C4BB;color:#ffffff;">
				<h4>Activity Log
					<button class="btn btn-box-tool pull-right" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<!-- <button class="btn btn-primary  pull-right" onclick="openActPop()" id="newAct" data-toggle="modal" data-target="#myModal">New Activity</button> -->
				</h4>
			</div>
			<div class="box-body" style="height:180px;overflow:scroll;">
				<table class="table table-bordered table-striped dataTable">
					<thead>
						<tr>
							<th>Activity No.</th>
							<th>Created Time</th>
							<th>Type</th>
							<th>Mode</th>
							<th>Status</th>
							<th>Description</th>
							<th>Start Time</th>
							<th>End Time</th>
							<th>Duration</th>
							<th>Activity Owner</th>
							<th>Call Recording</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div> 
</div>

</section>
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background: #00bbfc;font-size: 18px">
				  <button type="button" class="close" data-dismiss="modal">×</button>
				  <h4 class="modal-title">Add New Activity</h4>
				</div>
				<div class="modal-body" id="activity">
					   
				</div>
				 
			   <div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="addRCA()">Save</button>
			   </div>
			</div>
		</div>
	</div>
    </div>
    </div>
    
</body>
</html>