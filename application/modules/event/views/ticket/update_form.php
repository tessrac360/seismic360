<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style>
.tabbable-custom{margin-bottom:0;}
.tabbable-custom>.tab-content{padding:5px;}
.form-control[readonly] {  background:rgba(245, 245, 245, 0.7) !important; }
.custom-formgroup-ins{position:relative; top:-32px;}
	.amcharts-title{color:#000; fill:black;}
	.amcharts-title{
  transform: translate(145px,30px)
}
.amcharts-title:nth-child(2){
  transform: translate(160px, 150px)
}
	
		@media (min-width: 992px){
		.page-footer {
			background-color: #fff !important;
		}
		.page-content-wrapper .page-content{
			padding:0;
		}
		}
		.page-title {
			font-size: 17px;
			background: rgba(204, 204, 204, 0.22);
			padding: 6px 9px;
			margin: 0 0 10px;
		}
		
		@media (min-width: 992px){
			.mar-lr3{margin-left:1px !important; margin-right:1px !important;}
			.pad-tlr-3{padding-top:3px !important; padding-left:3px; padding-right:3px;}
			.remove_pad{padding:0 !important;}
			.remove_pad_left{padding-left:0;}
			.remove_pad_right{padding-right:0px;}
			.pad_right3{padding-right:3px !important;}
			}
		
		.mar-bottom{margin-bottom:0;}
		
		
		.tickets_data.col-md-10{padding-left:0;}

.portlet>.portlet-title>.caption {
    float: left;
    display: inline-block;
    line-height: 17px;
    padding: 6px 0 4px 0;
}

.portlet.light>.portlet-title.portlet-bg-title {
    min-height: 34px;
    margin-bottom: 6px;
    background: #17c4bb !important;
    padding: 3px 4px 3px 7px;
}

.page-bar {
    padding: 0px;
	float: right;
	background-color: transparent;
margin-bottom:0;
}

.portlet.light>.portlet-title>.caption>.caption-subject { font-size: 13px; text-transform: capitalize !important;     font-weight: 600 !important;
    color: #fff; }
.portlet.light>.portlet-title>.caption{padding: 3px 0;}

.form-control{height: 26px; font-size: 13px}
.tickets_data_header .btn-default.btn-search{padding: 4px 8px 3px 8px !important; font-size: 12px;margin-right: 0 !important;}
.tickets_data_header .btn.btn-toggle{    padding:1px 5px 1px 5px; font-size: 12px; background: #fff; border: 0; margin-right:6px; position:relative; top:-1px;}
.padd8{padding:8px;}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
        color: #ffffff !important;
        background-color: #17c4bb !important;
        border: 1px solid #ddd;
        border-bottom-color: transparent;
        cursor: default;
        .modal-header{padding:5px 10px !important;}
    }

.redarrow { color: red; }
.custom-padding-formgp hr{margin:5px 0;}
.custom-padding-formgp .form-group{margin-bottom:4px;}
.custom-padding-formgp .form-group .form-control { border-radius: 0; padding: 3px 6px; height: 30px;     background: #fff;}
.custom-padding-formgp .form-group textarea.form-control { height:auto;}
.custom-padding-formgp .form-group .col-sm-8 { padding-right:0; }
.custom-padding-formgp .form-group .col-sm-4 { padding-left: 0; font-size: 12px; line-height: 30px; margin-bottom: 0; text-align:right;}
.custom-padding-formgp .form-group .mt-checkbox-inline, .custom-padding-formgp .form-group .mt-radio-inline { padding: 5px 0;}  
.custom-padding-formgp .form-group .mt-checkbox, .custom-padding-formgp .form-group .mt-radio {margin-bottom: 0; font-size:13px;}  

.custom-padding-formgp .nav>li>a {padding: 5px 15px; font-size:13px; font-weight:400 !important;}
.btn-save{padding: 4px 12px; margin-left:4px; border:0;font-size: 12px; position:relative; top:1px;}

.tab-content{height:208px;overflow-y:auto; font-size:13px;transition: all 0.5s;}
.tab-content .bold-text{font-weight:400;}
.tabbable-custom>.nav-tabs>li.active>a:hover, .nav-tabs>li.active>a{border-color: #d4d4d4 #transparent !important;background: #fff !important; color: #000 !important;}	
.tabbable-custom .nav-tabs>li.active>a, .tabbable-custom  .nav-tabs>li.active>a:focus, .tabbable-custom  .nav-tabs>li.active>a:hover{color:#000 !important; background:#fff !important;}
.page_title_sub{font-size: 14px; font-weight:600; background: rgba(204, 204, 204, 0.22); padding: 4px 9px; margin: 0 0 3px;}
	</style>

	<style type="text/css">
    .highlight {
        background:#f3f4f6;
        border:2px solid #17C4BB;
    }

    .modal-dialog {width: 90%;margin: 30px auto;}
    .modal .modal-header{background:#17C4BB;color:#fff;font-weight:bold;}
    .bold-text{font-weight:bold;width:15%;}
    .normal-text{font-weight:400;}
    .bold-text1{font-weight:bold;width:15%;color:#000;}
    .normal-text1{font-weight:400;color:#000;}
	
	
	#actionmodal .modal-body, #myModal .modal-body{height: 85vh; overflow: auto;}
	.modal-body .nav-tabs>li a b{font-weight:600 !important;}
	.modal-body .nav-tabs>li a{padding: 6px 5px !important;}
	.modal-body .bold-text1{font-weight:600 !important; font-size:13px; text-transform:capitalize !important;}
	.modal-body hr{margin:3px 0 !important;}
	.modal-body .table{margin-bottom:0 !important;}
	.modal-body .form-group{margin-bottom:6px !important;}
	.modal-body .form-group .control-label{font-size:12px !important;}
	.modal-header{padding: 6px 10px !important;}
	.modal .modal-header .close { margin-top: 8px !important; }

    
	.portlet{margin-bottom:0;}
	.portlet.light{
		padding:0;
	}
	.portlet.light>.portlet-title {
		min-height: 35px;
		margin-bottom: 0;
		background:rgba(231, 233, 236, 0.62) !important;
		padding: 3px 8px;
	}
	.page-bar{background-color: transparent;}
	.portlet.light>.portlet-title>.caption, .btn#btn_eventSearch {
		padding: 6px 0 4px 0;
	}
	.btn#btn_eventSearch {
		padding: 4px 12px;
	}
	.portlet.light>.portlet-title>.caption>.caption-subject {
		font-size: 13px;
		text-transform: capitalize !important;
	}
	.form-control{
		padding: 4px 12px !important;
		height: 30px;
	}
	
	#myModal .nav-tabs>li:last-child{display:none;}
	.assign .input-group-addon{
		border-left: 0 !important;
		background: #17c4bb !important;
		color: #fff !important;
		border: 0 !important;
		cursor: pointer;
	}
	.assign .input-group-addon i{color:#fff;}
	.assign .input-group{ width: 100.2%;}
	.shn_desc{border:1px solid #ccc; background:#fbfcfd; min-height:100%; padding:10px; overflow-x:auto;}
	.shn_btn a{display:block;}
	.shn_btn a button{margin-bottom:3px; display:block; text-align:left !important; position: relative; border-radius:15px !important; background:#99e4e0;}
	.btn-label {border-radius:0 15px 15px 0 !IMPORTANT; position: absolute; right: 0; top: 0; display: inline-block;  padding: 6px 12px; background: rgba(0,0,0,0.15); border-radius: 3px 0 0 3px; background:#17c4bb; color:#fff;}
	.shn_btn a button:hover{background:#17c4bb; color:#fff;}
	.description{min-height:200px; width:100%;}
	#cke_1_contents.cke_contents.cke_reset{min-height:167px; height:auto;}
	.cke_top, .cke_bottom{display:none;}
	#myModal .nav-tabs>li:last-child {display: block;}
</style>
<style>
<!-- Activity Log Description column CSS -->	
	
div.text-container {
    margin: 0 auto;
    width: 75%;    
}

.hideContent {
    overflow: hidden;
    line-height: 1em;
    height: 2em;
}

.showContent {
    line-height: 1em;
    height: auto;
}
.showContent{
    height: auto;
}

h1 {
    font-size: 24px;        
}

.show-more {
    padding: 3px 0 0px 0;
    text-align: left;
}

td {
    width:200px;
    border: solid 1px;
}



</style>
<link href="<?php echo base_url() . "public/" ?>datapicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<div>
	<h1 class="page-title" style="margin-bottom:5px;"> Incidents </h1>
 <div class="alert alert-danger alert-dismissable fade in not_updated" style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Error!</strong> Please select an Engineer to assign an Incident.
  </div>
  <div class="alert alert-danger alert-dismissable fade in not_updated1" style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Error!</strong> Please change the status.
  </div>
	<div class="full-height-content-body">
		<div class="row">
			<div class="tickets_tabs col-md-2 pad_right3" style="display:none;">
				<div class="portlet light bordered full_height_block mar-bottom remove_pad">
					
				</div>
			</div>
			<div class="col-md-12 parent">
				<div class="portlet light bordered full_height_block mar-bottom remove_pad tickets_data ">
					<div class="col-md-12 portlet-title portlet-bg-title tickets_data_header">
						<div class="caption font-dark">
							<span class="caption-subject bold uppercase">
								<!--<button class="rightbar btn btn-default btn-toggle"><i class="fa fa-bars" aria-hidden="true"></i></button>-->
							<?php echo $ticketDetails['resultSet']['incident_id']?></span>
						</div>
					<form action="<?php echo base_url('event/ticket/editTicket/'.encode_url($ticketDetails['resultSet']['ticket_id']));?>" method="post" onsubmit="return submitResult();">
						<div class="page-bar form-inline">
							<div class="input-group add-on"> 
								<button type="submit" class="btn btn-default btn-save" id="update">Update</button>
								<a class="accordion-toggle btn btn-default btn-save" data-toggle="collapse" data-parent="#accordion" href="#incpanel1">
									<i class="glyphicon glyphicon glyphicon-chevron-up"></i>
								</a>
								<!--<a href="http://10.10.32.35:8080/guacamole/"><button class="btn btn-default btn-save">Login</button></a>
								<button class="btn btn-default btn-save">Run Auto Script</button>-->
							</div>
						</div>
					</div>
					
					<!-- Tickets Form -->
					<div class="portlet-body ticket_form_data remove_pad">
					<div class="tickets_data1" style="">
						
							<div class="row mar-lr3 custom-padding-formgp">
							
							<div id="incpanel1" class="panel-collapse collapse in" aria-expanded="true" style="">
								<div class="form-group col-md-4">
									<label for="incident_id" class="col-sm-4 control-label">Incident Number <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="incident_id" name="" value="<?php echo $ticketDetails['resultSet']['incident_id']?>" readonly>
									</div>
								</div>
								
								<div class="form-group col-md-4">
									<label for="client_id" class="col-sm-4 control-label">Partner <span class="redarrow"></span></label>
									<div class="col-sm-8">										
										<input type="text" class="form-control" id="partner" name="partner" value="<?php echo $ticketDetails['resultSet']['partner']?>" readonly>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="client_id" class="col-sm-4 control-label">Client <span class="redarrow"></span></label>
									<div class="col-sm-8">										
										<select class="form-control Clients check_has_error" id="client" name="client" data-validation="required" >
										<option></option>
									   </select>
									</div>
								</div>
								
								<div class="form-group col-md-4">
									<label for="requestor" class="col-sm-4 control-label">Requestor <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="requestor" name="" value="<?php echo $ticketDetails['resultSet']['requestor']?>" readonly>
									</div>
								</div>
								
								
								<div class="form-group col-md-4">
									<label for="client_id" class="col-sm-4 control-label">Location <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<select class="form-control Location check_has_error" id="Location" name="location" data-validation="required" >
										<option></option>
									   </select>										
									</div>
								</div>
								
								<div class="form-group col-md-4">
									<label for="status_code" class="col-sm-4 control-label">Status </label>
									<div class="col-sm-8">
										<select class="form-control V_Combo" id="status_code" name="status_code" tabindex="9" onChange="assginUser(this.value)" <?php if($NoOfDays['status'] == 'true'){if($NoOfDays['resultSet']['days'] > 7){echo "disabled";}}	//if($ticketDetails['resultSet']['status_code']== 7){ echo "disabled";} ?>>
											<?php						
												$check = access($ticketDetails['resultSet']['status_code']); 
												if ($ticketState['status'] == 'true') {
													foreach ($ticketState['resultSet'] as $value) {
														if (in_array($value['status_code'], $check)){ 
														?>
															<option value="<?php echo $value['status_code']; ?>" <?php //echo ($ticketDetails['resultSet']['status_code'] == 7)?'disabled':''; ?> <?php echo ($ticketDetails['resultSet']['status_code'] == $value['status_code'] )?'selected':''; ?> ><?php echo $value['status']; ?></option>  
														<?php
														}
													}
												}
											?> 
										</select>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="owner_id" class="col-sm-4 control-label">Devices <span class="redarrow"></span></label>
									<div class="col-sm-8"> 
										<select class="form-control Device check_has_error" id="Device" name="device" data-validation="required" onchange="getCategories(this.value)" >
										<option></option>
									   </select>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="owner_id" class="col-sm-4 control-label">Service <span class="redarrow"></span></label>
									<div class="col-sm-8"> 
										<select class="form-control serivces check_has_error" id="serivces" name="service" data-validation="required" >
										<option></option>
									   </select>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="owner_id" class="col-sm-4 control-label">Categories <span class="redarrow"></span></label>
									<div class="col-sm-8"> 
										<select class="form-control Category check_has_error" id="Category" name="category" data-validation="required" onchange="getSubCategories(this.value)" >
										<option></option>
									   </select>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="event_id" class="col-sm-4 control-label">Sub Category <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<select class="form-control subCategory check_has_error" id="subCategory" name="subcategory" data-validation="required" >
										<option></option>
									   </select>										
									</div>
								</div>								
								
								<div class="form-group col-md-4 assign">
											<label for="created_by" class="col-sm-4 control-label">Assign To <span class="redarrow"></span></label>
											<div class="col-sm-8">
												<div class="input-group">
													<input type="text" class="form-control m-input" id="txtName" value="<?php echo (isset($ticketDetails['resultSet']['assigned']) && !empty($ticketDetails['resultSet']['assigned']))?$ticketDetails['resultSet']['assigned']:"";?>" readonly="readonly" aria-describedby="basic-addon2" <?php if(empty($ticketDetails['resultSet']['assigned_to'])){?> onclick="SelectName()"  <?php } ?>>
													
													<span class="input-group-addon" id="reassign" onclick="SelectName()" title="Assign"><i class="fa fa-user"></i></span>
													
													<!-- <button type="button" class="btn btn-default btn-save" >Re-Assigned</button> -->
													<input type="hidden"  id="assignid" value="<?php echo $ticketDetails['resultSet']['assigned_to']?>" name="assignid">
													<input type="hidden"  id="oldAssignId" value="<?php echo $ticketDetails['resultSet']['assigned_to']?>" name="oldAssignId">
												</div>
											</div>
								</div>
								<div class="form-group col-md-4">
									<label for="created_by" class="col-sm-4 control-label">Created By <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="created_by" name="" value="<?php echo $ticketDetails['resultSet']['user']?>" readonly>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="created_on" class="col-sm-4 control-label">Created On <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="created_on" name="" value="<?php echo $ticketDetails['resultSet']['created_on']?>" readonly>
									</div>
								</div>
								<!-- <div class="form-group col-md-4">
									<label for="due_date" class="col-sm-4 control-label">Due Date <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="due_date" name="" value="<?php //echo $ticketDetails['resultSet']['due_date']?>" readonly> 
									</div> 
								</div>
								<div class="form-group col-md-4">
									<label for="estimated_time" class="col-sm-4 control-label">Estimated Time <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="estimated_time" name="" value="<?php //echo $ticketDetails['resultSet']['estimated_time']?>" readonly>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="event_id" class="col-sm-4 control-label">Event ID <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="event_id" name="" value="<?php //echo $ticketDetails['resultSet']['event_invoice']?>" readonly>
									</div>
								</div> -->
								
								<div class="form-group col-md-4">
									<label for="priority" class="col-sm-4 control-label">Urgency <span class="redarrow"></span></label>
									<div class="col-sm-8">										
									<select class="form-control urgency" id="urgency"  name="urgency">
										<?php if(isset($urgency) && (count($urgency['resultSet']) > 0)){
										foreach($urgency['resultSet'] as $urgency_data){	?>				
										<option value="<?php echo $urgency_data['urgency_id'];?>" <?php if($urgency_data['urgency_id'] == $ticketDetails['resultSet']['urgency_id']){echo "selected";} ?> ><?php echo $urgency_data['name'];?></option>
										<?php }} ?>
									</select>
									</div>
								</div>
								
								<div class="form-group col-md-4">
									<label for="priority" class="col-sm-4 control-label"> Event Critical <span class="redarrow"></span></label>
									<div class="col-sm-8">										
									<select class="form-control Severity" id="Severity"  name="severity">
										<?php if(isset($severities) && (count($severities['resultSet']) > 0)){
										foreach($severities['resultSet'] as $severity_data){
											if($severity_data['id'] != 2){ // for OK status
										?>				
										<option value="<?php echo $severity_data['id'];?>" <?php if($severity_data['id'] == $ticketDetails['resultSet']['severity_id']){echo "selected";} ?> ><?php echo $severity_data['severity'];?></option>
										<?php }}} ?>
									</select>
									</div>
								</div>
								
								<div class="form-group col-md-4">
									<label for="priority" class="col-sm-4 control-label"> Severity <span class="redarrow"></span></label>
									<div class="col-sm-8">										
									<select class="form-control skill_id" id="skill_id"  name="skill_id">
										<?php if(isset($skillset) && (count($skillset['resultSet']) > 0)){
										foreach($skillset['resultSet'] as $skillset_data){			
										?>				
										<option value="<?php echo $skillset_data['skill_id'];?>" <?php if($skillset_data['skill_id'] == $ticketDetails['resultSet']['skill_id']){echo "selected";} ?> ><?php echo $skillset_data['title'];?></option>
										<?php }} ?>
									</select>
									</div>
								</div>
								
								<div class="form-group col-md-4">
									<label for="event_id" class="col-sm-4 control-label">Contract Type <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="event_id" name="" value="<?php echo $available_boh['contract']; ?>" readonly>
									</div>
								</div>
								
								<div class="form-group col-md-4">
									<label for="event_id" class="col-sm-4 control-label">External Reference <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="" name="external_ref" value="<?php echo $ticketDetails['resultSet']['ticket_external_ref']?>">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group col-md-4">
									<label for="event_id" class="col-sm-4 control-label">Vendor Reference <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="" name="vendor_ref" value="<?php echo $ticketDetails['resultSet']['vendor_ref']?>">
									</div>
								</div>
								<?php if (substr($available_boh['available_boh'], 0, 1) === '-')
								{
									$availHours = 0.0;
								}else
								{
									$availHours = $available_boh['available_boh'];
								}?>
								
								
								<div class="form-group col-md-4">
									<label for="event_id" class="col-sm-4 control-label">Available BOH <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="event_id" name="" value="<?php echo $availHours; ?>" readonly>
									</div>
								</div>
								
								<?php if (substr($available_boh['available_boh'], 0, 1) === '-'){?>
								<div class="form-group col-md-4">
									<label for="event_id" class="col-sm-4 control-label">Extra access hours <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="" name="" value="<?php echo abs($available_boh['available_boh']); ?>" readonly>
									</div>
								</div>
								<?php } ?>
							<!--	<div class="form-group col-md-4 notes">
									<label for="notes" class="col-sm-4 control-label">Notes <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" placeholder="Enter ..." id="notes" name="notes"></textarea>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="subject" class="col-sm-4 control-label">Short Description <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" id="subject" name="" readonly><?php //echo $ticketDetails['resultSet']['description']?></textarea>
									</div>
								</div> -->
								
								<div class="clearfix"></div>
								<input type="hidden" name="same_status_code" value="<?php echo $ticketDetails['resultSet']['status_code']; ?>">
								
							</div>
								<div class="clearfix"></div>
								<hr />
								</form>
								
								
								<div class="tabbable-custom hover-up" style="padding:0 5px;">
									<ul class="nav nav-tabs ">
										<li class="active">
											<a href="#Description" data-toggle="tab" aria-expanded="false"> Description </a>
										</li>
										<li class="">
											<a href="#add_activity" data-toggle="tab" aria-expanded="false"> Add Activity </a>
										</li>
										
										<li class="">
											<a href="#activity_log" data-toggle="tab" aria-expanded="false"> Activity Log </a>
										</li>
										<li class="">
											<a href="#edit_activity_log" data-toggle="tab" aria-expanded="false"> Activity Edit History </a>
										</li>
										
										 
										<li>
											<a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Incident History </a>
										</li>
										<li class="">
											<a href="#device_details" data-toggle="tab" aria-expanded="false"> Device Details </a>
										</li>
										<li class="">
											<a href="#tab_5_3" data-toggle="tab" aria-expanded="false">Event Correlation </a>
										</li>
										<li class="">
											<a href="#shn" title="Special Handling Notes" data-toggle="tab" aria-expanded="false">SHN </a>
										</li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="Description">
											<table class="table table-striped table-bordered table table-hover" id="tblGrid">   
											<thead style="background:#17c4bb;">
												<tr>
													<th style="color:#fff;">Date</th>
													<th style="color:#fff;">Requestor</th>
													<!--<th style="color:#fff;">Subject</th>-->
													<th style="width:50%;color:#fff;">Description</th>
											
												</tr>
											</thead>
											<tbody class="bold-text" >
												
												<tr>
													<td> <?php echo $ticketDetails['resultSet']['created_on'];?> </td>
													<td> <?php echo $ticketDetails['resultSet']['requestor'];?> </td>
													<!--<td> <?php //echo $ticketDetails['resultSet']['subject'];?></td>-->
													<td> <?php echo $ticketDetails['resultSet']['description'];?></td>
												</tr>
																						  
											</tbody>
										</table>										
										</div>
										
										<div class="tab-pane editActivity" id="activity_log">
											<table class="table table-striped table-bordered table table-hover" id="tblGrid">   
											<thead style="background:#17c4bb;">
												<tr>
													<th style="width:7%;color:#fff;">Activity No</th>
													<th style="width:8%;color:#fff;">Activity Type</th>
													<th style="width:7%;color:#fff;">Start Time</th>
													<th style="width:7%;color:#fff;">End Time</th>
													<th style="width:6%;color:#fff;">Effort Hrs</th>
													<!--<th style="width:8%;color:#fff;">Available BOH</th>-->
													<th style="width:4%;color:#fff;">Worked</th>
													<th style="color:#fff;">Description</th>
													<th style="width:11%;color:#fff;">Internal Comments</th>
													<th style="width:6%;color:#fff;">Actions</th>
													
													
												</tr>
											</thead>
											<tbody class="bold-text">
											<?php
											if (!empty($activity_log["resultSet"])) {
													$i =1;
													foreach($activity_log["resultSet"] as $act_log){?>
												
												<tr>
													<td> <?php echo $act_log["incident_active_id"]; ?> </td>
													<td> <?php echo $act_log["name"]; ?> </td>
													<td> <?php echo $act_log["start_time"]; ?> </td>
													<td> <?php echo $act_log["end_time"]; ?> </td>
													<td> <?php echo effort($act_log["effort_time"]); ?> </td>
													<!--<td> <?php //echo $act_log["current_boh"]; ?> </td>-->
													<td> <?php echo $act_log["created_by"]; ?> </td>
													<td> 
														<?php if(!empty($act_log["activity_desc"])){?>
															<div class="content hideContent">
																<?php echo $act_log["activity_desc"]; ?>		
															</div>
															<?php if(strlen($act_log["activity_desc"]) > 41){?>
															<div class="show-more">
																	<a href="#">Show More</a>
															</div>
															<?php } ?>
														<?php } ?>		
													</td>
													<td> 
														<?php if(!empty($act_log["activity_comment"])){?>
															<div class="content hideContent">
																<?php echo $act_log["activity_comment"]; ?> 
															</div>
															<?php if(strlen($act_log["activity_comment"]) > 41){?>
															<div class="show-more">
																	<a href="#">Show More</a>
															</div>
															<?php } ?>
														<?php } ?>
													</td>
													
													
													
													<td>
													
														<button class="btn btn-default  btn-xs" title="Edit" onclick="updateActivity('<?php echo $act_log["activity_log_id"]; ?>','<?php echo $ticket_id; ?>')"><i class="fa fa-pencil-square-o"></i></button>
														
																																		<?php if($log_files[$act_log["activity_log_id"]][0]['status'] === 'true'){?>
														<div class="btn-group">
															<button class="btn btn-default btn-xs" data-toggle="dropdown">
																<i class="fa fa-download"></i>
															</button>
															<ul class="dropdown-menu pull-right">
															<?php foreach($log_files[$act_log["activity_log_id"]][0]['resultSet'] as $files){ ?>
																
															<li><a href="<?php echo base_url("event/ticket/download_files/".$files['activity_files']); ?>" title="Download Attachment"><?php echo $files['activity_files']; ?></a></li>
															<?php } ?>
															</ul>
														</div>										
										<?php } ?>
														
														
													</td>
												</tr> 
											<?php }}else{ ?>												
												<tr>
													<td colspan="12"> No Record Found </td>
												</tr> 
											<?php } ?>	
																						  
											</tbody>
										</table>
										</div>
										
										<div class="tab-pane" id="edit_activity_log">
											<table class="table table-striped table-bordered table table-hover" id="tblGrid">   
											<thead style="background:#17c4bb;">
												<tr>
													<th style="width:7%;color:#fff;">Activity No</th>
													<th style="width:8%;color:#fff;">Activity Type</th>
													<th style="width:7%;color:#fff;">Start Time</th>
													<th style="width:7%;color:#fff;">End Time</th>
													<th style="width:6%;color:#fff;">Effort Hrs</th>
													<!--<th style="width:8%;color:#fff;">Available BOH</th>-->
													<th style="width:4%;color:#fff;">Worked</th>
													<th style="color:#fff;">Description</th>
													<th style="width:11%;color:#fff;">Internal Comments</th>
													<th style="width:4%;color:#fff;">Attachments</th>
													<th style="width:7%;color:#fff;">Modified By</th>
													
													
												</tr>
											</thead>
											<tbody class="bold-text">
											<?php
											if (!empty($edit_activity_log["resultSet"])) {
													$i =1;
													foreach($edit_activity_log["resultSet"] as $edit_act_log){?>
												
												<tr>
													<td> <?php echo $edit_act_log["incident_active_id"]; ?> </td>
													<td> <?php echo $edit_act_log["name"]; ?> </td>
													<td> <?php echo $edit_act_log["start_time"]; ?> </td>
													<td> <?php echo $edit_act_log["end_time"]; ?> </td>
													<td> <?php echo effort($edit_act_log["effort_time"]); ?> </td>
													<!--<td> <?php //echo $edit_act_log["current_boh"]; ?> </td>-->
													<td> <?php echo $edit_act_log["created_by"]; ?> </td>
													<td> 
														<?php if(!empty($edit_act_log["activity_desc"])){?>
															<div class="content hideContent">
																<?php echo $edit_act_log["activity_desc"]; ?>		
															</div>
															<?php if(strlen($edit_act_log["activity_desc"]) > 41){?>
															<div class="show-more">
																	<a href="#">Show More</a>
															</div>
															<?php } ?>
														<?php } ?>		
													</td>
													<td> 
														<?php if(!empty($edit_act_log["activity_comment"])){?>
															<div class="content hideContent">
																<?php echo $edit_act_log["activity_comment"]; ?> 
															</div>
															
																<?php if(strlen($edit_act_log["activity_comment"]) > 41){?>
															<div class="show-more">
																	<a href="#">Show More</a>
															</div>
																<?php } ?>
														<?php } ?>
													</td>
													<?php if(isset($edit_act_log["activity_file"]) && !empty($edit_act_log["activity_file"])){?>
													<td> <a href="<?php echo base_url("event/ticket/download_files/".$edit_act_log["activity_file"]); ?>"> Attachments </a> </td>
													<?php }else{ ?>
													<td>  -  </td>
													<?php } ?>
													<td>  <?php echo $edit_act_log["created_by"]; ?>  </td>													
												</tr> 
											<?php }}else{ ?>												
												<tr>
													<td colspan="12"> No Record Found </td>
												</tr> 
											<?php } ?>	
																						  
											</tbody>
										</table>
										</div>
										
										
										
										<div class="tab-pane " id="tab_5_1">
											<table class="table table-striped table-bordered table table-hover" id="tblGrid">   
											<thead style="background:#17c4bb;">
												<tr>
													<th style="color:#fff; width:2%;">S.no</th>
													<th style="color:#fff;">Start Time</th>
													<th style="color:#fff;">End Time</th>
													<th style="color:#fff;">Notes</th>
													<th style="color:#fff;">Status</th>
													<th style="color:#fff;">User</th>
												</tr>
											</thead>
											<tbody class="bold-text">
											<?php
												if (!empty($ticketActivity)) {
													$i =1;
													foreach($ticketActivity as $activity){?>
												<tr>								
													<td><?php echo $i++;?></td>         
													<td><?php echo $activity['start_time'];?></td>         
													<td>
														<?php if($activity['state_status'] != 'Closed'){?>
															<?php echo $activity['end_time'];?>
														<?php }else{?>
															<?php echo $activity['start_time'];?>
														<?php } ?>
													</td>         
													<td><?php echo $activity['notes'];?></td>         
													<td><?php echo $activity['state_status'];?></td>         
													<td><?php echo $activity['username'];?></td>         
												</tr>	
											<?php
													}
												} else {?>
												<tr>
													<td colspan="6"> No Record Found </td>
												</tr> 
											<?php } ?>											  
											</tbody>
										</table>
										</div>
										<div class="tab-pane " id="tab_5_3">
											<table class="table table-striped table-bordered table table-hover" id="tblGrid">   
											<thead style="background:#17c4bb;">
												<tr>
													<th style="color:#fff;">Event ID</th>
													<th style="color:#fff;">Event Time</th>
													<th style="color:#fff;">Severity</th>
													<th style="color:#fff;">Event Type</th>
													<th style="color:#fff; width:25%;">Event Text</th>
													
												</tr>
											</thead>
											<tbody class="bold-text">
											<?php
												if (!empty($Alarm_Correlation)) {
												
													foreach($Alarm_Correlation as $alarm){?>
												<tr>
													<td style="position:relative;"><a href="javascript:void(0);" class="coupon_question" evtid='<?php echo $alarm['event_id'] ?> ' ><?php echo $alarm['event_invoice']; ?></a></td>
													<!--<td style="position:relative;"><?php //echo $alarm['event_invoice'];?></td>-->
													<td><?php echo $alarm['start_time'];?></td>         
													<td><?php echo $alarm['severity'];?></td>         
													<td><?php echo $alarm['service'];?></td>         
													<td><?php echo $alarm['event_text'];?></td>         
													<!--<td><?php //echo $alarm['event_text'] .'-'. $alarm['ticket_id'];?></td>-->         
													         
												</tr>	
											<?php
													}
												} else {?>
												<tr>
													<td colspan="9"> No Record Found </td>
												</tr> 
											<?php } ?>											  
											</tbody>
										</table>
										</div>
										
										
										<div class="tab-pane" id="add_activity">
										<form action="<?php echo base_url("event/ticket/save_activity/"); ?>" method="POST" class="activity" enctype="multipart/form-data" onsubmit = "return checkDates()">
										
										
										<div class="row">
											<div class="col-md-3">
												<div class="form-group col-md-12">
													<label for="event_id" class="col-sm-4 control-label">Start Time <span class="redarrow">*</span></label>
													<div class="col-sm-8 " data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
														<input type="text" class="form-control datetimeSupress" name="start_time" id="datetimeSupress" style="width:100%;" data-validation="required" data-validation-error-msg="Start time is required">
														<span id="msg_datetimeSupress"></span>
													</div>
												</div>
												<div class="form-group col-md-12">
													<label for="event_id" class="col-sm-4 control-label">End Time <span class="redarrow">*</span></label>
													<div class="col-sm-8 " data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
														<input type="text" class="form-control datetimeSupress" name="end_time" id="datetimeSupress" style="width:100%;" data-validation="required" data-validation-error-msg="End time is required">
														<span id="msg_datetimeSupress" class="error_end_time" ></span>
													</div>
												</div>
												<div class="form-group col-md-12">
													<label for="event_id" class="col-sm-4 control-label">Activity Type <span class="redarrow">*</span>
													</label>
													<div class="col-sm-8">
														<select class="form-control" id="activity_type" name="activity_type" data-validation="required">
															<?php foreach ($activity_type[ 'resultSet'] as $value) { ?>
															<option value="<?php echo $value['activity_type_id']; ?>">
																<?php echo $value[ 'name']; ?>
															</option>
															<?php } ?>
														</select>
													</div>
												</div>
												<div class="form-group col-md-12">
													<label for="event_id" class="col-sm-4 control-label">Comments </label>
													<div class="col-sm-8">
														<textarea class="form-control" name="comment" rows="3" placeholder="Enter ..." name="notes"></textarea>
													</div>
												</div>
												<div class="form-group col-md-12">
													<label for="event_id" class="col-sm-4 control-label">Attachment </label>
													<div class="col-sm-8">
														<input type="file" class="form-control" id="attachment" name="userfile[]" multiple="multiple">
													</div>
												</div>
												<input type="hidden" name="ticket_id" value="<?php echo $ticket_id; ?>">
												
											</div>
											<div class="col-md-9 padl0">
												<div class="form-group col-md-12 padl0">
													<div class="col-sm-12">
														<textarea class="form-control description" name="activity_description" rows="3" placeholder="Enter Description ..." name="notes"></textarea>
													</div>
												</div>
												<div><button class="btn btn-green green pull-right" style="padding: 1px 4px; 	font-size: 13px; margin-right: 11px;" id="save_activity"> Save </button></div>
											</div>
										</div>
										<input type="hidden" name="contractId" value="<?php echo $ticketDetails['resultSet']['contract_type']?>">
											
											<div class="clearfix"></div>
										</form>
										</div>
										
										
										<div class="tab-pane" id="device_details">
											<div class="row">
												<div class="col-md-3">
													<div class="page_title_sub">Device Details</div>
													<table class="table table-striped table-bordered" id="tblGrid">
														<tbody class="bold-text">
															<tr>								
																<td style="width:50% !important;">Device Type :</td>  
																<td style="width:50% !important;"><?php echo $ticketDetails['resultSet']['device_name']?></td>         
															</tr>
															<tr>								
																<td style="width:10% !important;">IP Address :</td>  
																<td style="width:90% !important;"><?php echo $ticketDetails['resultSet']['address']?></td>         
															</tr>
															<tr>								
																<td style="width:10% !important;">Username :</td>  
																<td style="width:90% !important;"><?php echo $ticketDetails['resultSet']['username']?></td>         
															</tr>
															<tr>								
																<td style="width:10% !important;">Password :</td>  
																<td>
																<?php if($ticketDetails['resultSet']['password']){ ?>
																<input type="password"  style="width:90% !important;" value="<?php echo $ticketDetails['resultSet']['password']; ?>" readonly >
																
																<button type="button" id="copy" class="btn btn-primary btn-xs">Copy</button>
																<?php } ?>
																</td>
																																	
															</tr>
														<?php if(isset($ticketDetails['resultSet']['username'],$ticketDetails['resultSet']['password']) && !empty($ticketDetails['resultSet']['username']) && !empty($ticketDetails['resultSet']['password'])){?>	
															<tr>
																<td colspan="2">
																	<button  class="btn yellow" onclick="SelectName2()"> SSH </button>
																</td>
															</tr>
														<?php }?>
														</tbody>
													</table>
												</div>
												<div class="col-md-9">
													<div class="page_title_sub">Login History</div>
													<table class="table table-striped table-bordered table table-hover" id="tblGrid">
														<thead style="background:#17c4bb;">
															<tr>
																<th style="color:#fff; width:2%;">S.no</th>
																<th style="color:#fff;">User</th>
																<th style="color:#fff;">Login Time</th>
																<th style="color:#fff;">Logout Time</th>
																<th style="color:#fff;">Login Mode</th>
																<th style="color:#fff;">Worked Duration</th>
															</tr>
														</thead>
														<tbody class="bold-text" id="tabledatassh">
														<?php
															if (!empty($ssh_log["resultSet"])) {
															$i =1;
															foreach($ssh_log["resultSet"] as $ssh){?>
															<tr>								
																<td style="width:2%;"><?php echo $i++; ?></td>         
																<td> <?php echo $ssh["username"]; ?> </td>         
																<td> <?php echo $ssh["login_time"]; ?> </td>         
																<td> <?php echo $ssh["logout_time"]; ?> </td>         
																<td> <?php echo $ssh["login_mode"]; ?> </td>         
																<td> <?php echo effort($ssh["effort_time"]); ?> </td>         
																        
															</tr>
														<?php }}else{ ?>												
															<tr>
																<td colspan="6"> No Record Found </td>
															</tr> 
														<?php } ?>	
														</tbody>
													</table>
												</div>

											</div>
												
												<!--<a class="btn green" href="#" target="_blank"> Run Auto Script </a>-->
										</div>
										<div class="tab-pane" id="tab_5_3">
											<p> Howdy, I'm in Section 3. </p>
											<p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel
												eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
											<p>
												<a class="btn yellow" href="http://10.10.32.35:8080/guacamole/" target="_blank"> SSH </a>
												<a class="btn green" href="#" target="_blank"> Run Auto Script </a>
											</p>
										</div>
										
										<div class="tab-pane" id="shn">
											<div class="col-md-9 padl0">
												<h6><strong>Special Handling Notes</strong></h6>
												<div class="shn_desc">
												<?php if(isset($ticketDetails['resultSet']['shn']) && $ticketDetails['resultSet']['shn'] != ""){ echo $ticketDetails['resultSet']['shn']; } else {
													echo "Not Available";
												}?>
													
												</div>
											</div>
											<?php if(isset($ticketDetails['resultSet']['client_doc_url']) && $ticketDetails['resultSet']['client_doc_url'] != ""){?>
											<div class="col-md-3 shn_btn">
												<h6><strong>Download Files</strong></h6>
												<a class="" href="<?php echo base_url('client/ClientShn/download_files/'.$ticketDetails['resultSet']['client_doc_url']); ?>">
													<button type="button" class="btn  btn-default btn-block">
														<?php echo $ticketDetails['resultSet']['client_doc_url']?>
														<span class="btn-label"><i class="fa fa-download" aria-hidden="true"></i></span>
													</button>
												</a>
												
											</div>
											<?php }?>
										</div>
										
										
									</div>
								</div>
							</div>
						
					</div>
					</div>
					<!-- Tickets Form End -->	

					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>
					
	<div class="modal fade" id="myModal">
    <div class="modal-dialog"  id="load-data">
		
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<input type="hidden" value="" name="ssh" id="ssh">
<?php
	function effort($seconds)
	{
		//$seconds = 86400;
		$H = floor($seconds / 3600);
		$i = ($seconds / 60) % 60;
		$s = $seconds % 60;
		return sprintf("%02d:%02d:%02d", $H, $i, $s);
	}

	function access($val){
	 switch($val){
			case '0': // New
				$access = array(0,1);
				break;
			case '1': // Assigned
				$access = array(1,2,10);  
				break;
			case '2': // In-Progress
				$access = array(2,3,4,5,8,9,11,12);
				break;
			case '3': // Pending Customer
				$access = array(2,3,8,9,10);
				break;
			case '4': // Scheduled
				$access = array(2,4,10);
				break;
			case '5': // Resolved
				$access = array(5,6,7); 
				break;
			case '6': // Reopen
				$access = array(1,6,10); 
				break;
			case '7': // Closed
				$access = array(7,6);
				break;
			case '8': // Pending Vendor
				$access = array(2,3,8,9,10);
				break;
			case '9': // Pending Work Order
				$access = array(2,3,8,9,10);
				break;
			case '10': // Re-Assigned
				$access = array(2,10);
				break;
			case '11': // Monitoring
				$access = array(2,3,4,5,8,9,11,12);
				break;
			case '12': // Hold
				$access = array(2,3,4,5,8,9,11,12);
				break;
				
	 }
	 return $access;
	}
?>

<script src="<?php echo base_url() . "public/" ?>js/form/form_event_ticket.js" type="text/javascript"></script>	
<script src="<?php echo base_url() . "public/" ?>datapicker/onejquery.datetimepicker.full.js"></script>
<script src="<?php echo base_url() . "public/" ?>form-validator/jquery.form-validator.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>

	$(document).ready(function() {
		var baseURL = $('#baseURL').val();		
		$('.full_height_block').height($(window).height() - 133);
		$('.tickets_data1').height($(window).height() - 180);
	});	

	$(".parent .rightbar").click(function(){
	  
		if($('.tickets_tabs').css('display') == 'none'){ 
			$(".parent").removeClass('col-md-12');
			$(".parent").addClass('col-md-10');
			$(".tickets_tabs").show(); 
		} else { 
			$(".parent").removeClass('col-md-10');
			$(".parent").addClass('col-md-12');
			$(".tickets_tabs").hide(); 
		}
	
	});
	
	$('.datetimeSupress').datetimepicker({
            format: 'Y-m-d H:i',
            mask: false,
            dayOfWeekStart: 1,
            lang: 'en',
            timepicker: true,
            showTimePicker: true,
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false,
            minDate: 0,
            step: 5
	});			
		
	$(function() {
	// setup validate
		$.validate({
			modules : 'file'
		});
	});	
		
	var popup;
	var url = "<?php echo base_url('event/ticket/assignusers?skill='.encode_url($ticketDetails['resultSet']['priority']).'&p='.$ticketDetails['resultSet']['patner_id']) ?>";
    function SelectName() {
        popup = window.open(url, "Popup", "width=700,height=400");
        popup.focus();
    }
	
		
	var incident = <?php echo $ticket_id; ?>;
	var device_id = "<?php echo base64_encode($ticketDetails['resultSet']['device_id']); ?>";
	var postUrl = "<?php echo base_url('event/ticket/sshdetails'); ?>";
	var postUrlSsh = "<?php echo base_url('event/ticket/sshloglist'); ?>";
	var url2 = "<?php echo base_url('guacamole/gme/index?id='.base64_encode($ticketDetails['resultSet']['device_id'])); ?>";
	var popup2;	
    function SelectName2() {
		check();		
        popup2 = window.open(url2, "Popup", "width=1000,height=500");
		popup2.focus();	
		var form_data = {
        id: incident,
         };
		$.ajax({
			url: postUrl,
			type: 'POST',
			data: form_data,
			success: function(msg) {
				$('#ssh').val(msg);				
			}
		});			
	
    }
	
	function check(){		
		window.timer = setInterval(checkChild, 500);		
	}

	function checkChild() {
	var updateUrl = "<?php echo base_url('event/ticket/sshdetailsupdate'); ?>";	
		if (popup2.closed) {
			//alert("Child window closed");   
			clearInterval(window.timer);
			var form_data = {
				id: $('#ssh').val(),
			 };
			$.ajax({
				url: updateUrl,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//alert(msg);				
					var form_data = {
					id: incident,
					};	
					$.ajax({
					url: postUrlSsh,
					type: 'POST',
					data: form_data,
					success: function(msg) {
					//alert('ok');
					$('#tabledatassh').html('');
					$('#tabledatassh').html(msg);				
					}
					});
				}
			});
	

						
		}
	}

	
	function copyToClipboard(text) {
		if (window.clipboardData && window.clipboardData.setData) {
			// IE specific code path to prevent textarea being shown while dialog is visible.
			return clipboardData.setData("Text", text); 

		} else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
			var textarea = document.createElement("textarea");
			textarea.textContent = text;
			textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
			document.body.appendChild(textarea);
			textarea.select();
			try {
				return document.execCommand("copy");  // Security exception may be thrown by some browsers.
			} catch (ex) {
				console.warn("Copy to clipboard failed.", ex);
				return false;
			} finally {
				document.body.removeChild(textarea);
			}
		}
	}

	$("#copy").on('click',function(){
		var pwd = '<?php echo $ticketDetails['resultSet']['password'];  ?>';
		var result = copyToClipboard(pwd);
		console.log("copied?", result);
	});
	


	function submitResult() {
		var status_code = $('#status_code').val();
		var assignid = $('#assignid').val();
		if(status_code == 0)
		{
			$('.not_updated1').show();
			return false;		
		}else
		{
			$('.not_updated').hide();
		}
		if(status_code != 0 && assignid != 0)
		{
			$('.not_updated').hide();
			return true;
			
		}else
		{
			$('.not_updated').show();
			return false;		
		}  
	}

	function assginUser(val)
	{
		if(val == 1 || val == 10)
		{
			SelectName();
		}
	}	

	$(".show-more a").on("click", function() {
		var $this = $(this); 
		var $content = $this.parent().prev("div.content");
		var linkText = $this.text().toUpperCase();    
		//alert(linkText);
		if(linkText === "SHOW MORE"){
			linkText = "Show Less";
			//$content.switchClass("hideContent", "showContent", 200);
			$content.addClass('showContent').removeClass('hideContent');
		} else {
			linkText = "Show More";
			//$content.switchClass("showContent", "hideContent", 200);
			$content.addClass('hideContent').removeClass('showContent');
		};

		$this.text(linkText);
	});


	CKEDITOR.replace( 'activity_description' );
	var update_url = '<?php echo base_url('event/ticket/update_activity'); ?>';
	function updateActivity(val,id)
	{
		var form = {activityId:val,ticketId:id}; 
		$.ajax({
			url:update_url,
			type:'POST',
			data:form,
			success:function(response)
			{
				
			 $(".editActivity").html(response);
			 afterAjaxCall();	
				
			},
		});
	}

	function afterAjaxCall()
	{
		 $('.datetimeSupress').datetimepicker({
				format: 'Y-m-d H:i',
				mask: false,
				dayOfWeekStart: 1,
				lang: 'en',
				timepicker: true,
				showTimePicker: true,
				scrollMonth: false,
				scrollTime: false,
				scrollInput: false,
				minDate: 0,
				step: 5
			});	
			
			
			$(function() {
			// setup validate
				$.validate({
					modules : 'file'
				});
			});
			
			CKEDITOR.replace( 'activity_description' );
			
			$('.datetimeSupress').on('keypress',function(event){	
				return false;
			});
	}

	var DownloadUrl = "<?php echo base_url('event/ticket/download_files/'); ?>";	
	function downloadFile(val)
	{
	 if (confirm("Do you want to download the file?") == true) {
			var form_data = {file:val}
			var win = window.open(DownloadUrl+val, '_blank');			
		} 
		else{
			return false;			
		}	
	}
 
	$('#cke_1_contents').css({'min-height': '167px !important', 'height': 'auto !important'});
	$('#incpanel1').on('show.bs.collapse hidden.bs.collapse', function (e) {
		if (e.type == 'hidden') {
		$('.tab-content').css({'height': (($(window).height()) - 225) + 'px'});
		$(window).bind('resize', function () {
			$('.tab-content').css({'height': (($(window).height()) - 225) + 'px'});
		});
		
		$('#cke_1_contents').css({'height': (($(window).height()) - 275) + 'px'});
		$(window).bind('resize', function () {
			$('#cke_1_contents').css({'height': (($(window).height()) - 275) + 'px'});
		});
		
		} else {
		$('.tab-content').css({'height': '208px'});
		$('#cke_1_contents').css({'height': '167px'});
		}
		$('.accordion-toggle').find('.glyphicon').toggleClass('glyphicon glyphicon-chevron-down glyphicon glyphicon-chevron-up');
	});
	
var data = <?php echo $clients; ?>; 
	$('.Clients').select2({
       placeholder: 'Select Client',
       width: "100%",
       data: data,      
    });
var data = <?php echo $locations; ?>; 
	$('.Location').select2({
       placeholder: 'Select Location',
       width: "100%",
       data: data,      
    });
var device = <?php echo $devices; ?>; 
	$('.Device').select2({
       placeholder: 'Select Device',
       width: "100%",
       data: device,      
    });
var services = <?php echo $services; ?>; 
	$('.serivces').select2({
       placeholder: 'Select service',
       width: "100%",
       data: services,      
    });
var cate = <?php echo $categories; ?>; 
	$('.Category').select2({
	   placeholder: 'Select Category',
	   width: "100%",
	   data: cate,      
	});
var subcate = <?php echo $subcategories; ?>; 
	$('.subCategory').select2({
	   placeholder: 'Select SubCategory',
	   width: "100%",
	   data: subcate,      
	});

function getCategories(val)
{
	$('.subCategory').empty().append('<option></option>');
	var Url = "<?php echo base_url('manual/incidents/getAllCategories'); ?>";	
	var form_data = {
				device_id: val
			 };
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//console.log(msg);
					var obj = jQuery.parseJSON(msg);
					//console.log(obj);
					$('.Category').val(obj.categoryId); 
					$('.Category').trigger('change');					 					
					$('.subCategory').append(obj.subcategory).trigger('change'); 					
					$('.serivces').append(obj.serivces).trigger('change');
					$('.serivces').val(obj.serviceId); 
					$('.serivces').trigger('change'); 
									
				},
			});	
		
}

	
function getSubCategories(val)
{
	$('.subCategory').empty().append('<option></option>');
	var Url = "<?php echo base_url('manual/incidents/getAllSubCategories'); ?>";	
	var form_data = {
				cate_id: val
			 };
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//console.log(msg);
					var obj = jQuery.parseJSON(msg);				 					
					$('.subCategory').append(obj.subcategory).trigger('change');						
				},
			});	
		
}	

/* $('.hover-up').on('click',function(){	
	var a = $('.accordion-toggle').hasClass( "collapsed" );	
	if(a == false)
	{
	 $('.accordion-toggle').click();
	}	
});	 */

function checkDates()
{
	var d_1 = $( "input[name*='start_time']" ).val();
	var d_2 = $( "input[name*='end_time']" ).val();
	if(d_2)
	{
		var d1 = new Date(d_1);
		var d2 = new Date(d_2);
		if(d2 > d1)
		{
		  return true;
		}else
		{
		 $(".error_end_time").html("<font color='red'>End time is not lower than start time</font>");	
		 return false;
		}
	}
}


 $('.datetimeSupress').on('keypress',function(event){
	
if(!isNaN(event.key))
{
	return true;
}else{
	return false;
}	
	
}); 
	
</script>