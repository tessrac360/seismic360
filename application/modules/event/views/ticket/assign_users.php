<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <style>
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
		padding: 3px 6px;
		font-size: 13px;
	}
	.page-title{    font-size: 17px;
    background: rgba(204, 204, 204, 0.22);
    padding: 6px 9px;
    margin: 0 0 10px;}
  </style>
  
</head>
<body>
  <h4 class="page-title">Assign Incident to user</h4>
<div class="container">                                                             
  <div class="table-responsive"> 

  <table class="table table-striped table-bordered table table-hover">
    <thead style="background: #17c4bb; color:#fff;">
      <tr>
        <th>#</th>
        <th>Username</th>
       <!-- <th>Priority</th>  -->      
      </tr>
    </thead>
    <tbody>
		
		<?php if($getLoginUsers['status'] == 'true' && $getLoginUsers['resultSet'] > 0){
			foreach($getLoginUsers['resultSet'] as $data){
			?>
      <tr>
        <td><input type="radio" name="user" value="<?php echo $data['id']; ?>" id="ddlNames" onclick="SetName(this.value,'<?php echo $data['username']; ?>');" /></td>
        <td><?php echo $data['username']; ?></td>        
      <!--  <td><?php //echo $data['title']; ?></td>  -->      
      </tr>
		<?php }}else{ ?>
	 <tr>
        <td colspan="3">Login users not available!</td>
     </tr>
		<?php } ?>
	   

    </tbody>
  </table>

  </div>

<script type="text/javascript">
    function SetName(val,name) {
		
        if (window.opener != null && !window.opener.closed) {
            var txtName = window.opener.document.getElementById("txtName");
            var assignid = window.opener.document.getElementById("assignid");
            txtName.value = name;
            assignid.value = val;
        }
        window.close();
    }
</script>
</div>

</body>
</html>
