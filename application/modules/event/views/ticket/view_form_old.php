<style type="text/css">
	.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 3px 6px;
    font-size: 12px !important;
}

	.form-search .control-label{font-size:13px; text-align:right;}
	.form-search .form-control{height:24px; padding:0px 8px; font-size:13px;}
	.form-search .form-group{height: 16px;margin-bottom: 12px;}
	
	.page-toolbar .input-group{width: 245px;}
	.page-toolbar .input-group .btn.btn-default{padding: 4px 12px;}
	.page-toolbar .input-group .form-control{padding: 4px 12px !important; height: 30px;}
	
	table#role th, table#role td { white-space: nowrap;}
	
</style>
<!-- END PAGE HEADER-->
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Incidents</span>
			</div>
			<div class="page-toolbar">
				<div class="input-group add-on"> 
					<input class="form-control" placeholder="Search" name="incidentSearch" id="incidentSearch" type="text">
					
					<div class="input-group-btn">
						<button class="btn btn-default hidden-xs" id="btn_incidentSearch" type="submit"><i class="glyphicon glyphicon-search"></i></button>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		
		
		<form class="form-search" style="padding:10px 0; background: rgba(255, 255, 255, 0.78);">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Client</label>
						<div class="col-md-8">
							<select  class="form-control onchange" id="id" name="id">
								<option value="">All</option>
								<?php
								if ($clients['status'] == 'true') {
									foreach ($clients['resultSet'] as $value) {
										if($value['id'] != 1){?>
									<option value="<?php echo $value['id']; ?>"><?php echo $value['client_title'];?></option>
									<?php }
								}
								}?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Status</label>
						<div class="col-md-8">
							<select  class="form-control onchange" id="status_code" name="status_code">
								<option value="">All</option>
								<?php
								if ($ticketState['status'] == 'true') {
									foreach ($ticketState['resultSet'] as $value) {?>
									<option value="<?php echo $value['status_code']; ?>"><?php echo $value['status'];?></option>
									<?php }
								}?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Category</label>
						<div class="col-md-8">
							<select  class="form-control onchange" id="ticket_cate_id" name="ticket_cate_id">
								<option value="">All</option>
								<?php
								if ($category['status'] == 'true') {
									foreach ($category['resultSet'] as $value) {?>
									<option value="<?php echo $value['ticket_cate_id']; ?>"><?php echo $value['name'];?></option>
									<?php }
								}?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Sub Category</label>
						<div class="col-md-8">
							<select  class="form-control onchange" id="ticket_sub_cate_id" name="ticket_sub_cate_id">
								<option value="">All</option>
								<?php
								if ($subcategory['status'] == 'true') {
									foreach ($subcategory['resultSet'] as $value) {?>
									<option value="<?php echo $value['ticket_sub_cate_id']; ?>"><?php echo $value['name'];?></option>
									<?php }
								}?>
							</select>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Ticket Type</label>
						<div class="col-md-8">
							<select  class="form-control onchange" id="ticket_type_id" name="ticket_type_id">
								<option value="">All</option>
								<?php
								if ($ticketType['status'] == 'true') {
									foreach ($ticketType['resultSet'] as $value) {?>
									<option value="<?php echo $value['ticket_type_id']; ?>"><?php echo $value['name'];?></option>
									<?php }
								}?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Queue</label>
						<div class="col-md-8">
							<select  class="form-control onchange" id="queue_id" name="queue_id">
								<option value="">All</option>
								<?php
								if ($queue['status'] == 'true') {
									foreach ($queue['resultSet'] as $value) {?>
									<option value="<?php echo $value['queue_id']; ?>"><?php echo $value['queue_name'];?></option>
									<?php }
								}?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Assigned To</label>
						<div class="col-md-8">
						    <?php if($myincidents== 'ok'){?>
								<select  class="form-control onchange" id="user_id" name="user_id" disabled>
									<?php
									if ($clientUsers['status'] == 'true') {
										foreach ($clientUsers['resultSet'] as $value) {?>
										<option value="<?php echo $value['id']; ?>" <?php if($user_id ==$value['id']){echo "selected";}?>><?php echo $value['first_name'];?></option>
										<?php }
									}?>
								</select>							
							<?php }else{?>
								<select  class="form-control onchange" id="user_id" name="user_id">
									<option value="">All</option>
									<?php
									if ($clientUsers['status'] == 'true') {
										foreach ($clientUsers['resultSet'] as $value) {?>
										<option value="<?php echo $value['id']; ?>"><?php echo $value['first_name'];?></option>
										<?php }
									}?>
								</select>							
							<?php }?>

						</div>
					</div>
				</div>
				<!--<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Requestor</label>
						<div class="col-md-8">
							<select  class="form-control onchange" id="client_id" name="client_id">
								<option value="">All</option>
								<?php
								//if ($subClients['status'] == 'true') {
									//foreach ($subClients['resultSet'] as $value) {?>
									<option value="<?php //echo $value['id']; ?>"><?php //echo $value['name'];?> ; <?php //echo $value['client_email'];?></option>
									<?php //}
								//}?>
							</select>
						</div>
					</div>
				</div>-->
				
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Severity</label>
						<div class="col-md-8">
							<select  class="form-control onchange" id="severity_id" name="severity_id">
								<option value="">All</option>
								<?php
								if ($severity['status'] == 'true') {
									foreach ($severity['resultSet'] as $value) {?>
									<option value="<?php echo $value['id']; ?>"><?php echo $value['severity'];?></option>
									<?php }
								}?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Priority</label>
						<div class="col-md-8">
							<select  class="form-control onchange" id="priority_id" name="priority_id">
								<option value="">All</option>
								<?php
								if ($priority['status'] == 'true') {
									foreach ($priority['resultSet'] as $value) {?>
									<option value="<?php echo $value['skill_id']; ?>"><?php echo $value['title'];?></option>
									<?php }
								}?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label col-md-4">Date</label>
						<div class="col-md-8">
							<select  class="form-control onchange" id="date" name="date">
								<option value="">All</option>
								<option value="1">Today</option>
								<option value="2">Yesterday</option>
								<option value="3">This Week</option>
								<option value="4">Last Week</option>
								<option value="5">This Month</option>
								<option value="6">Last Month</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<!--<label class="control-label col-md-4">Assigned To</label>
						<div class="col-md-8">
							<input type="text" class="form-control"> </div>-->
					</div>
				</div>
			</div>
			<input type="hidden" id="user" name="user" value="<?php echo $user_id; ?>">
			<input type="hidden" id="myincidents" name="myincidents" value="<?php echo $myincidents; ?>">
			<!--<div><button class="btn btn-green green pull-right" style="padding: 1px 4px; font-size: 13px; margin-right: 11px;">Advance Search</button></div>-->
			<div class="clearfix"></div>
		</form>
		
		<div class="clearfix"></div>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0 full-height-content full-height-content-scrollable" style="margin-left: 0; margin-right: 0;" id = "list">
            <div class="portlet-body" style="padding-top:0;">
				<div class="col-md-12" style="overflow:auto;padding: 0;float:left;" id="style-3">
                <table class="table table-striped table-bordered table-hover order-column" id="role">
                    <thead style="background: #17C4BB !important; color:white;">
                        <tr>
							<th style="width:12%">Incident Number</th>
							<th style="width:12%">Incident Date & Time</th>
							<th>Client</th>
							<th>Requestor</th>
							<th>Created By</th>
							<th>Summary</th>
							<th>Assigned To</th>
							<th>Queue</th>
							<th>State</th>
							<th>Priority</th>
							<th>Severity</th>
							<th>Ticket Type</th>
							<th>Category</th>
							<th>Sub Category</th>
							<th>Last Updated</th>
							<th>Closed On</th>
							
							
						</tr>
                    </thead>
                    <tbody id="siddhu">
                        <?php
                        if (!empty($ticketColumn)) {
                            foreach($ticketColumn as $columns){?>
							<tr>
							<td><?php if($columns['ticket_type_id']==1){?>
							<a href="<?php echo base_url() . 'event/ticket/editTicket/'.encode_url($columns['ticket_id']) ; ?>"><?php echo $columns['incident_id']; ?></a>
							<?php }else{ ?>
							<a href="<?php echo base_url() . 'event/ticket/macEdit/'.encode_url($columns['ticket_id']) ; ?>"><?php echo $columns['incident_id']; ?></a>
							<?php } ?>
							
							</td>
								<td><?php echo $columns['created_on'];?></td>
								<td><?php echo $columns['client_name'];?></td>
								<td><?php echo $columns['requestor'];?></td>
								<td><?php echo $columns['created_by'];?></td>								
								<td><?php echo $columns['subject'];?></td>
								<td><?php echo $columns['assigned_to'];?></td>
								<td><?php echo $columns['queue_name'];?></td>
								<td><?php echo $columns['status'];?></td>
								<td><?php echo $columns['priority'];?></td>
								<td><?php echo $columns['severity'];?></td>
								<td><?php echo $columns['ticket_type'];?></td>
								<td><?php echo $columns['category'];?></td>
								<td><?php echo $columns['subcategory'];?></td>
								<td><?php 
								if($columns['last_updated_on'] == '0000-00-00 00:00:00')
								{
									echo " - ";
								}else{
								echo $columns['last_updated_on'];
								}?></td>
								<td><?php 
								if($columns['status_code'] == 7){
								echo $columns['last_updated_on'];
								}else
								{
									echo " - ";
								}
								?></td>
								
								
								
								
								<!--<td><?php //echo $columns['priority'];?></td>-->
								<!--<td><?php //echo $columns['queue_name'];?></td>
								<td><?php //echo $columns['last_updated_on'];?></td>-->
                                    
                                  <!--  <td>
                                        <input type="checkbox" <?php
                                       // if ($value['status'] == 'Y') {
                                           // echo 'checked';
                                        //}
                                        ?>  id="onoffactiontoggle" class="onoffactiontoggle" myval="<?php //echo encode_url($value['id']); ?>">
                                    </td>
									
                                    <td>
                                        <a href="<?php //echo base_url() . 'client/gateway/edit/' .encode_url($client_id)."/". encode_url($value['id']); ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>  
                                         <a href="<?php //echo base_url() . 'client/gateway/delete/' .encode_url($client_id)."/". encode_url($value['id']); ?>"  userId="<?php echo encode_url($value['id']);?>" class="btn btn-xs red isdeleteAdminUsers" onlick=''>
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                        <a href="<?php //echo base_url() . 'nagiosmanagements/nagios_config/' . encode_url($value['id']) ; ?>">

                                            <button class="btn blue" style=" padding: 2px 2px;font-size:12px;" id="addsubclient" type="button">Manage Config Files</button>
											

                                        </a>													
                                    </td>-->
                            </tr> 
                            <?php
							}
                        } else {
                            ?>
                            <tr>
                                <td colspan="15"> No Record Found </td>
                            </tr> 
<?php } ?>
                    </tbody>
                </table>
				</div>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php // echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

<script>
$(function()
{
        $('#style-3') .css({'height': (($(window).height()) - 260)+'px'});
    
        $(window).bind('resize', function(){
            $('#style-3') .css({'height': (($(window).height()) - 260)+'px'});
            //alert('resized');
        });
});
</script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_ticket.js" type="text/javascript"></script>	