<style>
	.amcharts-title{color:#000; fill:black;}
	.amcharts-title{
  transform: translate(145px,30px)
}
.amcharts-title:nth-child(2){
  transform: translate(160px, 150px)
}
	
		@media (min-width: 992px){
		.page-footer {
			background-color: #fff !important;
		}
		.page-content-wrapper .page-content{
			padding:0;
		}
		}
		.page-title {
			font-size: 17px;
			background: rgba(204, 204, 204, 0.22);
			padding: 6px 9px;
			margin: 0 0 10px;
		}
		.row {
			margin-left: 0;
			margin-right:0;
		}
		.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{
			padding-left: 5px;
			padding-right: 5px;
		}
		.portlet{
			margin-bottom: 9px;
		}
		.page-header.navbar .page-logo .logo-default {margin: 18px 0 0 0;}
		
		@media (min-width: 992px){
			.home-alerts .page-sidebar1.alerts-right {
				float: right !important;
				position:relative !important;
				margin-right: 0  !important;
				margin-left: -100%  !important;
				top:0;
				z-index:9;
				    padding: 0;
			}
			.home-alerts .page-content-wrapper .page-content {
				margin-right: 290px !important;
			}
			.page-sidebar-closed1.home-alerts .page-content-wrapper .page-content{margin-right: 0 !important;}
			.page-sidebar-closed1 .page-sidebar1 { width: 0px !important; display: block!important; height: auto!important; padding-bottom: 0; overflow: hidden !important;}
			
			.mar-lr3{margin-left:1px !important; margin-right:1px !important;}
			.pad-tlr-3{padding-top:3px !important; padding-left:3px; padding-right:3px;}
			.remove_pad{padding:0 !important;}
			.remove_pad_left{padding-left:0;}
			.remove_pad_right{padding-right:0px;}
			.pad_right3{padding-right:3px !important;}
			}
		
		.mar-bottom{margin-bottom:0;}
		
		
		.tickets_data.col-md-10{padding-left:0;}

.portlet>.portlet-title>.caption {
    float: left;
    display: inline-block;
    line-height: 17px;
    padding: 6px 0 4px 0;
}

.portlet.light>.portlet-title.portlet-bg-title {
    min-height: 34px;
    margin-bottom: 6px;
    background: #17c4bb;
    padding: 3px 4px 3px 7px;
}

.page-bar {
    padding: 0px;
	float: right;
	background-color: transparent;
margin-bottom:0;
}

.portlet.light>.portlet-title>.caption>.caption-subject { font-size: 13px; text-transform: capitalize !important;     font-weight: 600 !important;
    color: #fff; }
.portlet.light>.portlet-title>.caption{padding: 3px 0;}

.form-control{height: 26px; font-size: 13px}
.tickets_data_header .btn-default.btn-search{padding: 4px 8px 3px 8px !important; font-size: 12px;margin-right: 0 !important;}
.tickets_data_header .btn.btn-toggle{    padding:1px 5px 1px 5px; font-size: 12px; background: #fff; border: 0; margin-right:6px; position:relative; top:-1px;}
.padd8{padding:8px;}

.redarrow { color: red; }
.custom-padding-formgp hr{margin:5px 0;}
.custom-padding-formgp .form-group{margin-bottom:4px;}
.custom-padding-formgp .form-group .form-control { border-radius: 0; padding: 3px 6px; height: 30px;     background: #f5f5f5;}
.custom-padding-formgp .form-group textarea.form-control { height:auto;}
.custom-padding-formgp .form-group .col-sm-8 { padding-right:0; }
.custom-padding-formgp .form-group .col-sm-4 { padding-left: 0; font-size: 12px; line-height: 30px; margin-bottom: 0; text-align:right;}
.custom-padding-formgp .form-group .mt-checkbox-inline, .custom-padding-formgp .form-group .mt-radio-inline { padding: 5px 0;}  
.custom-padding-formgp .form-group .mt-checkbox, .custom-padding-formgp .form-group .mt-radio {margin-bottom: 0; font-size:13px;}  

.custom-padding-formgp .nav>li>a {padding: 5px 15px; font-size:13px; font-weight:400 !important;}
.btn-save{padding: 4px 12px; margin-left:4px; border:0;font-size: 12px; position:relative; top:1px;}

.tab-content{max-height:200px; overflow-y:scroll; font-size:13px;}
		
	</style>
<div>
	<h1 class="page-title" style="margin-bottom:5px;"> Tickets </h1>
	<div class="full-height-content-body">
		<div class="row">
			<div class="tickets_tabs col-md-2 pad_right3" style="display:none;">
				<div class="portlet light bordered full_height_block mar-bottom remove_pad">
					
				</div>
			</div>
			<div class="col-md-12 parent">
				<div class="portlet light bordered full_height_block mar-bottom remove_pad tickets_data ">
					<div class="col-md-12 portlet-title portlet-bg-title tickets_data_header">
						<div class="caption font-dark">
							<span class="caption-subject bold uppercase">
								<button class="rightbar btn btn-default btn-toggle"><i class="fa fa-bars" aria-hidden="true"></i></button>
							Incidents  - 123456</span>
						</div>
						<div class="page-bar form-inline">
							<div class="input-group add-on"> 
								<button class="btn btn-default btn-save">Save</button>
								<button class="btn btn-default btn-save">Login</button>
								<button class="btn btn-default btn-save">Run Auto Script</button>
							</div>
						</div>
					</div>
					
					<!-- Tickets Form -->
					<div class="portlet-body ticket_form_data remove_pad">
					<div class="tickets_data1" style="overflow:auto;">
						<form>
							<div class="row mar-lr3 custom-padding-formgp">
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Number <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txtPortLoginId" name="txtPortLoginId" tabindex="26" placeholder="Portal Login ID" maxlength="200">
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Opened <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<select class="form-control V_Combo" id="ddlClient" name="ddlClient" tabindex="9" >
											<option value="0">Select</option>
											<option value="3">CLIENT1</option>
											<option value="4">CLIENT2</option>
											<option value="5">CLIENT3</option>
										</select>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Requister <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txtPortLoginId" name="txtPortLoginId" tabindex="26" placeholder="Portal Login ID" maxlength="200">
									</div>
								</div>

								<div class="clearfix"></div>

								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Opened By <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txtPortLoginId" name="txtPortLoginId" tabindex="26" placeholder="Portal Login ID" maxlength="200">
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Company <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<select class="form-control V_Combo" id="ddlClient" name="ddlClient" tabindex="9" >
											<option value="0">Select</option>
											<option value="3">CLIENT1</option>
											<option value="4">CLIENT2</option>
											<option value="5">CLIENT3</option>
										</select>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Contact Type <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<div class="mt-radio-inline">
											<label class="mt-radio">
												<input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked=""> Option 1
												<span></span>
											</label>
											<label class="mt-radio">
												<input type="radio" name="optionsRadios" id="optionsRadios26" value="option2" checked=""> Option 2
												<span></span>
											</label>
										</div>
									</div>
								</div>

								<div class="clearfix"></div>

								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Service Location <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<div class="mt-checkbox-inline">
											<label class="mt-checkbox">
												<input type="checkbox" name="optionsRadios" id="optionsRadios25" value="option1" checked=""> Option 1
												<span></span>
											</label>
											<label class="mt-checkbox">
												<input type="checkbox" name="optionsRadios" id="optionsRadios26" value="option2" checked=""> Option 2
												<span></span>
											</label>
										</div>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">State <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<select class="form-control V_Combo" id="ddlClient" name="ddlClient" tabindex="9" >
											<option value="0">Select</option>
											<option value="3">CLIENT1</option>
											<option value="4">CLIENT2</option>
											<option value="5">CLIENT3</option>
										</select>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Client Name<span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txtPortLoginId" name="txtPortLoginId" tabindex="26" placeholder="Portal Login ID" maxlength="200">
									</div>
								</div>
								
								<div class="clearfix"></div>	

								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Service Location <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txtPortLoginId" name="txtPortLoginId" tabindex="26" placeholder="Portal Login ID" maxlength="200">
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">State <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<select class="form-control V_Combo" id="ddlClient" name="ddlClient" tabindex="9" >
											<option value="0">Select</option>
											<option value="3">CLIENT1</option>
											<option value="4">CLIENT2</option>
											<option value="5">CLIENT3</option>
										</select>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Client Name<span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txtPortLoginId" name="txtPortLoginId" tabindex="26" placeholder="Portal Login ID" maxlength="200">
									</div>
								</div>
								
								<div class="clearfix"></div>	

								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Service Location <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txtPortLoginId" name="txtPortLoginId" tabindex="26" placeholder="Portal Login ID" maxlength="200">
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">State <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<select class="form-control V_Combo" id="ddlClient" name="ddlClient" tabindex="9" >
											<option value="0">Select</option>
											<option value="3">CLIENT1</option>
											<option value="4">CLIENT2</option>
											<option value="5">CLIENT3</option>
										</select>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Client Name<span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txtPortLoginId" name="txtPortLoginId" tabindex="26" placeholder="Portal Login ID" maxlength="200">
									</div>
								</div>
								
								<div class="clearfix"></div>	

								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Description <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" placeholder="Enter ..." id="txtDesc" name="txtDesc"></textarea>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Additional comments <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" placeholder="Enter ..." id="txtDesc" name="txtDesc"></textarea>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="firstname" class="col-sm-4 control-label">Navision Message<span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" placeholder="Enter ..." id="txtDesc" name="txtDesc"></textarea>
									</div>
								</div>
							
								<div class="clearfix"></div>
								<hr />
								
								<div class="tabbable-custom" style="padding:0 5px;">
									<ul class="nav nav-tabs ">
										<li class="active">
											<a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Related Records </a>
										</li>
										<li class="">
											<a href="#tab_5_2" data-toggle="tab" aria-expanded="false"> Closure Information </a>
										</li>
										<li class="">
											<a href="#tab_5_3" data-toggle="tab" aria-expanded="false"> Billing </a>
										</li>
										<li class="">
											<a href="#tab_5_4" data-toggle="tab" aria-expanded="false"> Shipping  </a>
										</li>
										<li class="">
											<a href="#tab_5_5" data-toggle="tab" aria-expanded="false"> Notes </a>
										</li>
										<li class="">
											<a href="#tab_5_6" data-toggle="tab" aria-expanded="false"> Escalation </a>
										</li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab_5_1">
											<p> I'm in Section 1. </p>
											<p> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
												consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
												consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequatDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
												consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequatDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
												consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequatDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
												consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
										</div>
										<div class="tab-pane" id="tab_5_2">
											<p> Howdy, I'm in Section 2. </p>
											<p> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
												consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation. </p>
											<p>
												<a class="btn green" href="ui_tabs_accordions_navs.html#tab_5_2" target="_blank"> Activate this tab via URL </a>
											</p>
										</div>
										<div class="tab-pane" id="tab_5_3">
											<p> Howdy, I'm in Section 3. </p>
											<p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel
												eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
											<p>
												<a class="btn yellow" href="ui_tabs_accordions_navs.html#tab_5_3" target="_blank"> Activate this tab via URL </a>
											</p>
										</div>
										<div class="tab-pane" id="tab_5_4">
											<p> Howdy, I'm in Section 3. </p>
											<p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel
												eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
											<p>
												<a class="btn yellow" href="ui_tabs_accordions_navs.html#tab_5_3" target="_blank"> Activate this tab via URL </a>
											</p>
										</div>
										<div class="tab-pane" id="tab_5_5">
											<p> Howdy, I'm in Section 3. </p>
											<p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel
												eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
											<p>
												<a class="btn yellow" href="ui_tabs_accordions_navs.html#tab_5_3" target="_blank"> Activate this tab via URL </a>
											</p>
										</div>
										<div class="tab-pane" id="tab_5_6">
											<p> Howdy, I'm in Section 3. </p>
											<p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel
												eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
											<p>
												<a class="btn yellow" href="ui_tabs_accordions_navs.html#tab_5_3" target="_blank"> Activate this tab via URL </a>
											</p>
										</div>
									</div>
								</div>
								
						</div>
						
						

						</form>
					</div>
					</div>
					<!-- Tickets Form End -->	

					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>
					
					
					
<script>

	$(document).ready(function() {
		$('.full_height_block').height($(window).height() - 133);
	});
	$(document).ready(function() {
		$('.tickets_data1').height($(window).height() - 180);
	});

	$(".parent .rightbar").click(function(){
	  
		if($('.tickets_tabs').css('display') == 'none'){ 
			$(".parent").removeClass('col-md-12');
			$(".parent").addClass('col-md-10');
			$(".tickets_tabs").show(); 
		} else { 
			$(".parent").removeClass('col-md-10');
			$(".parent").addClass('col-md-12');
			$(".tickets_tabs").hide(); 
		}
	
	});
</script>					