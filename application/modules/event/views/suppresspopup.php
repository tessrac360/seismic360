<link href="<?php echo base_url() . "public/" ?>datapicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<style>
    .xdsoft_noselect{z-index:99999 !important;}
    .custom-model-content{margin-top:30px;}
    .custom-model-content .modal-body{height: 25vh !important;}
</style>
<div class="modal-content custom-model-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Suppress E-<?php echo str_pad($event_id, 10, '0', STR_PAD_LEFT) ?></h4>
    </div>
    <div class="modal-body ">					    
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <ul class="nav nav-tabs tabs-left">
                        <li class="active">
                            <a href="#actionsupress" data-toggle="tab" style="font-size:13px;color:#000;"><b>Temporary</b></a>
                        </li>
                        <li>
                            <a href="#actionpermanent" data-toggle="tab" style="font-size:13px;color:#000;"><b>Permanent</b></a>
                        </li>
                       <!-- <li>
                            <a href="#ack" data-toggle="tab" style="font-size:13px;color:#000;"><b>Acknowledgement</b></a>
                        </li> -->
                    </ul>                    
                </div>

                <div class="col-md-9 col-sm-9 col-xs-9">
                    <div class="tab-content">

                        <div class="tab-pane active" id="actionsupress">
                            <form id ="frm_Supress_pupup" method="post" action="">
                                <div class="form-group col-md-12">
                                    <label for="firstname" style="margin-left:0px;" class="col-sm-3 control-label">Select Date :</label>
                                    <div class="col-sm-9">
                                        <div class="input-group date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d" style="width:100%;">
                                            <input type="text" class="form-control datetimeSupress" name="datetimeSupress" id="datetimeSupress" style="width:100%;">
                                            <span id="msg_datetimeSupress"></span>
                                        </div>  
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12">
                                    <label for="firstname" style="margin-left:0px;" class="col-sm-3 control-label">Comment :</label>
                                    <div class="col-sm-9">
                                        <textarea style="width:100%;" class="form-control" id="txtsurpress" name="txtsurpress" placeholder="Comment !"></textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group col-md-12">
                                    <label for="firstname" style="margin-left:0px;" class="col-sm-3 control-label hidden-xs">&nbsp;</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" value="<?php echo $event_id; ?>" name="su_event_id">
                                        <input type="hidden" value="T" name="ack_type">
                                        <input type="hidden" value="<?php echo $events['resultSet']['client_id']; ?>" name="client_id">
                                        <button class="btn btn-success pull-right" style="padding:5px;" type="submit" id="btnsurpresspupup" >Save</button>
                                    </div>
                                </div>															
                            </form>
                        </div>

                        <div class="tab-pane fade" id="actionpermanent">
                            <div class="col-sm-12">
                                <form id ="frm_Supress_permanent_pupup" method="post" action="">
                                    <div class="clearfix"></div>
                                <div class="form-group col-md-12">
                                    <label for="firstname" style="margin-left:0px;" class="col-sm-3 control-label">Comment :</label>
                                    <div class="col-sm-9">                                         
                                        <textarea style="width:100%;" class="form-control" id="txtsurpress" name="txtsurpress" placeholder="Comment !"></textarea>
                                        <span id="msg_manualack"></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group col-md-12">
                                    <label for="firstname" style="margin-left:0px;" class="col-sm-3 control-label hidden-xs">&nbsp;</label>
                                    <div class="col-sm-9">
                                    <input type="hidden" value="P" name="ack_type">
                                    <input type="hidden" value="<?php echo $event_id; ?>" name="su_event_id" >                                                              
                                    
                                    <input type="hidden" value="<?php echo $events['resultSet']['client_id']; ?>" name="client_id">
                                    <button class="btn btn-success pull-right" style="padding:5px;" id="btnsurpresspermanentpupup">Save</button>
                                    </div>
                                </div>
									
									
									
									
                                </form>
                            </div>
                        </div> 


                        <div class="tab-pane" id="ack">
                            <form id ="frm_acknowledgement_popup" method="post" action="">                                
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12">
                                    <label for="firstname" style="margin-left:0px;" class="col-sm-3 control-label">Acknowledgement :</label>
                                    <div class="col-sm-9">                                         
                                        <textarea style="width:100%;" class="form-control" id="txtacknowledgement" name="txtacknowledgement" placeholder="Comment !"></textarea>
                                        <span id="msg_manualack"></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group col-md-12">
                                    <label for="firstname" style="margin-left:0px;" class="col-sm-3 control-label hidden-xs">&nbsp;</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" value="<?php echo $event_id; ?>" name="su_event_id">
                                        <input type="hidden" value="A" name="ack_type">
                                        <input type="hidden" value="<?php echo $events['resultSet']['client_id']; ?>" name="client_id">
                                        <button class="btn btn-success pull-right" style="padding:5px;" type="submit" id="btnackpupup" >Save</button>
                                    </div>
                                </div>															
                            </form>
                        </div>
                    </div>                                       
                </div>
            </div>	 
        </div>																
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>datapicker/onejquery.datetimepicker.full.js"></script>
<script>

    $('.datetimeSupress').datetimepicker({
        format: 'Y-m-d H:i',
        mask: true,
        dayOfWeekStart: 1,
        lang: 'en',
        timepicker: true,
        showTimePicker: true,
        scrollMonth: false,
        scrollTime: false,
        scrollInput: false,
        minDate: 0,
        step: 5
    });
</script>