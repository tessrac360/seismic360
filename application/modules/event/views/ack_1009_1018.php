<link href="<?php echo base_url() . "public/" ?>datapicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<style>
    .xdsoft_noselect{z-index:99999 !important;}
</style>
<?php //pr($events) ?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title" style="font-size:15px;"><b>Basic Info</b></h3>
    </div>
    <div class="modal-body">

        <table class="table table-striped table-bordered table table-hover" id="tblGrid">
            <thead>
                <tr>
                    <th class="bold-text1">CLIENT :</th>
                    <th class="bold-text1">HOST NAME :</th>
                    <th class="bold-text1">HOST IP :</th>

                </tr>
            </thead>
            <tbody class="bold-text">
                <tr>

                    <td class="normal-text"><?php echo $events['resultSet']['client_title'] ?></td>    
                    <td class="normal-text"><?php echo $events['resultSet']['device_name'] ?></td>
                    <td class="normal-text"><?php echo $events['resultSet']['ip_address'] ?></td>
                </tr>

            </tbody>
        </table>
        <div class="portlet"> 
            <div class="portlet-body">                                  
                <div class="tabbable-line">
                    <div class="portlet light ">									
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#eventhistory" data-toggle="tab" style="font-size:13px;color:#000;"><b>Event History </b></a>
                                </li>
                                <li>
                                    <a href="#monitorservices" data-toggle="tab" style="font-size:13px;color:#000;"><b> Monitor Services</b></a>
                                </li>
                                <li>
                                    <a href="#activitylog" data-toggle="tab" style="font-size:13px;color:#000;"><b> Suppress Action</b></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="eventhistory">
                                    <table class="table table-striped table-bordered table table-hover" style="text-align:center;" id="tblGrid">   
                                        <thead style="background:#17c4bb;">
                                            <tr>
                                                <th class="bold-text1" style="color:#fff;text-align:center;">Host Name</th>
                                                <th class="bold-text1" style="color:#fff;text-align:center;">Service</th>
                                                <th class="bold-text1" style="color:#fff;text-align:center;">Description</th>
                                                <th class="bold-text1" style="color:#fff;text-align:center;">State</th>
                                                <th class="bold-text1" style="color:#fff;text-align:center;">Event Start Time</th>
                                            </tr>
                                        </thead>
                                        <tbody class="bold-text">
                                            <?php foreach ($eventsHistory['resultSet'] as $historyData) { ?>
                                        <tr>
                                            <td class="normal-text1"><?php echo $historyData['device_name'] ?></td>
                                            <td class="normal-text1"><?php echo $historyData['service_name'] ?></td>
                                            <td class="normal-text1"><?php echo $historyData['event_text'] ?></td>
                                            <td class="normal-text1"><?php echo $historyData['severity'] ?></td>
                                            <td class="normal-text1"><?php echo $historyData['event_start_time'] ?></td>


                                        </tr>  
                                    <?php } ?>								 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="monitorservices">
                                    <table class="table table-striped table-bordered table table-hover" style="text-align:center;" id="tblGrid">   
                                        <thead style="background:#17c4bb;">
                                            <tr>
                                                <th class="bold-text1" style="color:#fff;text-align:center;">Service</th>
                                                <th class="bold-text1" style="color:#fff;text-align:center;">Event Text</th>
                                                <th class="bold-text1" style="color:#fff;text-align:center;">Severity</th>
                                                <th class="bold-text1" style="color:#fff;text-align:center;">Start Time</th>
                                                <th class="bold-text1" style="color:#fff;text-align:center;">Duration</th>
                                            </tr>
                                        </thead>
                                        <tbody class="bold-text">
                                            <?php foreach ($eventMonitorService['resultSet'] as $historyData) { ?>
                                        <tr>

                                            <td class="normal-text1"><?php echo $historyData['service_name'] ?></td>
                                            <td class="normal-text1"><?php echo $historyData['event_text'] ?></td>
                                            <td class="normal-text1"><?php echo $historyData['severity'] ?></td>
                                            <td class="normal-text1"><?php echo $historyData['event_start_time'] ?></td>
                                            <td class="normal-text1"><?php echo $historyData['Duration'] ?></td>								


                                        </tr>  
                                    <?php } ?>								 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="activitylog">                         
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <ul class="nav nav-tabs tabs-left">
                                                    <li class="active">
                                                        <a href="#supress" data-toggle="tab" style="font-size:13px;color:#000;"><b>SUPPRESS</b></a>
                                                    </li>
                                                    <li>
                                                        <a href="#permanent" data-toggle="tab" style="font-size:13px;color:#000;"><b>PERMANENT</b></a>
                                                    </li>

                                                </ul>
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <div class="tab-content">
                                                    
                                                    <div class="tab-pane active" id="supress">
                                                        <form id ="frm_Supress" method="post" action="">
                                                        <label class="control-label col-md-3 bold-text1">Select Date :</label>
                                                        <div>
                                                            <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                                                <input type="text" class="form-control datetimeSupress" name="datetimeSupress" id="datetimeSupress">
                                                                <span id="msg_datetimeSupress"></span>
                                                            </div>  
                                                             
                                                            <br>
                                                        </div>
                                                        <label class="control-label col-md-3 bold-text1">Comment :</label>
                                                        <div>
                                                            <div class="input-icon">
                                                                <textarea style="width:28%;" id="txtsurpress" name="txtsurpress" placeholder="Comment !"></textarea>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="col-sm-5">
                                                            <input type="hidden" value="<?php echo $event_id;?>" name="su_event_id">
                                                            <input type="hidden" value="T" name="su_action">
                                                            <button class="btn btn-success pull-right" style="padding:5px;" type="submit" id="btnsurpress" >Save</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    
                                                    <div class="tab-pane fade" id="permanent">
                                                        <div class="col-sm-1">
                                                            <form id ="frm_Supress_permanent" method="post" action="">
                                                                <input type="hidden" value="<?php echo $event_id;?>" name="su_event_id">
                                                                <input type="hidden" value="P" name="su_action">
                                                                <button class="btn btn-success pull-right" style="padding:5px;" id="btnsurpresspermanent">Save</button>
                                                            </form>
                                                            
                                                        </div>
                                                    </div>                                              
                                                </div>
                                            </div>
                                        </div>
                                    </div>                        
                                </div>                                  
                            </div>                                 
                        </div>
                    </div>						
                </div>     
            </div>
        </div> 
    
    </div><!-- /.modal-content -->
    <script src="<?php echo base_url() . "public/" ?>datapicker/onejquery.datetimepicker.full.js"></script>
    <script>

        $('.datetimeSupress').datetimepicker({
            format: 'Y-m-d H:i',
            mask: true,
            dayOfWeekStart: 1,
            lang: 'en',
            timepicker: true,
            showTimePicker: true,
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false,
            minDate: 0,
            step: 5
        });
    </script>