<style>
    .inline-form-body div[class*='col-']{padding:0;}
    .inline-form-body div[class*='col-'] .btn-group, .inline-form-body div[class*='col-'] .multiselect.dropdown-toggle.btn, .inline-form-body div[class*='col-'] .input-group.input-medium.date-picker, .inline-form-body div[class*='col-'] button[type="submit"]{width:100% !important;}

    .inline-form-body div[class*='col-'] button[type="submit"]{width:49% !important; margin-left: 3px;}
    .inline-form-body div[class*='col-'] button[type="button"]{width:44% !important;}

    .inline-form-body div[class*='col-'] .btn-group{border-right:0;padding-right:3px;      }
    .inline-form-body div[class*='col-'] .multiselect.dropdown-toggle.btn{text-align: left;}
    .inline-form-body div[class*='col-'] .multiselect.dropdown-toggle.btn .caret{ position:absolute; right:10px; top:50%;}
    .inline-form-body div[class*='col-'] .multiselect.dropdown-toggle.btn span{ overflow: hidden; width: 92%; display: block;}
    .inline-form-body div[class*='col-'] .multiselect.dropdown-toggle.btn-default:hover{border-color: #ccc;}
    .input-daterange .input-group-addon{padding: 3px 5px 1px 5px;}
    .portlet-body .form-body { padding: 0 5px 0 5px !important;}
</style>


<!-- END PAGE HEADER-->
<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Event Suppress</span>
            </div>
<!--            <div class="page-toolbar">           
                <button href="#costom_search" data-toggle="collapse" class="btn tooltips" data-container="body" data-placement="bottom" data-original-title="Search" data-placement="top" style="background: #16c4bb; color: #fff; padding: 3px 6px; position: relative; top: 1px;"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>-->
            <div class="clearfix"></div>
        </div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                
                <table class="table table-striped table-bordered table-checkable order-column" id="role">
                    <thead style="background: #17C4BB !important; color:white;">
                        <tr>
                            <th width="130"> Event ID </th>                           
                            <th > Suppress Start</th>
                            <th> Suppress End </th>    
							<th> Notes</th>							
                            <th width="150"> Duration </th>                              
                            <th> Suppress Type</th>
                            <th> Action </th>                           
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($eventSuppress)) {
                            foreach ($eventSuppress as $value) {
                                 //pr($value);
                                ?>
                                <tr class="event_<?php echo $value->event_id; ?>">
                                    <td> <?php echo 'E-' . str_pad($value->event_id, 10, '0', STR_PAD_LEFT); ?> </td>
                                    <td> <?php echo $value->suppress_starttime; ?>  </td>
                                    <td> <?php echo $value->suppress_endtime; ?>  </td> 
<td> <?php echo $value->ack_notes; ?>  </td>									
                                    <td> <?php echo $value->duration; ?>  </td> 
                                    <td> <?php echo ($value->ack_type == 'P') ? 'Permanent' : 'Temporary'; ?>  </td> 
                                    <td> 
                                        
                                        <a title="Suppress" href="javascript:void(0);" eventSlug="<?php echo 'E-' . str_pad($value->event_id, 10, '0', STR_PAD_LEFT); ?>" class="reactivateSuppress" ack_type="<?php echo $value->ack_type; ?>"  ackId="<?php echo $value->ack_id; ?>" evtid="<?php echo $value->event_id; ?>">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        </a>  
                                    </td> 

                                </tr> 
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="6"> No Record Found </td>
                            </tr> 
                        <?php } ?> 
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_event_history.js" type="text/javascript"></script>