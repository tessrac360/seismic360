<style type="text/css">
    body{font-size:13px;}
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 3px 6px;
        font-size: 12px !important;
    }

    .form-search .control-label{font-size:13px; text-align:right;}
    .form-search .form-control{height:24px; padding:0px 8px; font-size:13px;}
    .form-search .form-group{height: 16px;margin-bottom: 12px;}

    .page-toolbar .input-group{width: 245px;}
    .page-toolbar .input-group .btn.btn-default{padding: 4px 12px;}
    .page-toolbar .input-group .form-control{padding: 4px 12px !important; height: 30px;}
    table thead tr th, table tbody tr td{white-space: nowrap;}
    .dataTables_scrollHeadInner table.display.dataTable.no-footer tr th{ white-space: nowrap; font-weight:400 !important; font-size:12px !important; padding:3px 6px !important;}
    table tbody tr td { white-space: nowrap; font-weight:400 !important; font-size:12px !important; padding:3px 6px !important;}
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{bottom:3px;}
    table.dataTable, .dataTables_scroll{margin-top:0 !important; margin-bottom:0;}
    input[type="search"]{border: 1px solid #c2cad8 !important;}
    .dataTables_info{    position: absolute;  top: -6px; font-size:13px;}
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/css/fSelect.css">
<!-- END PAGE HEADER-->
<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Tasks</span>
            </div>
            <!--<div class="page-title">
            <?php
            //if($myincidents== 'no'){
            //if ($queue['status'] == 'true') {
            //foreach ($queue['resultSet'] as $value) {
            ?>
                                    
                    <a href="<?php //echo base_url() . 'event/ticket/queueFilter?qid='.$value['queue_id']; ?>" class="btn btn-xs  blue"><?php //echo $value['queue_name'];?></a>
            <?php //} ?>
<?php //}  ?>
<?php //}  ?>
            </div>-->

            <div class="clearfix"></div>
        </div>


        <form class="form-search" style="padding:10px 0; background: rgba(255, 255, 255, 0.78);">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Client</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange client_id" id="id" name="id[]" multiple="multiple">
                                <option value="">Select Option</option>
                                <?php
                                if ($clients['status'] == 'true') {
                                    foreach ($clients['resultSet'] as $value) {
                                        if ($value['id'] != 1) {
                                            ?>
                                            <option value="<?php echo $value['id']; ?>" <?php if (isset($_GET['client_id']) && $_GET['client_id'] == $value['id']) {
                                    echo "selected";
                                } ?>><?php echo $value['client_title']; ?></option>
        <?php
        }
    }
}
?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Status</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange status_code" id="status_code" name="status_code[]" multiple="multiple">
                                <option value="">Select Option</option>
<?php
if ($ticketState['status'] == 'true') {
    foreach ($ticketState['resultSet'] as $value) {
        ?>
                                        <option value="<?php echo $value['status_code']; ?>" <?php if (isset($_GET['status_id']) && $_GET['status_id'] == $value['status_code']) {
            echo "selected";
        } ?>><?php echo $value['status']; ?></option>
    <?php }
}
?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Category</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange ticket_cate_id" id="ticket_cate_id" name="ticket_cate_id[]" multiple="multiple">
                                <option value="">Select Option</option>
<?php
if ($category['status'] == 'true') {
    foreach ($category['resultSet'] as $value) {
        ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['category']; ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Sub Category</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange ticket_sub_cate_id" id="ticket_sub_cate_id" name="ticket_sub_cate_id[]" multiple="multiple">
                                <option value="">Select Option</option>
<?php
if ($subcategory['status'] == 'true') {
    foreach ($subcategory['resultSet'] as $value) {
        ?>
                                        <option value="<?php echo $value['id']; ?>" <?php if (isset($_GET['alarmtype']) && $_GET['alarmtype'] == $value['id']) {
            echo "selected";
        } ?>><?php echo $value['subcategory']; ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Assigned To</label>
                        <div class="col-md-8">
                                <?php if ($mytasks == 'ok') { ?>
                                <select  class="form-control onchange" id="user_id" name="user_id" disabled>
                                <?php
                                if ($clientUsers['status'] == 'true') {
                                    foreach ($clientUsers['resultSet'] as $value) {
                                        ?>
                                            <option value="<?php echo $value['id']; ?>" <?php if ($user_id == $value['id']) {
                                            echo "selected";
                                        } ?>><?php echo $value['first_name']; ?></option>
        <?php }
    }
    ?>
                                </select>						
<?php } else { ?>
                                <select  class="form-control onchange user_id" id="user_id" name="user_id">
                                    <option value="">Select Option</option>
                                    <?php
                                    if ($clientUsers['status'] == 'true') {
                                        foreach ($clientUsers['resultSet'] as $value) {
                                            ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['first_name']; ?></option>
        <?php }
    }
    ?>
                                </select>							
<?php } ?>

                        </div>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Severity</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange severity_id" id="severity_id" name="severity_id[]" multiple="multiple">
                                <option value="">Select Option</option>
<?php
if ($severity['status'] == 'true') {
    foreach ($severity['resultSet'] as $value) {
        ?>
                                        <option value="<?php echo $value['skill_id']; ?>"><?php echo $value['title']; ?></option>
    <?php }
}
?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Urgency</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange priority_id" id="priority_id" name="priority_id[]" multiple="multiple">
                                <option value="">Select Option</option>
<?php
if ($priority['status'] == 'true') {
    foreach ($priority['resultSet'] as $value) {
        ?>
                                        <option value="<?php echo $value['urgency_id']; ?>"><?php echo $value['name']; ?></option>
    <?php }
}
?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label col-md-4">Date</label>
                        <div class="col-md-8">
                            <select  class="form-control onchange date" id="date" name="date">
                                <option value="">Select Option</option>
                                <option value="1">Today</option>
                                <option value="2">Yesterday</option>
                                <option value="3">This Week</option>
                                <option value="4">Last Week</option>
                                <option value="5">This Month</option>
                                <option value="6">Last Month</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="user" name="user" value="<?php echo $user_id; ?>">
            <input type="hidden" id="mytasks" name="mytasks" value="<?php echo $mytasks; ?>">
            <!--<div><button class="btn btn-green green pull-right" style="padding: 1px 4px; font-size: 13px; margin-right: 11px;">Advance Search</button></div>-->
            <div class="clearfix"></div>
        </form>

        <div class="clearfix"></div>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0 full-height-content full-height-content-scrollable" style="margin-left: 0; margin-right: 0;" id = "list">
            <div class="portlet-body" style="padding-top:0;">
                <div class="col-md-12" style="overflow:auto;padding:6px 3px;float:left;">
                    <table id="employee-grid"  class="eventTable tablesorter custom-popup bordered cellpadding-0 cellspacing-0 table table-bordered table-hover" cellspacing="0" width="100%">
                        <thead style="background: #17C4BB !important; color:white;">
                            <tr>
                                <th style="width:12%">Task Number</th>
                                <th style="width:12%">Request Date & Time</th>
                                <th>Client</th>
                                <th>Requestor</th>
                                <th>Created By</th>
                                <th>Summary</th>
                                <th>Assigned To</th>
                                <th>State</th>
                                <th>Urgency</th>
                                <th>Severity</th>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Last Updated</th>
                                <th>Closed On</th>


                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
<?php // echo $this->pagination->create_links();  ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

<script>
    $(function ()
    {

    });
</script> 

<script type="text/javascript" language="javascript" >
    $(document).ready(function () {
        var dataTable;
        getIncidents();
    });

    //$('.onchange').on('change', function () {
    function changeFilter()
    {
        dataTable.destroy();
        getIncidents();
    }//} );

    function getIncidents() {

        var url_string = document.URL;
        var url = new URL(url_string);
        var livestatust = url.searchParams.get("livestatust");
        var breachedstatust = url.searchParams.get("breachedstatust");

        var baseURL = $('#baseURL').val();
        var clientId = $('#id').val();
        //alert(clientId);
        var status_code = $('#status_code').val();
        var ticket_cate_id = $('#ticket_cate_id').val();
        var ticket_sub_cate_id = $('#ticket_sub_cate_id').val();
        var user_id = $('#user_id').val();
        var severity_id = $('#severity_id').val();
        var priority_id = $('#priority_id').val();
        var date = $('#date').val();
        var mytasks = $('#mytasks').val();
        $(".dataTables_scrollHeadInner").css({"width": "100%", "padding-left": "0"});
        $("#employee-grid").css({"width": "100%"});

        dataTable = $('#employee-grid').DataTable({
            serverSide: true,
            stateSave: true,

            "aaSorting": [[0, "desc"]],
            ajax: {
                url: baseURL + 'event/tasks/getIncidentData', // json datasource
                type: "post", // method  , by default get
                data: {livestatust:livestatust,breachedstatust:breachedstatust,clientId: clientId, status_code: status_code, ticket_cate_id: ticket_cate_id, ticket_sub_cate_id: ticket_sub_cate_id, user_id: user_id, severity_id: severity_id, priority_id: priority_id, date: date, mytasks: mytasks},
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="20">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");
                }
            },
            columnDefs: [{
                    "targets": 0,

                    "render": function (data, type, full, meta) {
                        return '<a href="tasks/editTicket/' + full[14] + '">' + full[0] + '</a>';
                    }
                }],
            dom: "frtiS",
            scrollY: 387,
            //pageLength: 20,
            scrollX: "100%",
            "aaSorting": [[0, "desc"]],
            deferRender: true,
            deferLoading: 57,
            scrollCollapse: true,
            scroller: {
                loadingIndicator: true
            }
        });

        dataTable.order([0, 'desc']);
        //dataTable .draw();
    }


</script>

<script>
    (function ($) {
        $(function () {
            $('.client_id').fSelect();
            $('.status_code').fSelect();
            $('.ticket_cate_id').fSelect();
            $('.ticket_sub_cate_id').fSelect();
            $('.user_id').fSelect();
            $('.severity_id').fSelect();
            $('.priority_id').fSelect();
            $('.date').fSelect();
        });
    })(jQuery);
</script>



<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/datatable-scroller-server-side/js/jquery.dataTables.js" type="text/javascript"></script>	
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/datatable-scroller-server-side/js/dataTables.scroller.js" type="text/javascript"></script>	
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/fSelect.js" type="text/javascript"></script>	