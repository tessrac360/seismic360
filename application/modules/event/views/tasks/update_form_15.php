<style>
	.amcharts-title{color:#000; fill:black;}
	.amcharts-title{
  transform: translate(145px,30px)
}
.amcharts-title:nth-child(2){
  transform: translate(160px, 150px)
}
	
		@media (min-width: 992px){
		.page-footer {
			background-color: #fff !important;
		}
		.page-content-wrapper .page-content{
			padding:0;
		}
		}
		.page-title {
			font-size: 17px;
			background: rgba(204, 204, 204, 0.22);
			padding: 6px 9px;
			margin: 0 0 10px;
		}
		.row {
			margin-left: 0;
			margin-right:0;
		}
		.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{
			padding-left: 5px;
			padding-right: 5px;
		}
		.portlet{
			margin-bottom: 9px;
		}
		.page-header.navbar .page-logo .logo-default {margin: 18px 0 0 0;}
		
		@media (min-width: 992px){
			.home-alerts .page-sidebar1.alerts-right {
				float: right !important;
				position:relative !important;
				margin-right: 0  !important;
				margin-left: -100%  !important;
				top:0;
				z-index:9;
				    padding: 0;
			}
			.home-alerts .page-content-wrapper .page-content {
				margin-right: 290px !important;
			}
			.page-sidebar-closed1.home-alerts .page-content-wrapper .page-content{margin-right: 0 !important;}
			.page-sidebar-closed1 .page-sidebar1 { width: 0px !important; display: block!important; height: auto!important; padding-bottom: 0; overflow: hidden !important;}
			
			.mar-lr3{margin-left:1px !important; margin-right:1px !important;}
			.pad-tlr-3{padding-top:3px !important; padding-left:3px; padding-right:3px;}
			.remove_pad{padding:0 !important;}
			.remove_pad_left{padding-left:0;}
			.remove_pad_right{padding-right:0px;}
			.pad_right3{padding-right:3px !important;}
			}
		
		.mar-bottom{margin-bottom:0;}
		
		
		.tickets_data.col-md-10{padding-left:0;}

.portlet>.portlet-title>.caption {
    float: left;
    display: inline-block;
    line-height: 17px;
    padding: 6px 0 4px 0;
}

.portlet.light>.portlet-title.portlet-bg-title {
    min-height: 34px;
    margin-bottom: 6px;
    background: #17c4bb !important;
    padding: 3px 4px 3px 7px;
}

.page-bar {
    padding: 0px;
	float: right;
	background-color: transparent;
margin-bottom:0;
}

.portlet.light>.portlet-title>.caption>.caption-subject { font-size: 13px; text-transform: capitalize !important;     font-weight: 600 !important;
    color: #fff; }
.portlet.light>.portlet-title>.caption{padding: 3px 0;}

.form-control{height: 26px; font-size: 13px}
.tickets_data_header .btn-default.btn-search{padding: 4px 8px 3px 8px !important; font-size: 12px;margin-right: 0 !important;}
.tickets_data_header .btn.btn-toggle{    padding:1px 5px 1px 5px; font-size: 12px; background: #fff; border: 0; margin-right:6px; position:relative; top:-1px;}
.padd8{padding:8px;}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
        color: #ffffff !important;
        background-color: #17c4bb !important;
        border: 1px solid #ddd;
        border-bottom-color: transparent;
        cursor: default;
        .modal-header{padding:5px 10px !important;}
    }

.redarrow { color: red; }
.custom-padding-formgp hr{margin:5px 0;}
.custom-padding-formgp .form-group{margin-bottom:4px;}
.custom-padding-formgp .form-group .form-control { border-radius: 0; padding: 3px 6px; height: 30px;     background: #fff;}
.custom-padding-formgp .form-group textarea.form-control { height:auto;}
.custom-padding-formgp .form-group .col-sm-8 { padding-right:0; }
.custom-padding-formgp .form-group .col-sm-4 { padding-left: 0; font-size: 12px; line-height: 30px; margin-bottom: 0; text-align:right;}
.custom-padding-formgp .form-group .mt-checkbox-inline, .custom-padding-formgp .form-group .mt-radio-inline { padding: 5px 0;}  
.custom-padding-formgp .form-group .mt-checkbox, .custom-padding-formgp .form-group .mt-radio {margin-bottom: 0; font-size:13px;}  

.custom-padding-formgp .nav>li>a {padding: 5px 15px; font-size:13px; font-weight:400 !important;}
.btn-save{padding: 4px 12px; margin-left:4px; border:0;font-size: 12px; position:relative; top:1px;}

.tab-content{max-height:200px; min-height: 200px; overflow-y:scroll; font-size:13px;}
.tab-content .bold-text{font-weight:400;}
.tabbable-custom>.nav-tabs>li.active>a:hover, .nav-tabs>li.active>a{border-color: #d4d4d4 #transparent !important;background: #fff !important; color: #000 !important;}	
.tabbable-custom .nav-tabs>li.active>a, .tabbable-custom  .nav-tabs>li.active>a:focus, .tabbable-custom  .nav-tabs>li.active>a:hover{color:#000 !important; background:#fff !important;}
.page_title_sub{font-size: 14px; font-weight:600; background: rgba(204, 204, 204, 0.22); padding: 4px 9px; margin: 0 0 3px;}
	</style>

	<style type="text/css">
    .highlight {
        background:#f3f4f6;
        border:2px solid #17C4BB;
    }

    .modal-dialog {width: 90%;margin: 30px auto;}
    .modal .modal-header{background:#17C4BB;color:#fff;font-weight:bold;}
    .bold-text{font-weight:bold;width:15%;}
    .normal-text{font-weight:400;}
    .bold-text1{font-weight:bold;width:15%;color:#000;}
    .normal-text1{font-weight:400;color:#000;}
	
	
	#actionmodal .modal-body, #myModal .modal-body{height: 85vh; overflow: auto;}
	.modal-body .nav-tabs>li a b{font-weight:600 !important;}
	.modal-body .nav-tabs>li a{padding: 6px 5px !important;}
	.modal-body .bold-text1{font-weight:600 !important; font-size:13px; text-transform:capitalize !important;}
	.modal-body hr{margin:3px 0 !important;}
	.modal-body .table{margin-bottom:0 !important;}
	.modal-body .form-group{margin-bottom:6px !important;}
	.modal-body .form-group .control-label{font-size:12px !important;}
	.modal-header{padding: 6px 10px !important;}
	.modal .modal-header .close { margin-top: 8px !important; }

    
	.portlet{margin-bottom:0;}
	.portlet.light{
		padding:0;
	}
	.portlet.light>.portlet-title {
		min-height: 35px;
		margin-bottom: 0;
		background:rgba(231, 233, 236, 0.62) !important;
		padding: 3px 8px;
	}
	.page-bar{background-color: transparent;}
	.portlet.light>.portlet-title>.caption, .btn#btn_eventSearch {
		padding: 6px 0 4px 0;
	}
	.btn#btn_eventSearch {
		padding: 4px 12px;
	}
	.portlet.light>.portlet-title>.caption>.caption-subject {
		font-size: 13px;
		text-transform: capitalize !important;
	}
	.form-control{
		padding: 4px 12px !important;
		height: 30px;
	}
	
	#myModal .nav-tabs>li:last-child{display:none;}
	.assign .input-group-addon{
		border-left: 0 !important;
		background: #17c4bb !important;
		color: #fff !important;
		border: 0 !important;
		cursor: pointer;
	}
	.assign .input-group-addon i{color:#fff;}
	.assign .input-group{ width: 100.2%;}

</style>
<style>
<!-- Activity Log Description column CSS -->	
	
div.text-container {
    margin: 0 auto;
    width: 75%;    
}

.hideContent {
    overflow: hidden;
    line-height: 1em;
    height: 2em;
}

.showContent {
    line-height: 1em;
    height: auto;
}
.showContent{
    height: auto;
}

h1 {
    font-size: 24px;        
}
p {
    padding: 10px 0;
}
.show-more {
    padding: 10px 0;
    text-align: center;
}

td {
    width:200px;
    border: solid 1px;
}
}
</style>
<link href="<?php echo base_url() . "public/" ?>datapicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<div>
	<h1 class="page-title" style="margin-bottom:5px;"> Tasks </h1>
 <div class="alert alert-danger alert-dismissable fade in not_updated" style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Error!</strong> Please select an Engineer to assign an Task.
  </div>
  <div class="alert alert-danger alert-dismissable fade in not_updated1" style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Error!</strong> Please change the status.
  </div>
	<div class="full-height-content-body">
		<div class="row">
			<div class="tickets_tabs col-md-2 pad_right3" style="display:none;">
				<div class="portlet light bordered full_height_block mar-bottom remove_pad">
					
				</div>
			</div>
			<div class="col-md-12 parent">
				<div class="portlet light bordered full_height_block mar-bottom remove_pad tickets_data ">
					<div class="col-md-12 portlet-title portlet-bg-title tickets_data_header">
						<div class="caption font-dark">
							<span class="caption-subject bold uppercase">
								<!--<button class="rightbar btn btn-default btn-toggle"><i class="fa fa-bars" aria-hidden="true"></i></button>-->
							<?php echo $ticketDetails['resultSet']['incident_id']?></span>
						</div>
					<form action="<?php echo base_url('event/tasks/editTicket/'.encode_url($ticketDetails['resultSet']['ticket_id']));?>" method="post" onsubmit="return submitResult();">
						<div class="page-bar form-inline">
							<div class="input-group add-on"> 
								<button type="submit" class="btn btn-default btn-save" id="update">Update</button>
								<!--<a href="http://10.10.32.35:8080/guacamole/"><button class="btn btn-default btn-save">Login</button></a>
								<button class="btn btn-default btn-save">Run Auto Script</button>-->
							</div>
						</div>
					</div>
					
					<!-- Tickets Form -->
					<div class="portlet-body ticket_form_data remove_pad">
					<div class="tickets_data1" style="overflow:auto;">
						
							<div class="row mar-lr3 custom-padding-formgp">
								<div class="form-group col-md-4">
									<label for="incident_id" class="col-sm-4 control-label">Task Number <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="incident_id" name="" value="<?php echo $ticketDetails['resultSet']['incident_id']?>" readonly>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="created_on" class="col-sm-4 control-label">Created On <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="created_on" name="" value="<?php echo $ticketDetails['resultSet']['created_on']?>" readonly>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="client_id" class="col-sm-4 control-label">Client Name <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="client_id" name="" value="<?php echo $ticketDetails['resultSet']['client_title']?>" readonly>
									</div>
								</div>
								
								<div class="form-group col-md-4">
									<label for="requestor" class="col-sm-4 control-label">Requestor <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="requestor" name="" value="<?php echo $ticketDetails['resultSet']['requestor']?>" readonly>
									</div>
								</div>
								
								<div class="form-group col-md-4">
									<label for="queue_id" class="col-sm-4 control-label">Queue Name <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="queue_id" name="" value="<?php echo $ticketDetails['resultSet']['queue_name']?>" readonly>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="client_id" class="col-sm-4 control-label">Location <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="Location" name="" value="NA" readonly>
									</div>
								</div>
								
								<div class="form-group col-md-4">
									<label for="status_code" class="col-sm-4 control-label">Status <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<select class="form-control V_Combo" id="status_code" name="status_code" tabindex="9" onChange="assginUser(this.value)" <?php 	if($ticketDetails['resultSet']['status_code']== 7){ echo "disabled";} ?>>
											<?php						
												$check = access($ticketDetails['resultSet']['status_code']); 
												if ($ticketState['status'] == 'true') {
													foreach ($ticketState['resultSet'] as $value) {
														if (in_array($value['status_code'], $check)){ 
														?>
															<option value="<?php echo $value['status_code']; ?>" <?php echo ($ticketDetails['resultSet']['status_code'] == 7)?'disabled':''; ?> <?php echo ($ticketDetails['resultSet']['status_code'] == $value['status_code'] )?'selected':''; ?> ><?php echo $value['status']; ?></option>  
														<?php
														}
													}
												}
											?> 
										</select>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="owner_id" class="col-sm-4 control-label">Category <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="owner_id" name="" value="<?php echo $ticketDetails['resultSet']['category']?>" readonly>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="priority" class="col-sm-4 control-label">Priority <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="priority" name=""  value="<?php echo $ticketDetails['resultSet']['priority']?>" readonly>
									</div>
								</div>
								
								<div class="form-group col-md-4 assign">
											<label for="created_by" class="col-sm-4 control-label">Assign To <span class="redarrow"></span></label>
											<div class="col-sm-8">
												<div class="input-group">
													<input type="text" class="form-control m-input" id="txtName" value="<?php echo (isset($ticketDetails['resultSet']['assigned']) && !empty($ticketDetails['resultSet']['assigned']))?$ticketDetails['resultSet']['assigned']:"";?>" readonly="readonly" aria-describedby="basic-addon2" <?php if(empty($ticketDetails['resultSet']['assigned_to'])){?> onclick="SelectName()"  <?php } ?>>							
													<span class="input-group-addon" id="basic-addon2" <?php if(empty($ticketDetails['resultSet']['assigned_to'])){?> onclick="SelectName()"> <?php } ?> <i class="fa fa-user"></i></span>
													<input type="hidden"  id="assignid" value="<?php echo $ticketDetails['resultSet']['assigned_to']?>" name="assignid">
												</div>
											</div>
								</div>
								<div class="form-group col-md-4">
									<label for="created_by" class="col-sm-4 control-label">Created By <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="created_by" name="" value="<?php echo $ticketDetails['resultSet']['created_by']?>" readonly>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="due_date" class="col-sm-4 control-label">Due Date <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="due_date" name="" value="<?php echo $ticketDetails['resultSet']['due_date']?>" readonly> 
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="estimated_time" class="col-sm-4 control-label">Estimated Time <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="estimated_time" name="" value="<?php echo $ticketDetails['resultSet']['estimated_time']?>" readonly>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="event_id" class="col-sm-4 control-label">Event ID <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="event_id" name="" value="<?php echo $ticketDetails['resultSet']['event_invoice']?>" readonly>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="event_id" class="col-sm-4 control-label">Sub Category <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="event_id" name="" value="<?php echo $ticketDetails['resultSet']['sub_category']?>" readonly>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group col-md-4 notes">
									<label for="notes" class="col-sm-4 control-label">Notes <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" placeholder="Enter ..." id="notes" name="notes"></textarea>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="subject" class="col-sm-4 control-label">Short Description <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" id="subject" name="" readonly><?php echo $ticketDetails['resultSet']['subject']?></textarea>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="status_code" class="col-sm-4 control-label">Contract Type <span class="redarrow">*</span></label>
									<div class="col-sm-8">
										<select class="form-control V_Combo" id="Contract" name="Contract" tabindex="9" onchange="fake(this.value)" >
												<option value="0" >Contract</option>  
												<option value="0" >Warranty</option>  
												<option value="1" >T/M</option>  
												
										</select>
									</div>
								</div>
								<div class=" col-md-4 siddhu" style="display:none;">
									<label for="due_date" class="col-sm-4 control-label"><span class="pull-right">Use BOH</span> <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="checkbox" class="" id="Block" name="" value="0122257555" readonly> 
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="due_date" class="col-sm-4 control-label">Contract Number <span class="redarrow"></span></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="Contract_number" name="" value="0122257555" readonly> 
									</div>
								</div>
								<div class="clearfix"></div>
								<input type="hidden" name="same_status_code" value="<?php echo $ticketDetails['resultSet']['status_code']; ?>">
								
							
								<div class="clearfix"></div>
								<hr />
								</form>
								
								
								<div class="tabbable-custom" style="padding:0 5px;">
									<ul class="nav nav-tabs ">
										<li class="active">
											<a href="#activity_log" data-toggle="tab" aria-expanded="false"> Activity Log </a>
										</li>
										<li class="">
											<a href="#add_activity" data-toggle="tab" aria-expanded="false"> Add Activity </a>
										</li> 
										<li>
											<a href="#tab_5_1" data-toggle="tab" aria-expanded="true"> Task History </a>
										</li>
										<li class="">
											<a href="#device_details" data-toggle="tab" aria-expanded="false"> Device Details </a>
										</li>
										<li class="">
											<a href="#tab_5_3" data-toggle="tab" aria-expanded="false">Event Correlation </a>
										</li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="activity_log">
											<table class="table table-striped table-bordered table table-hover" id="tblGrid">   
											<thead style="background:#17c4bb;">
												<tr>
													<th class="bold-text1" style="width:10%;color:#fff;">Activity No</th>
													<th class="bold-text1" style="color:#fff;">Activity Type</th>
													<th class="bold-text1" style="width:10%;color:#fff;">Start Time</th>
													<th class="bold-text1" style="width:10%;color:#fff;">End Time</th>
													<th class="bold-text1" style="width:8%;color:#fff;">Effort Hours</th>
													<th class="bold-text1" style="width:12%;color:#fff;">Worked</th>
													<th class="bold-text1" style="color:#fff;">Description</th>
													<th class="bold-text1" style="color:#fff;">Internal Comments</th>
													<th class="bold-text1" style="color:#fff;">Attachments</th>
													
												</tr>
											</thead>
											<tbody class="bold-text">
											<?php
											if (!empty($activity_log["resultSet"])) {
													$i =1;
													foreach($activity_log["resultSet"] as $act_log){?>
												
												<tr>
													<td> <?php echo $act_log["incident_active_id"]; ?> </td>
													<td> <?php echo $act_log["name"]; ?> </td>
													<td> <?php echo $act_log["start_time"]; ?> </td>
													<td> <?php echo $act_log["end_time"]; ?> </td>
													<td> <?php echo effort($act_log["effort_time"]); ?> </td>
													<td> <?php echo $act_log["created_by"]; ?> </td>
													<td> 
														<?php if(!empty($act_log["activity_desc"])){?>
															<div class="content hideContent">
																<?php echo $act_log["activity_desc"]; ?>		
															</div>
															<div class="show-more">
																<?php if(strlen($act_log["activity_desc"]) > 41){?>
																	<a href="#">Show More</a>
																<?php } ?>
															</div>
														<?php } ?>		
													</td>
													<td> 
														<?php if(!empty($act_log["activity_comment"])){?>
															<div class="content hideContent">
																<?php echo $act_log["activity_comment"]; ?> 
															</div>
															<div class="show-more">
																<?php if(strlen($act_log["activity_comment"]) > 41){?>
																	<a href="#">Show More</a>
																<?php } ?>
															</div>
														<?php } ?>
													</td>
													<?php if(isset($act_log["activity_file"]) && !empty($act_log["activity_file"])){?>
													<td> <a href="<?php echo base_url("event/ticket/download_files/".$act_log["activity_file"]); ?>"> Attachments </a> </td>
													<?php }else{ ?>
													<td>  -  </td>
													<?php } ?>
												</tr> 
											<?php }}else{ ?>												
												<tr>
													<td colspan="9"> No Record Found </td>
												</tr> 
											<?php } ?>	
																						  
											</tbody>
										</table>
										</div>
										<div class="tab-pane " id="tab_5_1">
											<table class="table table-striped table-bordered table table-hover" id="tblGrid">   
											<thead style="background:#17c4bb;">
												<tr>
													<th class="bold-text1" style="color:#fff; width:2%;">S.no</th>
													<th class="bold-text1" style="color:#fff;">Start Time</th>
													<th class="bold-text1" style="color:#fff;">End Time</th>
													<th class="bold-text1" style="color:#fff;">Notes</th>
													<th class="bold-text1" style="color:#fff;">Status</th>
													<th class="bold-text1" style="color:#fff;">User</th>
												</tr>
											</thead>
											<tbody class="bold-text">
											<?php
												if (!empty($ticketActivity)) {
													$i =1;
													foreach($ticketActivity as $activity){?>
												<tr>								
													<td><?php echo $i++;?></td>         
													<td><?php echo $activity['start_time'];?></td>         
													<td>
														<?php if($activity['state_status'] != 'Closed'){?>
															<?php echo $activity['end_time'];?>
														<?php }else{?>
															<?php echo $activity['start_time'];?>
														<?php } ?>
													</td>         
													<td><?php echo $activity['notes'];?></td>         
													<td><?php echo $activity['state_status'];?></td>         
													<td><?php echo $activity['username'];?></td>         
												</tr>	
											<?php
													}
												} else {?>
												<tr>
													<td> No Record Found </td>
												</tr> 
											<?php } ?>											  
											</tbody>
										</table>
										</div>
										<div class="tab-pane " id="tab_5_3">
											<table class="table table-striped table-bordered table table-hover" id="tblGrid">   
											<thead style="background:#17c4bb;">
												<tr>
													<th class="bold-text1" style="color:#fff;">Event ID</th>
													<th class="bold-text1" style="color:#fff;">Event Time</th>
													<th class="bold-text1" style="color:#fff;">Severity</th>
													<th class="bold-text1" style="color:#fff;">Event Type</th>
													<th class="bold-text1" style="color:#fff; width:25%;">Event Text</th>
													
												</tr>
											</thead>
											<tbody class="bold-text">
											<?php
												if (!empty($Alarm_Correlation)) {
												
													foreach($Alarm_Correlation as $alarm){?>
												<tr>
													<td style="position:relative;"><a href="javascript:void(0);" class="coupon_question" evtid='<?php echo $alarm['event_id'] ?> ' >E-<?php echo str_pad($alarm['event_id'], 10, '0', STR_PAD_LEFT) ?></a></td>
													<td><?php echo $alarm['start_time'];?></td>         
													<td><?php echo $alarm['severity'];?></td>         
													<td><?php echo $alarm['service'];?></td>         
													<td><?php echo $alarm['event_text'];?></td>         
													<!--<td><?php //echo $alarm['event_text'] .'-'. $alarm['ticket_id'];?></td>-->         
													         
												</tr>	
											<?php
													}
												} else {?>
												<tr>
													<td colspan="9"> No Record Found </td>
												</tr> 
											<?php } ?>											  
											</tbody>
										</table>
										</div>
										
										
										<div class="tab-pane" id="add_activity">
										<form action="<?php echo base_url("event/ticket/save_activity/"); ?>" method="POST" class="activity" enctype="multipart/form-data">
											<div class="form-group col-md-4">
												<label for="event_id" class="col-sm-4 control-label">Start Time <span class="redarrow">*</span></label>
												<div class="col-sm-8 " data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
													<input type="text" class="form-control datetimeSupress" name="start_time" id="datetimeSupress" style="width:100%;" data-validation="required" data-validation-error-msg="Start time is required">
													<span id="msg_datetimeSupress"></span>
												</div>
											</div>
											<div class="form-group col-md-4">
												<label for="event_id" class="col-sm-4 control-label">End Time <span class="redarrow">*</span></label>
												<div class="col-sm-8 " data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
													<input type="text" class="form-control datetimeSupress" name="end_time" id="datetimeSupress" style="width:100%;" data-validation="required" data-validation-error-msg="End time is required">
													<span id="msg_datetimeSupress" ></span>
												</div>
											</div>
											<!--<div class="form-group col-md-4">
												<label for="event_id" class="col-sm-4 control-label">Effort Hours <span class="redarrow"></span></label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="event_id" name="" value="">
												</div>
											</div>-->
											
											
											
									<div class="form-group col-md-4">
										<label for="event_id" class="col-sm-4 control-label">Activity Type <span class="redarrow">*</span>
										</label>
										<div class="col-sm-8">
											<select class="form-control" id="activity_type" name="activity_type" data-validation="required">
												<?php foreach ($activity_type[ 'resultSet'] as $value) { ?>
												<option value="<?php echo $value['activity_type_id']; ?>">
													<?php echo $value[ 'name']; ?>
												</option>
												<?php } ?>
											</select>
										</div>
									</div>
																						
											<div class="clearfix"></div>
											
											<div class="form-group col-md-4">
												<label for="event_id" class="col-sm-4 control-label">Description </label>
												<div class="col-sm-8">
													<textarea class="form-control" name="description" rows="3" placeholder="Enter ..." name="notes"></textarea>
												</div>
											</div>
											<div class="form-group col-md-4">
												<label for="event_id" class="col-sm-4 control-label">Comments </label>
												<div class="col-sm-8">
													<textarea class="form-control" name="comment" rows="3" placeholder="Enter ..." name="notes"></textarea>
												</div>
											</div>
											<div class="form-group col-md-4">
												<label for="event_id" class="col-sm-4 control-label">Attachment </label>
												<div class="col-sm-8">
													<input type="file" class="form-control" id="attachment" name="userfile" value=""  data-validation="mime size"  data-validation-allowing="jpg, png, gif, pdf, txt"		 data-validation-max-size="2M">
												</div>
											</div>
											<input type="hidden" name="ticket_id" value="<?php echo $ticket_id; ?>">
											
											<div class="clearfix"></div>
											<div><button class="btn btn-green green pull-right" style="padding: 1px 4px; 	font-size: 13px; margin-right: 11px;" id="save_activity"> Save </button></div>
											<div class="clearfix"></div>
										</form>
										</div>
										
										
										<div class="tab-pane" id="device_details">
											<div class="row">
												<div class="col-md-3">
													<div class="page_title_sub">Device Details</div>
													<table class="table table-striped table" id="tblGrid">
														<tbody class="bold-text">
															<tr>								
																<td style="width:50% !important;">Device Type :</td>  
																<td style="width:50% !important;"><?php echo $ticketDetails['resultSet']['device_name']?></td>         
															</tr>
															<tr>								
																<td style="width:10% !important;">IP Address :</td>  
																<td style="width:90% !important;"><?php echo $ticketDetails['resultSet']['address']?></td>         
															</tr>
															<tr>								
																<td style="width:10% !important;">Username :</td>  
																<td style="width:90% !important;"><?php echo $ticketDetails['resultSet']['username']?></td>         
															</tr>
															<tr>								
																<td style="width:10% !important;">Password :</td>  
																<td>
																<?php if($ticketDetails['resultSet']['password']){ ?>
																<input type="password"  style="width:90% !important;" value="<?php echo $ticketDetails['resultSet']['password']; ?>" readonly >
																
																<button type="button" id="copy" class="btn btn-primary btn-xs">Copy</button>
																<?php } ?>
																</td>
																																	
															</tr>
															<tr>
																<td>
																	<button  class="btn yellow" onclick="SelectName2()"> SSH </button>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="col-md-9">
													<div class="page_title_sub">Login History</div>
													<table class="table table-striped table-bordered table table-hover" id="tblGrid">
														<thead style="background:#17c4bb;">
															<tr>
																<th class="bold-text1" style="color:#fff; width:2%;">S.no</th>
																<th class="bold-text1" style="color:#fff;">User</th>
																<th class="bold-text1" style="color:#fff;">Login Time</th>
																<th class="bold-text1" style="color:#fff;">Logout Time</th>
																<th class="bold-text1" style="color:#fff;">Login Mode</th>
																<th class="bold-text1" style="color:#fff;">Worked Duration</th>
															</tr>
														</thead>
														<tbody class="bold-text">
														<?php
															if (!empty($ssh_log["resultSet"])) {
															$i =1;
															foreach($ssh_log["resultSet"] as $ssh){?>
															<tr>								
																<td style="width:2%;"><?php echo $i++; ?></td>         
																<td> <?php echo $ssh["username"]; ?> </td>         
																<td> <?php echo $ssh["login_time"]; ?> </td>         
																<td> <?php echo $ssh["logout_time"]; ?> </td>         
																<td> <?php echo $ssh["login_mode"]; ?> </td>         
																<td> <?php echo effort($ssh["effort_time"]); ?> </td>         
																        
															</tr>
														<?php }}else{ ?>												
															<tr>
																<td colspan="6"> No Record Found </td>
															</tr> 
														<?php } ?>	
														</tbody>
													</table>
												</div>

											</div>
												
												<!--<a class="btn green" href="#" target="_blank"> Run Auto Script </a>-->
										</div>
										<div class="tab-pane" id="tab_5_3">
											<p> Howdy, I'm in Section 3. </p>
											<p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel
												eum iriure dolor in hendrerit in vulputate velit esse molestie consequat </p>
											<p>
												<a class="btn yellow" href="http://10.10.32.35:8080/guacamole/" target="_blank"> SSH </a>
												<a class="btn green" href="#" target="_blank"> Run Auto Script </a>
											</p>
										</div>
										
									</div>
								</div>
							</div>
						
					</div>
					</div>
					<!-- Tickets Form End -->	

					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>
					
	<div class="modal fade" id="myModal">
    <div class="modal-dialog"  id="load-data">
		
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<input type="hidden" value="" name="ssh" id="ssh">

				
	<script src="<?php echo base_url() . "public/" ?>js/form/form_event_ticket.js" type="text/javascript"></script>				
<script>

	$(document).ready(function() {
		 var baseURL = $('#baseURL').val();
		 
		
		$('.full_height_block').height($(window).height() - 133);
	});
	$(document).ready(function() {
		$('.tickets_data1').height($(window).height() - 180);
	});

	$(".parent .rightbar").click(function(){
	  
		if($('.tickets_tabs').css('display') == 'none'){ 
			$(".parent").removeClass('col-md-12');
			$(".parent").addClass('col-md-10');
			$(".tickets_tabs").show(); 
		} else { 
			$(".parent").removeClass('col-md-10');
			$(".parent").addClass('col-md-12');
			$(".tickets_tabs").hide(); 
		}
	
	});
	
	

</script>
<script src="<?php echo base_url() . "public/" ?>datapicker/onejquery.datetimepicker.full.js"></script>
<script src="<?php echo base_url() . "public/" ?>form-validator/jquery.form-validator.js"></script>

    <script>

        $('.datetimeSupress').datetimepicker({
            format: 'Y-m-d H:i',
            mask: false,
            dayOfWeekStart: 1,
            lang: 'en',
            timepicker: true,
            showTimePicker: true,
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false,
            minDate: 0,
            step: 5
        });	
		
		
		$(function() {
		// setup validate
			$.validate({
				modules : 'file'
			});
		});	
		
		
		
	var popup;
	var url = "<?php echo base_url('event/tasks/assignusers?skill='.encode_url($ticketDetails['resultSet']['skill'])); ?>";
    function SelectName() {
        popup = window.open(url, "Popup", "width=700,height=400");
        popup.focus();
    }
	
	var incident = <?php echo $ticket_id; ?>;
	var postUrl = "<?php echo base_url('event/tasks/sshdetails'); ?>";
	var popup2;
	var url2 = "http://10.10.32.35:8080/guacamole/";
    function SelectName2() {
		check();		
        popup2 = window.open(url2, "Popup", "width=1000,height=500");
		popup2.focus();	
		var form_data = {
        id: incident,
         };
		$.ajax({
			url: postUrl,
			type: 'POST',
			data: form_data,
			success: function(msg) {
				$('#ssh').val(msg);				
			}
		});	
		
    }
	
	

	
	
    function check(){		
		window.timer = setInterval(checkChild, 500);		
	}

	function checkChild() {
	var updateUrl = "<?php echo base_url('event/tasks/sshdetailsupdate'); ?>";	
		if (popup2.closed) {
			//alert("Child window closed");   
			clearInterval(window.timer);
			var form_data = {
				id: $('#ssh').val(),
			 };
			$.ajax({
				url: updateUrl,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//alert(msg);				
				}
			});	
		}
	}

	
</script>



<?php
function effort($seconds)
{
	//$seconds = 86400;
	$H = floor($seconds / 3600);
	$i = ($seconds / 60) % 60;
	$s = $seconds % 60;
	return sprintf("%02d:%02d:%02d", $H, $i, $s);
}
?>

<script>
function copyToClipboard(text) {
    if (window.clipboardData && window.clipboardData.setData) {
        // IE specific code path to prevent textarea being shown while dialog is visible.
        return clipboardData.setData("Text", text); 

    } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
        var textarea = document.createElement("textarea");
        textarea.textContent = text;
        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
        document.body.appendChild(textarea);
        textarea.select();
        try {
            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
        } catch (ex) {
            console.warn("Copy to clipboard failed.", ex);
            return false;
        } finally {
            document.body.removeChild(textarea);
        }
    }
}

document.querySelector("#copy").onclick = function() {
	var pwd = '<?php echo $ticketDetails['resultSet']['password'];  ?>';
    var result = copyToClipboard(pwd);
    console.log("copied?", result);
};
function fake(e)
{
	if(e == 1)
	{
		$('.siddhu').show();
	}else
	{
		$('.siddhu').hide();
	}
}

function submitResult() {
	var status_code = $('#status_code').val();
	var assignid = $('#assignid').val();
	if(status_code == 0)
	{
		$('.not_updated1').show();
		return false;		
	}else
	{
		$('.not_updated').hide();
	}
	if(status_code != 0 && assignid != 0)
	{
		$('.not_updated').hide();
		return true;
		
	}else
	{
		$('.not_updated').show();
		return false;		
	}  
}

function assginUser(val)
{
	if(val == 1 || val == 10)
	{
		SelectName();
	}
}	
</script>

<?php
function access($val){
 switch($val){
		case '0': // New
			$access = array(0,1);
			break;
		case '1': // Assigned
			$access = array(1,2,10);  
			break;
		case '2': // In-Progress
			$access = array(2,3,4,5,8,9,10);
			break;
		case '3': // Pending Customer
			$access = array(2,3,8,9,10);
			break;
		case '4': // Scheduled
			$access = array(2,4,10);
			break;
		case '5': // Resolved
			$access = array(5,6,7); 
			break;
		case '6': // Reopen
			$access = array(1,6,10); 
			break;
		case '7': // Closed
			$access = array(7);
			break;
		case '8': // Pending Vendor
			$access = array(2,3,8,9,10);
			break;
		case '9': // Pending Work Order
			$access = array(2,3,8,9,10);
			break;
		case '10': // Re-Assigned
			$access = array(2,10);
			break;
			
 }
 return $access;
}
 ?>	
<script>
$(".show-more a").on("click", function() {
    var $this = $(this); 
    var $content = $this.parent().prev("div.content");
    var linkText = $this.text().toUpperCase();    
    //alert(linkText);
    if(linkText === "SHOW MORE"){
        linkText = "Show Less";
        //$content.switchClass("hideContent", "showContent", 200);
		$content.addClass('showContent').removeClass('hideContent');
    } else {
        linkText = "Show More";
        //$content.switchClass("showContent", "hideContent", 200);
		$content.addClass('hideContent').removeClass('showContent');
    };

    $this.text(linkText);
});
</script>			