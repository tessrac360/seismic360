<div class="tab-pane" id="add_activity2">
<form action="<?php echo base_url("event/tasks/save_activity/"); ?>" method="POST" class="activity" enctype="multipart/form-data">


<div class="row">
	<div class="col-md-3">
		<div class="form-group col-md-12">
			<label for="event_id" class="col-sm-4 control-label">Start Time <span class="redarrow">*</span></label>
			<div class="col-sm-8 " data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				<input type="text" class="form-control datetimeSupress" name="start_time" id="datetimeSupress" style="width:100%;" data-validation="required" data-validation-error-msg="Start time is required" value="<?php echo $activity['resultSet']['start_time']; ?>">
				<span id="msg_datetimeSupress"></span>
			</div>
		</div>
		<div class="form-group col-md-12">
			<label for="event_id" class="col-sm-4 control-label">End Time <span class="redarrow">*</span></label>
			<div class="col-sm-8 " data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				<input type="text" class="form-control datetimeSupress" name="end_time" id="datetimeSupress" style="width:100%;" data-validation="required" data-validation-error-msg="End time is required" value="<?php echo $activity['resultSet']['end_time']; ?>">
				<span id="msg_datetimeSupress" ></span>
			</div>
		</div>
		<div class="form-group col-md-12">
			<label for="event_id" class="col-sm-4 control-label">Activity Type <span class="redarrow">*</span>
			</label>
			<div class="col-sm-8">
				<select class="form-control" id="activity_type" name="activity_type" data-validation="required">
					<?php foreach ($activity_type[ 'resultSet'] as $value) { ?>
					<option value="<?php echo $value['activity_type_id']; ?>" <?php if($activity['resultSet']['activity_type_id'] == $value['activity_type_id']){ echo "selected"; } ?>>
						<?php echo $value[ 'name']; ?>
					</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group col-md-12">
			<label for="event_id" class="col-sm-4 control-label">Comments </label>
			<div class="col-sm-8">
				<textarea class="form-control" name="comment" rows="3" placeholder="Enter ..." name="notes"><?php echo $activity['resultSet']['activity_comment']; ?></textarea>
			</div>
		</div>
		<div class="form-group col-md-12">
			<label for="event_id" class="col-sm-4 control-label">Attachment </label>
			<div class="col-sm-8">
				<input type="file" class="form-control" id="attachment" name="userfile[]" multiple="multiple">
			</div>
		</div>
		<?php if(isset($activity['resultSet']['activity_file']) && !empty($activity['resultSet']['activity_file'])){ ?>
		<div class="col-md-12 remove_file">	
			<div class="form-group">
				<div class="col-md-12">
			<button type="button" class="btn btn-info" onclick="downloadFile('<?php echo $activity['resultSet']['activity_file']; ?>')">file</button><!-- <button type="button" class="btn btn-danger" onclick="removeFile('clie_4e11853e-43e4-51b3-9913-c60bb264b80d','19th.png')">X</button> -->
			</div>
			</div>
		</div>
		<?php } ?>
		<?php
			$timeFirst  = strtotime($activity['resultSet']['start_time']);
			$timeSecond = strtotime($activity['resultSet']['end_time']);
			$differenceInSeconds = $timeSecond - $timeFirst;
		?>
		<input type="hidden" name="old_effort_hours" value="<?php echo $differenceInSeconds; ?>">
		<input type="hidden" name="ticket_id" value="<?php echo $ticket_id; ?>">
		<input type="hidden" name="activity_id" value="<?php echo $activity_id; ?>">
		<input type="hidden" name="incident_active_id" value="<?php echo $activity['resultSet']['incident_active_id']; ?>">
		<input type="hidden" name="old_file" value="<?php echo (isset($activity['resultSet']['activity_file']) && !empty($activity['resultSet']['activity_file']))?$activity['resultSet']['activity_file']:''; ?>">
		<input type="hidden" name="contractId" value="<?php echo $ticketDetails['resultSet']['contract_type']?>">
		
	</div>
	<div class="col-md-9 padl0">
		<div class="form-group col-md-12 padl0">
			<div class="col-sm-12">
				<textarea class="form-control description" name="activity_description" rows="3" placeholder="Enter Description ..." name="notes"><?php echo $activity['resultSet']['activity_desc']; ?></textarea>
			</div>
		</div>
	</div>
</div>

	
	<div class="clearfix"></div>
	<div><button class="btn btn-green green pull-right" style="padding: 1px 4px; 	font-size: 13px; margin-right: 11px;" id="save_activity"> Update </button></div>
	<div class="clearfix"></div>
</form>
</div>