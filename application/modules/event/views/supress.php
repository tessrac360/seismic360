<link href="<?php echo base_url() . "public/" ?>datapicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<style>
    .xdsoft_noselect{z-index:99999 !important;}
</style>
<?php //pr($events) ?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title" style="font-size:15px;"><b>Suppress</b></h3>
    </div>
    <div class="modal-body">


        <div class="col-md-9 col-sm-9 col-xs-9">
            <div class="tab-content">
                <div class="tab-pane active" id="supress">
                    <form id ="frm_Supress" method="post" action="">
                        <label class="control-label col-md-3 bold-text1">Select Date :</label>
                        <div>
                            <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                <input type="text" class="form-control" name="datetimeSupress" id="datetimeSupress">
                                <span id="msg_datetimeSupress"></span>
                            </div>  

                            <br>
                        </div>
                        <label class="control-label col-md-3 bold-text1">Comment :</label>
                        <div>
                            <div class="input-icon">
                                <textarea style="width:28%;" id="txtsurpress" name="txtsurpress" placeholder="Comment !"></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="col-sm-5">
                            <input type="hidden" value="<?php echo $event_id; ?>" name="su_event_id">
                            <input type="hidden" value="T" name="su_action">
                            <button class="btn btn-success pull-right" style="padding:5px;" type="submit" id="btnsurpress" >Save</button>
                        </div>
                    </form>
                </div>

                <div class="tab-pane fade" id="permanent">
                    <div class="col-sm-1">
                        <form id ="frm_Supress_permanent" method="post" action="">
                            <input type="hidden" value="<?php echo $event_id; ?>" name="su_event_id">
                            <input type="hidden" value="P" name="su_action">
                            <button class="btn btn-success pull-right" style="padding:5px;" id="btnsurpresspermanent">Save</button>
                        </form>

                    </div>
                </div>                                              
            </div>
        </div>

    </div><!-- /.modal-content -->
    <script src="<?php echo base_url() . "public/" ?>datapicker/onejquery.datetimepicker.full.js"></script>
    <script>

        $('#datetimeSupress').datetimepicker({
            format: 'Y-m-d H:i',
            mask: true,
            dayOfWeekStart: 1,
            lang: 'en',
            timepicker: true,
            showTimePicker: true,
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false,
            minDate: 0,
            step: 5
        });
    </script>