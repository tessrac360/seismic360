<link href="<?php echo base_url() . "public/" ?>datapicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />


<div class="portlet light bordered">
    <div class="portlet-title tabbable-line rightbar-th">
        <div class="actions">
            <!-- <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a> -->
            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                <i class="close fa fa-close"></i>
            </a>
        </div>
        <style>
            .rightbar-th{background:#3598dc!important; }
            .rightbar-th ul li a{color:#ffffff !important;}
        </style>
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#portlet_tab1" data-toggle="tab"> Acknowledge </a>
            </li>

            <li>
                <a href="#portlet_tab2" data-toggle="tab"> Suppress </a>
            </li>
            <li>
                <a href="#portlet_tab3" data-toggle="tab"> TT </a>
            </li>
        </ul>
    </div>
    <div class="portlet-body">
        <div class="tab-content">
            <div class="tab-pane active" id="portlet_tab1">
                <?php if ($surpressStatus == 'false') {
                    ?>
                    <form id ="frm_manual" method="post" action="">
                        <textarea id="txtmanualack" name="txtmanualack" placeholder="Comment !" cols="38" rows="10"></textarea>
                        <span id="msg_manualack"></span>
                        <p>
                            <button class="btn btn-primary pull-right" type="submit" id="btnmanualack" style="padding:8px 16px;">Save</button>
                        </p>
                    </form>
                <?php } else { ?>
                <p><strong>Event is already supressed till <?php echo ($resultSet->event_suppress_till) ? $resultSet->event_suppress_till : ''; ?></strong></p>
                <?php } ?>
            </div>
            <div class="tab-pane" id="portlet_tab2">
                <div class="form-body">
                    <?php if ($surpressStatus == 'true') {
                        ?>
                        <form id ="frm_Supressupdate" method="post" action="">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="control-label col-md-4" style="margin:3% 0;color:#000;">Select Date :</label>
                                    <div class="col-md-8">

                                        <input type="text" value="<?php echo ($resultSet->event_suppress_till) ? $resultSet->event_suppress_till : ''; ?>" name="datetimeSupress" placeholder="Y-m-d H:i" id="datetimeSupress"/>
                                    </div>
                                </div>
                            </div> <span id="msg_datetimeSupress"></span>
                            <textarea id="txtsurpress" name="txtsurpress" placeholder="Comment !" cols="38" rows="10"><?php echo ($resultSet->ack_notes != '') ? $resultSet->ack_notes : '' ?></textarea>
                            <span id="msg_txtsurpress"></span>
                            <p>
                                <input type="hidden" name="ack_id" value="<?php echo ($resultSet->ack_id != '') ? $resultSet->ack_id : '' ?>">
                                <button class="btn btn-primary pull-right" type="submit" id="btnsurpressupdate" style="padding:8px 16px;">Update</button>
                                <a class="btn btn-success pull-right suppress_reactivate" suppress_event_id="<?php echo ($resultSet->event_id != '') ? $resultSet->event_id : '' ?>" href="javascript:void();" suppress_ack_id ="<?php echo ($resultSet->ack_id != '') ? $resultSet->ack_id : '' ?>" style="padding:8px 16px;margin-right:5px;">Reactivate</a>
                            </p>                       
                        </form>

                    <?php } else { ?>
                        <form id ="frm_Supress" method="post" action="">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="control-label col-md-4" style="margin:3% 0;color:#000;">Select Date :</label>
                                    <div class="col-md-8">
                                        <input type="text" value="" name="datetimeSupress" placeholder="Y-m-d H:i" id="datetimeSupress"/>
                                    </div>
                                </div>
                            </div> <span id="msg_datetimeSupress"></span>
                            <textarea id="txtsurpress" name="txtsurpress" placeholder="Comment !" cols="38" rows="10"></textarea>
                            <span id="msg_txtsurpress"></span>
                            <p>
                                <button class="btn btn-primary pull-right" type="submit" id="btnsurpress" style="padding:8px 16px;">Save</button>
                            </p>                       
                        </form>
                    <?php } ?>
                    <!---->
                </div>
            </div>
            <div class="tab-pane" id="portlet_tab3">								
                <div id="alert-info" class="alert alert-info alert-top" role="alert">									
                    <span class="alert-msg"></span>
                </div>
                <!-- <div id="alert-warn" class="alert alert-warning alert-top" role="alert">
                       <button type="button" aria-label="Close"><span aria-hidden="true">×</span></button>
                       <span class="alert-msg"></span>
               </div> -->
                <div style="margin-top:10px;">
                    <a class="btn btn-info" href="<?php echo base_url() . "event/ticket?eventId=".$eventTTurl; ?>" id="info" style="padding:8px 16px;">TT</a>
                    <!-- <button class="btn btn-warning" id="warn">warn alert</button> -->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() . "public/" ?>datapicker/onejquery.datetimepicker.full.js"></script>
<script>

    $('#datetimeSupress').datetimepicker({
        format: 'Y-m-d H:i',
        //mask: true,
        dayOfWeekStart: 1,
        lang: 'en',
        timepicker: true,
        showTimePicker: true,
        scrollMonth: false,
        scrollTime: false,
        scrollInput: false,
        minDate: 0,
        step: 5
    });
</script>



