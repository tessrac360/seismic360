<link href="<?php echo base_url() . "public/" ?>js/event/style.css" rel="stylesheet" type="text/css" />
<style>		
            body.wait, body.wait *{
                cursor: wait !important;   
            }
            .logo_img{position:relative; top:5px;}
            .main-header {    background: #367FA9;}
            .sidebar-mini.sidebar-collapse .navbar {
                margin-left: 50px !important;
            }
            .sidebar-mini.sidebar-collapse .main-sidebar{
                min-width: 50px !important;
            }
            /* .main-sidebar{min-width:230px !important;} */
            /*.loader {
                    position: fixed;
                    left: 0px;
                    top: 0px;
                    width: 100%;
                    height: 100%;
                    z-index: 9999;
                    background: url( "http://eventedge1.ites.org/assets/img/busy.gif") 50% 50% no-repeat rgb(249,249,249);
            }*/
            .arrow_click{z-index:999;}
            .alerts
            {
                border-left: 1px solid #a8a8a8;
                border-radius: 5px;	
            }
            .sidebar-menu li.active > .treeview-menu {
                display: block;
                position: relative;
            }.sidebar-menu li {
                display: block;
                position: relative;
            }
            ul.treeview-menu:first-child a:nth-child(2){right:0px !important;}
            /*ul{padding-left:5px !important;} ul.treeview-menu{padding-left:3px !important;} */
            .text-dark-blue{color:#1B99D8;}
            .skin-blue .sidebar-menu>li:hover>a, .skin-blue .sidebar-menu>li.active>a{background:#222d32 !important;}
            .treeview{background:#222d32;}
            .skin-blue .sidebar-menu>li:hover>a, .skin-blue .sidebar-menu>li.active>a{border-left-color:transparent !important;}
            .sidebar-collapse .all_1{display:none !important;}
            .sidebar-menu .treeview-menu {padding:0;}
            .skin-blue .sidebar-menu > li > .treeview-menu {margin:0;}
            /* a.hover_effects:hover{background:none !Important;} 
            .skin-blue .sidebar-menu>li:hover>a, .skin-blue .sidebar-menu>li.active>a {background:none !Important;} */
            @-moz-document url-prefix() { 
                .sidebar-menu, .main-sidebar .user-panel, .sidebar-menu > li.header {
                    white-space: normal !important;
                }
            }
            table.event_table tr td:first-child{text-align:center !important; font-size:15px;}
            table.event_table th{text-align:center; background:#2b3b55 ; font-size:14px;}
            table.event_table tr{height:10px;}
            table.event_table tr:nth-child(odd){background: beige;}
            table.event_table tr:nth-child(even){background: #efefef;}
            table.event_table tr td{vertical-align:middle !important; font-size:13px; border-bottom: 1pt solid silver;}
            /* .sidebar-collapse .sidebar-menu{min-width:230px;} */
            .sidebar-mini.sidebar-collapse .main-sidebar .user-panel>.info, .sidebar-mini.sidebar-collapse .sidebar-form, .sidebar-mini.sidebar-collapse .sidebar-menu>li>a>span, .sidebar-mini.sidebar-collapse .sidebar-menu>li>.treeview-menu, .sidebar-mini.sidebar-collapse .sidebar-menu>li>a>.pull-right, .sidebar-mini.sidebar-collapse .sidebar-menu li.header{
                min-width:315px !important;
                display:inline-block;
            }
            /* .sidebar-mini.sidebar-collapse .sidebar-menu li a.arrow_click{
                    pointer-events: none;
            } */
            .treeview-menu .treeview .fa-angle-down{padding-right:7px !important;}
            .fa-bell{color:lightgreen;}
            .columnFilter{border: none;background-color: red;}		
            .fa-filter{color:#17C4BB;}
            <!-- filter on hover  -->
            .chkdropdown {
                display:inline-block;
                position:relative;
                color:black;
            }
            .chkdropdown, .chkdropdown ul {


                padding:0.1ex 0.5ex 0;
            }
            .chkdropdown ul {
                position:absolute;
                width:100%;
                list-style-type:none;
                padding:0;
                margin:0;
                left:0;
                height:0;
                overflow:hidden;
                border-color:transparent;
                background-clip:padding-box;
            }
            .chkdropdown:hover ul {
                height:300px;
                border-color:black;
                overflow:auto;
            }
            .chkdropdown li:nth-child(n+2) {
                border-top:1px solid silver;
            }
            .chkdropdown input[type="checkbox"] {
                float:left;
            }
            .chkdropdown label {
                display:block;
                margin-left: 18px;
                margin-top: 2px;
            }

            <!-- filter on hover -->
        </style>
        <style>
            .dropbtn {
                /*background-color: #4CAF50;
                color: white;*/
                padding: 0px;
                font-size: 14px;
                border: none;
                cursor: pointer;
                background-color: Transparent;
                outline:none;
            }
            .tablesorter {
                text-transform: none;	
            }
            /*
            .dropbtn:hover, .dropbtn:focus {
                    background-color: #3e8e41;
            }*/	
            .dropdown {
                position: relative;
                display: inline-block;
                padding: 0px;
            }
            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                overflow: auto;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                text-align: left;
            }
            .chkbox {
                color: black;
                padding: 5px 5px 5px 10px;
                display: block;			
            }
            .chkbox:hover {background-color: #f1f1f1}
            .show {display:block;}
            .table-filter td{
                text-align: left !important;
                position: relative;
                padding: 0 !important;
                height: 0;
                margin: 0;
                border: 0 !important;
            }
            .table-filter td a{position: relative; 
                               <!-- top: 24px;  -->
                               left: 4px;   
                               z-index: 99999;}

            #demo-table_filter{float:right;}
            #demo-table_wrapper .col-sm-12{overflow:auto;}
            .pagination
            {	
                float:right;
            }		
            .dropdown-menu>li>a {
                padding: 2px 14px;
                color: #6f6f6f;
                text-decoration: none;
                display: block;
                clear: both;
                font-weight: 300;
                line-height: 18px;
                white-space: nowrap;
            }
        </style>
        <style>
            .dropbtn {			
                border: none;
                cursor: pointer;
            }
            .dropbtn:hover, .dropbtn:focus {		
            }
            .dropdown {
                position: relative;
                display: inline-block;
            }
            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                overflow: auto;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
            }
            .dropdown-content a {
                color: black;
                padding: 7px 1px;
                text-decoration: none;
                display: block;
                font-weight: bold;
            }
            .dropdown a:hover {background-color: #f1f1f1}
            .show {display:block;}	
            .filter-pos
            {
                position: absolute;
                top: 6px;
                left: 6px;
                z-index: 9;
            }
            .scroll
            {
                overflow:scroll;
            }

            /*** custom css only popup ***/
            .columnSelectorWrapper {
                position: relative;
                margin: 10px 0;
                display: inline-block;
            }
            .columnSelector, .hidden {
                display: none;
            }
            .columnSelectorButton {
                background: #99bfe6;
                border: #888 1px solid;
                color: #111;
                border-radius: 5px;
                padding: 5px;
            }
            #colSelect1:checked + label {
                background: #5797d7;
                border-color: #555;
            }
            #colSelect1:checked ~ #columnSelector {
                display: block;
            }
            .columnSelector {
                width: 120px;
                position: absolute;
                top: 30px;
                padding: 10px;
                background: #fff;
                border: #99bfe6 1px solid;
                border-radius: 5px;
            }
            .columnSelector label {
                display: block;
            }
            .columnSelector label:nth-child(1) {
                border-bottom: #99bfe6 solid 1px;
                margin-bottom: 5px;
            }
            .columnSelector input {
                margin-right: 5px;
            }
            .columnSelector .disabled {
                color: #ddd;
            }

            /*** Bootstrap popover ***/
            #popover-target label {
                margin: 0 5px;
                display: block;
            }
            #popover-target input {
                margin-right: 5px;
            }
            #popover-target .disabled {
                color: #ddd;
            }


            .columnSelector{z-index: 999;     min-width: 200px; max-width: 300px; max-height:350px; overflow:auto;}	

            .custom_filter{

                background:#fff;
                height: 350px;
                overflow: auto;
                width:200px !important;
            }
            *, ul.custom_filter:focus{outline: 0;}
            .custom_filter li{padding:5px; }

        </style>

<div class="full-height-content full-height-content-scrollable">
    <h1 class="page-title"> Events Table </h1>
    <div class="full-height-content-body"  style="overflow:auto !important;">
        <div class="row">
            <div class="col-md-12">

                <!-- Bootstrap popover button -->
                <div class="columnSelectorWrapper">
                    <input id="colSelect1" type="checkbox" class="hidden">
                    <label class="columnSelectorButton" for="colSelect1">Column</label>
                    <div id="columnSelector" class="columnSelector">
                        <!-- this div is where the column selector is added -->
                    </div>
                </div>
                <div class="col-md-12" style="overflow:auto;">	
                    <table id="demo-table" class="tablesorter custom-popup bordered cellpadding-0 cellspacing-0 table table-bordered table-hover">
                        <thead>
                            <tr class="table-filter">
                                <td>											
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>											
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <td>
                                    <div class="chkdropdown filter-pos">
                                        <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="custom_filter">
                                            <li>
                                                <input type="checkbox" id="cdd1">
                                                <label for="cdd1">My Data 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd2">
                                                <label for="cdd2">My Data 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 3</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 4</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 5</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 6</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 7</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 8</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 9</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 10</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 12</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 13</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" id="cdd3">
                                                <label for="cdd3">My Data 14</label>
                                            </li>

                                        </ul>
                                    </div>		
                                </td>
                                <!-- <td> <a href="#" ><i class="fa fa-filter"></i></a> </td> -->
                            </tr>
                            <tr class=" dnd-moved">
                                <th id='column-header-1' data-priority="critical" >Column Header 1<div id='column-header-1-sizer'></div> </th>
                                <th id='column-header-2' data-priority="3">Column Header 2<div id='column-header-2-sizer'></div></th>
                                <th id='column-header-3' data-priority="3">Column Header 3<div id='column-header-3-sizer'></div></th>
                                <th id='column-header-4' data-priority="4">Column Header 4<div id='column-header-4-sizer'></div></th>
                                <th id='column-header-5' data-priority="5">Column Header 5<div id='column-header-5-sizer'></div></th>
                                <th id='column-header-6' data-priority="6">Column Header 6<div id='column-header-6-sizer'></div></th>
                                <th id='column-header-7' data-priority="7">Column Header 7<div id='column-header-7-sizer'></div></th>
                                <th id='column-header-8' data-priority="8">Column Header 8<div id='column-header-8-sizer'></div></th>
                                <th id='column-header-9' data-priority="9">Column Header 9<div id='column-header-9-sizer'></div></th>
                                <th id='column-header-10'data-priority="9">Column Header 10<div id='column-header-10-sizer'></div></th>
                                <th id='column-header-11'data-priority="10">Column Header 11<div id='column-header-11-sizer'></div></th>
                                <th id='column-header-12'data-priority="10">Column Header 12<div id='column-header-12-sizer'></div></th>
                                <th id='column-header-13'data-priority="3">Column Header 13<div id='column-header-13-sizer'></div></th>
                                <th id='column-header-14'data-priority="3">Column Header 14<div id='column-header-14-sizer'></div></th>
                                <th id='column-header-15'data-priority="3">Column Header 15<div id='column-header-15-sizer'></div></th>
                                <th id='column-header-16'data-priority="3">Column Header 16<div id='column-header-16-sizer'></div></th>
                                <th id='column-header-17'data-priority="3">Column Header 17<div id='column-header-17-sizer'></div></th>
                                <th id='column-header-18'data-priority="3">Column Header 18<div id='column-header-18-sizer'></div></th>
                                <th id='column-header-19'data-priority="3">Column Header 19<div id='column-header-19-sizer'></div></th>
                                <th id='column-header-20'data-priority="3">Column Header 20<div id='column-header-20-sizer'></div></th>
                                <th id='column-header-21'data-priority="3">Column Header 21<div id='column-header-21-sizer'></div></th>
                                <th id='column-header-22'data-priority="3">Column Header 22<div id='column-header-22-sizer'></div></th>
                                <th id='column-header-23'data-priority="3">Column Header 23<div id='column-header-23-sizer'></div></th>
                                <th id='column-header-24'data-priority="3">Column Header 24 <div id='column-header-24-sizer'></div></th>
                                <th id='column-header-25'data-priority="3">Column Header 25 <div id='column-header-25-sizer'></div></th>
                            </tr>		


                        </thead>
                        <tbody>
                            <tr class="dnd-moved">
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                                <td>My Data 1</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                                <td>My Data 2</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                                <td>My Data 3</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                                <td>My Data 4</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                                <td>My Data 5</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                                <td>My Data 6</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                                <td>My Data 7</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                                <td>My Data 8</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                            </tr>
                            <tr class="dnd-moved">
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                                <td>My Data 9</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() . "public/" ?>js/event/drag/dragndrop.table.columns.js" type="text/javascript"></script>		
    <style>body.tablesorter-disableSelection { -ms-user-select: none; -moz-user-select: -moz-none;-khtml-user-select: none; -webkit-user-select: none; user-select: none; }.tablesorter-resizable-container { position: relative; height: 1px; }.tablesorter-resizable-handle { position: absolute; display: inline-block; width: 8px;top: 1px; cursor: ew-resize; z-index: 3; user-select: none; -moz-user-select: none; }</style>
    <script src="<?php echo base_url() . "public/" ?>js/event/js/index.js"></script>
    <script>
        $('.table').dragableColumns();
    </script>
<!-- Tablesorter: required -->
<script src="<?php echo base_url() . "public/" ?>js/event/js/jquery.tablesorter.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/event/js/jquery.tablesorter.widgets.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/event/js/widgets/widget-columnSelector.js"></script>

<script id="js">
    $(document).ready(function () {
        var baseURL = $('#baseURL').val();

            $.ajax({
                type: 'POST',
                url: baseURL + 'event/ajax_eventLoad',
                dataType: 'json',
                //data: {menuid: menuid},
                success: function (data) {
                    if (data.status == 'true') {
                        $('.eventTable tbody').html(data.html);
                    } 
                }
            });


        });
        $(function () {
            /*** custom css only button popup ***/
            $(".custom-popup").tablesorter({
                theme: 'blue',
                widgets: ['zebra', 'columnSelector', 'stickyHeaders'],
                widgetOptions: {
                    // target the column selector markup
                    columnSelector_container: $('#columnSelector'),
                    // column status, true = display, false = hide
                    // disable = do not display on list
                    columnSelector_columns: {
                        0: 'disable' /* set to disabled; not allowed to unselect it */
                    },
                    // remember selected columns (requires $.tablesorter.storage)
                    columnSelector_saveColumns: true,

                    // container layout
                    columnSelector_layout: '<label><input type="checkbox">{name}</label>',
                    // layout customizer callback called for each column
                    // function($cell, name, column){ return name || $cell.html(); }
                    columnSelector_layoutCustomizer: null,
                    // data attribute containing column name to use in the selector container
                    columnSelector_name: 'data-selector-name',

                    /* Responsive Media Query settings */
                    // enable/disable mediaquery breakpoints
                    columnSelector_mediaquery: true,
                    // toggle checkbox name
                    columnSelector_mediaqueryName: 'Auto: ',
                    // breakpoints checkbox initial setting
                    columnSelector_mediaqueryState: true,
                    // hide columnSelector false columns while in auto mode
                    columnSelector_mediaqueryHidden: true,

                    // set the maximum and/or minimum number of visible columns; use null to disable
                    columnSelector_maxVisible: null,
                    columnSelector_minVisible: null,
                    // responsive table hides columns with priority 1-6 at these breakpoints
                    // see http://view.jquerymobile.com/1.3.2/dist/demos/widgets/table-column-toggle/#Applyingapresetbreakpoint
                    // *** set to false to disable ***
                    columnSelector_breakpoints: ['20em', '30em', '40em', '50em', '60em', '70em'],
                    // data attribute containing column priority
                    // duplicates how jQuery mobile uses priorities:
                    // http://view.jquerymobile.com/1.3.2/dist/demos/widgets/table-column-toggle/
                    columnSelector_priority: 'data-priority',

                    // class name added to checked checkboxes - this fixes an issue with Chrome not updating FontAwesome
                    // applied icons; use this class name (input.checked) instead of input:checked
                    columnSelector_cssChecked: 'checked',

                }
            });

        });
</script>