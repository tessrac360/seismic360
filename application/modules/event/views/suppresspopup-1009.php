<link href="<?php echo base_url() . "public/" ?>datapicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<style>
    .xdsoft_noselect{z-index:99999 !important;}
	.modal-hei{height:225px;}
</style>
<div class="modal-content modal-hei">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actions</h4>
    </div>
    <div class="modal-body">					    
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <ul class="nav nav-tabs tabs-left">
                        <li class="active">
                            <a href="#actionsupress" style="font-size: 13px;color: #000;" data-toggle="tab"><b>SUPPRESS</b></a>
                        </li>
                        <li>
                            <a href="#actionpermanent" style="font-size: 13px;color: #000;"data-toggle="tab"><b>PERMANENT</b></a>
                        </li>

                    </ul>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9">
                    <div class="tab-content">

                        <div class="tab-pane active" id="actionsupress">
                            <form id ="frm_Supress_pupup" method="post" action="">
                                <div class="col-sm-4">
                                    <label class="control-label">Select Date :</label>
                                </div>
                                <div class="col-sm-8">
                                    <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                        <input type="text" class="form-control datetimeSupress" name="datetimeSupress" id="datetimeSupress">
                                        <span id="msg_datetimeSupress"></span>

                                    </div>                                                    
                                    <br>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label">Comment :</label>
                                </div>
                                <div class="col-sm-8">
                                    <div class="input-icon">
                                        <textarea style="width:80%;" id="txtsurpress" name="txtsurpress" placeholder="Comment !"></textarea>

                                    </div>
                                </div>
                                <br>
								
								<div class="col-sm-4">
                                    <label class="control-label">&nbsp;</label>
                                </div>
                                <div class="col-sm-8">
                                    <div class="input-icon">
                                        <input type="hidden" value="<?php echo $event_id; ?>" name="su_event_id">
										<input type="hidden" value="T" name="su_action">
										<button class="btn btn-success" style="padding:5px;" type="submit" id="btnsurpresspupup" >Save</button>
                                    </div>
                                </div>
								
								<br />
                                <div class="col-sm-5">
                                    

                                </div>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="actionpermanent">
                            <div class="col-sm-1">
                                <form id ="frm_Supress_permanent_pupup" method="post" action="">
                                    <input type="hidden" value="<?php echo $event_id; ?>" name="su_event_id">
                                    <input type="hidden" value="P" name="su_action">
                                    <button class="btn btn-success pull-right" style="padding:5px;" id="btnsurpresspermanentpupup">Save</button>
                                </form>
                            </div>
                        </div>                                              
                    </div>                                       
                </div>
            </div>	 
        </div>																
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>datapicker/onejquery.datetimepicker.full.js"></script>
<script>

    $('.datetimeSupress').datetimepicker({
        format: 'Y-m-d H:i',
        mask: true,
        dayOfWeekStart: 1,
        lang: 'en',
        timepicker: true,
        showTimePicker: true,
        scrollMonth: false,
        scrollTime: false,
        scrollInput: false,
        minDate: 0,
        step: 5
    });
</script>