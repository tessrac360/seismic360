<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tasks extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        $this->userId = $this->session->userdata('id');
        $this->client_id = $this->session->userdata('client_id');
        $current_date = $this->config->item('datetime');
        $this->current_date = $this->config->item('datetime');
        $this->uData = array('last_updated_on' => $current_date);
        $this->load->library('form_validation');
        $this->load->model('Model_tasks');
        $this->load->model('Model_ticket');
    }

    public function index() {
        $this->getIncidentList();
    }

    public function editTicket($ticket_id = NULL) {
        $roleAccess = helper_fetchPermission('63', 'view');
        if ($roleAccess == 'Y') {
            $ticket_id = decode_url($ticket_id);
			if(isset($ticket_id) && $ticket_id!= ""){
				//echo $ticket_id;exit;
				$data['ticketDetails'] = $ticketDetails = $this->Model_tasks->getTicketDetails($ticket_id);
				//pr($ticketDetails);exit;
				$data['ticketState'] = $this->Model_tasks->getTicketStatus();
				$data['activity_type'] = $this->Model_tasks->getActivityType();
				$data['activity_log'] = $activity_log = $this->Model_ticket->getActivityLog($ticket_id);
				$activity_files = array();
				if(isset($activity_log,$activity_log['resultSet']) && !empty($activity_log) && !empty($activity_log['resultSet']))
				{
					foreach($activity_log['resultSet'] as $log_files)
					{
						$activity_files[$log_files['activity_log_id']][] = $this->Model_ticket->getActivityLogFiles($log_files['activity_log_id']);
					}
				}
				$data['log_files'] = $activity_files;
				$data['edit_activity_log'] = $this->Model_ticket->getActivityLogEdit($ticket_id);
				$data['ssh_log'] = $this->Model_tasks->getSSHLog($ticket_id);
				$data['pageTitle'] = 'Ticket Details';
				$data['ticket_id'] = $ticket_id;
				$data['file'] = 'tasks/update_form';
				$oldStatusId = $ticketDetails['resultSet']['status_code'];
				$reopened_count = $ticketDetails['resultSet']['reopened_count'];
				$device_id = $ticketDetails['resultSet']['device_id'];
				$data['Alarm_Correlation'] = $this->Model_tasks->getDetailsDeviceId($device_id);
				$status_code = $this->input->post("status_code");
				$same_status_code = $this->input->post("same_status_code");
				$assignid = $this->input->post("assignid");
				$oldAssignId = $this->input->post("oldAssignId");

				$this->form_validation->set_rules('status_code', 'Status', 'required');
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
				if ($this->form_validation->run() == TRUE) {
					$responseText = "";
					$status = ""; $oldStatus = "";
					if(isset($oldStatusId,$status_code))
					{
						$Details = helper_getIncidentStausName('ID',$status_code);
						$status = $Details['resultSet']->status;
						$oldDetails = helper_getIncidentStausName('ID',$oldStatusId);		
						$oldStatus = $oldDetails['resultSet']->status;
					}
					
					if ($status_code == 6) {
						$postTicketDetails = array('status_code' => $status_code, 'assigned_to' => $assignid, 'reopened_count' => $reopened_count + 1);
					} else if ($status_code == 0) {//new
						$postTicketDetails = array('status_code' => 1, 'assigned_to' => $assignid);			
					} else {
						$postTicketDetails = array('status_code' => $status_code, 'assigned_to' => $assignid);
					}
					
					#check Status
					if($oldStatusId != $status_code)
					{
						$responseText .= "Status updated from ".$oldStatus." to ".$status." </br>";
					}
					
					
					#check Assign
					if($oldAssignId != $assignid)
					{
						$assignUserName = ""; $oldassignUserName = "";
						if(isset($assignid,$oldAssignId))
						{
							$assignUserDetails = helper_getUserInformation('ID',$assignid);
							$assignUserName = $assignUserDetails['resultSet']->first_name.' '.$assignUserDetails['resultSet']->last_name;
							if(!empty($oldAssignId))
							{
								$oldassignUserDetails = helper_getUserInformation('ID',$oldAssignId);
								$oldassignUserName = $oldassignUserDetails['resultSet']->first_name.' '.$oldassignUserDetails['resultSet']->last_name;
							}					
						}					
						
						if($oldStatusId == 0)
						{
							$responseText .= "Assigned to ".$assignUserName." </br>";
						}elseif(($oldStatusId == 1) && ($status_code == 1)) 
						{
							$responseText .= "Reassigned from ".$oldassignUserName." to ".$assignUserName." </br>"; //reassign
						}else
						{
							$responseText .= "Reassigned from ".$oldassignUserName." to ".$assignUserName." </br>"; //reassign
						}     			
						
					}

					$postTicketDetails = array_merge($postTicketDetails, $this->uData);
					$updateStatus = $this->Model_tasks->updateTicketDetails($postTicketDetails, $ticket_id);
					$lastTicketActivityID = $this->Model_tasks->getLastTicketActivityID($ticket_id);
				  
						$activityData = array('start_time' => date('Y-m-d H:i:s'),
							'notes' => $responseText,
							'state_status' => $status_code,
							'ticket_id' => $ticket_id,
							'user_id' => $this->userId);

						$insertTicketActivity = $this->Model_tasks->insertTicketActivity($activityData);
						$lastActiveUpdate = array('end_time' => date('Y-m-d H:i:s'));
						$updateLastActiveTicket = $this->Model_tasks->updateLastActiveTicket($lastActiveUpdate, array('activity_id' => $lastTicketActivityID['activity_id']));
				   


					if ($updateStatus['status'] == 'true') {
						$this->session->set_flashdata("success_msg", "Ticket Details is Updated successfully ..!!");
						redirect('event/tasks/editTicket/' . encode_url($ticket_id));
					} else {
						$this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('event/tasks/editTicket/' . encode_url($ticket_id));
					}
				} else {
					$data['ticketActivity'] = $this->Model_tasks->ticketActivity($ticket_id);
					$this->load->view('template/front_template', $data);
				}
			}else{
				redirect('my404');
			}
        } else {
            redirect('unauthorized');
        }
    }

 

    public function ajax_client_incidents() {

        $client_id = (isset($_POST['id'])) ? $_POST['id'] : "";
        $user_id = (isset($_POST['user'])) ? $_POST['user'] : "";
        $myincidents = (isset($_POST['myincidents'])) ? $_POST['myincidents'] : "";
        //echo $user_id;exit;
        $status_id = "";
        $ticketIds = array();
        $ajax_multiple_ids = array("status_code" => $_POST['status_code'],
            "ticket_cate_id" => $_POST['ticket_cate_id'],
            "ticket_sub_cate_id" => $_POST['ticket_sub_cate_id'],
            "ticket_type_id" => $_POST['ticket_type_id'],
            "queue_id" => $_POST['queue_id'],
            "user_id" => $_POST['user_id'],
            "severity_id" => $_POST['severity_id'],
            "priority_id" => $_POST['priority_id'],
            "date" => $_POST['date']);

        if (isset($_GET['status_id']) && $_GET['status_id'] != NULL && !empty($_GET['status_id'])) {
            $status_id = (isset($_GET['status_id'])) ? $_GET['status_id'] : "";
        }
        if (isset($_GET['alarmtype'])) {
            $alarmtype = ($_GET['alarmtype']) ? $_GET['alarmtype'] : "";
            $ticketIds = $this->Model_tasks->getAlarmtypeTicket($alarmtype);
        }
        if ($myincidents == "ok") {
            $data = $this->Model_tasks->getTicketColumn($status_id, $client_id, $ticketIds, $dashboard = "", $ajax_multiple_ids, $user_id);
        } else {
            $data = $this->Model_tasks->getTicketColumn($status_id, $client_id, $ticketIds, $dashboard = "", $ajax_multiple_ids);
        }
        //pr($data);exit;
        $html = "";
        if (isset($data) && !empty($data)) {
            $html = " <tbody id='siddhu'>";

            /* $severity = array();
              $severities = $this->Model_tasks->getSeverity();
              foreach($severities['resultSet'] as $severity_val){
              $severity[$severity_val['id']] = $severity_val['severity'];
              } */

            foreach ($data as $ticket) {
                if ($ticket['last_updated_on'] == '0000-00-00 00:00:00') {
                    $last_updated_on = ' - ';
                } else {
                    $last_updated_on = $ticket['last_updated_on'];
                }
                if ($ticket['status_code'] == 7) {
                    $closed_on = $ticket['last_updated_on'];
                } else {
                    $closed_on = ' - ';
                }


                $html .= "<tr>
								<td><a href=" . base_url() . 'event/tasks/editTicket/' . encode_url($ticket['ticket_id']) . ">" . $ticket['incident_id'] . "</a></td>
								<td>" . $ticket['created_on'] . "</td>
								<td>" . $ticket['client_name'] . "</td>
								<td>" . $ticket['requestor'] . "</td>
								<td>" . $ticket['created_by'] . "</td>
								<td>" . $ticket['subject'] . "</td>
								<td>" . $ticket['assigned_to'] . "</td>
								<td>" . $ticket['queue_name'] . "</td>
								<td>" . $ticket['status'] . "</td>
								<td>" . $ticket['priority'] . "</td>
								<td>" . $ticket['severity'] . "</td>
								<td>" . $ticket['ticket_type'] . "</td>
								<td>" . $ticket['category'] . "</td>
								<td>" . $ticket['subcategory'] . "</td>
								<td>" . $last_updated_on . "</td>
								<td>" . $closed_on . "</td>
							</tr>";
            }
        } else {
            $html .= "<tbody id='siddhu'><tr>
										<td colspan='15'> No Record Found </td>
									</tr>";
        }
        $html .= "</tbody>";
        echo $html;
    }

    public function ajax_incidents_search($user_id = "") {

        $incidentSearch = (isset($_POST['incidentSearch'])) ? $_POST['incidentSearch'] : "";
        $user_id = (isset($_POST['user'])) ? $_POST['user'] : "";
        $myincidents = (isset($_POST['myincidents'])) ? $_POST['myincidents'] : "";
        $client_id = (isset($_POST['id'])) ? $_POST['id'] : "";

        if ($myincidents == "ok") {
            $incidentslist = $this->Model_tasks->getIncidentSearch($incidentSearch, $user_id, $client_id);
        } else {
            $incidentslist = $this->Model_tasks->getIncidentSearch($incidentSearch, "", $client_id);
        }

        $html = "";
        if (isset($incidentslist) && !empty($incidentslist)) {
            $html = " <tbody id='siddhu'>";

            /* $severity = array();
              $severities = $this->Model_tasks->getSeverity();
              foreach($severities['resultSet'] as $severity_val){
              $severity[$severity_val['id']] = $severity_val['severity'];
              } */

            foreach ($incidentslist as $ticket) {
                if ($ticket['last_updated_on'] == '0000-00-00 00:00:00') {
                    $last_updated_on = ' - ';
                } else {
                    $last_updated_on = $ticket['last_updated_on'];
                }
                if ($ticket['status_code'] == 7) {
                    $closed_on = $ticket['last_updated_on'];
                } else {
                    $closed_on = ' - ';
                }


                $html .= "<tr>
							<td><a href=" . base_url() . 'event/tasks/editTicket/' . encode_url($ticket['ticket_id']) . ">" . $ticket['incident_id'] . "</a></td>
							<td>" . $ticket['created_on'] . "</td>
							<td>" . $ticket['client_name'] . "</td>
							<td>" . $ticket['requestor'] . "</td>
							<td>" . $ticket['created_by'] . "</td>
							<td>" . $ticket['subject'] . "</td>
							<td>" . $ticket['assigned_to'] . "</td>
							<td>" . $ticket['queue_name'] . "</td>
							<td>" . $ticket['status'] . "</td>
							<td>" . $ticket['priority'] . "</td>
							<td>" . $ticket['severity'] . "</td>
							<td>" . $ticket['ticket_type'] . "</td>
							<td>" . $ticket['category'] . "</td>
							<td>" . $ticket['subcategory'] . "</td>
							<td>" . $last_updated_on . "</td>
							<td>" . $closed_on . "</td>
						</tr>";
            }
        } else {
            $html .= "<tbody id='siddhu'><tr>
									<td colspan='15'> No Record Found </td>
								</tr>";
        }
        $html .= "</tbody>";
        echo $html;
    }

    public function ajaxGetAllSubCategories() {
        $data['ticket_cate_id'] = $this->input->post('ticket_cate_id');
        if ($data['ticket_cate_id'] == "") {
            $data = array();
        }
        $sub_cats = $this->Model_tasks->getAllSubCategories($data);
        $option = "";
        if ($sub_cats['status'] == 'true') {
            $option .= '<option value="">All</option>';
            foreach ($sub_cats['resultSet'] as $sub_cat) {
                $option .= '<option value="' . $sub_cat["ticket_sub_cate_id"] . '">' . $sub_cat["name"] . '</option>';
            }
        } else {
            $option = '<option value="">No Sub Categories Available</option>';
        }
        echo $option;
    }

    public function save_activity()
	{
				$ticket_id =  $_POST['ticket_id'];
				$this->form_validation->set_rules('start_time', 'Start time', 'required');
				$this->form_validation->set_rules('end_time', 'End time', 'required');
				$this->form_validation->set_rules('activity_type', 'Activity type', 'required');
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
				if ($this->form_validation->run() == TRUE) {
					$y = $_FILES['userfile']['name'];
					$file_name = array();
					if ($y != '')
					{
						$z = $this->do_upload($y);						
						if($z['success'] == 1)
						{
							unset($z['success']);							
							$file_name = $z;
						}
					}
					$startTime = $_POST['start_time'];
					$endTime = $_POST['end_time'];
					$timeFirst  = strtotime($startTime);
					$timeSecond = strtotime($endTime);
					$differenceInSeconds = $timeSecond - $timeFirst;					
			
					
					$data = array(
						"start_time" => $startTime,
						"end_time" => $endTime,
						"activity_type_id" => $_POST['activity_type'],
						"activity_desc" => $_POST['activity_description'],
						"activity_comment" => $_POST['comment'],
						"ticket_id" => $ticket_id,						
						"created_by" => $this->userId,
						"created_on" => $this->current_date,
						"effort_time" => $differenceInSeconds,
					);
					
					
										
				
					$avilTime = 0;
					if(isset($differenceInSeconds, $_POST['contractId']) && !empty($differenceInSeconds))
					{
						$sec = $differenceInSeconds;
						$contractId = $_POST['contractId'];
						if(isset($_POST['old_effort_hours']) && !empty($_POST['old_effort_hours']))// for edit
						{
							$old_effort_hour = $_POST['old_effort_hours'];
							$hours = $differenceInSeconds - $old_effort_hour;						
							$updateConsumed = $this->Model_ticket->updateContractConsumed($hours,$contractId);
						}else
						{
							$updateConsumed = $this->Model_ticket->updateContractConsumed($sec,$contractId);
						}
						
						$avilBoh = $this->Model_ticket->getAvailableBohSec($contractId);
						$avilTime = $avilBoh['available_boh'];
					}
					
					$insertIncidentActivity = $this->Model_ticket->insertIncidentActivity($data);
					
					/* file uploads starts */
					if(!empty($file_name) && isset($file_name))
					{
						foreach($file_name as $file)
						{
							$farray = array(
								"activity_files"=> $file['file_name'],
								"activity_log_id"=> $insertIncidentActivity,
							);
							$this->Model_ticket->insertTicketActivityFiles($farray);
						}						
					}					
					/* file uploads ends */
					
					if(isset($_POST['incident_active_id']) && !empty($_POST['incident_active_id']))
					{
						$incident_active_id = $_POST['incident_active_id'];
					}else
					{
						$incident_active_id = "WO-00".$insertIncidentActivity;
					}
					
					if(isset($_POST['activity_id']) && !empty($_POST['activity_id']))
					{
						$activity_id = $_POST['activity_id'];
						
						$updateDataFile = array(
							"activity_log_id" => $insertIncidentActivity,					
						);						
						$updateIncidentActivityFile = $this->Model_ticket->updateIncidentActivityFile($updateDataFile,$activity_id);
					}
					
					$updateData = array(
					"incident_active_id" => $incident_active_id,
					"current_boh" => $avilTime,
					);
					$updateIncidentActivity = $this->Model_ticket->updateIncidentActivity($updateData,$insertIncidentActivity);
					
					if(isset($_POST['activity_id']) && !empty($_POST['activity_id']))// for edit
					{
						$actId = $_POST['activity_id'];
						$updateValue = array('is_revision'=>'Y');
						$updateOldActivity = $this->Model_ticket->updateIncidentActivity($updateValue,$actId);
					}
					
					//updating total time for ticket
					$getTotalSec = $this->Model_ticket->getTotalSecForTicket($ticket_id);
					if(isset($getTotalSec['status']) && !empty($getTotalSec['resultSet']))
					{
						$previousTotal = $getTotalSec['resultSet']['totalSeconds'];
						//$presentSeconds = $differenceInSeconds;
						$totalTicketEffortTime = $previousTotal;
						$updateTicketData = array('total_time'=>$totalTicketEffortTime);
						$updateTicket = $this->Model_ticket->updateTicketDetails($updateTicketData, $ticket_id);						
					}
					
					
					
					if ($insertIncidentActivity) {
						$this->session->set_flashdata("success_msg", "Active Log is Inserted successfully ..!!");
						redirect('event/tasks/editTicket/'.encode_url($ticket_id));
					} else {
						$this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('event/tasks/editTicket/'.encode_url($ticket_id));
					}
				
				}
				else
				{
					redirect('event/tasks/editTicket/'.encode_url($ticket_id));
				}
	}

    public function do_upload($d)
	{
		$this->load->library('upload');
		$number_of_files_uploaded = count($d);	
		$file_name = 'userfile';
		$files = $_FILES;
		$config = array();
		
		
			for ($i = 0; $i < $number_of_files_uploaded; $i++){
				
				if(isset($files[$file_name]['name'][$i]) && !empty($files[$file_name]['name'][$i])){
				  $_FILES['userfile']['name']     = $files[$file_name]['name'][$i];
				  $_FILES['userfile']['type']     = $files[$file_name]['type'][$i];
				  $_FILES['userfile']['tmp_name'] = $files[$file_name]['tmp_name'][$i];
				  $_FILES['userfile']['error']    = $files[$file_name]['error'][$i];
				  $_FILES['userfile']['size']     = $files[$file_name]['size'][$i];		  
			    
			  
					// Specify configuration for File 1
					$config['upload_path'] = './uploads/incident_activity/';
					$config['allowed_types'] = '*';
					$config['max_size'] = '*';
					$config['max_width']  = '*';
					$config['max_height']  = '*'; 
					//$config['encrypt_name']  = true; 
					
					
					
					  $this->upload->initialize($config);
					  if ( ! $this->upload->do_upload('userfile'))
					  {
						$upload_data['errors'] = $this->upload->display_errors( '<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>',' </div> ');
						$upload_data['success'] = 0;				
					  }
					  else
					  {
						$upload_data[] = $this->upload->data();
						$upload_data['success'] = 1;			
					  }
				}
				
			}
			return $upload_data;
		
	}

    public function download_files($fileName) {
        $this->load->helper('download');
        $file_path = file_get_contents(base_url() . 'uploads/incident_activity/' . $fileName);
        force_download($fileName, $file_path);
    }

    public function assignusers() {
        if (isset($_GET['skill']) && !empty($_GET['skill'])) {
            $skill = decode_url($_GET['skill']);
            $data['getLoginUsers'] = $this->Model_tasks->getLoginUsers($this->client_id, $skill);
            $this->load->view('tasks/assign_users', $data);
        } else {
            echo "Something went wrong";
        }
    }

    public function sshdetails() {
        if (isset($_POST['id']) && !empty($_POST['id'])) {

            $data = array(
                "incident_id" => $_POST['id'],
                "user_id" => $this->userId,
                "login_time" => $this->current_date,
                "login_mode" => "System",
            );
            $bringInsertId = $this->Model_tasks->insertSSHUsers($data);
            echo $bringInsertId;
        } else {
            echo 0;
        }
    }

    public function sshdetailsupdate() {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            $sshId = $_POST['id'];
            $data = array(
                "logout_time" => $this->current_date,
            );
            $bringInsertId = $this->Model_tasks->updateSSHUsers($data, $sshId);
            echo 1;
        } else {
            echo 0;
        }
    }

    public function queueFilter($user_id = "") {
        $queue_id = (isset($_GET['qid'])) ? $_GET['qid'] : "";
        $client_id = $this->client_id;
        /* if(isset($_GET['myincidents']) || $user_id != "")
          {
          $data['myincidents'] = 'ok';
          $user_id = $this->userId;
          //echo "test";exit;

          }else{
          $data['myincidents'] = 'no';
          } */

        //$severity = array();
        $data['clients'] = $this->Model_tasks->getAllClients();
        $data['ticketState'] = $this->Model_tasks->getTicketStatus();
        $data['category'] = $this->Model_tasks->getAllDeviceCategories();
        $data['subcategory'] = $this->Model_tasks->getAllDeviceSubCategories();
        $data['ticketType'] = $this->Model_tasks->getTicketType();
        $data['queue'] = $this->Model_tasks->getQueue($this->userId);
        $data['clientUsers'] = $this->Model_tasks->getClientUsers($this->client_id);
        //$data['subClients'] = $this->Model_tasks->getSubclients($this->client_id);
        $data['severity'] = $this->Model_tasks->getSkillSeverity();
        //pr($data['severity']);exit;
        /* if($data['severity']['status'] == 'true'){
          foreach($data['severity']['resultSet'] as $severity_val){
          $severity[$severity_val['id']] = $severity_val['severity'];
          }
          }
          $data['severity_keys'] = $severity; */
        $data['priority'] = $this->Model_tasks->getUrgencyPriority();
        $data['user_id'] = $this->userId;

        $queueByID = $this->Model_tasks->getQueueDataByID($queue_id);
        //pr($queueByID);exit;
        $urgency_id_string = $queueByID['resultSet']->urgency_id;
        $urgency_id = "'" . str_replace(",", "','", $urgency_id_string) . "'";

        $skill_id_string = $queueByID['resultSet']->skill_id;
        $skill_id = "'" . str_replace(",", "','", $skill_id_string) . "'";

        $ticket_status_id_string = $queueByID['resultSet']->ticket_status_id;
        $ticket_status_id = "'" . str_replace(",", "','", $ticket_status_id_string) . "'";
        //pr($ticket_status_id);exit;
        $data['ticketColumn'] = $this->Model_tasks->getQueueFilter($client_id, $urgency_id, $skill_id, $ticket_status_id);
        //pr($data['ticketColumn']);exit;
        $data['file'] = 'tasks/view_form';
        $this->load->view('template/front_template', $data);
    }

    public function macEdit($ticket_id = "") {
        $roleAccess = helper_fetchPermission('63', 'view');
        if ($roleAccess == 'Y') {
            $ticket_id = decode_url($ticket_id);
            $data['macData'] = $this->Model_tasks->getMacDetails($ticket_id);
            $data['ticketState'] = $this->Model_tasks->getTicketStatus();
            //pr($data['ticketState']);exit;
            //pr($data['macData']);exit;
            $data['file'] = 'tasks/mac_update_form';
            $this->load->view('template/front_template', $data);
        } else {
            redirect('unauthorized');
        }
    }

    public function getIncidentList($dashboard = "", $user_id = "") {
        $status_id = "";
        $client_id = "";
        $ticketIds = array();

        if (isset($_GET['mytasks']) || $user_id != "") {
            $data['mytasks'] = 'ok';
            $user_id = $this->userId;
            //echo "test";exit;
        } else {
            $data['mytasks'] = 'no';
        }


        if (isset($_GET['status_id']) && $_GET['status_id'] != NULL) {
            $status_id = (isset($_GET['status_id'])) ? $_GET['status_id'] : "";
        }
        if (isset($_GET['client_id'])) {
            $client_id = ($_GET['client_id']) ? $_GET['client_id'] : "";
        }

        if (isset($_GET['alarmtype'])) {
            $alarmtype = ($_GET['alarmtype']) ? $_GET['alarmtype'] : "";
            $ticketIds = $this->Model_tasks->getAlarmtypeTicket($alarmtype);
        }
        $data['clients'] = $this->Model_tasks->getAllClients();
        $data['ticketState'] = $this->Model_tasks->getTicketStatus();
        $data['category'] = $this->Model_tasks->getAllDeviceCategories();
        $data['subcategory'] = $this->Model_tasks->getAllDeviceSubCategories();
        $data['ticketType'] = $this->Model_tasks->getTicketType();
        //pr($data['ticketType']);exit;
        $data['queue'] = $this->Model_tasks->getQueue($this->userId);
        $data['clientUsers'] = $this->Model_tasks->getClientUsers($this->client_id);
        $data['severity'] = $this->Model_tasks->getSkillSeverity();
        $data['priority'] = $this->Model_tasks->getUrgencyPriority();
        $data['user_id'] = $this->userId;
        $data['file'] = 'tasks/view_form';
        $this->load->view('template/front_template', $data);
    }

    public function getIncidentData($dashboard = "", $user_id = "") {
        $status_id = "";
        $client_id = "";
        $ticketIds = array();

        if (isset($_GET['mytasks']) || $user_id != "") {
            $data['mytasks'] = 'ok';
            $user_id = $this->userId;
        } else {

            $data['mytasks'] = 'no';
        }

        if (isset($_GET['status_id']) && $_GET['status_id'] != NULL) {
            $status_id = (isset($_GET['status_id'])) ? $_GET['status_id'] : "";
        }
        if (isset($_GET['client_id'])) {
            $client_id = ($_GET['client_id']) ? $_GET['client_id'] : "";
        }

        if (isset($_GET['alarmtype'])) {
            $alarmtype = ($_GET['alarmtype']) ? $_GET['alarmtype'] : "";
            $ticketIds = $this->Model_tasks->getAlarmtypeTicket($alarmtype);
        }

        $requestData = $_REQUEST;
        //pr($requestData);exit;
        $start = $requestData['start'];
        $length = $requestData['length'];
        //$search = $requestData['search']['value'];
        $type = 2;
        $total = $this->Model_tasks->IncidentData($requestData, $type);
        $columns = array(
            // datatable column index  => database column name
            0 => 't.incident_id',
            1 => 't.created_on',
            2 => 'cl.client_title',
            3 => 't.requestor',
            4 => 'uc.first_name',
            5 => 't.subject',
            6 => 'ua.first_name',
            7 => 'ts.status',
            8 => 'p.name',
            9 => 's.title',
            10 => 'dc.category',
            11 => 'sr.service_description',
            12 => 't.last_updated_on',
            13 => 't.last_updated_on'
        );
        $column = $columns[$requestData['order'][0]['column']];
        $dir = $requestData['order'][0]['dir'];
        $result = $this->Model_tasks->IncidentData($requestData, $type, $start, $length, $column, $dir, $status_id, $client_id, $ticketIds, $dashboard, $user_id);
        //pr($result);exit;
        $data = array();
        if (isset($result) && !empty($result)) {
            foreach ($result as $ticket) {  // preparing an array
                if ($ticket['last_updated_on'] == '0000-00-00 00:00:00') {
                    $last_updated_on = ' - ';
                } else {
                    $last_updated_on = $ticket['last_updated_on'];
                }
                if ($ticket['state'] == 7) {
                    $closed_on = $ticket['last_updated_on'];
                } else {
                    $closed_on = ' - ';
                }
                $nestedData = array();

                $nestedData[] = $ticket["incident_id"];
                $nestedData[] = $ticket["created_on"];
                $nestedData[] = $ticket["client"];
                $nestedData[] = $ticket["requestor"];
                $nestedData[] = $ticket["user"];
                $nestedData[] = strip_tags($ticket["subject"]);
                $nestedData[] = $ticket["assigned_to"];
                $nestedData[] = $ticket["state"];
                $nestedData[] = $ticket["priority"];
                $nestedData[] = $ticket["severity"];
                $nestedData[] = $ticket["category"];
                $nestedData[] = $ticket["sub_category"];
                $nestedData[] = $last_updated_on;
                $nestedData[] = $closed_on;
                $nestedData[] = encode_url($ticket["ticket_id"]);

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($total), // total number of records
            "recordsFiltered" => intval($total), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );
        echo json_encode($json_data);  // send data as json format
    }
	
	public function update_activity()
	{
		if(isset($_POST['activityId'],$_POST['ticketId']) && !empty($_POST['activityId']) && !empty($_POST['ticketId']))
		{
			$ticket_id = $_POST['ticketId'];
			$activity_id = $_POST['activityId'];
			$data['ticketDetails'] = $ticketDetails = $this->Model_ticket->getTicketDetails($ticket_id);
			$data['activity_type'] = $this->Model_ticket->getActivityType();
			$data['ticket_id'] = $ticket_id;
			$data['activity_id'] = $activity_id;
			$data['activity'] = $this->Model_ticket->getActivityLogById($activity_id);			
			$ajaxFile = $this->load->view('tasks/edit_activity', $data, TRUE);
			
		}
		else
		{
			$ajaxFile = '';
		}
		echo $ajaxFile;
	}

}
