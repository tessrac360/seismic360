<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ticket extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
		//$this->output->enable_profiler(TRUE);
        $this->userId = $this->session->userdata('id');       
        $this->client_id = $this->session->userdata('client_id');       
        $current_date = $this->config->item('datetime');
		$this->current_date = $this->config->item('datetime');		
        $this->uData = array('last_updated_on' => $current_date);
        $this->load->library('form_validation');
        $this->load->model('Model_ticket');
        $this->load->model('manual/incidents_model','inc');
    }
	
	public function index() {
		$this->getIncidentList();		
    }
	
	public function editTicket($ticket_id = NULL) {
		$roleAccess = helper_fetchPermission('63', 'view');
        if ($roleAccess == 'Y') {
			$ticket_id = decode_url($ticket_id);
			if(isset($ticket_id) && $ticket_id!= ""){
				$data['ticketDetails'] = $ticketDetails = $this->Model_ticket->getTicketDetails($ticket_id);
				$partnerId = $ticketDetails['resultSet']['patner_id'];
				$clientId = $ticketDetails['resultSet']['client_id'];
				$locationId = $ticketDetails['resultSet']['client_loaction'];
				$cateId = $ticketDetails['resultSet']['ticket_cate_id'];			
				$oldStatus = $ticketDetails['resultSet']['status_code'];
				$reopened_count = $ticketDetails['resultSet']['reopened_count'];
				$device_id = $ticketDetails['resultSet']['device_id'];
				$subCateId = $ticketDetails['resultSet']['ticket_sub_cate_id'];
				$serviceId = $ticketDetails['resultSet']['service_id'];
				$data['clients'] = $this->getClients($partnerId,$clientId);
				$data['locations'] = $this->getLocations($clientId,$locationId);			
				$data['services'] = $this->getServices($device_id,$serviceId);			
				$data['devices'] = $this->getDevices($device_id,$clientId);			
				$data['categories'] = $this->getCategory($cateId);			
				$data['subcategories'] = $this->getSubCategory($cateId,$subCateId);
				$data['severities'] = $this->inc->getAllSeverities();
				$data['skillset'] = $this->inc->getAllSkillSet();
				$data['ticketState'] = $this->Model_ticket->getTicketStatus();
				$data['activity_type'] = $this->Model_ticket->getActivityType();			
				$data['activity_log'] = $activity_log = $this->Model_ticket->getActivityLog($ticket_id);
				$activity_files = array();
				if(isset($activity_log,$activity_log['resultSet']) && !empty($activity_log) && !empty($activity_log['resultSet']))
				{
					foreach($activity_log['resultSet'] as $log_files)
					{
						$activity_files[$log_files['activity_log_id']][] = $this->Model_ticket->getActivityLogFiles($log_files['activity_log_id']);
					}
				}
				$data['log_files'] = $activity_files;			
				$data['edit_activity_log'] = $this->Model_ticket->getActivityLogEdit($ticket_id);
				$data['ssh_log'] = $this->Model_ticket->getSSHLog($ticket_id);
				$data['available_boh'] = $this->Model_ticket->getAvailableBoh($ticketDetails['resultSet']['contract_type']);			
				$data['NoOfDays'] = $this->Model_ticket->getTicketClosedDays($ticket_id);
				$data['urgency'] = $this->inc->getUrgency();
				$data['pageTitle'] = 'Ticket Details';
				$data['ticket_id'] = $ticket_id;
				$data['file'] = 'ticket/update_form';			
				$data['Alarm_Correlation'] = $this->Model_ticket->getDetailsDeviceId($device_id);			
						
					$this->form_validation->set_rules('status_code', 'Status', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
					if ($this->form_validation->run() == TRUE) {
						$status_code = $_POST['status_code'];
						$same_status_code = $this->input->post("same_status_code"); 
						$assignid = $this->input->post("assignid");	
						$postTicketDetails = $this->incident_update_array($ticketDetails); //send $ticketDetails for compare old and new post values for maintain incident history
						$updateStatus = $this->Model_ticket->updateTicketDetails($postTicketDetails['update'], $ticket_id);					
						$lastTicketActivityID = $this->Model_ticket->getLastTicketActivityID($ticket_id);
						if($status_code != 0)// not new
						{
							$activityData = array('start_time' => date('Y-m-d H:i:s'),
								'notes' => $postTicketDetails['notes'], 
								'state_status' => $status_code,
								'ticket_id' => $ticket_id,
								'user_id' => $this->userId);
								
							$insertTicketActivity = $this->Model_ticket->insertTicketActivity($activityData);
							$lastActiveUpdate = array('end_time' => date('Y-m-d H:i:s'));
							$updateLastActiveTicket = $this->Model_ticket->updateLastActiveTicket($lastActiveUpdate,array('activity_id' => $lastTicketActivityID['activity_id']));
						}else if(($status_code == 0) && (!empty($assignid)) && ($status_code != $same_status_code))
						{
							$activityData = array('start_time' => date('Y-m-d H:i:s'),
								'notes' => $postTicketDetails['notes'],
								'state_status' => 1,//assgined
								'ticket_id' => $ticket_id,
								'user_id' => $this->userId);
								
							$insertTicketActivity = $this->Model_ticket->insertTicketActivity($activityData);
							$lastActiveUpdate = array('end_time' => date('Y-m-d H:i:s'));
							$updateLastActiveTicket = $this->Model_ticket->updateLastActiveTicket($lastActiveUpdate,array('activity_id' => $lastTicketActivityID['activity_id']));
						}
						
						
						if ($updateStatus['status'] == 'true') {
							$this->session->set_flashdata("success_msg", "Ticket Details are Updated successfully ..!!");
							redirect('event/ticket/editTicket/'.encode_url($ticket_id));
						} else {
							$this->session->set_flashdata("error_msg", "Some thing went wrong");
							redirect('event/ticket/editTicket/'.encode_url($ticket_id));
						}
					} else {
						$data['ticketActivity'] = $this->Model_ticket->ticketActivity($ticket_id);
						$this->load->view('template/front_template', $data);
					}
			}else{
				redirect('my404'); 
			}       
        } else {
            redirect('unauthorized');
        }
    }	
	
	
	public function ajax_client_incidents(){
		
		$client_id = (isset($_POST['id']))?$_POST['id']:"";
		$user_id = (isset($_POST['user']))?$_POST['user']:"";
		$myincidents = (isset($_POST['myincidents']))?$_POST['myincidents']:"";
		//echo $user_id;exit;
		$status_id = "";
		$ticketIds = array();
		$ajax_multiple_ids = array("status_code" => $_POST['status_code'],
		"ticket_cate_id" => $_POST['ticket_cate_id'],
		"ticket_sub_cate_id" => $_POST['ticket_sub_cate_id'],
		"ticket_type_id" => $_POST['ticket_type_id'],
		"queue_id" => $_POST['queue_id'],
		"user_id" => $_POST['user_id'],
		"severity_id" => $_POST['severity_id'],
		"priority_id" => $_POST['priority_id'],
		"date" => $_POST['date']);
		
		if(isset($_GET['status_id']) && $_GET['status_id']!= NULL && !empty($_GET['status_id'])){
			$status_id =  (isset($_GET['status_id']))?$_GET['status_id']:"";
		}			
		if(isset($_GET['alarmtype'])){			
			$alarmtype =  ($_GET['alarmtype'])?$_GET['alarmtype']:"";
			$ticketIds = $this->Model_ticket->getAlarmtypeTicket($alarmtype );	
		}
		if($myincidents =="ok"){
			$data = $this->Model_ticket->getTicketColumn($status_id,$client_id,$ticketIds,$dashboard="",$ajax_multiple_ids,$user_id);
		}else{
			$data = $this->Model_ticket->getTicketColumn($status_id,$client_id,$ticketIds,$dashboard="",$ajax_multiple_ids);	
		}
		//pr($data);exit;
			$html = "";
			if(isset($data) && !empty($data)){
				$html = " <tbody id='siddhu'>";
				
				/* $severity = array();
				$severities = $this->Model_ticket->getSeverity();
				foreach($severities['resultSet'] as $severity_val){
					$severity[$severity_val['id']] = $severity_val['severity'];
				} */
				
				foreach($data as $ticket){
					if($ticket['last_updated_on'] == '0000-00-00 00:00:00')
					{
						$last_updated_on = ' - ';
					}
					else
					{
						$last_updated_on = $ticket['last_updated_on'];
					}
					if($ticket['status_code'] == 7)
					{
						$closed_on = $ticket['last_updated_on'];
					}
					else
					{
						$closed_on = ' - ';
					}

					
					$html .= "<tr>
								<td><a href=".base_url() . 'event/ticket/editTicket/'.encode_url($ticket['ticket_id']).">".$ticket['incident_id']."</a></td>
								<td>".$ticket['created_on']."</td>
								<td>".$ticket['client_name']."</td>
								<td>".$ticket['requestor']."</td>
								<td>".$ticket['created_by']."</td>
								<td>".$ticket['subject']."</td>
								<td>".$ticket['assigned_to']."</td>
								<td>".$ticket['queue_name']."</td>
								<td>".$ticket['status']."</td>
								<td>".$ticket['priority']."</td>
								<td>".$ticket['severity']."</td>
								<td>".$ticket['ticket_type']."</td>
								<td>".$ticket['category']."</td>
								<td>".$ticket['subcategory']."</td>
								<td>".$last_updated_on."</td>
								<td>".$closed_on."</td>
							</tr>";
				}
			}
			else
				{
					$html .= "<tbody id='siddhu'><tr>
										<td colspan='15'> No Record Found </td>
									</tr>";
				}
			$html .= "</tbody>";
		 echo $html;	
		
	}

	
	public function ajax_incidents_search($user_id = ""){
		
		$incidentSearch = (isset($_POST['incidentSearch']))?$_POST['incidentSearch']:""; 
		$user_id = (isset($_POST['user']))?$_POST['user']:"";
		$myincidents = (isset($_POST['myincidents']))?$_POST['myincidents']:"";
		$client_id = (isset($_POST['id']))?$_POST['id']:"";
		
		if($myincidents =="ok"){
			$incidentslist = $this->Model_ticket->getIncidentSearch($incidentSearch,$user_id,$client_id);
		}else{
			$incidentslist = $this->Model_ticket->getIncidentSearch($incidentSearch,"",$client_id);
		}
				
		$html = "";
		if(isset($incidentslist) && !empty($incidentslist)){
			$html = " <tbody id='siddhu'>";
			
			/* $severity = array();
			$severities = $this->Model_ticket->getSeverity();
			foreach($severities['resultSet'] as $severity_val){
				$severity[$severity_val['id']] = $severity_val['severity'];
			} */
			
			foreach($incidentslist as $ticket){
				if($ticket['last_updated_on'] == '0000-00-00 00:00:00')
				{
					$last_updated_on = ' - ';
				}
				else
				{
					$last_updated_on = $ticket['last_updated_on'];
				}
				if($ticket['status_code'] == 7)
				{
					$closed_on = $ticket['last_updated_on'];
				}
				else
				{
					$closed_on = ' - ';
				}

				
				$html .= "<tr>
							<td><a href=".base_url() . 'event/ticket/editTicket/'.encode_url($ticket['ticket_id']).">".$ticket['incident_id']."</a></td>
							<td>".$ticket['created_on']."</td>
							<td>".$ticket['client_name']."</td>
							<td>".$ticket['requestor']."</td>
							<td>".$ticket['created_by']."</td>
							<td>".$ticket['subject']."</td>
							<td>".$ticket['assigned_to']."</td>
							<td>".$ticket['queue_name']."</td>
							<td>".$ticket['status']."</td>
							<td>".$ticket['priority']."</td>
							<td>".$ticket['severity']."</td>
							<td>".$ticket['ticket_type']."</td>
							<td>".$ticket['category']."</td>
							<td>".$ticket['subcategory']."</td>
							<td>".$last_updated_on."</td>
							<td>".$closed_on."</td>
						</tr>";
			}
		}
		else
			{
				$html .= "<tbody id='siddhu'><tr>
									<td colspan='15'> No Record Found </td>
								</tr>";
			}
		$html .= "</tbody>";
		echo $html;	
	}
	
	public function ajaxGetAllSubCategories()
	{
		$data['ticket_cate_id'] = $this->input->post('ticket_cate_id');
		if($data['ticket_cate_id']=="")
		{
			$data = array();
		}
		$sub_cats = $this->Model_ticket->getAllSubCategories($data);
		$option = "";
		if($sub_cats['status'] == 'true')
		{
			$option.= '<option value="">All</option>';
			foreach($sub_cats['resultSet'] as $sub_cat)
			{
				$option.= '<option value="'.$sub_cat["ticket_sub_cate_id"].'">'.$sub_cat["name"].'</option>';
			}
		}
		else
		{
			$option = '<option value="">No Sub Categories Available</option>';
		}
		echo $option;
	}
	
	
	public function save_activity()
	{
				$ticket_id =  $_POST['ticket_id'];
				$this->form_validation->set_rules('start_time', 'Start time', 'required');
				$this->form_validation->set_rules('end_time', 'End time', 'required');
				$this->form_validation->set_rules('activity_type', 'Activity type', 'required');
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
				if ($this->form_validation->run() == TRUE) {
					$y = $_FILES['userfile']['name'];
					$file_name = array();
					if ($y != '')
					{
						$z = $this->do_upload($y);						
						if($z['success'] == 1)
						{
							unset($z['success']);							
							$file_name = $z;
						}
					}
					$startTime = $_POST['start_time'];
					$endTime = $_POST['end_time'];
					$timeFirst  = strtotime($startTime);
					$timeSecond = strtotime($endTime);
					$differenceInSeconds = $timeSecond - $timeFirst;					
			
					
					$data = array(
						"start_time" => $startTime,
						"end_time" => $endTime,
						"activity_type_id" => $_POST['activity_type'],
						"activity_desc" => $_POST['activity_description'],
						"activity_comment" => $_POST['comment'],
						"ticket_id" => $ticket_id,						
						"created_by" => $this->userId,
						"created_on" => $this->current_date,
						"effort_time" => $differenceInSeconds,
					);
					
					
										
				
					$avilTime = 0;
					if(isset($differenceInSeconds, $_POST['contractId']) && !empty($differenceInSeconds))
					{
						$sec = $differenceInSeconds;
						$contractId = $_POST['contractId'];
						if(isset($_POST['old_effort_hours']) && !empty($_POST['old_effort_hours']))// for edit
						{
							$old_effort_hour = $_POST['old_effort_hours'];
							$hours = $differenceInSeconds - $old_effort_hour;						
							$updateConsumed = $this->Model_ticket->updateContractConsumed($hours,$contractId);
						}else
						{
							$updateConsumed = $this->Model_ticket->updateContractConsumed($sec,$contractId);
						}
						
						$avilBoh = $this->Model_ticket->getAvailableBohSec($contractId);
						$avilTime = $avilBoh['available_boh'];
					}
					
					$insertIncidentActivity = $this->Model_ticket->insertIncidentActivity($data);
					
					
					/* file uploads starts */
					if(!empty($file_name) && isset($file_name))
					{
						foreach($file_name as $file)
						{
							$farray = array(
								"activity_files"=> $file['file_name'],
								"activity_log_id"=> $insertIncidentActivity,
							);
							$this->Model_ticket->insertTicketActivityFiles($farray);
						}						
					}					
					/* file uploads ends */
					
					
					
					if(isset($_POST['incident_active_id']) && !empty($_POST['incident_active_id']))
					{
						$incident_active_id = $_POST['incident_active_id'];
					}else
					{
						$incident_active_id = "WO-00".$insertIncidentActivity;
					}
					
					if(isset($_POST['activity_id']) && !empty($_POST['activity_id']))
					{
						$activity_id = $_POST['activity_id'];
						
						$updateDataFile = array(
							"activity_log_id" => $insertIncidentActivity,					
						);						
						$updateIncidentActivityFile = $this->Model_ticket->updateIncidentActivityFile($updateDataFile,$activity_id);
					}
					
					$updateData = array(
					"incident_active_id" => $incident_active_id,
					"current_boh" => $avilTime,
					);
					
					
					
					$updateIncidentActivity = $this->Model_ticket->updateIncidentActivity($updateData,$insertIncidentActivity);
					
					
					if(isset($_POST['activity_id']) && !empty($_POST['activity_id']))// for edit
					{
						$actId = $_POST['activity_id'];
						$updateValue = array('is_revision'=>'Y');
						$updateOldActivity = $this->Model_ticket->updateIncidentActivity($updateValue,$actId);
					}
					
					//updating total time for ticket
					$getTotalSec = $this->Model_ticket->getTotalSecForTicket($ticket_id);
					if(isset($getTotalSec['status']) && !empty($getTotalSec['resultSet']))
					{
						$previousTotal = $getTotalSec['resultSet']['totalSeconds'];
						//$presentSeconds = $differenceInSeconds;
						$totalTicketEffortTime = $previousTotal;
						$updateTicketData = array('total_time'=>$totalTicketEffortTime);
						$updateTicket = $this->Model_ticket->updateTicketDetails($updateTicketData, $ticket_id);						
					}
					
					
					
					if ($insertIncidentActivity) {
						$this->session->set_flashdata("success_msg", "Active Log is Inserted successfully ..!!");
						redirect('event/ticket/editTicket/'.encode_url($ticket_id));
					} else {
						$this->session->set_flashdata("error_msg", "Some thing went wrong");
						redirect('event/ticket/editTicket/'.encode_url($ticket_id));
					}
				
				}
				else
				{
					redirect('event/ticket/editTicket/'.encode_url($ticket_id));
				}
	}
	
	public function do_upload($d)
	{
		$this->load->library('upload');
		$number_of_files_uploaded = count($d);	
		$file_name = 'userfile';
		$files = $_FILES;
		$config = array();
		
		
			for ($i = 0; $i < $number_of_files_uploaded; $i++){
				
				if(isset($files[$file_name]['name'][$i]) && !empty($files[$file_name]['name'][$i])){
				  $_FILES['userfile']['name']     = $files[$file_name]['name'][$i];
				  $_FILES['userfile']['type']     = $files[$file_name]['type'][$i];
				  $_FILES['userfile']['tmp_name'] = $files[$file_name]['tmp_name'][$i];
				  $_FILES['userfile']['error']    = $files[$file_name]['error'][$i];
				  $_FILES['userfile']['size']     = $files[$file_name]['size'][$i];		  
			    
			  
					// Specify configuration for File 1
					$config['upload_path'] = './uploads/incident_activity/';
					$config['allowed_types'] = '*';
					$config['max_size'] = '*';
					$config['max_width']  = '*';
					$config['max_height']  = '*'; 
					//$config['encrypt_name']  = true; 
					
					
					
					  $this->upload->initialize($config);
					  if ( ! $this->upload->do_upload('userfile'))
					  {
						$upload_data['errors'] = $this->upload->display_errors( '<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>',' </div> ');
						$upload_data['success'] = 0;				
					  }
					  else
					  {
						$upload_data[] = $this->upload->data();
						$upload_data['success'] = 1;			
					  }
				}
				
			}
			return $upload_data;
		
	}
	
	
	public function download_files($fileName) {
		$this->load->helper('download');
		$file_path = file_get_contents(base_url().'uploads/incident_activity/'.$fileName); 
		 force_download($fileName, $file_path);
	}
	
	public function update_activity()
	{
		if(isset($_POST['activityId'],$_POST['ticketId']) && !empty($_POST['activityId']) && !empty($_POST['ticketId']))
		{
			$ticket_id = $_POST['ticketId'];
			$activity_id = $_POST['activityId'];
			$data['ticketDetails'] = $ticketDetails = $this->Model_ticket->getTicketDetails($ticket_id);
			$data['activity_type'] = $this->Model_ticket->getActivityType();
			$data['ticket_id'] = $ticket_id;
			$data['activity_id'] = $activity_id;
			$data['activity'] = $this->Model_ticket->getActivityLogById($activity_id);			
			$ajaxFile = $this->load->view('ticket/edit_activity', $data, TRUE);
			
		}
		else
		{
			$ajaxFile = '';
		}
		echo $ajaxFile;
	}
	
	
	public function assignusers()
	{
		if(isset($_GET['skill'],$_GET['p']) && !empty($_GET['skill']) && !empty($_GET['p']))
		{
			$skill = decode_url($_GET['skill']);
			$patnerId = $_GET['p'];
			$data['getLoginUsers'] = $this->Model_ticket->getLoginUsers($patnerId,$skill);
			$this->load->view('ticket/assign_users', $data);
		}
		else
		{
			echo "Something went wrong";
		}
		
	}
	
	public function sshdetails()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			
			$data = array(
			"incident_id" => $_POST['id'], 
			"user_id" => $this->userId, 
			"login_time" => $this->current_date, 
			"login_mode" => "System", 
			);
			$bringInsertId = $this->Model_ticket->insertSSHUsers($data);
			echo $bringInsertId;
			
		}
		else
		{
			echo 0;
		}
		
	}
	public function sshloglist()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
					
			$ssh_log = $this->Model_ticket->getSSHLog($_POST['id']);
			//echo count($ssh_log["resultSet"]);exit;
			$list ="";
			if (!empty($ssh_log["resultSet"])) {
				$i =1;
				
				foreach($ssh_log["resultSet"] as $ssh){
					$list .='<tr><td style="width:2%;">'.$i++.'</td>         
					<td>'.$ssh["username"].'</td>         
					<td>'.$ssh["login_time"].'</td>         
					<td>'.$ssh["logout_time"].'</td>         
					<td>'.$ssh["login_mode"].'</td>         
					<td>'.$this->effort($ssh["effort_time"]).'</td></tr>';
				}
				echo $list ;//exit;
			}else{
				$list .='<tr><td colspan="6"> No Record Found </td></tr>'; 
				echo $list ;				
			}			
		}
		else
		{
			$list .='<tr><td colspan="6"> No Record Found </td></tr>'; 
			echo $list ;
		}
		
	}
	public function effort($seconds)
	{
		//$seconds = 86400;
		$H = floor($seconds / 3600);
		$i = ($seconds / 60) % 60;
		$s = $seconds % 60;
		return sprintf("%02d:%02d:%02d", $H, $i, $s);
	}		
	public function sshdetailsupdate()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			$sshId = $_POST['id'];
			$data = array(
				"logout_time" => $this->current_date, 			
			);
			$bringInsertId = $this->Model_ticket->updateSSHUsers($data,$sshId);
			echo 1;
			
		}
		else
		{
			echo 0;
		}
		
	}
	
	public function queueFilter($user_id = ""){
		$queue_id = (isset($_GET['qid']))?$_GET['qid']:"";
		$client_id =  $this->client_id;
		/* if(isset($_GET['myincidents']) || $user_id != "")
		{
			$data['myincidents'] = 'ok';
			$user_id = $this->userId;
			//echo "test";exit;

		}else{
			$data['myincidents'] = 'no';
		} */
		
		//$severity = array();
		//$data['clients'] = $this->Model_ticket->getAllClients();
		$data['ticketState'] = $this->Model_ticket->getTicketStatus();
		$data['category'] = $this->Model_ticket->getAllDeviceCategories();
		$data['subcategory'] = $this->Model_ticket->getAllDeviceSubCategories();
		$data['ticketType'] = $this->Model_ticket->getTicketType();
		$data['queue'] = $this->Model_ticket->getQueue($this->userId);
		$data['clientUsers'] = $this->Model_ticket->getClientUsers($this->client_id);
		//$data['subClients'] = $this->Model_ticket->getSubclients($this->client_id);
		$data['severity'] = $this->Model_ticket->getSkillSeverity();
		//pr($data['severity']);exit;
		/* if($data['severity']['status'] == 'true'){
			foreach($data['severity']['resultSet'] as $severity_val){
				$severity[$severity_val['id']] = $severity_val['severity'];
			}
		}
		$data['severity_keys'] = $severity; */
		$data['priority'] = $this->Model_ticket->getUrgencyPriority();
		$data['user_id'] = $this->userId;
		
		$queueByID = $this->Model_ticket->getQueueDataByID($queue_id);
		//pr($queueByID);exit;
		$urgency_id_string = $queueByID['resultSet']->urgency_id;
		$urgency_id = "'".str_replace(",","','",$urgency_id_string)."'";
		
		$skill_id_string = $queueByID['resultSet']->skill_id;
		$skill_id = "'".str_replace(",","','",$skill_id_string)."'";

		$ticket_status_id_string = $queueByID['resultSet']->ticket_status_id;
		$ticket_status_id = "'".str_replace(",","','",$ticket_status_id_string)."'";
		//pr($ticket_status_id);exit;
		$data['ticketColumn'] = $this->Model_ticket->getQueueFilter($client_id,$urgency_id,$skill_id,$ticket_status_id);
		//pr($data['ticketColumn']);exit;
		$data['file'] = 'ticket/view_form';
        $this->load->view('template/front_template', $data);
	}
	
	public function macEdit($ticket_id=""){
		$roleAccess = helper_fetchPermission('63', 'view');
        if ($roleAccess == 'Y') {
			$ticket_id = decode_url($ticket_id);
			$data['macData'] = $this->Model_ticket->getMacDetails($ticket_id);	
			$data['ticketState'] = $this->Model_ticket->getTicketStatus();
			//pr($data['ticketState']);exit;
			//pr($data['macData']);exit;
			$data['file'] = 'ticket/mac_update_form';
			$this->load->view('template/front_template', $data);
		}else {
            redirect('unauthorized');
        }
	}
	
	public function getIncidentList($dashboard = "",$user_id = ""){
		$status_id = "";
		$client_id = "";
		$ticketIds = array();
		
		if(isset($_GET['myincidents']) || $user_id != "")
		{
			$data['myincidents'] = 'ok';
			$user_id = $this->userId;
			//echo "test";exit;

		}else{
			$data['myincidents'] = 'no';
		}
		
		
		if(isset($_GET['status_id'])&& $_GET['status_id']!= NULL){
			$data1 = base64_decode($_GET['status_id']);
			$status_array = explode(",", $data1);
			$data['status_id'] =  (isset($status_array))? $status_array:"";
			//pr($status_id);exit;
			
		}
		
		if(isset($_GET['client_id'])){
			$client_id =  ($_GET['client_id'])?$_GET['client_id']:"";
		}		

		if(isset($_GET['alarmtype'])){			
			$alarmtype =  ($_GET['alarmtype'])?$_GET['alarmtype']:"";
			$ticketIds = $this->Model_ticket->getAlarmtypeTicket($alarmtype);	
		}
		$userClients = helper_getAccessClientDropDown();
		//pr($userClients);exit;
		if(isset($userClients['resultSet']) && (count($userClients['resultSet'])>0))
		{
			$data['partners'] = $this->Model_ticket->getAllClients($userClients['resultSet']);
		}
		//$data['requestors'] = $this->Model_ticket->getRequestors();
		//$getClients = helper_getClientsDetails($data['partners']['resultSet']['id']);
		//pr($getClients);exit;
		$data['ticketState'] = $this->Model_ticket->getTicketStatus();
		$data['category'] = $this->Model_ticket->getAllDeviceCategories();
		//$data['subcategory'] = $this->Model_ticket->getAllDeviceSubCategories();
		$data['ticketType'] = $this->Model_ticket->getTicketType();
		//pr($data['ticketType']);exit;
		$data['queue'] = $this->Model_ticket->getQueue($this->userId);
		//$data['clientUsers'] = $this->Model_ticket->getClientUsers($this->client_id);
		$data['severity'] = $this->Model_ticket->getSkillSeverity();
		$data['priority'] = $this->Model_ticket->getUrgencyPriority();
		$data['user_id'] = $this->userId;
		//$data['status_ids'] = isset($status_id) && $status_id !="";
		$data['file'] = 'ticket/view_form';
        $this->load->view('template/front_template', $data);
	}
	
	public function getIncidentData($dashboard = "",$user_id = "")
	{
		$status_id = "";
		$client_id = "";
		$ticketIds = array();
		
		if(isset($_GET['myincidents']) || $user_id != "")
		{
			$data['myincidents'] = 'ok';
			$user_id = $this->userId;
			//echo "test";exit;

		}else{
			$data['myincidents'] = 'no';
		}
		
		if(isset($_GET['status_id'])&& $_GET['status_id']!= NULL){
			
			$data = base64_decode($_GET['status_id']);
			$status_array = explode(",", $data);
			$status_id =  (isset($status_array))? $status_array:"";
			
		}		
		if(isset($_GET['client_id'])){
			$client_id =  ($_GET['client_id'])?$_GET['client_id']:"";
		}		

		if(isset($_GET['alarmtype'])){			
			$alarmtype =  ($_GET['alarmtype'])?$_GET['alarmtype']:"";
			$ticketIds = $this->Model_ticket->getAlarmtypeTicket($alarmtype);	
		}
		
		$requestData = $_REQUEST;
		//pr($requestData);exit;
		$start = (isset($requestData['start']))?$requestData['start']:'';
		$length = (isset($requestData['length']))?$requestData['length']:'';
		//$search = $requestData['search']['value'];
		$type = 1;
		$total = $this->Model_ticket->IncidentData($requestData,$type);
		//pr($total); exit;
		$columns = array( 
		// datatable column index  => database column name
			0 =>'t.incident_id', 
			1 => 't.created_on',
			2=> 'cls.client_title',
			3=> 'cl.client_title',
			4=> 't.requestor',
			5=> 'uc.first_name',
			6=> 't.subject',
			7=> 'ua.first_name',
			8=> 'ts.status',
			9=> 'p.name',
			10=> 's.title',
			//11=> 'tt.name',
			11=> 'dc.category',
			12=> 'dsb.subcategory',
			13=> 't.last_updated_on',
			14=> 't.last_updated_on'
			
		);
		
		
		$column = (isset($columns[$requestData['order'][0]['column']]) && !empty($columns[$requestData['order'][0]['column']]))?$columns[$requestData['order'][0]['column']]:'';
		$dir = (isset($requestData['order'][0]['dir']))?$requestData['order'][0]['dir']:'';
		$result = $this->Model_ticket->IncidentData($requestData,$type,$start,$length,$column,$dir,$status_id,$client_id,$ticketIds,$dashboard,$user_id);
		//pr($result);exit;
		$data = array();
		if(isset($result) && !empty($result)){
			foreach($result as $ticket){  // preparing an array
			
				if($ticket['last_updated_on'] == '0000-00-00 00:00:00')
				{
					$last_updated_on = ' - ';
				}
				else
				{
					$last_updated_on = $ticket['last_updated_on'];
				}
				if($ticket['status_code'] == 7)
				{
					$closed_on = $ticket['last_updated_on'];
				}
				else
				{
					$closed_on = ' - ';
				}
				if($ticket["partner"] == $ticket["client"]){
					$client = "NA";
				}else{
					$client = $ticket["client"];
				}
				
				if($ticket["requestor"]== ""){
					$requestor = "System Generated";
				}else{
					$requestor = $ticket["requestor"];
				}
				$nestedData=array(); 

				$nestedData[] = $ticket["incident_id"];
				$nestedData[] = $ticket["created_on"];
				$nestedData[] = $ticket["partner"];
				$nestedData[] = $client;
				$nestedData[] = $requestor;
				$nestedData[] = $ticket["user"];
				$nestedData[] = strip_tags($ticket["subject"]);
				$nestedData[] = $ticket["assigned_to"];
				$nestedData[] = $ticket["state"];
				$nestedData[] = $ticket["priority"];
				$nestedData[] = $ticket["severity"];
				//$nestedData[] = $ticket["ticket_type"];
				$nestedData[] = $ticket["category"];
				$nestedData[] = $ticket["sub_category"];
				$nestedData[] = $last_updated_on;
				$nestedData[] = $closed_on; 
				$nestedData[] = encode_url($ticket["ticket_id"]);
				
				$data[] = $nestedData;
			}
		}
			$json_data = array(
			"draw"            => intval($requestData['draw']),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval($total),  // total number of records
			"recordsFiltered" => intval($total), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
			echo json_encode($json_data);  // send data as json format
	
	}
	
	public function avilBog()
	{
		if(isset($_POST['client_id']) && !empty($_POST['client_id']))
		{
			$clientId = $_POST['client_id'];
		}
	}
	

	
	function getLocations($client,$locationId = NULL)
	{
		//get Locations
		$locationsData = array();
			$getUUIDofClient = $this->inc->getAllClients($client);
			if(isset($getUUIDofClient['resultSet']) && (count($getUUIDofClient['resultSet'])>0))
			{
				$getLocations = $this->inc->getOneClientAlllocations($getUUIDofClient['resultSet'][0]['client_uuid']);
				
				if(isset($getLocations['resultSet']) && (count($getLocations['resultSet'])>0))
				{
					foreach($getLocations['resultSet'] as $location_data)
					{
						$locationDetais = array(
								'id'=>$location_data['location_uuid'],
								'text'=>$location_data['location_name'],
							);
							if(($locationId != NULL) && ($location_data['location_uuid'] == $locationId))
							{
								$selected = array('selected'=>'true');
								$locationDetais = array_merge($locationDetais,$selected);
							}
							array_push($locationsData,$locationDetais);
					}					
					
				}
				else
				{
					$locationDetais = array(
								'id' => 0,
								'text' => 'NA',
								'selected'=>'true'
							);
					array_push($locationsData,$locationDetais);
				}
			}
			return json_encode($locationsData);
	}
	
	function getClients($partner = NULL,$clientId = NULL)
	{
		$clientData = array();
		if($partner != NULL)
		{
			
			if($clientId == $partner)
			{
				$clientDetais = array(
						'id'=>$partner,
						'text'=>'NA',
						'selected'=>'true'
					);					
				array_push($clientData,$clientDetais);
			}
			
				//get Cleints
				$getClients = helper_getClientsDetails($partner);
				if(isset($getClients['resultSet']) && (count($getClients['resultSet'])>0))
				{
									
					if(isset($getClients['resultSet']) && (count($getClients['resultSet'])>0))
					{
						foreach($getClients['resultSet'] as $client_data)
						{
								$clientDetais = array(
									'id'=>$client_data['id'],
									'text'=>$client_data['client_title'],
								);
								if(($clientId != NULL) && ($client_data['id'] == $clientId))
								{
									$selected = array('selected'=>'true');
									$clientDetais = array_merge($clientDetais,$selected);
								}
								array_push($clientData,$clientDetais);
							
						}					
						
					}
					
				}
				
		}
		return json_encode($clientData);
	}
	
	function getDevices($device_id = NULL,$client = NULL)
	{
		$deviceData = array();
		if($client != NULL)
		{
			$devices = $this->inc->getOneClientDevices($client);

					if(isset($devices['resultSet']) && (count($devices['resultSet'])>0))
					{
						foreach($devices['resultSet'] as $device_data)
						{
							$deviceDetais = array(
								'id'=>$device_data['device_id'],
								'text'=>$device_data['device_name']
								);
							if(($device_id != NULL) && ($device_data['device_id'] == $device_id))
							{
								$selected = array('selected'=>'true');
								$deviceDetais = array_merge($deviceDetais,$selected);
							}
								array_push($deviceData,$deviceDetais);						
						}					
					}
		}
		return json_encode($deviceData);
	}
	
	
	function getServices($device_id = NULL,$serviceId = NULL)
	{
		$serviceData = array();
		if($device_id != NULL)
		{
			$services = $this->inc->getOneDeviceServices($device_id);

					if(isset($services['resultSet']) && (count($services['resultSet'])>0))
					{
						foreach($services['resultSet'] as $service_data)
						{
							$serviceDetais = array(
								'id'=>$service_data['service_id'],
								'text'=>$service_data['service_description']
								);
							if(($serviceId != NULL) && ($service_data['service_id'] == $serviceId))
							{
								$selected = array('selected'=>'true');
								$serviceDetais = array_merge($serviceDetais,$selected);
							}
								array_push($serviceData,$serviceDetais);						
						}					
					}
		}
		return json_encode($serviceData);
	}
	
	function getCategory($cateId = NULL)
	{
		$categoryData = array();
		$categories = $this->inc->getAllCategory();	

				if(isset($categories['resultSet']) && (count($categories['resultSet'])>0))
				{
					foreach($categories['resultSet'] as $category_data)
					{
						$cateDetais = array(
							'id'=>$category_data['id'],
							'text'=>$category_data['category']
							);
						if(($cateId != NULL) && ($category_data['id'] == $cateId))
						{
							$selected = array('selected'=>'true');
							$cateDetais = array_merge($cateDetais,$selected);
						}
							array_push($categoryData,$cateDetais);						
					}					
				}
				return json_encode($categoryData);
	}
	
	function getSubCategory($cateId = NULL,$subCateId = NULL)
	{
		$subcategoryData = array();
		if($cateId != NULL)
		{
			$subcategories = $this->inc->getSubCategoriesByCateId($cateId);
				if(isset($subcategories['resultSet']) && (count($subcategories['resultSet'])>0))
				{
					foreach($subcategories['resultSet'] as $subcategory_data)
					{
						$subcateDetais = array(
							'id'=>$subcategory_data['id'],
							'text'=>$subcategory_data['subcategory']
							);
						if(($subCateId != NULL) && ($subcategory_data['id'] == $subCateId))
						{
							$selected = array('selected'=>'true');
							$subcateDetais = array_merge($subcateDetais,$selected);
						}
							array_push($subcategoryData,$subcateDetais);						
					}					
				}
		}
		return json_encode($subcategoryData);
	}
	
	
	function incident_update_array($oldIncidentDetails = NULL){
		$newValues = $_POST;
        $notes = $this->compareOldAndNew($oldIncidentDetails,$newValues);		
		$clientId = $_POST['client'];
		$status_code = $_POST['status_code'];
		$same_status_code = $this->input->post("same_status_code"); 
		$assignid = $this->input->post("assignid");
		$oldAssignId = $this->input->post("oldAssignId");		
		$statusDetails = $this->Model_ticket->getStatusDetails($status_code);
		$userDetails = helper_getUserInformation('ID',$this->userId);
		$userName = $userDetails['resultSet']->first_name.' '.$userDetails['resultSet']->last_name;
		
		
		
		if($status_code == 6) // reopen
		{
			$postTicketDetails = array('status_code' => $status_code, 'assigned_to' => $assignid, 'reopened_count' => $reopened_count+1);						
		}
		else if($status_code == 0)//new
		{
			$postTicketDetails = array('status_code' => 1, 'assigned_to' => $assignid);			
		}
		else if(($status_code == 1) && ($assignid != $oldAssignId))//reassigned
		{
			$postTicketDetails = array('status_code' => 1, 'assigned_to' => $assignid);			
		}
		else if($status_code == 1)//assigned
		{
			$postTicketDetails = array('status_code' => 1, 'assigned_to' => $assignid);			
		}
		else
		{
			$postTicketDetails = array('status_code' => $status_code, 'assigned_to' => $assignid);			
		}					
		$postTicketDetails = array_merge($postTicketDetails, $this->uData);	
		
		$remainPostValues = array(
			"client_id" => $clientId,
			"client_loaction" => $_POST['location'],
			"device_id" => $_POST['device'],
			"service_id" => $_POST['service'],
			"ticket_cate_id" => $_POST['category'],
			"ticket_sub_cate_id" => $_POST['subcategory'],
			"priority" => $_POST['urgency'],
			"severity_id" => $_POST['severity'],
			"skill_id" => $_POST['skill_id'],
		);
		$postTicketDetails = array_merge($postTicketDetails, $remainPostValues);
		$returnArray = array(
			'update' => $postTicketDetails,
			'notes' => $notes,
		);
		return $returnArray;
	}
	
	function compareOldAndNew($old = NULL, $new = NULL)
	{
		$responseText = '';	
		$userDetails = helper_getUserInformation('ID',$this->userId);
		$userName = $userDetails['resultSet']->first_name.' '.$userDetails['resultSet']->last_name;
		
		
		if(($old != NUll) && ($new != NULL))
		{
			if(isset($old['resultSet'], $new) && !empty($old['resultSet']) && !empty($new))
			{
				$oldValues = $old['resultSet'];				
				$clientName = ""; $oldClientName = "";
				if(isset($oldValues['client_id'],$new['client']) && !empty($oldValues['client_id']) && !empty($new['client']))
				{
					$Details = helper_getClientInformation('ID',$new['client']);					
					$clientName = $Details['resultSet']->client_title;
					$oldDetails = helper_getClientInformation('ID',$oldValues['client_id']);					
					$oldClientName = $oldDetails['resultSet']->client_title;
				}
				$location = ""; $oldLocation = "";
				if(isset($oldValues['client_loaction'],$new['location']) && !empty($oldValues['client_loaction']) && !empty($new['location']))
				{
					$Details = helper_getClientLocationInfo('UUID',$new['location']);					
					$location = $Details['resultSet']->location_name;
					$oldDetails = helper_getClientLocationInfo('UUID',$oldValues['client_loaction']);					
					$oldLocation = $oldDetails['resultSet']->location_name;
				}
				$status = ""; $oldStatus = "";
				if(isset($oldValues['status_code'],$new['status_code']))
				{
					$Details = helper_getIncidentStausName('ID',$new['status_code']);
					$status = $Details['resultSet']->status;
					$oldDetails = helper_getIncidentStausName('ID',$oldValues['status_code']);		
					$oldStatus = $oldDetails['resultSet']->status;
				}
				$device = ""; $oldDevice = "";
				if(isset($oldValues['device_id'],$new['device']) && !empty($oldValues['device_id']) && !empty($new['device']))
				{
					$Details = helper_getdevicesInfo('ID',$new['device']);					
					$device = $Details['resultSet']->device_name;
					$oldDetails = helper_getdevicesInfo('ID',$oldValues['device_id']);					
					$oldDevice = $oldDetails['resultSet']->device_name;
				}
				$service = ""; $oldService = "";
				if(isset($oldValues['service_id'],$new['service']) && !empty($oldValues['service_id']) && !empty($new['service']))
				{
					$Details = helper_getservicesInfo('ID',$new['service']);					
					$service = $Details['resultSet']->service_description;
					$oldDetails = helper_getservicesInfo('ID',$oldValues['service_id']);					
					$oldService = $oldDetails['resultSet']->service_description;
				}
				$category = ""; $oldCategory = "";
				if(isset($oldValues['ticket_cate_id'],$new['category']) && !empty($oldValues['ticket_cate_id']) && !empty($new['category']))
				{
					$Details = helper_getDeviceCateInfo('ID',$new['category']);					
					$category = $Details['resultSet']->category;
					$oldDetails = helper_getDeviceCateInfo('ID',$oldValues['ticket_cate_id']);					
					$oldCategory = $oldDetails['resultSet']->category;
				}
				$subcategory = ""; $oldSubCategory = "";
				if(isset($oldValues['ticket_sub_cate_id'],$new['subcategory']) && !empty($oldValues['ticket_sub_cate_id']) && !empty($new['subcategory']))
				{
					$Details = helper_getDeviceSubCateInfo('ID',$new['subcategory']);					
					$subcategory = $Details['resultSet']->subcategory;
					$oldDetails = helper_getDeviceSubCateInfo('ID',$oldValues['ticket_sub_cate_id']);					
					$oldSubCategory = $oldDetails['resultSet']->subcategory;
				}
				$assignUserName = ""; $oldassignUserName = "";
				if(isset($new['assignid'],$oldValues['assigned_to']))
				{
					$assignUserDetails = helper_getUserInformation('ID',$new['assignid']);
					$assignUserName = $assignUserDetails['resultSet']->first_name.' '.$assignUserDetails['resultSet']->last_name;
					if(!empty($oldValues['assigned_to']))
					{
						$oldassignUserDetails = helper_getUserInformation('ID',$oldValues['assigned_to']);
						$oldassignUserName = $oldassignUserDetails['resultSet']->first_name.' '.$oldassignUserDetails['resultSet']->last_name;
					}					
				}
				$urgency = ""; $oldUrgency = "";
				if(isset($oldValues['priority'],$new['urgency']) && !empty($oldValues['priority']) && !empty($new['urgency']))
				{
					$Details = helper_getUrgencyInfo('ID',$new['urgency']);					
					$urgency = $Details['resultSet']->name;
					$oldDetails = helper_getUrgencyInfo('ID',$oldValues['priority']);					
					$oldUrgency = $oldDetails['resultSet']->name;
				}
				$eventCritical = ""; $oldEventCritical = "";
				if(isset($oldValues['severity_id'],$new['severity']) && !empty($oldValues['severity_id']) && !empty($new['severity']))
				{
					$Details = helper_getSeveritiesInfo('ID',$new['severity']);					
					$eventCritical = $Details['resultSet']->severity;
					$oldDetails = helper_getSeveritiesInfo('ID',$oldValues['severity_id']);					
					$oldEventCritical = $oldDetails['resultSet']->severity;
				}
				$severity = ""; $oldSeverity = "";
				if(isset($oldValues['skill_id'],$new['skill_id']) && !empty($oldValues['skill_id']) && !empty($new['skill_id']))
				{
					$Details = helper_getSkillsInfo('ID',$new['skill_id']);					
					$severity = $Details['resultSet']->title;
					$oldDetails = helper_getSkillsInfo('ID',$oldValues['skill_id']);					
					$oldSeverity = $oldDetails['resultSet']->title;
				}
						
				
				
				#1 check client
				if($oldValues['client_id'] != $new['client'])
				{
					if(isset($oldValues['client_id'],$new['client']) && !empty($oldValues['client_id']) && !empty($new['client'])){
					$responseText .= "Client name changed from ".$oldClientName." to ".$clientName." </br>";
					}
				}
				#2 check Location
				if($oldValues['client_loaction'] != $new['location'])
				{
					
					if(isset($oldValues['client_loaction'],$new['location']) && !empty($oldValues['client_loaction']) && !empty($new['location'])){
					$responseText .= "Location changed from ".$oldLocation." to ".$location." </br>";
					}
				}
				#3 check Status
				if($oldValues['status_code'] != $new['status_code'])
				{
					if(isset($oldValues['status_code'],$new['status_code']) && !empty($oldValues['status_code']) && !empty($new['status_code'])){
					$responseText .= "Status updated from ".$oldStatus." to ".$status." </br>";
					}
				}
				#4 check Device
				if($oldValues['device_id'] != $new['device'])
				{
					if(isset($oldValues['device_id'],$new['device']) && !empty($oldValues['device_id']) && !empty($new['device'])){
					 $responseText .= "Device changed from ".$oldDevice." to ".$device." </br>";
					}
				}
				#5 check Service
				if($oldValues['service_id'] != $new['service'])
				{
					if(isset($oldValues['service_id'],$new['service']) && !empty($oldValues['service_id']) && !empty($new['service'])){
					$responseText .= "Service changed from ".$oldService." to ".$service." </br>";
					}
				}
				#6 check Category
				if($oldValues['ticket_cate_id'] != $new['category'])
				{
					if(isset($oldValues['ticket_cate_id'],$new['category']) && !empty($oldValues['ticket_cate_id']) && !empty($new['category'])){
					$responseText .= "Category changed from ".$oldCategory." to ".$category."  </br>";
					}
				}
				#7 check Subcategory
				if($oldValues['ticket_sub_cate_id'] != $new['subcategory'])
				{
					if(isset($new['subcategory'],$oldValues['ticket_sub_cate_id']) && !empty($new['subcategory']) && !empty($oldValues['ticket_sub_cate_id']))
					{
							$responseText .= "Subcategory changed from ".$oldSubCategory." to ".$subcategory." </br>";
					}
					
				}
				#8 check Assign
				if($oldValues['assigned_to'] != $new['assignid'])
				{
					if($oldValues['status_code'] == 0)
					{
						$responseText .= "Assigned to ".$assignUserName." </br>";
					}elseif(($oldValues['status_code'] == 1) && ($new['status_code'] == 1)) 
					{
						$responseText .= "Reassigned from ".$oldassignUserName." to ".$assignUserName." </br>"; //reassign
					}else
					{
						$responseText .= "Reassigned from ".$oldassignUserName." to ".$assignUserName." </br>"; //reassign
					}     			
					
				}
				#9 check Urgency
				if($oldValues['priority'] != $new['urgency'])
				{
					if(isset($new['urgency'],$oldValues['priority']) && !empty($new['urgency']) && !empty($oldValues['priority']))
					{
					$responseText .= "Urgency changed from ".$oldUrgency." to ".$urgency."  </br>";
					}
				}
				#10 check Event Critical
				if($oldValues['severity_id'] != $new['severity'])
				{
					if(isset($new['severity'],$oldValues['severity_id']) && !empty($new['severity']) && !empty($oldValues['severity_id']))
					{
					$responseText .= "Event Critical changed from ".$oldEventCritical." to ".$eventCritical." </br>";
					}
				}
				#11 check Severity
				if($oldValues['skill_id'] != $new['skill_id'])
				{
					if(isset($new['skill_id'],$oldValues['skill_id']) && !empty($new['skill_id']) && !empty($oldValues['skill_id']))
					{
					$responseText .= "Severity changed from ".$oldSeverity." to ".$severity." </br>";
					}
				}
				#12 check External Reference
				if($oldValues['ticket_external_ref'] != $new['external_ref'])
				{
					$responseText .= "External Reference changed from ".$oldValues['ticket_external_ref']." to ".$new['external_ref']." </br>";
				}
				#13 check Vendor Reference
				if($oldValues['vendor_ref'] != $new['vendor_ref'])
				{
					$responseText .= "Vendor Reference changed from ".$oldValues['vendor_ref']." to ".$new['vendor_ref']." </br>";
				}
			}
		}
		else
		{
			$responseText = '';
		}
		return $responseText;
	}
	

	public function ajax_getAllClientDetails(){
		$requesterData = '';		
		$usersData = '<option value="">Select Option</option>';
		$clientData = '';
		if(isset($_POST['value']) && !empty($_POST['value']))
		{
			$partner_ids = $_POST['value'];
			
			//get requester
			$getUUIDofClient = $this->Model_ticket->getAllClients($partner_ids);
			$clientUUIDs = array();
			if(isset($getUUIDofClient['resultSet']) && (count($getUUIDofClient['resultSet'])>0))
			{	
				foreach($getUUIDofClient['resultSet'] as $value){
					$clientUUIDs[]= $value['client_uuid'];
				}
			}
			$getRequester = $this->Model_ticket->getAllRequestorsFromClients($clientUUIDs);
			if(isset($getRequester['resultSet']) && (count($getRequester['resultSet'])>0))
			{
				foreach($getRequester['resultSet'] as $requester_data)
				{
					$requesterData .= '<option value='.$requester_data['contact_uuid'].'>'.$requester_data['contact_name'].'</option>';	
				}				
			}			
			
			//get Users
			$getClientId = $this->Model_ticket->getAllClients($partner_ids);
			$clientIDs = array();
			if(isset($getClientId['resultSet']) && (count($getClientId['resultSet'])>0))
			{	
				foreach($getClientId['resultSet'] as $value){
					$clientIDs[]= $value['id'];
				}
			}
			$getUsers = $this->Model_ticket->getUsersListByCompanyId($clientIDs);
			if(isset($getUsers['resultSet']) && (count($getUsers['resultSet'])>0))
			{
				foreach($getUsers['resultSet'] as $user_data)
				{
					$usersData .= '<option value='.$user_data['id'].'>'.$user_data['user'].'</option>';	
				}								
				
			} 

			//get Cleints
			$getClients = $this->Model_ticket->getClientsDetails($partner_ids);
			if(isset($getClients['resultSet']) && (count($getClients['resultSet'])>0))
			{
				foreach($getClients['resultSet'] as $client_data)
				{
					$clientData .= '<option value='.$client_data['id'].'>'.$client_data['client_title'].'</option>';	
				}			
				
			}

		}
		$return['requesterData'] = $requesterData;
		$return['usersData'] = $usersData;
		$return['clientData'] = $clientData;
		echo json_encode($return);
		
	}
	
	public function ajax_getAllSubCategories()
	{
		$subcategoryData = '';
		if(isset($_POST['value']) && !empty($_POST['value']))
		{
			$cateIds = $_POST['value'];
			//pr($cateIds);exit;
			$subcategories = $this->Model_ticket->getSubCategoriesByCateId($cateIds);			
			if(isset($subcategories['resultSet']) && (count($subcategories['resultSet'])>0))
			{
				foreach($subcategories['resultSet'] as $subcategory_data)
				{
					$subcategoryData .= '<option value='.$subcategory_data['id'].'>'.$subcategory_data['subcategory'].'</option>';					
				}
			}
	
		}
		$return['subcategoryData'] = $subcategoryData;
		echo json_encode($return);
	}
	
	public function generateManualUuid(){
		$x = generate_uuid($_GET['prefix'].'_');
		echo $x;
		//var_dump($x);
		
	}
}

	
