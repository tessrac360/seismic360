<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//comit
class Event extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->userId = $this->session->userdata('id');
        $this->client_id = $this->session->userdata('client_id');
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->client_id = $this->session->userdata('client_id');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->load->library('form_validation');
        $this->load->model('Model_event');
    }
   

    public function treeClient($client_id = "") {
        $parent_client_id = $client_id;
        $clients = array();
        $resultSet = $this->Model_event->getClientTreeView($parent_client_id, 1);
        if (!empty($resultSet)) {
            foreach ($resultSet as $value) {
                $client['id'] = $value['id'];
                $client['client_title'] = $value['client_title'];
                $client['parent_client_id'] = $value['parent_client_id'];
                $client['children'] = $this->getClientChildren($value['id']);
                $clients[] = $client;
            }
            return $clients[0];
        } else {
            return array();
        }
    }

    public function getClientChildren($parent_id) {
        $clients = array();
        $resultSet = $this->Model_event->getClientTreeView($parent_id);
        foreach ($resultSet as $value) {
            $client['id'] = $value['id'];
            $client['client_title'] = $value['client_title'];
            $client['parent_client_id'] = $value['parent_client_id'];
            $client['children'] = $this->getClientChildren($value['id']);
            $clients[] = $client;
        }
        return $clients;
    }

    public function index() {
        $this->importEventColumnSort();
        $data['eventColumn'] = $this->Model_event->getEventColumn();
        //pr($data['eventColumn']);exit;
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }
    public function dashBoardEvents() {
        $this->importEventColumnSort();
        $data['eventColumn'] = $this->Model_event->getEventColumn();
        //pr($data['eventColumn']);exit;
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }
    private function importEventColumnSort() {
        $dataResult = $this->Model_event->getEventColumnRename();
        if ($dataResult['status'] == 'true') {
            for ($i = 0; $i < count($dataResult['resultSet']); $i++) {
                $insertEventColumnSort[] = array("rename_id" => $dataResult['resultSet'][$i]->rename_id, "position" => $i, "client_id" => $this->client_id, "user_id" => $this->userId);
            }
            $this->db->insert_batch('event_column_sort', $insertEventColumnSort);
        }
    }

    public function ajax_eventLoad() {
        $eventColumn = $this->Model_event->getEventColumn();
        //pr($_POST);exit;

        if (isset($_POST['eventId'])) {
            $eventId = $_POST['eventId'];
        }

        $col_1 = $eventColumn['resultSet'][1]->table_column_name;
        $col_2 = $eventColumn['resultSet'][2]->table_column_name;
        $col_3 = $eventColumn['resultSet'][3]->table_column_name;
        $col_4 = $eventColumn['resultSet'][4]->table_column_name;
        $col_5 = $eventColumn['resultSet'][5]->table_column_name;
        $col_6 = $eventColumn['resultSet'][6]->table_column_name;
        $col_7 = $eventColumn['resultSet'][7]->table_column_name;
        $col_8 = $eventColumn['resultSet'][8]->table_column_name;
        $col_9 = $eventColumn['resultSet'][9]->table_column_name;
        $col_10 = $eventColumn['resultSet'][10]->table_column_name;
        $col_11 = $eventColumn['resultSet'][11]->table_column_name;
        $col_12 = $eventColumn['resultSet'][12]->table_column_name;
        $eventFilter = $this->input->post();
        $returnEvent = $this->Model_event->getEvent($eventFilter);
        if ($returnEvent['status'] == 'true') {
            $html = "";
            foreach ($returnEvent['resultSet'] as $value) {
                $html .= '<tr  class="eventtr dnd-moved event_' . $value->event_id . '"  rowId=' . $value->event_id . '>                
                                <td style="position:relative;"><a href="javascript:void(0);" class="coupon_question" evtid=' . $value->event_id . ' >' . $value->event_invoice . '</a></td>
                                <td style="position:relative;">' . $value->$col_1 . '</td>
                                <td style="position:relative;">' . $value->$col_2 . '</td>
                                <td style="position:relative;">' . $value->$col_3 . '</td>
                                <td style="position:relative;">' . $value->$col_4 . '</td>
                                <td style="position:relative;">' . $value->$col_5 . '</td>
                                <td style="position:relative;">' . $value->$col_6 . '</td>
                                <td style="position:relative;">' . $value->$col_7 . '</td>
                                <td style="position:relative;">' . $value->$col_8 . '</td>
                                <td style="position:relative;">' . $value->$col_9 . '</td>
                                <td style="position:relative;">' . $value->$col_10 . '</td>   
                                <td style="position:relative;">' . $value->$col_11 . '</td> 
                                <td style="position:relative;">' . $value->$col_12 . '</td>   
                            </tr>';
            }
            $return['html'] = $html;
            $return['status'] = 'true';
        } else {
            $html = '<tr><td colspan=13>No Event Data found...</td></tr>';

            $return['html'] = $html;
            $return['status'] = 'false';
        }
//pr($return['html']);exit;
        echo $json = json_encode($return);
        exit;
    }

    public function ajax_eventHeaderLoad() {
        $data['filter_hostName'] = $this->Model_event->getHostName();
        $data['filter_severity'] = $this->Model_event->getSeverity();
        $data['filter_clients'] = $this->Model_event->getClientFilter();
        $data['filter_tools'] = $this->Model_event->getToolFilter();
        $data['eventColumn'] = $this->Model_event->getEventColumn();
        $data['filter_service'] = $this->Model_event->getService();
        $data['filter_state'] = $this->Model_event->getState();
        $this->load->view('ajax_eventHeaderLoad', $data);
    }

    function viewdetail() {
        $event_id = $this->input->get_post('u');
        $data['events'] = $this->Model_event->getEventDetails($event_id);
        $data['eventsHistory'] = $this->Model_event->getEventsHistory($data['events']['resultSet']['device_id'], $data['events']['resultSet']['service_id']);
        $data['eventMonitorService'] = $this->Model_event->getEventMonitorService($data['events']['resultSet']['device_id']);
        $this->load->view('event/event_view', $data);
    }

    public function ajax_eventSorting() {
        $i = 1;
        foreach ($_POST["position"] as $sort_id) {
            $this->db->where('sort_id', $sort_id);
            $this->db->update('event_column_sort', array('position' => $i));
            $i++;
        }
        $return['status'] = 'true';
        echo $json = json_encode($return);
        exit;
    }

    public function ajax_eventDetails() {
        $event_id = $_POST['eventId'];
        $events = $this->Model_event->getEventDetails($event_id);
        $eventsHistory = $this->Model_event->getEventsHistory($events['resultSet']['device_id'], $events['resultSet']['service_id']);
        $eventMonitorService = $this->Model_event->getEventMonitorService($events['resultSet']['device_id']);
        if ($events['status'] == 'true') {
            $eventResultSet = $events['resultSet'];
            $htmlEvent = '<div class="box-body" style="height:475px; overflow: scroll; display: block;">
                                    <table class="table">
                                        <tbody>
                                            <tr><th>Client :</th><td>' . $eventResultSet['client_title'] . '</td></tr>
                                            <tr><th>Host name :</th><td>' . $eventResultSet['device_name'] . '</td></tr>
                                            <tr><th>Host IP :</th><td>' . $eventResultSet['ip_address'] . '</td></tr>
                                        </tbody>
                                    </table>
                                </div>';
            $html['htmlEvent'] = $htmlEvent;
        }

        if ($eventsHistory['status'] == 'true') {
            $htmlEventHistory = "";
            $htmlEventHistory .= '<div class="box-body" style="height:475px;overflow:scroll;">
                        <table class="table table-bordered dataTable">
                            <thead>
                                <tr>
                                    <th>Host Name</th>
                                    <th>Service</th>
                                    <th>Description</th>
                                    <th>State</th>
                                    <th>Event Start Time</th>                      
                                </tr>
                            </thead>
                            <tbody>';
            foreach ($eventsHistory['resultSet'] as $historyData) {
                $htmlEventHistory .= '<tr style="cursor:pointer" title="Click to open" bgcolor="#99FF99">
                                <td>' . $historyData['device_name'] . '</td>
                                <td>' . $historyData['service_name'] . '</td>
                                <td>' . $historyData['event_text'] . '</td>
                                <td>' . $historyData['severity'] . '</td>
                                <td>' . $historyData['event_start_time'] . '</td>
                            </tr>';
            }
            $htmlEventHistory .= '</tbody>
                        </table>
                    </div>';
            $html['htmlEventHistory'] = $htmlEventHistory;
        }

        if ($eventMonitorService['status'] == 'true') {
            $htmlEventMonitorService = "";
            $htmlEventMonitorService .= ' <div class="box-body" style="height:475px;overflow:scroll;">
                <table class="table table-bordered table-striped dataTable">
                    <thead>
                        <tr>
                           <th>Service</th>
                            <th>Event Text</th>
                            <th>Severity</th>
                            <th>Start Time</th>
                            <th>Duration</th>
                        </tr>
                    </thead>
                    <tbody>';

            foreach ($eventMonitorService['resultSet'] as $historyData) {

                $htmlEventMonitorService .= '<tr>
                            <td>' . $historyData['service_name'] . '</td>
                        <td>' . $historyData['event_text'] . '</td>
                        <td>' . $historyData['severity'] . '</td>
                        <td>' . $historyData['event_start_time'] . '</td>
                        <td>' . $historyData['Duration'] . '</td>	
                        </tr>';
            }

            $htmlEventMonitorService .= '</tbody>
                </table>
            </div>';
            $html['htmlEventMonitorService'] = $htmlEventMonitorService;
        }

        $htmlActivityLog = '<div class="box-body" style="height:475px;overflow:scroll;">
                                    <table class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <tr>
                                                <th>Activity No.</th>
                                                <th>Created Time</th>
                                                <th>Type</th>
                                                <th>Mode</th>
                                                <th>Status</th>
                                                <th>Description</th>
                                                <th>Start Time</th>
                                                <th>End Time</th>
                                                <th>Duration</th>
                                                <th>Activity Owner</th>
                                                <th>Call Recording</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>';

        $html['htmlActivityLog'] = $htmlActivityLog;
        $return['html'] = $html;
        $return['status'] = 'true';

        echo $json = json_encode($return);
        exit;
    }

    public function ajax_manualAcknowledge() {
        $event_acknowledgements = array();
        for ($i = 0; $i < count($_POST['client_id']); $i++) {

            $event_acknowledgements[] = array('client_id' => $_POST['client_id'][$i], 'event_id' => $_POST['eventId'][$i], 'ack_status_id' => $_POST['ack_status_id'], 'ack_status_id' => $_POST['ack_status_id'], 'ack_notes' => $_POST['txtmanualack'], 'created_on' => $this->current_date, 'created_by' => $this->current_user, 'ack_type' => 'A');
        }
        $insertid = $this->db->insert_batch('event_acknowledgements', $event_acknowledgements);
        if ($insertid) {
            $return['eventId'] = $_POST['eventId'];
            $return['message'] = 'Manually Acknowledged !!';
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        echo $json = json_encode($return);
        die();
    }

    public function ajax_surpressAcknowledge() {
        if ($_POST['su_event_id'] != "" && $_POST['client_id'] != "" && $_POST['datetimeSupress'] != "" && $_POST['ack_type'] != "") {
            $duration = strtotime($_POST['datetimeSupress']) - strtotime($this->current_date);
            $data = array('event_id' => $_POST['su_event_id'], 'client_id' => $_POST['client_id'], 'ack_status_id' => '4', 'event_suppress_starttime' => $this->current_date, 'event_suppress_endtime' => $_POST['datetimeSupress'], 'duration' => $duration, 'ack_notes' => $_POST['txtsurpress'], 'created_on' => $this->current_date, 'created_by' => $this->current_user, 'ack_type' => $_POST['ack_type']);
            if ($this->db->insert('event_acknowledgements', $data)) {
                $dataEvent = array('is_suppressed' => 'Y','is_suppress_type' => $_POST['ack_type']);
                $this->db->where('event_id', $_POST['su_event_id']);
                $this->db->update('events', $dataEvent);
                $return['message'] = 'Event is Suppressed';
                $return['eventId'] = $_POST['su_event_id'];
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        echo $json = json_encode($return);
        die();
    }

    public function ajax_surpressPermanent() {

        if ($_POST['su_event_id'] != "" && $_POST['client_id'] != "" && $_POST['ack_type'] != "") {
            $data = array('event_id' => $_POST['su_event_id'], 'client_id' => $_POST['client_id'], 'ack_status_id' => '4', 'created_on' => $this->current_date, 'created_by' => $this->current_user, 'ack_type' => $_POST['ack_type'],'ack_notes' => $_POST['txtsurpress']);
            if ($this->db->insert('event_acknowledgements', $data)) {                
                $dataEvent = array('is_suppressed' => 'Y','is_suppress_type' => $_POST['ack_type']);
                $this->db->where('event_id', $_POST['su_event_id']);
                $this->db->update('events', $dataEvent);
                $return['message'] = 'Event is Permanently Suppressed';
                $return['eventId'] = $_POST['su_event_id'];
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        echo $json = json_encode($return);
        die();
    }

    public function ajax_acknowledgement() {
        if ($_POST['su_event_id'] != "" && $_POST['client_id'] != "" && $_POST['ack_type'] != "") {
            $data = array('event_id' => $_POST['su_event_id'], 'client_id' => $_POST['client_id'], 'ack_status_id' => '2', 'ack_notes' => $_POST['txtacknowledgement'], 'created_on' => $this->current_date, 'created_by' => $this->current_user, 'ack_type' => $_POST['ack_type']);
            // pr($data);exit;
            if ($this->db->insert('event_acknowledgements', $data)) {
                $return['message'] = 'Manually Acknowledged !!';
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        echo $json = json_encode($return);
        die();
    }

    public function ajax_surpressAcknowledgeUpdate() {
        $data = array('event_suppress_till' => $_POST['datetimeSupress'], 'ack_notes' => $_POST['txtsurpress']);
        $this->db->where('ack_id', $_POST['ack_id']);
        if ($this->db->update('event_acknowledgements', $data)) {
            $return['message'] = 'Suppressed Event is Updated';
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        echo $json = json_encode($return);
        die();
    }

    public function ajax_openAcknowledgeDiv() {
        $event_id = $_POST['eventId'];
        $data['events'] = $this->Model_event->getEventDetails($event_id);
        $data['eventsAck'] = $this->Model_event->getEventAck($event_id);
        $data['eventsHistory'] = $this->Model_event->getEventsHistory($data['events']['resultSet']['device_id'], $data['events']['resultSet']['service_id']);
        $data['eventMonitorService'] = $this->Model_event->getEventMonitorService($data['events']['resultSet']['device_id']);
        $data['event_id'] = $event_id;
        //pr($data['events']);exit;
        $this->load->view('ack', $data);
    }

    public function ajax_openSuppressPopup() {
        $event_id = $_POST['eventId'];
        $data['events'] = $this->Model_event->getEventDetails($event_id);
        $data['event_id'] = $event_id;
        $this->load->view('event/suppresspopup', $data);
    }

    public function ajax_suppressReactivate() {
        if (isset($_POST['suppress_ack_id']) && isset($_POST['suppress_event_id'])) {
            $this->db->where('ack_id', $_POST['suppress_ack_id']);
            $this->db->where('event_id', $_POST['suppress_event_id']);
            if ($this->db->delete('event_acknowledgements')) {
                $return['status'] = 'true';
                $return['message'] = "Suppressed Event is Reactivated";
                $return['event_id'] = $_POST['suppress_event_id'];
            }
        } else {
            $return['status'] = 'false';
        }


        //pr($return);
        echo $json = json_encode($return);
        exit;
    }

    public function ajax_isEnterSubpress() {
        //pr($_POST);
        if (isset($_POST['currentVal'])) {
            $this->db->select('event_id');
            $this->db->from('event_acknowledgements');
            $this->db->where('event_id', $_POST['currentVal']);
            $this->db->where('event_suppress_till >=', $this->current_date);
            $query = $this->db->get();
            //echo $this->db->last_query();
            if ($query->num_rows() > 0) {

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }

        echo $json = json_encode($return);
        exit;
    }

    public function ajax_acknowlegementDetails() {
        $event_id = $_POST['eventId'];
        $events = $this->Model_event->getAcknoledgeDetails($event_id);

        if ($events['status'] == 'true') {
            $htmlAcknowledgement = "";

            foreach ($events['resultSet'] as $value) {

                $htmlAcknowledgement .= ' <li class="mt-list-item">                                                                              
                                        <div class="list-item-content">
                                            <ul style="list-style-type: none;">
                                                <li><strong>Name:</strong> ' . $value->name . '</li>
                                                <li><strong>Time:</strong> ' . $value->ack_time . '</li>';
                if ($value->suppress_datetime != '00-00-0000 00:00') {
                    $htmlAcknowledgement .= '<li><strong>Suppress Time:</strong> ' . $value->suppress_datetime . '</li>';
                }
                $htmlAcknowledgement .= '<li><strong>Status:</strong> ' . $value->ack_status . '</li>
                                                    <li><strong> Notes:</strong><span title="' . $value->ack_notes . '"> ' . wordlimit($value->ack_notes, 50) . '</span></li>
                                                 </ul>
                                                 </div>
                                                 </li>';
            }


            $return['html'] = $htmlAcknowledgement;
            $return['status'] = 'true';
        }
        echo $json = json_encode($return);
        exit;
    }

    public function history() {
        $data['clients'] = $this->Model_event->getClientFilter();
        $data['hostName'] = $this->Model_event->getHostName();
        $data['service'] = $this->Model_event->getService();
        $data['severity'] = $this->Model_event->getSeverityFilter();
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'event/history/';
        if ($this->input->post()) {
            $filterPost = $this->input->post();
            if (isset($_GET)) {
                $getvalue = $_GET;
                $filterSerializePost = $filterPost;
                $mergeData = array_merge($getvalue, $filterSerializePost);
                $filterSerializePost = serialize($mergeData);
            } else {
                $filterSerializePost = serialize($filterPost);
            }
            $this->session->set_userdata("eventHistorySession", $filterSerializePost);
            $config['total_rows'] = count($this->Model_event->getEventHistory());
            $config['per_page'] = 100;
            $config['uri_segment'] = 3;
            $config['display_pages'] = TRUE;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['eventHistory'] = $this->Model_event->getEventHistory($config['per_page'], $page);
        } else if (isset($_GET)) {
            $getvalue = $_GET;
            $this->session->unset_userdata('eventHistorySession'); 
            if (isset($_SESSION['eventHistorySession'])) {
                $oldSessionvalue = unserialize($_SESSION['eventHistorySession']);
                $mergeData = array_merge($getvalue, $oldSessionvalue);
                $filterSerializePost = serialize($mergeData);
            } else {
                $filterSerializePost = serialize($_GET);
            }
            $this->session->set_userdata("eventHistorySession", $filterSerializePost);
            $config['total_rows'] = count($this->Model_event->getEventHistory());
            $config['per_page'] = 100;
            $config['uri_segment'] = 3;
            $config['display_pages'] = TRUE;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['eventHistory'] = $this->Model_event->getEventHistory($config['per_page'], $page);
           
        } else {
            $config['total_rows'] = count($this->Model_event->getEventHistory());
            $config['per_page'] = 100;
            $config['uri_segment'] = 3;
            $config['display_pages'] = TRUE;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['eventHistory'] = $this->Model_event->getEventHistory($config['per_page'], $page);
        }
        $data['file'] = 'event_history';
        $this->load->view('template/front_template', $data);
    }

    public function search($serv = "") {
        $data['clients'] = $this->Model_event->getClientFilter();
        $data['hostName'] = $this->Model_event->getHostName();
        $data['service'] = $this->Model_event->getService();
        $data['severity'] = $this->Model_event->getSeverityFilter();
        $data['eventHistory'] = array();
        if ($serv != "") {
            $data['eventHistory'] = $this->Model_event->getEventHistorySearch($serv);
        } else {
            $data['eventHistory'] = array();
        }
        $data['file'] = 'event_history_search';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_eventHistoryReset() {
        $this->session->unset_userdata('eventHistorySession');       
        $return['status'] = 'true';
        echo $json = json_encode($return);
        exit;
    }

    public function ajax_ticket() {
        $data['file'] = 'event_history';
        $this->load->view('template/front_template', $data);
    }

    public function cronSuppressedActive() {
        $this->db->select('event_id,(SELECT event_suppress_endtime FROM `eventedge_event_acknowledgements` WHERE event_id=eventedge_events.event_id ORDER by ack_id DESC LIMIT 1) as suppress_end_time');
        $this->db->from('events');
        $this->db->where('is_suppressed', 'Y');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                if ($value->suppress_end_time != '0000-00-00 00:00:00') {
                    if ($value->suppress_end_time <= $this->current_date) {
                        $this->db->where('event_id', $value->event_id);
                        $this->db->update('events', array('is_suppressed' => 'N'));
                    }
                }
            }
        }
        exit;
    }

    public function suppress() {
        $data['clients'] = $this->Model_event->getClientFilter();
        $data['hostName'] = $this->Model_event->getHostName();
        $data['service'] = $this->Model_event->getService();
        $data['severity'] = $this->Model_event->getSeverityFilter();
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'event/suppresss/';
        $config['total_rows'] = count($this->Model_event->getEventSuppress());
        $config['per_page'] = 100;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['eventSuppress'] = $this->Model_event->getEventSuppress($config['per_page'], $page);
        $data['file'] = 'event_suppress';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_reactivateSuppress() {
        // pr($_POST);

        if ($_POST['ack_type'] == 'T') {
            $ackId = $_POST['ackId'];
            $this->db->select("*");
            $this->db->from('event_acknowledgements');
            $this->db->where('ack_id', $ackId);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $dataAck = $query->row();
                $duration = strtotime($this->current_date) - strtotime($dataAck->event_suppress_starttime);
                $dataNewRecord = array('client_id' => $dataAck->client_id, 'event_id' => $dataAck->event_id, 'ack_status_id' => $dataAck->ack_status_id, 'event_suppress_starttime' => $dataAck->event_suppress_starttime, 'event_suppress_endtime' => $this->current_date, 'duration' => $duration, 'ack_notes' => $dataAck->ack_notes, 'created_on' => $this->current_date, 'created_by' => $this->current_user, 'ack_type' => 'T');
                if ($this->db->insert('event_acknowledgements', $dataNewRecord)) {
                    $this->db->where('event_id', $_POST['evtid']);
                    $this->db->update('events', array('is_suppressed' => 'N','is_suppress_type'=>''));
                   // echo $this->db->last_query();
                    $this->db->where('ack_id', $ackId);
                    $this->db->update('event_acknowledgements', array('is_history' => 'Y'));
                    $return['status'] = 'true';
                    $return['message'] = "Event is reactivated";
                } else {
                    $return['status'] = 'true';
                    $return['message'] = "Some thing went wrong";
                }
            }
        } elseif ($_POST['ack_type'] = 'P') {
            $this->db->where('event_id', $_POST['evtid']);
            $this->db->update('events',  array('is_suppressed' => 'N','is_suppress_type'=>''));
            $return['status'] = 'true';
            $return['message'] = "Event is reactivated";
        }
        echo $json = json_encode($return);
        exit;
    }

   

}
