<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Queue extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->userId = $this->session->userdata('id');
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model("Model_queue");
        $this->load->model("Model_ticket");
        $this->load->library('form_validation');
    }

    public function index() {
       //$roleAccess = helper_fetchPermission('13', 'view');
       // if ($roleAccess == 'Y') {
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		$config['base_url'] = base_url() . 'event/queue/index/';
        $config['total_rows'] = $this->Model_queue->getQueueDetails($search);
		//pr($config['total_rows']);exit;
		$config['uri_segment'] = 4;		      
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;      
        $data['queue'] = $this->Model_queue->getQueueDetails($search, $config['per_page'], $page);
		//pr($data['queue']);exit;
		$data['hiddenURL'] = 'event/queue/index/';
		//pr($data['clientTitles']);exit;
        $data['file'] = 'queue/view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
	        /* } else {
            redirect('unauthorized');
        } */
    }

	public function create() {
		//$roleAccess = helper_fetchPermission('68', 'add');
       // if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $queuePost = $this->input->post();
				//pr($queuePost);exit;
                
                $this->form_validation->set_rules('queue_name', 'Queue Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
					$queuePost['severity_id'] = implode(",", $queuePost['severity_id']);
					$queuePost['skill_id'] = implode(",", $queuePost['skill_id']);
					$queuePost['ticket_status_id'] = implode(",", $queuePost['ticket_status_id']);
                    $queuePost = array_merge($queuePost, $this->cData);
                    $resultData = $this->Model_queue->insertQueue($queuePost);
                    if ($resultData['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "Queue is created successfully ..!!");
                        redirect('event/queue');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('event/queue/create');
                    }
                } else {
                    $data['severity'] = $this->Model_queue->getSeverity();
					$data['priority'] = $this->Model_ticket->getPriority();
					$data['ticketState'] = $this->Model_ticket->getTicketStatus();
					$data['pageTitle'] = 'Your p44age title';
                    $data['file'] = 'queue/create_form';
					$this->load->view('template/front_template', $data);
                }
			} else {
					$data['severity'] = $this->Model_queue->getSeverity();
					$data['priority'] = $this->Model_ticket->getPriority();
					$data['ticketState'] = $this->Model_ticket->getTicketStatus();
					$data['pageTitle'] = 'Your p44age title';
                    $data['file'] = 'queue/create_form';
					$this->load->view('template/front_template', $data);
            }
		/*} else {
            redirect('unauthorized');
        } */
    } 
	
	public function edit($postId = NULL) {			
		//$client_id = decode_url($client_id);
        //$roleAccess = helper_fetchPermission('67', 'edit');
		//echo $roleAccess;exit;
        //if ($roleAccess == 'Y') {
            $postId = decode_url($postId);
			//pr($postId);exit;
                if ($this->input->post()) {
                    $queuePost = $this->input->post();
					//pr($queuePost);exit;
					
					$this->form_validation->set_rules('queue_name', 'Queue Name', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
						$queuePost['severity_id'] = implode(",", $queuePost['severity_id']);
						$queuePost['skill_id'] = implode(",", $queuePost['skill_id']);
						$queuePost['ticket_status_id'] = implode(",", $queuePost['ticket_status_id']);
						$queuePost = array_merge($queuePost, $this->uData);
                        $updateStatus = $this->Model_queue->updateQueue($queuePost, $postId);
						//pr($updateStatus);exit;
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "Queue is Updated successfully ..!!");
							redirect('event/queue');								
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
							redirect('event/queue/edit/'.encode_url($postId));							
                        }
                    } else {
						$data['severity'] = $this->Model_queue->getSeverity();
						$data['priority'] = $this->Model_ticket->getPriority();
						$data['ticketState'] = $this->Model_ticket->getTicketStatus();
                        $data['getQueue'] = $this->Model_queue->getQueueEdit($postId);
                        $data['pageTitle'] = 'Your page title';
                        $data['file'] = 'queue/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['severity'] = $this->Model_queue->getSeverity();
					$data['priority'] = $this->Model_ticket->getPriority();
					$data['ticketState'] = $this->Model_ticket->getTicketStatus();
                    $data['getQueue'] = $this->Model_queue->getQueueEdit($postId);
//pr($data['getQueue']);exit;					
                    $data['pageTitle'] = 'Your page title';
                    $data['file'] = 'queue/update_form';
                    $this->load->view('template/front_template', $data);
                }
            
        /* } else {
            redirect('unauthorized');
        } */
    }
	
	public function delete($postId = "") {
        $postId = decode_url($postId);
        if ($postId != "") {
            $deleteQueue = $this->Model_queue->isDeleteQueue($postId);
			//pr($deleteRole);exit;
            if ($deleteQueue['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "Queue is deleted successfully..!!");
                redirect('event/queue/');
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('event/queue/');
            }
        } else {
            redirect('event/queue/');
        }
    }
	
	public function ajax_checkUniqueQueueName() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $recordCount = $this->db->get_where($table, array($field => $value))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

}
