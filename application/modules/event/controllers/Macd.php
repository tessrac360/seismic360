<?php
header('Access-Control-Allow-Origin: *');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Macd extends MX_Controller {

  
    public function __construct() {
        parent::__construct();
		$this->load->model('Model_macd','macd');		
    }	
	
	public function client()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			$cid = $_POST['id'];
			$contacts = $this->macd->getLocations($cid);
			if(isset($contacts['resultSet']) && !empty($contacts['resultSet']) && (count($contacts['resultSet']) > 0))
			{
				$contact_persons = array();
				foreach($contacts['resultSet'] as $data)
				{
					$contact_person['location_id'] = $data['location_id'];
					$contact_person['location_uuid'] = $data['location_uuid'];
					$contact_person['location_name'] = $data['location_name'];
					$contact_person['contact_name'] = $data['contact_name'];
					$contact_persons[] = $contact_person;
				}
				$return_data['status'] = TRUE;
				$return_data['data'] = $contact_persons;
				echo json_encode($return_data);
			}else
			{
				$return_data['status'] = FALSE;
				$return_data['data'] = '';
				echo json_encode($return_data);
			}
		}
		else
		{
			$return_data['status'] = FALSE;
			$return_data['data'] = '';
			echo json_encode($return_data);
		}
	}
	
	
	public function client_title()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			$cid = $_POST['id'];
			$contacts = $this->macd->getClient($cid);
			if(isset($contacts['resultSet']) && !empty($contacts['resultSet']) && (count($contacts['resultSet']) > 0))
			{
				
				$return_data['status'] = TRUE;
				$return_data['data'] = $contacts['resultSet']['client_title'];			
				echo json_encode($return_data);
			}else
			{
				$return_data['status'] = FALSE;
				$return_data['data'] = '';
				echo json_encode($return_data);
			}
		}
		else
		{
			$return_data['status'] = FALSE;
			$return_data['data'] = '';
			echo json_encode($return_data);
		}
	}
	
	public function client_devices()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			$cid = $_POST['id'];
			$contacts = $this->macd->getClientDevices($cid);
			if(isset($contacts['resultSet']) && !empty($contacts['resultSet']) && (count($contacts['resultSet']) > 0))
			{
				$client_devices = array();
				foreach($contacts['resultSet'] as $data)
				{
					$client_device['device_id'] = $data['device_id'];
					$client_device['device_name'] = $data['device_name'];
					$client_devices[] = $client_device;
				}
				$return_data['status'] = TRUE;
				$return_data['data'] = $client_devices;
				echo json_encode($return_data);
			}else
			{
				$return_data['status'] = FALSE;
				$return_data['data'] = '';
				echo json_encode($return_data);
			}
		}
		else
		{
			$return_data['status'] = FALSE;
			$return_data['data'] = '';
			echo json_encode($return_data);
		}
	}
	
	public function client_data()
	{

		$data = array(
				"client_id" => isset($_POST['client_id'])?$_POST['client_id']:'', 
				"requested_by" =>  isset($_POST['requested_by'])?$_POST['requested_by']:'', 
				"requested_date" => isset($_POST['requested_date'])?(date('Y-m-d H:i:s', strtotime($_POST['requested_date']))):'',
				"requested_for" => isset($_POST['requested_for'])?$_POST['requested_for']:'', 
				"work_phone" => isset($_POST['work_phone'])?$_POST['work_phone']:'',
				"request_type" => isset($_POST['request_type'])?$_POST['request_type']:'', 
				"voice_mail" =>isset($_POST['voice_mail'])?$_POST['voice_mail']:'', 
				"comments" => isset($_POST['comments'])?$_POST['comments']:'',
				"firstname_from" => isset($_POST['firstname_from'])?$_POST['firstname_from']:'', // move, Add, change, Delete
				"lastname_from" => isset($_POST['lastname_from'])?$_POST['lastname_from']:'', // move, add, Change, Delete
				"phone" => isset($_POST['phone'])?$_POST['phone']:'', // move, add, change, phone
				"location_uuid_from" => isset($_POST['location_from'])?$_POST['location_from']:'', // move, add
				"cubicle_from" => isset($_POST['cubicle_from'])?$_POST['cubicle_from']:'', // move, add
				"location_uuid_to" => isset($_POST['location_to'])?$_POST['location_to']:'', // move, Delete
				"cubicle_to" => isset($_POST['cubicle_to'])?$_POST['cubicle_to']:'', // move, Delete
				"device_id" => isset($_POST['device_type'])?$_POST['device_type']:'', // move, add, Change, Delete
				"mac_address" => isset($_POST['mac_address'])?$_POST['mac_address']:'', // move, add, Change, Delete
				"dialing_capabilities" => isset($_POST['dialing_capabilities'])?$_POST['dialing_capabilities']:'', // move, add, change-dialing
				"required_by" =>  isset($_POST['required_by'])?(date('Y-m-d H:i:s', strtotime($_POST['required_by']))):'', // move, add, Delete												
				"change_request" => isset($_POST['change_request'])?$_POST['change_request']:'', // Change											
				"firstname_to" => isset($_POST['firstname_to'])?$_POST['firstname_to']:'', // Change											
				"lastname_to" => isset($_POST['lastname_to'])?$_POST['lastname_to']:'', // Change											
				"feature_like" => isset($_POST['feature_like'])?$_POST['feature_like']:'',//Change-feature, 											
				"device_type_to" => isset($_POST['device_type_to'])?$_POST['device_type_to']:'',//Change-device, 											
				"mac_address_to" => isset($_POST['mac_address_to'])?$_POST['mac_address_to']:'',//Change-device, 											
				"contract_type" => isset($_POST['contract_id'])?$_POST['contract_id']:'',				
				"dialing_capabilities_to" => isset($_POST['dialing_capabilities_to'])?$_POST['dialing_capabilities_to']:'',//Change-dialing,
				"created_on" => date('Y-m-d H:i:s'), 											
				"created_by" => $_POST['uid'], 											
												
					);
				//pr($data); exit; echo generate_uuid('loc_'); 
				$insertClientTask = $this->macd->insertClientTask($data);
				/* update insertId readable format */
					$mack_id = "MACD-".str_pad($insertClientTask, 15, '0', STR_PAD_LEFT);
					$update_macd_data = array('macd_request_id'=> $mack_id); 				
					$update_macd = $this->macd->updateMacd($update_macd_data,array("macd_id"=>$insertClientTask));				
				/* update insertId readable format end */
				/* Email Starts */
				$username = 'inspiredge';
				$password = 'inspiredge@123';
				$context = stream_context_create(array(
				'http' => array(
				'header' => "Authorization: Basic " . base64_encode("$username:$password")
				)
				));
				$url = base_url("webapi/createMacd/macd_id/".$insertClientTask."/primary_client_Id/2");
				$data = file_get_contents($url, false, $context);
				$array = json_decode($data, true);
				/* Email Ends */
				
				
				
				$device= "";
				if(isset($_POST['device_type']) && !empty($_POST['device_type']))
				{
					$deviceName = $this->macd->getDeviceName($_POST['device_type']);
					if(isset($deviceName['status']) && !empty($deviceName['resultSet']['device_name']))
					{
						$device = $deviceName['resultSet']['device_name'];
					}
					
				}
				$subject = "";
				$firstNameFrom = (isset($_POST['firstname_from']) && !empty($_POST['firstname_from']))?'FirstName(From): '.$_POST['firstname_from']:'';
				$LastNameFrom = (isset($_POST['lastname_from']) && !empty($_POST['lastname_from']))?'LastName(From): '.$_POST['lastname_from']:'';
				$phone = (isset($_POST['phone']) && !empty($_POST['phone']))?'Phone: '.$_POST['phone']:'';
				$device = (isset($device) && !empty($device))?'Device: '.$device:'';
				//$device = $device;
				$mac_address = (isset($_POST['mac_address']) && !empty($_POST['mac_address']))?'Mac Address: '.$_POST['mac_address']:'';
				$dialing_capabilities = (isset($_POST['dialing_capabilities']) && !empty($_POST['dialing_capabilities']))?'Dialing Capabilities: '.$_POST['dialing_capabilities']:'';
				$location_to = (isset($_POST['location_to']) && !empty($_POST['location_to']))?'Location To: '.$_POST['location_to']:'';
				$required_by = ($_POST['required_by'])?'Required by: '.$_POST['required_by']:'';
				$firstNameTo = (isset($_POST['firstname_to']) && !empty($_POST['firstname_to']))?'FirstName(To): '.$_POST['firstname_to']:'';
				$LastNameTo = (isset($_POST['lastname_to']) && !empty($_POST['lastname_to']))?'LastName(To): '.$_POST['lastname_to']:'';
				$subject .= $firstNameFrom.'<br/>'.$LastNameFrom.'<br/>'.$phone.'<br/>'.$device.'<br/>'.$mac_address.'<br/>'.$dialing_capabilities.'<br/>'.$location_to.'<br/>'.$required_by.'<br/>'.$firstNameTo.'<br/>'.$LastNameTo;
		
		$macd_queue = array(
			"macd_queue_uuid" =>  generate_uuid('MACD_'),
			"request_id" =>  $insertClientTask, // macd insertID
			"task_uuid" =>  '', // if task improved. later update
			"subject" =>  $subject,
			"requested_on" =>  date('Y-m-d H:i:s'),
			"reqested_by" =>  $_POST['uid'],
			"macd_status" =>  0, // pending
			"macd_text" =>  json_encode($data), 
			"created_by" =>  $_POST['uid'], 
			"created_on" =>  date('Y-m-d H:i:s'), 
		);
		$macdQueue = $this->macd->insertMacdQueue($macd_queue);
		$macd_active_log = array(
			"macd_queue_uuid" =>  $insertClientTask,
			"approver" =>  $_POST['approver'], 
			"received_on" =>  date('Y-m-d H:i:s'), 
			"macd_status" =>  0,
			"created_by" =>  $_POST['uid'],
			"created_on" =>  date('Y-m-d H:i:s'),			 
		);
		$macdActive = $this->macd->insertMacdActiveLog($macd_active_log);
		
		$approver = $_POST['approver'];
		if($approver == 0)
		{
			$ctlObj = modules::load('users/Auto_tickets/');
			$s = $ctlObj->macd_manual($insertClientTask,1);//1 = approved
			
		}
		if(isset($insertClientTask) && !empty($insertClientTask))
		{
			$return_data['status'] = TRUE;
			$return_data['data'] = $insertClientTask;
			echo json_encode($return_data);
		}else
		{
			$return_data['status'] = FALSE;
			$return_data['data'] = "";
			echo json_encode($return_data);
		}
	}
	
	
	public function user_login()
	{
		if(isset($_POST['user'],$_POST['pwd']) && !empty($_POST['user']) && !empty($_POST['pwd']))
		{
			$user = $_POST['user'];
			$pwd = $_POST['pwd'];
			$result = $this->macd->getLogin($user,$pwd);
			//pr($result); exit;
			if($result['status'] != false)
			{
				if($result['resultSet']['is_mac_access'] != 'E')
				{
					if($result['resultSet']['client_uuid'])
					{
						$contract = $this->macd->getContractsByClient($result['resultSet']['client_uuid']);
						if($contract['resultSet']['uuid'] && !empty($contract['resultSet']['uuid']))
						{
							$data = array(
								'uid' => $result['resultSet']['id'],
								'name' => $result['resultSet']['name'],
								'client' => $result['resultSet']['client'],
								'client_uuid' => $result['resultSet']['client_uuid'],
								'reporting_manager_id' => $result['resultSet']['reporting_manager_id'],
								'is_approval' => $result['resultSet']['is_approval'],
								'phone' => $result['resultSet']['phone'],
								'contract_id' => $contract['resultSet']['uuid'],
							);
						
							$return_data['status'] = TRUE;
							$return_data['data'] = $data;
							echo json_encode($return_data);
						}
						else
						{
								$return_data['status'] = FALSE;
								$return_data['error'] = "Your Contract is expired. Please contact your Manager";
								$return_data['data'] = '';
								echo json_encode($return_data);
						} 
					}
					
					
					
				}
				else
				{
					$return_data['status'] = FALSE;
					$return_data['error'] = "You Don't Have MACD Access Permission. Please Contact Your Manager";
					$return_data['data'] = '';
					echo json_encode($return_data);
				}
			}
			else
			{
				$return_data['status'] = FALSE;
				$return_data['error'] = 'Invalid Login Details';
				$return_data['data'] = '';
				echo json_encode($return_data);
			}
		}
		else
		{
			$return_data['status'] = FALSE;
			$return_data['error'] = 'Invalid Login Details';
			$return_data['data'] = '';
			echo json_encode($return_data);
		}
	}
	
	public function getMacdDetailsOfUser()
	{
		
		$requestData= $_REQUEST;
		
		$columns = array( 
			1 => 'macd_id', 
			2 => 'subject',
			3 => 'macd_status',
			4 => 'createdOn',
			5 => 'requested_by',
			6 => 'task_uuid',
			7 => 'task_status'
		);

		if(isset($_GET['id']) && !empty($_GET['id']))
		{
			$uid = $_GET['id'];
			$sts = "";
			if(isset($_GET['sts']))
			{
				$sts = $_GET['sts'];
			}		
			$results = $this->macd->getMacdDetails($uid,$requestData,$columns,$sts);
			$ajax_results = $this->macd->ajax_getMacdDetails($uid,$requestData,$sts);			
			$totalData = count($ajax_results);
			$totalFiltered = $totalData;
			$data = array();
			if((count($results) > 0) && isset($results) && !empty($results) )
			{					
				foreach($results as $result) {  // preparing an array
					$nestedData=array(); 
					if($result["check_status"] == 0)
					{
						$check = "";
					}else
					{
						$check = "disabled";
					}
					
					if(isset($_GET['mng']) && ($_GET['mng'] == 0) )
					{
						$nestedData[] = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline '.$check.'"><input type="checkbox" '.$check.' class="mail-checkbox checked " value="'.$result["request_id"].'"><span></span></label>';
					}
					
					$nestedData[] = "<a href='javascript:data_details_by_id(".$result["request_id"].")'>".$result["macd_id"]."</a>";
					$nestedData[] = "<span data-toggle='tooltip' title='".$result["subject"]."'>".substr($result["subject"],0,30)."</span>";
					$nestedData[] = $result["macd_status"];
					$nestedData[] = $result["createdOn"];
					$nestedData[] = $result["requested_by"];
					$nestedData[] = $result["task_uuid"];
					$nestedData[] = $result["task_status"];
					
					$data[] = $nestedData;
				}			

				$json_data = array(
							"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
							"recordsTotal"    => intval( $totalData ),  // total number of records
							"recordsFiltered" => intval($totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
							"data"            => $data   // total data array
							);

				echo json_encode($json_data);  // send data as json format
				
			}
			else
				{
					$json_data = array(
							"draw"            => intval( $requestData['draw'] ), 
							"recordsTotal"    => intval( $totalData ), 
							"recordsFiltered" => intval( 0 ), 
							"data"            => $data  
							);
					echo json_encode($json_data);
				}
			
			
		}
		else
		{
			$return_data['status'] = FALSE;
			$return_data['error'] = 'Something went wrong. Please Login after sometime';
			$return_data['data'] = '';
			echo json_encode($return_data);
		}
	}
	
	
	public function bringCounts()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			$uid = $_POST['id'];
			$requestData = array();
			$all = $this->macd->ajax_getMacdDetails($uid,$requestData);
			$pending = $this->macd->ajax_getMacdDetails($uid,$requestData,0);
			$approved = $this->macd->ajax_getMacdDetails($uid,$requestData,1);
			$rejected = $this->macd->ajax_getMacdDetails($uid,$requestData,2);
			$trash = $this->macd->ajax_getMacdDetails($uid,$requestData,3);
			
			$return_data['status'] = TRUE;
			$return_data['all'] = (isset($all) && !empty($all))?count($all):0; 
			$return_data['pending'] = (isset($pending) && !empty($pending))?count($pending):0; 
			$return_data['approved'] = (isset($approved) && !empty($approved))?count($approved):0; 
			$return_data['rejected'] = (isset($rejected) && !empty($rejected))?count($rejected):0; 
			$return_data['trash'] = (isset($trash) && !empty($trash))?count($trash):0; 
			echo json_encode($return_data);
		}	
		
	}
	
	
	public function updateIndts()
	{
		if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['macd_id']) && !empty($_POST['macd_id']))
		{
			$id = $_POST['id'];
			$macd_id = $_POST['macd_id'];
			$ctlObj = modules::load('users/Auto_tickets/');
			if($id == 1)
			{
				foreach($macd_id as $data)
				{
					
					$s = $ctlObj->macd_manual($data,$id);
				/* Email Starts */
					$username = 'inspiredge';
					$password = 'inspiredge@123';
					$context = stream_context_create(array(
					'http' => array(
					'header' => "Authorization: Basic " . base64_encode("$username:$password")
					)
					));
					$url = base_url("webapi/macdStatusChange/macd_id/".$data."/primary_client_Id/2");
					$data = file_get_contents($url, false, $context);
					$array = json_decode($data, true);
				/* Email Ends */
				}
				
				echo 1;
			}
			else
			{
				echo 0;
			}
			
			
		}
		
	}
	
	public function updateIndtsSingle()
	{
		if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['macd_id']) && !empty($_POST['macd_id']))
		{
			$status = $_POST['id'];
			$macd_id = $_POST['macd_id'];
			$reporting_manager_id = $_POST['reporting_manager_id'];
			$notes = $_POST['notes'];
			$ctlObj = modules::load('users/Auto_tickets/');
			if($status == 1)
			{
				$s = $ctlObj->macd_manual($macd_id,$status);			
			}
			
		$macd_active_log = array(
			"macd_queue_uuid" =>  $macd_id,
			"approver" =>  $reporting_manager_id, 
			"acted_on" =>  date('Y-m-d H:i:s'), 
			"notes" =>  $notes, 
			"macd_status" =>  $status,
			"created_by" =>  $reporting_manager_id,
			"created_on" =>  date('Y-m-d H:i:s'),			 
		);
		$macdActive = $this->macd->insertMacdActiveLog($macd_active_log);
		
		/* Email Starts */
				$username = 'inspiredge';
				$password = 'inspiredge@123';
				$context = stream_context_create(array(
				'http' => array(
				'header' => "Authorization: Basic " . base64_encode("$username:$password")
				)
				));
				$url = base_url("webapi/macdStatusChange/macd_id/".$macd_id."/primary_client_Id/2");
				$data = file_get_contents($url, false, $context);
				$array = json_decode($data, true);
		/* Email Ends */
		}
		
	}
	
	public function updateMacd()
	{
		if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['macd_id']) && !empty($_POST['macd_id']))
		{
			$status = $_POST['id'];
			$macd_id = $_POST['macd_id'];
			
				foreach($macd_id as $data)
				{
					$update = array("macd_status"=>$status);
					$s = $this->macd->ajax_updateMacd($update,array("request_id"=>$data));
		/* Email Starts */
				$username = 'inspiredge';
				$password = 'inspiredge@123';
				$context = stream_context_create(array(
				'http' => array(
				'header' => "Authorization: Basic " . base64_encode("$username:$password")
				)
				));
				$url = base_url("webapi/macdStatusChange/macd_id/".$data."/primary_client_Id/2");
				$data = file_get_contents($url, false, $context);
				$array = json_decode($data, true);
		/* Email Ends */
				}		
		}
		
	}
	
	public function updateMacdSingle()
	{
		if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['macd_id']) && !empty($_POST['macd_id']))
		{
			$status = $_POST['id'];
			$macd_id = $_POST['macd_id'];
			$reporting_manager_id = $_POST['reporting_manager_id'];
			$notes = $_POST['notes'];
			$update = array("macd_status"=>$status);
			$s = $this->macd->ajax_updateMacd($update,array("request_id"=>$macd_id));
			$macd_active_log = array(
			"macd_queue_uuid" =>  $macd_id,
			"approver" =>  $reporting_manager_id, 
			"acted_on" =>  date('Y-m-d H:i:s'), 
			"notes" =>  $notes, 
			"macd_status" =>  $status,
			"created_by" =>  $reporting_manager_id,
			"created_on" =>  date('Y-m-d H:i:s'),			 
		);
		$macdActive = $this->macd->insertMacdActiveLog($macd_active_log);
		
			/* Email Starts */
				$username = 'inspiredge';
				$password = 'inspiredge@123';
				$context = stream_context_create(array(
				'http' => array(
				'header' => "Authorization: Basic " . base64_encode("$username:$password")
				)
				));
				$url = base_url("webapi/macdStatusChange/macd_id/".$macd_id."/primary_client_Id/2");
				$data = file_get_contents($url, false, $context);
				$array = json_decode($data, true);
			/* Email Ends */
			
		}
		
	}
	
	public function getMacdDetailsById()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			$macd_id = $_POST['id'];
			$data = $this->macd->ajax_getMacdDetailById($macd_id);
			$return_data['status'] = TRUE;
			$return_data['data'] = $data;
			echo json_encode($return_data);			
		}
		else
		{
			$return_data['status'] = FALSE;
			$return_data['error'] = 'Something went wrong. Please Login after sometime';
			$return_data['data'] = '';
			echo json_encode($return_data);
		}
		
	}
	
	
	public function macdActivity()
	{
		pr($_POST); exit;
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			$cid = base64_decode($_POST['id']);
			$contacts = $this->macd->getLocations($cid);
			if(isset($contacts['resultSet']) && !empty($contacts['resultSet']) && (count($contacts['resultSet']) > 0))
			{
				$contact_persons = array();
				foreach($contacts['resultSet'] as $data)
				{
					$contact_person['location_id'] = $data['location_id'];
					$contact_person['location_uuid'] = $data['location_uuid'];
					$contact_person['location_name'] = $data['location_name'];
					$contact_person['contact_name'] = $data['contact_name'];
					$contact_persons[] = $contact_person;
				}
				$return_data['status'] = TRUE;
				$return_data['data'] = $contact_persons;
				echo json_encode($return_data);
			}else
			{
				$return_data['status'] = FALSE;
				$return_data['data'] = '';
				echo json_encode($return_data);
			}
		}
		else
		{
			$return_data['status'] = FALSE;
			$return_data['data'] = '';
			echo json_encode($return_data);
		}
	}

}
