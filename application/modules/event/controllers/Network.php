<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Network extends MX_Controller {

   

    public function __construct() {
        parent::__construct();
        $this->userId = $this->session->userdata('id');       
        $current_date = $this->config->item('datetime');		
        $this->load->model('Model_network', 'network');
    }
	
	public function index($id = NULL) {
	
	  $trees = array();
	  $gateways = $this->network->getGateways();
	  if(isset($gateways) && !empty($gateways) && (count($gateways)>0) && ($id != NULL))
	  {
		  foreach($gateways as $gate)
		  {
			  if(decode_url($id) == $gate['id'])
			  {
				  $tree['id'] = $gate['id'];
				  $tree['name'] = $gate['title'];
				  $tree['data'] = '{}';
				  $tree['children'] = $this->getDevices($gate['id']);
				  $trees[] = $tree;	
			  }
		  }		  
			
		$data['tree'] = trim(json_encode($trees),'[]');
		$data['file'] = 'network/view_network';
       $this->load->view('template/front_template', $data);
		
	  }
	  else
	  {
		  echo "No data available";
	  }
	   
		
    }
	
	
	function getChild($id) {
	
	  $trees = array();
	  $childDevices = $this->network->getChildDevices($id);
	  if(isset($childDevices) && !empty($childDevices) && (count($childDevices)>0))
	  {
		  foreach($childDevices as $device)
		  {
			  $tree['id'] = $device['device_id'];
			  $tree['name'] = $device['device_name'];
			  $tree['data'] = '{}';
			  $tree['children'] = $this->getChild($device['device_id']);
			  $trees[] = $tree;			  
		  }
		 return $trees;
	  }
	  return array();
	   
		
    }	
	
	
	function getDevices($id) {
	
	  $trees = array();
	  $parentDevices = $this->network->getParentDevices($id);
	  if(isset($parentDevices) && !empty($parentDevices) && (count($parentDevices)>0))
	  {
		  foreach($parentDevices as $device)
		  {
			$tree['id'] = $device['device_id'];
			$tree['name'] = $device['device_name'];
			$tree['data'] = '{}';
			$tree['children'] = $this->getChild($device['device_id']);
			$trees[] = $tree;  
			  
		  }
		 return $trees;
	  }
	  return array();		
    }
	
	
	public function readMail() {

		$dns = "{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX";
		$email = "nectaralerts@gmail.com";
		$password = "India@321";

		$openmail = imap_open($dns,$email,$password ) or die("Cannot Connect ".imap_last_error());
		if ($openmail) {

			echo  "You have ".imap_num_msg($openmail). " messages in your inbox";

			for($i=1; $i <= 100; $i++) {

				$header = imap_header($openmail,$i);
				echo "";
				echo $header->Subject." (".$header->Date.")";
			}

			$msg = imap_fetchbody($openmail,1,"","FT_PEEK");

			/*
			$msgBody = imap_fetchbody ($openmail, $i, "2.1");
			if ($msgBody == "") {
			   $portNo = "2.1";
			   $msgBody = imap_fetchbody ($openmail, $i, $portNo);
			}

			$msgBody = trim(substr(quoted_printable_decode($msgBody), 0, 200));

			*/
			echo $msg;
			imap_close($openmail);
			
		} else {

			echo "Failed reading messages!!";

		}

	}

	
	

}
