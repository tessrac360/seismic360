<?php

class Model_queue extends CI_Model {

   // private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'severities';
        $this->queue = 'ticket_queue';
        $this->company_id = $this->session->userdata('company_id');
        $this->client_id = $this->session->userdata('client_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function getSeverity(){
		$this->db->select('*');
        $this->db->from($this->tablename);
		$query = $this->db->get();
       // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
	}
	
	public function insertQueue($postData = array()) {
        $this->db->insert($this->queue, $postData);
        $insert_id = $this->db->insert_id();
		//echo $this->db->last_query();exit;
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getQueueDetails($search = '', $limit = 0, $start = 0) {
        $this->db->select('q.*,CONCAT(u.first_name, " ", u.last_name) AS username');
        $this->db->from('ticket_queue as q');
		$this->db->join('eventedge_users as u', 'u.id = q.created_by');
		//$this->db->where('status', 'Y');
		if ($search != "") {
            $this->db->like('queue_name', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
		$query = $this->db->get();
       //echo $this->db->last_query();exit;
        if ($limit == 0 && $start == 0) {
            return $query->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $query->result_array();
    }
	

	public function updateQueue($postData = array(), $id) {
		$this->db->where('queue_id', $id);
        $update_status = $this->db->update($this->queue, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getQueueEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->queue);
        $this->db->where('queue_id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

	public function isDeleteQueue($postId) {
        if ($postId != "") {
            $this->db->where('queue_id', $postId);
            $this->db->delete($this->queue);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    
}

?>