<?php

class Model_macd extends CI_Model {

    private $tablename, $address, $client, $devices, $macd;
	
       public function __construct() {
        parent::__construct();
        $this->tablename = 'client_locations';
        $this->address = 'address';
        $this->client = 'client';
        $this->devices = 'devices';
        $this->users = 'users';
        $this->macd = 'macd';
        $this->macd_queue = 'macd_queue';
        $this->macd_active_log = 'macd_activity_log';
        $current_date = $this->config->item('datetime');
        
    }
	
	
	public function getLocations($cid) {
        $this->db->select('*');
        $this->db->from($this->tablename);
		$this->db->where('referrence_uuid', $cid);
		$this->db->where('is_disabled', 0);
		$this->db->where('is_deleted', 0);
        $query = $this->db->get();
		//echo $this->db->last_query(); exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = false;
        }
        return $return;
    }
	
	public function getAddress($cid) {
        $this->db->select('*');
        $this->db->from($this->address);
		$this->db->where('referrence_uuid', $cid);
        $query = $this->db->get();     
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = false;
        }
        return $return;
    }
	
	public function getClient($cid) {
        $this->db->select('*');
        $this->db->from($this->client);
		$this->db->where('client_uuid', $cid);
        $query = $this->db->get(); 
		//echo $this->db->last_query(); exit;		
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
        }
        return $return;
    }
	
	public function getClientDevices($cid) {
        $this->db->select('device_id,device_name');
        $this->db->from($this->devices);
		$this->db->where('client_uuid', $cid);
        $query = $this->db->get();     
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = false;
        }
        return $return;
    }
	
	public function getDeviceName($did) {
        $this->db->select('device_id,device_name');
        $this->db->from($this->devices);
		$this->db->where('device_id', $did);
        $query = $this->db->get();     
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
        }
        return $return;
    }
	
	
	public function insertClientTask($data)
	{
		$this->db->insert($this->macd,$data);
		return $this->db->insert_id();
	}
	
	public function insertMacdQueue($data)
	{
		$this->db->insert($this->macd_queue,$data);
		return $this->db->insert_id();
	}
	
	public function insertMacdActiveLog($data)
	{
		$this->db->insert($this->macd_active_log,$data);
		return $this->db->insert_id();
	}
	
	public function updateMacd($data,$con)
	{
		return $this->db->update($this->macd,$data,$con);
	}
	
	public function ajax_updateMacd($data,$con)
	{
		return $this->db->update($this->macd_queue,$data,$con);
	}
	
	public function getLogin($user,$pwd) {
        $this->db->select('id, concat_ws("  ",first_name ,  ,last_name) as name, client, client_uuid, is_mac_access, reporting_manager_id, is_approval, phone');
        $this->db->from($this->users);
		$this->db->where('email_id',$user);
		$this->db->where('password',md5($pwd));
		$this->db->where('is_mac_access','B');//both
		$this->db->or_where('is_mac_access','M');// MACD		
        $query = $this->db->get(); 
		//echo $this->db->last_query(); exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
        }
        return $return;
    }
	
	public function getMacdDetails($uid,$requestData,$columns,$sts="")
	{
		
		if($sts != "")
		{
			$cond = " m.macd_status = {$sts} AND";
		}
		else if($sts === 0)
		{
			$cond = " m.macd_status = {$sts} AND";
		}
		else
		{
			$cond = "";
		}
		$sql = "";	
		if( !empty($requestData['search']['value']) ) {
			$s = $requestData['search']['value'];
			$sql.="WHERE subject LIKE '%{$s}%' OR id LIKE '%{$s}%' OR task_uuid LIKE '%{$s}%' OR task_status LIKE '%{$s}%' OR createdOn LIKE '%{$s}%' OR requested_by LIKE '%{$s}%' OR macd_status LIKE '%{$s}%' OR macd_id LIKE '%{$s}%' "; 
						
		}
		if( !empty($columns[$requestData['order'][0]['column']]) ) {
		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		}

		$sql .=	"  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$query = $this->db->query("SELECT * FROM (SELECT m.subject,m.macd_queue_uuid as id,m.macd_status as check_status,m.request_id,
			IF(task_uuid IS NULL or task_uuid = '', ' - ',  (SELECT incident_id FROM eventedge_tickets WHERE ticket_id = m.task_uuid)) as task_uuid ,
			IF(status_code = NULL, ' - ', (SELECT s.status FROM `eventedge_ticket_status` s where  s.status_code  = (SELECT status_code FROM eventedge_tickets WHERE ticket_id = m.task_uuid)))  as task_status,
			DATE_FORMAT(m.created_on, '%m-%d-%Y   %H:%i') as createdOn,
			(SELECT concat_ws('  ',u.first_name,  u.last_name)  FROM eventedge_users u WHERE u.id = m.created_by) as requested_by,

			(CASE
				WHEN m.macd_status = 0 THEN 'Pending'
				WHEN m.macd_status = 1 THEN 'Approved'
				WHEN m.macd_status = 2 THEN 'Rejected'
				WHEN m.macd_status = 3 THEN 'Cancel'  
				 ELSE ' - '
			END) as macd_status,
			(SELECT macd_request_id  FROM eventedge_macd  WHERE macd_id = m.request_id) as macd_id
			FROM  eventedge_macd_queue m where {$cond} (m.created_by = {$uid} or m.created_by in (SELECT  id   FROM `eventedge_users`  where reporting_manager_id = {$uid}))) as db {$sql}");
			//echo $this->db->last_query(); exit;
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function ajax_getMacdDetails($uid,$requestData,$cond="")
	{
		if($cond != "")
		{
			$sql = " m.macd_status = {$cond} AND";
		}
		else if($cond === 0)
		{
			$sql = " m.macd_status = {$cond} AND";
		}
		else
		{
			$sql = "";
		}
		$search = "";
		if( !empty($requestData['search']['value']) ) {
			$s = $requestData['search']['value'];
			$search .= "WHERE subject LIKE '%{$s}%' OR id LIKE '%{$s}%' OR task_uuid LIKE '%{$s}%' OR task_status LIKE '%{$s}%' OR createdOn LIKE '%{$s}%' OR requested_by LIKE '%{$s}%' OR macd_status LIKE '%{$s}%' OR macd_id LIKE '%{$s}%' ";				
		}		
		
		$query = $this->db->query("SELECT * FROM (SELECT m.subject,m.macd_queue_uuid as id,
			IF(task_uuid IS NULL or task_uuid = '', ' - ', task_uuid) as task_uuid ,
			IF(status_code IS NULL or status_code = '', ' - ', (SELECT s.status FROM `eventedge_ticket_status` s where  s.status_code  = m.status_code))  as task_status,
			DATE_FORMAT(m.created_on, '%m-%d-%Y   %H:%i') as createdOn,
			(SELECT concat_ws('  ',u.first_name,  u.last_name)  FROM eventedge_users u WHERE u.id = m.created_by) as requested_by,

			(CASE
				WHEN m.macd_status = 0 THEN 'Pending'
				WHEN m.macd_status = 1 THEN 'Approved'
				WHEN m.macd_status = 2 THEN 'Rejected'
				WHEN m.macd_status = 3 THEN 'Deleted'  
				 ELSE ' - '
			END) as macd_status,
			(SELECT macd_request_id  FROM eventedge_macd  WHERE macd_id = m.request_id) as macd_id 
			FROM  eventedge_macd_queue m where {$sql} (m.created_by = {$uid} or m.created_by in(SELECT  id   FROM `eventedge_users`  where reporting_manager_id = {$uid})) ) as db {$search}");
			//echo $this->db->last_query(); exit;
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function ajax_getMacdDetailById($macdId)
	{
		
		$query = $this->db->query("SELECT m.*,
(CASE
	WHEN m.dialing_capabilities = 1 THEN 'Internal999 only'
	WHEN m.dialing_capabilities = 2 THEN 'Option 1 plus LocalToll Free'
	WHEN m.dialing_capabilities = 3 THEN 'Option 2 plus LocalToll'
	WHEN m.dialing_capabilities = 4 THEN 'Option 3 plus Long Distance'  
	WHEN m.dialing_capabilities = 5 THEN 'Option 4 plus International'  
	 ELSE ' - '
END) as dialing_capabilities_name,
(CASE
	WHEN m.dialing_capabilities_to = 1 THEN 'Internal999 only'
	WHEN m.dialing_capabilities_to = 2 THEN 'Option 1 plus LocalToll Free'
	WHEN m.dialing_capabilities_to = 3 THEN 'Option 2 plus LocalToll'
	WHEN m.dialing_capabilities_to = 4 THEN 'Option 3 plus Long Distance'  
	WHEN m.dialing_capabilities_to = 5 THEN 'Option 4 plus International'  
	 ELSE ' - '
END) as dialing_capabilities_name_to,
m.requested_date as requested_date_name,
m.required_by as required_by_date_formet,
 (SELECT  client_title  from eventedge_client WHERE id = m.client_id) as client,
 (SELECT  location_name  from eventedge_client_locations WHERE location_uuid = m.location_uuid_from) as location_from,
  (SELECT  location_name  from eventedge_client_locations WHERE location_uuid = m.location_uuid_to) as location_to,
 (SELECT  reqested_by  from eventedge_macd_queue WHERE m.macd_id = request_id) as user_id,
 (SELECT  macd_status  from eventedge_macd_queue WHERE m.macd_id = request_id) as macd_status,
  (SELECT  device_name  from eventedge_devices WHERE m.device_id = device_id) as device,
  (SELECT  device_name  from eventedge_devices WHERE m.device_type_to = device_id) as device_to,
 (SELECT reporting_manager_id FROM eventedge_users WHERE id = (SELECT reqested_by FROM eventedge_macd_queue WHERE request_id = m.macd_id) ) as is_approved,
 (SELECT concat_ws(' ',u.first_name, u.last_name) FROM eventedge_users u WHERE u.id = (SELECT  reqested_by  from eventedge_macd_queue WHERE m.macd_id = request_id)) as requested_user,
 (SELECT concat_ws(' ',u.first_name, u.last_name) FROM eventedge_users u WHERE u.id = (SELECT reporting_manager_id FROM eventedge_users WHERE id = (SELECT reqested_by FROM eventedge_macd_queue WHERE request_id = m.macd_id) )) as request_approver
 FROM eventedge_macd m where m.macd_id = {$macdId}");
			//echo $this->db->last_query(); exit;
		if($query->num_rows() > 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function getContractsByClient($id){
		
       $query = $this->db->query("SELECT c.uuid, (SELECT contract_type FROM eventedge_contract_types WHERE uuid = c.contract_type_uuid) as contract_type, (SELECT is_default FROM eventedge_contract_types WHERE uuid = c.contract_type_uuid) as is_default  FROM `eventedge_contracts` c WHERE reference_uuid = '{$id}' AND validity_status = 'Active'");	
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	

}

?>