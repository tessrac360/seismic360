<?php

class Model_network extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $devices, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'temp_role';
        $this->devices = 'devices';
        $this->client = 'client';
        $this->users = 'users';
        $this->events = 'events';
        $this->tickets = 'tickets';
        $this->ticket_status = 'ticket_status';
        $this->ticket_queue = 'ticket_queue';
        $this->ticket_activity = 'ticket_activity';
        $this->company_id = $this->session->userdata('company_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $this->client_id = $this->session->userdata('client_id');
		$this->accessLevelClient = helper_getAccessLevelClient();
        $this->cData = array('created_on' => $current_date, 'created_by' => $this->userId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $this->userId);
    }


	public function getGateways(){
		$query = $this->db->query("SELECT id, title FROM `eventedge_client_gateways` WHERE status = 'Y'");
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	
	public function getParentDevices($id){
		$query = $this->db->query("SELECT device_id, device_name FROM eventedge_devices WHERE device_parent_id = 0 and gateway_id = {$id}");
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function getChildDevices($id){
		$query = $this->db->query("SELECT device_id, device_name FROM `eventedge_devices` WHERE `device_parent_id` = {$id}");
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}

	
}

?>