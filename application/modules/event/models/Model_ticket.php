<?php
class Model_ticket extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'temp_role';
        $this->client = 'client';
        $this->users = 'users';
        $this->events = 'events';
        $this->tickets = 'tickets';
        $this->ticket_status = 'ticket_status';
        $this->ticket_queue = 'ticket_queue';
        $this->ticket_activity = 'ticket_activity';
        $this->ticket_activity_files = 'incident_activity_log_files';
        $this->activity_type = 'activity_type';
        $this->incident_activity_log = 'incident_activity_log';      
        $this->ssh_log = 'ssh_log';
        $this->company_id = $this->session->userdata('company_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $this->client_id = $this->session->userdata('client_id');

        $this->accessLevelClient = helper_getAccessLevelClient();
        $this->cData = array('created_on' => $current_date, 'created_by' => $this->userId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $this->userId);
    }

    public function getTicketColumn($status_id = "", $client_id = "", $ticketIds = array(), $notDashboard = "", $ajax_multiple_ids = array(), $user_id = "") {
        $accessLevelClient = helper_getAccessLevelClient();
        $this->db->select('t.ticket_id,t.incident_id,e.severity_id,SV.severity,t.created_on,cl.client_title as client_name,t.ticket_type_id,t.subject,t.event_id,ts.status,tq.queue_name,cl.name,t.requestor,sk.title as priority,usr.first_name as created_by,t.created_on,t.due_date,t.estimated_time,tti.name as ticket_type,ttc.name as category, tsc.name as subcategory, t.last_updated_on,t.status_code,us.first_name as assigned_to');
        $this->db->from('tickets t');
        $this->db->join('client as cl', 'cl.id = t.client_id', 'left');
        $this->db->join('ticket_status as ts', 'ts.status_code = t.status_code', 'left');
        $this->db->join('ticket_queue as tq', 'tq.queue_id = t.queue_id', 'left');
        $this->db->join('skill_set as sk', 'sk.skill_id = t.priority', 'left');
        $this->db->join('eventedge_ticket_types as tti', 'tti.ticket_type_id = t.ticket_type_id', 'left');
        $this->db->join('eventedge_tickets_category as ttc', 'ttc.ticket_cate_id = t.ticket_cate_id', 'left');
        $this->db->join('eventedge_ticket_sub_category as tsc', 'tsc.ticket_sub_cate_id = t.ticket_sub_cate_id', 'left');
        $this->db->join('users as usr', 'usr.id = t.created_by', 'left');
        $this->db->join('users as us', 'us.id = t.assigned_to', 'left');
        $this->db->join('events as e', 'e.event_id = t.event_id', 'left');
        $this->db->join('severities as SV', 'SV.id = e.severity_id', 'left');
        $this->db->order_by("t.ticket_id", "desc");
        if (isset($status_id) && !empty($status_id) && $status_id != NULL) {
            $this->db->where('t.status_code', $status_id);
        }

        if (is_array($ajax_multiple_ids) && isset($ajax_multiple_ids) && !empty($ajax_multiple_ids)) {
            if (isset($ajax_multiple_ids['status_code']) && $ajax_multiple_ids['status_code'] != NULL) {
                $this->db->where('t.status_code', $ajax_multiple_ids['status_code']);
            }
            if (isset($ajax_multiple_ids['ticket_cate_id']) && !empty($ajax_multiple_ids['ticket_cate_id'])) {
                $this->db->where('t.ticket_cate_id', $ajax_multiple_ids['ticket_cate_id']);
            }
            if (isset($ajax_multiple_ids['ticket_sub_cate_id']) && !empty($ajax_multiple_ids['ticket_sub_cate_id'])) {
                $this->db->where('t.ticket_sub_cate_id', $ajax_multiple_ids['ticket_sub_cate_id']);
            }
            if (isset($ajax_multiple_ids['ticket_type_id']) && !empty($ajax_multiple_ids['ticket_type_id'])) {
                $this->db->where('t.ticket_type_id', $ajax_multiple_ids['ticket_type_id']);
            }
            if (isset($ajax_multiple_ids['queue_id']) && !empty($ajax_multiple_ids['queue_id'])) {
                $this->db->where('t.queue_id', $ajax_multiple_ids['queue_id']);
            }
            if (isset($ajax_multiple_ids['user_id']) && !empty($ajax_multiple_ids['user_id'])) {
                $this->db->where('t.assigned_to', $ajax_multiple_ids['user_id']);
            }
            /* if(isset($ajax_multiple_ids['client_id']) && !empty($ajax_multiple_ids['client_id'])){
              $this->db->where('t.requestor', $ajax_multiple_ids['client_id']);
              } */
            if (isset($ajax_multiple_ids['severity_id']) && !empty($ajax_multiple_ids['severity_id'])) {
                $this->db->where('e.severity_id', $ajax_multiple_ids['severity_id']);
            }
            if (isset($ajax_multiple_ids['priority_id']) && !empty($ajax_multiple_ids['priority_id'])) {
                $this->db->where('t.priority', $ajax_multiple_ids['priority_id']);
            }
            if (isset($ajax_multiple_ids['date']) && !empty($ajax_multiple_ids['date']) && $ajax_multiple_ids['date'] == 1) {
                $this->db->where('DATE(t.created_on)', date('Y-m-d'));
            }
            if (isset($ajax_multiple_ids['date']) && !empty($ajax_multiple_ids['date']) && $ajax_multiple_ids['date'] == 2) {
                $this->db->where('DATE(t.created_on)', date('Y-m-d', strtotime("-1 days")));
            }

            if (isset($ajax_multiple_ids['date']) && !empty($ajax_multiple_ids['date']) && $ajax_multiple_ids['date'] == 3) {
                $this->db->where('yearweek(DATE(t.created_on))', date('YW'));
            }

            if (isset($ajax_multiple_ids['date']) && !empty($ajax_multiple_ids['date']) && $ajax_multiple_ids['date'] == 4) {
                $this->db->where('yearweek(DATE(t.created_on))', date('YW', strtotime("-1 weeks")));
            }

            if (isset($ajax_multiple_ids['date']) && !empty($ajax_multiple_ids['date']) && $ajax_multiple_ids['date'] == 5) {
                $this->db->where(array('year(DATE(t.created_on))' => date('Y'), 'month(DATE(t.created_on))' => date('m')));
            }

            if (isset($ajax_multiple_ids['date']) && !empty($ajax_multiple_ids['date']) && $ajax_multiple_ids['date'] == 6) {
                $this->db->where(array('year(DATE(t.created_on))' => date('Y', strtotime("-1 months")), 'month(DATE(t.created_on))' => date('m', strtotime("-1 months"))));
            }
        }
        if (isset($notDashboard) && !empty($notDashboard)) {
            if ($client_id != "") {
                $this->db->where('t.client_id', $client_id);
                $this->db->where('t.status_code', 7);
            }
        } else if (isset($client_id) && !empty($client_id)) {
            //$this->db->where_in('t.client_id', $accessLevelClient['resultSet']);
            $this->db->where('t.client_id', $client_id);
        } else {
            $this->db->where_in('t.client_id', $accessLevelClient['resultSet']);
        }
        if (count($ticketIds) > 0) {
            $this->db->where_in('t.ticket_id', $ticketIds);
        }
        if (!empty($user_id)) {
            $this->db->where_in('t.assigned_to', $user_id);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function getIncidentSearch($incidentSearch = "", $user_id = "", $client_id = "") {
        $accessLevelClient = helper_getAccessLevelClient();
        $this->db->select('t.ticket_id,t.incident_id,e.severity_id,SV.severity,t.created_on,cl.client_title as client_name,t.subject,t.event_id,ts.status,tq.queue_name,cl.name,t.requestor,sk.title as priority,usr.first_name as created_by,t.created_on,t.due_date,t.estimated_time,tti.name as ticket_type,ttc.name as category, tsc.name as subcategory, t.last_updated_on,t.status_code,us.first_name as assigned_to');
        $this->db->from('tickets t');
        $this->db->join('client as cl', 'cl.id = t.client_id');
        $this->db->join('ticket_status as ts', 'ts.status_code = t.status_code');
        $this->db->join('ticket_queue as tq', 'tq.queue_id = t.queue_id');
        $this->db->join('skill_set as sk', 'sk.skill_id = t.priority');
        $this->db->join('eventedge_ticket_types as tti', 'tti.ticket_type_id = t.ticket_type_id');
        $this->db->join('eventedge_tickets_category as ttc', 'ttc.ticket_cate_id = t.ticket_cate_id');
        $this->db->join('eventedge_ticket_sub_category as tsc', 'tsc.ticket_sub_cate_id = t.ticket_sub_cate_id');
        $this->db->join('users as usr', 'usr.id = t.created_by');
        $this->db->join('users as us', 'us.id = t.assigned_to', 'left');
        $this->db->join('events as e', 'e.event_id = t.event_id');
        $this->db->join('severities as SV', 'SV.id = e.severity_id');
        $this->db->order_by("t.ticket_id", "desc");

        if (isset($incidentSearch)) {
            $this->db->group_start();
            $this->db->like('t.incident_id', $incidentSearch);
            $this->db->or_like('SV.severity', $incidentSearch);
            $this->db->or_like('cl.client_title', $incidentSearch);
            $this->db->or_like('t.subject', $incidentSearch);
            $this->db->or_like('ts.status', $incidentSearch);
            $this->db->or_like('tq.queue_name', $incidentSearch);
            $this->db->or_like('cl.name', $incidentSearch);
            $this->db->or_like('t.requestor', $incidentSearch);
            $this->db->or_like('sk.title', $incidentSearch);
            $this->db->or_like('t.requestor', $incidentSearch);
            $this->db->or_like('usr.first_name', $incidentSearch);
            $this->db->or_like('tti.name', $incidentSearch);
            $this->db->or_like('ttc.name', $incidentSearch);
            $this->db->or_like('tsc.name', $incidentSearch);
            $this->db->or_like('us.first_name', $incidentSearch);
            $this->db->group_end();
        }
        if (!empty($user_id)) {
            $this->db->where_in('t.assigned_to', $user_id);
        }

        if (isset($client_id) && !empty($client_id)) {
            //$this->db->where_in('t.client_id', $accessLevelClient['resultSet']);
            $this->db->where('t.client_id', $client_id);
        } else {
            $this->db->where_in('t.client_id', $accessLevelClient['resultSet']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function getQueueFilter($client_id = "", $urgency_id = "", $skill_id = "", $ticket_status_id = "") {
        $accessLevelClient = helper_getAccessLevelClient();
        $client_array = $accessLevelClient['resultSet'];
        $client_string = implode(",", $client_array);
        $client_ids = "'" . str_replace(",", "','", $client_string) . "'";
        //pr($client_ids);exit;
        $query = $this->db->query("SELECT `t`.`incident_id`, `t`.`created_on`, `t`.`requestor`, `t`.`subject`, `t`.`last_updated_on`, `cl`.`client_title` as `client`, CONCAT_WS(' ', `uc`.`first_name`, uc.last_name) as user, CONCAT_WS(' ', `ua`.`first_name`, ua.last_name) as assigned_to, `ts`.`status` as `state`, `p`.`name` as `priority`, `s`.`title` as `severity`, `tt`.`name` as `ticket_type`, `dc`.`category` as `category`, `sr`.`service_description` as `sub_category`
			FROM `eventedge_tickets` `t`
			JOIN `eventedge_client` as `cl` ON `cl`.`id` = `t`.`client_id`
			JOIN `eventedge_users` as `uc` ON `uc`.`id` = `t`.`created_by`
			LEFT JOIN `eventedge_users` as `ua` ON `ua`.`id` = `t`.`assigned_to`
			JOIN `eventedge_ticket_status` as `ts` ON `ts`.`status_code` = `t`.`status_code`
			JOIN `eventedge_ticket_urgency` as `p` ON `p`.`urgency_id` = `t`.`priority`
			JOIN `eventedge_events` as `e` ON `e`.`event_id` = `t`.`event_id`
			JOIN `eventedge_devices` as `d` ON `d`.`device_id` = `e`.`device_id`
			JOIN `eventedge_skill_set` as `s` ON `s`.`skill_id` = `d`.`priority_id`
			JOIN `eventedge_ticket_types` as `tt` ON `tt`.`ticket_type_id` = `t`.`ticket_type_id`
			JOIN `eventedge_device_categories` as `dc` ON `dc`.`id` = `d`.`device_category_id`
			JOIN `eventedge_services` as `sr` ON `sr`.`service_id` = `e`.`service_id` WHERE `t`.`priority` IN({$urgency_id}) AND `d`.`priority_id` IN({$skill_id})  AND `t`.`status_code` IN({$ticket_status_id}) AND `t`.`client_id` IN({$client_ids}) ORDER BY `t`.`ticket_id` DESC"
        );

        echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getTicketDetails($incident_id = "") {
        $this->db->select("t.*,e.event_invoice,d.uuid,d.device_name,d.address,sc.username,sc.password,cl.client_title as client,cl.client_shn_link as shn,cl.client_doc_url,pt.client_title as partner,CONCAT_WS(' ', uc.first_name,uc.last_name) as user , CONCAT_WS(' ', ua.first_name,ua.last_name) as assigned,cont.contact_name as requestor,loc.location_name,ts.status as state,p.name as priority_name,p.urgency_id,s.title as severity,tt.name as ticket_type,dc.category as category,sr.service_description  as sub_category");
        $this->db->from('tickets t');
        $this->db->join('client as cl', 'cl.id = t.client_id');
		$this->db->join('client as pt', 'pt.id = t.patner_id');  
        $this->db->join('users as uc', 'uc.id = t.created_by');
        $this->db->join('users as ua', 'ua.id = t.assigned_to', 'left');
        $this->db->join('contacts as cont', 'cont.contact_uuid = t.requestor', 'left');
        $this->db->join('client_locations as loc', 'loc.referrence_uuid = t.client_uuid', 'left');
        $this->db->join('ticket_status as ts', 'ts.status_code = t.status_code');
        $this->db->join('ticket_urgency as p', 'p.urgency_id = t.priority', 'left');
        $this->db->join('events as e', 'e.event_id = t.event_id', 'left');
        $this->db->join('devices as d', 'd.device_id = t.device_id', 'left');
        $this->db->join('system_credentials as sc', 'sc.referrence_uuid = d.uuid AND sc.is_primary = 1', 'left');
        $this->db->join('skill_set as s', 's.skill_id = t.skill_id', 'left');
        $this->db->join('ticket_types as tt', 'tt.ticket_type_id = t.ticket_type_id', 'left');
        $this->db->join('device_categories as dc', 'dc.id = t.ticket_cate_id', 'left');
        $this->db->join('services as sr', 'sr.service_id = t.service_id', 'left');
        $this->db->where('t.ticket_id', $incident_id);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateTicketDetails($postData = "", $ticket_id) {
        $this->db->where('ticket_id', $ticket_id);
        $update_status = $this->db->update($this->tickets, $postData);
        //pr($postData);
        //echo $ticket_id;exit;
        //echo $this->db->last_query();exit;
        if ($update_status) {
            if ($postData['assigned_to']) {

                $username = 'inspiredge';
                $password = 'inspiredge@123';
                $context = stream_context_create(array(
                    'http' => array(
                        'header' => "Authorization: Basic " . base64_encode("$username:$password")
                    )
                ));
                $url = base_url("webapi/assignEngineer/ticket_id/" . $ticket_id . "/primary_client_Id/2");
                $data = file_get_contents($url, false, $context);
                $array = json_decode($data, true);
            }
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateSSHUsers($postData = "", $sshId) {
        $this->db->where('ssh_id', $sshId);
        $update_status = $this->db->update($this->ssh_log, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateIncidentActivity($postData = "", $activity_log_id) {
        $this->db->where('activity_log_id', $activity_log_id);
        $update_status = $this->db->update($this->incident_activity_log, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function updateIncidentActivityFile($postData = "", $activity_log_id) {
        $this->db->where('activity_log_id', $activity_log_id);
        $update_status = $this->db->update($this->ticket_activity_files, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateContractConsumed($sec, $id) {
        $query = $this->db->query("UPDATE eventedge_contracts SET consumed_BOH = consumed_BOH+({$sec}) WHERE uuid = '{$id}'");
        //echo $this->db->last_query();exit;
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return false;
        }
    }

    public function getTicketStatus() {
        $this->db->select('*');
        $this->db->from($this->ticket_status);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    public function getActivityType() {
        $this->db->select('*');
        $this->db->from($this->activity_type);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function insertTicketActivity($data) {
        $this->db->insert($this->ticket_activity, $data);
        return $this->db->insert_id();
    }
	
	public function insertTicketActivityFiles($data) {
        $this->db->insert($this->ticket_activity_files, $data);
        return $this->db->insert_id();
    }

    public function insertIncidentActivity($data) {
        $this->db->insert($this->incident_activity_log, $data);
        return $this->db->insert_id();
    }

    public function insertSSHUsers($data) {
        $this->db->insert($this->ssh_log, $data);
        return $this->db->insert_id();
    }

    public function getLastTicketActivityID($ticket_id) {
        $query = $this->db->query("SELECT `activity_id` FROM `eventedge_ticket_activity` WHERE `ticket_id` ={$ticket_id} order by activity_id desc limit 1");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getAvailableBoh($contract_type) {
        $query = $this->db->query("SELECT TIME_FORMAT( sec_to_time((purchased_BOH+BOH_topup_hours)-consumed_BOH), '%H:%i')  as available_boh, (SELECT c.contract_type FROM eventedge_contract_types c WHERE c.uuid = contract_type_uuid) as contract FROM `eventedge_contracts`  WHERE uuid = '{$contract_type}' AND  validity_status = 'Active'");
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getAvailableBohSec($contract_type) {
        $query = $this->db->query("SELECT 
		((purchased_BOH+BOH_topup_hours)-consumed_BOH) as available_boh, (SELECT c.contract_type FROM eventedge_contract_types c WHERE c.uuid = contract_type_uuid) as contract FROM `eventedge_contracts`  WHERE uuid = '{$contract_type}' AND  validity_status = 'Active'");
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function updateLastActiveTicket($data, $con) {
        $this->db->update($this->ticket_activity, $data, $con);
    }

    public function ticketActivity($ticket_id) {
        $query = $this->db->query("SELECT s.*,(select status FROM eventedge_ticket_status where status_code = s.state_status) as state_status,(select concat(first_name,' ',last_name) FROM eventedge_users where id = s.user_id) as username FROM `eventedge_ticket_activity`s where s.ticket_id={$ticket_id} ORDER BY `activity_id` ASC");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getDetailsDeviceId($device_id = "") {
        $query = $this->db->query("SELECT e.*, date_format(e.event_start_time, '%D %b %Y %H:%i:%S') as start_time,
			(SELECT device_name FROM eventedge_devices WHERE device_id = e.device_id) as device,
			(SELECT incident_id FROM eventedge_tickets WHERE ticket_id = e.ticket_id) as ticket_id,
			(SELECT address FROM eventedge_devices WHERE device_id = e.device_id) as address,
			(SELECT service_description FROM eventedge_services WHERE service_id = e.service_id) as service,
			(SELECT severity FROM eventedge_severities WHERE id = e.severity_id) as severity,
			(SELECT event_state FROM eventedge_event_states WHERE id = e.event_state_id) as event_state,
			(SELECT priority_id FROM eventedge_devices WHERE device_id = e.device_id) as priority_id,
			(SELECT sla FROM eventedge_skill_set WHERE skill_id = (SELECT priority_id FROM eventedge_devices WHERE device_id = e.device_id)) as sla
			FROM `eventedge_events` e  where e.device_id = {$device_id} and e.ticket_id != 0 and e.severity_id != 2");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getAlarmtypeTicket($alarmtype) {
        $filter = "";
        if ($this->accessLevelClient['status'] == 'true') {
            $x = implode(",", $this->accessLevelClient['resultSet']);
            $filter = " and client_id in(" . $x . ")";
        }
        $query = $this->db->query("SELECT ticket_id from eventedge_events  where ticket_id!=0 and service_id={$alarmtype} " . $filter);
        $tickets_ids = array();

        if ($query->num_rows() > 0) {
            //echo $this->db->last_query();exit;
            $array = $query->result_array();
            for ($i = 0; $i < count($array); $i++) {
                $tickets_ids[] = $array[$i]['ticket_id'];
            }
            //pr($tickets_ids);exit;
            return $tickets_ids;
        } else {
            return false;
        }
    }

    public function getActivityLog($incident_id = "") {
        $this->db->select("i.activity_log_id,i.start_time,i.end_time,i.activity_desc,i.activity_comment,i.activity_file,i.created_on,i.created_by, at.name,usr.first_name as created_by,i.effort_time,i.incident_active_id,TIME_FORMAT( sec_to_time(i.current_boh), '%H:%i')  as current_boh ");
        $this->db->from('incident_activity_log i');
        $this->db->join('activity_type as at', 'at.activity_type_id = i.activity_type_id');
        $this->db->join('users as usr', 'usr.id = i.created_by');
        $this->db->where('i.ticket_id', $incident_id);
        $this->db->where('i.is_revision', 'N');
        $this->db->order_by('i.incident_active_id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	public function getActivityLogFiles($activityID = "") {
        $this->db->select("*");
        $this->db->from('incident_activity_log_files');
        $this->db->where('activity_log_id', $activityID);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getActivityLogEdit($incident_id = "") {
        $this->db->select("i.activity_log_id,i.incident_active_id,i.start_time,i.end_time,i.activity_desc,i.activity_comment,i.activity_file,i.created_on,i.created_by, at.name,usr.first_name as created_by,TIMESTAMPDIFF(SECOND, i.start_time, i.end_time) as effort_time,i.incident_active_id,TIME_FORMAT( sec_to_time(i.current_boh), '%H:%i')  as current_boh");
        $this->db->from('incident_activity_log i');
        $this->db->join('activity_type as at', 'at.activity_type_id = i.activity_type_id');
        $this->db->join('users as usr', 'usr.id = i.created_by');
        $this->db->where('i.ticket_id', $incident_id);
        $this->db->where('i.is_revision', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getActivityLogById($activityID) {
        $this->db->select('i.activity_log_id,i.start_time,i.end_time,i.activity_desc,i.activity_comment,i.activity_file,i.created_on,i.created_by, at.name,usr.first_name as created_by,TIMESTAMPDIFF(SECOND, i.start_time, i.end_time) as effort_time,i.incident_active_id,i.activity_type_id,i.current_boh');
        $this->db->from('incident_activity_log i');
        $this->db->join('activity_type as at', 'at.activity_type_id = i.activity_type_id');
        $this->db->join('users as usr', 'usr.id = i.created_by');
        $this->db->where('i.activity_log_id', $activityID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getSSHLog($incident_id = "") {
        $this->db->select('i.login_time,i.logout_time,i.login_mode, concat_ws("  ",usr.first_name ,  ,usr.last_name) as username,TIMESTAMPDIFF(SECOND, i.login_time, i.logout_time) as effort_time');
        $this->db->from('ssh_log i');
        $this->db->join('users as usr', 'usr.id = i.user_id');
        $this->db->where('i.incident_id', $incident_id);
		$this->db->order_by('i.ssh_id','desc');
		
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getLoginUsers($partner = NULL, $skill = NULL) {
        $accessLevelClient = helper_getAccessClientDropDown();
        $this->db->select('id,concat_ws("  ",first_name ,  , last_name) as username, at.title');
        $this->db->from('users');
        $this->db->join('skill_set as at', 'at.skill_id =' . $skill);
        if( $accessLevelClient['status']== 'true'){
            $this->db->where_in('client', $accessLevelClient['resultSet']);
        }
        else
        {
            $this->db->where('client', 0);
        }
       // $this->db->where('partner_id', $partner);
        //$this->db->where('is_login', 'N'); hold for time been
		$this->db->where('is_deleted', "N");
        $this->db->like('skill', $skill);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    /* public function getAllClients(){
      //$accessLevelClient = helper_getAccessClientDropDown();
      $this->db->select('id,client_title');
      $this->db->from('client');
      //$this->db->where_in('id', $accessLevelClient['resultSet']);
      $query = $this->db->get();
      // echo $this->db->last_query();exit;
      if ($query->num_rows() > 0) {
      $return['status'] = 'true';
      $return['resultSet'] = $query->result_array();
      } else {
      $return['status'] = 'false';
      }
      //       // pr($return);exit;
      return $return;
      } */

    public function getAllCategories() {
        $this->db->select('*');
        $this->db->from('tickets_category');
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    public function getAllSubCategories($params = array()) {
        $this->db->select('*');
        $this->db->from('ticket_sub_category');

        if (is_array($params) && count($params) > 0) {
            $this->db->where($params);
        }

        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    public function getTicketType() {
        $this->db->select('*');
        $this->db->from('ticket_types');
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    public function getQueue($userId) {
        $this->db->select('*');
        $this->db->from('ticket_queue');
        $this->db->where_in('created_by', $userId);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    public function getQueueDataByID($queueId) {
        $this->db->select('*');
        $this->db->from('ticket_queue');
        $this->db->where('queue_id', $queueId);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    /* 	public function getClientUsers($clientId = ""){
      $this->db->select('*');
      $this->db->from('users');
      $this->db->where('client',$clientId);
      $query = $this->db->get();
      //echo $this->db->last_query();exit;
      if ($query->num_rows() > 0) {
      $return['status'] = 'true';
      $return['resultSet'] = $query->result_array();
      } else {
      $return['status'] = 'false';
      }
      //       // pr($return);exit;
      return $return;
      } */

    public function getSubclients($clientId = "") {
        $this->db->select('*');
        $this->db->from('client');
        $this->db->where('parent_client_id', $clientId);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    public function getSeverity() {
        $permalink = array(2, 3);
        $accessLevelClient = helper_getAccessLevelClient();
        $this->db->select('severity_id as id,(SELECT severity FROM `eventedge_severities` WHERE id=eventedge_client_severities.severity_id) as severity');
        $this->db->from('client_severities');
        if ($accessLevelClient['status'] == 'true') {
            $this->db->where_in('client_id', $accessLevelClient['resultSet']);
        }
        $this->db->where_not_in('severity_id', $permalink);
        $this->db->group_by('severity_id');
        $query = $this->db->get();
//       echo $this->db->last_query();
//                
//        exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getPriority() {
        $this->db->select('*');
        $this->db->from('skill_set');
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    public function getMacDetails($ticket_id = "") {
        $query = $this->db->query("SELECT t.*, m.request_type,
									(select device_id from eventedge_macd where ticket_id = t.ticket_id) as device_id,
									(select device_name from eventedge_devices where device_id = (select device_id from eventedge_macd where ticket_id = t.ticket_id)) as device_name,
									(select location_uuid_to from eventedge_macd where ticket_id = t.ticket_id) as location_uuid,
									(select location_name from eventedge_client_locations where location_uuid = (select location_uuid_to from eventedge_macd where ticket_id = t.ticket_id)) as location,
									(select client_title from eventedge_client where id = t.client_id) as client
									FROM `eventedge_tickets`t, `eventedge_macd`m  where  m.ticket_id = t.ticket_id and t.ticket_id = {$ticket_id}");
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function IncidentData($requestData = array(), $type = "", $start = "", $length = "", $column = "", $dir = "", $status_id = "", $client_id = "", $ticketIds = array(), $notDashboard = "", $user_id = "") {
		//pr($requestData);exit;
		
        $this->db->select("t.ticket_id,t.incident_id,t.created_on,t.requestor,t.subject,t.last_updated_on,t.ticket_external_ref,t.vendor_ref,t.status_code,cls.client_title as partner,cl.client_title as client, cl.client_shn_link as shn,CONCAT_WS(' ', uc.first_name,uc.last_name) as user ,CONCAT_WS(' ', ua.first_name,ua.last_name) as assigned_to, cont.contact_name as requestor, ts.status as state,p.name as priority,s.title as severity,tt.name as ticket_type,dc.category as category,dsb.subcategory as sub_category");
        $this->db->from('tickets t');
        $this->db->join('client as cls', 'cls.id = t.patner_id', 'left');
        $this->db->join('client as cl', 'cl.id = t.client_id', 'left');
        $this->db->join('users as uc', 'uc.id = t.created_by', 'left');
        $this->db->join('users as ua', 'ua.id = t.assigned_to', 'left');
        $this->db->join('contacts as cont', 'cont.contact_uuid = t.requestor', 'left');
        $this->db->join('ticket_status as ts', 'ts.status_code = t.status_code','left');
        $this->db->join('ticket_urgency as p', 'p.urgency_id = t.priority', 'left');
        //$this->db->join('events as e', 'e.event_id = t.event_id','left');
        //$this->db->join('devices as d', 'd.device_id = e.device_id','left');
        $this->db->join('skill_set as s', 's.skill_id = t.skill_id', 'left');
        $this->db->join('ticket_types as tt', 'tt.ticket_type_id = t.ticket_type_id', 'left');
        $this->db->join('device_categories as dc', 'dc.id = t.ticket_cate_id', 'left');
        $this->db->join('device_sub_categories as dsb', 'dsb.id = t.ticket_sub_cate_id', 'left');
        $this->db->where('t.ticket_type_id', $type);
        if ($this->accessLevelClient['status'] == 'true') {
            $this->db->where_in('t.client_id', $this->accessLevelClient['resultSet']);
        }
        if (isset($column) && !empty($column) && isset($dir) && !empty($dir)) {
            $this->db->order_by($column, $dir);
        }
        ///////////////////////////////////////////////////////////////////////////////////
        //pr($requestData);
        /* $where = array();
          if(isset($requestData['clientId']) && $requestData['clientId']!="")
          {
          $where['t.client_id'] = $requestData['clientId'];
          } */
        if (isset($requestData['patner_id']) && $requestData['patner_id'] != "" && count($requestData['patner_id']) > 0) {
            $this->db->where_in('t.patner_id', $requestData['patner_id']);
        }
        if (isset($requestData['clientId']) && $requestData['clientId'] != "" && count($requestData['clientId']) > 0) {
            $this->db->where_in('t.client_id', $requestData['clientId']);
        }
        if (isset($requestData['requestor']) && $requestData['requestor'] != "" && count($requestData['requestor']) > 0) {
            $this->db->where_in('t.requestor', $requestData['requestor']);
        }
        if (isset($requestData['status_code']) && $requestData['status_code'] != "" && count($requestData['status_code']) > 0) {
            $this->db->where_in('t.status_code', $requestData['status_code']);
        }
        if (isset($requestData['ticket_cate_id']) && $requestData['ticket_cate_id'] != "" && count($requestData['ticket_cate_id']) > 0) {
            $this->db->where_in('t.ticket_cate_id', $requestData['ticket_cate_id']);
        }
        if (isset($requestData['ticket_sub_cate_id']) && $requestData['ticket_sub_cate_id'] != "" && count($requestData['ticket_sub_cate_id']) > 0) {
            $this->db->where_in('t.ticket_sub_cate_id', $requestData['ticket_sub_cate_id']);
        }
        if (isset($requestData['user_id']) && $requestData['user_id'] != "" && count($requestData['user_id']) > 0) {
            $this->db->where_in('t.assigned_to', $requestData['user_id']);
        }
        if (isset($requestData['severity_id']) && $requestData['severity_id'] != "" && count($requestData['severity_id']) > 0) {
            $this->db->where_in('t.skill_id', $requestData['severity_id']);
        }
        if (isset($requestData['priority_id']) && $requestData['priority_id'] != "" && count($requestData['priority_id']) > 0) {
            $this->db->where_in('t.priority', $requestData['priority_id']);
        }
        if (isset($requestData['date']) && $requestData['date'] != "" && $requestData['date'] == 1) {
            $this->db->where('DATE(t.created_on)', date('Y-m-d'));
        }
        if (isset($requestData['date']) && $requestData['date'] != "" && $requestData['date'] == 2) {
            $this->db->where('DATE(t.created_on)', date('Y-m-d', strtotime("-1 days")));
        }
        if (isset($requestData['date']) && $requestData['date'] != "" && $requestData['date'] == 3) {
            $this->db->where('YEARWEEK(t.created_on)= YEARWEEK(CURRENT_DATE)');
        }
        if (isset($requestData['date']) && $requestData['date'] != "" && $requestData['date'] == 4) {
            $this->db->where('YEARWEEK(t.created_on) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)');
        }
        if (isset($requestData['date']) && $requestData['date'] != "" && $requestData['date'] == 5) {
            $this->db->where(array('year(DATE(t.created_on))' => date('Y'), 'month(DATE(t.created_on))' => date('m')));
        }
        if (isset($requestData['date']) && $requestData['date'] != "" && $requestData['date'] == 6) {
            $this->db->where(array('year(DATE(t.created_on))' => date('Y', strtotime("-1 months")), 'month(DATE(t.created_on))' => date('m', strtotime("-1 months"))));
        }

        ///////////////////////////////////////////////////////////////////////////////////

        if (isset($requestData['search']['value']) && $requestData['search']['value'] != "" && count($requestData['search']['value']) > 0) {
            //$this->db->where_in('t.client_id',$requestData['clientId']);
            $this->db->group_start();
            $this->db->like('t.incident_id', $requestData['search']['value']);
            $this->db->or_like('t.ticket_external_ref', $requestData['search']['value']);
            $this->db->or_like('t.vendor_ref', $requestData['search']['value']);
            $this->db->or_like('s.title', $requestData['search']['value']);
            $this->db->or_like('cls.client_title', $requestData['search']['value']);
            $this->db->or_like('cl.client_title', $requestData['search']['value']);
            $this->db->or_like('t.subject', $requestData['search']['value']);
            $this->db->or_like('ts.status', $requestData['search']['value']);
            //$this->db->or_like('tt.name', $requestData['search']['value']);
            $this->db->or_like('cont.contact_name', $requestData['search']['value']);
            $this->db->or_like('p.name', $requestData['search']['value']);
            $this->db->or_like("CONCAT_WS(' ', uc.first_name,uc.last_name)", $requestData['search']['value']);
            $this->db->or_like('dc.category', $requestData['search']['value']);
            $this->db->or_like('dsb.subcategory', $requestData['search']['value']);
            $this->db->or_like("CONCAT_WS(' ', ua.first_name,ua.last_name)", $requestData['search']['value']);
            $this->db->group_end();
        }

        if (isset($requestData['status_id']) && !empty($requestData['status_id'])) {
            $data = base64_decode($requestData['status_id']);
            $status_array = explode(",", $data);
            $status_id = (isset($status_array)) ? $status_array : "";
            $this->db->where_in('t.status_code', $status_id);
        }

        if (isset($requestData['lastupdatedbucket']) && !empty($requestData['lastupdatedbucket'])) {
            $data = base64_decode($requestData['lastupdatedbucket']);
            $status_array = explode("-", $data);
            $lastupdatedbucket = (isset($status_array)) ? $status_array : "";
            $this->db->where('DATEDIFF(CURDATE(), (SELECT end_time FROM `eventedge_incident_activity_log` WHERE `ticket_id` = t.ticket_id ORDER by activity_log_id DESC LIMIT 1)) >=', $lastupdatedbucket[0]);
            $this->db->where('DATEDIFF(CURDATE(), (SELECT end_time FROM `eventedge_incident_activity_log` WHERE `ticket_id` = t.ticket_id ORDER by activity_log_id DESC LIMIT 1)) <=', $lastupdatedbucket[1]);
            $this->db->where('t.status_code !=', '7');
        }

        if (isset($requestData['ageingbucket']) && !empty($requestData['ageingbucket'])) {
            $data = base64_decode($requestData['ageingbucket']);
            $status_array = explode("-", $data);
            $ageingbucket = (isset($status_array)) ? $status_array : "";
            $this->db->where('DATEDIFF(CURDATE(), t.created_on) >=', $ageingbucket[0]);
            $this->db->where('DATEDIFF(CURDATE(), t.created_on) <=', $ageingbucket[1]);
            $this->db->where('t.status_code !=', '7');
        }
        
        if (isset($requestData['livestatust']) && !empty($requestData['livestatust'])) {
            $permalinklivestatust = array(5,7);
            $severity_id = base64_decode($requestData['livestatust']);
            $this->db->where('t.severity_id', $severity_id);
            $this->db->where_not_in('t.status_code', $permalinklivestatust); 
        }
        if (isset($requestData['ticket_idv']) && !empty($requestData['ticket_idv'])) {
			
			 $ticket_idv = base64_decode($requestData['ticket_idv']);
			 $this->db->where('t.ticket_id', $ticket_idv);
		}
        if (isset($requestData['breachedstatust']) && !empty($requestData['breachedstatust'])) {
            $permalinkbreachedstatust = array(5,7);
            $severity_id = base64_decode($requestData['breachedstatust']);
            $this->db->where('t.sla_due_time <= now()');
            $this->db->where('t.severity_id', $severity_id);
            $this->db->where_not_in('t.status_code', $permalinkbreachedstatust); 
        }

        if (isset($notDashboard) && !empty($notDashboard)) {
            if ($client_id != "") {
                $this->db->where('t.client_id', $client_id);
                $this->db->where('t.status_code', 7);
            }
        } else if (isset($client_id) && !empty($client_id)) {
            //$this->db->where_in('t.client_id', $accessLevelClient['resultSet']);
            $this->db->where('t.client_id', $client_id);
        }/* else{
          $this->db->where_in('t.client_id', $accessLevelClient['resultSet']);
          } */
        if (count($ticketIds) > 0) {
            $this->db->where_in('t.ticket_id', $ticketIds);
        }

        if (!empty($user_id)) {
            $this->db->where_in('t.assigned_to', $user_id);
        }

        if ($start >= 0 and $length > 0) {
            $this->db->limit($length, $start);
        }
        $query = $this->db->get();

        //echo $this->db->last_query();exit;		
        if ($length == 0 && $start == 0 && $requestData['search']['value'] == 0) {
            return $query->num_rows();
        }
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getAllDeviceCategories() {
        $this->db->select('*');
        $this->db->from('device_categories');
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    public function getAllDeviceSubCategories($params = array()) {
        $this->db->select('*');
        $this->db->from('services');

        if (is_array($params) && count($params) > 0) {
            $this->db->where($params);
        }

        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    public function getSkillSeverity() {
        /* $permalink = array(2, 3);
          $accessLevelClient = helper_getAccessLevelClient(); */
        $this->db->select('*');
        $this->db->from('skill_set');
        /* if ($accessLevelClient['status'] == 'true') {
          $this->db->where_in('client_id', $accessLevelClient['resultSet']);
          } */
        //$this->db->where_not_in('severity_id', $permalink);
        //$this->db->group_by('severity_id');
        $query = $this->db->get();
//       echo $this->db->last_query();
//                
//        exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getUrgencyPriority() {
        $this->db->select('*');
        $this->db->from('ticket_urgency');
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    public function getStatusDetails($status_code) {
        $this->db->select('status');
        $this->db->from('ticket_status');
        $this->db->where('status_code', $status_code);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    //SELECT sum(effort_time) as totalSeconds FROM eventedge_incident_activity_log WHERE ticket_id = 8
    public function getTotalSecForTicket($ticketId) {
        $this->db->select('sum(effort_time) as totalSeconds');
        $this->db->from('incident_activity_log');
        $this->db->where('ticket_id', $ticketId);
        $this->db->where('is_revision', 'N');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getTicketClosedDays($ticketId) {
        $this->db->select('DATEDIFF(NOW(),last_updated_on) as days');
        $this->db->from('eventedge_tickets');
        $this->db->where('ticket_id', $ticketId);
        $this->db->where('status_code', 7);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getAllClients($ids = array()) {
        $this->db->select('cnt.id,cnt.client_title,cnt.client_uuid,cn.client_title as parent_name');
        $this->db->from('client as cnt');
        $this->db->join('client as cn', 'cn.id = cnt.parent_client_id');
        $this->db->where_in('cnt.id', $ids);
        /*  $this->db->select('id,client_title,client_uuid');
          $this->db->from('client');
          $this->db->where('status', 'Y');
          $this->db->where_in('id', $ids); */
        $query = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getClientsDetails($partnerId = "") {
        $this->db->select('id,client_title');
        $this->db->from('client');
        $this->db->where('status', 'Y');
        $this->db->where('client_type', 'C');
        $this->db->where_in('parent_client_id', $partnerId);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;	
        if ($query->num_rows() > 0) {
            $data['status'] = 'true';
            $data = $query->result_array();
            //pr($data);exit;
            // $rows = implode(",", array_column($data, 'client_id'));
            //$return['status'] = 'true';
            $return['resultSet'] = $data;
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getAllRequestorsFromClients($ids = array()) {

        $this->db->select('contact_uuid,contact_name,reference_uuid');
        $this->db->from('contacts');
        $this->db->where('status', 'Y');
        $this->db->where_in('reference_uuid', $ids);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getUsersListByCompanyId($ids = array()) {
        $this->db->select("id,CONCAT_WS(' ', first_name,last_name,email_id) as user");
        $this->db->from($this->users);
        $this->db->where_in('client', $ids);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    public function getSubCategoriesByCateId($cate_ids = array()) {
        $this->db->select("id,subcategory");
        $this->db->from('device_sub_categories');
        $this->db->where('status', 'Y');
        $this->db->where_in('device_category_id', $cate_ids);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

}

?>