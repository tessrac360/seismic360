<?php

class Model_event extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'temp_role';
        $this->client = 'client';
        $this->company_id = $this->session->userdata('company_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $this->client_id = $this->session->userdata('client_id');

        $this->cData = array('created_on' => $current_date, 'created_by' => $this->userId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $this->userId);
    }

    public function getClientTreeView($parent_id = "", $access = 0) { // 1:top level 0:low level
        $this->db->select('id,client_title,parent_client_id');
        $this->db->from($this->client);
        if ($access == 1) {
            $this->db->where('id', $parent_id);
        } else {
            $this->db->where('parent_client_id', $parent_id);
        }
        $query = $this->db->get();
        //echo $this->db->last_query().'<br>';//exit;

        if ($query->num_rows() > 0) {

            return $query->result_array();
        } else {
            return array();
        }
    }

    public function getEvent($eventFilter = array()) {        
        //$skill = helper_getSkill();
        $accessLevelClient = helper_getAccessLevelClient();
        //pr($eventFilter);exit;
        $type = "";
        if (!empty($eventFilter)) {
            if ($eventFilter['param_severitypost'] != "") {
                $eventparamSeveritypost = $eventFilter['param_severitypost'];
            }

            if ($eventFilter['eventSearch'] != "") {
                $eventSearch = $eventFilter['eventSearch'];
            }

            if (isset($eventFilter['severity'])) {
                $severity = $eventFilter['severity'];
            }
            if (isset($eventFilter['device_name'])) {
                $device_name = $eventFilter['device_name'];
            }
            if (isset($eventFilter['clients'])) {
                $clients = $eventFilter['clients'];
            }

            if (isset($eventFilter['service'])) {
                $service = $eventFilter['service'];
            }

            if (isset($eventFilter['state'])) {
                $state = $eventFilter['state'];
            }

            if (isset($eventFilter['type'])) {
                $type = $eventFilter['type'];
            }

            if (isset($eventFilter['acktype'])) {
                $acktype = $eventFilter['acktype'];
            }
            
            if (isset($eventFilter['tools'])) {
                $tools = $eventFilter['tools'];
            }
        }
        $permalink = array(2, 3);
        $current_date = $this->config->item('datetime');        
        $ticket_id = "(SELECT ticket_id FROM `eventedge_tickets` WHERE `ticket_id` = ev.ticket_id) as ticket_id";
        $incident_id = "(SELECT incident_id FROM `eventedge_tickets` WHERE `ticket_id` = ev.ticket_id) as incident_id";        
        $this->db->select("$incident_id,$ticket_id,part.client_title as partner_title,gettype.title as getway_title,ev.client_name,ev.event_id,ev.event_invoice,ev.event_text,SUBSTR(ev.event_text, 1, 30) as event_text_small,ev.event_start_time,ev.event_end_time,ev.event_created_time,ev.event_description,ev.client_id,clnt.client_title,devi.device_name,serv.service_description as service_name,severity.severity,group.title as domain_name,eventsta.event_state,TIMEDIFF(NOW(), ev.event_start_time) as Duration,severity.color_code,ev.is_suppressed");
        $this->db->from('events as ev');
        $this->db->join('client as clnt', 'clnt.id = ev.client_id');
        $this->db->join('client as part', 'part.id = ev.partner_id');
        $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
        $this->db->join('services as serv', 'serv.service_id = ev.service_id');
        $this->db->join('severities as severity', 'severity.id = ev.severity_id');
        $this->db->join('group as group', 'group.id = ev.domain_id');
        $this->db->join('client_gateways as cgat', 'cgat.id = ev.gateway_id');
        $this->db->join('gateway_types as gettype', 'gettype.gatewaytype_uuid = cgat.gateway_type');        
        $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');
        $this->db->join('tickets tkt', 'tkt.ticket_id = ev.ticket_id','LEFT');

        if ($type != 'BREACHED') {
            $this->db->where('ev.event_end_time', '0000-00-00 00:00:00');
        }

        $this->db->where_not_in('ev.severity_id', $permalink);
        
        if ($accessLevelClient['status'] == 'true') {
            $this->db->where_in('ev.client_id', $accessLevelClient['resultSet']);
        }
        //$this->db->where_in('ev.priority_id', $skill);
        if (isset($eventSearch)) {
            $this->db->group_start();
            $this->db->like('clnt.client_title', $eventSearch);
            $this->db->like('part.client_title', $eventSearch);
            $this->db->or_like('group.title', $eventSearch);
            $this->db->or_like('devi.device_name', $eventSearch);
            $this->db->or_like('serv.service_description', $eventSearch);
            $this->db->or_like('severity.severity', $eventSearch);
            $this->db->or_like('eventsta.event_state', $eventSearch);
            $this->db->or_like('ev.event_invoice', $eventSearch);
            $this->db->or_like('ev.client_name', $eventSearch);
            $this->db->or_like('gettype.title', $eventSearch);
            $this->db->or_like('tkt.incident_id', $eventSearch);
            $this->db->group_end();
        }

        if (isset($eventparamSeveritypost)) {
            $this->db->where('ev.severity_id', $eventparamSeveritypost);
        }
        
        if (isset($tools)) {
            $this->db->where_in('cgat.gateway_type', $tools);
        }

        if (isset($severity)) {
            $this->db->where_in('ev.severity_id', $severity);
        }
        if (isset($device_name)) {
            $this->db->where_in('ev.device_id', $device_name);
        }
        if (isset($clients)) {
            $this->db->where_in('ev.client_id', $clients);
        }
        if (isset($service)) {
            $this->db->where_in('ev.service_id', $service);
        }
        if (isset($state)) {
            $this->db->where_in('ev.event_state_id', $state);
        }
        $this->db->order_by('ev.event_start_time', 'desc');
        $this->db->limit(500);
        $query = $this->db->get();
           //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $newArray = array();
            foreach ($query->result() as $row) {
                if ($row->is_suppressed == 'N') {
                    // pr($row);
                    $showTickeicon = "";
                    if ($row->ticket_id == "") {
                        $showTickeicon = '    <a title="manual ticket" class="manual_ticket mticket_' . $row->event_id . '" evtid=' . $row->event_id . '  href="javascript:void(0);"><i class="fa fa-ticket" aria-hidden="true"></i></a>';
                    }
                    $row->event_suppress_action = '<a title="Suppress" href="javascript:void(0);" class="suppress_action" evtid=' . $row->event_id . '><i class="fa fa-clock-o" aria-hidden="true"></i></a>' . $showTickeicon . '</span>';
                    
                    $event_description = json_decode($row->event_description,'true');                    
                    
                    $state_str = (isset($event_description['state_str']))?$event_description['state_str']:'';
                    
                   
                    $event_description =  $state_str. '  -  ' . $event_description['service_description'] . '  -  ' . $event_description['output'];                   
                    
                    $row->service_name = '<span title="' . $row->service_name . '">' . wordlimit($row->service_name, 20) . '</span>';
                    $row->event_description = '<span title="' . $event_description . '">' . wordlimit($event_description, 10) . '</span>';
                    $row->event_text = '<span  title="' . $row->event_text . '">' . $row->event_text_small . '</span>';
                    $row->severity = '<span class="severityEvent" style="background-color: ' . $row->color_code . '; ">' . $row->severity . '</span>';
                    $row->ticketDetails = '<a href="' . base_url('event/ticket/editTicket/' . encode_url($row->ticket_id)) . '">' . $row->incident_id . '</a>';
                    $data[] = $row;
                }
            }
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }
//        pr($return);
//        exit;
        return $return;
    }

    public function getAcknowlegement($event_id = "") {
        $current_date = $this->config->item('datetime');
        $this->db->select('acks.ack_status');
        $this->db->from('event_acknowledgements as evack');
        $this->db->join('ack_status as acks', 'acks.id = evack.ack_status_id');
        //$this->db->where('evack.event_suppress_till >=', $current_date);
        $this->db->where('evack.event_id', $event_id);
        $this->db->order_by('evack.ack_id', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
        }
        return $return;
        //echo $this->db->last_query();
    }

    public function getHostName() {
        $accessLevelClient = helper_getAccessLevelClient();
        $this->db->select('device_id,device_name');
        $this->db->from('devices');
        $this->db->where('device_name !=', 'NA');
        if ($accessLevelClient['status'] == 'true') {
            $this->db->where_in('client_id', $accessLevelClient['resultSet']);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getService() {
        $accessLevelClient = helper_getAccessLevelClient();
       // $this->db->select('service_id,(SELECT service_description FROM `eventedge_services` WHERE service_id=eventedge_gateway_services.service_id) as service_description');
		
		$this->db->select('service_id,service_description');
        $this->db->from('eventedge_services');
        //$this->db->where('service_description !=', '');
       // if ($accessLevelClient['status'] == 'true') {
        //    $this->db->where_in('client_id', $accessLevelClient['resultSet']);
        //}
        //$this->db->group_by('service_id');
        $query = $this->db->get();

        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getSeverity() {
        $permalink = array(2, 3);
        $accessLevelClient = helper_getAccessLevelClient();
        $this->db->select('id, severity');
        $this->db->from('severities');
//        if ($accessLevelClient['status'] == 'true') {
//            $this->db->where_in('client_id', $accessLevelClient['resultSet']);
//        }
        $this->db->where_not_in('id', $permalink);
        //$this->db->group_by('severity_id');
        $query = $this->db->get();
//        echo $this->db->last_query();
//                
//        exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getSeverityFilter() {
        $this->db->select('id,severity');
        $this->db->from('severities');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getState() {
        $this->db->select('id,event_state');
        $this->db->from('event_states');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getEventColumn() {
        $this->db->select('ecs.sort_id,ecr.rename_column,ec.column_id,ec.is_filter,ec.filter_function,ec.table_column_name');
        $this->db->from('event_column_rename ecr');
        $this->db->join('event_column_sort as ecs', 'ecs.rename_id = ecr.rename_id');
        $this->db->join('event_column as ec', 'ec.column_id = ecr.column_id');
        $this->db->where('ecr.status', 'Y');
        $this->db->order_by('ecs.position', 'asc');
        $this->db->where('ecs.user_id', $this->userId);
        $this->db->where('ecs.client_id', $this->client_id);
        $query = $this->db->get();
        //    echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
//       // pr($return);exit;
        return $return;
    }

    public function getEventDetails($event_id) {
        $this->db->select('ev.client_name,ev.client_id,ev.event_id,ev.event_text,ev.service_id,ev.device_id,ev.event_start_time,ev.event_end_time,ev.event_created_time,ev.event_description,clnt.client_title,devi.device_name,devi.address,serv.service_description as service_name,severity.severity,group.title as domain_name,eventsta.event_state,TIMEDIFF(NOW(), ev.event_start_time) as Duration,cget.ip_address');
        $this->db->from('events as ev');
        $this->db->join('client as clnt', 'clnt.id = ev.client_id');
        $this->db->join('client_gateways as cget', 'cget.id = ev.gateway_id');
        $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
        $this->db->join('services as serv', 'serv.service_id = ev.service_id');
        $this->db->join('severities as severity', 'severity.id = ev.severity_id');
        $this->db->join('group as group', 'group.id = ev.domain_id');
        $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');
        $this->db->where('ev.event_id', $event_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function getEventsHistory($device_id, $service_id) {
        $this->db->select('ev.event_id,ev.event_text,ev.event_start_time,ev.event_end_time,ev.event_created_time,ev.event_description,clnt.client_title,devi.device_name,serv.service_description as service_name,severity.severity,group.title as domain_name,eventsta.event_state,TIMEDIFF(NOW(), ev.event_start_time) as Duration');
        $this->db->from('events as ev');
        $this->db->join('client as clnt', 'clnt.id = ev.client_id');
        $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
        $this->db->join('services as serv', 'serv.service_id = ev.service_id');
        $this->db->join('severities as severity', 'severity.id = ev.severity_id');
        $this->db->join('group as group', 'group.id = ev.domain_id');
        $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');
        $this->db->where('ev.device_id', $device_id);
        $this->db->where('ev.service_id', $service_id);
        $this->db->order_by('ev.event_start_time', 'desc');
        $this->db->limit(10);
        //echo $this->db->last_query();
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getEventMonitorService($device_id) {
        $this->db->select('ev.event_id,ev.event_text,ev.event_start_time,ev.event_end_time,ev.event_created_time,ev.event_description,clnt.client_title,devi.device_name,serv.service_description as service_name,severity.severity,group.title as domain_name,eventsta.event_state,TIMEDIFF(NOW(), ev.event_start_time) as Duration');
        $this->db->from('events as ev');
        $this->db->join('client as clnt', 'clnt.id = ev.client_id');
        $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
        $this->db->join('services as serv', 'serv.service_id = ev.service_id');
        $this->db->join('severities as severity', 'severity.id = ev.severity_id');
        $this->db->join('group as group', 'group.id = ev.domain_id');
        $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');
        $this->db->where('ev.device_id', $device_id);
        $this->db->where('ev.event_end_time', '0000-00-00 00:00:00');
        $this->db->order_by('ev.event_start_time', 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getEventColumnRename() {
        $this->db->select('sort_id');
        $this->db->from('event_column_sort');
        $this->db->where('client_id', $this->client_id);
        $this->db->where('user_id', $this->userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'false';
        } else {
            $this->db->select('rename_id');
            $this->db->from('event_column_rename');
            $this->db->where('client_id', $this->client_id);
            $this->db->where('status', 'Y');
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $return['status'] = 'true';
                $return['resultSet'] = $query->result();
            } else {
                $return['status'] = 'false';
            }
        }
        return $return;
    }

    public function getMasterKey() {
        $this->db->select('masterkey');
        $this->db->from('masterkey');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        }
        return $return;
    }

    public function getAcknoledgeDetails($event_id) {
        $this->db->select('acks.ack_status,DATE_FORMAT(evack.event_suppress_till, "%m-%d-%Y %H:%i") as suppress_datetime ,evack.ack_notes,DATE_FORMAT(evack.created_on, "%m-%d-%Y %H:%i") as ack_time,CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from('event_acknowledgements as evack');
        $this->db->join('ack_status as acks', 'acks.id = evack.ack_status_id');
        $this->db->join('users as usr', 'usr.id = evack.created_by');
        $this->db->where('evack.event_id', $event_id);
        $this->db->order_by('evack.ack_id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    /* Start Event History Module */

    public function getEventHistory($limit = "", $start = "") {
        //pr($_SESSION);exit;
//        if(isset($searchdata)){
//            $data = $searchdata['date'];
//            $time = $searchdata['hour'];
//        }
        //pr($_SESSION);exit;
        if (isset($_SESSION['eventHistorySession'])) {
            $filterPost = unserialize($_SESSION['eventHistorySession']);
            //pr($filterPost);//exit;
            if (!empty($filterPost)) {
                if (isset($filterPost['client'])) {
                    $clients = $filterPost['client'];
                }

                if (isset($filterPost['device'])) {
                    $device_name = $filterPost['device'];
                }
                if (isset($filterPost['service'])) {
                    $service = $filterPost['service'];
                }

                if (isset($filterPost['severity'])) {
                    $severity = $filterPost['severity'];
                }

                if (isset($filterPost['from_date']) && $filterPost['from_date'] != "") {
                    $from_date = $filterPost['from_date'];
                }

                if (isset($filterPost['to_date']) && $filterPost['to_date'] != "") {
                    $to_date = $filterPost['to_date'];
                }

                if (isset($filterPost['date'])) {
                    $date = $filterPost['date'];
                }

                if (isset($filterPost['hour'])) {
                    $hour = $filterPost['hour'];
                }

                if (isset($filterPost['acktype'])) {
                    $acktype = $filterPost['acktype'];
                }
            }
        }
        //  echo $acktype;exit;

        $domain = helper_getDomain();
        //$skill = helper_getSkill();
        $accessLevelClient = helper_getAccessLevelClient();
        $this->db->select('cn.client_title as partnername,ev.event_invoice,ev.event_id,ev.event_text,SUBSTR(ev.event_text, 1, 30) as event_text_small,ev.event_start_time,ev.event_end_time,ev.event_created_time,ev.event_description,ev.client_id,clnt.client_title,devi.device_name,serv.service_description as service_name,severity.severity,group.title as domain_name,eventsta.event_state,(case when (ev.event_end_time = "0000-00-00 00:00:00") THEN TIMEDIFF(NOW(), ev.event_start_time) ELSE TIMEDIFF(ev.event_end_time, ev.event_start_time) END) as Duration,severity.color_code');
        $this->db->from('events as ev');
        $this->db->join('client as clnt', 'clnt.id = ev.client_id');
        $this->db->join('client as cn', 'cn.id = ev.partner_id');
        $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
        $this->db->join('services as serv', 'serv.service_id = ev.service_id');
        $this->db->join('severities as severity', 'severity.id = ev.severity_id');
        $this->db->join('group as group', 'group.id = ev.domain_id');
        $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');
        //$this->db->join('tickets tkt', 'tkt.event_id = ev.event_id');
        if ($domain['status'] == 'true') {
            $this->db->where_in('ev.domain_id', $domain['resultSet']);
        }
        if ($accessLevelClient['status'] == 'true') {
            $this->db->where_in('ev.client_id', $accessLevelClient['resultSet']);
        }

        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
      
        if (isset($date)) {
            $permalink = array(2, 3);
            $this->db->where_not_in('ev.severity_id', $permalink);
            $this->db->where('DATE(ev.event_start_time)', $date);
        }
        if (isset($hour)) {
            $this->db->where('HOUR(ev.event_start_time)', $hour);
        }

        /* start costom filter */
        if (isset($clients)) {
            $this->db->where_in('ev.partner_id', $clients);
        }
        if (isset($device_name)) {
            $this->db->where_in('ev.device_id', $device_name);
        }

        if (isset($service)) {
            $this->db->where_in('ev.service_id', $service);
        }

        if (isset($severity)) {
            $this->db->where_in('ev.severity_id', $severity);
        }

        if (isset($from_date) && isset($to_date)) {
            $this->db->where('DATE(ev.event_start_time) >=', $from_date);
            $this->db->where('DATE(ev.event_start_time) <=', $to_date);
        }
        if (isset($acktype)) {
            $permalink = array(2, 3);
            $this->db->where_not_in('ev.severity_id', $permalink);
            $this->db->where('ev.is_suppressed', 'N');
            if ($acktype == 'unacknowledged') {
                $this->db->where('ev.ticket_id', '0');
            } else if ($acktype == 'acknowledged') {
                $this->db->where('ev.ticket_id !=', '0');
            }
        }
        /* end costom filter */

        $this->db->order_by('ev.event_start_time', 'desc');
        $query = $this->db->get();
        //echo $query->num_rows();
      // echo $this->db->last_query();
     //  exit;       
        if ($query->num_rows() > 0) {
            $newArray = array();            
            $newArray = $query->result();            
        } else {
            $newArray = array();
        }
//        pr($newArray);
//exit;
        return $newArray;
    }

    public function getClientFilter() {
        $accessLevelClient = helper_getAccessClientDropDown();
        $this->db->select('id,client_title');
        $this->db->from('client');
        $this->db->where_in('id', $accessLevelClient['resultSet']);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }
    
    public function getToolFilter() {        
        $this->db->select('gatewaytype_uuid as id,title');
        $this->db->from('gateway_types');        
        $query = $this->db->get();
        // echo $this->db->last_query();exit;       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }
    
   
    
    public function getClientFilter123() {
        pr($_SESSION);
        $accessLevelClient = helper_getAccessClientDropDown();
        pr($accessLevelClient);exit;
        $this->db->select('id,client_title');
        $this->db->from('client');
        $this->db->where_in('id', $accessLevelClient['resultSet']);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    /* End Event History Moule */

    public function getEventHistorySearch($severity = "") {
        $domain = helper_getDomain();
        //$skill = helper_getSkill();
        $accessLevelClient = helper_getAccessLevelClient();
        $this->db->select('ev.event_id,ev.event_text,SUBSTR(event_text, 1, 30) as event_text_small,ev.event_start_time,ev.event_end_time,ev.event_created_time,ev.event_description,ev.client_id,clnt.client_title,devi.device_name,serv.service_description as service_name,severity.severity,group.title as domain_name,eventsta.event_state,(case when (ev.event_end_time = "0000-00-00 00:00:00") THEN TIMEDIFF(NOW(), ev.event_start_time) ELSE TIMEDIFF(ev.event_end_time, ev.event_start_time) END) as Duration,severity.color_code');
        $this->db->from('events as ev');
        $this->db->join('client as clnt', 'clnt.id = ev.client_id');
        $this->db->join('devices as devi', 'devi.device_id = ev.device_id');
        $this->db->join('services as serv', 'serv.service_id = ev.service_id');
        $this->db->join('severities as severity', 'severity.id = ev.severity_id');
        $this->db->join('group as group', 'group.id = ev.domain_id');
        $this->db->join('event_states eventsta', 'eventsta.id = ev.event_state_id');
        if ($domain['status'] == 'true') {
            $this->db->where_in('ev.domain_id', $domain['resultSet']);
        }
        if ($accessLevelClient['status'] == 'true') {
            $this->db->where_in('ev.client_id', $accessLevelClient['resultSet']);
        }



        /* start costom filter */


        if (isset($severity)) {
            $this->db->where_in('ev.severity_id', $severity);
        }



        $this->db->order_by('ev.event_start_time', 'desc');
        $query = $this->db->get();

//        echo $this->db->last_query();
//        exit;
        if ($query->num_rows() > 0) {
            $newArray = array();
            // pr($query->result());
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getEventSuppress() {
        /* is_suppress is Y and event should be active  ack_notes*/
        $accessLevelClient = helper_getAccessLevelClient();
        $this->db->select("event_id,(SELECT ack_notes FROM `eventedge_event_acknowledgements` WHERE event_id=eventedge_events.event_id ORDER by ack_id DESC LIMIT 1) as ack_notes,(SELECT ack_type FROM `eventedge_event_acknowledgements` WHERE event_id=eventedge_events.event_id ORDER by ack_id DESC LIMIT 1) as ack_type,(SELECT ack_id FROM `eventedge_event_acknowledgements` WHERE event_id=eventedge_events.event_id ORDER by ack_id DESC LIMIT 1) as ack_id,(SELECT event_suppress_starttime FROM `eventedge_event_acknowledgements` WHERE event_id=eventedge_events.event_id ORDER by ack_id DESC LIMIT 1) as suppress_starttime,(SELECT event_suppress_endtime FROM `eventedge_event_acknowledgements` WHERE event_id=eventedge_events.event_id ORDER by ack_id DESC LIMIT 1) as suppress_endtime,(SELECT CONCAT(FLOOR(HOUR(SEC_TO_TIME(duration)) / 24), 'Day:',MOD(HOUR(SEC_TO_TIME(duration)), 24), 'H:',MINUTE(SEC_TO_TIME(duration)), 'Min:', second(SEC_TO_TIME(duration)), 'Sec') FROM `eventedge_event_acknowledgements` WHERE event_id=eventedge_events.event_id ORDER by ack_id DESC LIMIT 1) as duration");
        $this->db->from('events');
        $this->db->where('is_suppressed', 'Y');
        $this->db->where('event_end_time', '0000-00-00 00:00:00');
        if ($accessLevelClient['status'] == 'true') {
            $this->db->where_in('client_id', $accessLevelClient['resultSet']);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $newArray = array();
            //pr($query->result());exit;
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getEventAck($eventId = "") {
        $this->db->select("CONCAT(usr.first_name, '', usr.last_name) AS name,ack.created_on,ack.ack_notes,ack.event_suppress_starttime,ack.event_suppress_endtime,my_sec_to_time(ack.duration) as duration,(case when (ack.ack_type = 'T')  THEN 'Temporary' ELSE  'Permanent' END) as ack_type");
        $this->db->from('event_acknowledgements as ack');
        $this->db->join('client as clnt', 'clnt.id = ack.client_id');
        $this->db->join('users as usr', 'usr.id = ack.created_by');
        $this->db->where('ack.is_history', 'N');
        $this->db->where('ack.event_id', $eventId);
        $this->db->order_by('ack.created_on', 'desc');
        $query = $this->db->get();
       // echo $this->db->last_query();exit;       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
  
    
}

?>