<?php

class Auto_assign_model extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'temp_role';
        $this->client = 'client';
        $this->users = 'users';
        $this->events = 'events';
        $this->tickets = 'tickets';
        $this->ticket_status = 'ticket_status';
        $this->ticket_queue = 'ticket_queue';
        $this->ticket_activity = 'ticket_activity';
        $this->activity_type = 'activity_type';
        $this->incident_activity_log = 'incident_activity_log';
        $this->ssh_log = 'ssh_log';       
        $current_date = $this->config->item('datetime');		
    }
	
	public function get_login_users()
	{
		$query = $this->db->query("SELECT u.id, concat_ws('  ',u.first_name,  u.last_name) as username, u.client, u.skill,
					(SELECT COUNT(t.assigned_to) FROM eventedge_tickets t WHERE t.assigned_to = u.id AND t.status_code != 7 AND t.status_code != 5) as incidents_count,
					(SELECT GROUP_CONCAT(t.ticket_id) FROM eventedge_tickets t WHERE t.assigned_to = u.id AND t.status_code != 7 AND t.status_code != 5) as ticket_ids,
					(SELECT GROUP_CONCAT(s.client_id) FROM eventedge_users_subclient_manage s WHERE s.user_id = u.id) as subclient_ids
					FROM eventedge_users u WHERE u.is_login = 'Y'");
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function getAllIncidents(){
		
        $this->db->select('ticket_id,priority,client_id');
        $this->db->from('tickets');
		$this->db->where('assigned_to', 0);
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getLoginCurrentUsersCount($uid)
	{
		$query = $this->db->query("SELECT COUNT(t.assigned_to) as current_work_incidents FROM eventedge_tickets t, eventedge_users u WHERE t.assigned_to = u.id AND t.status_code != 7 AND t.status_code != 5 AND u.is_login = 'Y' AND u.id = {$uid}");
		if($query->num_rows() > 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function updateTicketDetails($postData = "", $ticket_id) {
		$this->db->where('ticket_id', $ticket_id);
        $update_status = $this->db->update($this->tickets, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }


}

?>