<style>
h3.sub-head{border-bottom: 1px solid #3ccc; background:#f9f9f9; line-height: 14px; margin:0;}
h3.sub-head span{font-size: 13px; font-weight: 600; background: #3ccc; padding: 6px 12px; color: #fff;display: inline-block; }
.portlet-body .form-group.form-md-line-input.form-md-floating-label .form-control~label{font-size: 13px;}
.form-group.form-md-line-input.form-md-floating-label .form-control.edited~label, .form-group.form-md-line-input.form-md-floating-label .form-control.focus:not([readonly])~label, .form-group.form-md-line-input.form-md-floating-label .form-control.form-control-static~label, .form-group.form-md-line-input.form-md-floating-label .form-control:focus:not([readonly])~label, .form-group.form-md-line-input.form-md-floating-label .form-control[readonly]~label{top: 6px;}
.portlet-body .form-group.form-md-line-input{margin: 0 0 4px;padding-top: 15px;}
.add_field_button, .remove_field{font-size: 12px;  padding: 5px;}
.client-panel-heading{padding:0;background-color: transparent; border:0;}
.client-panel-heading .panel-body, .custom-panel .panel-footer{padding:5px;}
.custom-panel{border-top:0;}
.custom-panel .panel-body{padding:0;}
.custom-panel hr{margin: 11px 0 3px 0;}
.custom-panel .portlet-body.pad0{padding-top:0;}
.custom-panel .accordion-toggle i{font-size:13px;position: relative; top: 6px; right: 10px;}
.input_fields_wrap .countdivs:nth-child(event){background:#f6f6f6;}
.input_fields_wrap .countdivs:nth-child(odd){background:#f9f9f9;}
</style>
<!-- END PAGE HEADER-->
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Client</span>
				<span style="color:black;"><?php if($clientName['resultSet']['client_title']){
						echo " (".$clientName['resultSet']['client_title'].")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmClient" id="frmClient" method="post" action="">
                    <div class="form-body">
                        <div class="row"> 
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_title" name="client_title" type="text" data-validation="required" data-validation-error-msg="Please enter a client name">
                                    <label for="form_control_1">Client Name</label>                                             
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_display_name" name="client_display_name" type="text" data-validation="required" data-validation-error-msg="Please enter a client display name">
                                    <label for="form_control_1">Client Display Name</label>                                               
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_erp_number" name="client_erp_number" type="text">
                                    <label for="form_control_1">Client ERP Number</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_crm_number" name="client_crm_number" type="text">
                                    <label for="form_control_1">Client CRM Number</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_url" name="client_url" type="text">
                                    <label for="form_control_1">Client Url</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_credit_limit" name="client_credit_limit" type="text">
                                    <label for="form_control_1">Client Credit Limit</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_discount" name="client_discount" type="text">
                                    <label for="form_control_1">Client Discount</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_domain" name="client_domain" type="text">
                                    <label for="form_control_1">Client Domain</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_salesperson_code" name="client_salesperson_code" type="text">
                                    <label for="form_control_1">Client Salesperson Code</label>                                               
                                </div>
                            </div>
							
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_ssr" name="client_ssr" type="text">
                                    <label for="form_control_1">Client SSR</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_tax_exemption_no" name="client_tax_exemption_no" type="text">
                                    <label for="form_control_1">Client TAX Exemption No</label>                                               
                                </div>
                            </div>
						
							<div class="col-sm-3">

								<div id="div_id_stock_1_service">										
									<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
									<div class="controls has-success">
									<div class="radio">
										<label class="mt-checkbox mt-checkbox-outline">
										<?php if($rolesCnt >0){$che= "checked";}else{$che= "disabled";} ?>
										<input type="checkbox" <?php echo $che;?> name ="intialize" value="1" > Initialize Default Roles
										<span></span>
										</label>
									</div> 
									</div>
								</div>								
							</div>

                    </div>						
							<!--<div class="col-sm-3">
								<label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
								<input type="checkbox" name ="is_subclients" value="1" > No Subclients
								<span></span>
								</label>
							</div>-->


						<div class="panel-group" id="accordion">
							<div class="panel panel-default custom-panel">
								<div class="panel-heading client-panel-heading">
									<h3 class="sub-head">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
											<span>Address</span> <i class="more-less glyphicon glyphicon-minus pull-right"></i>
										</a>
									</h3>
								</div>
								<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
									<div class="panel-body">
										<div class="portlet-body pad0">                                
											<div class="input_fields_wrap">

												<div  class="countdivs">
													<div class="fieldRow clearfix">
													
													   <div class="col-md-3">
															 <div class="form-group form-md-line-input form-md-floating-label">
															
															 <input type="text" name="city[]"  data-validation="required" data-validation-error-msg="Please enter a City" class="textinput form-control city" />
															  <label for="form_control_1">City</label>
															 </div>

													   </div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
															 <input type="text" name="district[]"  class="textinput form-control district" data-validation="required" data-validation-error-msg="Please enter a District"/>
															 <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField"></span></label>
															 </div>
															 
														  </div>
													   </div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															
															 <div class="form-group form-md-line-input form-md-floating-label">
															 
															 <input type="text" name="state[]"  class="textinput form-control state" data-validation="required" data-validation-error-msg="Please enter a state"/>
															  <label for="id_stock_1_product" class="control-label requiredField">state<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>	
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
															 <input type="text" name="country[]"  class="textinput form-control country" data-validation="required" data-validation-error-msg="Please enter a country"/>
															 <label for="id_stock_1_product" class="control-label">country<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
															 <input type="text" name="postal_code[]"  class="textinput form-control postal_code" data-validation="required" data-validation-error-msg="Please enter a postal code"/>
															 <label for="id_stock_1_product" class="control-label requiredField">postal_code<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>
														<div class="col-md-3">
														  <div id="div_id_stock_1_service">
														
															 <div class="form-group form-md-line-input form-md-floating-label">
															 <input type="text" name="house_no[]"  class="textinput form-control house_no" data-validation="required" data-validation-error-msg="Please enter a house no"/>
																 <label for="id_stock_1_product" class="control-label requiredField">house_no<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>

													   <div class="col-md-3">
														  <div id="div_id_stock_1_unit">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
																<select class="select form-control address_type edited" id="id_stock_1_unit" name="address_type[]" data-validation="required" data-validation-error-msg="Please select a address type">
																   <option value="" >office</option>
																   <option value="1">home</option>
																</select>
																<label for="id_stock_1_unit" class="control-label requiredField">address_type<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>								   
														<div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
															 
															 <input type="text" name="street[]"  class="textinput form-control street" data-validation="required" data-validation-error-msg="Please enter a street"/>
															 <label for="id_stock_1_product" class="control-label requiredField">street<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>	
														<div class="clearfix"></div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															
															 <div class="form-group form-md-line-input form-md-floating-label">
															 <textarea class="form-control full_address" rows="2" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter a full address"></textarea>
															  <label for="id_stock_1_product" class="control-label requiredField">full_address<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
															 
															 <input type="text" name="logitude[]"  class="textinput form-control logitude" data-validation="required" data-validation-error-msg="Please enter a logitude"/>
															 <label for="id_stock_1_product" class="control-label requiredField">logitude<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label"><input type="text" name="latitude[]"  class="textinput form-control latitude" data-validation="required" data-validation-error-msg="Please enter a latitude"/>
															 <label for="id_stock_1_product" class="control-label requiredField">latitude<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>
													   <div class="col-md-2">
														  <div id="div_id_stock_1_service">										
															<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
															<div class="controls ">
															<div class="radio">
																<label><input type="radio" class="is_primary" name="is_primary" value="0" data-validation="required" data-validation-error-msg="Please select a primary address" />Is Primary</label>
															</div> 
															 </div>
														  </div>
													   </div>

													</div>
												
												</div>	
								
						</div>
										</div> 
									</div>
								<div class="panel-footer">
									<button class="add_field_button btn btn-success mt-repeater-add pull-right">Add More Address</button><div class="clearfix"></div>
								</div>
								</div>
							</div>
						</div>

                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
		
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>   
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAXkP7Z7L54odTOQJshVFVvQSt-FkAd77I&sensor=false&libraries=places"></script>  
 <script type="text/javascript">
   		var i=0;
		google.maps.event.addDomListener(window, 'load', function () {
			$('.locationtext').each(function(){
				//alert($(this).attr('name'));
			var x = $(this).attr('name');
			var places = new google.maps.places.Autocomplete(this);
			google.maps.event.addListener(places, 'place_changed', function () {
				var yy = x.replace("[text-input]", "");
				alert(yy);
				var place = places.getPlace();
				//var name = place.name;
				//var name = $(this).closest('.mt-repeater-item').find('.nameval').val();
				
				//alert(name);
												
			});
			});			
		}); 
	</script>
    <script type="text/javascript">

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = $('.countdivs').length; //initlal text box count
	x = 0;
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
			//alert(x);
             //text box increment
        }
            $(wrapper).append('<div class="countdivs"> <hr/> <div class="fieldRow clearfix"> <div class="col-md-3"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="city[]" data-validation="required" data-validation-error-msg="Please enter a City" class="textinput form-control city" /> <label for="form_control_1">City</label> </div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="district[]" class="textinput form-control district" data-validation="required" data-validation-error-msg="Please enter a District"/> <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="state[]" class="textinput form-control state" data-validation="required" data-validation-error-msg="Please enter a state"/> <label for="id_stock_1_product" class="control-label requiredField">state<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="country[]" class="textinput form-control country" data-validation="required" data-validation-error-msg="Please enter a country"/> <label for="id_stock_1_product" class="control-label requiredField">country<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="postal_code[]" class="textinput form-control postal_code" data-validation="required" data-validation-error-msg="Please enter a postal code"/> <label for="id_stock_1_product" class="control-label requiredField">postal_code<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="house_no[]" class="textinput form-control house_no" data-validation="required" data-validation-error-msg="Please enter a house no"/> <label for="id_stock_1_product" class="control-label requiredField">house_no<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_unit"> <div class="form-group form-md-line-input form-md-floating-label"> <select class="select form-control address_type" id="id_stock_1_unit" name="address_type[]" data-validation="required" data-validation-error-msg="Please select a address type"> <option value="">office</option> <option value="1">home</option> </select> <label for="id_stock_1_unit" class="control-label requiredField">address_type<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="street[]" class="textinput form-control street" data-validation="required" data-validation-error-msg="Please enter a street"/> <label for="id_stock_1_product" class="control-label requiredField">street<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <textarea class="form-control full_address" rows="2" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter a full address"></textarea> <label for="id_stock_1_product" class="control-label requiredField">full_address<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="logitude[]" class="textinput form-control logitude" data-validation="required" data-validation-error-msg="Please enter a logitude"/> <label for="id_stock_1_product" class="control-label requiredField">logitude<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"><input type="text" name="latitude[]" class="textinput form-control latitude" data-validation="required" data-validation-error-msg="Please enter a latitude"/> <label for="id_stock_1_product" class="control-label requiredField">latitude<span class="asteriskField"></span></label> </div></div></div><div class="col-md-2"> <div id="div_id_stock_1_service"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "> <div class="radio"><label><input type="radio" class="is_primary" name="is_primary" value="'+x+'" data-validation="required" data-validation-error-msg="Please select a primary address">Is Primary</label></div></div></div></div><div class="col-md-1"> <div id="div_id_stock_1_service"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i>Remove</a> </div></div></div></div></div>'); //adding form
			x++;
			recall();
    });
     
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove button
	
        e.preventDefault(); $(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove(); x--;
		var y=0;
		$('.is_primary').each(function(i, obj) {
			alert(y);
			$(this).val(y);
			y++;
			//alert(val);

		});		
		//alert(x);
    })
});

</script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });

function recall()
{
	 $.validate();
}
$('#panel1').on('show.bs.collapse hidden.bs.collapse', function (e) {
	if (e.type == 'hidden') {
		$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
		$(window).bind('resize', function () {
			$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
			//alert('resized');
		});
	} else {
		$('.dataTables_scrollBody').css({'height': '210px'});
	}
	$(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');


});
</script>




