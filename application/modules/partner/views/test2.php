
<!-- END PAGE HEADER-->
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Client</span>
				<span style="color:black;"><?php if($clientName['resultSet']['client_title']){
						echo " (".$clientName['resultSet']['client_title'].")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmClient" id="frmClient" method="post" action="">
                    <div class="form-body">
                        <div class="row">                                                                            
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_title" name="client_title" type="text">
                                    <label for="form_control_1">Client Name</label>                                               
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_display_name" name="client_display_name" type="text">
                                    <label for="form_control_1">Client Display Name</label>                                               
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_title" name="client_title" type="text">
                                    <label for="form_control_1">Client Name</label>                                               
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_erp_number" name="client_erp_number" type="text">
                                    <label for="form_control_1">Client ERP Number</label>                                               
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_crm_number" name="client_crm_number" type="text">
                                    <label for="form_control_1">Client CRM Number</label>                                               
                                </div>
                            </div>                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_url" name="client_url" type="text">
                                    <label for="form_control_1">Client Url</label>                                               
                                </div>
                            </div>                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_credit_limit" name="client_credit_limit" type="text">
                                    <label for="form_control_1">Client Credit Limit</label>                                               
                                </div>
                            </div>                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_discount" name="client_discount" type="text">
                                    <label for="form_control_1">Client Discount</label>                                               
                                </div>
                            </div>                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_domain" name="client_domain" type="text">
                                    <label for="form_control_1">Client Domain</label>                                               
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_salesperson_code" name="client_salesperson_code" type="text">
                                    <label for="form_control_1">Client Salesperson Code</label>                                               
                                </div>
                            </div>
							
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_ssr" name="client_ssr" type="text">
                                    <label for="form_control_1">Client SSR</label>                                               
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_tax_exemption_no" name="client_tax_exemption_no" type="text">
                                    <label for="form_control_1">Client TAX Exemption No</label>                                               
                                </div>
                            </div>
						
							<div class="col-sm-3">
								<label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
								<?php if($rolesCnt >0){$che= "checked";}else{$che= "disabled";} ?>
								<input type="checkbox" <?php echo $che;?> name ="intialize" value="1" > Initialize Default Roles
								<span></span>
								</label>
							</div>

                    </div>						
							<!--<div class="col-sm-3">
								<label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
								<input type="checkbox" name ="is_subclients" value="1" > No Subclients
								<span></span>
								</label>
							</div>-->
						<div class="input_fields_wrap">
							<div>
							<button class="add_field_button btn btn-success mt-repeater-add" style="float:right">Add Address</button>
							</div>
							<div class="clearfix"></div>
								<div style="margin:10px;">
								<div class="fieldRow clearfix">
								
								   <div class="col-md-3">
									  <div id="div_id_stock_1_service" class="form-group">
										 <label for="id_stock_1_product" class="control-label requiredField">City<span class="asteriskField">*</span></label>
										 <div class="controls "><input type="text" name="city[]"  class="textinput form-control"/></div>
									  </div>
								   </div>
								   <div class="col-md-3">
									  <div id="div_id_stock_1_service" class="form-group">
										 <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField">*</span></label>
										 <div class="controls "><input type="text" name="district[]"  class="textinput form-control"/></div>
									  </div>
								   </div>
								   <div class="col-md-3">
									  <div id="div_id_stock_1_service" class="form-group">
										 <label for="id_stock_1_product" class="control-label requiredField">state<span class="asteriskField">*</span></label>
										 <div class="controls "><input type="text" name="state[]"  class="textinput form-control"/></div>
									  </div>
								   </div>	
								   <div class="col-md-3">
									  <div id="div_id_stock_1_service" class="form-group">
										 <label for="id_stock_1_product" class="control-label requiredField">country<span class="asteriskField">*</span></label>
										 <div class="controls "><input type="text" name="country[]"  class="textinput form-control"/></div>
									  </div>
								   </div>
								   <div class="col-md-3">
									  <div id="div_id_stock_1_service" class="form-group">
										 <label for="id_stock_1_product" class="control-label requiredField">postal_code<span class="asteriskField">*</span></label>
										 <div class="controls "><input type="text" name="postal_code[]"  class="textinput form-control"/></div>
									  </div>
								   </div>
									<div class="col-md-3">
									  <div id="div_id_stock_1_service" class="form-group">
										 <label for="id_stock_1_product" class="control-label requiredField">house_no<span class="asteriskField">*</span></label>
										 <div class="controls "><input type="text" name="house_no[]"  class="textinput form-control"/></div>
									  </div>
								   </div>

								   <div class="col-md-3">
									  <div id="div_id_stock_1_unit" class="form-group">
										 <label for="id_stock_1_unit" class="control-label requiredField">address_type<span class="asteriskField">*</span></label>
										 <div class="controls ">
											<select class="select form-control" id="id_stock_1_unit" name="address_type[]">
											   <option value="" selected="selected">office</option>
											   <option value="1">home</option>
											</select>
										 </div>
									  </div>
								   </div>								   
									<div class="col-md-3">
									  <div id="div_id_stock_1_service" class="form-group">
										 <label for="id_stock_1_product" class="control-label requiredField">street<span class="asteriskField">*</span></label>
										 <div class="controls "><input type="text" name="street[]"  class="textinput form-control"/></div>
									  </div>
								   </div>									   
								   <div class="col-md-3">
									  <div id="div_id_stock_1_service" class="form-group">
										 <label for="id_stock_1_product" class="control-label requiredField">full_address<span class="asteriskField">*</span></label>
										 <div class="controls ">
										 <textarea class="form-control" rows="5" name="full_address[]"></textarea>
										 </div>
									  </div>
								   </div>
								   <div class="col-md-3">
									  <div id="div_id_stock_1_service" class="form-group">
										 <label for="id_stock_1_product" class="control-label requiredField">logitude<span class="asteriskField">*</span></label>
										 <div class="controls "><input type="text" name="logitude[]"  class="textinput form-control"/></div>
									  </div>
								   </div>
								   <div class="col-md-3">
									  <div id="div_id_stock_1_service" class="form-group">
										 <label for="id_stock_1_product" class="control-label requiredField">latitude<span class="asteriskField">*</span></label>
										 <div class="controls "><input type="text" name="latitude[]"  class="textinput form-control"/></div>
									  </div>
								   </div>
								   <div class="col-md-2">
									  <div id="div_id_stock_1_service" class="form-group">										
										<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
										<div class="controls ">
										<div class="radio">
										<label><input type="radio" name="optradio">Is Default</label>
										</div> 
										 </div>
									  </div>
								   </div>
								   
								   <div class="col-md-1">
									  <div id="div_id_stock_1_service" class="form-group">										
										<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
										<div class="controls ">
											<a href="#" class="remove_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i>Remove</a>
										 </div>
									  </div>
								   </div>								   

								</div>
								
								</div>							
						</div>
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
		
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>

            <!-- END CORE PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/jquery-repeater/jquery.repeater.js" type="text/javascript"></script>
            <script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL SCRIPTS -->

            <script src="<?php echo base_url() . "public/" ?>assets/pages/scripts/form-repeater.js" type="text/javascript"></script>
            <script src="<?php echo base_url() . "public/" ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->



    
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAXkP7Z7L54odTOQJshVFVvQSt-FkAd77I&sensor=false&libraries=places"></script>   <script type="text/javascript">
   		var i=0;
		google.maps.event.addDomListener(window, 'load', function () {
			$('.locationtext').each(function(){
				//alert($(this).attr('name'));
			var x = $(this).attr('name');
			var places = new google.maps.places.Autocomplete(this);
			google.maps.event.addListener(places, 'place_changed', function () {
				var yy = x.replace("[text-input]", "");
				alert(yy);
				var place = places.getPlace();
				//var name = place.name;
				//var name = $(this).closest('.mt-repeater-item').find('.nameval').val();
				
				//alert(name);
												
			});
			});			
		}); 

		

	</script>
	
	<script src="<?php echo base_url() . "public/" ?>assets/pages/scripts/jquery.czMore-1.5.3.2.js" type="text/javascript"></script>
    <script type="text/javascript">
        //One-to-many relationship plugin by Yasir O. Atabani. Copyrights Reserved.
        $("#czContainer").czMore();
    </script>
    </div>
    <script type="text/javascript">

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
        }
            $(wrapper).append('<div style="margin:10px;"><div class="fieldRow clearfix"> <div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">City<span class="asteriskField">*</span></label> <div class="controls "><input type="text" name="city[]" class="textinput form-control"/></div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField">*</span></label> <div class="controls "><input type="text" name="district[]" class="textinput form-control"/></div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">state<span class="asteriskField">*</span></label> <div class="controls "><input type="text" name="state[]" class="textinput form-control"/></div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">country<span class="asteriskField">*</span></label> <div class="controls "><input type="text" name="country[]" class="textinput form-control"/></div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">postal_code<span class="asteriskField">*</span></label> <div class="controls "><input type="text" name="postal_code[]" class="textinput form-control"/></div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">house_no<span class="asteriskField">*</span></label> <div class="controls "><input type="text" name="house_no[]" class="textinput form-control"/></div></div></div><div class="col-md-3"> <div id="div_id_stock_1_unit" class="form-group"> <label for="id_stock_1_unit" class="control-label requiredField">address_type<span class="asteriskField">*</span></label> <div class="controls "><select class="select form-control" id="id_stock_1_unit" name="address_type[]"> <option value="" selected="selected">office</option> <option value="1">home</option></select> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">street<span class="asteriskField">*</span></label> <div class="controls "><input type="text" name="street[]" class="textinput form-control"/></div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">full_address<span class="asteriskField">*</span></label> <div class="controls "> <textarea class="form-control" rows="5" name="full_address[]"></textarea> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">logitude<span class="asteriskField">*</span></label> <div class="controls "><input type="text" name="logitude[]" class="textinput form-control"/></div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">latitude<span class="asteriskField">*</span></label> <div class="controls "><input type="text" name="latitude[]" class="textinput form-control"/></div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service" class="form-group"> <div class="controls "><div class="radio"><label><input type="radio" name="optradio">Is Default</label></div></div></div></div></div><a href="#" class="remove_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i>Remove</a></div>'); //add input box
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').parent('div').parent('div').remove(); x--;
    })
});

</script>




