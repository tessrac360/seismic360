<!-- END PAGE HEADER-->
<?php 
//pr($address_types);exit;

$address_types_select = '<select class="select form-control address_type edited" id="id_stock_1_unit" name="address_type[]" data-validation="required" data-validation-error-msg="Please select address type">';
if ($address_types['status'] == 'true') {
	foreach ($address_types['resultSet'] as $value) {
		
	   $address_types_select.='<option value="'.$value['address_type_uuid'].'">'.$value['address_type'].'</option>';  

	}
}				   
$address_types_select.='</select>';
//echo $address_types_select;exit;	
?>
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Partner</span>
				<span style="color:black;"><?php if($clientName['resultSet']['client_title']){
						echo " (".$clientName['resultSet']['client_title'].")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('client') ?>">Partner</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmClient" id="frmClient" method="post" action="">
                    <div class="form-body">
                        <div class="row"> 
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_title" name="client_title" type="text" data-validation="required" data-validation-error-msg="Please enter a client name">
                                    <label for="form_control_1">Partner Name</label>                                             
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_display_name" name="client_display_name" type="text" data-validation="required" data-validation-error-msg="Please enter a client display name">
                                    <label for="form_control_1">Partner Display Name</label>                                               
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_erp_number" name="client_erp_number" type="text">
                                    <label for="form_control_1">Partner ERP Number</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_crm_number" name="client_crm_number" type="text">
                                    <label for="form_control_1">Partner CRM Number</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_url" name="client_url" type="text">
                                    <label for="form_control_1">Partner Url</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_credit_limit" name="client_credit_limit" type="text">
                                    <label for="form_control_1">Partner Credit Limit</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_discount" name="client_discount" type="text">
                                    <label for="form_control_1">Partner Discount</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_domain" name="client_domain" type="text">
                                    <label for="form_control_1">Partner Domain</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_salesperson_code" name="client_salesperson_code" type="text">
                                    <label for="form_control_1">Partner Salesperson Code</label>                                               
                                </div>
                            </div>
							
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_ssr" name="client_ssr" type="text">
                                    <label for="form_control_1">Partner SSR</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_tax_exemption_no" name="client_tax_exemption_no" type="text">
                                    <label for="form_control_1">Partner TAX Exemption No</label>                                               
                                </div>
                            </div>
						
						<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="timezone_id" name="timezone_id" data-validation="required" data-validation-error-msg="Please select timezone">
										<option value=""></option>
                                        <?php
                                        if ($timezones['status'] == 'true') {
                                            foreach ($timezones['resultSet'] as $value) {
												if($value['timezone'] !='NA'){
                                                ?>
                                               <option value="<?php echo $value['id']; ?>"><?php echo $value['timezone']; ?></option>  
                                                <?php
												}
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Timezone</label>
                                </div>
                            </div>
						
							<div class="col-sm-3">
 <input type="hidden"  name ="intialize" value="1" >
<!--								<div id="div_id_stock_1_service">										
									<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
									<div class="controls has-success">
									<div class="radio">
										<label class="mt-checkbox mt-checkbox-outline">
										<?php //if($rolesCnt >0){$che= "checked";}else{$che= "disabled";} ?>
                                                                                   
										<span></span>
										</label>
									</div> 
									</div>
								</div>								-->
							</div>

                    </div>						
							<!--<div class="col-sm-3">
								<label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
								<input type="checkbox" name ="is_subclients" value="1" > No Subclients
								<span></span>
								</label>
							</div>-->


						<div class="panel-group" id="accordion">
							<div class="panel panel-default custom-panel">
								<div class="panel-heading client-panel-heading">
									<h3 class="sub-head">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
											<span>Address</span> <i class="more-less glyphicon glyphicon-minus pull-right"></i>
										</a>
									</h3>
								</div>
								<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
									<div class="panel-body">
										<div class="portlet-body pad0">                                
											<div class="input_fields_wrap wrapping">

												<div  class="countdivs">
													<div class="fieldRow clearfix">
													
													   <div class="col-md-3">
															 <div class="form-group form-md-line-input form-md-floating-label">
															
															 <input type="text" name="city[]" class="textinput form-control city" />
															  <label for="form_control_1">City</label>
															 </div>

													   </div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
															 <input type="text" name="district[]" class="textinput form-control district" />
															 <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField"></span></label>
															 </div>
															 
														  </div>
													   </div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															
															 <div class="form-group form-md-line-input form-md-floating-label">
															 
															 <input type="text" name="state[]"  class="textinput form-control state"/>
															  <label for="id_stock_1_product" class="control-label requiredField">State<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>	
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
															 <input type="text" name="country[]"  class="textinput form-control country"/>
															 <label for="id_stock_1_product" class="control-label">Country<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
															 <input type="text" name="postal_code[]"  class="textinput form-control postal_code" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values"/>
															 <label for="id_stock_1_product" class="control-label requiredField">Postal Code<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>
														<div class="col-md-3">
														  <div id="div_id_stock_1_service">
														
															 <div class="form-group form-md-line-input form-md-floating-label">
															 <input type="text" name="house_no[]"  class="textinput form-control house_no"/>
																 <label for="id_stock_1_product" class="control-label requiredField">House Number<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>

													   <div class="col-md-3">
														  <div id="div_id_stock_1_unit">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
																<?php echo $address_types_select;?>
																<label for="id_stock_1_unit" class="control-label requiredField">Address Type<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>								   
														<div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
															 
															 <input type="text" name="street[]"  class="textinput form-control street"/>
															 <label for="id_stock_1_product" class="control-label requiredField">Street<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>	
														<div class="clearfix"></div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															
															 <div class="form-group form-md-line-input form-md-floating-label">
															 <textarea class="form-control full_address" rows="2" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter full address"></textarea>
															  <label for="id_stock_1_product" class="control-label requiredField">Full Address<span class="asteriskField"></span></label>
															 </div>
														  </div>
													   </div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label">
															 
															 <input type="text" name="logitude[]"  class="textinput form-control logitude" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values" />
															 <label for="id_stock_1_product" class="control-label requiredField" >Longitude</label>
															 </div>
														  </div>
													   </div>
													   <div class="col-md-3">
														  <div id="div_id_stock_1_service">
															 
															 <div class="form-group form-md-line-input form-md-floating-label"><input type="text" name="latitude[]"  class="textinput form-control latitude" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values" />
															 <label for="id_stock_1_product" class="control-label requiredField">Latitude</label>
															 </div>
														  </div>
													   </div>
													   <div class="col-md-2">
														  <div id="div_id_stock_1_service">										
															<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
															<div class="controls ">
															<div class="radio">
																<label><input type="radio" class="is_primary" name="is_primary" value="0" data-validation="required" data-validation-error-msg="Please select primary address" />Is Primary</label>
															</div> 
															 </div>
														  </div>
													   </div>

													</div>
												
												</div>	
								
						</div>
										</div> 
									</div>
								<!--<div class="panel-footer">
									<button class="add_field_button btn blue btn-outline mt-repeater-add pull-right">Add More Address</button><div class="clearfix"></div>
								</div>-->
								</div>
							</div>
						</div>

                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('partner'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
		
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>   
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAXkP7Z7L54odTOQJshVFVvQSt-FkAd77I&sensor=false&libraries=places"></script>  
 <script type="text/javascript">
   		var i=0;
		google.maps.event.addDomListener(window, 'load', function () {
			$('.locationtext').each(function(){
				//alert($(this).attr('name'));
			var x = $(this).attr('name');
			var places = new google.maps.places.Autocomplete(this);
			google.maps.event.addListener(places, 'place_changed', function () {
				var yy = x.replace("[text-input]", "");
				alert(yy);
				var place = places.getPlace();
				//var name = place.name;
				//var name = $(this).closest('.mt-repeater-item').find('.nameval').val();
				
				//alert(name);
												
			});
			});			
		}); 
	</script>
    <script type="text/javascript">

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
	var address_types_select = '<?php echo $address_types_select;?>';
    
    var x = $('.countdivs').length; //initlal text box count
	x = 0;
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
			//alert(x);
             //text box increment
        }
            $(wrapper).append('<div class="countdivs"> <hr/> <div class="fieldRow clearfix"> <div class="col-md-3"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="city[]" data-validation="required" data-validation-error-msg="Please enter city" class="textinput form-control city"/> <label for="form_control_1">City</label> </div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="district[]" class="textinput form-control district" data-validation="required" data-validation-error-msg="Please enter district"/> <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="state[]" class="textinput form-control state" data-validation="required" data-validation-error-msg="Please enter state"/> <label for="id_stock_1_product" class="control-label requiredField">State<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="country[]" class="textinput form-control country" data-validation="required" data-validation-error-msg="Please enter acountry"/> <label for="id_stock_1_product" class="control-label requiredField">Country<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="postal_code[]" class="textinput form-control postal_code" data-validation="required" data-validation-error-msg="Please enter postal code"/> <label for="id_stock_1_product" class="control-label requiredField">Postal Code<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="house_no[]" class="textinput form-control house_no" data-validation="required" data-validation-error-msg="Please enter house number"/> <label for="id_stock_1_product" class="control-label requiredField">House Number<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_unit"> <div class="form-group form-md-line-input form-md-floating-label"> '+address_types_select+' <label for="id_stock_1_unit" class="control-label requiredField">Address Type<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="street[]" class="textinput form-control street" data-validation="required" data-validation-error-msg="Please enter street"/> <label for="id_stock_1_product" class="control-label requiredField">Street<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <textarea class="form-control full_address" rows="2" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter full address"></textarea> <label for="id_stock_1_product" class="control-label requiredField">Full Address<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="logitude[]" class="textinput form-control logitude"/> <label for="id_stock_1_product" class="control-label requiredField">Longitude</label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"><input type="text" name="latitude[]" class="textinput form-control latitude"/> <label for="id_stock_1_product" class="control-label requiredField">Latitude</label> </div></div></div><div class="col-md-2"> <div id="div_id_stock_1_service"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "> <div class="radio"><label><input type="radio" class="is_primary" name="is_primary" value="'+x+'" data-validation="required" data-validation-error-msg="Please select primary address">Is Primary</label></div></div></div></div><div class="col-md-1"> <div id="div_id_stock_1_service"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i> Remove</a> </div></div></div></div></div>'); //adding form
			x++;
			recall();
    });
     
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove button
	
        e.preventDefault(); $(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove(); x--;
		var y=0;
		$('.is_primary').each(function(i, obj) {
			alert(y);
			$(this).val(y);
			y++;
			//alert(val);

		});		
		//alert(x);
    })
});

</script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });

function recall()
{
	 $.validate();
}
$('#panel1').on('show.bs.collapse hidden.bs.collapse', function (e) {
	if (e.type == 'hidden') {
		$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
		$(window).bind('resize', function () {
			$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
			//alert('resized');
		});
	} else {
		$('.dataTables_scrollBody').css({'height': '210px'});
	}
	$(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');


});
</script>




