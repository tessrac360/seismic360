<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Edit Column</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Edit</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">                
                    <div class="form-body">
 						<?php
						if (!empty($rename_columns)) {
							for ($i = 0; $i < count($rename_columns); $i++) {
						//pr($rename_columns);
						?>		  
						<div class="row"> 				
							<div class="col-md-8">
								<div class="form-group form-md-line-input form-md-floating-label">
									<input class="form-control" id="column_name<?php echo $rename_columns[$i]['rename_id']; ?>" name="column_name" type="text" value="<?php echo $rename_columns[$i]['rename_column']; ?>">
									<label for="form_control_1">Column Name</label> 
								</div>
							</div>
							<div class="col-md-4">
								<button type="button" id="column_value" myval ="<?php echo $rename_columns[$i]['rename_id']; ?>" class="btn btn-sm btn-circle green column_value">Save</button>
							</div>
						</div>
							<?php } ?>	
						<?php }else{ ?>	
						<tr>
                            <td colspan="9"> No Records Found </td>
                        </tr> 
						<?php } ?>
					</div>
                
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_column.js" type="text/javascript"></script>