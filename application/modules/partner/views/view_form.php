<?php
error_reporting(0);
$accessAdd = helper_fetchPermission('15', 'add');
$accessEdit = helper_fetchPermission('15', 'edit');
$accessStatus = helper_fetchPermission('15', 'active');
$accessDelete = helper_fetchPermission('15', 'delete');
?>
<style>

</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('.tree').treegrid();
        //  $('.tree').treegrid('collapseAll');
    });
</script>
<link rel="stylesheet" href="<?php echo base_url() . "public/" ?>css/jquery.treegrid.css">
<!-- END PAGE HEADER-->

<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Partner</span>
            </div>
            <div class="page-toolbar">
                <ul class="page-breadcrumb breadcrumb custom-bread">
                    <li>
                        <i class="fa fa-cog"></i>
                        <span>Managements</span>  
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo base_url('partner') ?>">Partner</a>                            
                        <i class="fa fa-angle-right"></i>
                    </li>						
                    <li>
                        <span>List</span>                            
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="">






            <div class="full-height-content-body">
                <div class="row">
                    <div class="col-md-12 parent padr0">
                        <div class="row table-scroll scroller portlet light"  style="height: 338px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                            <?php if ($subclients['resultSet']->is_subclients == 0) { ?>
                                <div class="portlet-body padd-top0">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <?php
                                                if ($accessAdd == 'Y') {
                                                    ?> 
                                                    <div class="btn-group">
                                                        <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('partner/create') ?>"> Add New Partner
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="btn-group pull-right">
                                                    <div class="input-group">
                                                        <input placeholder="Search...." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                                        <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                                        <span class="input-group-btn">
                                                            <button class="btn blue" id="btn_datatableSearch" type="button">Search</button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <table class="table table-striped table-bordered table-hover tree" id="role">
                                        <thead>
                                            <tr>
                                                <th> Client Name </th>
                                                <th> Created By </th>
                                                <?php if ($accessStatus == 'Y') { ?> 
                                                    <th style="text-align: center;"> Status </th><?php } ?>
                                                <?php if ($accessEdit == 'Y') { ?> 
                                                    <th> Action </th>  <?php } ?>
                                                <th style="width:20%;"> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($client)) {
                                                for ($i = 0; $i < count($client); $i++) {
                                                    // pr($client);
                                                    ?>
                                                    <tr class="treegrid-<?php echo $client[$i]['id']; ?>
                                                    <?php
                                                    if ($client_id != 1) {
                                                        if ($i > 0) {
                                                            ?>
                                                                treegrid-parent-<?php
                                                                echo $client[$i]['parent_client_id'];
                                                            }
                                                        } else {
                                                            ?>
                                                            <?php if ($client_id != $client[$i]['parent_client_id']) { ?>
                                                                treegrid-parent-<?php
                                                                echo $client[$i]['parent_client_id'];
                                                            }
                                                            ?>

                                                        <?php } ?>
                                                        " 
                                                        >
                                                        <td><?php echo $client[$i]['client_title'] ?></td><td><?php echo $client[$i]['name'] ?></td>
                                                        <?php if ($accessStatus == 'Y') {
                                                            ?>


                                                            <td align="center">

                                                                <?php if ($client[$i]['id'] != 1) { ?>
                                                                    <?php if ($i != 0) { ?>
                                                                        <div class="status_active_<?php echo $client[$i]['id']; ?>">
                                                                            <?php if ($client[$i]['status'] == 'Y') { ?>
                                                                                <a title="Active"  href="javascript:void(0);" class="client_status" myval="<?php echo $client[$i]['id']; ?>" status="N"> <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i> </a>
                                                                            <?php } else { ?>
                                                                                <a title="De-activate"  href="javascript:void(0);" class="client_status" myval="<?php echo $client[$i]['id']; ?>" status="Y"> <i class="fa fa-times-circle-o fa-lg" aria-hidden="true"></i> </a>
                                                                            <?php } ?> 
                                                                        </div>


                                                                    <?php } elseif ($client_id == 1) { ?>

                                                                        <div class="status_active_<?php echo $client[$i]['id']; ?>">
                                                                            <?php if ($client[$i]['status'] == 'Y') { ?>
                                                                                <a title="Active" href="javascript:void(0);" class="client_status" myval="<?php echo $client[$i]['id']; ?>" status="N"> <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i> </a>
                                                                            <?php } else { ?>
                                                                                <a title="De-activate"  href="javascript:void(0);" class="client_status" myval="<?php echo $client[$i]['id']; ?>" status="Y"> <i class="fa fa-times-circle-o fa-lg" aria-hidden="true"></i> </a>
                                                                            <?php } ?> 
                                                                        </div>

                                                                        <?php
                                                                    }
                                                                }
                                                                ?>


                                                            </td>
                                                        <?php } ?>


                                                        <?php if ($accessEdit == 'Y') { ?> 
                                                            <td>
                                                                <?php
                                                                if ($client[$i]['id'] != 1) {
                                                                    if ($i != 0) {
                                                                        ?>
                                                                        <a title="Edit" href="<?php echo base_url() . 'partner/edit/' . encode_url($client[$i]['id']); ?>" class="btn btn-xs  blue">
                                                                            <i class="fa fa-edit"></i>
                                                                        </a>
                                                                        <?php if ($client[$i]['is_delete_manually'] <= date("Y-m-d") && $client[$i]['is_delete_manually'] != '0000-00-00') { ?>
                                                                            <a title="Delete" href="javascript:void(0);"  clientId="<?php echo encode_url($client[$i]['id']); ?>" class="btn btn-xs red isdelete hideClientDelete_<?php echo $client[$i]['id']; ?>">
                                                                                <i class="fa fa-trash"></i>
                                                                            </a>
                                                                        <?php } ?>

                                                                    <?php } elseif ($client_id == 1) { ?>
                                                                        <a title="Edit" href="<?php echo base_url() . 'partner/edit/' . encode_url($client[$i]['id']); ?>" class="btn btn-xs  blue">
                                                                            <i class="fa fa-edit"></i>
                                                                        </a>	
                                                                        <?php if ($client[$i]['is_delete_manually'] <= date("Y-m-d") && $client[$i]['is_delete_manually'] != '0000-00-00') { ?>
                                                                            <a title="Delete" href="javascript:void(0);"  clientId="<?php echo encode_url($client[$i]['id']); ?>" class="btn btn-xs red isdelete hideClientDelete_<?php echo $client[$i]['id']; ?>">
                                                                                <i class="fa fa-trash"></i>
                                                                            </a>
                                                                        <?php } ?>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </td>
                                                        <?php } ?>
                                                        <td>
                                                            <?php if ($client[$i]['id'] != 1) { ?>
                                                                <a class="btn btn-xs  blue"  data-toggle="tooltip" title="Add Sub Partner" href="<?php echo base_url() . 'partner/create/?cli=' . encode_url($client[$i]['id']) . '&lvl=' . encode_url($client[$i]['level']); ?>">
                                                                    <i class="fa fa-user-plus" aria-hidden="true" ></i>
                                                                </a>                                                                
                                                            <?php } ?>													
                                                            <button class="btn btn-xs  blue rightbar" data-toggle="tooltip" title="Subclients Bulk Upload" ref="<?php echo encode_url($client[$i]['id']); ?>" clientName="<?php echo $client[$i]['client_title'] ?>" client_uuid="<?php echo $client[$i]['client_uuid'] ?>" controlClient ="<?php echo $client_id; ?>" con="hidevalue">
                                                                More Actions
                                                            </button>												
                                                        </td>										
                                                    </tr>		
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="4"> No Record Found </td>
                                                </tr> 
                                            <?php } ?>
                                        <tbody>
                                    </table>



                                </div>
                            <?php } else { ?>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-12">
                                                Subclients Restricted.
                                            </div>
                                        </div>
                                    </div>
                                </div>						
                            <?php } ?>
                            <div class="box-footer clearfix">
                                <ul class="pagination pagination-sm no-margin pull-right">

                                    <?php
                                    if ($search == '') {
                                        echo $this->pagination->create_links();
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="right_block col-md-2 col-sm-3 padl0" style="display:none;">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase font-dark" id= "ClientName">Actions</span>
                                </div>

                                <div class="actions">
                                    <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                        <i class="close fa fa-close"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="portlet-body client-btn-scroll scroller" style="height: 338px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2" id ="actiontabs" >

                            </div>

                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<style type="text/css">

</style>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.treegrid.js" type="text/javascript"></script>


<script>
    //alerts cobntent height
    $(document).ready(function () {
        $('.table-scroll').height($(window).height() - 140);
    });

    $(document).ready(function () {
        $('.client-btn-scroll').height($(window).height() - 204);
    });

    //$(document).ready(function() {
    //$('.tab-content').css("height", $(window).height() - 130)+"px";
    //});
</script>

<script>

    $(".parent .rightbar").click(function () {


        var baseURL = $('#baseURL').val();
        //alert($(this).attr('class'));
        var client_id = $(this).attr('ref');
        var client_uuid = $(this).attr('client_uuid');
        var clientName = $(this).attr('clientName');
        var controlClient = $(this).attr('controlClient');
        var condtin = $(this).attr('con');
        var tabtitle = $('#ClientName').text();
        if (tabtitle != clientName && tabtitle != 'Actions') {
            $('.rightbar').each(function (i, obj) {
                $(this).attr('con', 'hidevalue');
                $(this).parent().parent().removeClass('active_tr');
                //alert(val);

            });
        }
        if (condtin == "hidevalue") {
            $(this).parent().parent().addClass('active_tr');
            len = clientName.length;
            if (len > 15)
            {

                $('#ClientName').html(clientName.substr(0, 15) + '...');
            } else {
                $('#ClientName').html(clientName);
            }


            $.ajax({
                type: 'POST',
                //dataType: 'json',
                url: baseURL + 'partner/ajax_actiontabs',
                data: {'client_id': client_id, 'client_uuid': client_uuid, 'controlClient': controlClient},
                success: function (response) {
                    $('#actiontabs').html(response);

                }
            }).done(function () {

                $(".parent").removeClass('col-md-12 col-sm-12');
                $(".parent").addClass('col-md-10 col-sm-9');
                $(".right_block").show();

                var body = $('body');
                var sidebar = $('.page-sidebar');
                var sidebarMenu = $('.page-sidebar-menu');
                $(".sidebar-search", sidebar).removeClass("open");

                if (body.hasClass("page-sidebar-closed")) {
                } else {
                    body.addClass("page-sidebar-closed");
                    sidebarMenu.addClass("page-sidebar-menu-closed");
                    if (body.hasClass("page-sidebar-fixed")) {
                        sidebarMenu.trigger("mouseleave");
                    }
                    if (Cookies) {
                        Cookies.set('sidebar_closed', '1');
                    }
                }

            });
            $(this).attr('con', 'showvalue');
        } else {
            $(".parent").removeClass('col-md-9');
            $(".parent").addClass('col-md-12');
            $(".right_block").hide();
            $(this).attr('con', 'hidevalue');
            $(this).parent().parent().removeClass('active_tr');


        }

    });
    $(".close").click(function () {
        $(".parent").removeClass('col-md-9');
        $(".parent").addClass('col-md-12');
        $(".right_block").hide();
        $('.rightbar').each(function (i, obj) {
            $(this).attr('con', 'hidevalue');
            $(this).parent().parent().removeClass('active_tr');
            //alert(val);

        });
    });



    $('.sidebar-toggler1').click(function () {
        $(this).find('i').toggleClass('icon-logout is-closed');
        $('.panel').slideToggle('slow');
    });

</script>