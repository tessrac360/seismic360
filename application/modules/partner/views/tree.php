 <link href="<?php echo base_url() . "public/" ?>treeview/style.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
 <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
 <ul class="treeview">
    <li><a href="#"><?php echo $clientData['client_title'] ?></a>
        <?php if (!empty($clientData['children'])) { ?>
            <?php
            echo childTree($clientData['children']);
            ?>
        <?php } ?>
    </li>              
</ul>
<?php
function childTree($treeArray = array()) { ?>
    <ul>
        <?php foreach ($treeArray as $value) {
            ?>
            <li><a href="#"><?php echo $value['client_title'] ?></a>
                <?php
                if (!empty($value['children'])) {
                    echo childTree($value['children']);
                }
                ?>        
            </li>             
        <?php }
        ?>
    </ul>
<?php }
?><script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo base_url() . "public/" ?>treeview/MultiNestedList.js" type="text/javascript"></script>
