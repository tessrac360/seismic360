<style>
h3.sub-head{border-bottom: 1px solid #3ccc; background:#f9f9f9; line-height: 14px; margin:0;}
h3.sub-head span{font-size: 13px; font-weight: 600; background: #3ccc; padding: 6px 12px; color: #fff;display: inline-block; }
.portlet-body .form-group.form-md-line-input.form-md-floating-label .form-control~label{font-size: 13px;}
.form-group.form-md-line-input.form-md-floating-label .form-control.edited~label, .form-group.form-md-line-input.form-md-floating-label .form-control.focus:not([readonly])~label, .form-group.form-md-line-input.form-md-floating-label .form-control.form-control-static~label, .form-group.form-md-line-input.form-md-floating-label .form-control:focus:not([readonly])~label, .form-group.form-md-line-input.form-md-floating-label .form-control[readonly]~label{top: 6px;}
.portlet-body .form-group.form-md-line-input{margin: 0 0 4px;padding-top: 15px;}
.add_field_button, .remove_field{font-size: 12px;  padding: 5px;}
.client-panel-heading{padding:0;background-color: transparent; border:0;}
.client-panel-heading .panel-body, .custom-panel .panel-footer{padding:5px;}
.custom-panel{border-top:0;}
.custom-panel .panel-body{padding:0;}
.custom-panel hr{margin: 11px 0 3px 0;}
.custom-panel .portlet-body.pad0{padding-top:0;}
.custom-panel .accordion-toggle i{font-size:13px;position: relative; top: 6px; right: 10px;}
</style>
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Edit Client</span>
				<span style="color:black;"><?php if($clientName['resultSet']['client_title']){
						echo " (".$clientName['resultSet']['client_title'].")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Edit</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmClient" id="frmClient" method="post" action="">
                    <div class="form-body">
                        <div class="row"> 
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_title" name="client_title" type="text" data-validation="required" data-validation-error-msg="Please enter a client name" value="<?php echo ($getClient['resultSet']->client_title) ? $getClient['resultSet']->client_title : ''; ?>">
                                    <label for="form_control_1">Client Name</label>                                             
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_display_name" name="client_display_name" type="text" data-validation="required" data-validation-error-msg="Please enter a client display name" value="<?php echo ($getClient['resultSet']->name) ? $getClient['resultSet']->name : ''; ?>">
                                    <label for="form_control_1">Client Display Name</label>                                               
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_erp_number" name="client_erp_number" type="text" value="<?php echo ($getClient['resultSet']->client_erp_number) ? $getClient['resultSet']->client_erp_number : ''; ?>">
                                    <label for="form_control_1">Client ERP Number</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_crm_number" name="client_crm_number" type="text" value="<?php echo ($getClient['resultSet']->client_crm_number) ? $getClient['resultSet']->client_crm_number : ''; ?>">
                                    <label for="form_control_1">Client CRM Number</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_url" name="client_url" type="text" value="<?php echo ($getClient['resultSet']->client_url) ? $getClient['resultSet']->client_url : ''; ?>">
                                    <label for="form_control_1">Client Url</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_credit_limit" name="client_credit_limit" type="text" value="<?php echo ($getClient['resultSet']->client_credit_limit) ? $getClient['resultSet']->client_credit_limit : ''; ?>">
                                    <label for="form_control_1">Client Credit Limit</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_discount" name="client_discount" type="text" value="<?php echo ($getClient['resultSet']->client_discount) ? $getClient['resultSet']->client_discount : ''; ?>">
                                    <label for="form_control_1">Client Discount</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_domain" name="client_domain" type="text" value="<?php echo ($getClient['resultSet']->client_domain) ? $getClient['resultSet']->client_domain : ''; ?>">
                                    <label for="form_control_1">Client Domain</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_salesperson_code" name="client_salesperson_code" type="text" value="<?php echo ($getClient['resultSet']->client_salesperson_code) ? $getClient['resultSet']->client_salesperson_code : ''; ?>">
                                    <label for="form_control_1">Client Salesperson Code</label>                                               
                                </div>
                            </div>
							
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_ssr" name="client_ssr" type="text" value="<?php echo ($getClient['resultSet']->client_ssr) ? $getClient['resultSet']->client_ssr : ''; ?>">
                                    <label for="form_control_1">Client SSR</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="client_tax_exemption_no" name="client_tax_exemption_no" type="text" value="<?php echo ($getClient['resultSet']->client_tax_exemption_no) ? $getClient['resultSet']->client_tax_exemption_no : ''; ?>">
                                    <label for="form_control_1">Client TAX Exemption No</label>                                               
                                </div>
                            </div>
						


                    </div>						
							<!--<div class="col-sm-3">
								<label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
								<input type="checkbox" name ="is_subclients" value="1" > No Subclients
								<span></span>
								</label>
							</div>-->


						<div class="panel-group" id="accordion">
							<div class="panel panel-default custom-panel">
								<div class="panel-heading client-panel-heading">
									<h3 class="sub-head">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
											<span>Address</span> <i class="more-less glyphicon glyphicon-minus pull-right"></i>
										</a>
									</h3>
								</div>
								<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
									<div class="panel-body">
										<div class="portlet-body pad0">                                
											<div class="input_fields_wrap">
							
											<?php
											if (!empty($getClientAddress)) {
												$i=0;
												foreach ($getClientAddress as $value) {
													
											?>

												<div  class="countdivs">
												<hr/>
												<div class="fieldRow clearfix">
												
												   <div class="col-md-3">
														 <div class="form-group form-md-line-input form-md-floating-label">
														
														 <input type="text" name="city[]"  data-validation="required" data-validation-error-msg="Please enter a City" class="textinput form-control city"  value ="<?php if(isset($value['city'])){ echo $value['city'];}?>"/>
														  <label for="form_control_1">City</label>
														 </div>

												   </div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
														 <input type="text" name="district[]"  class="textinput form-control district" data-validation="required" data-validation-error-msg="Please enter a District" value ="<?php if(isset($value['district'])){ echo $value['district'];}?>"/>
														 <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField"></span></label>
														 </div>
														 
													  </div>
												   </div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														
														 <div class="form-group form-md-line-input form-md-floating-label">
														 
														 <input type="text" name="state[]"  class="textinput form-control state" data-validation="required" data-validation-error-msg="Please enter a state" value ="<?php if(isset($value['state'])){ echo $value['state'];}?>"/>
														  <label for="id_stock_1_product" class="control-label requiredField">state<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>	
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
														 <input type="text" name="country[]"  class="textinput form-control country" data-validation="required" data-validation-error-msg="Please enter a country" value ="<?php if(isset($value['country'])){ echo $value['country'];}?>"/>
														 <label for="id_stock_1_product" class="control-label">country<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
														 <input type="text" name="postal_code[]"  class="textinput form-control postal_code" data-validation="required" data-validation-error-msg="Please enter a postal code" value ="<?php if(isset($value['postal_code'])){ echo $value['postal_code'];}?>"/>
														 <label for="id_stock_1_product" class="control-label requiredField">postal_code<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>
													<div class="col-md-3">
													  <div id="div_id_stock_1_service">
													
														 <div class="form-group form-md-line-input form-md-floating-label">
														 <input type="text" name="house_no[]"  class="textinput form-control house_no" data-validation="required" data-validation-error-msg="Please enter a house no" value ="<?php if(isset($value['house_no'])){ echo $value['house_no'];}?>"/>
															 <label for="id_stock_1_product" class="control-label requiredField">house_no<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>

												   <div class="col-md-3">
													  <div id="div_id_stock_1_unit">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
															<select class="select form-control address_type edited" id="id_stock_1_unit" name="address_type[]" data-validation="required" data-validation-error-msg="Please select a address type">
															   <option value="1" >office</option>
															   <option value="2">home</option>
															</select>
															<label for="id_stock_1_unit" class="control-label requiredField">address_type<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>								   
													<div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
														 
														 <input type="text" name="street[]"  class="textinput form-control street" data-validation="required" data-validation-error-msg="Please enter a street" value ="<?php if(isset($value['street'])){ echo $value['street'];}?>"/>
														 <label for="id_stock_1_product" class="control-label requiredField">street<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>	
													<div class="clearfix"></div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														
														 <div class="form-group form-md-line-input form-md-floating-label">
														 <textarea class="form-control full_address" rows="2" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter a full address"><?php if(isset($value['full_address'])){ echo $value['full_address'];}?></textarea>
														  <label for="id_stock_1_product" class="control-label requiredField">full_address<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
														 
														 <input type="text" name="logitude[]"  class="textinput form-control logitude" data-validation="required" data-validation-error-msg="Please enter a logitude" value ="<?php if(isset($value['logitude'])){ echo $value['logitude'];}?>"/>
														 <label for="id_stock_1_product" class="control-label requiredField">logitude<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label"><input type="text" name="latitude[]"  class="textinput form-control latitude" data-validation="required" data-validation-error-msg="Please enter a latitude" value ="<?php if(isset($value['latitude'])){ echo $value['latitude'];}?>"/>
														 <label for="id_stock_1_product" class="control-label requiredField">latitude<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>
												   <div class="col-md-2">
													  <div id="div_id_stock_1_service">										
														<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
														<div class="controls ">
														<div class="radio">
															<label><input type="radio" class="is_primary" name="is_primary" data-validation="required" data-validation-error-msg="Please select a primary address" value="<?php echo $i;?>" <?php if(isset($value['is_primary']) && $value['is_primary'] ==1){ echo "checked";}?>/>Is Primary</label>
														</div> 
														 </div>
													  </div>
												   </div>
												<?php if($i >0){ ?>
												  <div class="col-md-1">
													 <div id="div_id_stock_1_service">
														<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
														<div class="controls "><a href="#" class="remove_field btn btn-danger mt-repeater-delete" address_uuid="<?php if(isset($value['address_uuid'])){ echo $value['address_uuid'];}?>"><i class="fa fa-close"  ></i>Remove</a> </div>
													 </div>
												  </div>

												<?php }?>

												</div>
												
												</div>
												<input type="hidden" name="address_uuid[]"  class="textinput form-control" value ="<?php if(isset($value['address_uuid'])){ echo $value['address_uuid'];}?>" />
											<?php $i++;}}?>
								
													</div>
												<div>
													
												</div>
											</div> 
									</div>
								<div class="panel-footer">
									<button class="add_field_button btn btn-success mt-repeater-add pull-right">Add More Address</button><div class="clearfix"></div>
								</div>
								</div>
							</div>
						</div>

                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>
    <script type="text/javascript">

$(document).ready(function() {
	var baseURL = $('#baseURL').val();
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var x = 0;
    x = $('.countdivs').length; //initlal text box count
	
	
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
		//alert(x);
        if(x < max_fields){ //max input box allowed
			//alert(x);
            //x++; //text box increment
        }
            $(wrapper).append('<div class="countdivs"> <hr/> <div class="fieldRow clearfix"> <div class="col-md-3"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="city[]" data-validation="required" data-validation-error-msg="Please enter a City" class="textinput form-control city"/> <label for="form_control_1">City</label> </div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="district[]" class="textinput form-control district" data-validation="required" data-validation-error-msg="Please enter a District"/> <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="state[]" class="textinput form-control state" data-validation="required" data-validation-error-msg="Please enter a state"/> <label for="id_stock_1_product" class="control-label requiredField">state<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="country[]" class="textinput form-control country" data-validation="required" data-validation-error-msg="Please enter a country"/> <label for="id_stock_1_product" class="control-label requiredField">country<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="postal_code[]" class="textinput form-control postal_code" data-validation="required" data-validation-error-msg="Please enter a postal code"/> <label for="id_stock_1_product" class="control-label requiredField">postal_code<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="house_no[]" class="textinput form-control house_no" data-validation="required" data-validation-error-msg="Please enter a house no"/> <label for="id_stock_1_product" class="control-label requiredField">house_no<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_unit"> <div class="form-group form-md-line-input form-md-floating-label"> <select class="select form-control address_type" id="id_stock_1_unit" name="address_type[]" data-validation="required" data-validation-error-msg="Please select a address type"> <option value="">office</option> <option value="1">home</option> </select> <label for="id_stock_1_unit" class="control-label requiredField">address_type<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="street[]" class="textinput form-control street" data-validation="required" data-validation-error-msg="Please enter a street"/> <label for="id_stock_1_product" class="control-label requiredField">street<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <textarea class="form-control full_address" rows="2" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter a full address"></textarea> <label for="id_stock_1_product" class="control-label requiredField">full_address<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="logitude[]" class="textinput form-control logitude" data-validation="required" data-validation-error-msg="Please enter a logitude"/> <label for="id_stock_1_product" class="control-label requiredField">logitude<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"><input type="text" name="latitude[]" class="textinput form-control latitude" data-validation="required" data-validation-error-msg="Please enter a latitude"/> <label for="id_stock_1_product" class="control-label requiredField">latitude<span class="asteriskField"></span></label> </div></div></div><div class="col-md-2"> <div id="div_id_stock_1_service"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "> <div class="radio"><label><input type="radio" class="is_primary" name="is_primary" value="'+x+'" data-validation="required" data-validation-error-msg="Please select a primary address">Is Primary</label></div></div></div></div><div class="col-md-1"> <div id="div_id_stock_1_service"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i>Remove</a> </div></div></div></div></div>'); //adding form
			x++;
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove button
		
		var address_uuid = $(this).attr('address_uuid');
			if(address_uuid != undefined){
				//alert(address_uuid);
				e.preventDefault(); $(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove(); x--;				
				$.ajax({
					type: 'POST',
					//dataType: 'json',
					url: baseURL + 'client/ajaxAddressRemove',
					data: {'address_uuid': address_uuid},
					success: function (response) {
						//$('#actiontabs').html(response);
					}
				}).done(function() {
					alert('ok');

				});	
			}else{
				//alert('notdefined');
				e.preventDefault(); $(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove(); x--;			
			}
			var y=0;
			$('.is_primary').each(function(i, obj) {
				//alert(y);
				$(this).val(y);
				y++;
				//alert(val);

			});				
		//alert(x);
    });
});

</script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });

function recall()
{
	 $.validate();
}
$('#panel1').on('show.bs.collapse hidden.bs.collapse', function (e) {
	if (e.type == 'hidden') {
		$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
		$(window).bind('resize', function () {
			$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
			//alert('resized');
		});
	} else {
		$('.dataTables_scrollBody').css({'height': '210px'});
	}
	$(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');


});
</script>