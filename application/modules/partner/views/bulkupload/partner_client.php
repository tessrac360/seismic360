
<!-- END PAGE HEADER-->
<div class="row">
    <div>

        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Bulk Import</span>
            </div>
            <div class="page-toolbar">
                <ul class="page-breadcrumb breadcrumb custom-bread">
                    <li>
                        <i class="fa fa-cog"></i>
                        <span>Bulk Import</span>                          
                    </li>                   
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="col-sm-12 col-md-12">
                <div class="row box">
                    <div class="row">
                        <div class="col-lg-12">

                            <a class="pull-right btn btn-info btn-xs" href="<?php echo base_url('') . 'public/excel_template/all_template.zip' ?>">
                                <i class="fa fa-file-excel-o"></i> Download All
                            </a>
                        </div>
                    </div>
                    <!-- /.row -->
                    <form name="blukupload" id="blukupload" enctype="multipart/form-data" method="post">     
                        <div class="row">

                            <div class="col-lg-4 col-sm-4">
                                <div class="form-group">
                                    <label for="image">Import Type</label>
                                    <select class="form-control first-secetion filetype" name="filetype">
                                        <option value=""> -- choose field --</option>
                                        <?php
                                        foreach ($importFile as $key => $value) {
                                            ?>
                                            <option value="<?php echo $key; ?>"> <?php echo $value; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <div class="form-group">
                                    <label for="image">Choose file</label>
                                    <input type="file" name="userfile" id="userfile" class="form-control filestyle">
                                </div>

                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <div class="form-group dsample" style="display: none">
                                    <a class="btn btn-info btn-xs sampledownload" style="margin-top: 24px;height: 32px;padding-top: 7px;" href="#">
                                        Download Sample
                                    </a>
                                </div>

                            </div>
                            <div class="col-lg-12 col-sm-12">
                                <div class="form-group text-right" >
                                    <input type="submit" name="importfile" value="Import" id="importfile-id" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.filetype').on('change', function () {
            var fileName = $(this).val();
            var slug = $('.filetype option:selected').text();
            if (fileName != "") {
                $('.dsample').show();
                $(".sampledownload").attr("href", "<?php echo base_url('public/excel_template/') ?>" + fileName);
                $(".sampledownload").text('Download' + slug + ' Sample');
            }else{
                $('.dsample').hide();
            }
        });




        $("#blukupload").validate({
            rules: {
                'userfile': {
                    required: true,
                    extension: "xls,xlsx"
                },
                'filetype': {
                    required: true
                },
            },
            messages: {
                'userfile': {
                    required: "Input is required !",
                    extension: "Please choose an Excel file(.xls or .xlsx) as Input !"
                }
            },
            submitHandler: function (form) {
                document.getElementById("blukupload").submit();
            }

        });

    });

</script>



