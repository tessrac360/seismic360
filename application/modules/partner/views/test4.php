<div class="countdivs">
   <hr/>
   <div class="fieldRow clearfix">
      <div class="col-md-3">
         <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="city[]" data-validation="required" data-validation-error-msg="Please enter a City" class="textinput form-control city" /> <label for="form_control_1">City</label> </div>
      </div>
      <div class="col-md-3">
         <div id="div_id_stock_1_service">
            <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="district[]" class="textinput form-control district" data-validation="required" data-validation-error-msg="Please enter a District"/> <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField"></span></label> </div>
         </div>
      </div>
      <div class="col-md-3">
         <div id="div_id_stock_1_service">
            <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="state[]" class="textinput form-control state" data-validation="required" data-validation-error-msg="Please enter a state"/> <label for="id_stock_1_product" class="control-label requiredField">state<span class="asteriskField"></span></label> </div>
         </div>
      </div>
      <div class="col-md-3">
         <div id="div_id_stock_1_service">
            <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="country[]" class="textinput form-control country" data-validation="required" data-validation-error-msg="Please enter a country"/> <label for="id_stock_1_product" class="control-label requiredField">country<span class="asteriskField"></span></label> </div>
         </div>
      </div>
      <div class="col-md-3">
         <div id="div_id_stock_1_service">
            <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="postal_code[]" class="textinput form-control postal_code" data-validation="required" data-validation-error-msg="Please enter a postal code"/> <label for="id_stock_1_product" class="control-label requiredField">postal_code<span class="asteriskField"></span></label> </div>
         </div>
      </div>
      <div class="col-md-3">
         <div id="div_id_stock_1_service">
            <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="house_no[]" class="textinput form-control house_no" data-validation="required" data-validation-error-msg="Please enter a house no"/> <label for="id_stock_1_product" class="control-label requiredField">house_no<span class="asteriskField"></span></label> </div>
         </div>
      </div>
      <div class="col-md-3">
         <div id="div_id_stock_1_unit">
            <div class="form-group form-md-line-input form-md-floating-label">
               <select class="select form-control address_type" id="id_stock_1_unit" name="address_type[]" data-validation="required" data-validation-error-msg="Please select a address type">
                  <option value="">office</option>
                  <option value="1">home</option>
               </select>
               <label for="id_stock_1_unit" class="control-label requiredField">address_type<span class="asteriskField"></span></label> 
            </div>
         </div>
      </div>
      <div class="col-md-3">
         <div id="div_id_stock_1_service">
            <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="street[]" class="textinput form-control street" data-validation="required" data-validation-error-msg="Please enter a street"/> <label for="id_stock_1_product" class="control-label requiredField">street<span class="asteriskField"></span></label> </div>
         </div>
      </div>
      <div class="col-md-3">
         <div id="div_id_stock_1_service">
            <div class="form-group form-md-line-input form-md-floating-label"> <textarea class="form-control full_address" rows="2" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter a full address"></textarea> <label for="id_stock_1_product" class="control-label requiredField">full_address<span class="asteriskField"></span></label> </div>
         </div>
      </div>
      <div class="col-md-3">
         <div id="div_id_stock_1_service">
            <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="logitude[]" class="textinput form-control logitude" data-validation="required" data-validation-error-msg="Please enter a logitude"/> <label for="id_stock_1_product" class="control-label requiredField">logitude<span class="asteriskField"></span></label> </div>
         </div>
      </div>
      <div class="col-md-3">
         <div id="div_id_stock_1_service">
            <div class="form-group form-md-line-input form-md-floating-label"><input type="text" name="latitude[]" class="textinput form-control latitude" data-validation="required" data-validation-error-msg="Please enter a latitude"/> <label for="id_stock_1_product" class="control-label requiredField">latitude<span class="asteriskField"></span></label> </div>
         </div>
      </div>
      <div class="col-md-2">
         <div id="div_id_stock_1_service">
            <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> 
            <div class="controls ">
               <div class="radio"><label><input type="radio" class="is_primary" name="is_primary" value="'+x+'" data-validation="required" data-validation-error-msg="Please select a primary address">Is Primary</label></div>
            </div>
         </div>
      </div>
      <div class="col-md-1">
         <div id="div_id_stock_1_service">
            <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> 
            <div class="controls "><a href="#" class="remove_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i>Remove</a> </div>
         </div>
      </div>
   </div>
</div>