<?php if ($client_id != 1) { ?>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Manage Gateways" href="<?php echo base_url() . 'gateways/index/' . encode_url($client_id) ;?>">
	<i class="icon-settings" aria-hidden="true" ></i> Gateways
	</a>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Incidents SLA" href="<?php echo base_url() . 'sla/incident/' . encode_url($client_id) ;?>">
	<i class="fa fa-ticket" aria-hidden="true" ></i> SLA
	</a>
	<!--<a class="btn btn-default btn-block" data-toggle="tooltip" title="Subclients Bulk Upload" href="<?php echo base_url() . 'client/uploadclient/' . encode_url($client_id) ;?>">
	<i class="fa fa-upload" aria-hidden="true" ></i> Subclients Bulk Upload
	</a>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Client Locations" href="<?php echo base_url() . 'client/locations/view/' . encode_url($client_id) ;?>">
	<i class="fa fa-map-marker" aria-hidden="true"></i> Client Locations
	</a>-->
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Client Contacts" href="<?php echo base_url() . 'client/contacts/view/' . $client_uuid ;?>">
	<i class="fa fa-phone" aria-hidden="true"></i> Contacts
	</a>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Client Locations" href="<?php echo base_url() . 'client/locations/view/' . $client_uuid ;?>">
	<i class="fa fa-map-marker" aria-hidden="true"></i> Locations
	</a>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Client Escalations" href="<?php echo base_url() . 'client/ClientEscalations/view/' . $client_uuid ;?>">
	<i class="fa fa-empire" aria-hidden="true"></i> Escalations
	</a>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Client Devices" href="<?php echo base_url() . 'client/devices/view/' . encode_url($client_id) ;?>">
	<i class="fa fa-laptop" aria-hidden="true" ></i> Devices
	</a>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Client Holidays" href="<?php echo base_url() . 'client/ClientHolidays/view/' . $client_uuid ;?>">
	<i class="fa fa-suitcase" aria-hidden="true" ></i> Holidays
	</a>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Client System Credentials" href="<?php echo base_url() . 'client/ClientSystemCredentials/view/' . $client_uuid ;?>">
	<i class="fa fa-sign-in" aria-hidden="true" ></i> System Credentials
	</a>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Client Contracts" href="<?php echo base_url() . 'client/contracts/view/' . $client_uuid ;?>">
	<i class="fa fa-pencil-square-o" aria-hidden="true" ></i> Contracts
	</a>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Special Handling Notes" href="<?php echo base_url() . 'client/ClientShn/index/' . $client_uuid ;?>">
	<i class="fa fa-book" aria-hidden="true" ></i> SHN
	</a>	
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Manage User" href="<?php echo base_url() . '/users/partner/partner_list/?cli=' .encode_url($client_id);?>">
	<i class="fa fa-book" aria-hidden="true" ></i> Create Partner Admin
	</a>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Manage Clients" href="<?php echo base_url() . 'client/index/?partnerId=' . encode_url($client_id) ;?>">
	<i class="fa fa-book" aria-hidden="true" ></i> Manage Clients
	</a>
	<!--<a class="btn btn-default btn-block" data-toggle="tooltip" title="Email Account Settings" href="<?php //echo base_url() . 'client/EmailAccountSettings/view/' . $client_uuid ;?>">
	<i class="fa fa-pencil-square-o" aria-hidden="true" ></i> Email Account Settings
	</a>-->	
<?php }?>
<?php if ($controlClient == 1) { ?>
	<a class="btn btn-default btn-block" data-toggle="tooltip" title="Events Header" href="<?php echo base_url() . 'client/columnEdit/' . encode_url($client_id); ?>">
	<i class="fa fa-header" aria-hidden="true" ></i> Events Header
	</a>
<?php }?> 

 