<?php

class Model_client extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    private $finall;

    public function __construct() {
        parent::__construct();
        $this->tablename = 'client';
        $this->users = 'users';
        $this->temp_role = 'temp_role';
        $this->temp_role_permission = 'temp_role_permission';
        $this->role = 'role';
        $this->role_permission = 'role_permission';
        $this->master_columns = 'event_column';
        $this->address = 'address';
        $this->address_types = 'address_types';
		$this->timezones = 'timezones';
        $this->devices = 'devices';
        $this->rename_columns = 'event_column_rename';
        $this->company_id = $this->session->userdata('company_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function insertDevice($data = array()) {
        $this->db->insert($this->devices, $data);
        //echo $this->db->last_query();exit;
    }

    public function getActiveClient() {
        //pr($_SESSION);
        $this->db->select('client');
        $this->db->from($this->users);
        $this->db->where('active_status', 'Y');
        $query = $this->db->get();
//        echo $this->db->last_query();
//        exit;
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            $rows = implode(",", array_column($data, 'id'));
            $return['status'] = 'true';
            $return['resultSet'] = $rows;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    /* Start Module
     * Subject: Active/De-active Client 
      Name: Ketan
     */

    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'Y') {
                $data = array('status' => 'Y', 'is_delete_manually' => '', 'is_delete_auto' => '');
                $userData = array('active_status' => 'Y');
            } else if ($status == 'N') {
                $is_delete_manually = date('Y-m-d', strtotime("+" . CLIENT_MANUALLY_DELETE . " days"));
                $is_delete_auto = date('Y-m-d', strtotime("+" . CLIENT_AUTO_DELETE . " days"));
                $data = array('status' => 'N', 'is_delete_manually' => $is_delete_manually, 'is_delete_auto' => $is_delete_auto);
                $userData = array('active_status' => 'N');
            }
            //Client Status Change (Active/Deactive)
            $data = array_merge($data, $this->uData);
            $this->db->where('id', $id);
            $this->db->update($this->tablename, $data);
            //User Status Change (Active/Deactive)
            $userData = array_merge($userData, $this->uData);
            $this->db->where('client', $id);
            $this->db->update($this->users, $userData);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

    public function is_checkDownClientDisable($clientId = "") {
        $this->db->select('id');
        $this->db->from($this->tablename);
        $this->db->where('parent_client_id', $clientId);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $return['message'] = 'Please De-Active Sub-Clients';
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        //  pr($return);
        return $return;
    }

    public function is_checkUpClientEnable($clientId = "") {
        $this->db->select('parent_client_id');
        $this->db->from($this->tablename);
        $this->db->where('id', $clientId);
        $this->db->where('status', 'N');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            //pr($query->row());
            $parentClient = $query->row();
            $parentClientId = $parentClient->parent_client_id;
            $this->db->select('id');
            $this->db->from($this->tablename);
            $this->db->where('id', $parentClientId);
            $this->db->where('status', 'Y');
            $query = $this->db->get();
            //echo $this->db->last_query();
            if ($query->num_rows() == 0) {
                $return['status'] = 'true';
                $return['message'] = 'Please enable parent client';
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    /* End Module
     * Subject: Active/De-active Client 
      Name: Ketan
     */

    public function getAllClientId() {
        $this->db->select('id');
        $this->db->from($this->tablename);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            $rows = implode(",", array_column($data, 'id'));
            $return['status'] = 'true';
            $return['resultSet'] = $rows;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function isExitCleint($id) {
        $this->db->select('id');
        $this->db->from($this->tablename);
        $this->db->where('id', $id);
        if ($this->company_id != 0) {
            // $this->db->where('company_id', $this->getCompanyId);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    private function updateClientIDStatusY($clientID = "") {
        $data['client'] = $clientID;
        $this->db->where('chkallclient', 'Y');
        $this->db->update('users', $data);
    }

    public function insertClient($postData = array()) {
        $intialize = 0;
        if (isset($postData['intialize'])) {
            $intialize = $postData['intialize'];
            unset($postData['intialize']);
        }
        $postData['partner_id'] = $this->getCompanyId;
        $postData['client_type'] = 'P';
        $postData['role_id'] = $this->roleId;
        $this->db->insert($this->tablename, $postData);
        //echo $this->db->last_query();exit;
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            if ($intialize == 1) {
                $roles = $this->db->get($this->temp_role)->result_array();
                foreach ($roles as $role) {
                    $role_id = $this->copy_roles($insert_id, $role);
                    $this->db->where('role_id', $role['id']);
                    $role_permissions = $this->db->get($this->temp_role_permission)->result_array();
                    foreach ($role_permissions as $role_permission) {
                        $this->copy_role_permissions($role_id, $role_permission);
                    }
                }
            }
            //echo "<pre>";pr($role_permissions);exit;
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function copy_roles($client_id, $Data) {
        unset($Data['id']);
        $Data['client_id'] = $client_id;
        $Data['uuid'] = generate_uuid('eerl_');
        $Data['company_id'] = helpler_getCompanyId();
        $Data = array_merge($Data, $this->cData);
        $this->db->insert($this->role, $Data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function copy_role_permissions($role_id, $Data) {
        unset($Data['pid']);
        $Data['role_id'] = $role_id;
        $Data = array_merge($Data, $this->cData);
        $this->db->insert($this->role_permission, $Data);
    }

    public function updateClient($postData = array(), $id) {
        //echo "<pre>";pr($postData);exit;
        /* if ($postData['is_subclients'] == '') {
          $postData['is_subclients'] = 0;
          } */
        $this->db->where('id', $id);
        $update_status = $this->db->update($this->tablename, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function getClientPagination($search = '', $client_id = 0, $limit = 0, $start = 0, $allClients = array(), $type = 0) {
        $this->db->select('cl.*, CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from($this->tablename . ' as cl');
        $this->db->join('users as usr', 'usr.id = cl.created_by');
        $this->db->where('cl.client_type', 'P');
		$this->db->where('cl.is_deleted', '0');

        if ($type == 0) {
            $this->db->where('cl.id', $client_id);
        } else {
            $this->db->where('cl.parent_client_id', $client_id);
        }

        //$this->db->where_not_in('cl.id', 1);
        if ($search != '') {
            //$this->db->like('cl.client_title', $search);
        }
        $this->db->order_by('cl.id', "asc");
        if ($start >= 0 and $limit > 0 and $type == 0 and $search == '') {
            $this->db->limit($limit, $start);
        }

        $clients = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0 && $type == 0) {
            return $clients->num_rows();
        }
        foreach ($clients->result_array() as $client) {

            $allClients[] = $client;
            $allClients = $this->getClientPagination($search, $client['id'], 0, 0, $allClients, 1);
        }
        return $allClients;
    }

    public function getClientPag($search = '', $client_id = 0, $limit = 0, $start = 0, $allClients = array(), $type = 0) {

        $this->db->select('cl.*, CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from($this->tablename . ' as cl');
        $this->db->join('users as usr', 'usr.id = cl.created_by');

        if ($type == 0) {
            $this->db->where('cl.id', $client_id);
        } else {
            $this->db->where('cl.parent_client_id', $client_id);
        }



        //$this->db->where_not_in('cl.id', 1);
        if ($search != '') {
            //$this->db->like('cl.client_title', $search);
        }
        $this->db->order_by('cl.id', "asc");
        if ($start >= 0 and $limit > 0 and $type == 0 and $search == '') {
            $this->db->limit($limit, $start);
        }

        $clients = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0 && $type == 0) {
            return $clients->num_rows();
        }
        foreach ($clients->result_array() as $client) {

            $allClients[] = $client;
            $allClients = $this->getClientPag($search, $client['id'], 0, 0, $allClients, 1);
        }
        return $allClients;
    }

    public function getClients($client_id = 0, $allClients = array()) {

        $this->db->select('cl.*');
        $this->db->from($this->tablename . ' as cl');
        $this->db->where('cl.parent_client_id', $client_id);
        $this->db->where_not_in('cl.id', 1);
        $this->db->order_by('cl.id', "asc");
        $clients = $this->db->get();
        //echo $this->db->last_query();exit;			
        foreach ($clients->result_array() as $client) {
            //echo "<pre>";pr($client);exit;

            $allClients[] = $client;
            $allClients = $this->getClients($client['id'], $allClients);
        }
        return $allClients;
    }

    public function getClientsgat($client_id = 0, $allClients = array()) {

        $this->db->select('cl.*');
        $this->db->from($this->tablename . ' as cl');

        if (count($allClients) == 0 && $client_id != 1) {
            $this->db->where('cl.id', $client_id);
        } else {
            $this->db->where('cl.parent_client_id', $client_id);
        }
        $this->db->where_not_in('cl.id', 1);
        $this->db->order_by('cl.id', "asc");
        $clients = $this->db->get();
        //echo $this->db->last_query();exit;			
        foreach ($clients->result_array() as $client) {
            //echo "<pre>";pr($client);exit;

            $allClients[] = $client;
            $allClients = $this->getClients($client['id'], $allClients);
        }
        return $allClients;
    }

    /*
     * Objective: Pagination of Role moudle with Searching
     * parameters: Null
     * Return: Null
     */

    public function getUserSearchData($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {
        $this->db->select('cl.*, CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from($this->tablename . ' as cl');
        $this->db->join('users as usr', 'usr.id = cl.created_by');
        $this->db->where('cl.company_id', $this->getCompanyId);
        $this->db->where('cl.parent_client_id', $this->session->userdata('client_id'));
        if ($search != "") {
            $this->db->like('cl.client_title', $search);
        }
        $this->db->order_by('cl.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getClientEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $this->db->where('id', $postId);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

    public function getClientTreeView($parent_id = "", $access = 0) { // 1:top level 0:low level
        $this->db->select('id,client_title,parent_client_id');
        $this->db->from($this->tablename);
        if ($access == 1) {
            $this->db->where('id', $parent_id);
        } else {
            $this->db->where('parent_client_id', $parent_id);
        }
        $query = $this->db->get();
        //echo $this->db->last_query().'<br>';//exit;

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function getSubclient($clientId = "") {
        $this->db->select('id,client_title,is_subclients');
        $this->db->from($this->tablename);
        $this->db->where('parent_client_id', $clientId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

    public function getClientName($clientId = "") {
        // $getCompanyId = helpler_getCompanyId();
        $this->db->select('client_title');
        $this->db->from('client');
        //$this->db->where('status', 'Y');
        $this->db->where('id', $clientId);
        //echo $this->company_id;
        //if($this->company_id != 0)
        // $this->db->where('company_id', $getCompanyId);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    /* Start Module
     * Subject: Delete Client 
      Name: Ketan
     */

    public function deleteClient($clientId = "") {
        if ($clientId) {
            $this->db->where('id', $clientId);
            if ($this->db->delete($this->tablename)) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    /* End Module
     * Subject: Delete Client 
      Name: Ketan
     */

    public function updateParentClient($postData = array(), $id) {
        //echo "<pre>";pr($postData);exit;
        if ($postData['is_subclients'] == '') {
            $postData['is_subclients'] = 0;
        }
        $this->db->where('parent_client_id', $id);
        $update_status = $this->db->update($this->tablename, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function getClientTreeViewMove($parent_id = "", $access = 0) { // 1:top level 0:low level
        $this->db->select('*');
        $this->db->from($this->tablename);
        if ($access == 1) {
            $this->db->where('id', $parent_id);
        } else {
            $this->db->where('parent_client_id', $parent_id);
        }
        $query = $this->db->get();
        //echo $this->db->last_query()."<br>";

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    /*
      Start
      Name:Veeru
      Comment: Fetching event_column master data and inserting into the event_column_rename table
     */

    public function getColumnsInsert($client_id = "") {
        $this->db->select('column_id,column_name');
        $this->db->from($this->master_columns);
        $query = $this->db->get();
        //echo $this->db->last_query()."<br>";
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $res) {
                $data['column_id'] = $res['column_id'];
                $data['rename_column'] = $res['column_name'];
                $data['client_id'] = $client_id;
                $this->db->insert($this->rename_columns, $data, $client_id);
                //pr($data);exit;
            }
        } else {
            return array();
        }
    }

    public function getRenameColumns($postId = "") {
        $this->db->select('*');
        $this->db->from($this->rename_columns);
        $this->db->where('client_id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function updateColumn($text = array(), $id = NULL) {
        //$data['client'] = $client_id;
        $this->db->where('rename_id', $id);
        $update_status = $this->db->update($this->rename_columns, $text);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    /*
      Name:Veeru
      Comment: Fetching event_column master data and inserting into the event_column_rename table
      End
     */

    function insertClients($data) {
        $this->db->insert($this->tablename, $data);
        //echo $this->db->last_query();exit;
    }

    function insertAddress($data) {
        $this->db->insert_batch($this->address, $data);
        //echo $this->db->last_query();exit;
    }

    function updateAddress($data) {
        $this->db->update_batch($this->address, $data, 'address_uuid');
        //echo $this->db->last_query();exit;
    }

    public function getClientAddress($client_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->address);
        $this->db->where('referrence_uuid', $client_uuid);
        $this->db->where('is_deleted', '0');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {

            return $query->result_array();
        } else {

            return array();
        }
    }

    public function getAddressTypes($client_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->address_types);
        $this->db->where('is_deleted', '0');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

	public function getTimezones() {
        $this->db->select('*');
        $this->db->from($this->timezones);
        $this->db->where('is_deleted', '0');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
    public function deleteClientAddress($address_uuid = NULL) {
        //$data['client'] = $client_id;
        $this->db->where('address_uuid', $address_uuid);
        $data = array("is_deleted" => "1");
        $update_status = $this->db->update($this->address, $data);
        echo $this->db->last_query();
        exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function updateClientUuid($postData = array(), $idv, $id, $table) {
        //echo "<pre>";pr($postData);exit;
        /* if ($postData['is_subclients'] == '') {
          $postData['is_subclients'] = 0;
          } */
        $this->db->where($id, $idv);
        $update_status = $this->db->update($table, $postData);
        echo $this->db->last_query();
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function getIds($table = NULL, $field = NULL, $id = NULL) {
        $return['status'] = 'false';
        if ($table != NULL && $field != NULL) {
            $this->db->select($id);
            $this->db->from($table);
            $this->db->where($field, '');
            $this->db->order_by($id, "asc");
            $query = $this->db->get();
            //echo $this->db->last_query();exit;
            if ($query->num_rows() > 0) {
                $return['status'] = 'true';
                $return['resultSet'] = $query->result_array();
            } else {
                $return['status'] = 'false';
            }
        }
        return $return;
    }

}

?>