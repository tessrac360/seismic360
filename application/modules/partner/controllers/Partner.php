<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Partner extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user, 'company_id' => $this->getCompanyId);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user, 'company_id' => $this->getCompanyId);
        $this->client_id = $this->session->userdata('client_id');
        $this->cDataDevice = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->load->model("Model_client");
        $this->load->model('roles/Module_roles');
        $this->load->library('form_validation');
        $this->load->library('Csvimport');
    }

//    function _alpha_dash_space($str_in = '') {
//        if (!preg_match("/^([-a-zA-Z0-9_ '])+$/i", $str_in)) {
//            $this->form_validation->set_message('_alpha_dash_space', 'The %s field may only contain alpha-numeric characters, spaces, underscores, single quotes and dashes.');
//            return FALSE;
//        } else {
//            return TRUE;
//        } 
//    }

    public function dashboard() {
        $data['file'] = 'users_view';
        $this->load->view('template/front_template', $data);
    }

    public function index() {
//$this->getUser();
        $roleAccess = helper_fetchPermission('15', 'view');
        if ($roleAccess == 'Y') {
            $this->getClient();
        } else {
            redirect('unauthorized');
        }
    }

    public function getClient() {
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'partner/getclient/';
        $config['total_rows'] = $this->Model_client->getClientPagination($search, $this->client_id);
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['client'] = $this->Model_client->getClientPagination($search, $this->client_id, $config['per_page'], $page);
        //pr($data['client']);exit;
        if ($search != '') {
            $tmp = array();
            $cnt = count($data['client']);
            for ($i = 0; $i < $cnt; $i++) {
                if (preg_match("/" . $search . "/i", $data['client'][$i]['client_title'])) {
                    $tmp[] = $data['client'][$i];
                }
            }
            $data['client'] = array();
            $data['client'] = $tmp;
        }
        $data['client_id'] = $this->client_id;
        $data['subclients'] = $this->Model_client->getClientEdit($this->client_id);
        $data['hiddenURL'] = 'partner/getclient/';
        $data['file'] = 'view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }

    public function searchdata() {
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'partner/searchData/';
        $config['total_rows'] = count($this->Model_client->getUserSearchData($search));

        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
//$config['first_url'] = base_url() . "partner/searchdata/0?sd=" . $search;
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['client'] = $this->Model_client->getUserSearchData($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'partner/searchData';
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }

    public function create() {
        $client_id = (isset($_GET['cli'])) ? decode_url($_GET['cli']) : $this->session->userdata('client_id');
        $level = (isset($_GET['lvl'])) ? decode_url($_GET['lvl']) : 1;
        $roleAccess = helper_fetchPermission('15', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                //exit;
                $this->form_validation->set_rules('client_title', 'Client Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $clientPost = $this->input->post();
                    $ClientData = array();
                    $clientAddress = array();
                    //pr($clientPost);exit;				
                    $ClientData['parent_client_id'] = $client_id;
                    $ClientData['level'] = $level + 1;
                    $ClientData['client_title'] = $clientPost['client_title'];
                    $ClientData['name'] = $clientPost['client_display_name'];
                    $ClientData['client_erp_number'] = $clientPost['client_erp_number'];
					$ClientData['timezone_id'] = $clientPost['timezone_id'];
                    $ClientData['client_crm_number'] = $clientPost['client_crm_number'];
                    $ClientData['client_url'] = $clientPost['client_url'];
                    $ClientData['client_credit_limit'] = $clientPost['client_credit_limit'];
                    $ClientData['client_discount'] = $clientPost['client_discount'];
                    $ClientData['client_domain'] = $clientPost['client_domain'];
                    $ClientData['client_uuid'] = generate_uuid('clie_');
                    $ClientData['client_salesperson_code'] = $clientPost['client_salesperson_code'];
                    $ClientData['client_ssr'] = $clientPost['client_ssr'];
                    $ClientData['client_tax_exemption_no'] = $clientPost['client_tax_exemption_no'];
					$ClientData['private_key'] = createSecretKey($ClientData['client_uuid']);
					$ClientData['public_key'] = $ClientData['client_uuid'];
                    if (isset($clientPost['intialize'])) {
                        $ClientData['intialize'] = $clientPost['intialize'];
                    }

                    $ClientData = array_merge($ClientData, $this->cData);
                    $clientPost['level'] = $level + 1;
                    $res = $this->Model_client->getClientEdit($client_id);
                    if ($client_id != 1) {
                        $ClientData['primary_client_id'] = $res['resultSet']->primary_client_id;
                    }
                    $resultData = $this->Model_client->insertClient($ClientData);
   /* Start
                      Name:Veeru
                      Date: 26/12/17
                      Comment: Fetching client_id and client_uuid and inserting into the  eventedge_devices table
                     */
                    $ClientDevice['uuid'] = generate_uuid('ediv_');
                    $ClientDevice['device_name'] = 'NA';
                    $ClientDevice['client_id'] = $resultData['lastId'];
                    $ClientDevice['client_uuid'] = $ClientData['client_uuid'];
                    $ClientDevice['gateway_id'] = '0';
                    $ClientDevice['gateway_uuid'] = 'eecg_00000000-0000-0000-0000-000000000000';
                    $ClientDevice['device_category_id'] = '0';
                    $ClientDevice['device_category_uuid'] = 'dcat_00000000-0000-0000-0000-000000000000';
                    $ClientDevice['service_group_uuid'] = 'sgp_00000000-0000-0000-0000-000000000000';
                    $ClientDevice = array_merge($ClientDevice, $this->cDataDevice);
                    //pr($ClientDevice);exit;
                    $DeviceData = $this->Model_client->insertDevice($ClientDevice);

                    /* End
                      Name:Veeru
                      Date: 26/12/17
                      Comment: Fetching client_id and client_uuid and inserting into the  eventedge_devices table
                     */                  
                    if ($resultData['status'] == 'true') {
                        $lastInsertId = $resultData['lastId'];
                        for ($i = 0; $i < count($clientPost['city']); $i++) {
                            $clientAddress[$i]['city'] = $clientPost['city'][$i];
                            $clientAddress[$i]['district'] = $clientPost['district'][$i];
                            $clientAddress[$i]['state'] = $clientPost['state'][$i];
                            $clientAddress[$i]['country'] = $clientPost['country'][$i];
                            $clientAddress[$i]['postal_code'] = $clientPost['postal_code'][$i];
                            $clientAddress[$i]['house_no'] = $clientPost['house_no'][$i];
                            $clientAddress[$i]['address_type'] = $clientPost['address_type'][$i];
                            $clientAddress[$i]['referrence_uuid'] = $ClientData['client_uuid'];
                            $clientAddress[$i]['street'] = $clientPost['street'][$i];
                            $clientAddress[$i]['full_address'] = $clientPost['full_address'][$i];
                            $clientAddress[$i]['logitude'] = $clientPost['logitude'][$i];
                            $clientAddress[$i]['latitude'] = $clientPost['latitude'][$i];
                            $clientAddress[$i]['created_on'] = $this->current_date;
                            $clientAddress[$i]['created_by'] = $this->current_user;
                            //$clientAddress[$i]['company_id'] = $this->getCompanyId;
                            $clientAddress[$i]['address_uuid'] = generate_uuid('addr_');
                            if ($clientPost['is_primary'] == $i) {
                                $clientAddress[$i]['is_primary'] = 1;
                            } else {
                                $clientAddress[$i]['is_primary'] = 0;
                            }
                        }
                        $resultDataAddress = $this->Model_client->insertAddress($clientAddress);
                        //pr($clientAddress);exit;						
                        /*
                          Name:Veeru
                          Comment: Fetching event_column master data and inserting into the event_column_rename table
                         */
                        $columnsInsert = $this->Model_client->getColumnsInsert($lastInsertId);
                        /*
                          Name:Veeru
                          Comment: Fetching event_column master data and inserting into the event_column_rename table
                         */
                        if ($client_id == 1) {
                            $primary_client['primary_client_id'] = $lastInsertId;
                            $updateStatus = $this->Model_client->updateClient($primary_client, $lastInsertId);
                        }
                        /* Start new Log */
                        $getNewData = helper_LogFetchRecord('client', 'id', $lastInsertId);
                        $newValue = $getNewData['log'];
                        $actions = 'CREATE';
                        helper_createLogActions('client_log', $lastInsertId, $actions, $oldValue, $newValue);
                        /* Start new Log */
                        $this->session->set_flashdata("success_msg", "partner is created successfully ..!!");
                        redirect('partner');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('partner/create');
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['rolesCnt'] = count($this->Module_roles->getRolesPagin($client_id));
                    $data['clientName'] = $this->Model_client->getClientName($client_id);
                    $data['address_types'] = $this->Model_client->getAddressTypes();
					$data['timezones'] = $this->Model_client->getTimezones();

                    //echo "<pre>";pr($data['clientName']);exit;
                    $data['file'] = 'create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['user_type'] = $this->user_type;
                $data['rolesCnt'] = count($this->Module_roles->getRolesPagin($client_id));
                $data['clientName'] = $this->Model_client->getClientName($client_id);
                $data['address_types'] = $this->Model_client->getAddressTypes();
				$data['timezones'] = $this->Model_client->getTimezones();
                //echo "<pre>";pr($data['clientName']);exit;
                $data['file'] = 'create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($postId = NULL) {
        $roleAccess = helper_fetchPermission('15', 'view');
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId);
            $getStatus = $this->Model_client->isExitCleint($postId);
//$getStatus['status'] = 'true';
            if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
                    $clientPost = $this->input->post();
                    //pr($clientPost);exit;
                    $this->form_validation->set_rules('client_title', 'Client Name', 'required');
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
                        /* Start Old Log */
                        $getoldData = helper_LogFetchRecord('client', 'id', $postId);
                        $oldValue = $getoldData['log'];
                        /* Start Old Log */
//unset($postUsers['id']);
                        $ClientData = array();
                        $clientAddress = array();
                        //pr($clientPost);exit;				
                        $ClientData['client_title'] = $clientPost['client_title'];
                        $ClientData['name'] = $clientPost['client_display_name'];
                        $ClientData['client_erp_number'] = $clientPost['client_erp_number'];
                        $ClientData['client_crm_number'] = $clientPost['client_crm_number'];
                        $ClientData['client_url'] = $clientPost['client_url'];
						$ClientData['timezone_id'] = $clientPost['timezone_id'];
                        $ClientData['client_credit_limit'] = $clientPost['client_credit_limit'];
                        $ClientData['client_discount'] = $clientPost['client_discount'];
                        $ClientData['client_domain'] = $clientPost['client_domain'];
                        $ClientData['client_salesperson_code'] = $clientPost['client_salesperson_code'];
                        $ClientData['client_ssr'] = $clientPost['client_ssr'];
                        $ClientData['client_tax_exemption_no'] = $clientPost['client_tax_exemption_no'];
                        $ClientData = array_merge($ClientData, $this->uData); //pr($postUsers);exit;
                        $updateStatus = $this->Model_client->updateClient($ClientData, $postId);
                        if ($updateStatus['status'] == 'true') {
                            $clientAddress = array();
                            $clientAddressup = array();
                            $data['getClient'] = $this->Model_client->getClientEdit($postId);
                            //pr($clientPost['is_primary']);//exit;
                            for ($i = 0; $i < count($clientPost['city']); $i++) {
                                if (isset($clientPost['address_uuid'][$i])) {
                                    $clientAddressup[$i]['city'] = $clientPost['city'][$i];
                                    $clientAddressup[$i]['district'] = $clientPost['district'][$i];
                                    $clientAddressup[$i]['state'] = $clientPost['state'][$i];
                                    $clientAddressup[$i]['country'] = $clientPost['country'][$i];
                                    $clientAddressup[$i]['postal_code'] = $clientPost['postal_code'][$i];
                                    $clientAddressup[$i]['house_no'] = $clientPost['house_no'][$i];
                                    $clientAddressup[$i]['address_type'] = $clientPost['address_type'][$i];
                                    $clientAddressup[$i]['referrence_uuid'] = $data['getClient']['resultSet']->client_uuid;
                                    $clientAddressup[$i]['address_uuid'] = $clientPost['address_uuid'][$i];

                                    $clientAddressup[$i]['street'] = $clientPost['street'][$i];
                                    $clientAddressup[$i]['full_address'] = $clientPost['full_address'][$i];
                                    $clientAddressup[$i]['logitude'] = $clientPost['logitude'][$i];
                                    $clientAddressup[$i]['latitude'] = $clientPost['latitude'][$i];
                                    $clientAddressup[$i]['updated_on'] = $this->current_date;
                                    $clientAddressup[$i]['updated_by'] = $this->current_user;

                                    if ($clientPost['is_primary'] == $i) {
                                        $clientAddressup[$i]['is_primary'] = 1;
                                    } else {
                                        $clientAddressup[$i]['is_primary'] = 0;
                                    }
                                } else {
                                    $clientAddress[$i]['city'] = $clientPost['city'][$i];
                                    $clientAddress[$i]['district'] = $clientPost['district'][$i];
                                    $clientAddress[$i]['state'] = $clientPost['state'][$i];
                                    $clientAddress[$i]['country'] = $clientPost['country'][$i];
                                    $clientAddress[$i]['postal_code'] = $clientPost['postal_code'][$i];
                                    $clientAddress[$i]['house_no'] = $clientPost['house_no'][$i];
                                    $clientAddress[$i]['address_type'] = $clientPost['address_type'][$i];
                                    $clientAddress[$i]['referrence_uuid'] = $data['getClient']['resultSet']->client_uuid;
                                    $clientAddress[$i]['street'] = $clientPost['street'][$i];
                                    $clientAddress[$i]['full_address'] = $clientPost['full_address'][$i];
                                    $clientAddress[$i]['logitude'] = $clientPost['logitude'][$i];
                                    $clientAddress[$i]['latitude'] = $clientPost['latitude'][$i];
                                    $clientAddress[$i]['created_on'] = $this->current_date;
                                    $clientAddress[$i]['created_by'] = $this->current_user;
                                    //$clientAddress[$i]['company_id'] = $this->getCompanyId;
                                    $clientAddress[$i]['address_uuid'] = generate_uuid('addr_');
                                    if ($clientPost['is_primary'] == $i) {
                                        $clientAddress[$i]['is_primary'] = 1;
                                    } else {
                                        $clientAddress[$i]['is_primary'] = 0;
                                    }
                                }
                            }
                            //pr($clientAddressup);
                            //pr($clientAddress);//exit;
                            if (count($clientAddress) > 0) {
                                $resultDataAddress = $this->Model_client->insertAddress($clientAddress);
                            }
                            if (count($clientAddressup) > 0) {

                                $resultDataAddressup = $this->Model_client->updateAddress($clientAddressup);
                            }


                            //exit;							
                            /* Start new Log */
                            $getNewData = helper_LogFetchRecord('client', 'id', $postId);
                            $newValue = $getNewData['log'];
                            $actions = 'EDIT';
                            helper_createLogActions('client_log', $postId, $actions, $oldValue, $newValue);
                            /* Start new Log */
                            $this->session->set_flashdata("success_msg", "Client is Updated successfully ..!!");
                            redirect('partner');
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('partner/edit');
                        }
                    } else {
                        $data['user_type'] = $this->user_type;
                        $data['getClient'] = $this->Model_client->getClientEdit($postId);

                        $data['clientName'] = $this->Model_client->getClientName($data['getClient']['resultSet']->parent_client_id);
                        $data['getClientAddress'] = $this->Model_client->getClientAddress($data['getClient']['resultSet']->client_uuid);
                        $data['address_types'] = $this->Model_client->getAddressTypes();
						$data['timezones'] = $this->Model_client->getTimezones();
                        //pr($data['getClientAddress']);exit;
                        $data['subclient'] = $this->Model_client->getSubclient($postId);
						$data['timezones'] = $this->Model_client->getTimezones();
                        $data['file'] = 'update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['getClient'] = $this->Model_client->getClientEdit($postId);
                    $data['clientName'] = $this->Model_client->getClientName($data['getClient']['resultSet']->parent_client_id);
                    $data['getClientAddress'] = $this->Model_client->getClientAddress($data['getClient']['resultSet']->client_uuid);
                    $data['address_types'] = $this->Model_client->getAddressTypes();
                    //pr($data['getClientAddress']);exit;
                    $data['subclient'] = $this->Model_client->getSubclient($postId);
					$data['timezones'] = $this->Model_client->getTimezones();
                    $data['file'] = 'update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('/users');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = $_POST['id'];
        if ($id != "") {
            if ($status == 'Y') {
                $returnSet = $this->Model_client->is_checkUpClientEnable($id);
                if ($returnSet['status'] == 'true') {
                    $return['status'] = 'infoUP';
                    $return['message'] = $returnSet['message'];
                } else {
                    /* Start Old Log */
                    $getoldData = helper_LogFetchRecord('client', 'id', $id);
                    $oldValue = $getoldData['log'];
                    /* Start Old Log */
                    $Status = $this->Model_client->updateStatus($status, $id);
                    if ($Status['status'] == 'true') {
                        /* Start new Log */
                        $getNewData = helper_LogFetchRecord('client', 'id', $id);
                        $newValue = $getNewData['log'];
                        $actions = 'STATUS';
                        helper_createLogActions('client_log', $id, $actions, $oldValue, $newValue);
                        /* Start new Log */
                        $return['html'] = '<a title="Active" href="javascript:void(0);" class="client_status" myval="' . $id . '" status="N"> <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i> </a>';
                        $return['message'] = 'Client is activated successfully';
                        $return['status'] = 'true';
                    }
                }
            } else if ($status == 'N') {
                $returnSet = $this->Model_client->is_checkDownClientDisable($id);
                if ($returnSet['status'] == 'true') {
                    $return['status'] = 'infoDOWN';
                    $return['message'] = $returnSet['message'];
                } else {
                    /* Start Old Log */
                    $getoldData = helper_LogFetchRecord('client', 'id', $id);
                    $oldValue = $getoldData['log'];
                    /* Start Old Log */
                    $Status = $this->Model_client->updateStatus($status, $id);
                    if ($Status['status'] == 'true') {
                        /* Start new Log */
                        $getNewData = helper_LogFetchRecord('client', 'id', $id);
                        $newValue = $getNewData['log'];
                        $actions = 'STATUS';
                        helper_createLogActions('client_log', $id, $actions, $oldValue, $newValue);
                        /* Start new Log */
                        $return['status'] = 'true';
                        $return['message'] = 'Client is Deactivated successfully';
                        $return['html'] = '<a title="De-activate"  href="javascript:void(0);" class="client_status" myval="' . $id . '" status="Y"> <i class="fa fa-times-circle-o fa-lg" aria-hidden="true"></i> </a>';
                    }
                }
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    /* Start Module
     * Subject: Delete Client 
      Name: Ketan
     */

    public function delete($client_id = "") {
        $client_id = decode_url($client_id);
        if ($client_id != "") {
            //echo $client_id;
            $resultSet = $this->treeClientDelete($client_id);
            $this->session->set_flashdata("success_msg", "Client is Deleted successfully ..!!");
            redirect('client');
        }
        exit;
    }

    public function treeClientDelete($parent_client_id = "") {
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeView($parent_client_id, 1);
        if (!empty($resultSet)) {
            foreach ($resultSet as $value) {
                /* Start Old Log */
                $getoldData = helper_LogFetchRecord('client', 'id', $value['id']);
                $oldValue = $getoldData['log'];
                // pr($oldValue);exit;
                /* Start Old Log */
                $this->Model_client->deleteClient($value['id']);
                $newValue = '';
                $actions = 'DELETE';
                helper_createLogActions('client_log', $value['id'], $actions, $oldValue, $newValue);


                $client['id'] = $value['id'];
                $client['client_title'] = $value['client_title'];
                $client['parent_client_id'] = $value['parent_client_id'];
                $client['children'] = $this->getClientChildrenDelete($value['id']);
                $clients[] = $client;
            }
            return $clients[0];
        } else {
            return array();
        }
    }

    public function getClientChildrenDelete($parent_id) {
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeView($parent_id);
        if (!empty($resultSet)) {
            foreach ($resultSet as $value) {
                /* Start Old Log */
                $getoldData = helper_LogFetchRecord('client', 'id', $value['id']);
                $oldValue = $getoldData['log'];
                /* Start Old Log */
                $this->Model_client->deleteClient($value['id']);
                $newValue = '';
                $actions = 'DELETE';
                helper_createLogActions('client_log', $value['id'], $actions, $oldValue, $newValue);
                $client['id'] = $value['id'];
                $client['client_title'] = $value['client_title'];
                $client['parent_client_id'] = $value['parent_client_id'];
                $client['children'] = $this->getClientChildrenDelete($value['id']);
                $clients[] = $client;
            }
            return $clients;
        } else {
            return array();
        }
    }

    /* End Module
     * Subject: Delete Client 
      Name: Ketan
     */

    /* Name :Prasad 
      date :27-07-17
      Subject : Client Move
     */

    public function moveClient($clientId, $parentClientId) {

        if ($this->input->post()) {

            $postClient = $this->input->post();
            $postClient['parent_client_id'] = $postClient['subClients'];
            $data['subclients'] = $this->Model_client->getClientEdit($postClient['parent_client_id']);
            $clientId = decode_url($postClient['moveClientId']);
            $postClient['level'] = $data['subclients']['resultSet']->level + 1;
            unset($postClient['moveClientId']);
            unset($postClient['subClients']);
            $postClient = array_merge($postClient, $this->uData);
            /* Start Old Log */
            $getoldData = helper_LogFetchRecord('client', 'id', $clientId);
            $oldValue = $getoldData['log'];
            /* Start Old Log */
            //unset($postUsers['id']);
            $postClient = array_merge($postClient, $this->uData); //pr($postUsers);exit;
            $updateStatus = $this->Model_client->updateClient($postClient, $clientId);
            $this->updateClientChildrenLevel($clientId, $postClient['level']);

            if ($updateStatus['status'] == 'true') {
                /* Start new Log */
                $getNewData = helper_LogFetchRecord('client', 'id', $clientId);
                $newValue = $getNewData['log'];
                $actions = 'MOVE';
                helper_createLogActions('client_log', $clientId, $actions, $oldValue, $newValue);
                //echo $this->db->last_query();exit;
                /* Start new Log */
                $this->session->set_flashdata("success_msg", "Client is Moved successfully ..!!");
                redirect('client');
            } else {
                $this->session->set_flashdata("error_msg", "Some thing went wrong");
                redirect('partner/edit');
            }
        } else {
            $data['subclients'] = $this->Model_client->getClientEdit(decode_url($clientId));
            $data['sub_clients'] = $this->treeClient($data['subclients']['resultSet']->primary_client_id);
            echo $data['subclients']['resultSet']->level;
            //echo "<pre>";pr($data['subclients']);exit;
            $data['file'] = 'move_form';
            $data['clientMove'] = decode_url($clientId);
            $data['clientMoveParent'] = decode_url($parentClientId);
            $data['level'] = $data['subclients']['resultSet']->level;
            $data['title'] = $data['subclients']['resultSet']->client_title;
            $this->load->view('template/front_template', $data);
        }
    }

    public function updateClientChildrenLevel($parent_id, $level) {
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeViewMove($parent_id);
        if (count($resultSet) > 0) {
            $level = $level + 1;
            $postData['level'] = $level;
            $updateLevel = $this->Model_client->updateParentClient($postData, $parent_id);
            foreach ($resultSet as $value) {
                $client['id'] = $value['id'];
                $client['children'] = $this->updateClientChildrenLevel($client['id'], $level);
                //echo "<pre>";pr($clients);
            }
        }
    }

    public function treeClient($primaryClientId) {
        if ($this->session->userdata('client_id') == 1) {
            $parent_client_id = $primaryClientId;
        } else {
            $parent_client_id = $this->session->userdata('client_id');
        }
        //echo $parent_client_id;exit;
        $clients = array();
        if ($parent_client_id == 1) {
            $resultSet = $this->Model_client->getClientTreeViewMove($parent_client_id);
        } else {
            $resultSet = $this->Model_client->getClientTreeViewMove($parent_client_id, 1);
        }
        if (!empty($resultSet)) {
            foreach ($resultSet as $value) {
                $client['id'] = $value['id'];
                $client['client_title'] = $value['client_title'];
                $client['parent_client_id'] = $value['parent_client_id'];
                $client['level'] = $value['level'];
                $client['children'] = $this->getClientChildren($value['id']);
                $clients[] = $client;
            }
            //echo "<pre>";pr($clients);
            //exit;			
            return $clients;
        } else {
            return array();
        }
    }

    public function getClientChildren($parent_id) {
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeViewMove($parent_id);
        foreach ($resultSet as $value) {
            $client['id'] = $value['id'];
            $client['client_title'] = $value['client_title'];
            $client['parent_client_id'] = $value['parent_client_id'];
            $client['level'] = $value['level'];
            $client['children'] = $this->getClientChildren($value['id']);
            $clients[] = $client;
            //echo "<pre>";pr($clients);
        }
        return $clients;
    }

    /* Name :Prasad 
      date :27-07-17
      Subject : Client Move
     */

    /*
      Start
      Name:Veeru
      Comment: Fetching event_column master data and inserting into the event_column_rename table
     */

    public function columnEdit($client_id = NULL) {
        $client_id = decode_url($client_id);
        $data['rename_columns'] = $this->Model_client->getRenameColumns($client_id);
        $data['file'] = 'column_update_form';
        $this->load->view('template/front_template', $data);
    }

    public function ajaxRename() {
        $id = $_POST['id'];
        $text['rename_column'] = $_POST['rename_column'];
        //$this->input->post()
        if ($id != "") {
            $Status = $this->Model_client->updateColumn($text, $id);
            if ($Status['status'] == 'true') {
                $return['message'] = 'Column Name updated successfully';
                $return['status'] = 'true';
            }
        }
        echo $json = json_encode($return);
        die();
    }

    /*
      Name:Veeru
      Comment: Fetching event_column master data and inserting into the event_column_rename table
      End
     */

    function uploadClient($client_id = NULL) {
        $client_id = decode_url($client_id);
        //echo $client_id;exit;
        ini_set('auto_detect_line_endings', TRUE);
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';
        $this->load->library('upload', $config);
        // If upload failed, display error
        if (!$this->upload->do_upload()) {
            $data['clientName'] = $this->Model_client->getClientName($client_id);
            $data['error'] = $this->upload->display_errors();
            //exit;
            $data['file'] = 'bulkupload_form';
            $this->load->view('template/front_template', $data);
        } else {
            $file_data = $this->upload->data();
            $file_path = './uploads/' . $file_data['file_name'];
            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);
                //pr($csv_array);exit;
                foreach ($csv_array as $row) {
                    $insert_data = array(
                        'client_title' => $row['Client Title'],
                        'parent_client_id' => $client_id,
                        'intialize' => 1,
                        'created_on' => $this->current_date,
                        'created_by' => $this->current_user
                    );
                    $resultData = $this->Model_client->insertClient($insert_data);
                    $lastInsertId = $resultData['lastId'];
                    $columnsInsert = $this->Model_client->getColumnsInsert($lastInsertId);
                    /* Start new Log */
                    $getNewData = helper_LogFetchRecord('client', 'id', $lastInsertId);
                    $newValue = $getNewData['log'];
                    $actions = 'CREATE';
                    $oldValue = "";
                    helper_createLogActions('client_log', $lastInsertId, $actions, $oldValue, $newValue);
                    /* Start new Log */
                }
                $this->session->set_flashdata('success_msg', 'Csv Data Imported Succesfully');
                redirect(base_url() . 'partner/uploadpartner/' . encode_url($client_id));
            } else {
                $data['error'] = "Error occured";
                $data['file'] = 'bulkupload_form';
                $data['clientNameclientName'] = $this->Model_client->getClientName($client_id);
                $this->load->view('template/front_template', $data);
            }
        }
    }

    public function clientDetails($client_id = NULL) {
        $client_id = decode_url($client_id);
        $data['file'] = 'details';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_actiontabs() {
        $client_id = decode_url($_POST['client_id']);
        $client_uuid = $_POST['client_uuid'];
        $controlClient = $_POST['controlClient'];
        $data['client_id'] = $client_id;
        $data['client_uuid'] = $client_uuid;
        $data['controlClient'] = $controlClient;

        //$data['file'] = 'ajax_actiontabs';
        $this->load->view('ajax_actiontabs', $data);
    }

    public function ajaxAddressRemove() {
        $address_uuid = $_POST['address_uuid'];
        $this->Model_client->deleteClientAddress($address_uuid);
    }

    public function updateUuids($table = NULL, $field = NULL, $id = NULL, $prefix = NULL) {
        /* $table= "eventedge_ack_status";
          $field = "ack_status_uuid";
          $id = "id";
          $prefix ="eeas"; */
        if ($table != NULL && $field != NULL && $id != NULL && $prefix != NULL) {
            $data['getclientids'] = $this->Model_client->getIds($table, $field, $id);
            if ($data['getclientids']['status'] == 'true') {
                $updatearray = array();
                foreach ($data['getclientids']['resultSet'] as $value) {
                    $client_uuid = generate_uuid($prefix . "_");
                    $updatearray[$field] = $client_uuid;
                    echo $value[$id] . "<br>";
                    pr($updatearray);
                    $this->Model_client->updateClientUuid($updatearray, $value[$id], $id, $table);
                }
            }
        } else {
            echo "some parameter are null";
        }
    }

}
