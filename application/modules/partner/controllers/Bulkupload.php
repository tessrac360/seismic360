<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bulkupload extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user, 'company_id' => $this->getCompanyId);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user, 'company_id' => $this->getCompanyId);
        $this->client_id = $this->session->userdata('client_id');
        $this->cDataDevice = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->load->model("client/Model_client");
        $this->load->model("client/Model_locations");
        $this->load->model("client/Model_contacts");
        $this->load->model("client/Model_devices");
        $this->load->model("gateways/Model_gateways");
        $this->load->model('roles/Module_roles');
        $this->load->library('form_validation');
        $this->load->library('Csvimport');
    }

    private function excelArray($FILES) {
        $file_info = pathinfo($FILES["userfile"]["name"]);
        $new_file_name = date("d-m-Y ") . rand(000000, 999999) . "." . $file_info["extension"];
        $file_type = PHPExcel_IOFactory::identify($FILES['userfile']['tmp_name']);
        $objReader = PHPExcel_IOFactory::createReader($file_type);
        $objPHPExcel = $objReader->load($FILES['userfile']['tmp_name']);
        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
            $worksheetTitle = $worksheet->getTitle();
            $highestRow = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            for ($row = 1; $row <= $highestRow; ++$row) {
                for ($col = 0; $col < $highestColumnIndex; ++$col) {
                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                    $val = $cell->getValue();
                    if ($row == 1) {
                        $myarraykey[$worksheetTitle][] = $val;
                    }
                    if ($row != 1) {
                        $myarrayvalue[$worksheetTitle][$row][] = $val;
                    }
                }
            }
        }
        foreach ($myarraykey as $keytop => $valtop) {
            // pr($valtop);
            $final[$keytop] = array();
            foreach ($myarrayvalue[$keytop] as $key => $valRecord) {

                $myarr = array();
                foreach ($valRecord as $k => $val) {

                    $myarr[$myarraykey[$keytop][$k]] = trim($val);
                }
                $final[$keytop][] = $myarr;
            }
        }
        return $final;
    }

    private function checkPartnerNotAvailable($data = array()) {
        $partnerNotAvailable = array();
        foreach ($data as $partValue) {
            if ($partValue['PARTNER NAME'] != "") {
                $this->db->select('id');
                $this->db->from('client');
                $this->db->where('client_title', $partValue['PARTNER NAME']);
                $this->db->where('parent_client_id', $_SESSION['client_id']);
                $this->db->where('client_type', 'P');
                $query = $this->db->get();
                if ($query->num_rows() == 0) {
                    $partnerNotAvailable[] = $partValue['PARTNER NAME'];
                }
            }
        }
        return $partnerNotAvailable;
    }

    public function index() {
        // pr($_SESSION); echo $this->getCompanyId;exit;
        if ($this->input->post()) {
            if ($_SESSION['user_type'] == 'P') {
                $this->load->library("excel");
                $fileName = explode('.', $this->input->post('filetype'));
                $final = $this->excelArray($_FILES);
                if ($fileName[0] == 'Partner' && isset($final['PARTNER'])) {
                    if (!empty($final['PARTNER'])) {
                        $returnPartner = $this->ImportPartner($final['PARTNER']);
                        $addPartner = $existPartner = '';
                        if (isset($returnPartner['existingClients'])) {
                            $existPartner = '</br><strong> Warning! </strong>Already Exists Partner : </br>' . implode('</br>', $returnPartner['existingClients']);
                        }
                        if (isset($returnPartner['AddedClients'])) {
                            $addPartner = '<strong> Success! </strong>Added Partner : </br>' . implode('</br>', $returnPartner['AddedClients']);
                        }
                        $this->session->set_flashdata("alert_msg", $addPartner . '    ' . $existPartner);
                        redirect('partner/bulkupload');
                    } else {
                        $this->session->set_flashdata("alert_msg", '<strong> Error! </strong>No record Found in ExcelSheet');
                        redirect('partner/bulkupload');
                    }
                } else if ($fileName[0] == 'Client' && isset($final['CLIENT'])) {
                    if (!empty($final['CLIENT'])) {
                        $returnPartner = $this->checkPartnerNotAvailable($final['CLIENT']);
                        if (!empty($returnPartner)) {
                            $partner = 'Below partner is not available </br> Before you import client first you import partner details : </br>' . implode(',', $returnPartner);
                            $this->session->set_flashdata("alert_msg", $partner);
                            redirect('partner/bulkupload');
                        } else {
                            $returnClient = $this->ImportClient($final['CLIENT']);
                            //pr($returnClient);exit;
                            $addClient = $existClient = $blankClient = '';
                            if (isset($returnClient['existingClients'])) {
                                $existClient = '</br><strong> Warning! </strong>Already Exists Clients  : </br>' . implode('</br>', $returnClient['existingClients']);
                            }
                            if (isset($returnClient['AddedClients'])) {
                                $addClient = '<strong> Success! </strong>Added Client : </br>' . implode('</br>', $returnClient['AddedClients']);
                            }

                            if (isset($returnClient['blankRecord'])) {
                                $blankClient = '</br><strong> Warning! </strong>Blank Records  : </br>' . implode('</br>', $returnClient['blankRecord']);
                            }
                            $this->session->set_flashdata("alert_msg", $addClient . '    ' . $existClient . '    ' . $blankClient);
                            redirect('partner/bulkupload');
                        }
//                        
                    } else {
                        $this->session->set_flashdata("alert_msg", '<strong> Error! </strong> No record Found in ExcelSheet');
                        redirect('partner/bulkupload');
                    }
                } else if ($fileName[0] == 'Gateway' && isset($final['GATEWAY'])) {

                    //pr($final['GATEWAY']);exit;
                    $isNullCheck = 'true';
                    $checkGateWay = array();
                    foreach ($final['GATEWAY'] as $value) {
                        if ($value['PARTNER NAME'] == "" || $value['GATEWAY TYPE'] == "" || $value['GATEWAY NAME'] == "" || $value['IP ADDRESS'] == "") {
                            $isNullCheck = 'false';
                        }
                    }

                    $checkGateWay = array();
                    foreach ($final['GATEWAY'] as $value) {
                        $arrGateWay = $this->isCheckGateTypeAvailable($value['GATEWAY TYPE']);
                        if ($arrGateWay['status'] == 'false') {
                            $checkGateWay['notAvalgateway'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['GATEWAY TYPE'];
                        }
                    }
                    if ($isNullCheck == 'true' && empty($checkGateWay)) {
                        $returnGateway = $this->ImportGateway($final['GATEWAY']);

                        $gateWayLocationNotAvailable = $notFoundPartnerClient = $addedGateway = $gatewayIpaddressexists = '';

                        if (isset($returnGateway['gateWayLocationNotAvailable'])) {
                            $gateWayLocationNotAvailable = '</br><strong> Warning! </strong>Location Not Available </br>' . implode('</br>', $returnGateway['gateWayLocationNotAvailable']);
                        }
                        if (isset($returnGateway['notFoundPartnerClient'])) {
                            $notFoundPartnerClient = '</br><strong> Error! </strong>Not Found Partner & Client Details : </br>' . implode('</br>', $returnGateway['notFoundPartnerClient']);
                        }

                        if (isset($returnGateway['gatewayIpaddressexists'])) {
                            $gatewayIpaddressexists = '</br><strong> Warning! </strong>IP Address is Already exists : </br>' . implode('</br>', $returnGateway['gatewayIpaddressexists']);
                        }

                        if (isset($returnGateway['addedGateway'])) {
                            $addedGateway = '</br><strong> Success! </strong>Added Gateway : </br>' . implode('</br>', $returnGateway['addedGateway']);
                        }


                        $this->session->set_flashdata("alert_msg", $addedGateway . '    ' . $notFoundPartnerClient . '    ' . $gateWayLocationNotAvailable . ' ' . $gatewayIpaddressexists);
                        redirect('partner/bulkupload');
                    } else if ($isNullCheck == 'false') {
                        $error_message = '<strong> Error! </strong>Please check Gateway Excel sheet. PARTNER NAME , CLIENT NAME ,GATEWAY TYPE,GATEWAY NAME,IP ADDRESS are required !!';
                        $this->session->set_flashdata("alert_msg", $error_message);
                        redirect('partner/bulkupload');
                    } else {
                        $error_message = '<strong> Warning! </strong>Gateway Type Not Available : </br>' . implode('</br>', $checkGateWay['notAvalgateway']);
                        $this->session->set_flashdata("alert_msg", $error_message);
                        redirect('partner/bulkupload');
                    }
                } else if ($fileName[0] == 'Contact' && isset($final['CONTACT'])) {
                    //pr($final['CONTACT']);exit;
                    $isNullCheck = 'true';
                    foreach ($final['CONTACT'] as $value) {
                        if ($value['PARTNER NAME'] == "" || $value['CONTACT NAME'] == "" || $value['FIRST NAME'] == "" || $value['PRIMARY_PHONENO_COUNTRYCODE']=="" || $value['PRIMARY_PHONE_NUMBER'] == "" || $value['PRIMARY EMAIL ADDRESS'] == "") {
                            $isNullCheck = 'false';
                        }
                    }
                    $checkPhoneNoExist = array();
                    foreach ($final['CONTACT'] as $value) {
                       $primayPhone_exist =  $this->isCheckContactPhoneNoAvl($value['PRIMARY_PHONENO_COUNTRYCODE'],$value['PRIMARY_PHONE_NUMBER']);
                        $secondaryPhone_exist = "";
                        $secondaryPhone_exist['status'] = 'false';
                        if($value['SECONDARY_PHONENO_COUNTRYCODE'] != '' && $value['SECONDARY PHONE NUMBER']){
                           $secondaryPhone_exist = $this->isCheckContactPhoneNoAvl($value['SECONDARY_PHONENO_COUNTRYCODE'],$value['SECONDARY PHONE NUMBER']);
                        }
                        if($primayPhone_exist['status']=='true' || $secondaryPhone_exist['status']=='true'){
                            
                            $phonePrimary = $phoneSecondary ="";
                            if($primayPhone_exist['status']=='true'){
                               $phonePrimary =   $value['PRIMARY_PHONENO_COUNTRYCODE'].' '.$value['PRIMARY_PHONE_NUMBER'];
                            }
                            
                            if($secondaryPhone_exist['status']=='true'){
                               $phoneSecondary =   $value['SECONDARY_PHONENO_COUNTRYCODE'].' '.$value['SECONDARY PHONE NUMBER'];
                            }
                            
                            $checkPhoneNoExist['contactPhoneNoExists'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] .' - '.$phonePrimary .' '. $phoneSecondary;
                        }
                       
                    }
                    
                   
                    /*Email Validation*/
                    $checkEmailExist = array();
                    foreach ($final['CONTACT'] as $value) {
                       $primayEmail_exist =  $this->isCheckContactEmailIdAvl($value['PRIMARY EMAIL ADDRESS']);
                        
                        $secondaryEmail_exist = "";
                        $secondaryEmail_exist['status'] = 'false';                                                
                        if($value['SECONDARY EMAIL ADDRESS'] != ''){
                           $secondaryEmail_exist = $this->isCheckContactEmailIdAvl($value['SECONDARY EMAIL ADDRESS']);

                        }
                        if($primayEmail_exist['status']=='true' || $secondaryEmail_exist['status']=='true'){                            
                            $emailPrimary = $emailSecondary ="";
                            if($primayEmail_exist['status']=='true'){
                               $emailPrimary =   $value['PRIMARY EMAIL ADDRESS'];
                            }
                            
                            if($secondaryEmail_exist['status']=='true'){
                               $emailSecondary =   $value['SECONDARY EMAIL ADDRESS'];
                            }                            
                            $checkEmailExist['contactEmailExists'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] .' - '.$emailPrimary .' '. $emailSecondary;
                        }
                       
                    }
                    
                                        
                    if ($isNullCheck == 'true' && empty($checkPhoneNoExist) && empty($checkEmailExist)) {
                        $returnLocation = $this->ImportContact($final['CONTACT']);
                        $notFoundPartnerClient = $addedContact = '';
                        if (isset($returnLocation['notFoundPartnerClient'])) {
                            $notFoundPartnerClient = '</br><strong> Warning! </strong>Not Found Partner & Client Details : </br>' . implode('</br>', $returnLocation['notFoundPartnerClient']);
                        }
                        if (isset($returnLocation['addedContact'])) {
                            $addedContact = '<strong> Success! </strong>Added Contact : </br>' . implode('</br>', $returnLocation['addedContact']);
                        }
                        $this->session->set_flashdata("alert_msg", $addedContact . '    ' . $notFoundPartnerClient);
                        redirect('partner/bulkupload');
                    } else if($isNullCheck == 'false') {
                        $error_message = '<strong> Warning! </strong>Please check Contact Excel sheet. PARTNER NAME , CLIENT NAME ,CONTACT NAME,FIRST NAME,PRIMARY_PHONENO_COUNTRYCODE,PRIMARY_PHONE_NUMBER and PRIMARY EMAIL ADDRESS are required !!';
                        $this->session->set_flashdata("alert_msg", $error_message);
                        redirect('partner/bulkupload');
                    }else if(!empty($checkPhoneNoExist)){
                        $error_message = '<strong> Warning! </strong>Contact Phone number is Already exists : </br>' . implode('</br>', $checkPhoneNoExist['contactPhoneNoExists']);
                        $this->session->set_flashdata("alert_msg", $error_message);
                        redirect('partner/bulkupload');
                    }else if(!empty($checkEmailExist)){
                        $error_message = '<strong> Warning! </strong>Emailid is Already exists : </br>' . implode('</br>', $checkEmailExist['contactEmailExists']);
                        $this->session->set_flashdata("alert_msg", $error_message);
                        redirect('partner/bulkupload');
                    }
                } else if ($fileName[0] == 'Location' && isset($final['LOCATION'])) {
                    //pr($final['LOCATION']);//exit;
                    $isNullCheck = 'true';
                    foreach ($final['LOCATION'] as $value) {
                        if ($value['PARTNER NAME'] == "" || $value['LOCATION NAME'] == "") {
                            $isNullCheck = 'false';
                        }
                    }
                    if ($isNullCheck == 'true') {
                        $returnLocation = $this->ImportLocation($final['LOCATION']);
                        $notFoundPartnerClient = $addedLocaton = $existsLocation = '';
                        if (isset($returnLocation['notFoundPartnerClient'])) {
                            $notFoundPartnerClient = '</br><strong> Warning! </strong> Not Found Partner & Client Details : </br>' . implode('</br>', $returnLocation['notFoundPartnerClient']);
                        }
                        if (isset($returnLocation['addedLocaton'])) {
                            $addedLocaton = '<strong> Success! </strong> Added Location :  </br>' . implode('</br>', $returnLocation['addedLocaton']);
                        }

                        if (isset($returnLocation['existsLocation'])) {
                            $existsLocation = '</br><strong> Warning! </strong> Already exists Location : </br>' . implode('</br>', $returnLocation['existsLocation']);
                        }

                        $this->session->set_flashdata("alert_msg", $addedLocaton . '    ' . $notFoundPartnerClient . '    ' . $existsLocation);
                        redirect('partner/bulkupload');
                    } else {
                        $error_message = '<strong> Error! </strong> Please check Location Excel sheet. PARTNER NAME , CLIENT NAME and LOCATION NAME are required !!';
                        $this->session->set_flashdata("alert_msg", $error_message);
                        redirect('partner/bulkupload');
                    }
                } else if ($fileName[0] == 'Device' && isset($final['DEVICE'])) { //DEVICE BULK UPLOAD
                    $isNullCheck = 'true';
                    foreach ($final['DEVICE'] as $value) {
                        //pr($value);
                        if (($value['PARTNER NAME'] == "") || ($value['GATEWAY'] == "") || ($value['DEVICE CATEGORY'] == "") || ($value['SERVICE GROUP'] == "") || ($value['DEVICE NAME'] == "") || ($value['DEVICE ADDRESS'] == "")) {
                            $isNullCheck = 'false';
                        }
                    }
                    if ($isNullCheck == 'true') {
                        $returnDevice = $this->ImportDevice($final['DEVICE']);
                        $notFoundPartnerClient = $notFoundGateway = $notFoundDeviceCategory = $notFoundDeviceSubCategory = $notFoundServiceGroup = $notFoundDeviceParent = $addedLocaton = $existsLocation = '';

                        if (isset($returnDevice['notFoundPartnerClient'])) {
                            $notFoundPartnerClient = '</br><strong> Warning! </strong> Not Found Partner & Client Details : </br>' . implode('</br>', $returnDevice['notFoundPartnerClient']);
                        }

                        if (isset($returnDevice['notFoundGateway'])) {
                            $notFoundGateway = '</br><strong> Warning! </strong> Not Found Gateway Details : </br>' . implode('</br>', $returnDevice['notFoundGateway']);
                        }

                        if (isset($returnDevice['notFoundDeviceCategory'])) {
                            $notFoundDeviceCategory = '</br><strong> Warning! </strong> Not Found Device Category Details : </br>' . implode('</br>', $returnDevice['notFoundDeviceCategory']);
                        }

                        if (isset($returnDevice['notFoundDeviceSubCategory'])) {
                            $notFoundDeviceSubCategory = '</br><strong> Warning! </strong> Not Found Device Sub Category Details : </br>' . implode('</br>', $returnDevice['notFoundDeviceSubCategory']);
                        }

                        if (isset($returnDevice['notFoundServiceGroup'])) {
                            $notFoundServiceGroup = '</br><strong> Warning! </strong> Not Found Service Group Details : </br>' . implode('</br>', $returnDevice['notFoundServiceGroup']);
                        }

                        if (isset($returnDevice['notFoundDeviceParent'])) {
                            $notFoundDeviceParent = '</br><strong> Warning! </strong> Not Found Device Parent Details based on this Client and Gateway : </br>' . implode('</br>', $returnDevice['notFoundDeviceParent']);
                        }

                        if (isset($returnDevice['addedDevice'])) {
                            $addedDevice = '<strong> Success! </strong> Added Device : </br>' . implode('</br>', $returnDevice['addedDevice']);
                        }

                        if (isset($returnDevice['existsDevice'])) {
                            $existsDevice = '</br><strong> Warning! </strong> Already exists Device : </br>' . implode('</br>', $returnDevice['existsDevice']);
                        }

                        $this->session->set_flashdata("alert_msg", $addedDevice . '    ' . $notFoundPartnerClient . '    ' . $notFoundGateway . '    ' . $notFoundDeviceCategory . '    ' . $notFoundDeviceSubCategory . '    ' . $notFoundServiceGroup . '    ' . $notFoundDeviceParent . '    ' . $existsDevice);
                        redirect('partner/bulkupload');
                    } else {
                        $error_message = '<strong> Warning! </strong> Please check Excel sheet. PARTNER NAME , CLIENT NAME, GATEWAY, DEVICE CATEGORY, SERVICE GROUP, DEVICE NAME and DEVICE ADDRESS is required !!';
                        $this->session->set_flashdata("alert_msg", $error_message);
                        redirect('partner/bulkupload');
                    }
                } else {
                    $this->session->set_flashdata("alert_msg", '<strong> Error! </strong> Wrong file format');
                    redirect('partner/bulkupload');
                }
            } else {
                $this->session->set_flashdata("alert_msg", '<strong> Error! </strong> Only partner level can import this file');
                redirect('partner/bulkupload');
            }
        } else {
            $data['importFile'] = array('Partner.xlsx' => "Partner", 'Client.xlsx' => "Client", 'Gateway.xlsx' => "Gateway", 'Contact.xlsx' => "Contact", 'Location.xlsx' => "Location", 'Device.xlsx' => "Device");
            $data['file'] = 'bulkupload/partner_client';
            $this->load->view('template/front_template', $data);
        }
    }
    
     private function isCheckContactPhoneNoAvl($countryCode = '',$phone_number) {
        $this->db->select('phone_id');
        $this->db->from('phone_numbers');
        $this->db->where('phone_number', $phone_number);
        $this->db->where('phone_country_code', $countryCode);
        $query = $this->db->get();
        echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';           
        } else {

            $return['status'] = 'false';
        }
        return $return;
    }
    
     private function isCheckContactEmailIdAvl($emailId = '') {
        $this->db->select('email_uuid');
        $this->db->from('contact_emails');
        $this->db->where('email_address', $emailId);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';           
        } else {    
            $return['status'] = 'false';
        }
        return $return;
    }

    private function isCheckGateTypeAvailable($value = '') {
        $this->db->select('gatewaytype_uuid');
        $this->db->from('gateway_types');
        $this->db->where('title', $value);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {

            $return['status'] = 'false';
        }
        return $return;
    }

    private function isUniqueCheckPartner($tablname = "", $key = "", $value = "") {
        $this->db->select('*');
        $this->db->from($tablname);
        $this->db->where($key, $value);
        $this->db->where('parent_client_id', $_SESSION['client_id']);
        $this->db->where('client_type', 'P');
        $query = $this->db->get();
        // echo $this->db->last_query();     
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {

            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    private function ImportPartner($data = array()) {
        foreach ($data as $key => $value) {
            if ($value['PARTNER NAME'] != "") {
                $returnStatus = $this->isUniqueCheckPartner('client', 'client_title', $value['PARTNER NAME']);
                if ($returnStatus['status'] == 'false') {

                    $this->db->select('level,parent_client_id,primary_client_id');
                    $this->db->from('client');
                    $this->db->where('id', $this->getCompanyId);
                    $query = $this->db->get();
                    // echo $this->db->last_query();     
                    if ($query->num_rows() > 0) {
                        $returnData = $query->row();
                        $level = $returnData->level;
                        $parent_client_id = $this->getCompanyId;
                        $primary_client_id = $returnData->primary_client_id;
                        $client_uuid = generate_uuid('clie_');
                        $ClientData['parent_client_id'] = $parent_client_id;
                        $ClientData['primary_client_id'] = $primary_client_id;
                        $ClientData['level'] = $level + 1;
                        $ClientData['client_title'] = $value['PARTNER NAME'];
                        $ClientData['name'] = ($value['DISPLAY NAME']) ? $value['DISPLAY NAME'] : $value['PARTNER NAME'];
                        $ClientData['client_erp_number'] = ($value['ERP NUMBER']) ? $value['ERP NUMBER'] : '';
                        $ClientData['client_crm_number'] = ($value['CRM NUMBER']) ? $value['CRM NUMBER'] : '';
                        $ClientData['client_url'] = ($value['URL']) ? $value['URL'] : '';
                        $ClientData['client_credit_limit'] = ($value['CREDIT LIMIT']) ? $value['CREDIT LIMIT'] : '';
                        $ClientData['client_discount'] = ($value['DISCOUNT']) ? $value['DISCOUNT'] : '';
                        $ClientData['client_domain'] = ($value['DOMAIN']) ? $value['DOMAIN'] : '';
                        $ClientData['client_uuid'] = $client_uuid;
                        $ClientData['client_salesperson_code'] = ($value['SALESPERSON CODE']) ? $value['SALESPERSON CODE'] : '';
                        $ClientData['client_ssr'] = ($value['SSR']) ? $value['SSR'] : '';
                        $ClientData['client_tax_exemption_no'] = ($value['TAX EXEMPTION NO']) ? $value['TAX EXEMPTION NO'] : '';
                        $ClientData['partner_id'] = $this->getCompanyId;
                        $ClientData['client_type'] = 'P';
                        $ClientData['intialize'] = '1';
						$ClientData['private_key'] = createSecretKey($client_uuid);
						$ClientData['public_key'] = $client_uuid;						
                        $ClientData = array_merge($ClientData, $this->cData);
                        //pr($ClientData);exit;
                        $resultData = $this->Model_client->insertClient($ClientData);
                        if ($resultData['status'] == 'true') {
                            /* End Default Device add */
                            $ClientDevice['uuid'] = generate_uuid('ediv_');
                            $ClientDevice['device_name'] = 'NA';
                            $ClientDevice['client_id'] = $resultData['lastId'];
                            $ClientDevice['client_uuid'] = $client_uuid;
							$ClientDevice['gateway_id'] = '0';
							$ClientDevice['gateway_uuid'] = 'eecg_00000000-0000-0000-0000-000000000000';
							$ClientDevice['device_category_id'] = '0';
							$ClientDevice['device_category_uuid'] = 'dcat_00000000-0000-0000-0000-000000000000';
							$ClientDevice['service_group_uuid'] = 'sgp_00000000-0000-0000-0000-000000000000';
                            $ClientDevice = array_merge($ClientDevice, $this->cDataDevice);
                            $DeviceData = $this->Model_client->insertDevice($ClientDevice);
                            /* End Default Device add */
                            /* Start Client Address */
                            $clientAddress[0]['full_address'] = ($value['PRIMARY ADDRESS']) ? $value['PRIMARY ADDRESS'] : '';
                            $clientAddress[0]['referrence_uuid'] = $client_uuid;
                            $clientAddress[0]['created_on'] = $this->current_date;
                            $clientAddress[0]['created_by'] = $this->current_user;
                            $clientAddress[0]['address_uuid'] = generate_uuid('addr_');
                            $clientAddress[0]['is_primary'] = 1;
                            $resultDataAddress = $this->Model_client->insertAddress($clientAddress);
                            /* End Client Address */

							/*
							  Name:Veeru
							  Comment: Fetching event_column master data and inserting into the event_column_rename table
							 */
							$columnsInsert = $this->Model_client->getColumnsInsert($resultData['lastId']);
							/*
							  Name:Veeru
							  Comment: Fetching event_column master data and inserting into the event_column_rename table
							 */	
                        }
                        $status['AddedClients'][] = $value['PARTNER NAME'];
                    }
                } else {
                    $status['existingClients'][] = $value['PARTNER NAME'];
                }
            }
        }
        return $status;
    }

    private function isUniqueCheckPartnerClient($partnerName = "", $clientName = "") {
        $this->db->select('clnt.client_title');
        $this->db->from('client as clnt');
        $this->db->join('client as part', 'part.id = clnt.parent_client_id');
        $this->db->where('part.client_title', $partnerName);
        $this->db->where('clnt.client_title', $clientName);
        $this->db->where('part.parent_client_id', $_SESSION['client_id']);
        $this->db->where('clnt.client_type', 'C');
        $query = $this->db->get();
        //echo $this->db->last_query() . '<br>';
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {

            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    private function getParentDetails($partnerName = "") {
        $this->db->select('id,level,parent_client_id,primary_client_id,client_uuid');
        $this->db->from('client');
        $this->db->where('parent_client_id', $this->getCompanyId);
        $this->db->where('client_title', $partnerName);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row();
            $return['status'] = 'true';
        } else {

            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    private function ImportClient($data = array()) {
        foreach ($data as $value) {
            if ($value['CLIENT NAME'] != "") {
                $returnStatus = $this->isUniqueCheckPartnerClient($value['PARTNER NAME'], $value['CLIENT NAME']);
                if ($returnStatus['status'] == 'false') {
                    $isCheckLevel = $this->getParentDetails($value['PARTNER NAME']);
                    if ($isCheckLevel['status'] == 'true') {
                        $returnData = $isCheckLevel['resultSet'];
                        $level = $returnData->level;
                        $parent_client_id = $returnData->id;
                        $primary_client_id = $returnData->primary_client_id;
                        $client_uuid = generate_uuid('clie_');
                        $ClientData['parent_client_id'] = $parent_client_id;
                        $ClientData['primary_client_id'] = $primary_client_id;
                        $ClientData['level'] = $level + 1;
                        $ClientData['client_title'] = ($value['CLIENT NAME']) ? $value['CLIENT NAME'] : '';
                        $ClientData['name'] = ($value['DISPLAY NAME']) ? $value['DISPLAY NAME'] : $value['CLIENT NAME'];
                        $ClientData['client_erp_number'] = ($value['ERP NUMBER']) ? $value['ERP NUMBER'] : '';
                        $ClientData['client_crm_number'] = ($value['CRM NUMBER']) ? $value['CRM NUMBER'] : '';
                        $ClientData['client_url'] = ($value['URL']) ? $value['URL'] : '';
                        $ClientData['client_credit_limit'] = ($value['CREDIT LIMIT']) ? $value['CREDIT LIMIT'] : '';
                        $ClientData['client_discount'] = ($value['DISCOUNT']) ? $value['DISCOUNT'] : '';
                        $ClientData['client_domain'] = ($value['DOMAIN']) ? $value['DOMAIN'] : '';
                        $ClientData['client_uuid'] = $client_uuid;
                        $ClientData['client_salesperson_code'] = ($value['SALESPERSON CODE']) ? $value['SALESPERSON CODE'] : '';
                        $ClientData['client_ssr'] = ($value['SSR']) ? $value['SSR'] : '';
                        $ClientData['client_tax_exemption_no'] = ($value['TAX EXEMPTION NO']) ? $value['TAX EXEMPTION NO'] : '';
                        $ClientData['partner_id'] = $this->getCompanyId;
                        $ClientData['client_type'] = 'C';
						$ClientData['private_key'] = createSecretKey($client_uuid);
						$ClientData['public_key'] = $client_uuid;						
                        $ClientData = array_merge($ClientData, $this->cData);
                        //  pr($ClientData);exit;
                        $resultData = $this->Model_client->insertClient($ClientData);
                        //pr($resultData);exit;
                        if ($resultData['status'] == 'true') {
                            /* End Default Device add */
                            $ClientDevice['uuid'] = generate_uuid('ediv_');
                            $ClientDevice['device_name'] = 'NA';
                            $ClientDevice['client_id'] = $resultData['lastId'];
                            $ClientDevice['client_uuid'] = $client_uuid;
							$ClientDevice['gateway_id'] = '0';
							$ClientDevice['gateway_uuid'] = 'eecg_00000000-0000-0000-0000-000000000000';
							$ClientDevice['device_category_id'] = '0';
							$ClientDevice['device_category_uuid'] = 'dcat_00000000-0000-0000-0000-000000000000';
							$ClientDevice['service_group_uuid'] = 'sgp_00000000-0000-0000-0000-000000000000';
                            $ClientDevice = array_merge($ClientDevice, $this->cDataDevice);
                            $DeviceData = $this->Model_client->insertDevice($ClientDevice);
                            /* End Default Device add */
                            /* Start Client Address */
                            $clientAddress[0]['full_address'] = ($value['PRIMARY ADDRESS']) ? $value['PRIMARY ADDRESS'] : '';
                            $clientAddress[0]['referrence_uuid'] = $client_uuid;
                            $clientAddress[0]['created_on'] = $this->current_date;
                            $clientAddress[0]['created_by'] = $this->current_user;
                            $clientAddress[0]['address_uuid'] = generate_uuid('addr_');
                            $clientAddress[0]['is_primary'] = 1;
                            $resultDataAddress = $this->Model_client->insertAddress($clientAddress);
							/*
							  Name:Veeru
							  Comment: Fetching event_column master data and inserting into the event_column_rename table
							 */
							$columnsInsert = $this->Model_client->getColumnsInsert($resultData['lastId']);
							/*
							  Name:Veeru
							  Comment: Fetching event_column master data and inserting into the event_column_rename table
							 */	                            /* End Client Address */
							
                        }
                        $status['AddedClients'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'];
                    }
                } else {
                    $status['existingClients'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'];
                }
            } else {
                $status['blankRecord'][] = $value['PARTNER NAME'] . '  -  No Client Record Found!!';
            }
        }
//        pr($status);
//        exit;
        return $status;
    }

    public function checkClientWiseLocation($locationName = "", $referrence_uuid = "") {
        $this->db->select('location_uuid');
        $this->db->from('client_locations');
        $this->db->where('location_name', $locationName);
        $this->db->where('referrence_uuid', $referrence_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    private function checkUniqueIPAddressGateway($clientId = "", $ipAddress = "") {
        $this->db->select('id');
        $this->db->from('client_gateways');
        $this->db->where('client_id', $clientId);
        $this->db->where('ip_address', $ipAddress);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    private function ImportGateway($data = array()) {
        foreach ($data as $value) {
            if ($value['CLIENT NAME'] == "") {
                $returnStatus = $this->isUniqueCheckPartner('client', 'client_title', $value['PARTNER NAME']);
                if ($returnStatus['status'] == 'true') {
                    $isCheckLevel = $this->getParentDetails($value['PARTNER NAME']);
                    if ($isCheckLevel['status'] == 'true') {
                        $returnData = $isCheckLevel['resultSet'];
                        $reference_uuid = $returnData->client_uuid;
                        $ClientId = $returnData->id;
                        $ischeckUniqueIPAddressGateway = $this->checkUniqueIPAddressGateway($ClientId, $value['IP ADDRESS']);
                        if ($ischeckUniqueIPAddressGateway['status'] == 'false') {
                            $uuidGateType = $this->isCheckGateTypeAvailable($value['GATEWAY TYPE']);
                            $gatewayPost = array();
                            if ($value['GATEWAY LOCATION'] == "") {
                                $gatewayPost['client_id'] = $ClientId;
                                $gatewayPost['client_uuid'] = $reference_uuid;
                                $gatewayPost['gateway_uuid'] = generate_uuid('eecg_');
                                $gatewayPost['private_key'] = createSecretKey($gatewayPost['gateway_uuid']);
                                $gatewayPost['created_on'] = $this->current_date;
                                $gatewayPost['created_by'] = $this->current_user;
                                $gatewayPost['title'] = ($value['GATEWAY NAME']) ? $value['GATEWAY NAME'] : '';
                                $gatewayPost['ip_address'] = ($value['IP ADDRESS']) ? $value['IP ADDRESS'] : '';
                                $gatewayPost['gateway_sync_port'] = ($value['SYNC PORT']) ? $value['SYNC PORT'] : '';
                                $gatewayPost['gateway_remote_port'] = ($value['REMOTE PORT']) ? $value['REMOTE PORT'] : '';
                                $gatewayPost['gateway_email'] = ($value['EMAIL']) ? $value['EMAIL'] : '';
                                $gatewayPost['gateway_password'] = ($value['PASSWORD']) ? $value['PASSWORD'] : '';
                                $gatewayPost['gateway_type'] = $uuidGateType['resultSet']->gatewaytype_uuid;
                                $gatewayPost['gateway_location'] = '';
                                $this->Model_gateways->insertGateway($gatewayPost);
                                $status['addedGateway'][] = $value['PARTNER NAME'] . '  -  ' . $value['GATEWAY NAME'];
                            } else {
                                $chkLocation = $this->checkClientWiseLocation($value['GATEWAY LOCATION'], $reference_uuid);
                                if ($chkLocation['status'] == 'true') {
                                    $gatewayPost['client_id'] = $ClientId;
                                    $gatewayPost['client_uuid'] = $reference_uuid;
                                    $gatewayPost['gateway_uuid'] = generate_uuid('eecg_');
                                    $gatewayPost['private_key'] = createSecretKey($gatewayPost['gateway_uuid']);
                                    $gatewayPost['created_on'] = $this->current_date;
                                    $gatewayPost['created_by'] = $this->current_user;
                                    $gatewayPost['title'] = ($value['GATEWAY NAME']) ? $value['GATEWAY NAME'] : '';
                                    $gatewayPost['ip_address'] = ($value['IP ADDRESS']) ? $value['IP ADDRESS'] : '';
                                    $gatewayPost['gateway_sync_port'] = ($value['SYNC PORT']) ? $value['SYNC PORT'] : '';
                                    $gatewayPost['gateway_remote_port'] = ($value['REMOTE PORT']) ? $value['REMOTE PORT'] : '';
                                    $gatewayPost['gateway_email'] = ($value['EMAIL']) ? $value['EMAIL'] : '';
                                    $gatewayPost['gateway_password'] = ($value['PASSWORD']) ? $value['PASSWORD'] : '';
                                    $gatewayPost['gateway_type'] = $uuidGateType['resultSet']->gatewaytype_uuid;
                                    $gatewayPost['gateway_location'] = $chkLocation['resultSet']->location_uuid;
                                    $this->Model_gateways->insertGateway($gatewayPost);
                                    $status['addedGateway'][] = $value['PARTNER NAME'] . '  -  ' . $value['GATEWAY NAME'];
                                } else {
                                    $status['gateWayLocationNotAvailable'][] = $value['PARTNER NAME'] . '  -  ' . $value['GATEWAY LOCATION'];
                                }
                            }
                        } else {
                            $status['gatewayIpaddressexists'][] = $value['PARTNER NAME'] . '  -  ' . $value['IP ADDRESS'];
                        }
                    }
                } else {
                    $status['notFoundPartnerClient'][] = $value['PARTNER NAME'];
                }
            } else {
                $returnStatus = $this->isUniqueCheckPartnerClient($value['PARTNER NAME'], $value['CLIENT NAME']);
                if ($returnStatus['status'] == 'true') {
                    $isCheckLevel = $this->getClientDetails($value['PARTNER NAME'], $value['CLIENT NAME']);
                    if ($isCheckLevel['status'] == 'true') {
                        $returnData = $isCheckLevel['resultSet'];
                        $reference_uuid = $returnData->client_uuid;
                        $ClientId = $returnData->id;
                        $ischeckUniqueIPAddressGateway = $this->checkUniqueIPAddressGateway($ClientId, $value['IP ADDRESS']);
                        if ($ischeckUniqueIPAddressGateway['status'] == 'false') {
                            $uuidGateType = $this->isCheckGateTypeAvailable($value['GATEWAY TYPE']);
                            $gatewayPost = array();
                            if ($value['GATEWAY LOCATION'] == "") {
                                $gatewayPost['client_id'] = $ClientId;
                                $gatewayPost['client_uuid'] = $reference_uuid;
                                $gatewayPost['gateway_uuid'] = generate_uuid('eecg_');
                                $gatewayPost['private_key'] = createSecretKey($gatewayPost['gateway_uuid']);
                                $gatewayPost['created_on'] = $this->current_date;
                                $gatewayPost['created_by'] = $this->current_user;
                                $gatewayPost['title'] = ($value['GATEWAY NAME']) ? $value['GATEWAY NAME'] : '';
                                $gatewayPost['ip_address'] = ($value['IP ADDRESS']) ? $value['IP ADDRESS'] : '';
                                $gatewayPost['gateway_sync_port'] = ($value['SYNC PORT']) ? $value['SYNC PORT'] : '';
                                $gatewayPost['gateway_remote_port'] = ($value['REMOTE PORT']) ? $value['REMOTE PORT'] : '';
                                $gatewayPost['gateway_email'] = ($value['EMAIL']) ? $value['EMAIL'] : '';
                                $gatewayPost['gateway_password'] = ($value['PASSWORD']) ? $value['PASSWORD'] : '';
                                $gatewayPost['gateway_type'] = $uuidGateType['resultSet']->gatewaytype_uuid;
                                $gatewayPost['gateway_location'] = '';
                                $this->Model_gateways->insertGateway($gatewayPost);
                                $status['addedGateway'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['GATEWAY NAME'];
                            } else {
                                $chkLocation = $this->checkClientWiseLocation($value['GATEWAY LOCATION'], $reference_uuid);
                                if ($chkLocation['status'] == 'true') {
                                    $gatewayPost['client_id'] = $ClientId;
                                    $gatewayPost['client_uuid'] = $reference_uuid;
                                    $gatewayPost['gateway_uuid'] = generate_uuid('eecg_');
                                    $gatewayPost['private_key'] = createSecretKey($gatewayPost['gateway_uuid']);
                                    $gatewayPost['created_on'] = $this->current_date;
                                    $gatewayPost['created_by'] = $this->current_user;
                                    $gatewayPost['title'] = ($value['GATEWAY NAME']) ? $value['GATEWAY NAME'] : '';
                                    $gatewayPost['ip_address'] = ($value['IP ADDRESS']) ? $value['IP ADDRESS'] : '';
                                    $gatewayPost['gateway_sync_port'] = ($value['SYNC PORT']) ? $value['SYNC PORT'] : '';
                                    $gatewayPost['gateway_remote_port'] = ($value['REMOTE PORT']) ? $value['REMOTE PORT'] : '';
                                    $gatewayPost['gateway_email'] = ($value['EMAIL']) ? $value['EMAIL'] : '';
                                    $gatewayPost['gateway_password'] = ($value['PASSWORD']) ? $value['PASSWORD'] : '';
                                    $gatewayPost['gateway_type'] = $uuidGateType['resultSet']->gatewaytype_uuid;
                                    $gatewayPost['gateway_location'] = $chkLocation['resultSet']->location_uuid;
                                    $this->Model_gateways->insertGateway($gatewayPost);
                                    $status['addedGateway'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['GATEWAY NAME'];
                                } else {
                                    $status['gateWayLocationNotAvailable'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['GATEWAY LOCATION'];
                                }
                            }
                        } else {
                            $status['gatewayIpaddressexists'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['IP ADDRESS'];
                        }
                    }
                } else {
                    $status['notFoundPartnerClient'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'];
                }
            }
        }
        return $status;
    }

    private function isCheckDuplicateContact($clientUUID = "", $location = "") {
        $this->db->select('location_id');
        $this->db->from('client_locations');
        $this->db->where('referrence_uuid', $clientUU);
        $this->db->where('location_name', $location);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {

            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    private function ImportContact($data = array()) {
        foreach ($data as $value) {
            if ($value['CLIENT NAME'] == "") {
                $returnStatus = $this->isUniqueCheckPartner('client', 'client_title', $value['PARTNER NAME']);
                if ($returnStatus['status'] == 'true') {
                    $isCheckLevel = $this->getParentDetails($value['PARTNER NAME']);
                    if ($isCheckLevel['status'] == 'true') {
                        $returnData = $isCheckLevel['resultSet'];
                        $reference_uuid = $returnData->client_uuid;
                        /* Start Phone Number & Email Id */
                        if ($value['PRIMARY_PHONE_NUMBER'] == $value['SECONDARY PHONE NUMBER']) {
                            $value['SECONDARY PHONE NUMBER'] = "";
                        }

                        if ($value['PRIMARY EMAIL ADDRESS'] == $value['SECONDARY EMAIL ADDRESS']) {
                            $value['PRIMARY EMAIL ADDRESS'] = "";
                        }
                        /* End Phone Number & Email Id */

                        $ContactData = array();
                        $contact_uuid = generate_uuid('cont_');
                        $ContactData['contact_uuid'] = $contact_uuid;
                        $ContactData['reference_uuid'] = $reference_uuid;
                        $ContactData['contact_name'] = ($value['CONTACT NAME']) ? $value['CONTACT NAME'] : '';
                        $ContactData['first_name'] = ($value['FIRST NAME']) ? $value['FIRST NAME'] : '';
                        $ContactData['second_name'] = ($value['SECOND NAME']) ? $value['SECOND NAME'] : '';
                        $ContactData['last_name'] = ($value['LAST NAME']) ? $value['LAST NAME'] : '';
                        $ContactData['full_name'] = $value['FIRST NAME'] . ' ' . $value['SECOND NAME'] . ' ' . $value['LAST NAME'];
                        $ContactData['created_on'] = $this->current_date;
                        $ContactData['created_by'] = $this->current_user;
                        //pr($ContactData);
                        $resultContactData = $this->Model_contacts->insertContacts($ContactData);
                        if ($resultContactData['status'] == 'true') {
                            $EmailData = array();
                            if ($value['PRIMARY EMAIL ADDRESS'] != "") {
                                $EmailData[0]['email_uuid'] = generate_uuid('emai_');
                                $EmailData[0]['reference_uuid'] = $contact_uuid;
                                $EmailData[0]['email_address'] = ($value['PRIMARY EMAIL ADDRESS']) ? $value['PRIMARY EMAIL ADDRESS'] : '';
                                $EmailData[0]['email_category'] = 'work';
                                $EmailData[0]['is_primary'] = 1;
                                $EmailData[0]['created_on'] = $this->current_date;
                                $EmailData[0]['created_by'] = $this->current_user;
                            }
                            if ($value['SECONDARY EMAIL ADDRESS'] != "") {
                                $EmailData[1]['email_uuid'] = generate_uuid('emai_');
                                $EmailData[1]['reference_uuid'] = $contact_uuid;
                                $EmailData[1]['email_address'] = ($value['SECONDARY EMAIL ADDRESS']) ? $value['SECONDARY EMAIL ADDRESS'] : '';
                                $EmailData[1]['email_category'] = 'work';
                                $EmailData[1]['is_primary'] = 0;
                                $EmailData[1]['created_on'] = $this->current_date;
                                $EmailData[1]['created_by'] = $this->current_user;
                            }
                            // pr($locationAddress);exit;
                            if ($value['PRIMARY EMAIL ADDRESS'] != "" || $value['SECONDARY EMAIL ADDRESS'] != "") {
                                $resultEmailData = $this->Model_contacts->insertEmails($EmailData);
                            }

                            $PhoneData = array();
                            if ($value['PRIMARY_PHONE_NUMBER'] != "") {
                                $PhoneData[0]['phone_uuid'] = generate_uuid('phon_');
                                $PhoneData[0]['referrence_uuid'] = $contact_uuid;
                                $PhoneData[0]['phone_country_code'] = ($value['PRIMARY_PHONENO_COUNTRYCODE']) ? $value['PRIMARY_PHONENO_COUNTRYCODE'] : '';
                                $PhoneData[0]['phone_number'] = ($value['PRIMARY_PHONE_NUMBER']) ? $value['PRIMARY_PHONE_NUMBER'] : '';
                                $PhoneData[0]['pn_category'] = 'work';
                                $PhoneData[0]['is_primary'] = 1;
                                $PhoneData[0]['created_on'] = $this->current_date;
                                $PhoneData[0]['created_by'] = $this->current_user;
                            }
                            if ($value['SECONDARY PHONE NUMBER'] != "") {
                                $PhoneData[1]['phone_uuid'] = generate_uuid('phon_');
                                $PhoneData[1]['referrence_uuid'] = $contact_uuid;
                                $PhoneData[1]['phone_country_code'] = ($value['SECONDARY_PHONENO_COUNTRYCODE']) ? $value['SECONDARY_PHONENO_COUNTRYCODE'] : '';
                                $PhoneData[1]['phone_number'] = ($value['SECONDARY PHONE NUMBER']) ? $value['SECONDARY PHONE NUMBER'] : '';
                                $PhoneData[1]['pn_category'] = 'work';
                                $PhoneData[1]['is_primary'] = 0;
                                $PhoneData[1]['created_on'] = $this->current_date;
                                $PhoneData[1]['created_by'] = $this->current_user;
                            }
                           
                            if ($value['PRIMARY_PHONE_NUMBER'] != "" || $value['SECONDARY PHONE NUMBER'] != "") {
                                $resultPhoneData = $this->Model_contacts->insertPhones($PhoneData);
                            }

                            $AddressData = array();
                            if ($value['PRIMARY ADDRESS'] != "") {
                                $AddressData[0]['address_uuid'] = generate_uuid('addr_');
                                $AddressData[0]['referrence_uuid'] = $contact_uuid;
                                $AddressData[0]['full_address'] = ($value['PRIMARY ADDRESS']) ? $value['PRIMARY ADDRESS'] : '';
                                $AddressData[0]['is_primary'] = 1;
                                $AddressData[0]['created_on'] = $this->current_date;
                                $AddressData[0]['created_by'] = $this->current_user;
                            }
                            if ($value['SECONDARY ADDRESS'] != "") {
                                $AddressData[1]['address_uuid'] = generate_uuid('addr_');
                                $AddressData[1]['referrence_uuid'] = $contact_uuid;
                                $AddressData[1]['full_address'] = ($value['SECONDARY ADDRESS']) ? $value['SECONDARY ADDRESS'] : '';
                                $AddressData[1]['is_primary'] = 0;
                                $AddressData[1]['created_on'] = $this->current_date;
                                $AddressData[1]['created_by'] = $this->current_user;
                            }
                            if ($value['PRIMARY ADDRESS'] != "" || $value['SECONDARY ADDRESS'] != "") {
                                $resultDataAddress = $this->Model_contacts->insertAddress($AddressData);
                            }
                        }
                        $status['addedContact'][] = $value['PARTNER NAME'] . '  -  ' . $value['CONTACT NAME'];
                    }
                } else {
                    $status['notFoundPartnerClient'][] = $value['PARTNER NAME'];
                }
            } else {
                $returnStatus = $this->isUniqueCheckPartnerClient($value['PARTNER NAME'], $value['CLIENT NAME']);
                if ($returnStatus['status'] == 'true') {
                    $isCheckLevel = $this->getClientDetails($value['PARTNER NAME'], $value['CLIENT NAME']);
                    if ($isCheckLevel['status'] == 'true') {
                        $returnData = $isCheckLevel['resultSet'];
                        $reference_uuid = $returnData->client_uuid;

                        /* Start Phone Number & Email Id */
                        if ($value['PRIMARY_PHONE_NUMBER'] == $value['SECONDARY PHONE NUMBER']) {
                            $value['SECONDARY PHONE NUMBER'] = "";
                        }

                        if ($value['PRIMARY EMAIL ADDRESS'] == $value['SECONDARY EMAIL ADDRESS']) {
                            $value['PRIMARY EMAIL ADDRESS'] = "";
                        }
                        /* End Phone Number & Email Id */

                        $ContactData = array();
                        $contact_uuid = generate_uuid('cont_');
                        $ContactData['contact_uuid'] = $contact_uuid;
                        $ContactData['reference_uuid'] = $reference_uuid;
                        $ContactData['contact_name'] = ($value['CONTACT NAME']) ? $value['CONTACT NAME'] : '';
                        $ContactData['first_name'] = ($value['FIRST NAME']) ? $value['FIRST NAME'] : '';
                        $ContactData['second_name'] = ($value['SECOND NAME']) ? $value['SECOND NAME'] : '';
                        $ContactData['last_name'] = ($value['LAST NAME']) ? $value['LAST NAME'] : '';
                        $ContactData['full_name'] = $value['FIRST NAME'] . ' ' . $value['SECOND NAME'] . ' ' . $value['LAST NAME'];
                        $ContactData['created_on'] = $this->current_date;
                        $ContactData['created_by'] = $this->current_user;
                        //pr($ContactData);
                        $resultContactData = $this->Model_contacts->insertContacts($ContactData);
                        if ($resultContactData['status'] == 'true') {
                            $EmailData = array();
                            if ($value['PRIMARY EMAIL ADDRESS'] != "") {
                                $EmailData[0]['email_uuid'] = generate_uuid('emai_');
                                $EmailData[0]['reference_uuid'] = $contact_uuid;
                                $EmailData[0]['email_address'] = ($value['PRIMARY EMAIL ADDRESS']) ? $value['PRIMARY EMAIL ADDRESS'] : '';
                                $EmailData[0]['email_category'] = 'work';
                                $EmailData[0]['is_primary'] = 1;
                                $EmailData[0]['created_on'] = $this->current_date;
                                $EmailData[0]['created_by'] = $this->current_user;
                            }
                            if ($value['SECONDARY EMAIL ADDRESS'] != "") {
                                $EmailData[1]['email_uuid'] = generate_uuid('emai_');
                                $EmailData[1]['reference_uuid'] = $contact_uuid;
                                $EmailData[1]['email_address'] = ($value['SECONDARY EMAIL ADDRESS']) ? $value['SECONDARY EMAIL ADDRESS'] : '';
                                $EmailData[1]['email_category'] = 'work';
                                $EmailData[1]['is_primary'] = 0;
                                $EmailData[1]['created_on'] = $this->current_date;
                                $EmailData[1]['created_by'] = $this->current_user;
                            }
                            // pr($locationAddress);exit;
                            if ($value['PRIMARY EMAIL ADDRESS'] != "" || $value['SECONDARY EMAIL ADDRESS'] != "") {
                                $resultEmailData = $this->Model_contacts->insertEmails($EmailData);
                            }

                            $PhoneData = array();
                            if ($value['PRIMARY_PHONE_NUMBER'] != "") {
                                $PhoneData[0]['phone_uuid'] = generate_uuid('phon_');
                                $PhoneData[0]['referrence_uuid'] = $contact_uuid;
                                $PhoneData[0]['phone_country_code'] = ($value['PRIMARY_PHONENO_COUNTRYCODE']) ? $value['PRIMARY_PHONENO_COUNTRYCODE'] : '';
                                $PhoneData[0]['phone_number'] = ($value['PRIMARY_PHONE_NUMBER']) ? $value['PRIMARY_PHONE_NUMBER'] : '';
                                $PhoneData[0]['pn_category'] = 'work';
                                $PhoneData[0]['is_primary'] = 1;
                                $PhoneData[0]['created_on'] = $this->current_date;
                                $PhoneData[0]['created_by'] = $this->current_user;
                            }
                            if ($value['SECONDARY PHONE NUMBER'] != "") {
                                $PhoneData[1]['phone_uuid'] = generate_uuid('phon_');
                                $PhoneData[1]['referrence_uuid'] = $contact_uuid;
                                $PhoneData[1]['phone_country_code'] = ($value['SECONDARY_PHONENO_COUNTRYCODE']) ? $value['SECONDARY_PHONENO_COUNTRYCODE'] : '';
                                $PhoneData[1]['phone_number'] = ($value['SECONDARY PHONE NUMBER']) ? $value['SECONDARY PHONE NUMBER'] : '';
                                $PhoneData[1]['pn_category'] = 'work';
                                $PhoneData[1]['is_primary'] = 0;
                                $PhoneData[1]['created_on'] = $this->current_date;
                                $PhoneData[1]['created_by'] = $this->current_user;
                            }
                           
                            if ($value['PRIMARY_PHONE_NUMBER'] != "" || $value['SECONDARY PHONE NUMBER'] != "") {
                                $resultPhoneData = $this->Model_contacts->insertPhones($PhoneData);
                            }

                            $AddressData = array();
                            if ($value['PRIMARY ADDRESS'] != "") {
                                $AddressData[0]['address_uuid'] = generate_uuid('addr_');
                                $AddressData[0]['referrence_uuid'] = $contact_uuid;
                                $AddressData[0]['full_address'] = ($value['PRIMARY ADDRESS']) ? $value['PRIMARY ADDRESS'] : '';
                                $AddressData[0]['is_primary'] = 1;
                                $AddressData[0]['created_on'] = $this->current_date;
                                $AddressData[0]['created_by'] = $this->current_user;
                            }
                            if ($value['SECONDARY ADDRESS'] != "") {
                                $AddressData[1]['address_uuid'] = generate_uuid('addr_');
                                $AddressData[1]['referrence_uuid'] = $contact_uuid;
                                $AddressData[1]['full_address'] = ($value['SECONDARY ADDRESS']) ? $value['SECONDARY ADDRESS'] : '';
                                ;
                                $AddressData[1]['is_primary'] = 0;
                                $AddressData[1]['created_on'] = $this->current_date;
                                $AddressData[1]['created_by'] = $this->current_user;
                            }
                            if ($value['PRIMARY ADDRESS'] != "" || $value['SECONDARY ADDRESS'] != "") {
                                $resultDataAddress = $this->Model_contacts->insertAddress($AddressData);
                            }
                        }
                        $status['addedContact'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['CONTACT NAME'];
                    }
                } else {
                    $status['notFoundPartnerClient'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'];
                }
            }
        }
        return $status;
    }

    private function isCheckDuplicateLocation($clientUUID = "", $location = "") {
        $this->db->select('location_id');
        $this->db->from('client_locations');
        $this->db->where('referrence_uuid', $clientUUID);
        $this->db->where('location_name', $location);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {

            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    private function getClientDetails($partnerName = "", $clientName = "") {
        $this->db->select('clnt.client_uuid,clnt.id');
        $this->db->from('client as clnt');
        $this->db->join('client as part', 'part.id = clnt.parent_client_id');
        $this->db->where('part.client_title', $partnerName);
        $this->db->where('clnt.client_title', $clientName);
        $this->db->where('part.parent_client_id', $_SESSION['client_id']);
        $this->db->where('clnt.client_type', 'C');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row();
            $return['status'] = 'true';
        } else {

            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    private function ImportLocation($data = array()) {
        foreach ($data as $value) {
            if ($value['CLIENT NAME'] == "") {
                $returnStatus = $this->isUniqueCheckPartner('client', 'client_title', $value['PARTNER NAME']);
                if ($returnStatus['status'] == 'true') {
                    $isCheckLevel = $this->getParentDetails($value['PARTNER NAME']);
                    if ($isCheckLevel['status'] == 'true') {
                        $returnData = $isCheckLevel['resultSet'];
                        $client_uuid = $returnData->client_uuid;
                        $isCheckLocation = $this->isCheckDuplicateLocation($client_uuid, $value['LOCATION NAME']);
                        if ($isCheckLocation['status'] == 'false') {
                            $location_uuid = generate_uuid('loca_');
                            $locationData['location_uuid'] = $location_uuid;
                            $locationData['referrence_uuid'] = $returnData->client_uuid;
                            $locationData['created_on'] = $this->current_date;
                            $locationData['created_by'] = $this->current_user;
                            $locationData['location_name'] = ($value['LOCATION NAME']) ? $value['LOCATION NAME'] : '';
                            $locationData['location_short_name'] = ($value['LOCATION SHORT NAME']) ? $value['LOCATION SHORT NAME'] : $value['LOCATION NAME'];
                            $locationData['location_erp_number'] = ($value['LOCATION ERP NUMBER']) ? $value['LOCATION ERP NUMBER'] : '';
                            $locationData['location_crm_number'] = ($value['LOCATION CRM NUMBER']) ? $value['LOCATION CRM NUMBER'] : '';
                            $locationData['location_description'] = ($value['LOCATION DESCRIPTION']) ? $value['LOCATION DESCRIPTION'] : '';
                            $locationData['location_domain'] = ($value['LOCATION DOMAIN']) ? $value['LOCATION DOMAIN'] : '';
                            $locationData['location_salesperson_code'] = ($value['LOCATION SALESPERSON CODE']) ? $value['LOCATION SALESPERSON CODE'] : '';
                            $locationData['location_site_id'] = ($value['LOCATION SITE ID']) ? $value['LOCATION SITE ID'] : '';
                            // pr($locationData);
                            $resultData = $this->Model_locations->insertLocation($locationData);
                            if ($resultData['status'] == 'true') {
                                $locationAddress = array();
                                if ($value['PRIMARY ADDRESS'] != "") {
                                    $locationAddress[0]['full_address'] = ($value['PRIMARY ADDRESS']) ? $value['PRIMARY ADDRESS'] : '';
                                    $locationAddress[0]['referrence_uuid'] = $locationData['location_uuid'];
                                    $locationAddress[0]['address_uuid'] = generate_uuid('addr_');
                                    $locationAddress[0]['is_primary'] = 1;
                                }
                                if ($value['SECONDARY ADDRESS'] != "") {
                                    $locationAddress[1]['full_address'] = ($value['SECONDARY ADDRESS']) ? $value['SECONDARY ADDRESS'] : '';
                                    $locationAddress[1]['referrence_uuid'] = $locationData['location_uuid'];
                                    $locationAddress[1]['address_uuid'] = generate_uuid('addr_');
                                    $locationAddress[1]['is_primary'] = 0;
                                }
                                // pr($locationAddress);exit;
                                if ($value['PRIMARY ADDRESS'] != "" || $value['SECONDARY ADDRESS'] != "") {
                                    $resultDataAddress = $this->Model_locations->insertAddress($locationAddress);
                                }
                            }
                            $status['addedLocaton'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['LOCATION NAME'];
                        } else {
                            $status['existsLocation'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['LOCATION NAME'];
                        }
                    }
                } else {
                    $status['notFoundPartnerClient'][] = $value['PARTNER NAME'];
                }
            } else {
                $returnStatus = $this->isUniqueCheckPartnerClient($value['PARTNER NAME'], $value['CLIENT NAME']);
                if ($returnStatus['status'] == 'true') {
                    $isCheckLevel = $this->getClientDetails($value['PARTNER NAME'], $value['CLIENT NAME']);
                    if ($isCheckLevel['status'] == 'true') {
                        $returnData = $isCheckLevel['resultSet'];
                        $client_uuid = $returnData->client_uuid;
                        $isCheckLocation = $this->isCheckDuplicateLocation($client_uuid, $value['LOCATION NAME']);
                        if ($isCheckLocation['status'] == 'false') {
                            $location_uuid = generate_uuid('loca_');
                            $locationData['location_uuid'] = $location_uuid;
                            $locationData['referrence_uuid'] = $returnData->client_uuid;
                            $locationData['created_on'] = $this->current_date;
                            $locationData['created_by'] = $this->current_user;
                            $locationData['location_name'] = ($value['LOCATION NAME']) ? $value['LOCATION NAME'] : '';
                            $locationData['location_short_name'] = ($value['LOCATION SHORT NAME']) ? $value['LOCATION SHORT NAME'] : $value['LOCATION NAME'];
                            $locationData['location_erp_number'] = ($value['LOCATION ERP NUMBER']) ? $value['LOCATION ERP NUMBER'] : '';
                            $locationData['location_crm_number'] = ($value['LOCATION CRM NUMBER']) ? $value['LOCATION CRM NUMBER'] : '';
                            $locationData['location_description'] = ($value['LOCATION DESCRIPTION']) ? $value['LOCATION DESCRIPTION'] : '';
                            $locationData['location_domain'] = ($value['LOCATION DOMAIN']) ? $value['LOCATION DOMAIN'] : '';
                            $locationData['location_salesperson_code'] = ($value['LOCATION SALESPERSON CODE']) ? $value['LOCATION SALESPERSON CODE'] : '';
                            $locationData['location_site_id'] = ($value['LOCATION SITE ID']) ? $value['LOCATION SITE ID'] : '';
                            // pr($locationData);
                            $resultData = $this->Model_locations->insertLocation($locationData);
                            if ($resultData['status'] == 'true') {
                                $locationAddress = array();
                                if ($value['PRIMARY ADDRESS'] != "") {
                                    $locationAddress[0]['full_address'] = ($value['PRIMARY ADDRESS']) ? $value['PRIMARY ADDRESS'] : '';
                                    $locationAddress[0]['referrence_uuid'] = $locationData['location_uuid'];
                                    $locationAddress[0]['address_uuid'] = generate_uuid('addr_');
                                    $locationAddress[0]['is_primary'] = 1;
                                }
                                if ($value['SECONDARY ADDRESS'] != "") {
                                    $locationAddress[1]['full_address'] = ($value['SECONDARY ADDRESS']) ? $value['SECONDARY ADDRESS'] : '';
                                    $locationAddress[1]['referrence_uuid'] = $locationData['location_uuid'];
                                    $locationAddress[1]['address_uuid'] = generate_uuid('addr_');
                                    $locationAddress[1]['is_primary'] = 0;
                                }
                                // pr($locationAddress);exit;
                                if ($value['PRIMARY ADDRESS'] != "" || $value['SECONDARY ADDRESS'] != "") {
                                    $resultDataAddress = $this->Model_locations->insertAddress($locationAddress);
                                }
                            }
                            $status['addedLocaton'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['LOCATION NAME'];
                        } else {
                            $status['existsLocation'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['LOCATION NAME'];
                        }
                    }
                } else {
                    $status['notFoundPartnerClient'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'];
                }
            }
        }
//                pr($status);
//        exit;
        return $status;
    }

    /* ###### DEVICE BULK UPLOAD CODE ######## */

    //**** Checking gateway exists for client first, if exists gateway id insert in eventedge_devices table. If not exists then check gateway exists for partner, if exists gateway id insert in eventedge_devices table. If not exists for partener also then return erro message ****//
    private function isGatewayExistsForClient($clientName = "", $gatewayName) {
        $this->db->select('clgt.id as gateway_id,clgt.gateway_uuid,clgt.gateway_type,clnt.parent_client_id as partner_id');
        $this->db->from('client_gateways as clgt');
        $this->db->join('client as clnt', 'clnt.id = clgt.client_id');
        $this->db->where('clnt.client_title', $clientName);
        $this->db->where('clgt.title', $gatewayName);
        $query = $this->db->get();
        $client_gateway = $query->row_array();
        $partner_id = $client_gateway['partner_id'];
        //echo $this->db->last_query().'<br>';     
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $this->db->select('clgt.id,clgt.gateway_uuid');
            $this->db->from('client_gateways as clgt');
            $this->db->join('client as clnt', 'clnt.id = clgt.client_id');
            $this->db->where('clgt.client_id', $partner_id);
            $this->db->where('clgt.title', $gatewayName);
            $query1 = $this->db->get();
            //echo $this->db->last_query().'<br>';
            if ($query1->num_rows() > 0) {
                $return['status'] = 'true';
                $return['resultSet'] = $query1->row_array();
            } else {
                $return['status'] = 'false';
            }
        }
        return $return;
    }

    //**** Checking device category exists in database or not, if exists then insert device category id in eventedge_devices table ****//
    private function isDeviceCategoryExists($deviceCategory = "") {
        $this->db->select('id,device_category_uuid');
        $this->db->from('device_categories');
        $this->db->where('category', $deviceCategory);
        $query = $this->db->get();
        //echo $this->db->last_query().'<br>';
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    //**** Checking device sub-category exists in database or not, if exists then insert device sub-category id in eventedge_devices table ****//
    private function getDeviceSubCategory($deviceCategory = "", $deviceSubCategory = "") {
        $this->db->select('dsc.id,dsc.uuid');
        $this->db->from('device_sub_categories as dsc');
        $this->db->join('device_categories as dc', 'dc.device_category_uuid = dsc.device_category_uuid');
        $this->db->where('dc.category', $deviceCategory);
        $this->db->where('dsc.subcategory', $deviceSubCategory);
        $query = $this->db->get();
        //echo $this->db->last_query().'<br>';
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    //**** Checking service group exists in database or not, if exists then insert service group id in eventedge_devices table ****//
    private function isServiceGroupExists($deviceCategory = "", $gateway_type_uuid = "", $serviceGroup = "") {
        $this->db->select('sg.service_group_uuid');
        $this->db->from('service_groups as sg');
        $this->db->join('device_categories as dc', 'dc.id = sg.device_category_id');
        $this->db->where('dc.category', $deviceCategory);
        $this->db->where('sg.service_group_name', $serviceGroup);
        $this->db->where('sg.gateway_type_uuid', $gateway_type_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query().'<br>';
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    //**** Checking Device Parent exists for client and gateway in device table or not, if exists then take that device id and uuid and insert in eventedge_devices table ****//
    private function getDeviceParent($clientName = "", $gatewayName = "", $deviceParent = "") {
        $this->db->select('d.device_id,d.uuid');
        $this->db->from('devices as d');
        $this->db->join('client as clnt', 'clnt.id = d.client_id');
        $this->db->join('client_gateways as clgt', 'clgt.id = d.gateway_id');
        $this->db->where('clnt.client_title', $clientName);
        $this->db->where('clgt.title', $gatewayName);
        $this->db->where('d.device_name', $deviceParent);
        $query = $this->db->get();
        //echo $this->db->last_query().'<br>';
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    //**** Checking Device Template exists or not, if exists then take that template id and uuid and insert in eventedge_devices table ****//
    private function getDeviceTemplate($deviceTemplate = "") {
        $this->db->select('dt.device_template_id,dt.uuid');
        $this->db->from('devices_templates as dt');
        $this->db->where('dt.name', $deviceTemplate);
        $query = $this->db->get();
        //echo $this->db->last_query().'<br>';
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    //**** Checking Device exists or not, if exists then we throw error like already exits, not exits then insert device in device table ****//
    private function isCheckDuplicateDevice($clientUUID = "", $device = "") {
        $this->db->select('device_id');
        $this->db->from('devices');
        $this->db->where('client_uuid', $clientUUID);
        $this->db->where('device_name', $device);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    /* Get Client uuid  by client title and partner title */

    private function getClientUuid($partnerName = "", $clientName = "") {
        $this->db->select('clnt.client_uuid,clnt.id');
        $this->db->from('client as clnt');
        $this->db->join('client as part', 'part.id = clnt.parent_client_id');
        $this->db->where('part.client_title', $partnerName);
        $this->db->where('clnt.client_title', $clientName);
        $this->db->where('clnt.client_type', 'C');
        $query = $this->db->get();
        // echo $this->db->last_query() . '<br>';
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    /* Get Client uuid  by partner title */

    private function getPartnerUuid($partnerName = "") {
        $this->db->select('clnt.client_uuid,clnt.id');
        $this->db->from('client as clnt');
        $this->db->where('clnt.client_title', $partnerName);
        $this->db->where('clnt.client_type', 'P');
        $query = $this->db->get();
        //echo $this->db->last_query() . '<br>';
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    //*** Import Functionality***//
    private function ImportDevice($data = array()) {
        $i = 0;
        foreach ($data as $value) {
            $i++;
            //echo $i."<br>";
            $isClient = true;
            if ($value['CLIENT NAME'] == "") {
                $isClient = false;
            }
            if ($isClient) {
                $returnPartnerClient = $this->isUniqueCheckPartnerClient($value['PARTNER NAME'], $value['CLIENT NAME']);
            } else {
                $returnPartnerClient['status'] = 'true';
            }
            if ($returnPartnerClient['status'] == 'true') {
                if ($isClient) {
                    $returnGateway = $this->isGatewayExistsForClient($value['CLIENT NAME'], $value['GATEWAY']);
                } else {
                    $returnGateway = $this->isGatewayExistsForClient($value['PARTNER NAME'], $value['GATEWAY']);
                }
                //pr($returnGateway);exit;
                if ($returnGateway['status'] == 'true') {
                    $returnDeviceCategory = $this->isDeviceCategoryExists($value['DEVICE CATEGORY']);
                    if ($returnDeviceCategory['status'] == 'true') {
                        $returnDeviceSubCategory = $this->getDeviceSubCategory($value['DEVICE CATEGORY'], $value['DEVICE SUB CATEGORY']);
                        if ($returnDeviceSubCategory['status'] == 'true' || $value['DEVICE SUB CATEGORY'] == "") {
                            $returnServiceGroup = $this->isServiceGroupExists($value['DEVICE CATEGORY'], $returnGateway['resultSet']['gateway_type'], $value['SERVICE GROUP']);
                            //pr($returnServiceGroup);exit;
                            if ($returnServiceGroup['status'] == 'true') {
                                if ($isClient) {
                                    $returnDeviceParent = $this->getDeviceParent($value['CLIENT NAME'], $value['GATEWAY'], $value['DEVICE PARENT']);
                                } else {
                                    $returnDeviceParent = $this->getDeviceParent($value['PARTNER NAME'], $value['GATEWAY'], $value['DEVICE PARENT']);
                                }
                                //pr($returnDeviceParent);exit;
                                if ($returnDeviceParent['status'] == 'true' || $value['DEVICE PARENT'] == "") {
                                    if ($returnGateway['resultSet']['gateway_type'] == 1) {
                                        $returnDeviceTemplate = $this->getDeviceTemplate($value['DEVICE TEMPLATE']);
                                        if ($returnDeviceTemplate['status'] == 'true') {
                                            $device_template_id = $returnDeviceTemplate['resultSet']['device_template_id'];
                                        } else {
                                            $device_template_id = 0;
                                        }
                                    } else {
                                        $device_template_id = 0;
                                    }
                                    //pr($returnDeviceTemplate);exit;
                                    if ($isClient) {
                                        $isCheckLevel = $this->getClientUuid($value['PARTNER NAME'], $value['CLIENT NAME']);
                                    } else {
                                        $isCheckLevel = $this->getPartnerUuid($value['PARTNER NAME']);
                                    }
                                    //pr($isCheckLevel);exit;
                                    if ($isCheckLevel['status'] == 'true') {
                                        $client_id = $isCheckLevel['resultSet']['id'];
                                        $client_uuid = $isCheckLevel['resultSet']['client_uuid'];
                                        $isCheckDevice = $this->isCheckDuplicateDevice($client_uuid, $value['DEVICE NAME']);
                                        //pr($isCheckDevice);exit;
                                        if ($isCheckDevice['status'] == 'false') {
                                            $device_uuid = generate_uuid('ediv_');
                                            $DeviceData['uuid'] = $device_uuid;
                                            $DeviceData['device_name'] = ($value['DEVICE NAME']) ? $value['DEVICE NAME'] : '';
											if(filter_var($value['DEVICE ADDRESS'], FILTER_VALIDATE_IP,FILTER_FLAG_NO_RES_RANGE)) {
												$DeviceData['address'] = ($value['DEVICE ADDRESS']) ? $value['DEVICE ADDRESS'] : '';
											}
											else {
												$this->session->set_flashdata("alert_msg", '<strong> Error! </strong> Not Valid IP Address');
												redirect('partner/bulkupload');
											}
                                            
                                            $DeviceData['client_id'] = $client_id;
                                            $DeviceData['device_template_id'] = $device_template_id;
                                            $DeviceData['client_uuid'] = $client_uuid;
                                            $DeviceData['gateway_id'] = $returnGateway['resultSet']['gateway_id'];
                                            $DeviceData['gateway_uuid'] = $returnGateway['resultSet']['gateway_uuid'];
                                            $DeviceData['device_category_id'] = $returnDeviceCategory['resultSet']['id'];
                                            $DeviceData['device_category_uuid'] = $returnDeviceCategory['resultSet']['device_category_uuid'];
                                            $DeviceData['device_sub_category'] = isset($returnDeviceSubCategory['resultSet']['id']) ? $returnDeviceSubCategory['resultSet']['id'] : 0;
                                            $DeviceData['device_sub_category_uuid'] = isset($returnDeviceSubCategory['resultSet']['uuid']) ? $returnDeviceSubCategory['resultSet']['uuid'] : "";
                                            $DeviceData['service_group_uuid'] = $returnServiceGroup['resultSet']['service_group_uuid'];
                                            $DeviceData['device_parent_id'] = isset($returnDeviceParent['resultSet']['device_id']) ? $returnDeviceParent['resultSet']['device_id'] : 0;
                                            $DeviceData['device_parent_uuid'] = isset($returnDeviceParent['resultSet']['uuid']) ? $returnDeviceParent['resultSet']['uuid'] : "";
                                            $DeviceData['device_location'] = ($value['LOCATION']) ? $value['LOCATION'] : '';
                                            $DeviceData['device_description'] = ($value['DEVICE DESCRIPTION']) ? $value['DEVICE DESCRIPTION'] : '';
                                            $DeviceData['device_mac_address'] = ($value['MAC ADDRESS']) ? $value['MAC ADDRESS'] : '';
                                            $DeviceData['device_serial_number'] = ($value['SERIAL NUMBER']) ? $value['SERIAL NUMBER'] : '';
                                            $DeviceData['device_adr_room'] = ($value['DEVICE ADDRESS ROOM']) ? $value['DEVICE ADDRESS ROOM'] : '';
                                            $DeviceData['device_adr_rack'] = ($value['ADDRESS RACK']) ? $value['ADDRESS RACK'] : '';
                                            $DeviceData['device_model_number'] = ($value['MODEL NUMBER']) ? $value['MODEL NUMBER'] : '';
                                            $DeviceData['device_manufacturer'] = ($value['DEVICE MANUFACTURER']) ? $value['DEVICE MANUFACTURER'] : '';
                                            $DeviceData['created_on'] = $this->current_date;
                                            $DeviceData['created_by'] = $this->current_user;

                                            $resultData = $this->Model_devices->insertDevice($DeviceData);
                                            $status['addedDevice'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['DEVICE NAME'];
                                        } else {
                                            $status['existsDevice'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['DEVICE NAME'];
                                        }
                                    }
                                } else {
                                    $status['notFoundDeviceParent'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['GATEWAY'] . '  -  ' . $value['DEVICE PARENT'];
                                }
                            } else {
                                $status['notFoundServiceGroup'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['SERVICE GROUP'];
                            }
                        } else {
                            $status['notFoundDeviceSubCategory'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['DEVICE SUB CATEGORY'];
                        }
                    } else {
                        $status['notFoundDeviceCategory'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['DEVICE CATEGORY'];
                    }
                } else {
                    $status['notFoundGateway'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'] . '  -  ' . $value['GATEWAY'];
                }
            } else {
                $status['notFoundPartnerClient'][] = $value['PARTNER NAME'] . '  -  ' . $value['CLIENT NAME'];
            }
        }
        return $status;
    }

}
