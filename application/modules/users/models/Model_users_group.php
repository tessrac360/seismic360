<?php

class Model_users_group extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'assign_group';
        $this->company_id = $this->session->userdata('company_id');
        $this->client_id = $this->session->userdata('client_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('modified_on' => $current_date, 'modified_by' => $current_user);
    }

    public function updateStatus($status = NULL, $id = NULL) {
        //echo $status .' ---- '.$id;//exit;
        if ($id != "") {
            if ($status == 'true') {
                $data = array('active_status' => 'Y');
            } else if ($status == 'false') {
                $data = array('active_status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('a_id', $id);
            $this->db->update($this->tablename, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

	public function getUsersGroups($search = '', $limit = 0, $start = 0) {
        $this->db->select("ag.*,cl.client_title,CONCAT_WS(' ', uc.first_name,uc.last_name) as username");
        $this->db->from('assign_group as ag');
        $this->db->join('client as cl','cl.id = ag.client_id');
        $this->db->join('users as uc','uc.id = ag.created_by');
		if ($search != "") {
            $this->db->like('group_name', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($limit == 0 && $start == 0) {
            return $query->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $query->result_array();
    }

    public function insertUsersGroup($postData = array()) {
        
        $this->db->insert('assign_group', $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateUsersGroup($postData = array(), $id) {
        $this->db->where('a_id', $id);
        $update_status = $this->db->update($this->tablename, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }
	
	
	public function getUsersGroupEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $this->db->where('a_id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

    
}

?>