<?php

class Model_partner extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    var $column_order = array('cl.client_title', 'LastName', 'usr.first_name', 'usr.phone', 'usr.email_id', 'rl.title');

    public function __construct() {
        parent::__construct();
        $this->tablename = 'users';
        $this->tablename_group = 'group';
        $this->tablename_group_location = 'group_location';
        $this->users_domain = 'users_domain';
        $this->users_assigned_group = 'users_assigned_group';
        $this->company_id = $this->session->userdata('company_id');
        $this->client_id = $this->session->userdata('client_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }
    
     public function getRoles($clientId = "") {
        $this->db->select('id,title');
        $this->db->from('role');
        $this->db->where('status', 'Y');
        $this->db->where('is_deleted', 'N');
        $this->db->where('level', '1');
        $this->db->where('client_id', $clientId);
        //$this->db->where('company_id', $this->getCompanyId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->result();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
    public function getSkill() {
        $this->db->select('skill');
        $this->db->from('users');
        $this->db->where('id', $this->userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['status'] = 'true';
            $data = $query->row_array();

            //$rows = implode(",", array_column($data, 'domain_id'));
            $return['status'] = 'true';
            $return['resultSet'] = explode(',', $data['skill']);
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    private function _get_datatables_query() {
        $getSkill = $this->getSkill();
        
         $partner_id = decode_url($_POST['partner_id']);
        $order = array('usr.id' => 'desc');
        $this->db->select('cl.client_title,usr.id,usr.first_name,usr.last_name,usr.phone,usr.active_status,usr.email_id,usr.is_login,rl.title,CONCAT(cuser.first_name, " ", cuser.last_name) AS username');
        $this->db->from('users as usr');
        $this->db->join('role as rl', 'rl.id = usr.role_id');
        $this->db->join('client as cl', 'cl.id = usr.client');
        $this->db->join('users as cuser', 'cuser.id = usr.created_by');
        $this->db->where('usr.partner_id', $partner_id);
        $this->db->where('usr.is_deleted != ', 'Y');
		$this->db->where('usr.user_type', 'P');
        if ($getSkill['status'] == 'true') {
            $this->db->group_start();
            foreach ($getSkill['resultSet'] as $value) {
                $this->db->or_where("FIND_IN_SET('" . $value . "', usr.skill)");
            }
            $this->db->group_end();
        }
        if ($_POST['search']['value']) {
            $search = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('usr.first_name', $search);
            $this->db->or_like('usr.last_name', $search);
            $this->db->or_like('usr.email_id', $search);
            $this->db->or_like('usr.phone', $search);
            $this->db->or_like('rl.title', $search);
            $this->db->or_like('cl.client_title', $search);
            $this->db->or_like('rl.title', $search);
            $this->db->or_like('cuser.first_name', $search);
            $this->db->or_like('cuser.last_name', $search);
            $this->db->group_end();
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables() {
        //pr($_POST);exit;
        $this->_get_datatables_query();
        //exit;
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
         //echo $this->db->last_query();exit;
        return $query->result();
    }

    public function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getClientPag($search = '', $client_id = 0, $limit = 0, $start = 0, $allClients = array(), $type = 0) {
        $this->db->select('cl.*, CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from('client as cl');
        $this->db->join('users as usr', 'usr.id = cl.created_by');
        $this->db->where('cl.client_type', 'P');
        $this->db->where('cl.parent_client_id', $client_id);
        $this->db->order_by('cl.id', "asc");
        if ($start >= 0 and $limit > 0 and $type == 0 and $search == '') {
            $this->db->limit($limit, $start);
        }

        $clients = $this->db->get();
        //echo $this->db->last_query().'<br>';//exit;		
        if ($limit == 0 && $start == 0 && $type == 0) {
            return $clients->num_rows();
        }
        foreach ($clients->result_array() as $client) {

            $allClients[] = $client;
            $allClients = $this->getClientPag($search, $client['id'], 0, 0, $allClients, 1);
        }
        //exit;
        return $allClients;
    }

    public function getPartner($pat_id = "") {
        $this->db->select('id,client_title');
        $this->db->from('client');
        $this->db->where('client_type', 'P');
        $this->db->where('id', $pat_id);
        $query = $this->db->get();

        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->result();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
     private function getRoleUUID($roleId) {
        $this->db->select('uuid');
        $this->db->from('role');
        $this->db->where('status', 'Y');
        $this->db->where('is_deleted', 'N');
        $this->db->where('id', $roleId);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $clientId = $query->row();
            $uuid = $clientId->uuid;
        } else {
            $uuid = '';
        }
        return $uuid;
    }

    private function getClientUUID($clientId) {
        $this->db->select('client_uuid');
        $this->db->from('client');
        $this->db->where('status', 'Y');      
        $this->db->where('id', $clientId);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $clientId = $query->row();
            $uuid = $clientId->client_uuid;
        } else {
            $uuid = '';
        }
        return $uuid;
    }

    public function insertPartner($postData = array()) {
        unset($postData['group_title']);
        unset($postData['notification_sms']);
        unset($postData['notification_email']);
        unset($postData['technology']);
        // pr($_POST);
        $postData['client'] = $postData['client_id'];
        $postData['partner_id'] = $postData['client_id'];
        $postData['uuid'] = generate_uuid('eeur_');
        /* End */
        unset($postData['client_id']);
        $postData['assigned_role_id'] = $this->roleId;
        //$postData['password'] = md5($postData['password']);
        $postData['password'] = md5(123456);
        $postData['user_type'] = decode_url($postData['user_type']);
        $postData['company_id'] = $this->getCompanyId;
        $postData['assign_client_type'] = 'P';
        $postData['role_uuid'] = $this->getRoleUUID($this->roleId);
        $postData['client_uuid'] = $this->getClientUUID($postData['client']);
        //pr($postData);exit;
        $this->db->insert($this->tablename, $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	public function getClientName($partner_id ="") {

        $this->db->select('client_title');
        $this->db->from('client');
        $this->db->where('id', $partner_id);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $name = $row['client_title'];
        } else {
            $name = '';
        }
        return $name;
    }

}

?>