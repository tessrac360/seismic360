<?php
class Model_indivisualrecords extends CI_Model {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();       
        $this->company_id = $this->session->userdata('company_id');
        $this->client_id = $this->session->userdata('client_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }
    
	
	public function getClientInformation($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('id,client_uuid,client_title,parent_client_id,status,company_id,partner_id');
                $this->db->from('client');               
                if ($key == 'UUID') {
                    $this->db->where('client_uuid', $value);
                } else if ($key == 'ID') {
                    $this->db->where('id', $value);
                }
                $query = $this->db->get();               
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	
	public function getClientLocationInformation($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('location_id,location_uuid,location_name');
                $this->db->from('client_locations');               
                if ($key == 'UUID') {
                    $this->db->where('location_uuid', $value);
                } else if ($key == 'ID') {
                    $this->db->where('location_id', $value);
                }
                $query = $this->db->get();               
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	
	public function getIncidentStausName($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('status_id,uuid,status,status_code');
                $this->db->from('ticket_status');               
                if ($key == 'UUID') {
                    $this->db->where('status_code', $value);
                } else if ($key == 'ID') {
                    $this->db->where('status_code', $value);
                }
                $query = $this->db->get();               
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getdevicesInfo($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('device_id,uuid,device_name,gateway_id,gateway_uuid,status');
                $this->db->from('devices');               
                if ($key == 'UUID') {
                    $this->db->where('uuid', $value);
                } else if ($key == 'ID') {
                    $this->db->where('device_id', $value);
                }
                $query = $this->db->get();               
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	
	public function getservicesInfo($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('service_id,uuid,service_description');
                $this->db->from('services');               
                if ($key == 'UUID') {
                    $this->db->where('uuid', $value);
                } else if ($key == 'ID') {
                    $this->db->where('service_id', $value);
                }
                $query = $this->db->get();               
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getDeviceCateInfo($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('id,category,device_category_uuid,status');
                $this->db->from('device_categories');               
                if ($key == 'UUID') {
                    $this->db->where('device_category_uuid', $value);
                } else if ($key == 'ID') {
                    $this->db->where('id', $value);
                }
                $query = $this->db->get();               
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getDeviceSubCateInfo($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('id,uuid,subcategory,status,device_category_uuid,device_category_id');
                $this->db->from('device_sub_categories');               
                if ($key == 'UUID') {
                    $this->db->where('uuid', $value);
                } else if ($key == 'ID') {
                    $this->db->where('id', $value);
                }
                $query = $this->db->get();               
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getUrgencyInfo($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('urgency_id,uuid,name');
                $this->db->from('ticket_urgency');               
                if ($key == 'UUID') {
                    $this->db->where('uuid', $value);
                } else if ($key == 'ID') {
                    $this->db->where('urgency_id', $value);
                }
                $query = $this->db->get();               
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getSeveritiesInfo($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('id,uuid,severity,status');
                $this->db->from('severities');               
                if ($key == 'UUID') {
                    $this->db->where('uuid', $value);
                } else if ($key == 'ID') {
                    $this->db->where('id', $value);
                }
                $query = $this->db->get();               
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getSkillsInfo($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('skill_id,title,sla,status');
                $this->db->from('skill_set');               
                if ($key == 'UUID') {
                    $this->db->where('uuid', $value);
                } else if ($key == 'ID') {
                    $this->db->where('skill_id', $value);
                }
                $query = $this->db->get();               
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
   
}

?>