<?php

class Model_tickets extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $ticket, $ticket_activity, $events, $macd, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'users';
        $this->tablename_group = 'group';
        $this->ticket = 'tickets';
        $this->macd = 'macd';
        $this->macd_queue = 'macd_queue';
        $this->ticket_activity = 'ticket_activity';
        $this->events = 'events';
        $this->tablename_group_location = 'group_location';
        $this->users_domain = 'users_domain';
        $this->company_id = $this->session->userdata('company_id');
        $this->client_id = $this->session->userdata('client_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function get_latest_tickets()
	{
		$query = $this->db->query("SELECT e.*, date_format(e.event_start_time, '%D %b %Y %H:%i:%S') as DATE, 
			(SELECT device_name FROM eventedge_devices WHERE device_id = e.device_id) as device,
			(SELECT device_category_id FROM eventedge_devices WHERE device_id = e.device_id) as device_category_id,
			(SELECT device_category_uuid FROM eventedge_devices WHERE device_id = e.device_id) as device_category_uuid,
			(SELECT device_sub_category FROM eventedge_devices WHERE device_id = e.device_id) as device_sub_category,
			(SELECT device_sub_category_uuid FROM eventedge_devices WHERE device_id = e.device_id) as device_sub_category_uuid,
			(SELECT device_location FROM eventedge_devices WHERE device_id = e.device_id) as device_location,
			(SELECT service_description FROM eventedge_services WHERE service_id = e.service_id) as service, 
			(SELECT severity FROM eventedge_severities WHERE id = e.severity_id) as severity,
            (SELECT event_state FROM eventedge_event_states WHERE id = e.event_state_id) as event_state,
			(SELECT priority_id FROM eventedge_devices WHERE device_id = e.device_id) as priority_id,
			(SELECT sla FROM eventedge_skill_set WHERE skill_id = (SELECT priority_id FROM eventedge_devices WHERE device_id = e.device_id)) as sla
			FROM eventedge_events e WHERE ticket_id = 0 and severity_id != 2 and event_end_time = '0000-00-00 00:00:00'");
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_latest_macd()
	{
		$query = $this->db->query("SELECT m.*,
(SELECT device_name FROM eventedge_devices WHERE device_id = m.device_id) as Device,
(SELECT device_category_id FROM eventedge_devices WHERE device_id = m.device_id) as device_category_id,
(SELECT device_category_uuid FROM eventedge_devices WHERE device_id = m.device_id) as device_category_uuid,
(SELECT device_sub_category FROM eventedge_devices WHERE device_id = m.device_id) as device_sub_category,
(SELECT device_sub_category_uuid FROM eventedge_devices WHERE device_id = m.device_id) as device_sub_category_uuid,
(SELECT device_location FROM eventedge_devices WHERE device_id = m.device_id) as device_location,
(SELECT location_name FROM eventedge_client_locations WHERE location_uuid = m.location_uuid_to) as locationTo,
(SELECT reqested_by FROM eventedge_macd_queue WHERE request_id = m.macd_id) as userid,
(SELECT reporting_manager_id FROM eventedge_users WHERE id = (SELECT reqested_by FROM eventedge_macd_queue WHERE request_id = m.macd_id) ) as is_approved
FROM `eventedge_macd` m WHERE m.ticket_id = 0  
 ORDER BY `macd_id` DESC");
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	
	public function get_latest_macd_byId($id)
	{
		$query = $this->db->query("SELECT m.*,
(SELECT device_name FROM eventedge_devices WHERE device_id = m.device_id) as Device,
(SELECT device_category_id FROM eventedge_devices WHERE device_id = m.device_id) as device_category_id,
(SELECT device_category_uuid FROM eventedge_devices WHERE device_id = m.device_id) as device_category_uuid,
(SELECT device_sub_category FROM eventedge_devices WHERE device_id = m.device_id) as device_sub_category,
(SELECT device_sub_category_uuid FROM eventedge_devices WHERE device_id = m.device_id) as device_sub_category_uuid,
(SELECT device_location FROM eventedge_devices WHERE device_id = m.device_id) as device_location,
(SELECT location_name FROM eventedge_client_locations WHERE location_uuid = m.location_uuid_to) as locationTo,
(SELECT reqested_by FROM eventedge_macd_queue WHERE request_id = m.macd_id) as userid,
(SELECT reporting_manager_id FROM eventedge_users WHERE id = (SELECT reqested_by FROM eventedge_macd_queue WHERE request_id = m.macd_id) ) as is_approved
FROM `eventedge_macd` m WHERE m.ticket_id = 0 and m.macd_id = {$id} 
 ORDER BY `macd_id` DESC");
// echo $this->db->last_query(); exit;
		if($query->num_rows() > 0){
			return $query->row_array();
		}else{
			return false;
		}
	}

	public function get_event_ticket($eid)
	{
		$query = $this->db->query("SELECT e.ticket_id,e.severity_id
			FROM eventedge_events e WHERE e.event_id = {$eid}");
		if($query->num_rows() > 0){
			return $query->row_array();
		}else{
			return false;
		}
	}

	public function getTicketDetails($eventId)
	{
		$query = $this->db->query("SELECT e.*, date_format(e.event_start_time, '%D %b %Y %H:%i:%S') as DATE, 
			(SELECT device_name FROM eventedge_devices WHERE device_id = e.device_id) as device,
			(SELECT device_category_id FROM eventedge_devices WHERE device_id = e.device_id) as device_category_id,
			(SELECT device_category_uuid FROM eventedge_devices WHERE device_id = e.device_id) as device_category_uuid,
			(SELECT device_sub_category FROM eventedge_devices WHERE device_id = e.device_id) as device_sub_category,
			(SELECT device_sub_category_uuid FROM eventedge_devices WHERE device_id = e.device_id) as device_sub_category_uuid,
			(SELECT device_location FROM eventedge_devices WHERE device_id = e.device_id) as device_location,
			(SELECT service_description FROM eventedge_services WHERE service_id = e.service_id) as service, 
			(SELECT severity FROM eventedge_severities WHERE id = e.severity_id) as severity,
            (SELECT event_state FROM eventedge_event_states WHERE id = e.event_state_id) as event_state,
			(SELECT priority_id FROM eventedge_devices WHERE device_id = e.device_id) as priority_id,
			(SELECT sla FROM eventedge_skill_set WHERE skill_id = (SELECT priority_id FROM eventedge_devices WHERE device_id = e.device_id)) as sla
			FROM `eventedge_events` e where  e.severity_id != 2 and e.event_end_time = '0000-00-00 00:00:00' AND e.event_id = {$eventId}");
			//e.ticket_id = 0 AND
		if($query->num_rows() > 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function save_ticket($data)
	{
		$this->db->insert($this->ticket,$data);
		return $this->db->insert_id();
	}
	
	public function save_macd($data)
	{
		$this->db->insert($this->macd,$data);
		return $this->db->insert_id();
	}
	
	public function save_ticket_activity($data)
	{
		$this->db->insert($this->ticket_activity,$data);
		return $this->db->insert_id();
	}
	
	public function updateEvents($data,$con)
	{
		return $this->db->update($this->events,$data,$con);
	}
	public function updateMacd($data,$con)
	{
		return $this->db->update($this->macd,$data,$con);
	}
	
	public function updateTicket($data,$con)
	{
		return $this->db->update($this->ticket,$data,$con);
	}
	
	public function updateQueueMacd($data,$con)
	{
		return $this->db->update($this->macd_queue,$data,$con);
	}

}

?>