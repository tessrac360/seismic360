<?php

class Model_users extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    var $column_order = array('cl.client_title', 'LastName', 'usr.first_name', 'usr.phone', 'usr.email_id', 'rl.title');

    public function __construct() {
        parent::__construct();
        $this->tablename = 'users';
        $this->tablename_group = 'group';
        $this->tablename_group_location = 'group_location';
        $this->users_domain = 'users_domain';
        $this->users_assigned_group = 'users_assigned_group';
        $this->company_id = $this->session->userdata('company_id');
        $this->client_id = $this->session->userdata('client_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        // $this->accessLevelClient = helper_getAccessClientDropDown();
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function getLevelOfUser($roleId) {
        $this->db->select('level');
        $this->db->from('role');
        $this->db->where('is_active', 'Y');
        $this->db->where('id', $roleId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $page = $query->row();
            if ($page->level == '1' || $page->level == '2') {
                $return = 'Y';
            } else {
                $return = 'N';
            }
        } else {
            $return = 'N';
        }
        return $return;
    }

    public function getDashBoardStatus($roleId) {
        $this->db->select('level');
        $this->db->from('role');
        $this->db->where('is_active', 'Y');
        $this->db->where('id', $roleId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $page = $query->row();
            if ($page->level == '1' || $page->level == '2' || $page->level == '3') {
                //$page = 'dashboard';
                 $page = 'dashboard/incident';
            } else {
                $page = 'dashboard/incident';
            }
        } else {
            $page = 'dashboard/incident';
        }
        return $page;
    }

    public function getUserInformation($key = "", $value = "") {

        if ($key != "" && $value != "") {

            if ($key == "ID" || $key == 'UUID') {

                $this->db->select('id,first_name,last_name,phone,user_name,email_id');
                $this->db->from('users');
                $this->db->where('active_status', 'Y');
                if ($key == 'UUID') {
                    $this->db->where('uuid', $value);
                } else if ($key == 'ID') {
                    $this->db->where('id', $value);
                }
                $query = $this->db->get();
               // echo $this->db->last_query();
                if ($query->num_rows() > 0) {
                    $return['resultSet'] = $query->row();
                    $return['status'] = 'true';
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    private function _get_datatables_query() {
        $getSkill = $this->getSkill();
        $order = array('usr.id' => 'desc');
        $this->db->select('cl.client_title,usr.id,usr.first_name,usr.last_name,usr.phone,usr.active_status,usr.email_id,usr.is_login,rl.title,CONCAT(cuser.first_name, " ", cuser.last_name) AS username');
        $this->db->from('users as usr');
        $this->db->join('role as rl', 'rl.id = usr.role_id');
        $this->db->join('client as cl', 'cl.id = usr.client');
        $this->db->join('users as cuser', 'cuser.id = usr.created_by');
        $this->db->where('usr.company_id', $this->getCompanyId);
        $this->db->where('usr.user_type', 'U');
        $this->db->where('usr.is_deleted != ', 'Y');
        if ($getSkill['status'] == 'true') {
            $this->db->group_start();
            foreach ($getSkill['resultSet'] as $value) {
                $this->db->or_where("FIND_IN_SET('" . $value . "', usr.skill)");
            }
            $this->db->group_end();
        }
        if ($_POST['search']['value']) {
            $search = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('usr.first_name', $search);
            $this->db->or_like('usr.last_name', $search);
            $this->db->or_like('usr.email_id', $search);
            $this->db->or_like('usr.phone', $search);
            $this->db->or_like('rl.title', $search);
            $this->db->or_like('cl.client_title', $search);
            $this->db->or_like('rl.title', $search);
            $this->db->or_like('cuser.first_name', $search);
            $this->db->or_like('cuser.last_name', $search);
            $this->db->group_end();
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables() {
        //pr($_POST);exit;
        $this->_get_datatables_query();
        //exit;
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    public function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getParentClients() {
        $accessLevelClient = $this->getAccessOnlyClient();
        //pr($accessLevelClient);
        $this->db->select('cnt.id,cnt.client_title,cn.client_title as parent_name');
        $this->db->from('client as cnt');
        $this->db->join('client as cn', 'cn.id = cnt.parent_client_id');
        //$this->db->where('cnt.client_type', 'C');
        if ($accessLevelClient['status'] == 'true') {
            $this->db->where_in('cnt.id', $accessLevelClient['resultSet']);
        }
        //$this->db->where('partner_id', $this->getCompanyId);
        $query = $this->db->get();
//        echo $this->db->last_query();
//        exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->result();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
//        pr($return);
//        exit;
        return $return;
    }

    public function updateStatus($status = NULL, $id = NULL) {
        //echo $status .' ---- '.$id;//exit;
        if ($id != "") {
            if ($status == 'true') {
                $data = array('active_status' => 'Y');
            } else if ($status == 'false') {
                $data = array('active_status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            $this->db->where('id', $id);
            $this->db->update($this->tablename, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

    public function getUserName() {
        $this->db->select('first_name');
        $this->db->from($this->tablename);
        $this->db->where('id', $this->userId);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $name = $row['first_name'];
        } else {
            $name = '';
        }
        return $name;
    }

    public function getClientName() {

        $this->db->select('client_title');
        $this->db->from('client');
        $this->db->where('id', $this->client_id);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $name = $row['client_title'];
        } else {
            $name = '';
        }
        return $name;
    }

    public function isExitUser($id) {
        $this->db->select('id');
        $this->db->from($this->tablename);
        $this->db->where('id', $id);        
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function isLoginUpdate($id, $status) {
        if ($status == 'Y') {
            $var = 'N';
        } else {
            $var = 'Y';
        }
        $data = array(
            'is_login' => $var,
        );
        $data = array_merge($data, $this->uData);
        $this->db->where('id', $id);
        $getdata = $this->db->update($this->tablename, $data);
        if ($getdata != '') {
            if ($var == 'N') {
                $data = '<a title="Sign out"  href="javascript:void(0);" class="btn btn-xs green"> 
                                                    <i class="fa fa-sign-out"></i>
                                                </a>';
            }
            $return['status'] = 'true';
            $return['data'] = $data;
        } else {
            $return['status'] = 'false';
            $return['msg'] = "Something went wrong";
        }
        return $return;
    }

    public function getClient() {
        $this->db->select('id,client_title');
        $this->db->from('client');
        $this->db->where('status', 'Y');
        $this->db->where_not_in('id', 1);
        $this->db->where('parent_client_id', $this->session->userdata('client_id'));
        $this->db->where('company_id', $this->getCompanyId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->result();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getRoles($clientId = "") {
        $this->db->select('id,title');
        $this->db->from('role');
        $this->db->where('status', 'Y');
        $this->db->where('is_deleted', 'N');
        $this->db->where('client_id', $clientId);
        $this->db->where_not_in('level', '1');
        //$this->db->where('company_id', $this->getCompanyId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->result();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    private function getRoleUUID($roleId) {
        $this->db->select('uuid');
        $this->db->from('role');
        $this->db->where('status', 'Y');
        $this->db->where('is_deleted', 'N');
        $this->db->where('id', $roleId);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $clientId = $query->row();
            $uuid = $clientId->uuid;
        } else {
            $uuid = '';
        }
        return $uuid;
    }

    private function getClientUUID($clientId) {
        $this->db->select('client_uuid');
        $this->db->from('client');
        $this->db->where('status', 'Y');
        $this->db->where('id', $clientId);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $clientId = $query->row();
            $uuid = $clientId->client_uuid;
        } else {
            $uuid = '';
        }
        return $uuid;
    }

    public function insertUsers($postData = array()) {

        unset($postData['group_title']);
        unset($postData['notification_sms']);
        unset($postData['notification_email']);

        /* Name: Veeru
          Date: 24/11/17
          Functionality: Unset the assing_group_id
         */
        unset($postData['technology']);
        // pr($_POST);
        if ($postData['assign_client_type'] == 'P') {
            $postData['client'] = $this->getCompanyId;
        } else {
            $postData['client'] = $postData['client_id'];
        }
        /* End */
        unset($postData['client_id']);
        unset($postData['notification_email']);
        $postData['assigned_role_id'] = $this->roleId;
        //$postData['password'] = md5($postData['password']);
        $postData['password'] = md5(123456);
        $postData['user_type'] = decode_url($postData['user_type']);
        $postData['company_id'] = $this->getCompanyId;
        $postData['partner_id'] = $this->getCompanyId;
        $postData['uuid'] = generate_uuid('eeur_');
        $postData['role_uuid'] = $this->getRoleUUID($this->roleId);
        $postData['client_uuid'] = $this->getClientUUID($postData['client']);
//        pr($postData);
//        exit;
        $this->db->insert($this->tablename, $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
//            if ($postData['user_type']) {
//                if (($this->user_type == 'S') && ($postData['user_type'] == 'O')) {
//                    $data['company_id'] = $this->userId;
//                } elseif (($this->user_type == 'O') && ($postData['user_type'] == 'O')) {
//                    $data['company_id'] = $this->company_id;
//                } else if ($postData['user_type'] == 'A') {
//                    $data['company_id'] = '1';
//                } elseif ($postData['user_type'] == 'S') {
//                    $data['company_id'] = '1';
//                }
//                $this->db->where('id', $insert_id);
//                $this->db->update($this->tablename, $data);
//            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function insertUsersSubclientManage($postData = array()) {
        $this->db->insert('users_subclient_manage', $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateUsers($postData = array(), $id) {
        unset($postData['group_title']);
        unset($postData['notification_sms']);
        unset($postData['notification_email']);
        unset($postData['technology']);
        if ($postData['assign_client_type'] == 'P') {
            $postData['client'] = $this->getCompanyId;
        } else {
            $postData['client'] = $postData['client_id'];
        }
        unset($postData['client_id']);
        unset($postData['notification_email']);

        $postData['user_type'] = decode_url($postData['user_type']);
        //pr($postData);exit;
        $this->db->where('id', $id);
        $update_status = $this->db->update('users', $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function UpdateUserAssignGroups($postData = array(), $id) {
        $this->db->where('user_id', $id);
        $update_status = $this->db->update($this->users_assigned_group, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function getUserPagination($limit = "", $start = "") {
        $this->db->select('cl.client_title,usr.id,usr.first_name,usr.last_name,usr.phone,usr.active_status,usr.email_id,usr.is_login,rl.title,CONCAT(cuser.first_name, " ", cuser.last_name) AS username');
        $this->db->from('users as usr');
        $this->db->join('role as rl', 'rl.id = usr.role_id');
        $this->db->join('client as cl', 'cl.id = usr.client');
        $this->db->join('users as cuser', 'cuser.id = usr.created_by');
        $this->db->where('usr.is_deleted', 'N');
        $this->db->where('usr.company_id', $this->getCompanyId);
        $this->db->order_by('usr.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        // pr($data);exit;
        return $data;
    }

    /*
     * Objective: Pagination of Role moudle with Searching
     * parameters: Null
     * Return: Null
     */

    public function getUserSearchData($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {
        $this->db->select('cl.client_title,usr.id,usr.first_name,usr.last_name,usr.phone,usr.active_status,usr.email_id,usr.is_login,rl.title,CONCAT(cuser.first_name, " ", cuser.last_name) AS username');
        $this->db->from('users as usr');
        $this->db->join('role as rl', 'rl.id = usr.role_id');
        $this->db->join('client as cl', 'cl.id = usr.client');
        $this->db->join('users as cuser', 'cuser.id = usr.created_by');
        $this->db->where('usr.is_deleted', 'N');
        $this->db->where('usr.company_id', $this->getCompanyId);
        $this->db->order_by('usr.id', "desc");
        if ($search != "") {
            $this->db->group_start();
            $this->db->like('usr.first_name', $search);
            $this->db->or_like('usr.last_name', $search);
            $this->db->or_like('usr.email_id', $search);
            $this->db->or_like('cl.client_title', $search);
            $this->db->or_like('rl.title', $search);
            $this->db->group_end();
        }

        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

    public function getUsersEdit($postId = "") {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //  pr($return);exit;
        return $return;
    }

    /* Start user module */

    private function getClientID($menuLevel = "") {
        $this->db->select('client');
        $this->db->from('users');
        $this->db->where('active_status', 'Y');
        $this->db->where('id', $this->userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['status'] = 'true';
            $data = $query->row();
            $rows = $data->client;
            $return['status'] = 'true';
            $return['resultSet'] = explode(',', $rows);
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getUsersSubclientManage($userId) {
        $this->db->select('*');
        $this->db->from('users_subclient_manage');

        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function deleteUsersSubclientManage($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->delete('users_subclient_manage');
    }

    public function getClientForUser() {
        $clientId = $this->getClientID();
        if ($clientId['status'] == 'true') {
            $this->db->select('id,client_title');
            $this->db->from('client');
            $this->db->where('status', 'Y');
            $this->db->where_in('id', $clientId['resultSet']);
            $query = $this->db->get();
//            echo $this->db->last_query();
//            exit;
            if ($query->num_rows() > 0) {
                $return['resultSet'] = $query->result();
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
            }
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getGroups() {
        //exit;
        $this->tablename_group = 'group';
        $this->tablename_group_location = 'group_location';
        $query = $this->db->get($this->tablename_group);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $result_array = array();
            $result_array = $query->result();
        } else {
            $data = array();
        }
        return $result_array;
    }

    public function getSkillset() {
        $this->tablename_skill_set = 'skill_set';
        $query = $this->db->get($this->tablename_skill_set);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $result_array = array();
            $result_array = $query->result();
        } else {
            $data = array();
        }
        return $result_array;
    }

    public function getGroupsofcompany($company_id) {
        $this->db->select('domain_id');
        $this->db->where('user_id', $company_id);
        $query = $this->db->get($this->users_domain);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            //echo "ddddddd";
            $data = $query->result_array();
            $rows = implode(",", array_column($data, 'domain_id'));
            $data = explode(',', $rows);
        } else {
            $data = array();
        }

        return $data;
    }

    public function getTechnologyById($userId) {
        $this->db->select('tech_id');
        $this->db->where('user_id', $userId);
        $query = $this->db->get('users_assigned_technology');
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            //echo "ddddddd";
            $data = $query->result_array();
            $rows = implode(",", array_column($data, 'tech_id'));
            $data = explode(',', $rows);
        } else {
            $data = array();
        }
        //pr($data);exit;
        return $data;
    }

    public function insertTechnology($postData = array()) {
        $this->db->insert('users_assigned_technology', $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function deleteTechnology($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->delete('users_assigned_technology');
    }

    public function updateuserTechnology($postData = array()) {
        $this->db->insert('users_assigned_technology', $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function deleteuserGroups($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->delete($this->users_domain);
    }

    public function updateuserGroups($postData = array()) {
        $this->db->insert($this->users_domain, $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    /* Start for Client Admin and sub-admin */

    public function getDomain() {
        $this->db->select('domain_id');
        $this->db->from('users_domain');
        $this->db->where('user_id', $this->userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['status'] = 'true';
            $data = $query->result_array();
            $rows = implode(",", array_column($data, 'domain_id'));
            $return['status'] = 'true';
            $return['resultSet'] = explode(',', $rows);
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    public function getAccessLevelClient() {
        if (!empty($_SESSION)) {
            if ($_SESSION['assign_client_type'] == 'C') {
                $this->db->select('id as client_id');
                $this->db->from('client');
                $this->db->where('id', $_SESSION['client_id']);
                $this->db->where('status', 'Y');
                $this->db->where('client_type', 'C');
                $query = $this->db->get();
                //echo $this->db->last_query();//exit;	
                if ($query->num_rows() > 0) {
                    $data['status'] = 'true';
                    $data = $query->result_array();
                    //pr($data);exit;
                    $rows = implode(",", array_column($data, 'client_id'));
                    $return['status'] = 'true';
                    $return['resultSet'] = explode(',', $rows);
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $clientDetails = $this->getClientList($_SESSION['client_id']);
                $clientId = array();
                if (!empty($clientDetails)) {                   
                    foreach ($clientDetails as $key => $value) {
                        //if ($value['client_type'] == 'C') {
                            $clientId[] = $value['client_id'];
                        //}
                    }
                    $return['status'] = 'true';
                    $return['resultSet'] = $clientId;
                } else {
                    $return['status'] = 'false';
                }
            }
        }
        //pr($return);exit;
        return $return;
    }
    
    public function getAccessOnlyClient() {
        if (!empty($_SESSION)) {
            if ($_SESSION['assign_client_type'] == 'C') {
                $this->db->select('id as client_id');
                $this->db->from('client');
                $this->db->where('id', $_SESSION['client_id']);
                $this->db->where('status', 'Y');
                $this->db->where('client_type', 'C');
                $query = $this->db->get();
                //echo $this->db->last_query();//exit;	
                if ($query->num_rows() > 0) {
                    $data['status'] = 'true';
                    $data = $query->result_array();
                    //pr($data);exit;
                    $rows = implode(",", array_column($data, 'client_id'));
                    $return['status'] = 'true';
                    $return['resultSet'] = explode(',', $rows);
                } else {
                    $return['status'] = 'false';
                }
            } else {
                $clientDetails = $this->getClientList($_SESSION['client_id']);
                $clientId = array();
                if (!empty($clientDetails)) {                   
                    foreach ($clientDetails as $key => $value) {
                        if ($value['client_type'] == 'C') {
                            $clientId[] = $value['client_id'];
                        }
                    }
                    $return['status'] = 'true';
                    $return['resultSet'] = $clientId;
                } else {
                    $return['status'] = 'false';
                }
            }
        }
        //pr($return);exit;
        return $return;
    }

    public function getClientList($client_id = 0, $allClients = array(), $type = 0) {
        $this->db->select('id as client_id,client_type');
        $this->db->from('client');
        if ($type == 0) {
            $this->db->where('id', $client_id);
        } else {
            $this->db->where('parent_client_id', $client_id);
        }
        $clients = $this->db->get();

        foreach ($clients->result_array() as $client) {
            //pr($client);
            $allClients[] = $client;
            $allClients = $this->getClientList($client['client_id'], $allClients, 1);
        }
        //pr($allClients);
        return $allClients;
    }

    public function getClientsDetails($partnerId = "") {
        $this->db->select('id,client_title');
        $this->db->from('client');
        $this->db->where('status', 'Y');
        $this->db->where('client_type', 'C');
        $this->db->where('parent_client_id', $partnerId);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;	
        if ($query->num_rows() > 0) {
            $data['status'] = 'true';
            $data = $query->result_array();
            //pr($data);exit;
            // $rows = implode(",", array_column($data, 'client_id'));
            //$return['status'] = 'true';
            $return['resultSet'] = $data;
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getAccessClientDropDown() {
        if ($_SESSION['assign_client_type'] == 'C') {
            $this->db->select('id as client_id');
            $this->db->from('client');
            $this->db->where('id', $_SESSION['client_id']);
            $this->db->where('status', 'Y');
            $this->db->where('client_type', 'C');
            $query = $this->db->get();
            //echo $this->db->last_query();//exit;	
            if ($query->num_rows() > 0) {
                $data['status'] = 'true';
                $data = $query->result_array();
                //pr($data);exit;
                $rows = implode(",", array_column($data, 'client_id'));
                $return['status'] = 'true';
                $return['resultSet'] = explode(',', $rows);
            } else {
                $return['status'] = 'false';
            }
        } else {
            $clientDetails = $this->getClientList($_SESSION['client_id']);
            $clientId = array();
            if (!empty($clientDetails)) {
                foreach ($clientDetails as $key => $value) {
                    if ($value['client_type'] == 'P') {
                        $clientId[] = $value['client_id'];
                    }
                }
                $return['status'] = 'true';
                $return['resultSet'] = $clientId;
            } else {
                $return['status'] = 'false';
            }
        }
        //pr($return);exit;
        return $return;
    }

    public function getAccessClientPartner() {
        if ($_SESSION['assign_client_type'] == 'C') {
            $this->db->select('id as client_id');
            $this->db->from('client');
            $this->db->where('id', $_SESSION['client_id']);
            $this->db->where('status', 'Y');
           $this->db->where('client_type', 'C');
            $query = $this->db->get();
            //echo $this->db->last_query();//exit;	
            if ($query->num_rows() > 0) {
                $data['status'] = 'true';
                $data = $query->result_array();
                //pr($data);exit;
                $rows = implode(",", array_column($data, 'client_id'));
                $return['status'] = 'true';
                $return['resultSet'] = explode(',', $rows);
            } else {
                $return['status'] = 'false';
            }
        } else {
            $clientDetails = $this->getClientList($_SESSION['client_id']);
            $clientId = array();
            if (!empty($clientDetails)) {
                foreach ($clientDetails as $key => $value) {                   
                        $clientId[] = $value['client_id'];                    
                }
                $return['status'] = 'true';
                $return['resultSet'] = $clientId;
            } else {
                $return['status'] = 'false';
            }
        }
        //pr($return);exit;
        return $return;
    }

    public function getSkill() {
        $this->db->select('skill');
        $this->db->from('users');
        $this->db->where('id', $this->userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['status'] = 'true';
            $data = $query->row_array();

            //$rows = implode(",", array_column($data, 'domain_id'));
            $return['status'] = 'true';
            $return['resultSet'] = explode(',', $data['skill']);
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    public function getDomainUser() {
        $getDomain = $this->getDomain();
//pr($getDomain);exit;
        if ($getDomain['status'] == 'true') {

            $this->tablename_group = 'group';
            $this->db->where_in('id', $getDomain['resultSet']);
            $query = $this->db->get($this->tablename_group);
            //echo $this->db->last_query();exit;
            if ($query->num_rows() > 0) {
                $result_array = array();
                $result_array = $query->result();
            } else {
                $result_array = array();
            }
        } else {
            $result_array = array();
        }
        return $result_array;
    }

    public function getSkillsetUser() {
        $getSkill = $this->getSkill();
        //pr($getSkill);exit;
        if ($getSkill['status'] == 'true') {
            $this->tablename_skill_set = 'skill_set';
            $this->db->where_in('skill_id', $getSkill['resultSet']);
            $query = $this->db->get($this->tablename_skill_set);
            //echo $this->db->last_query();exit;
            if ($query->num_rows() > 0) {
                $result_array = array();
                $result_array = $query->result();
            } else {
                $result_array = array();
            }
        } else {
            $result_array = array();
        }
        return $result_array;
    }

    /* Start for Client Admin and sub-admin */



    /* Start change password and change profile */

    public function getUserDetails() {
        $this->db->select('cl.client_title,usr.id,usr.user_avatar,usr.user_name,usr.first_name,usr.middle_name,usr.last_name,usr.phone,usr.active_status,usr.email_id,usr.skill,rl.title');
        $this->db->from('users as usr');
        $this->db->join('role as rl', 'rl.id = usr.role_id');
        $this->db->join('client as cl', 'cl.id = usr.client');
        $this->db->where('usr.id', $this->userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

    public function updateProfile($postData = array()) {
        $this->db->where('id', $this->userId);
        $update_status = $this->db->update('users', $postData);
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function oldPasswordCheck($postData = array()) {
        $userId = $this->userId;
        $old_password = md5($postData['old_password']);
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('active_status', 'Y');
        $this->db->where('is_deleted', 'N');
        $this->db->where('id', $userId);
        $this->db->where('password', $old_password);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function changePassword($postData = array()) {
        $this->db->where('id', $this->userId);
        $postData['password'] = md5($postData['password']);
        $update_status = $this->db->update('users', $postData);
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function getUserPagination_subClients($limit = "", $start = "") {
        // $getDomain = $this->getDomain();
        $getSkill = $this->getSkill();
        $this->db->select('cl.client_title,usr.id,usr.first_name,usr.last_name,usr.phone,usr.active_status,usr.email_id,usr.is_login,rl.title,CONCAT(cuser.first_name, " ", cuser.last_name) AS username');
        $this->db->from('users as usr');
        $this->db->join('role as rl', 'rl.id = usr.role_id');
        $this->db->join('client as cl', 'cl.id = usr.client');
        if ($getSkill['status'] == 'true') {
            $this->db->group_start();
            foreach ($getSkill['resultSet'] as $value) {
                $this->db->or_where("FIND_IN_SET('" . $value . "', usr.skill)");
            }
            $this->db->group_end();
        }
        $this->db->join('users as cuser', 'cuser.id = usr.created_by');
        $this->db->where('usr.company_id', $this->getCompanyId);
        $this->db->where_not_in('usr.id', $this->userId);
        $this->db->where('usr.is_deleted != ', 'Y');

        $this->db->order_by('usr.id', "desc");

        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();exit;    
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        // pr($data);exit;
        return $data;
    }

    /*
     * Objective: Pagination of Role moudle with Searching
     * parameters: Null
     * Return: Null
     */

    public function getUserSearchData_subClients($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {
        $getDomain = $this->getDomain();
        $getSkill = $this->getSkill();
        //pr($getDomain);pr($getSkill);
        $this->db->select('cl.client_title,usr.id,usr.first_name,usr.last_name,usr.phone,usr.active_status,usr.email_id,usr.is_login,rl.title,CONCAT(cuser.first_name, " ", cuser.last_name) AS username');
        $this->db->from('users as usr');
        $this->db->join('role as rl', 'rl.id = usr.role_id');
        $this->db->join('client as cl', 'cl.id = usr.client');
        $this->db->join('users as cuser', 'cuser.id = usr.created_by');
        //$this->db->where('usr.active_status', 'Y');
        //$this->db->where('usr.company_id', $this->getCompanyId);
        $this->db->where('usr.assigned_role_id', $this->roleId);

        if ($getSkill['status'] == 'true') {
            $this->db->group_start();
            foreach ($getSkill['resultSet'] as $value) {
                $this->db->or_where("FIND_IN_SET('" . $value . "', usr.skill)");
            }
            $this->db->group_end();
        }
        $this->db->group_by('domain.user_id');
        $this->db->order_by('usr.id', "desc");
        if ($search != "") {
            $this->db->group_start();
            $this->db->like('usr.first_name', $search);
            $this->db->or_like('usr.last_name', $search);
            $this->db->or_like('usr.email_id', $search);
            $this->db->or_like('usr.phone', $search);
            $this->db->or_like('rl.title', $search);
            $this->db->or_like('cl.client_title', $search);
            $this->db->or_like('rl.title', $search);
            $this->db->like('cuser.first_name', $search);
            $this->db->like('cuser.last_name', $search);
            $this->db->group_end();
        }

        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

    public function isDeleteUser($postId) {
        if ($postId != "") {
            $data = array('is_deleted' => 'Y');
            $this->db->where('id', $postId);
            $this->db->update($this->tablename, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getTechnology() {
        $this->db->select('id,category');
        $this->db->from('device_categories');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    public function getUsersListByCompanyId() {
        $this->db->select("id,CONCAT_WS(' ', first_name,last_name,email_id) as user");
        $this->db->from($this->tablename);
        $this->db->where('partner_id', $this->getCompanyId);
        $this->db->where('is_deleted', "N");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    public function getAssignId($postId = "") {
        $this->db->select("*");
        $this->db->from('users_assigned_group');
        $this->db->where('user_id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
    }

    public function siddhus() {
        $query = $this->db->query("UPDATE `eventedge_skill_set` SET `sla` = '150' WHERE `eventedge_skill_set`.`skill_id` = 4");
    }

}

?>