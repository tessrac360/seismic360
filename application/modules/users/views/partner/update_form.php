<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url() . "public/" ?>treeview/treejs/js/jquery.tree.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>treeview/treejs/css/jquery.tree.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.css">
<script>
    $(document).ready(function () {
        $('#example-0 div').tree();
    });
</script>
<style>
    .ui-widget.ui-widget-content {
        border: 0px solid #cccccc;
    }
    .ui-widget-content {
        border: 0px solid #0000 !important;
        background: top repeat-x !important;
    }
</style>
<!-- END PAGE HEADER-->
<?php error_reporting(0); ?>
<?php //pr($gettechnology);exit;?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<span class="caption-subject font-green-steel bold uppercase">Edit Partner</span>
					<span style="color:black;"><?php if(isset($clientName) && !empty($clientName)){
							echo " (".$clientName.")";
						} ?>
					</span>
				</div>
				<div class="page-toolbar">
					<ul class="page-breadcrumb breadcrumb custom-bread">
						<li>
							<i class="fa fa-cog"></i>
							<span>Managements</span>  
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url('partner');?>">Partner</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url('users/partner/partner_list/?cli='.encode_url($patnerId)); ?>">Partner Admin</a>                            
							<i class="fa fa-angle-right"></i>
						</li>						
						<li>
							<span>Create</span>                            
						</li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
            <?php
            if ($getUser['status'] = 'true') {
                $getResult = $getUser['resultSet'];
                $getRole = $getUser['role'];
                ?>
                <div class="portlet-body form">
                    <form role="form" name="frmEditUser" id="frmEditUser" method="post" action="" autocomplete="off">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Basic Information</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="first_name" name="first_name" value="<?php echo ($getResult->first_name) ? $getResult->first_name : ''; ?>" type="text">
                                       <label for="form_control_1">First Name<span class="required"  aria-required="true">*</span></label>


                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="middle_name" value="<?php echo ($getResult->middle_name) ? $getResult->middle_name : ''; ?>" name="middle_name" type="text">
										<label for="form_control_1">Middle Name<span class="required"  aria-required="true"></span></label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="last_name" value="<?php echo ($getResult->last_name) ? $getResult->last_name : ''; ?>" name="last_name" type="text">
										<label for="form_control_1">Last Name<span class="required"  aria-required="true">*</span></label>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="phone" name="phone" type="text" value="<?php echo ($getResult->phone) ? $getResult->phone : ''; ?>" onkeyup="this.value = this.value.replace(/[^0-9]/g, '')" minlength = "10" maxlength = "10">
                                        <label for="form_control_1">Phone Number<span class="required"  aria-required="true">*</span></label>                                               
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" readonly id="email_id" name="email_id" type="text" value="<?php echo ($getResult->email_id) ? $getResult->email_id : ''; ?>" tableName='users' tableField='email_id'>
                                        <label for="form_control_1">Email<span class="required" aria-required="true" >*</span></label>  
                                        <!--<span id="errmsg" class="error" style="display: none;"></span>-->
                                    </div>
                                </div>  
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input  class="form-control" id="user_name" name="user_name" type="text" value="<?php echo ($getResult->user_name) ? $getResult->user_name : ''; ?>">
                                        <label for="form_control_1">Username<span class="required" aria-required="true">*</span></label>                                               
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase">Partner Option</span>
                                    </div>
                                </div>


                               
                                 <div class="col-md-4" style="margin-top: 10px;">
                                <div class="form-group">
                                    <label for="form_control_1">Partner<span class="required" aria-required="true">*</span></label>
                                    </br><select class="form-control partner" name="client_id" id="client_id">                                         
                                        <?php
                                        if ($client['status'] == 'true') {
                                            foreach ($client['resultSet'] as $value) {
                                                ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->client_title; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                     
                                </div>
                            </div>                         
                            <div class="col-md-4" style="margin-top: 10px;">
                                <div class="form-group">
                                    <label for="form_control_1">Role & Permission<span class="required" aria-required="true">*</span></label>
                                    <select class="form-control edited" name="role_id" id="role_id">
                                        <option value="">Select Role & Permission</option>
                                        <?php
                                        if ($roles['status'] == 'true') {
                                            foreach ($roles['resultSet'] as $value) {
                                                ?>
                                                <option <?= ($getResult->role_id == $value->id) ? 'selected' : ''; ?> value="<?php echo $value->id; ?>"><?php echo $value->title; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>

                                </div>
                            </div>


                            <div id="groupdiv">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Assigning</span>
                                    </div>
                                </div>
                                <div class="col-md-12" style="padding: 10px 0 10px 15px">
                                    <label for="form_control_1 col-md-6">Technology/Category :<span class="required" aria-required="true">*</span></label>
                                    <select class="form-control assign_group_id sd" name="technology[]" id="assign_group_id" multiple="multiple">
                                        <option value=""></option>   
                                        <?php
                                        if ($technology['status'] == 'true') {
                                            foreach ($technology['resultSet'] as $value) {
                                                ?>
                                                <option <?php if (in_array($value['id'], $gettechnology)) echo 'selected'; ?> value="<?php echo $value['id']; ?>"><?php echo $value['category']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>
                                    <div class="col-sm-12" id="assign_error"></div>
                                </div>
                            </div>
                            <div id="groupdiv">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Access Limit</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12" style="padding: 10px 0 10px 15px">
                                        <span class="caption-subject  uppercase" > Skill Set  :</span> &nbsp; &nbsp;
                                        <?php
                                        foreach ($skillset as $value) {
                                            ?>	
                                            <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;margin-right: 25px;    margin-bottom: 6px;">
                                                <input type="checkbox" checked="checked" readonly  name ="skill[]" value="<?php echo $value->skill_id; ?>" id="<?php echo $value->skill_id; ?>"><?php echo $value->title; ?>
                                                <span></span>
                                            </label>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-12" id="skill_error"></div>
                                </div>

                            </div>
<!--                            <div id="groupdiv">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Send Notification to User</span>
                                    </div>
                                </div>	

                                <div class="form-group">
                                    <div class="col-sm-12" style="padding: 10px 0 10px 15px">
                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                            <input type="checkbox" class="notification"  name ="notification_sms">SMS
                                            <span></span>
                                        </label>
                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                            <input type="checkbox" class="notification" name ="notification_email">Email
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12" id="notification_error"></div>
                                </div>
                            </div>-->


                            <!-- Start
                                               Name: veeru 
                                               Date: 24/11/17
                                               Functionality: Assign Group Functionality
                            -->


                            <!-- End -->
                        </div>

                            <div class="form-actions noborder">
								<!--<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" >-->
                                <input type="hidden" name="user_type" value="<?php echo encode_url('P'); ?>" >
                                <input  id="old_email"  type="hidden"  value="<?php echo ($getResult->email_id) ? $getResult->email_id : ''; ?>">                                
                                <button type="submit" class="btn green">Save</button>
                                &nbsp; &nbsp; <a href="<?php echo base_url('users/partner/partner_list/?cli='.encode_url($patnerId)); ?>" class="btn default">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>

            <?php } ?>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
            <?php

        function childTree($treeArray = array(),$accessLimit= array()) {
            ?>
    <ul>
    <?php foreach ($treeArray as $value) {
        ?>
            <li>
                <label class="mt-checkbox mt-checkbox-outline">                                                                    
                    <input type="checkbox" <?php if($accessLimit[$value['id'] . '-' . $value['parent_client_id']]){
																echo "checked";
																
														}?> name="subClients[]" value="<?php echo $value['id'] . '-' . $value['parent_client_id']; ?>">
                    <span></span>
                </label>
                <span><?php echo $value['client_title']; ?></span>
                <?php
                if (!empty($value['children'])) {

                    echo childTree($value['children'],$accessLimit);
                }
                ?>        
            </li>             
    <?php }
    ?>
    </ul>
<?php }
?>
<script>
$(':checkbox[readonly]').click(function(){ return false; });
</script>
<script>
    $(':checkbox[readonly]').click(function () {
        return false;
    });
    $(function () {
        $('.assign_clients').change(function () {
            var checkVal = $('input[name=assign_client_type]:checked').val();
            $('.subClients_list').hide();
            if (checkVal == 'C') {
                $('.subClients_list').show();
            }
        });

//        $('.partner').fSelect();
        $('.assign_group_id').fSelect();

    });

</script>         
<script src="<?php echo base_url() . "public/" ?>js/form/form_user.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.js" type="text/javascript"></script>	





