<?php
$accessAdd = helper_fetchPermission('15', 'add');
$accessEdit = helper_fetchPermission('15', 'edit');
$accessStatus = helper_fetchPermission('15', 'active');
$accessDelete = helper_fetchPermission('15', 'delete');
?>
<style>
    .dataTables_wrapper .dataTables_paginate .paginate_button:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{background: #337ab7 !important; color: #fff !important;                                                                                                                                                                                                        border-color: #337ab7 !important;}
    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover{background: transparent !important; border:1px solid #ddd !important;}
    .dataTables_scroll{margin-bottom: 0;}
    div.dataTables_wrapper div.dataTables_paginate{margin-bottom: 3px;}
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{    bottom: 2px;}
    .mt-checkbox>span, .mt-radio>span {width: 20px;}
</style>
<?php //pr($clientName);exit;?>
<!-- END PAGE HEADER-->
<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Partner</span>
				<span style="color:black;"><?php if(isset($clientName) && !empty($clientName)){
						echo " (".$clientName.")";
					} ?>
				</span>
            </div>
            <div class="page-toolbar">
                <ul class="page-breadcrumb breadcrumb custom-bread">
                    <li>
                        <i class="fa fa-cog"></i>
                        <span>Managements</span>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo base_url('partner');?>">Partner</a>
						<i class="fa fa-angle-right"></i>
                    </li>
					<li>
                        <span>Partner Admin</span>
						<i class="fa fa-angle-right"></i>
                    </li>
					<li>
						<span>List</span>                            
					</li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
                            <?php if ($accessAdd == 'Y') { ?> 
                                <div class="btn-group">
                                    <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('users/partner/create/'.$_GET['cli']) ?>"> Add New Partner
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>                        
                    </div>
                </div>
                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead style="background: #17C4BB !important; color:white;">
                        <tr>
                            <th> Name </th>                           
                            <th> Email </th>
                            <th> Phone No </th>
                            <th> Partner </th>
                            <th> Role Name </th>   
                            <th> Created By </th>   
                            <th> Status </th>
                            <th> Edit </th>
                            <th> Delete </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>            
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var url_string = document.URL;
        var url = new URL(url_string);
        var partner_id = url.searchParams.get("cli");  
        var table = $('#table').DataTable({
            "language": {
                "infoFiltered": ""
            },
            "pageLength": 50,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('users/partner/ajax_userdata') ?>",
                "type": "POST",
                "data": function (data) {
                     data.partner_id = partner_id;
                }
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [1, 6, 7, 8], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
        });



    });
</script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_user.js" type="text/javascript"></script>


