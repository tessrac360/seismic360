<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Change Password</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Change Password</span>  

                        </li>
                    </ul>                                                        
                </div>
            </div>
            <div class="portlet-body form">

                <form role="form" name="frmUserChangepassword" id="frmUserChangepassword" method="post" action="">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="old_password" name="old_password" type="password">
                                    <label for="form_control_1">Old Password<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="password" name="password" type="password">
                                    <label for="form_control_1">Password<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="password_confirm" name="password_confirm" type="password">
                                    <label for="form_control_1">Confirm Password</label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>                           
                        </div>                        
                        <div class="form-actions noborder">                            
                            <button type="submit" class="btn green">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_user.js" type="text/javascript"></script>