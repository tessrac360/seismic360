<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />       
<link href="<?php echo base_url() . "public/" ?>assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />

<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Change Profile</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Change Profile</span>  

                        </li>
                    </ul>                                                        
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" name="frmChangeprofile" id="frmChangeprofile" method="post" action="" enctype="multipart/form-data">
                <?php
                if ($user['status'] == 'true') {
                    $user = $user['resultSet'];                    
                    ?>                    
                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="first_name" name="first_name" value="<?php echo ($user->first_name) ? $user->first_name : ''; ?>" type="text">
                                        <label for="form_control_1">First Name<span class="required"  aria-required="true">*</span></label>
    <!--                                    <span class="help-block">Enter your name...</span>-->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="middle_name" name="middle_name" value="<?php echo ($user->middle_name) ? $user->middle_name : ''; ?>" type="text">
                                        <label for="form_control_1">Middle Name</label>
    <!--                                    <span class="help-block">Enter your name...</span>-->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="last_name" name="last_name" value="<?php echo ($user->last_name) ? $user->last_name : ''; ?>" type="text">
                                        <label for="form_control_1">Last Name</label>
    <!--                                    <span class="help-block">Enter your name...</span>-->
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="phone" name="phone" type="text" value="<?php echo ($user->phone) ? $user->phone : ''; ?>" onkeyup="this.value = this.value.replace(/[^0-9]/g, '')" minlength = "10" maxlength = "10">
                                        <label for="form_control_1">Phone Number<span class="required"  aria-required="true">*</span></label>                                               
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" readonly=""  type="text" value="<?php echo ($user->email_id) ? $user->email_id : ''; ?>">
                                        <label for="form_control_1">Email<span class="required" aria-required="true" >*</span></label>  
                                        <!--<span id="errmsg" class="error" style="display: none;"></span>-->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" readonly=""  type="text" value="<?php echo ($user->user_name) ? $user->user_name : ''; ?>">
                                        <label for="form_control_1">Username<span class="required" aria-required="true" >*</span></label>  
                                        <!--<span id="errmsg" class="error" style="display: none;"></span>-->
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase">User Option</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" readonly="" type="text" value="<?php echo ($user->client_title) ? $user->client_title : ''; ?>" >
                                        <label for="form_control_1">Client</label>                                               
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" readonly=""  type="text" value="<?php echo ($user->title) ? $user->title : ''; ?>">
                                        <label for="form_control_1">Role</label>  
                                        <!--<span id="errmsg" class="error" style="display: none;"></span>-->
                                    </div>
                                </div>

                                <?php if (!empty($getDomain)) { ?>                                
                                    <div id="groupdiv">
                                        <div class="col-md-12">
                                            <div class="caption font-dark">
                                                <span class="caption-subject bold uppercase"> Access Limit</span>
                                            </div>
                                        </div>	
                                        <div class="form-group" >
                                            <div class="col-sm-12" style="padding: 20px 0 10px 20px">
                                                <span class="caption-subject  uppercase" > Domains</span>
                                                <br>
                                                <?php
                                                foreach ($groups as $value) {
                                                    ?>	
                                                    <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                                        <input type="checkbox" <?php if (in_array($value->id, $getDomain)) echo 'checked="checked"'; ?> value="<?php echo $value->id; ?>" disabled><?php echo $value->title; ?>
                                                        <span></span>
                                                    </label>
                                                <?php } ?>
                                            </div>
                                            <div class="col-sm-12" id="group_id_error"></div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-12" style="padding: 20px 0 10px 20px">
                                                <span class="caption-subject  uppercase" > Skill Set</span>
                                                <br>
                                                <?php
                                                $skill = explode(',', $user->skill);
                                                foreach ($skillset as $value) {
                                                    ?>	
                                                    <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                                        <input type="checkbox"  value="<?php echo $value->skill_id; ?>" <?php if (in_array($value->skill_id, $skill)) echo 'checked="checked"'; ?> disabled><?php echo $value->title; ?>
                                                        <span></span>
                                                    </label>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>     
                                <?php } ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if($user->user_avatar != "") { ?>
                                                <img src="<?php echo $user->user_avatar;?>" alt="" /> 
                                               <?php } else { ?>                                                
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                               <?php  }?>
                                                
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="user_avatar"> 
                                                </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions noborder">                            
                            <button type="submit" class="btn green">Save</button>
                        </div>
                </div>
            <?php }
            ?>

            </form>
        </div>
    </div>
</div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() . "public/" ?>js/form/form_user.js" type="text/javascript"></script>