<style>
    .remove-pad-right{padding-right:0;}
    .dashboard-stat2{padding: 10px 10px 6px;}
    .dashboard-stat2 .display .number small{font-size: 13px;font-weight: 400;text-transform: capitalize;}
    .dashboard-stat2, .dashboard-stat2 .display{margin-bottom: 8px;}
    .amcharts-title{color:#000; fill:black;}
    .amcharts-title{
        transform: translate(145px,30px)
    }
    .amcharts-title:nth-child(2){
        transform: translate(165px, 145px)
    }
</style>
<div class="full-height-content full-height-content-scrollable">
    <h1 class="page-title"> Client Dashboard </h1>
    <div class="full-height-content-body">
        <div class="row">
            <div class="col-md-3 parent remove-pad-right">
                <div class="row">
                    <div class="col-md-12 remove-pad-right">
                        <div class="dashboard-stat2 bordered">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp">
                                        <span data-counter="counterup" data-value="310">310</span>

                                    </h3>
                                    <small>Up</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                </div>
                            </div>
                        </div>

                        <div class="dashboard-stat2 bordered">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-red-haze">
                                        <span data-counter="counterup" data-value="170">170</span>
                                    </h3>
                                    <small>Down</small>
                                </div>
                               <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                </div>
                            </div>
                        </div>

                        <div class="dashboard-stat2 bordered">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-blue-sharp">
                                        <span data-counter="counterup" data-value="25">25</span>
                                    </h3>
                                    <small>Unknown</small>
                                </div>
                               <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                </div>
                            </div>
                        </div>

                        <div class="dashboard-stat2 bordered">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-blue-sharp">
                                        <span data-counter="counterup" data-value="200">200</span>
                                    </h3>
                                    <small>Ok</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                </div>
                            </div>
                        </div>


                        <div class="dashboard-stat2 bordered">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-purple-soft">
                                        <span data-counter="counterup" data-value="10">10</span>
                                    </h3>
                                    <small>Warning</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                </div>
                            </div>
                        </div>

 <div class="dashboard-stat2 bordered">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-purple-soft">
                                        <span data-counter="counterup" data-value="5">5</span>
                                    </h3>
                                    <small>Pending</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
            <div class="col-md-9 parent">
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-xs-12 col-sm-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase font-dark">Devices</span>

<!--<span class="rightbar">Click</span>-->
                                </div>
                                <div class="actions">
                                    <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                        <i class="icon-cloud-upload"></i>
                                    </a>
                                    <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                        <i class="icon-wrench"></i>
                                    </a>
                                    <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                        <i class="icon-trash"></i>
                                    </a>
                                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="chart_6" class="chart" style="height: 250px;"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6 col-xs-12 col-sm-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase font-dark">Devices Availability</span>														
                                </div>
                                <div class="actions">
                                    <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                        <i class="icon-cloud-upload"></i>
                                    </a>
                                    <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                        <i class="icon-wrench"></i>
                                    </a>
                                    <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                        <i class="icon-trash"></i>
                                    </a>
                                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="dashboard_amchart_3" class="CSSAnimationChart" style="height: 250px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12col-xs-12 col-sm-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-equalizer font-dark hide"></i>
                                    <span class="caption-subject font-dark bold uppercase">Server Stats</span>
                                    <span class="caption-helper">monthly stats...</span>
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"> </a>
                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                    <a href="" class="reload"> </a>
                                    <a href="" class="remove"> </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="sparkline-chart">
                                            <div class="number" id="sparkline_bar5"></div>
                                            <a class="title" href="javascript:;"> Network
                                                <i class="icon-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="margin-bottom-10 visible-sm"> </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="sparkline-chart">
                                            <div class="number" id="sparkline_bar6"></div>
                                            <a class="title" href="javascript:;"> CPU Load
                                                <i class="icon-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="margin-bottom-10 visible-sm"> </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="sparkline-chart">
                                            <div class="number" id="sparkline_line"></div>
                                            <a class="title" href="javascript:;"> Load Rate
                                                <i class="icon-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="right_block col-md-3" style="display:none;">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">Revenue</span>
                        </div>

                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-cloud-upload"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-wrench"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-trash"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="close fa fa-close"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
