
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Manage User</span>
                </div> 
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Managements</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo base_url('users/admin') ?>">Users</a>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>List</span>                            
                        </li>
                    </ul>   
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('users/admin/create') ?>"> Add New User
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">

                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="role">
                    <thead>
                        <tr>
                            <th> Name </th>                           
                            <th> Email </th>
                            <th> Phone No </th>
                            <th> Client </th>                           
                            <th> Role Name </th>   
                            <th> Created By </th>  
                            <th> Is Login </th>
                            <th> Status </th>
                            <th> Action </th>  
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($users)) {
                            foreach ($users as $value) {
                                //  pr($value);
                                ?>
                                <tr>
                                    <td> <?php echo $value->first_name ?> </td>
                                    <td> <?php echo $value->email_id; ?>  </td>
                                    <td> <?php echo $value->phone; ?>  </td>
                                    <td> <?php echo $value->client_title ?> </td>                                    
                                    <td> <?php echo $value->title; ?>  </td> 
                                    <td> <?php echo $value->username; ?>  </td> 
                                    <td> 
                                        <div class="islogin_<?php echo $value->id ?>">
                                            <?php if ($value->is_login == 'Y') { ?>
                                                <a href="javascript:void(0);" title="Sign in"  href="javascript:void(0);" callid="<?php echo $value->id ?>" status="<?php echo $value->is_login ?>" class="btn btn-xs green status_islogin">
                                                    <i class="fa fa-sign-in"></i>
                                                </a>
                                            <?php } else { ?>                                            
                                                <a title="Sign out"  href="javascript:void(0);" class="btn btn-xs green"> 
                                                    <i class="fa fa-sign-out"></i>
                                                </a>
                                            <?php }
                                            ?> 
                                        </div>
                                    </td>
                                    <td>
                                        <input type="checkbox" <?php
                                        if ($value->active_status == 'Y') {
                                            echo 'checked';
                                        }
                                        ?>  id="onoffactiontoggle" class="onoffactiontoggle" myval="<?php echo encode_url($value->id); ?>">
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url() . 'users/admin/edit/' . encode_url($value->id); ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>  
                                         <a href="javascript:void(0);"  userId="<?php echo encode_url($value->id);?>" class="btn btn-xs red isdeleteAdminUsers" onlick=''>
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                    </td>
                                </tr> 
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9"> No Record Found </td>
                            </tr> 
<?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_user.js" type="text/javascript"></script>
