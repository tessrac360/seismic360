<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url() . "public/" ?>treeview/treejs/js/jquery.tree.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>treeview/treejs/css/jquery.tree.css"/>
<script>
    $(document).ready(function () {
        $('#example-0 div').tree();
    });
</script>
<style>
    .ui-widget.ui-widget-content {
        border: 0px solid #cccccc;
    }
    .ui-widget-content {
        border: 0px solid #0000 !important;
        background: top repeat-x !important;
    }
</style>
<!-- END PAGE HEADER-->
<?php error_reporting(0); ?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> User</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Managements</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo base_url('users/admin') ?>">User</a>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>Edit</span>                            
                        </li>
                    </ul>   

                </div>
            </div>
            <?php
            if ($getUser['status'] = 'true') {
                $getResult = $getUser['resultSet'];
                $getRole = $getUser['role'];
                ?>
                <div class="portlet-body form">
                    <form role="form" name="frmEditUser" id="frmEditUser" method="post" action="" autocomplete="off">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Basic Information</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="first_name" name="first_name" value="<?php echo ($getResult->first_name) ? $getResult->first_name : ''; ?>" type="text">
                                       <label for="form_control_1">First Name<span class="required"  aria-required="true">*</span></label>


                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="middle_name" value="<?php echo ($getResult->middle_name) ? $getResult->middle_name : ''; ?>" name="middle_name" type="text">
										<label for="form_control_1">Middle Name<span class="required"  aria-required="true"></span></label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="last_name" value="<?php echo ($getResult->last_name) ? $getResult->last_name : ''; ?>" name="last_name" type="text">
										<label for="form_control_1">Last Name<span class="required"  aria-required="true">*</span></label>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="phone" name="phone" type="text" value="<?php echo ($getResult->phone) ? $getResult->phone : ''; ?>" onkeyup="this.value = this.value.replace(/[^0-9]/g, '')" minlength = "10" maxlength = "10">
                                        <label for="form_control_1">Phone Number<span class="required"  aria-required="true">*</span></label>                                               
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" readonly id="email_id" name="email_id" type="text" value="<?php echo ($getResult->email_id) ? $getResult->email_id : ''; ?>" tableName='users' tableField='email_id'>
                                        <label for="form_control_1">Email<span class="required" aria-required="true" >*</span></label>  
                                        <!--<span id="errmsg" class="error" style="display: none;"></span>-->
                                    </div>
                                </div>  
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input  class="form-control" id="user_name" name="user_name" type="text" value="<?php echo ($getResult->user_name) ? $getResult->user_name : ''; ?>">
                                        <label for="form_control_1">Username<span class="required" aria-required="true">*</span></label>                                               
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase">User Option</span>
                                    </div>
                                </div>


                                <div class="col-md-6" style="margin-top: 10px;">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <select class="form-control" id="admin_client" name="client" >
                                            <option value=""></option>
                                            <?php
                                            if ($client['status'] == 'true') {
                                                foreach ($client['resultSet'] as $value) {
                                                    ?>
                                                    <option value="<?php echo $value->id; ?>" <?= ($getResult->client == $value->id) ? 'selected' : ''; ?>><?php echo $value->client_title; ?></option>  
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </select>                                    
                                        <label for="form_control_1">Client<span class="required" aria-required="true">*</span></label>
                                    </div>
                                </div>
                                <div class="col-md-6" style="margin-top: 10px;">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <select class="form-control edited" name="role_id" id="role_id">
                                            <option value=""></option>   
                                            <?php
                                            if (!empty($getRole)) {
                                                foreach ($getRole as $value) {
                                                    ?>
                                                    <option value="<?php echo $value->id; ?>" <?= ($getResult->role_id == $value->id) ? 'selected' : ''; ?>><?php echo $value->title; ?></option>  
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </select>
                                        <label for="form_control_1">Role & Permission<span class="required" aria-required="true">*</span></label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                        <input type="checkbox"  name ="multi_signon_allow"  <?php if ($getResult->multi_signon_allow==1) echo 'checked="checked"'; ?>> Allow multiple sign-in's
                                        <span></span>
                                    </label>

                                </div>
                                <div id="groupdiv">
                                    <div class="col-md-12">
                                        <div class="caption font-dark">
                                            <span class="caption-subject bold uppercase"> Access Limit</span>
                                        </div>
                                    </div>
									<div id="subClients_list">

										<div class="form-group" >
											<div class="col-sm-12" style="padding: 20px 0 10px 20px">
												<span class="caption-subject  uppercase" > Clients  : </span>
												<br>
												<div id="example-0" style="margin-top: 10px;">
													<div>           
														<ul> 
															<li>

																<label class="mt-checkbox mt-checkbox-outline">                                                                        
																	<input type="checkbox" checked
																	<?php if($accessLimit[$sub_client['id'] . '-' . $sub_client['parent_client_id']]){
																		echo "checked";
																		}?> name="subClients[]" value="<?php echo $sub_client['id'] . '-' . $sub_client['parent_client_id']; ?>" readonly="readonly">
																	<span></span>
																</label>
																<span><?php echo $sub_client['client_title'] ?></span>   
																<?php if (!empty($sub_client['children'])) { ?>       
																	<?php
																	echo childTree($sub_client['children'],$accessLimit);
																	?>
																<?php } ?>
															</li>              
														</ul>
													</div>
												</div>
											</div>
											<div class="col-sm-12" id="group_id_error"></div>
										</div>										
									</div>
									
									

                                    <div class="form-group" >
                                        <div class="col-sm-12" style="padding: 20px 0 10px 20px">
                                            <span class="caption-subject  uppercase" > Domains</span>
                                            <br>
                                            <?php
                                            foreach ($groups as $value) {
                                                ?>	
                                                <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                                    <input type="checkbox" class='groupcheck' name ="group_title[]" <?php if (in_array($value->id, $getDomain)) echo 'checked="checked"'; ?> value="<?php echo $value->id; ?>" id="<?php echo $value->id; ?>"><?php echo $value->title; ?>
                                                    <span></span>
                                                </label>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-12" id="group_id_error"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12" style="padding: 20px 0 10px 20px">
                                            <span class="caption-subject  uppercase" > Skill Set</span>
                                            <br>
                                            <?php
                                            $skill = explode(',', $getResult->skill);
                                            foreach ($skillset as $value) {
                                                ?>	
                                                <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                                    <input type="checkbox"  name ="skill[]" value="<?php echo $value->skill_id; ?>" <?php if (in_array($value->skill_id, $skill)) echo 'checked="checked"'; ?> id="<?php echo $value->skill_id; ?>"><?php echo $value->title; ?>
                                                    <span></span>
                                                </label>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-12" id="skill_error"></div>
                                    </div>
                                </div>


                                <!--                                <div id="groupdiv">
                                                                    <div class="col-md-12">
                                                                        <div class="caption font-dark">
                                                                            <span class="caption-subject bold uppercase"> Send Notification to User</span>
                                                                        </div>
                                                                    </div>	
                                
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12" style="padding: 20px 0 10px 20px">
                                                                            <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                                                                <input type="checkbox"  name ="notification_sms">SMS
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                                                                <input type="checkbox" name ="notification_email">Email
                                                                                <span></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                            </div>
                            <div class="form-actions noborder">
								<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" >
                                <input type="hidden" name="user_type" value="<?php echo encode_url('S'); ?>" >
                                <input  id="old_email"  type="hidden"  value="<?php echo ($getResult->email_id) ? $getResult->email_id : ''; ?>">                                
                                <button type="submit" class="btn green">Save</button>
                                &nbsp; &nbsp; <a href="<?php echo base_url('users/admin'); ?>" class="btn default">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>

            <?php } ?>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
            <?php

        function childTree($treeArray = array(),$accessLimit= array()) {
            ?>
    <ul>
    <?php foreach ($treeArray as $value) {
        ?>
            <li>
                <label class="mt-checkbox mt-checkbox-outline">                                                                    
                    <input type="checkbox" <?php if($accessLimit[$value['id'] . '-' . $value['parent_client_id']]){
																echo "checked";
																
														}?> name="subClients[]" value="<?php echo $value['id'] . '-' . $value['parent_client_id']; ?>">
                    <span></span>
                </label>
                <span><?php echo $value['client_title']; ?></span>
                <?php
                if (!empty($value['children'])) {

                    echo childTree($value['children'],$accessLimit);
                }
                ?>        
            </li>             
    <?php }
    ?>
    </ul>
<?php }
?>
<script>
$(':checkbox[readonly]').click(function(){ return false; });
</script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_user.js" type="text/javascript"></script>





