<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url() . "public/" ?>treeview/treejs/js/jquery.tree.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>treeview/treejs/css/jquery.tree.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('#example-0 div').tree();
    });
</script>
<style>
    .ui-widget.ui-widget-content {
        border: 0px solid #cccccc;
    }
    .ui-widget-content {
        border: 0px solid #0000 !important;
        background: top repeat-x !important;
    }

</style>
<!-- END PAGE HEADER-->
<?php error_reporting(0);
?>
<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
               <span class="caption-subject font-green-steel bold uppercase">Update User</span>
            </div>
            <div class="page-toolbar">
                <ul class="page-breadcrumb breadcrumb custom-bread">
                    <li>
                        <i class="fa fa-cog"></i>
                        <span>Managements</span>  
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo base_url('users') ?>">User</a>                            
                        <i class="fa fa-angle-right"></i>
                    </li>						
                    <li>
                        <span>Edit</span>                            
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="portlet light padd0">
            <?php
            if ($getUser['status'] = 'true') {

                $getResult = $getUser['resultSet'];
                //pr($getResult);
                $getRole = $getUser['role'];
                ?>
                <div class="portlet-body form padd-top0">
                    <form role="form" name="frmEditUser" id="frmEditUser" method="post" action="" autocomplete="off">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Basic Information</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="first_name" name="first_name" value="<?php echo ($getResult->first_name) ? $getResult->first_name : ''; ?>" type="text">
                                        <label for="form_control_1">First Name<span class="required" aria-required="true">*</span></label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="middle_name" value="<?php echo ($getResult->middle_name) ? $getResult->middle_name : ''; ?>" name="middle_name" type="text">
                                        <label for="form_control_1">Middle Name</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="last_name" value="<?php echo ($getResult->last_name) ? $getResult->last_name : ''; ?>" name="last_name" type="text">
                                        <label for="form_control_1">Last Name<span class="required" aria-required="true">*</span></label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" id="phone" name="phone" type="text" value="<?php echo ($getResult->phone) ? $getResult->phone : ''; ?>" onkeyup="this.value = this.value.replace(/[^0-9]/g, '')" minlength = "10" maxlength = "10">
                                        <label for="form_control_1">Phone Number</label>                                               
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input class="form-control" readonly id="email_id" name="email_id" type="text" value="<?php echo ($getResult->email_id) ? $getResult->email_id : ''; ?>" tableName='users' tableField='email_id'>
                                        <label for="form_control_1">Email<span class="required" aria-required="true" >*</span></label>  
                                        <!--<span id="errmsg" class="error" style="display: none;"></span>-->
                                    </div>
                                </div>  
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input  class="form-control" id="user_name" name="user_name" type="text" value="<?php echo ($getResult->user_name) ? $getResult->user_name : ''; ?>">
                                        <label for="form_control_1">Username<span class="required" aria-required="true">*</span></label>                                               
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase">User Option</span>
                                    </div>
                                </div>
                                <div class="col-md-4" style="margin-top: 10px;">
                                    <div class="form-group">
                                        <label for="form_control_1">Role & Permission<span class="required" aria-required="true">*</span></label>
                                        </br><select class="form-control role_id" name="role_id" id="role_id">
                                            <option value=""> Select Role </option>   
                                            <?php
                                            if ($roles['status'] == 'true') {
                                                foreach ($roles['resultSet'] as $value) {
                                                    ?>
                                                    <option <?= ($getResult->role_id == $value->id) ? 'selected' : ''; ?>  value="<?php echo $value->id; ?>"><?php echo $value->title; ?></option>  
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </select>                                    
                                    </div>
                                </div>                         
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input form-md-floating-label" style="margin:-11px 0 0 100px">
                                        <div class="form-group form-md-radios">
                                            <label for="form_control_1">Assign Clients<span class="required" aria-required="true">*</span></label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio"  class="assign_clients" name="assign_client_type" id="assign_clients" value="P" <?= ($getResult->assign_client_type == 'P') ? 'checked' : '';  ?>> Partner
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio"  class="assign_clients" name="assign_client_type" id="assign_clients" value="C" <?= ($getResult->assign_client_type == 'C') ? 'checked' : '';  ?>> Clients
                                                    <span></span>
                                                </label>

                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                
                                <div class="col-md-4 subClients_list" style="margin-top: 10px; <?= ($getResult->assign_client_type == 'P') ? 'display: none' : '';  ?>   " >
                                    <div class="form-group">
                                        <label for="form_control_1">Clients<span class="required" aria-required="true">*</span></label>
                                        </br><select class="form-control client_id" name="client_id" id="client_id">
                                            <option value=""> Select Clients </option>   
                                            <?php
                                            if ($client['status'] == 'true') {
                                                foreach ($client['resultSet'] as $value) {
                                                    ?>
                                                    <option  <?php if ($value->id == $getResult->client) echo 'selected'; ?> value="<?php echo $value->id; ?>"><?php echo $value->client_title .' - '.$value->parent_name; ?></option>  
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </select>                                    
                                    </div>
                                </div>
                                <!-- Start
                                           Name: veeru 
                                           Date: 24/11/17
                                           Functionality: Assign Group Functionality
                                -->
                                <div id="groupdiv">
                                    <div class="col-md-12">
                                        <div class="caption font-dark">
                                            <span class="caption-subject bold uppercase"> Assigning</span>
                                        </div>
                                    </div>                                  
                                    <div class="col-md-12" style="padding: 10px 0 10px 15px">
                                        <label for="form_control_1 col-md-6">Technology/Category :<span class="required" aria-required="true">*</span></label>
                                        <select class="form-control assign_group_id sd" name="technology[]" id="assign_group_id" multiple="multiple">
                                            <option value=""></option>   
                                            <?php
                                            if ($technology['status'] == 'true') {
                                                foreach ($technology['resultSet'] as $value) {
                                                    ?>
                                                    <option <?php if (in_array($value['id'], $gettechnology)) echo 'selected'; ?> value="<?php echo $value['id']; ?>"><?php echo $value['category']; ?></option>  
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </select>
                                        <div class="col-sm-12" id="assign_error"></div>
                                    </div>		

                                    <div class="col-md-12" style="padding: 10px 0 10px 15px">

                                        <label for="form_control_1 col-md-6">Reporting Manager  :</label>
                                        <select class="form-control reporting_manager_id" name="reporting_manager_id" id="reporting_manager_id">
                                            <option value=""> Select options </option>   
                                            <?php
                                            if ($usersList['status'] == 'true') {
                                                foreach ($usersList['resultSet'] as $value) {
                                                    ?>
                                                    <option value="<?php echo $value['id']; ?>" <?= ($getResult->reporting_manager_id == $value['id']) ? 'selected' : ''; ?>><?php echo $value['user']; ?></option>  
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </select>


                                        <div class="col-sm-12" id="report_error"></div>
                                    </div>

                                    <div class="col-md-12" style="padding: 10px 0 10px 15px">
                                        <label for="form_control_1">Access  :</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_mac_access" value="M" <?php echo ($getResult->is_mac_access == 'M') ? "checked" : ""; ?>>MAC
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_mac_access" value="E" <?php echo ($getResult->is_mac_access == 'E') ? "checked" : ""; ?>>Eventedge
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_mac_access" value="B" <?php echo ($getResult->is_mac_access == 'B') ? "checked" : ""; ?>>Both
                                        </label>
                                        <div class="col-sm-12" id="access_error"></div>
                                    </div>

                                    <div class="col-sm-12" style="padding: 10px 0 10px 15px">
                                        <label for="form_control_1">Is Approval  :</label>
                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;margin-right: 25px;    margin-bottom: 6px;">
                                            <input type="checkbox" class='groupcheck' name ="is_approval" id="is_approval" <?php if ($getResult->is_approval == 'Y') echo 'checked="checked"'; ?>>
                                            <span></span>
                                        </label>
                                    </div>

                                </div>
                                <div id="groupdiv">
                                    <div class="col-md-12">
                                        <div class="caption font-dark">
                                            <span class="caption-subject bold uppercase"> Skill Set</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12" style="padding: 20px 0 10px 20px">                                         
                                            <?php
                                            $skill = explode(',', $getResult->skill);
                                            foreach ($skillset as $value) {
                                                ?>	
                                                <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                                    <input type="checkbox" name ="skill[]" value="<?php echo $value->skill_id; ?>" <?php if (in_array($value->skill_id, $skill)) echo 'checked="checked"'; ?> id="<?php echo $value->skill_id; ?>"><?php echo $value->title; ?>
                                                    <span></span>
                                                </label>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-12" id="skill_error"></div>
                                    </div>
                                </div>	
                                <div class="col-sm-12">
                                    <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                        <input type="checkbox" checked readonly name ="multi_signon_allow"  <?php if ($getResult->multi_signon_allow == 1) echo 'checked="checked"'; ?>> Allow multiple sign-in's
                                        <span></span>
                                    </label>

                                </div>
                                <!-- End -->
                            </div>
                            <div class="form-actions noborder">
                                <input type="hidden" name="user_type" value="<?php echo encode_url('U'); ?>" >
                                <input  id="old_email"  type="hidden"  value="<?php echo ($getResult->email_id) ? $getResult->email_id : ''; ?>">                                
                                <button type="submit" class="btn green">Save</button>
                                &nbsp; &nbsp; <a href="<?php echo base_url('users'); ?>" class="btn default">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
    .optionGroup {
        font-weight: bold;        
    }
    .optionChild {
        padding-left: 15px;
    }
</style>


<script>
    $(':checkbox[readonly]').click(function () {
        return false;
    });
    $(function () {
        $('.assign_clients').change(function () {
            var checkVal = $('input[name=assign_client_type]:checked').val();
            $('.subClients_list').hide();
            if (checkVal == 'C') {
                $('.subClients_list').show();
            }
        });

         $('.reporting_manager_id').select2({
	   placeholder: 'Select Options',
	   width: "200px",
	   data: '',      
	});
        $('.client_id').select2({
	   placeholder: 'Select Options',
	   width: "200px",
	   data: '',      
	});
        $('#role_id').select2({
	   placeholder: 'Select Options',
	   width: "200px",
	   data: '',      
	}); 
        $('.assign_group_id').fSelect();

    });

</script>

<script src="<?php echo base_url() . "public/" ?>js/form/form_user.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.js" type="text/javascript"></script>	




