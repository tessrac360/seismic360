<?php 
if(!isset($accessLimit)){
	$accessLimit ="";	
}
//echo "<pre>";pr($accessLimit);exit;?>
<script>
    $(document).ready(function () {
        $('#example-0 div').tree();
    });
</script>
<style>
    .ui-widget.ui-widget-content {
        border: 0px solid #cccccc;
    }
    .ui-widget-content {
        border: 0px solid #0000 !important;
        background: top repeat-x !important;
    }
</style>
<div class="form-group" >
    <div class="col-sm-12" style="padding: 20px 0 10px 20px">
        <span class="caption-subject  uppercase" > Clients  : </span>
        <br>
        <div id="example-0" style="margin-top: 10px;">
            <div>           
                <ul> 
                    <li>
                        <label class="mt-checkbox mt-checkbox-outline">                                                                        
                            <input type="checkbox"  checked name="subClients[]" value="<?php echo $sub_client['id'] . '-' . $sub_client['parent_client_id']; ?>" readonly="readonly" >
                            <span></span>
                        </label>
                        <span><?php echo $sub_client['client_title'] ?></span>   
                        <?php if (!empty($sub_client['children'])) { ?>       
                            <?php
								echo childTree($sub_client['children'],$accessLimit);

                            ?>
                        <?php } ?>
                    </li>              
                </ul>
            </div>
        </div>
    </div>
    <div class="col-sm-12" id="group_id_error"></div>
</div>
<?php

function childTree($treeArray = array(),$accessLimit= array()) { ?>
    <ul>
        <?php foreach ($treeArray as $value) {
            ?>
            <li>
                <label class="mt-checkbox mt-checkbox-outline">                                                                    
                    <input type="checkbox" <?php if(isset($accessLimit[$value['id'] . '-' . $value['parent_client_id']])){
																echo "checked";
																
														}?>  name="subClients[]" value="<?php echo $value['id'] . '-' . $value['parent_client_id']; ?>">
                    <span></span>
                </label>
                <span><?php echo $value['client_title'] ?></span>
                <?php
                if (!empty($value['children'])) {

                    echo childTree($value['children'],$accessLimit);
                }
                ?>        
            </li>             
        <?php }
        ?>
    </ul>
<?php }
?>