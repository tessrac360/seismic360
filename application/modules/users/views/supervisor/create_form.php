<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url() . "public/" ?>treeview/treejs/js/jquery.tree.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>treeview/treejs/css/jquery.tree.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('#example-0 div').tree();
    });
</script>
<style>
    .ui-widget.ui-widget-content {
        border: 0px solid #cccccc;
    }
    .ui-widget-content {
        border: 0px solid #0000 !important;
        background: top repeat-x !important;
    }
</style>
<!-- END PAGE HEADER-->
<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Create User</span>
            </div>
            <div class="page-toolbar">
                <ul class="page-breadcrumb breadcrumb custom-bread">
                    <li>
                        <i class="fa fa-cog"></i>
                        <span>Managements</span>  
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo base_url('users') ?>">User</a>                            
                        <i class="fa fa-angle-right"></i>
                    </li>						
                    <li>
                        <span>Create</span>                            
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">

                <form role="form" name="frmUser" id="frmUser" method="post" action="" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase"> Basic Information</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="first_name" name="first_name" type="text">
                                    <label for="form_control_1">First Name<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="middle_name" name="middle_name" type="text">
                                    <label for="form_control_1">Middle Name</label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="last_name" name="last_name" type="text">
                                    <label for="form_control_1">Last Name<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="phone" name="phone" type="text" onkeyup="this.value = this.value.replace(/[^0-9]/g, '')" minlength = "10" maxlength = "10">
                                    <label for="form_control_1">Phone Number</label>                                               
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="email_id" name="email_id" type="text" tableName='users' tableField='email_id'>
                                    <label for="form_control_1">Email<span class="required" aria-required="true" >*</span></label>  
                                    <!--<span id="errmsg" class="error" style="display: none;"></span>-->
                                </div>
                            </div>  
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  class="form-control" id="user_name" name="user_name" type="text">
                                    <label for="form_control_1">Username<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>



                            <div class="col-md-12" style="margin-top:10px;">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">User Option</span>
                                </div>
                            </div>


                            <div class="col-md-4" style="margin-top: 10px;">
                                <div class="form-group">
                                    <label for="form_control_1">Role & Permission<span class="required" aria-required="true">*</span></label>
                                    </br><select class="form-control role_id" name="role_id" id="role_id">
                                        <option value="0"> Select Role </option>   
                                        <?php
                                        if ($roles['status'] == 'true') {
                                            foreach ($roles['resultSet'] as $value) {
                                                ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->title; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>
                                     <div class="col-sm-12" id="role_error"></div>
                                </div>
                               
                            </div>                         
                            <div class="col-md-4" >
                                <div class="form-group form-md-line-input form-md-floating-label" style="margin:-11px 0 0 100px">
                                    <div class="form-group form-md-radios">
                                        <label for="form_control_1">Assign Clients<span class="required" aria-required="true">*</span></label>
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio"  class="assign_clients" name="assign_client_type" id="assign_clients" value="P" checked=""> Partner
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio"  class="assign_clients" name="assign_client_type" id="assign_clients" value="C" > Clients
                                                <span></span>
                                            </label>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4 subClients_list" style="margin-top: 10px;display: none" >
                                <div class="form-group">
                                    <label for="form_control_1">Clients<span class="required" aria-required="true">*</span></label>
                                    </br><select class="form-control client_id" name="client_id" id="client_id">                                        
                                        <?php
                                        if ($client['status'] == 'true') {
                                            foreach ($client['resultSet'] as $value) {
                                                ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->client_title .' - '.$value->parent_name; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                </div>
                            </div>
                            
                             <div id="groupdiv">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Assigning</span>
                                    </div>
                                </div>

                                <div class="col-md-12" style="padding: 10px 0 10px 15px">
									<label for="form_control_1 col-md-6">Technology/Category :<span class="required" aria-required="true">*</span></label>
                                   
                                    <select class="form-control assign_group_id sd" name="technology[]" id="assign_group_id" multiple="multiple">
                                        <option value=""></option>   
                                        <?php
                                        if ($technology['status'] == 'true') {
                                            foreach ($technology['resultSet'] as $value) {
                                                ?>
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['category']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>


                                    <div class="col-sm-12" id="assign_error"></div>
                                </div>	

                                <div class="col-md-12" style="padding: 10px 0 10px 15px">
									<label for="form_control_1 col-md-6">Reporting Manager  :</label>
                                    
                                    <select class="form-control reporting_manager_id" name="reporting_manager_id" id="reporting_manager_id">
                                        <option value=""> Select options </option>   
                                        <?php
                                        if ($usersList['status'] == 'true') {
                                            foreach ($usersList['resultSet'] as $value) {
                                                ?>
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['user']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>


                                    <div class="col-sm-12" id="report_error"></div>
                                </div>

                                <div class="col-md-12" style="padding: 10px 0 10px 15px">
									<label for="form_control_1">Access  :</label>
                                    
                                    <label class="radio-inline">
                                        <input type="radio" name="is_mac_access" value="M">MAC
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="is_mac_access" checked value="E">Eventedge
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="is_mac_access" value="B">Both
                                    </label>
                                    <div class="col-sm-12" id="access_error"></div>
                                </div>

                                <div class="col-sm-12" style="padding: 10px 0 10px 15px">
                                   
									<label for="form_control_1">Is Approval  :</label>
                                    <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;margin-right: 25px;    margin-bottom: 6px;">
                                        <input  type="checkbox" class='groupcheck' name ="is_approval" id="is_approval" value="Y">
                                        <span></span>
                                    </label>
                                    <div class="col-sm-12" id="approve_error"></div>
                                </div>

                            </div>
                            
                            
                          

                            <div id="groupdiv">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Access Limit</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12" style="padding: 10px 0 10px 15px">
                                        
										<label for="form_control_1">Skill Set  :</label>
                                        <?php
                                        foreach ($skillset as $value) {
                                            ?>	
                                            <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;margin-right: 25px;    margin-bottom: 6px;">
                                                <input type="checkbox" checked="checked"  name ="skill[]" value="<?php echo $value->skill_id; ?>" id="<?php echo $value->skill_id; ?>"><?php echo $value->title; ?>
                                                <span></span>
                                            </label>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-12" id="skill_error"></div>
                                </div>

                            </div>
                            <div id="groupdiv">
                                <div class="col-md-12">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase"> Send Notification to User</span>
                                    </div>
                                </div>	

<!--                                <div class="form-group">
                                    <div class="col-sm-12" style="padding: 10px 0 10px 15px">
                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                            <input type="checkbox" class="notification"  name ="notification_sms">SMS
                                            <span></span>
                                        </label>
                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                            <input type="checkbox" class="notification" name ="notification_email">Email
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12" id="notification_error"></div>
                                </div>-->
                            </div>
                              <div class="col-sm-12">
                                <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
                                    <input type="checkbox" checked="checked" readonly  name ="multi_signon_allow" > Allow multiple sign-in's
                                    <span></span>
                                </label>
                            </div>

                            <!-- Start
                                               Name: veeru 
                                               Date: 24/11/17
                                               Functionality: Assign Group Functionality
                            -->

                           
                            <!-- End -->
                        </div>

                        <div class="form-actions noborder">
                            <input type="hidden" name="user_type" value="<?php echo encode_url('U'); ?>" >
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('users'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .optionGroup {
        font-weight: bold;        
    }
    .optionChild {
        padding-left: 15px;
    }
</style>  
<script>
    $(':checkbox[readonly]').click(function () {
        return false;
    });


    $(function () {
        $('.assign_clients').change(function () {
            var checkVal = $('input[name=assign_client_type]:checked').val();
            $('.subClients_list').hide();
            if (checkVal == 'C') {
                $('.subClients_list').show();
            }
        });
        $('.reporting_manager_id').select2({
			placeholder: 'Select Options',
			width: "200px",
			data: '',      
		});
        $('.client_id').select2({
			placeholder: 'Select Options',
			width: "200px",
			data: '',      
		});
        $('#role_id').select2({
		    placeholder: 'Select Options',
		    width: "200px",
		    data: '',      
		}); 
        $('.assign_group_id').fSelect();

    });

</script> 
        
<script src="<?php echo base_url() . "public/" ?>js/form/form_user.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.js" type="text/javascript"></script>	