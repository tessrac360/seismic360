<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model("Model_users");
        $this->load->model("location/Model_location");
        $this->load->library('form_validation');
        $this->load->model("client/Model_client");
    }

    public function treeClient($client_id = "") {
        $parent_client_id = $client_id;
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeView($parent_client_id, 1);
        if (!empty($resultSet)) {
            foreach ($resultSet as $value) {
                $client['id'] = $value['id'];
                $client['client_title'] = $value['client_title'];
                $client['parent_client_id'] = $value['parent_client_id'];
                $client['children'] = $this->getClientChildren($value['id']);
                $clients[] = $client;
            }
            return $clients[0];
        } else {
            return array();
        }
    }

    public function getClientChildren($parent_id) {
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeView($parent_id);
        foreach ($resultSet as $value) {
            $client['id'] = $value['id'];
            $client['client_title'] = $value['client_title'];
            $client['parent_client_id'] = $value['parent_client_id'];
            $client['children'] = $this->getClientChildren($value['id']);
            $clients[] = $client;
        }
        return $clients;
    }

    public function ajax_subclients() {
        $client_id = $_POST['client_id'];
        if (isset($_POST['user_id'])) {
            $userId = decode_url($_POST['user_id']);
        } else {
            $userId = "";
        }

        //pr($_POST);
        //echo $userId;exit;
        $data['sub_client'] = $this->treeClient($client_id);
        $data['usersSubclientManage'] = $this->Model_users->getUsersSubclientManage($userId);
        if (isset($data['usersSubclientManage']['resultSet'])) {
            foreach ($data['usersSubclientManage']['resultSet'] as $rowVal) {

                $data['accessLimit'][$rowVal['client_id'] . "-" . $rowVal['parent_client_id']] = 1;
            }
        }
        //pr($data['accessLimit']);exit;
        $this->load->view('company/subclients', $data);
    }

//    function _alpha_dash_space($str_in = '') {
//        if (!preg_match("/^([-a-zA-Z0-9_ '])+$/i", $str_in)) {
//            $this->form_validation->set_message('_alpha_dash_space', 'The %s field may only contain alpha-numeric characters, spaces, underscores, single quotes and dashes.');
//            return FALSE;
//        } else {
//            return TRUE;
//        }
//    }

    public function index() {
        //$this->getUser();
        $roleAccess = helper_fetchPermission('52', 'view');
        if ($roleAccess == 'Y') {
            $this->getUser();
        } else {
            redirect('unauthorized');
        }
    }

    public function getUser() {
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'users/admin/getUser/';
        $config['total_rows'] = count($this->Model_users->getUserPagination());
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['users'] = $this->Model_users->getUserPagination($config['per_page'], $page);
        $data['hiddenURL'] = 'users/admin/searchData';
        $data['search'] = '';
        $data['file'] = 'company/view_form';
        $this->load->view('template/front_template', $data);
    }

    public function searchdata() {
        $search = $this->input->get_post('sd');
        if ($search == "") {
            redirect('users');
        }
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'users/admin/searchdata/';
        $config['total_rows'] = count($this->Model_users->getUserSearchData($search));
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $config['first_url'] = base_url() . "users/admin/searchdata/0?sd=" . $search;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['users'] = $this->Model_users->getUserSearchData($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'users/admin/searchData';
        $data['file'] = 'company/view_form';
        $this->load->view('template/front_template', $data);
    }

    public function create() {
        //$getNewData = helper_LogFetchRecordUser('users', 'id', '4');
        $roleAccess = helper_fetchPermission('52', 'add');
        if ($roleAccess == 'Y') {
            $userPost = $this->input->post();
            if ($this->input->post()) {
                $subClients = $userPost['subClients'];
                unset($userPost['subClients']);
                $this->form_validation->set_rules('first_name', 'Name', 'required');
                $this->form_validation->set_rules('phone', 'Mobile No', 'required|max_length[10]|numeric');
                $this->form_validation->set_rules('email_id', 'Email', 'required|valid_email|is_unique[users.email_id]');
                $this->form_validation->set_rules('group_title[]', 'Group', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $userPost['skill'] = implode(",", $userPost['skill']);
                    $userPost = array_merge($userPost, $this->cData);
                    $userPost['password'] = randomPassword();
                    $userPost['multi_signon_allow'] = ($userPost['multi_signon_allow']) ? '1' : 0;
                    $resultData = $this->Model_users->insertUsers($userPost);
                    if ($resultData['status'] == 'true') {
                        $lastInsertId = $resultData['lastId'];
                        $this->manageSubClients($lastInsertId, $subClients);
                        $userPostGroups = array();
                        foreach ($userPost['group_title'] as $value) {
                            $userPostGroups['user_id'] = $lastInsertId;
                            $userPostGroups['domain_id'] = $value;
                            $this->Model_users->insertuserGroups($userPostGroups);
                        }
                        /* Start new Log */
                        $getNewData = helper_LogFetchRecordUser('users', 'id', $lastInsertId);
                        $newValue = $getNewData['log'];
                        $actions = 'CREATE';
                        helper_createLogActions('user_log_manage',$userPost['client'], $actions, $oldValue, $newValue);
                        /* Start new Log */							
                        $this->session->set_flashdata("success_msg", "User is created successfully ..!!");
                        redirect('users/admin');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('users/admin/create');
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['client'] = $this->Model_users->getClient();
                    $data['groups'] = $this->Model_users->getGroups();
                    $data['skillset'] = $this->Model_users->getSkillset();
                    $data['pageTitle'] = 'Your p44age title';
                    $data['file'] = 'company/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['user_type'] = $this->user_type;
                $data['client'] = $this->Model_users->getClient();
                $data['groups'] = $this->Model_users->getGroups();
                $data['skillset'] = $this->Model_users->getSkillset();
                $data['pageTitle'] = 'Your page title';
                $data['file'] = 'company/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    private function manageSubClients($userId = "", $subClient = array()) {
        //echo "<pre>";pr($subClient);exit;
        $manageSubClients = array();
        foreach ($subClient as $values) {

            $value = explode("-", $values);
            $manageSubClients['user_id'] = $userId;
            $manageSubClients['client_id'] = $value[0];
            $manageSubClients['parent_client_id'] = $value[1];
            $res = $this->Model_users->insertUsersSubclientManage($manageSubClients);
        }
    }

    public function ajax_checkUnique() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $id = $this->input->post('id');
        $old_email = $this->input->post('old_email');
        if ($id != "") {
            $this->db->where('id !=', $id);
        }
        if ($old_email != "") {
            $this->db->where_not_in($field, $old_email);
        }

        $recordCount = $this->db->get_where($table, array($field => $value))->num_rows();
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

    function checkuseremail($email) {
        if ($this->input->post('id'))
            $id = $this->input->post('id');
        else
            $id = '';
        $result = $this->Model_users->check_unique_user_email($id, $email);
        if ($result == 0)
            $response = true;
        else {
            $this->form_validation->set_message('check_user_email', 'Email must be unique');
            $response = false;
        }
        return $response;
    }

    public function edit($postId = NULL) {

        $roleAccess = helper_fetchPermission('52', 'view');
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId);
            $getStatus = $this->Model_users->isExitUser($postId);
            if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
					
                    $postUsers = $this->input->post();
                    $postUsers = $this->input->post();
                    $subClients = $postUsers['subClients'];
                    //echo "<pre>";pr($subClients);exit;
                    //pr($userPost);exit;
                    unset($postUsers['subClients']);
                    unset($postUsers['user_id']);

                    $this->form_validation->set_rules('first_name', 'Name', 'required');
                    $this->form_validation->set_rules('phone', 'Mobile No', 'required|max_length[10]|numeric');
                    $this->form_validation->set_rules('group_title[]', 'Group', 'required');
                    //$this->form_validation->set_rules('email_id', 'Email', 'required|valid_email|callback_checkuseremail');
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
						/* Start Old Log */
						$getoldData = helper_LogFetchRecordUser('users', 'id', $postId);
						$oldValue = $getoldData['log'];
						/* Start Old Log */						
                        unset($postUsers['id']);
                        $postUsers['skill'] = implode(",", $postUsers['skill']);
                        $postUsers = array_merge($postUsers, $this->uData);
                        $postUsers['multi_signon_allow'] = ($postUsers['multi_signon_allow']) ? '1' : 0;
                        //pr($postUsers);exit;
                        //pr($postUsers);
                        //exit;
                        $updateStatus = $this->Model_users->updateUsers($postUsers, $postId);
                        if ($updateStatus['status'] == 'true') {
                            $this->Model_users->deleteUsersSubclientManage($postId);
                            $this->manageSubClients($postId, $subClients);
                            $userPostGroups = array();
                            $this->Model_users->deleteuserGroups($postId);
                            foreach ($postUsers['group_title'] as $value) {
                                $userPostGroups['user_id'] = $postId;
                                $userPostGroups['domain_id'] = $value;
                                $this->Model_users->updateuserGroups($userPostGroups);
                            }
                            /* Start new Log */
                            $getNewData = helper_LogFetchRecordUser('users', 'id', $postId);
                            $newValue = $getNewData['log'];
                            $actions = 'EDIT';
                            helper_createLogActions('user_log_manage', $postUsers['client'], $actions, $oldValue, $newValue);
                            /* Start new Log */								
                            $this->session->set_flashdata("success_msg", "User is Updated successfully ..!!");
                            redirect('users/admin');
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('users/admin/edit/' . encode_url($postId));
                        }
                    } else {
                        $data['user_type'] = $this->user_type;
                        $data['getUser'] = $this->Model_users->getUsersEdit($postId);
                        $data['getDomain'] = $this->Model_users->getGroupsofcompany($postId);
                        $data['client'] = $this->Model_users->getClient();
                        $data['groups'] = $this->Model_users->getGroups();
                        $data['skillset'] = $this->Model_users->getSkillset();
                        $data['usersSubclientManage'] = $this->Model_users->getUsersSubclientManage($postId);
                        if (isset($data['usersSubclientManage']['resultSet'])) {
                            foreach ($data['usersSubclientManage']['resultSet'] as $rowVal) {

                                $data['accessLimit'][$rowVal['client_id'] . "-" . $rowVal['parent_client_id']] = 1;
                            }
                        }
                        $data['user_id'] = encode_url($postId);
                        $data['sub_client'] = $this->treeClient($data['getUser']['resultSet']->client);
                        $data['pageTitle'] = 'Your p44age title';
                        $data['file'] = 'company/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['getUser'] = $this->Model_users->getUsersEdit($postId);
                    $data['getDomain'] = $this->Model_users->getGroupsofcompany($postId);
                    $data['client'] = $this->Model_users->getClient();
                    $data['groups'] = $this->Model_users->getGroups();
                    $data['skillset'] = $this->Model_users->getSkillset();
                    $data['usersSubclientManage'] = $this->Model_users->getUsersSubclientManage($postId);
                    if (isset($data['usersSubclientManage']['resultSet'])) {
                        foreach ($data['usersSubclientManage']['resultSet'] as $rowVal) {

                            $data['accessLimit'][$rowVal['client_id'] . "-" . $rowVal['parent_client_id']] = 1;
                        }
                    }
                    $data['user_id'] = encode_url($postId);
                    $data['sub_client'] = $this->treeClient($data['getUser']['resultSet']->client);
                    $data['pageTitle'] = 'Your p44age title';
                    $data['file'] = 'company/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('/users');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
		$data['getUser'] = $this->Model_users->getUsersEdit($id);
		$clientId = $data['getUser']['resultSet']->client;
		
		/* Start Old Log */
		$getoldData = helper_LogFetchRecordUser('users', 'id', $id);
		$oldValue = $getoldData['log'];
		/* Start Old Log */		
        if ($id != "") {
            $Status = $this->Model_users->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
				/* Start new Log */
				$getNewData = helper_LogFetchRecordUser('users', 'id', $id);
				$newValue = $getNewData['log'];
				$actions = 'STATUS';
				helper_createLogActions('user_log_manage', $clientId, $actions, $oldValue, $newValue);
				/* Start new Log */				
                if ($status == 'true') {
                    $return['message'] = 'User Account is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'User Account is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function unauthorized() {
        $data['file'] = 'unauthorized_form';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_getRoles() {
        $clientId = $this->input->post('clientId');
        if ($clientId != "") {
            $status = $this->Model_users->getRoles($clientId);
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

    public function delete($postId = "") {
		
        $postId = decode_url($postId);
		$data['getUser'] = $this->Model_users->getUsersEdit($postId);
		$clientId = $data['getUser']['resultSet']->client;
		
		/* Start Old Log */
		$getoldData = helper_LogFetchRecordUser('users', 'id', $postId);
		$oldValue = $getoldData['log'];
		
        if ($postId != "") {
            $deleteRole = $this->Model_users->isDeleteUser($postId);
            if ($deleteRole['status'] == 'true') {
				/* Insert Log */
				$actions = 'DELETE';
				helper_createLogActions('user_log_manage', $clientId, $actions, $oldValue, $newValue);
				/* Insert Log */				
                $this->session->set_flashdata("success_msg", "User is deleted successfully..!!");
                redirect('users/admin');
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('users/admin');
            }
        } else {
            redirect('roles');
        }
    }

}
