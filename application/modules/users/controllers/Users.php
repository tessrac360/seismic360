<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->userId = $this->session->userdata('id');
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model("Model_users");
        $this->load->model("client/Model_client");
        $this->load->model("location/Model_location");
        $this->getCompanyId = helpler_getCompanyId();
        $this->load->library('form_validation');
    }

//    function _alpha_dash_space($str_in = '') {
//        if (!preg_match("/^([-a-zA-Z0-9_ '])+$/i", $str_in)) {
//            $this->form_validation->set_message('_alpha_dash_space', 'The %s field may only contain alpha-numeric characters, spaces, underscores, single quotes and dashes.');
//            return FALSE;
//        } else {
//            return TRUE;
//        }
//    }


    public function ajax_checkUnique() {
//pr($_POST);exit;
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $id = $this->input->post('id');
        $old_email = $this->input->post('old_email');
        if ($id != "") {
            $this->db->where('id !=', $id);
        }
        if ($old_email != "") {
            $this->db->where_not_in($field, $old_email);
        }

        $recordCount = $this->db->get_where($table, array($field => $value))->num_rows();
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

    function checkuseremail($email) {
        if ($this->input->post('id'))
            $id = $this->input->post('id');
        else
            $id = '';
        $result = $this->Model_users->check_unique_user_email($id, $email);
        if ($result == 0)
            $response = true;
        else {
            $this->form_validation->set_message('check_user_email', 'Email must be unique');
            $response = false;
        }
        return $response;
    }

    public function ajax_logout() {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
        $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : '';
        if ($id != '' && $status != '') {
            $getstatus = $this->Model_users->isLoginUpdate($id, $status);
            if ($getstatus['status'] == 'true') {
                $return['message'] = 'Status updated successfully';
                $return['status'] = 'true';
                $return['html'] = $getstatus['data'];
            } else {
                $return['status'] = 'false';
                $return['msg'] = 'Something went wrong';
            }
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        echo $json = json_encode($return);
        exit;
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        $data['getUser'] = $this->Model_users->getUsersEdit($id);
        $clientId = $data['getUser']['resultSet']->client;

        /* Start Old Log */
        $getoldData = helper_LogFetchRecordUser('users', 'id', $id);
        $oldValue = $getoldData['log'];

        /* Start Old Log */
        if ($id != "") {
            $Status = $this->Model_users->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                /* Start new Log */
                $getNewData = helper_LogFetchRecordUser('users', 'id', $id);
                $newValue = $getNewData['log'];
                $actions = 'STATUS';
                helper_createLogActions('user_log_manage', $clientId, $actions, $oldValue, $newValue);
                /* Start new Log */
                if ($status == 'true') {
                    $return['message'] = 'User Account is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'User Account is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function unauthorized() {
        $data['file'] = 'unauthorized_form';
        $this->load->view('template/front_template', $data);
    }

    /* Start User module */

    public function ajax_userdata() {
        $accessEdit = helper_fetchPermission('13', 'edit');
        $accessStatus = helper_fetchPermission('13', 'active');
        $accessDelete = helper_fetchPermission('13', 'delete');
        $list = $this->Model_users->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $users) {
            $no++;
            $active_status = ($users->active_status == "Y") ? 'checked' : "";
            $row = array();
            $row[] = $users->first_name;
            $row[] = $users->email_id;
            $row[] = $users->phone;
            $row[] = $users->client_title;
            $row[] = $users->title;
            $row[] = $users->username;
            $row[] = ($accessStatus == 'Y') ? '<label class="mt-checkbox mt-checkbox-outline"><input type="checkbox"  class="onoffactiontoggle" myval="' . encode_url($users->id) . '" ' . $active_status . '><span></span></label>' : '';
            $row[] = ($accessEdit == 'Y') ? '<a href="' . base_url() . 'users/edit/' . encode_url($users->id) . '" class="btn btn-xs  blue"><i class="fa fa-edit"></i></a>' : "";
            ;
            $row[] = ($accessDelete == 'Y') ? '<a href="javascript:void(0);"  userId="' . encode_url($users->id) . '" class="btn btn-xs red isdeleteUser"><i class="fa fa-trash"></i></a>' : '';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => 0,
            "recordsFiltered" => $this->Model_users->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function index() {
        $roleAccess = helper_fetchPermission('13', 'view');
        if ($roleAccess == 'Y') {

            $data['file'] = 'supervisor/view_form';
            $this->load->view('template/front_template', $data);
        } else {
            redirect('unauthorized');
        }
    }

    public function treeClientDropdown() {
        $parent_client_id = $this->session->userdata('client_id');
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeView($parent_client_id, 1);
        if (!empty($resultSet)) {
            foreach ($resultSet as $value) {
                //  pr($value);
                $client['id'] = $value['id'];
                $client['client_title'] = $value['client_title'];
                $client['parent_client_id'] = $value['parent_client_id'];
                $client['children'] = $this->getClientChildren($value['id']);
                $clients[] = $client;
            }
            return $clients;
        } else {
            return array();
        }
    }

    public function treeClient($client_id = "") {
        if ($client_id != "") {
            $parent_client_id = $client_id;
        } else {
            $parent_client_id = $this->session->userdata('client_id');
        }
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeView($parent_client_id, 1);
        if (!empty($resultSet)) {
            foreach ($resultSet as $value) {
                $client['id'] = $value['id'];
                $client['client_title'] = $value['client_title'];
                $client['parent_client_id'] = $value['parent_client_id'];
                $client['children'] = $this->getClientChildren($value['id']);
                $clients[] = $client;
            }
            return $clients[0];
        } else {
            return array();
        }
    }

    public function getClientChildren($parent_id) {
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeView($parent_id);
        foreach ($resultSet as $value) {
            $client['id'] = $value['id'];
            $client['client_title'] = $value['client_title'];
            $client['parent_client_id'] = $value['parent_client_id'];
            $client['children'] = $this->getClientChildren($value['id']);
            $clients[] = $client;
        }
        return $clients;
    }

    public function create() {

        //pr($_SESSION);exit;
        $roleAccess = helper_fetchPermission('13', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $userPost = $this->input->post();
                $this->form_validation->set_rules('first_name', 'Name', 'required');
               // $this->form_validation->set_rules('phone', 'Mobile No', 'required|max_length[10]|numeric');
                $this->form_validation->set_rules('email_id', 'Email', 'required|valid_email|is_unique[users.email_id]');
                $this->form_validation->set_rules('technology[]', 'Assign Group', 'required');
                //$this->form_validation->set_rules('reporting_manager_id', 'Reporting Manager', 'required');
                //$this->form_validation->set_rules('is_mac_access', 'Access', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $userPost['skill'] = implode(",", $userPost['skill']);
                    $userPost = array_merge($userPost, $this->cData);
                    $userPost['password'] = randomPassword();
                    $userPost['multi_signon_allow'] = (isset($userPost['multi_signon_allow'])) ? '1' : 0;

                    /* Start
                      Name: veeru
                      Date: 24/11/17
                      Functionality: Inserting 3 more columns values into eventedge_users table
                     */
                    $userPost['reporting_manager_id'] = $userPost['reporting_manager_id'];
                    $userPost['is_mac_access'] = $userPost['is_mac_access'];

                    $userPost['is_approval'] = (isset($userPost['is_approval'])) ? 'Y' : 'N';
                    /* End */
                    //pr($userPost);exit;
                    $resultData = $this->Model_users->insertUsers($userPost);
                    if ($resultData['status'] == 'true') {
                        $lastInsertId = $resultData['lastId'];
                        //$this->manageSubClients($lastInsertId, $subClients);
                        /* Start
                          Name: veeru
                          Date: 24/11/17
                          Functionality: Inserting Assigned Group Id's into eventedge_users_assigned_group table
                         */
                        $technologyPost = array();
                        foreach ($userPost['technology'] as $value) {
                            $technologyPost['user_id'] = $lastInsertId;
                            $technologyPost['tech_id'] = $value;
                            $technologyPost['partner_id'] = $this->getCompanyId;
                            $this->Model_users->insertTechnology($technologyPost);
                        }
                        /* End */

                        /* Start new Log */
                        $getNewData = helper_LogFetchRecordUser('users', 'id', $lastInsertId);
                        $newValue = $getNewData['log'];
                        $actions = 'CREATE';
                        helper_createLogActions('user_log_manage', $userPost['client'], $actions, $oldValue, $newValue);
                        /* Start new Log */
                        $this->session->set_flashdata("success_msg", "User is created successfully ..!!");
                        redirect('users');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('users/create');
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['client'] = $this->Model_users->getParentClients();
                    $data['roles'] = $this->Model_users->getRoles($this->getCompanyId);
                    $data['skillset'] = $this->Model_users->getSkillset();
                    $data['technology'] = $this->Model_users->getTechnology();
                    $data['usersList'] = $this->Model_users->getUsersListByCompanyId();
                    $data['file'] = 'supervisor/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['user_type'] = $this->user_type;
                $data['client'] = $this->Model_users->getParentClients();
                $data['groups'] = $this->Model_users->getDomainUser();
                $data['technology'] = $this->Model_users->getTechnology();
                $data['usersList'] = $this->Model_users->getUsersListByCompanyId();
                $data['roles'] = $this->Model_users->getRoles($this->getCompanyId);
                $data['skillset'] = $this->Model_users->getSkillsetUser();
                $data['file'] = 'supervisor/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    private function manageSubClients($userId = "", $subClient = array()) {
        //echo "<pre>";pr($subClient);exit;
        $manageSubClients = array();
        foreach ($subClient as $values) {

            $value = explode("-", $values);
            $manageSubClients['user_id'] = $userId;
            $manageSubClients['client_id'] = $value[0];
            $manageSubClients['parent_client_id'] = $value[1];
            $res = $this->Model_users->insertUsersSubclientManage($manageSubClients);
        }
    }

    public function edit($postId = NULL) {
        $roleAccess = helper_fetchPermission('13', 'edit');
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId); //exit;
            $getStatus = $this->Model_users->isExitUser($postId);
            //pr($getStatus);exit;
            if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
                    $postUsers = $this->input->post();
                    //pr($postUsers);
                   // exit;
                    unset($postUsers['subClients']);
                    $this->form_validation->set_rules('first_name', 'Name', 'required');
                    //$this->form_validation->set_rules('phone', 'Mobile No', 'required|max_length[10]|numeric');
                    $this->form_validation->set_rules('email_id', 'Email', 'required|valid_email');
                    $this->form_validation->set_rules('technology[]', 'Assign Group', 'required');
                   // $this->form_validation->set_rules('reporting_manager_id', 'Reporting Manager', 'required');
                    //$this->form_validation->set_rules('email_id', 'Email', 'required|valid_email|callback_checkuseremail');
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
                        /* Start Old Log */
                        $getoldData = helper_LogFetchRecordUser('users', 'id', $postId);
                        $oldValue = $getoldData['log'];
                        /* Start Old Log */
                        unset($postUsers['id']);
                        $postUsers['skill'] = implode(",", $postUsers['skill']);
                        $postUsers = array_merge($postUsers, $this->uData);
                        $postUsers['multi_signon_allow'] = ($postUsers['multi_signon_allow']) ? '1' : 0;



                        /* Start
                          Name: veeru
                          Date: 24/11/17
                          Functionality: Inserting 3 more columns values into eventedge_users table
                         */
                        $postUsers['reporting_manager_id'] = $postUsers['reporting_manager_id'];
                        $postUsers['is_mac_access'] = $postUsers['is_mac_access'];
                        $postUsers['is_approval'] = ($postUsers['is_approval']) ? 'Y' : 'N';
                        /* End */

                        //pr($postUsers);exit;
                        //pr($postUsers);
                        //exit;
                        $updateStatus = $this->Model_users->updateUsers($postUsers, $postId);
                        if ($updateStatus['status'] == 'true') {
//                            $this->Model_users->deleteUsersSubclientManage($postId);
//                            $this->manageSubClients($postId, $subClients);
                            $technologyPost = array();
                            $this->Model_users->deleteTechnology($postId);
                            foreach ($postUsers['technology'] as $value) {                             
                                $technologyPost['user_id'] = $postId;
                                $technologyPost['tech_id'] = $value;
                                $technologyPost['partner_id'] = $this->getCompanyId;
                                $this->Model_users->updateuserTechnology($technologyPost);
                            }




                            /* Start
                              Name: veeru
                              Date: 24/11/17
                              Functionality: Inserting Assigned Group Id's into eventedge_users_assigned_group table
                             */
//                            $userAssignGroupPost['user_id'] = $postId;
//                            $userAssignGroupPost['assign_group_id'] = implode(",", $postUsers['assign_group_id']);
//                            $this->Model_users->UpdateUserAssignGroups($userAssignGroupPost, $postId);
                            /* End */

                            /* Start new Log */
                            $getNewData = helper_LogFetchRecordUser('users', 'id', $postId);
                            $newValue = $getNewData['log'];
                            $actions = 'EDIT';
                            helper_createLogActions('user_log_manage', $postUsers['client'], $actions, $oldValue, $newValue);
                            /* Start new Log */
                            $this->session->set_flashdata("success_msg", "User is Updated successfully ..!!");
                            redirect('users');
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('users/edit/' . encode_url($postId));
                        }
                    } else {
                        $data['user_type'] = $this->user_type;
                        $data['getUser'] = $this->Model_users->getUsersEdit($postId);
                        $data['client'] = $this->Model_users->getParentClients();
                        $data['groups'] = $this->Model_users->getDomainUser();
                        $data['technology'] = $this->Model_users->getTechnology();
                        $data['usersList'] = $this->Model_users->getUsersListByCompanyId();
                        $data['roles'] = $this->Model_users->getRoles($this->getCompanyId);
                        $data['skillset'] = $this->Model_users->getSkillsetUser();
                        $data['gettechnology'] = $this->Model_users->getTechnologyById($postId);
                        $data['file'] = 'supervisor/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['getUser'] = $this->Model_users->getUsersEdit($postId);
                    $data['client'] = $this->Model_users->getParentClients();
                    $data['groups'] = $this->Model_users->getDomainUser();
                    $data['technology'] = $this->Model_users->getTechnology();
                    $data['usersList'] = $this->Model_users->getUsersListByCompanyId();
                    $data['roles'] = $this->Model_users->getRoles($this->getCompanyId);
                    $data['skillset'] = $this->Model_users->getSkillsetUser();
                    $data['gettechnology'] = $this->Model_users->getTechnologyById($postId);
                    //pr($data);exit;
                    $data['file'] = 'supervisor/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('/users');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function my_profile() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('first_name', 'Name', 'required');
            $this->form_validation->set_rules('phone', 'Mobile No', 'required|max_length[10]|numeric');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
            if ($this->form_validation->run() == TRUE) {
                $postUsers = $this->input->post();
                pr($_FILES);

                if (isset($_FILES['user_avatar']['name']) && $_FILES['user_avatar']['name'] != '') {
                    $NewName = time() . rand() . '__' . $_FILES['user_avatar']['name'];
                    $upload_path = 'public/images/profile_image/';
                    $tempName = $_FILES['user_avatar']['tmp_name'];
                    $newpath = move_uploaded_file($tempName, $upload_path . $NewName);
                    $postUsers['user_avatar'] = base_url() . 'public/images/profile_image/' . $NewName;
                }

//                pr($postUsers);
//                exit;
                $resultData = $this->Model_users->updateProfile($postUsers);
                if ($resultData['status'] == 'true') {
                    $this->session->set_flashdata("success_msg", "Your Profile is updated successfully ..!!");
                    redirect('users/my_profile');
                } else {
                    $this->session->set_flashdata("error_msg", "Some thing went wrong");
                    redirect('users/my_profile');
                }
            } else {
                $data['getDomain'] = $this->Model_users->getGroupsofcompany($this->userId);
                $data['user'] = $this->Model_users->getUserDetails();
                $data['groups'] = $this->Model_users->getDomainUser();
                $data['skillset'] = $this->Model_users->getSkillsetUser();
                $data['pageTitle'] = 'Your page title';
                $data['file'] = 'change_profile';
                $this->load->view('template/front_template', $data);
            }
        } else {
            $data['getDomain'] = $this->Model_users->getGroupsofcompany($this->userId);
            $data['user'] = $this->Model_users->getUserDetails();
            $data['groups'] = $this->Model_users->getDomainUser();
            $data['skillset'] = $this->Model_users->getSkillsetUser();
            $data['pageTitle'] = 'Your page title';
            $data['file'] = 'change_profile';
            $this->load->view('template/front_template', $data);
        }
    }

    public function change_password() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
            if ($this->form_validation->run() == TRUE) {
                $postUsers = $this->input->post();

                $isCheckOldPassword = $this->Model_users->oldPasswordCheck($postUsers);
                if ($isCheckOldPassword['status'] == 'true') {
                    unset($postUsers['password_confirm']);
                    unset($postUsers['old_password']);
                    $resultData = $this->Model_users->changePassword($postUsers);
                    if ($resultData['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "Your Password is changed successfully ..!!");
                        redirect('users/change_password');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('users/change_password');
                    }
                } else {
                    $this->session->set_flashdata("error_msg", "Old Password does not match");
                    redirect('users/change_password');
                }
            } else {
                $data['file'] = 'change_password';
                $this->load->view('template/front_template', $data);
            }
        } else {
            $data['file'] = 'change_password';
            $this->load->view('template/front_template', $data);
        }
    }

    public function delete($postId = "") {
        $postId = decode_url($postId);
        $data['getUser'] = $this->Model_users->getUsersEdit($postId);
        $clientId = $data['getUser']['resultSet']->client;

        /* Start Old Log */
        $getoldData = helper_LogFetchRecordUser('users', 'id', $postId);
        $oldValue = $getoldData['log'];
        /* Start Old Log */
        if ($postId != "") {
            $deleteRole = $this->Model_users->isDeleteUser($postId);
            if ($deleteRole['status'] == 'true') {
                /* Insert Log */
                $actions = 'DELETE';
                helper_createLogActions('user_log_manage', $clientId, $actions, $oldValue, $newValue);
                /* Insert Log */
                $this->session->set_flashdata("success_msg", "User is deleted successfully..!!");
                redirect('users');
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('users');
            }
        } else {
            redirect('users');
        }
    }

    public function siddhu() {
       
        $cxx = helper_getDashBoardStatus('315');
        pr($cxx);
    }

    /* End User module */
}
