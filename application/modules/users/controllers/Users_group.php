<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_group extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->userId = $this->session->userdata('id');
		$this->client_id = $this->session->userdata('client_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('modified_on' => $current_date, 'modified_by' => $current_user);
        $this->load->model("Model_users_group");
        //$this->load->model("client/Model_client");
        //$this->load->model("location/Model_location");
        $this->load->library('form_validation');
    }

    public function index() {
        $roleAccess = helper_fetchPermission('79', 'view');
        if ($roleAccess == 'Y') {
            $search = $this->input->get_post('sd');
			$this->load->library('pagination');
			$config['per_page'] = PAGINATION_PERPAGE;
			$config['base_url'] = base_url() . 'users/users_group/index/';
			$config['total_rows'] = $this->Model_users_group->getUsersGroups($search);
			//pr($config['total_rows']);exit;
			$config['uri_segment'] = 4;		      
			$config['display_pages'] = TRUE;
			$config['enable_query_strings'] = true;
			if($_GET){
				$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
			}		
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;      
			$data['user_groups'] = $this->Model_users_group->getUsersGroups($search, $config['per_page'], $page);
			//pr($data['queue']);exit;
			$data['hiddenURL'] = 'users/users_group/index/';
			//pr($data['clientTitles']);exit;
			$data['file'] = 'users_group/view_form';
			$data['search'] = $search;
			$this->load->view('template/front_template', $data);
			
         } else {
            redirect('unauthorized');
        } 
    }

    public function create() {
        $roleAccess = helper_fetchPermission('79', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $userPost = $this->input->post();
                $this->form_validation->set_rules('group_name', 'Group Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $userPost['group_name'] = $userPost['group_name'];
                    $userPost['client_id'] = $this->client_id;
                    $userPost['created_by'] = $this->cData['created_by'];
                    $userPost['created_on'] = $this->cData['created_on'];
                    $userPost['modified_by'] = $this->uData['modified_by'];
                    $userPost['modified_on'] = $this->uData['modified_on'];
                    
                    $resultData = $this->Model_users_group->insertUsersGroup($userPost);
                    if ($resultData['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "User Group is created successfully ..!!");
                        redirect('users/users_group');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('users/users_group/create');
                    }
                } else {
                    $data['file'] = 'users_group/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['file'] = 'users_group/create_form';
                //pr($data['client']);
                //exit;
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        } 
    }

    public function edit($postId = NULL) {
        $roleAccess = helper_fetchPermission('79', 'edit');
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId); //exit;
                if ($this->input->post()) {
                    $userPost = $this->input->post();
                    $this->form_validation->set_rules('group_name', 'Group Name', 'required');
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
                        $userPost['group_name'] = $userPost['group_name'];
						$userPost['client_id'] = $this->client_id;
						$userPost['created_by'] = $this->cData['created_by'];
						$userPost['created_on'] = $this->cData['created_on'];
						$userPost['modified_by'] = $this->uData['modified_by'];
						$userPost['modified_on'] = $this->uData['modified_on'];
						$updateStatus = $this->Model_users_group->updateUsersGroup($userPost, $postId);
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "UserGroup is Updated successfully ..!!");
                            redirect('users/users_group');
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('users/users_group/edit/' . encode_url($postId));
                        }
                    } else {
						$data['getUsersGroupByID'] = $this->Model_users_group->getUsersGroupEdit($postId);
						$data['file'] = 'users_group/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['getUsersGroupByID'] = $this->Model_users_group->getUsersGroupEdit($postId);
                    $data['file'] = 'users_group/update_form';
                    $this->load->view('template/front_template', $data);
                }
            
        } else {
            redirect('unauthorized');
        }
    }

	 public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Model_users_group->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                
                if ($status == 'true') {
                    $return['message'] = 'User Account is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'User Account is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }
	
	
	public function ajax_checkUniqueGroupName() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $recordCount = $this->db->get_where($table, array($field => $value))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
}
