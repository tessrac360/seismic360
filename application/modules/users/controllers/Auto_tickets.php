<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auto_tickets extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->userId = $this->session->userdata('id');
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model("Model_users");
        $this->load->model("Model_tickets","tickets");
        $this->load->model("client/Model_client");
        $this->load->model("location/Model_location");
        $this->load->model("manual/incidents_model",'inc');
        $this->load->library('form_validation');
    }
	
	##############################################################################################################
	#	Incident create based on events, which events have incident_id = 0                                       #
	#	Once Incident is created it will update same incident Id for events which events has same device id      #
	#   Incidents are creating through CRON job for every 1 minute. And there is also manual creation            #
	##############################################################################################################   
	
	public function generate_tickets()
	{
		$tickets = $this->tickets->get_latest_tickets();
		
		$macd = $this->tickets->get_latest_macd();
		$end_time = date("h:i:s", time() + 30); // add 30 seconds for present time
		if(isset($macd) && !empty($macd))
		{
			
			
			
			$subject = "";
			foreach($macd as $macd_data){
				$clientUUID = ""; $partnerId = "";
				/* Get client_uuid and partnerId */
				$getUUIDofClient = $this->inc->getAllClients($macd_data['client_id']);
				if(isset($getUUIDofClient['resultSet']) && (count($getUUIDofClient['resultSet'])>0))
				{
					$clientUUID = $getUUIDofClient['resultSet'][0]['client_uuid'];
					$partnerId = $getUUIDofClient['resultSet'][0]['partner_id'];
					if($partnerId == 1)
					{
						$partnerId = $macd_data['client_id'];
					}
				}
				/* Get client_uuid and partnerId ends */
				
				
				
				if($macd_data['is_approved'] === 0){
				$firstNameFrom = (isset($macd_data['firstname_from']) && !empty($macd_data['firstname_from']))?'FirstName(From): '.$macd_data['firstname_from']:'';
				$LastNameFrom = (isset($macd_data['lastname_from']) && !empty($macd_data['lastname_from']))?'LastName(From): '.$macd_data['lastname_from']:'';
				$phone = (isset($macd_data['phone']) && !empty($macd_data['phone']))?'Phone: '.$macd_data['phone']:'';
				$device = (isset($macd_data['Device']) && !empty($macd_data['Device']))?'Device: '.$macd_data['Device']:'';
				$mac_address = (isset($macd_data['mac_address']) && !empty($macd_data['mac_address']))?'Mac Address: '.$macd_data['mac_address']:'';
				$dialing_capabilities = (isset($macd_data['dialing_capabilities']) && !empty($macd_data['dialing_capabilities']))?'Dialing Capabilities: '.$macd_data['dialing_capabilities']:'';
				$location_to = (isset($macd_data['locationTo']) && !empty($macd_data['locationTo']))?'Location To: '.$macd_data['locationTo']:'';
				$required_by = ($macd_data['required_by'] != '0000-00-00 00:00:00')?'Required by: '.$macd_data['required_by']:'';
				$firstNameTo = (isset($macd_data['firstname_to']) && !empty($macd_data['firstname_to']))?'FirstName(To): '.$macd_data['firstname_to']:'';
				$LastNameTo = (isset($macd_data['lastname_to']) && !empty($macd_data['lastname_to']))?'LastName(To): '.$macd_data['lastname_to']:'';
				$subject .= $firstNameFrom.' <br/>'.$LastNameFrom.' <br/>'.$phone.' <br/>'.$device.' <br/>'.$mac_address.' <br/>'.$dialing_capabilities.' <br/>'.$location_to.' <br/>'.$required_by.' <br/>'.$firstNameTo.' <br/>'.$LastNameTo;
				$macd_array = array(
					'subject' => $subject, //host_name-alaram_type-Seviority-State
					 'description' => $subject,
					 'queue_id' => 1,				 
					 'owner_id' =>  $macd_data['client_id'], //Client_id 
					 'requestor' => $macd_data['requested_by'], 
					 'status_code' => 0, //state_code // which means it is new
					 'priority' => 3, //by defult urgency is Low
					 'created_by' => 1, //created by defult 1 System genrate ticket refering to user table
					 'created_on' => date('Y-m-d H:i:s'), 
					 'assigned_to' => 0, 
					 'due_date' => 0,
					 'estimated_time' =>  0, 
					 'client_id' => $macd_data['client_id'], 
					 'skill_id' => 4,//TASK->Sev-4
					 'event_id' => 0, //event_id
					 'device_id' => $macd_data['device_id'], //device_id from mac_d table
					 'client_loaction' => $macd_data['device_location'], //device_location_id from devices table
					 'ticket_type_id' => 2, //ticket type id by defult it should be incident
					 'ticket_cate_id' => $macd_data['device_category_id'], //getting ticket_cate_id from device table
					 'category_uuid' => $macd_data['device_category_uuid'], //getting category_uuid from device table
					 'ticket_sub_cate_id' => $macd_data['device_sub_category'], //getting ticket_sub_cate_id from device table
					 'sub_category_uuid' => $macd_data['device_sub_category_uuid'], //getting sub_category_uuid from device table
					 'contract_type' => $macd_data['contract_type'], 
					 'patner_id' => $partnerId, 
					 'client_uuid' => $clientUUID, 
				);
				$save_macd = $this->tickets->save_ticket($macd_array);
				
					if($save_macd){
						$update_data = array('ticket_id'=>$save_macd);
						$update_event = $this->tickets->updateMacd($update_data,array("macd_id"=>$macd_data['macd_id']));
						# Get SLA, MTRS, MTTR starts #
							$requested_date = strtotime($macd_data['requested_date']);
							$client_id = $partnerId;
							$request_type = $macd_data['request_type'];
							$url = base_url("webapi/getIncidents/client_id/$client_id/severity_id/4/date/$requested_date/macd_action/$request_type");
							$sla_details = get_sla_details($url);
							$sla = ""; $mtrs = ""; $mttr = "";
							if(isset($sla_details['resultSet']) && !empty($sla_details['resultSet']))
							{
								$sla = $sla_details['resultSet']['slaDueTime'];
								$mtrs = $sla_details['resultSet']['mtrsDueTime'];
								$mttr = $sla_details['resultSet']['mttrDueTime'];
							}						
						# Get SLA, MTRS, MTTR ends #
						$task_id = "TASK-".str_pad($save_macd, 6, '0', STR_PAD_LEFT);
						
						$update_ticket_data = array(
												'incident_id'=> $task_id,
												'sla_due_time'=> $sla,
												'mtrs_due_time'=> $mtrs,
												'mttr_due_time'=> $mttr,
												); 				
						$update_ticket = $this->tickets->updateTicket($update_ticket_data,array("ticket_id"=>$save_macd));
						$update_macd_data = array('macd_status'=> 1,'task_uuid'=>$save_macd,'status_code'=>0); 				
						$update_macd = $this->tickets->updateQueueMacd($update_macd_data,array("request_id"=>$macd_data['macd_id']));
								
						
						$insert_activity_log = array(
														"start_time" => date('Y-m-d H:i:s'),
														"notes" => "System Auto generated the ticket",
														"ticket_id" => $save_macd,
														"user_id" => 1
													);
						$insert_ticket_log = $this->tickets->save_ticket_activity($insert_activity_log);
							
					}
				}
			}
		} 
		
		
		
		if(isset($tickets) && !empty($tickets))
		{
			$subject = "";
			foreach($tickets as $ticket){
				$clientUUID = ""; $partnerId = "";
				$partnerId = $ticket['partner_id'];
				
				$subject = "Host name: ".$ticket['device']." <br/>";
				$subject .= (isset($ticket['service']) && !empty($ticket['service']))?"Alarm Type: ".$ticket['service']:""." <br/>";
				$subject .= (isset($ticket['severity']) && !empty($ticket['severity']))?"Severity: ".$ticket['severity']:"";			
				$check_event = $this->tickets->get_event_ticket($ticket['event_id']);
				//When event is Critical- INC is Sev-1; Major-INC- Sev-2; Minor/Warning is Sev-3;
				if($check_event['severity_id'] == 1){//1->CRITICAL
					$skill_id = 1;// 1->Sev-1
				}elseif($check_event['severity_id'] == 10){//10->MAJOR
					$skill_id = 2;// 2->Sev-2
				}elseif($check_event['severity_id'] == 11 || $check_event['severity_id'] == 4 ){//11->MINOR, 4->WARNING
					$skill_id = 3;// 3->Sev-3
				}else{
					$skill_id = 0;
				}

				if($check_event['ticket_id'] == 0){ // create incident if ticket_id = 0 in event table.
					$ticket_data = array(
					 'subject' => $subject, //host_name-alaram_type-Seviority-State
					 'description' => $subject,
					 'queue_id' => 1,				 
					 'owner_id' =>  $ticket['client_id'], //Client_id
					 //'requestor' => "0", // System Generated
					 'status_code' => $ticket['event_status_id'], //state_code
					 'priority' => $ticket['priority_id'], //bring priority id from eventedge_devices based on device id
					 'created_by' => 1, //created by defult 1 System genrate ticket refering to user table
					 'created_on' => date('Y-m-d H:i:s'), 
					 'assigned_to' => 0, 
					 'due_date' => date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))+$ticket['sla']), // Get due date hours from skill_set table (created on + sla hours) insert date
					 'estimated_time' =>  date("Y-m-d H:i:s", strtotime($ticket['sla']. "sec")), // Get due date hours from skill_set table (created on + sla hours) convert seconds to hours 
					 'skill_id' => $skill_id,//skill_id of ticket table
					 'client_id' => $ticket['client_id'], 
					 'event_id' => $ticket['event_id'], //event_id
					 'device_id' => $ticket['device_id'], //device_id
					 'severity_id' => $ticket['severity_id'], //event severity i.e CRITICAL,MAJOR,MINOR
					 'client_loaction' => $ticket['device_location'], //device_location_id from devices table
					 'service_id' => $ticket['service_id'], //service_id from events table
					 'ticket_type_id' => 1, //ticket type id by defult it should be incident
					 'ticket_cate_id' => $ticket['device_category_id'], //getting ticket_cate_id from device table
					 'category_uuid' => $ticket['device_category_uuid'], //getting category_uuid from device table
					 'ticket_sub_cate_id' => $ticket['device_sub_category'], //getting ticket_sub_cate_id from device table
					 'sub_category_uuid' => $ticket['device_sub_category_uuid'], //getting sub_category_uuid from device table
					 'patner_id' => $partnerId, 
					 'client_uuid' => $clientUUID, 
					);
					
					$save_ticket = $this->tickets->save_ticket($ticket_data);
					
					if($save_ticket){
						
						$update_data = array('ticket_id'=>$save_ticket);
						$update_event = $this->tickets->updateEvents($update_data,array("device_id"=>$ticket['device_id'],"severity_id !=" => 2));
						
						$incident_id = "INC-".str_pad($save_ticket, 6, '0', STR_PAD_LEFT);
						
						/* Get SLA, MTRS, MTTR starts */
					    $requested_date = strtotime(date("Y-m-d H:i:s"));
					    $client_id = $ticket['client_id'];
					    $skill_id = $skill_id;
						$url = base_url("webapi/getSLAtime/client_id/$client_id/severity_id/$skill_id/date/$requested_date");
						$sla_details = get_sla_details($url);
						$sla = ""; $mtrs = ""; $mttr = "";
						if(isset($skill_id,$sla_details['resultSet']) && !empty($sla_details['resultSet']))
						{
							$sla = $sla_details['resultSet']['slaDueTime'];
							$mtrs = $sla_details['resultSet']['mtrsDueTime'];
							$mttr = $sla_details['resultSet']['mttrDueTime'];
						}
						
						
					/* Get SLA, MTRS, MTTR ends */
						$update_ticket_data = array(
											'incident_id'=> $incident_id,
											'sla_due_time'=> $sla,
											'mtrs_due_time'=> $mtrs,
											'mttr_due_time'=> $mttr,
											); 	
						$update_ticket = $this->tickets->updateTicket($update_ticket_data,array("ticket_id"=>$save_ticket));
						
						
						$insert_activity_log = array(
														"start_time" => date('Y-m-d H:i:s'),
														"notes" => "System Auto generated the ticket",
														"ticket_id" => $save_ticket,
														"user_id" => 1
													);
						$insert_ticket_log = $this->tickets->save_ticket_activity($insert_activity_log);
						
					}
					else
					{
						die("Not Inserted");
					}
					if(date('h:i:s') == $end_time)
					{ 
						exit; // after 30 seconds stop the loop. for next cron function call
					} 
					
				}			
			}
		}
		
	}
	
	

	public function manual()
	{
		$event_id = $_GET['eid'];
		$event = $this->tickets->getTicketDetails($event_id);
		//When event is Critical- INC is Sev-1; Major-INC- Sev-2; Minor/Warning is Sev-3;
		if($event['severity_id'] == 1){//1->CRITICAL
			$skill_id = 1;// 1->Sev-1
		}elseif($event['severity_id'] == 10){//10->MAJOR
			$skill_id = 2;// 2->Sev-2
		}elseif($event['severity_id'] == 11 || $event['severity_id'] == 4 ){//11->MINOR, 4->WARNING
			$skill_id = 3;// 3->Sev-3
		}else{
			$skill_id = 0;
		}
		if(isset($event) && !empty($event))
		{
			$clientUUID = ""; $partnerId = "";
			$partnerId = $ticket['partner_id'];
			
			$subject = "";
			$subject = "Host name: ".$event['device']." <br/>";
			$subject .= (isset($event['service']) && !empty($event['service']))?"Alarm Type: ".$event['service']:""." <br/>";
			$subject .= (isset($event['severity']) && !empty($event['severity']))?"Severity: ".$event['severity']:"";
			$ticket_data = array(
				 'subject' => $subject, //host_name-alaram_type-Seviority-State
				 'description' => $subject,
				 'queue_id' => 1,
				 'owner_id' =>  $event['client_id'], //Client_id
				 //'requestor' => "0", // System Generated
				 'status_code' => $event['event_status_id'], //state_code
				 'priority' => $event['priority_id'], //bring priority id from eventedge_devices based on device id
				 'created_by' => $this->userId, 
				 'created_on' => date('Y-m-d H:i:s'), 
				 'assigned_to' => 0, 
				 'due_date' => date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))+$event['sla']), // Get due date hours from skill_set table (created on + sla hours) insert date
				 'estimated_time' =>  date("Y-m-d H:i:s", strtotime($event['sla']. "sec")), // Get due date hours from skill_set table (created on + sla hours) convert seconds to hours 
				 'skill_id' => $skill_id,//skill_id of ticket table
				 'client_id' => $event['client_id'], 
				 'event_id' => $event['event_id'], //event_id
				 'device_id' => $event['device_id'], //device_id
				 'severity_id' => $ticket['severity_id'], //event severity i.e CRITICAL,MAJOR,MINOR
				 'client_loaction' => $event['device_location'], //device_location_id from devices table
				 'service_id' => $event['service_id'], //service_id from events table
				 'ticket_type_id' => 1, //ticket type id by defult it should be incident
				 'ticket_cate_id' => $event['device_category_id'], //getting ticket_cate_id from device table
				 'category_uuid' => $event['device_category_uuid'], //getting category_uuid from device table
				 'ticket_sub_cate_id' => $event['device_sub_category'], //getting ticket_sub_cate_id from device table
				 'sub_category_uuid' => $event['device_sub_category_uuid'], //getting sub_category_uuid from device table
				 'patner_id' => $partnerId, 
				 'client_uuid' => $clientUUID,
				);
				
				$save_ticket = $this->tickets->save_ticket($ticket_data);
				$update_data = array('ticket_id'=>$save_ticket);
				$update_event = $this->tickets->updateEvents($update_data,array("device_id"=>$event['device_id']));
				
				$incident_id = "INC-".str_pad($save_ticket, 6, '0', STR_PAD_LEFT);
					/* Get SLA, MTRS, MTTR starts */
					    $requested_date = strtotime(date("Y-m-d H:i:s"));
					    $client_id = $event['client_id'];
					    $skill_id = $skill_id;
						$url = base_url("webapi/getIncidents/client_id/$client_id/severity_id/$skill_id/date/$requested_date");
						$sla_details = get_sla_details($url);
						$sla = ""; $mtrs = ""; $mttr = "";
						if(isset($skill_id,$sla_details['resultSet']) && !empty($sla_details['resultSet']))
						{
							$sla = $sla_details['resultSet']['slaDueTime'];
							$mtrs = $sla_details['resultSet']['mtrsDueTime'];
							$mttr = $sla_details['resultSet']['mttrDueTime'];
						}						
					/* Get SLA, MTRS, MTTR ends */
						$update_ticket_data = array(
											'incident_id'=> $incident_id,
											'sla_due_time'=> $sla,
											'mtrs_due_time'=> $mtrs,
											'mttr_due_time'=> $mttr,
											); 				
				$update_ticket = $this->tickets->updateTicket($update_ticket_data,array("ticket_id"=>$save_ticket));
				
				$insert_activity_log = array(
												"start_time" => date('Y-m-d H:i:s'),
												"notes" => "Manual ticket",
												"ticket_id" => $save_ticket,
												"user_id" => $this->userId
											);
				$insert_ticket_log = $this->tickets->save_ticket_activity($insert_activity_log);
				if($insert_ticket_log)
				{
					echo 1;
				}else
				{
					echo 0;
				}
				
		}
		else
		{
			echo 0;
		}
		
		
	}

	public function macd_manual($id,$status)
	{
		
		if($status == 1)
		{
			$macd_data = $this->tickets->get_latest_macd_byId($id);
			
			
			if(isset($macd_data) && !empty($macd_data))
			{
				$clientUUID = ""; $partnerId = "";
				/* Get client_uuid and partnerId */
				$getUUIDofClient = $this->inc->getAllClients($macd_data['client_id']);
				if(isset($getUUIDofClient['resultSet']) && (count($getUUIDofClient['resultSet'])>0))
				{
					$clientUUID = $getUUIDofClient['resultSet'][0]['client_uuid'];
					$partnerId = $getUUIDofClient['resultSet'][0]['partner_id'];
					if($partnerId == 1)
					{
						$partnerId = $macd_data['client_id'];
					}
				}
				/* Get client_uuid and partnerId ends */
				
				
				$subject = "";				
					$firstNameFrom = (isset($macd_data['firstname_from']) && !empty($macd_data['firstname_from']))?'FirstName(From): '.$macd_data['firstname_from']:'';
					$LastNameFrom = (isset($macd_data['lastname_from']) && !empty($macd_data['lastname_from']))?'LastName(From): '.$macd_data['lastname_from']:'';
					$phone = (isset($macd_data['phone']) && !empty($macd_data['phone']))?'Phone: '.$macd_data['phone']:'';
					$device = (isset($macd_data['Device']) && !empty($macd_data['Device']))?'Device: '.$macd_data['Device']:'';
					$mac_address = (isset($macd_data['mac_address']) && !empty($macd_data['mac_address']))?'Mac Address: '.$macd_data['mac_address']:'';
					$dialing_capabilities = (isset($macd_data['dialing_capabilities']) && !empty($macd_data['dialing_capabilities']))?'Dialing Capabilities: '.$macd_data['dialing_capabilities']:'';
					$location_to = (isset($macd_data['locationTo']) && !empty($macd_data['locationTo']))?'Location To: '.$macd_data['locationTo']:'';
					$required_by = ($macd_data['required_by'] != '0000-00-00 00:00:00')?'Required by: '.$macd_data['required_by']:'';
					$firstNameTo = (isset($macd_data['firstname_to']) && !empty($macd_data['firstname_to']))?'FirstName(To): '.$macd_data['firstname_to']:'';
					$LastNameTo = (isset($macd_data['lastname_to']) && !empty($macd_data['lastname_to']))?'LastName(To): '.$macd_data['lastname_to']:'';
					$subject .= $firstNameFrom.' <br/>'.$LastNameFrom.' <br/>'.$phone.' <br/>'.$device.' <br/>'.$mac_address.' <br/>'.$dialing_capabilities.' <br/>'.$location_to.' <br/>'.$required_by.' <br/>'.$firstNameTo.' <br/>'.$LastNameTo;
					$macd_array = array(
						'subject' => $subject, //host_name-alaram_type-Seviority-State
						 'description' => $subject,
						 'queue_id' => 1,				 
						 'owner_id' =>  $macd_data['client_id'], //Client_id 
						 'requestor' => $macd_data['requested_by'], 
						 'status_code' => 0, //state_code // which means it is new
						 'priority' => 3, //by defult urgency is Low
						 'created_by' => 1, //created by defult 1 System genrate ticket refering to user table
						 'created_on' => date('Y-m-d H:i:s'), 
						 'assigned_to' => 0, 
						 'due_date' => 0,
						 'estimated_time' =>  0, 
						 'client_id' => $macd_data['client_id'],
						 'skill_id' => 4,//TASK->Sev-4						 
						 'event_id' => 0, //event_id
						 'device_id' => $macd_data['device_id'], //device_id from mac_d table
						 'client_loaction' => $macd_data['device_location'], //device_location_id from devices table
						 'ticket_type_id' => 2, //ticket type id by defult it should be incident
						 'ticket_cate_id' => $macd_data['device_category_id'], //getting ticket_cate_id from device table
						 'category_uuid' => $macd_data['device_category_uuid'], //getting category_uuid from device table
						 'ticket_sub_cate_id' => $macd_data['device_sub_category'], //getting ticket_sub_cate_id from device table
						 'sub_category_uuid' => $macd_data['device_sub_category_uuid'], //getting sub_category_uuid from device table
						 'patner_id' => $partnerId, 
						 'client_uuid' => $clientUUID,
						 'contract_type' => $macd_data['contract_type'],
					);
					$save_macd = $this->tickets->save_ticket($macd_array);
					
					if($save_macd){
						$update_data = array('ticket_id'=>$save_macd);
						$update_event = $this->tickets->updateMacd($update_data,array("macd_id"=>$macd_data['macd_id']));
						
						# Get SLA, MTRS, MTTR starts #
						$requested_date = strtotime($macd_data['requested_date']);
					    $client_id = $partnerId;
						$request_type = $macd_data['request_type'];
						$url = base_url("webapi/getIncidents/client_id/$client_id/severity_id/4/date/$requested_date/macd_action/$request_type");
						$sla_details = get_sla_details($url);
						$sla = ""; $mtrs = ""; $mttr = "";
						if(isset($sla_details['resultSet']) && !empty($sla_details['resultSet']))
						{
							$sla = $sla_details['resultSet']['slaDueTime'];
							$mtrs = $sla_details['resultSet']['mtrsDueTime'];
							$mttr = $sla_details['resultSet']['mttrDueTime'];
						}
						# Get SLA, MTRS, MTTR ends #						
						$task_id = "TASK-".str_pad($save_macd, 6, '0', STR_PAD_LEFT);						
						$update_ticket_data = array(
											'incident_id'=> $task_id,
											'sla_due_time'=> $sla,
											'mtrs_due_time'=> $mtrs,
											'mttr_due_time'=> $mttr,
											); 		
						$update_ticket = $this->tickets->updateTicket($update_ticket_data,array("ticket_id"=>$save_macd));
						$update_macd_data = array('macd_status'=> 1,'task_uuid'=>$save_macd,'status_code'=>0); 				
						$update_macd = $this->tickets->updateQueueMacd($update_macd_data,array("request_id"=>$macd_data['macd_id']));
						
						
						
						
						$insert_activity_log = array(
														"start_time" => date('Y-m-d H:i:s'),
														"notes" => "System Auto generated the ticket",
														"ticket_id" => $save_macd,
														"user_id" => 1
													);
						$insert_ticket_log = $this->tickets->save_ticket_activity($insert_activity_log);
							
					}
				
			}
		}
		
	}

	
}
