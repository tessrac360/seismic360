<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Partner extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->userId = $this->session->userdata('id');
        $this->client_id = $this->session->userdata('client_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model("Model_users");
        $this->load->model("Model_partner");
        $this->load->model("client/Model_client");
        $this->load->model("location/Model_location");
        $this->getCompanyId = helpler_getCompanyId();
        $this->load->library('form_validation');
         $this->getCompanyId = helpler_getCompanyId();
    }

    public function ajax_checkUnique() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $id = $this->input->post('id');
        $old_email = $this->input->post('old_email');
        if ($id != "") {
            $this->db->where('id !=', $id);
        }
        if ($old_email != "") {
            $this->db->where_not_in($field, $old_email);
        }

        $recordCount = $this->db->get_where($table, array($field => $value))->num_rows();
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

    function checkuseremail($email) {
        if ($this->input->post('id'))
            $id = $this->input->post('id');
        else
            $id = '';
        $result = $this->Model_users->check_unique_user_email($id, $email);
        if ($result == 0)
            $response = true;
        else {
            $this->form_validation->set_message('check_user_email', 'Email must be unique');
            $response = false;
        }
        return $response;
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        $data['getUser'] = $this->Model_users->getUsersEdit($id);
        $clientId = $data['getUser']['resultSet']->client;

        /* Start Old Log */
        $getoldData = helper_LogFetchRecordUser('users', 'id', $id);
        $oldValue = $getoldData['log'];

        /* Start Old Log */
        if ($id != "") {
            $Status = $this->Model_users->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                /* Start new Log */
                $getNewData = helper_LogFetchRecordUser('users', 'id', $id);
                $newValue = $getNewData['log'];
                $actions = 'STATUS';
                helper_createLogActions('user_log_manage', $clientId, $actions, $oldValue, $newValue);
                /* Start new Log */
                if ($status == 'true') {
                    $return['message'] = 'Partner Account is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Partner Account is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    /* Start User module */

    public function ajax_userdata() {
        $accessEdit = helper_fetchPermission('15', 'edit');
        $accessStatus = helper_fetchPermission('15', 'active');
        $accessDelete = helper_fetchPermission('15', 'delete');
        $list = $this->Model_partner->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $partner_id = $_POST['partner_id']; 
        foreach ($list as $users) {
            $no++;
            $active_status = ($users->active_status == "Y") ? 'checked' : "";
            $row = array();
            $row[] = $users->first_name;
            $row[] = $users->email_id;
            $row[] = $users->phone;
            $row[] = $users->client_title;
            $row[] = $users->title;
            $row[] = $users->username;
            $row[] = ($accessStatus == 'Y') ? '<label class="mt-checkbox mt-checkbox-outline"><input type="checkbox"  class="onoffactiontoggle" myval="' . encode_url($users->id) . '" ' . $active_status . '><span></span></label>' : '';
            $row[] = ($accessEdit == 'Y') ? '<a href="' . base_url() . 'users/partner/edit/' . encode_url($users->id) . '?patnerId='.$partner_id.'" class="btn btn-xs  blue"><i class="fa fa-edit"></i></a>' : "";            
            $row[] = ($accessDelete == 'Y') ? '<a href="javascript:void(0);"  userId="' . encode_url($users->id) . '" class="btn btn-xs red isdeleteUser"><i class="fa fa-trash"></i></a>' : '';
            $data[] = $row;
        }

        $output = array(  
            "draw" => $_POST['draw'],
            "recordsTotal" => 0,
            "recordsFiltered" => $this->Model_partner->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function index() {
        $roleAccess = helper_fetchPermission('15', 'view');
        if ($roleAccess == 'Y') {
            $this->getClient();
        } else {
            redirect('unauthorized');
        }
    }

    public function getClient() {
        //echo $this->client_id;
        //echo  $this->getCompanyId;exit;
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'roles/getclient/';
        $config['total_rows'] = $this->Model_partner->getClientPag($search, $this->getCompanyId);
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data['client'] = $this->Model_partner->getClientPag($search, $this->getCompanyId, $config['per_page'], $page);
        if ($search != '') {
            $tmp = array();
            $cnt = count($data['client']);
            for ($i = 0; $i < $cnt; $i++) {
                if (preg_match("/" . $search . "/i", $data['client'][$i]['client_title'])) {
                    $tmp[] = $data['client'][$i];
                }
            }
            $data['client'] = array();
            $data['client'] = $tmp;
        }

        $data['client_id'] = $this->client_id;
        $data['hiddenURL'] = 'roles/getclient/';
        $data['file'] = 'partner/view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }

    public function partner_list() {
		$partner_id = decode_url($_GET['cli']);
		//pr($partner_id);exit;
		$data['clientName'] = $this->Model_partner->getClientName($partner_id);
		//pr($data['clientName']);exit;
        $roleAccess = helper_fetchPermission('15', 'view');
        if ($roleAccess == 'Y') {
            //echo decode_url($_GET['cli']);exit;
            $data['file'] = 'partner/partner_list';
            $this->load->view('template/front_template', $data);
        } else {
            redirect('unauthorized');
        }
    }

    public function create($partnerId="") {
        //pr($_SESSION);exit;
        $roleAccess = helper_fetchPermission('15', 'add');
        if ($roleAccess == 'Y') {
             $pat_id= decode_url($partnerId);//exit;
            if ($this->input->post()) {
                $userPost = $this->input->post();
                $this->form_validation->set_rules('first_name', 'Name', 'required');
                //$this->form_validation->set_rules('phone', 'Mobile No', 'required|max_length[10]|numeric');
                $this->form_validation->set_rules('email_id', 'Email', 'required|valid_email|is_unique[users.email_id]');
                $this->form_validation->set_rules('role_id', 'Role & Permission', 'required');
                $this->form_validation->set_rules('technology[]', 'Assign Group', 'required');
               // $this->form_validation->set_rules('is_mac_access', 'Access', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $userPost['skill'] = implode(",", $userPost['skill']);
                    $userPost = array_merge($userPost, $this->cData);
                    $userPost['password'] = randomPassword();
                    $userPost['multi_signon_allow'] = (isset($userPost['multi_signon_allow'])) ? '0' : '1';
                    $userPost['is_mac_access'] = $userPost['is_mac_access'];
                    $userPost['is_approval'] = (isset($userPost['is_approval'])) ? 'Y' : 'N';
                    //pr($userPost);exit;

                    $resultData = $this->Model_partner->insertPartner($userPost);
                    if ($resultData['status'] == 'true') {
                        $lastInsertId = $resultData['lastId'];
                        //$this->manageSubClients($lastInsertId, $subClients);
                        /* Start
                          Name: veeru
                          Date: 24/11/17
                          Functionality: Inserting Assigned Group Id's into eventedge_users_assigned_group table
                         */
                        $technologyPost = array();
                        foreach ($userPost['technology'] as $value) {
                            $technologyPost['user_id'] = $lastInsertId;
                            $technologyPost['tech_id'] = $value;
                            $technologyPost['partner_id'] = $this->getCompanyId;
                            $this->Model_users->insertTechnology($technologyPost);
                        }
                        /* End */

                        /* Start new Log */
                        $getNewData = helper_LogFetchRecordUser('users', 'id', $lastInsertId);
                        $newValue = $getNewData['log'];
                        $actions = 'CREATE';
                        helper_createLogActions('user_log_manage', $userPost['client'], $actions, $oldValue, $newValue);
                        /* Start new Log */
                        $this->session->set_flashdata("success_msg", "Partner is created successfully ..!!");
                        redirect('users/partner/partner_list/?cli='.encode_url($pat_id));
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('users/partner');
                    }
                } else {
					$data['clientName'] = $this->Model_partner->getClientName($pat_id);
                    $data['user_type'] = $this->user_type;
                    $data['client'] = $this->Model_partner->getPartner();
                    $data['roles'] = $this->Model_users->getRoles($this->getCompanyId);
                    $data['skillset'] = $this->Model_users->getSkillset();
                    $data['technology'] = $this->Model_users->getTechnology();
                    $data['usersList'] = $this->Model_users->getUsersListByCompanyId();
					$data['pat_id'] = $pat_id;
                    $data['file'] = 'partner/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				$data['clientName'] = $this->Model_partner->getClientName($pat_id);
                $data['user_type'] = $this->user_type;
                $data['client'] = $this->Model_partner->getPartner($pat_id);
                $data['roles'] = $this->Model_partner->getRoles($pat_id);
                $data['groups'] = $this->Model_users->getDomainUser();
                $data['technology'] = $this->Model_users->getTechnology();
                $data['usersList'] = $this->Model_users->getUsersListByCompanyId();
                
                $data['skillset'] = $this->Model_users->getSkillsetUser();
				$data['pat_id'] = $pat_id;
                $data['file'] = 'partner/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function ajax_getRoles() {
        $clientId = $this->input->post('clientId');
        if ($clientId != "") {
            $status = $this->Model_users->getRoles($clientId);
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

    private function manageSubClients($userId = "", $subClient = array()) {
        //echo "<pre>";pr($subClient);exit;
        $manageSubClients = array();
        foreach ($subClient as $values) {

            $value = explode("-", $values);
            $manageSubClients['user_id'] = $userId;
            $manageSubClients['client_id'] = $value[0];
            $manageSubClients['parent_client_id'] = $value[1];
            $res = $this->Model_users->insertUsersSubclientManage($manageSubClients);
        }
    }

    public function edit($postId = NULL) {
        $roleAccess = helper_fetchPermission('15', 'edit');
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId); //exit;
            $patnerId = decode_url($_GET['patnerId']);
            $getStatus = $this->Model_users->isExitUser($postId);
            //pr($getStatus);exit;
            if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
                    $postUsers = $this->input->post();
                    //pr($postUsers);exit;
                    //$subClients = $postUsers['subClients'];
                    //echo "<pre>";pr($subClients);exit;
                    //pr($userPost);exit;
                    //unset($postUsers['subClients']);
                    $this->form_validation->set_rules('first_name', 'Name', 'required');
                    $this->form_validation->set_rules('role_id', 'Role & Permission', 'required');
					$this->form_validation->set_rules('technology[]', 'Assign Group', 'required');
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
                        /* Start Old Log */
                        $getoldData = helper_LogFetchRecordUser('users', 'id', $postId);
                        $oldValue = $getoldData['log'];
                        /* Start Old Log */
                        unset($postUsers['id']);
                        $postUsers['skill'] = implode(",", $postUsers['skill']);
                        $postUsers = array_merge($postUsers, $this->uData);
                        $updateStatus = $this->Model_users->updateUsers($postUsers, $postId);
                        if ($updateStatus['status'] == 'true') {
//                            $this->Model_users->deleteUsersSubclientManage($postId);
//                            $this->manageSubClients($postId, $subClients);
                            $technologyPost = array();
                            $this->Model_users->deleteTechnology($postId);
                            foreach ($postUsers['technology'] as $value) {                             
                                $technologyPost['user_id'] = $postId;
                                $technologyPost['tech_id'] = $value;
                                $technologyPost['partner_id'] = $this->getCompanyId;
                                $this->Model_users->updateuserTechnology($technologyPost);
                            }


                            /* Start
                              Name: veeru
                              Date: 24/11/17
                              Functionality: Inserting Assigned Group Id's into eventedge_users_assigned_group table
                             */
                            /* $userAssignGroupPost['user_id'] = $postId;
                            $userAssignGroupPost['assign_group_id'] = implode(",", $postUsers['assign_group_id']);
                            $this->Model_users->UpdateUserAssignGroups($userAssignGroupPost, $postId); */
                            /* End */

                            /* Start new Log */
                            $getNewData = helper_LogFetchRecordUser('users', 'id', $postId);
                            $newValue = $getNewData['log'];
                            $actions = 'EDIT';
                            helper_createLogActions('user_log_manage', $postUsers['client'], $actions, $oldValue, $newValue);
                            /* Start new Log */
                            $this->session->set_flashdata("success_msg", "Partner is Updated successfully ..!!");
                            redirect('users/partner/partner_list/?cli='.encode_url($patnerId));
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('users/partner/edit/' . encode_url($postId).'?patnerId='.encode_url($patnerId));
                        }
                    } else {
                        $data['user_type'] = $this->user_type;
                        $data['client'] = $this->Model_users->getParentClients();
                        $data['groups'] = $this->Model_users->getDomainUser();
                        // pr($data);exit;
                        /* Start
                          Name: veeru
                          Date: 24/11/17
                          Functionality: Get details group names,users list from eventedge_assign_group
                          and eventedge_users table
                         */
                        $data['technology'] = $this->Model_users->getTechnology();
                        $data['usersList'] = $this->Model_users->getUsersListByCompanyId();
                        /* End */
                        $data['roles'] = $this->Model_users->getRoles($this->getCompanyId);
                        $data['skillset'] = $this->Model_users->getSkillsetUser();
						$data['gettechnology'] = $this->Model_users->getTechnologyById($postId);

                        $data['user_type'] = $this->user_type;
                        $data['getUser'] = $this->Model_users->getUsersEdit($postId);
                        //echo $data['getUser']['resultSet']->client;exit;
                        $data['getDomain'] = $this->Model_users->getGroupsofcompany($postId);
						$data['clientName'] = $this->Model_partner->getClientName($patnerId);
                        /* Start
                          Name: veeru
                          Date: 24/11/17
                          Functionality: Get details group names,users list from eventedge_assign_group
                          and eventedge_users table
                         */
                        $data['technology'] = $this->Model_users->getTechnology();
                        $data['usersList'] = $this->Model_users->getUsersListByCompanyId();
                        $data['assignId'] = $this->Model_users->getAssignId($postId);
                        /* End */

                        //$data['client'] = $this->Model_users->getClient();
                        //$data['client'] = $this->treeClientDropdown();
                        $data['groups'] = $this->Model_users->getDomainUser();
                        $data['skillset'] = $this->Model_users->getSkillsetUser();
                        $data['usersSubclientManage'] = $this->Model_users->getUsersSubclientManage($postId);
						$data['patnerId'] = $patnerId;
                        if (isset($data['usersSubclientManage']['resultSet'])) {
                            foreach ($data['usersSubclientManage']['resultSet'] as $rowVal) {

                                $data['accessLimit'][$rowVal['client_id'] . "-" . $rowVal['parent_client_id']] = 1;
                            }
                        }
                        //$data['sub_client'] = $this->treeClient($data['getUser']['resultSet']->client);
                        //echo "<pre>";pr($data['accessLimit']);exit;
                        $data['file'] = 'partner/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
                    $data['user_type'] = $this->user_type;
                    $data['getUser'] = $this->Model_users->getUsersEdit($postId);
                    $data['client'] = $this->Model_users->getParentClients();
                    $data['groups'] = $this->Model_users->getDomainUser();
                    $data['technology'] = $this->Model_users->getTechnology();
                    $data['usersList'] = $this->Model_users->getUsersListByCompanyId();
                    $data['roles'] = $this->Model_users->getRoles($this->getCompanyId);
                    $data['skillset'] = $this->Model_users->getSkillsetUser();
					$data['gettechnology'] = $this->Model_users->getTechnologyById($postId);
                    $data['client'] = $this->Model_partner->getPartner($patnerId);
					$data['clientName'] = $this->Model_partner->getClientName($patnerId);
                    $data['roles'] = $this->Model_partner->getRoles($patnerId);
					$data['patnerId'] = $patnerId;
                    $data['file'] = 'partner/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('/users');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function my_profile() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('first_name', 'Name', 'required');
            $this->form_validation->set_rules('phone', 'Mobile No', 'required|max_length[10]|numeric');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
            if ($this->form_validation->run() == TRUE) {
                $postUsers = $this->input->post();
                pr($_FILES);

                if (isset($_FILES['user_avatar']['name']) && $_FILES['user_avatar']['name'] != '') {
                    $NewName = time() . rand() . '__' . $_FILES['user_avatar']['name'];
                    $upload_path = 'public/images/profile_image/';
                    $tempName = $_FILES['user_avatar']['tmp_name'];
                    $newpath = move_uploaded_file($tempName, $upload_path . $NewName);
                    $postUsers['user_avatar'] = base_url() . 'public/images/profile_image/' . $NewName;
                }

//                pr($postUsers);
//                exit;
                $resultData = $this->Model_users->updateProfile($postUsers);
                if ($resultData['status'] == 'true') {
                    $this->session->set_flashdata("success_msg", "Your Profile is updated successfully ..!!");
                    redirect('users/my_profile');
                } else {
                    $this->session->set_flashdata("error_msg", "Some thing went wrong");
                    redirect('users/my_profile');
                }
            } else {
                $data['getDomain'] = $this->Model_users->getGroupsofcompany($this->userId);
                $data['user'] = $this->Model_users->getUserDetails();
                $data['groups'] = $this->Model_users->getDomainUser();
                $data['skillset'] = $this->Model_users->getSkillsetUser();
                $data['pageTitle'] = 'Your page title';
                $data['file'] = 'change_profile';
                $this->load->view('template/front_template', $data);
            }
        } else {
            $data['getDomain'] = $this->Model_users->getGroupsofcompany($this->userId);
            $data['user'] = $this->Model_users->getUserDetails();
            $data['groups'] = $this->Model_users->getDomainUser();
            $data['skillset'] = $this->Model_users->getSkillsetUser();
            $data['pageTitle'] = 'Your page title';
            $data['file'] = 'change_profile';
            $this->load->view('template/front_template', $data);
        }
    }

    public function change_password() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
            if ($this->form_validation->run() == TRUE) {
                $postUsers = $this->input->post();

                $isCheckOldPassword = $this->Model_users->oldPasswordCheck($postUsers);
                if ($isCheckOldPassword['status'] == 'true') {
                    unset($postUsers['password_confirm']);
                    unset($postUsers['old_password']);
                    $resultData = $this->Model_users->changePassword($postUsers);
                    if ($resultData['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "Your Password is changed successfully ..!!");
                        redirect('users/change_password');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('users/change_password');
                    }
                } else {
                    $this->session->set_flashdata("error_msg", "Old Password does not match");
                    redirect('users/change_password');
                }
            } else {
                $data['file'] = 'change_password';
                $this->load->view('template/front_template', $data);
            }
        } else {
            $data['file'] = 'change_password';
            $this->load->view('template/front_template', $data);
        }
    }

    public function delete($postId = "") {
        $postId = decode_url($postId);
        $data['getUser'] = $this->Model_users->getUsersEdit($postId);
        $clientId = $data['getUser']['resultSet']->client;

        /* Start Old Log */
        $getoldData = helper_LogFetchRecordUser('users', 'id', $postId);
        $oldValue = $getoldData['log'];
        /* Start Old Log */
        if ($postId != "") {
            $deleteRole = $this->Model_users->isDeleteUser($postId);
            if ($deleteRole['status'] == 'true') {
                /* Insert Log */
                $actions = 'DELETE';
                helper_createLogActions('user_log_manage', $clientId, $actions, $oldValue, $newValue);
                /* Insert Log */
                $this->session->set_flashdata("success_msg", "User is deleted successfully..!!");
                redirect('users');
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('users');
            }
        } else {
            redirect('users');
        }
    }

    public function siddhu() {
        $this->Model_users->siddhus();
    }

    /* End User module */
}
