<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_temp extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model('Module_email_temp');
        $this->load->model('users/Model_users');
        $this->load->library('form_validation');
    }

    function slog_create($str) {
        return strtoupper(str_replace(" ", "_", trim($str)));
    }

    public function create($clientId="",$tempId = "") {
       
            if ($this->input->post()) {               
                $this->form_validation->set_rules('slug', 'Slug', 'required|is_unique[setting.slug]');
                $this->form_validation->set_rules('subject', 'Subject', 'required');
                $this->form_validation->set_rules('content', 'Content', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $settingPost = $this->input->post();
                    unset($settingPost['files']);
                    $settingPost = array_merge($settingPost, $this->cData);
                    $settingPost['slug'] = 'EMAIL_' . $this->slog_create($settingPost['slug']);                    
                    //pr($settingPost);exit;
                    $resultData = $this->Module_email_temp->insertEmail($settingPost);
                    if ($resultData['status'] == 'true') {
                        $lastInsertId = $resultData['lastId'];
                        $this->session->set_flashdata("success_msg", "Email Template is created successfully ..!!");
                        redirect('settings');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('settings/create');
                    }
                } else {
                    $data['clientMacro'] = $this->Module_email_temp->getMacroInfo();
                    $data['client'] = $this->Model_users->getClient();
                    $data['pageTitle'] = 'Your p44age title';
                    $data['file'] = 'view_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['clientMacro'] = $this->Module_email_temp->getMacroInfo();
                $data['pageTitle'] = 'Email Template';
                $data['file'] = 'emailtemp/create_form';
                $this->load->view('template/front_template', $data);
            }
       
    }

    private function masterEmailTemplateCreate($ClientId=""){
        $this->Module_email_temp->isCheckEmailTempExists($ClientId);
    }

    public function view($clientId = "") {
        //$this->masterEmailTemplateCreate($clientId);
        $data['file'] = 'emailtemp/view_form';
        $this->load->view('template/front_template', $data);        
    }

    public function searchdata() {
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'settings/searchData/';
        $config['total_rows'] = count($this->Module_email_temp->getSettingSearchData($search));
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        //$config['first_url'] = base_url() . "client/searchdata/0?sd=" . $search;
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['settings'] = $this->Module_email_temp->getSettingSearchData($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'settings/searchdata';
        $data['file'] = 'view_form';
        //pr($data);exit;
        $this->load->view('template/front_template', $data);
    }

    public function edit($clientId="",$tempId = "") {                 
            if ($this->input->post()) {                          
                $this->form_validation->set_rules('subject', 'Subject', 'required');
                $this->form_validation->set_rules('content', 'Content', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $settingPost = $this->input->post();
                    unset($settingPost['files']);                                      
                    //pr($settingPost);exit;
                    if($settingPost['isedit']=='N'){
                        
                        $settingPost = array_merge($settingPost, $this->cData);
                        //pr(settingPost) ;exit;             
                        $resultData = $this->Module_email_temp->updateClientWiseEmail($settingPost, $tempId);
                    }else{
                        $settingPost = array_merge($settingPost, $this->uData);
                        $resultData = $this->Module_email_temp->updateEmail($settingPost, $tempId);
                    }
exit;
                    
                    if ($resultData['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "Email Template is Updated successfully ..!!");
                        redirect('settings');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('settings/edit');
                    }
                } else {
                    $data['clientMacro'] = $this->Module_email_temp->getMacroInfo();                   
                    $data['getEmailSettings'] = $this->Module_email_temp->getEmailSettingsEdit($tempId);
                    $data['pageTitle'] = 'Edit Email Template';
                    $data['file'] = 'emailtemp/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['clientMacro'] = $this->Module_email_temp->getMacroInfo();               
                $data['getEmailSettings'] = $this->Module_email_temp->getEmailSettingsEdit($tempId);
                $data['pageTitle'] = 'Edit Email Template';
                $data['file'] = 'emailtemp/update_form';
                $this->load->view('template/front_template', $data);
            }
       
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Module_email_temp->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Email is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Email is Deactivated successfully';
                }
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function ajax_checkUnique() {
        $value = $this->input->post('value');
        $field = 'slug';
        $recordCount = $this->db->get_where('setting', array($field => $value))->num_rows();
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

    public function smsandothers() {
        $roleAccess = helper_fetchPermission('54', 'view');
        if ($roleAccess == 'Y') {
            $this->load->library('pagination');
            $config['base_url'] = base_url() . 'settings/index/';
            $config['total_rows'] = count($this->Module_email_temp->getSettingSMSPagination());
            $config['per_page'] = PAGINATION_PERPAGE;
            $config['uri_segment'] = 3;
            $config['display_pages'] = TRUE;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['settings'] = $this->Module_email_temp->getSettingSMSPagination($config['per_page'], $page);
            $data['hiddenURL'] = 'settings/searchdatasetting';
            $data['search'] = '';
            $data['file'] = 'smsandothers';
            //pr($data);exit;
            $this->load->view('template/front_template', $data);
        } else {
            redirect('unauthorized');
        }
    }

    public function searchdatasetting() {
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'settings/searchdatasetting/';
        $config['total_rows'] = count($this->Module_email_temp->getSettingSearchDataSMS($search));
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        //$config['first_url'] = base_url() . "client/searchdata/0?sd=" . $search;
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['settings'] = $this->Module_email_temp->getSettingSearchDataSMS($search, $config['per_page'], $page);
        $data['search'] = $search;
        $data['hiddenURL'] = 'settings/searchdatasetting';
        $data['file'] = 'smsandothers';
        //pr($data);exit;
        $this->load->view('template/front_template', $data);
    }

 

   

   

}
