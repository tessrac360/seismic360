<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EmailAccountSettings extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_email_account_settings","email_settings");
        $this->load->library('form_validation');
    }

	public function view($reference_uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            $getStatus = $this->email_settings->isExistsEmailAccountSetting($reference_uuid);
			if ($getStatus['status'] == 'true') {
				$data['clientName'] = $this->email_settings->getClientName($reference_uuid);
				if($data['clientName']['status'] == 'true'){
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
				}		
                if ($this->input->post()) {
					$this->form_validation->set_rules('outgoing_host', 'Outgoing Host', 'required');
					$this->form_validation->set_rules('outgoing_port', 'Outgoing Port', 'required');
					$this->form_validation->set_rules('incoming_host', 'Incoming Host', 'required');
					$this->form_validation->set_rules('incoming_port', 'Incoming Port', 'required');
					$this->form_validation->set_rules('username', 'Username', 'required');
					$this->form_validation->set_rules('password', 'Password', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
					if ($this->form_validation->run() == TRUE) {
						$EmailSettingsPost = $this->input->post();
						$EmailSettingsPost = array_merge($EmailSettingsPost, $this->uData);
						$updateStatus = $this->email_settings->updateEmailAccountSettings($EmailSettingsPost,$reference_uuid);
						if ($updateStatus['status'] == 'true') {
                           $this->session->set_flashdata("success_msg", "Email Account Setting is updated successfully ..!!");
                            redirect('client/EmailAccountSettings/view/'.$reference_uuid);
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/EmailAccountSettings/view/'.$reference_uuid);
						}
                    } else {
						$data['getEmailSettingsEdit'] = $this->email_settings->getEmailSettingsEdit($reference_uuid);
                        $data['reference_uuid'] = $reference_uuid;
                        $data['file'] = 'client_email_account_settings/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['getEmailSettingsEdit'] = $this->email_settings->getEmailSettingsEdit($reference_uuid);
                    $data['reference_uuid'] = $reference_uuid;
                    $data['file'] = 'client_email_account_settings/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                //redirect('client/EmailAccountSettings');
				$data['clientName'] = $this->email_settings->getClientName($reference_uuid);
				if($data['clientName']['status'] == 'true'){
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
				}		
				if ($this->input->post()){
					$this->form_validation->set_rules('outgoing_host', 'Outgoing Host', 'required');
					$this->form_validation->set_rules('outgoing_port', 'Outgoing Port', 'required');
					$this->form_validation->set_rules('incoming_host', 'Incoming Host', 'required');
					$this->form_validation->set_rules('incoming_port', 'Incoming Port', 'required');
					$this->form_validation->set_rules('username', 'Username', 'required');
					$this->form_validation->set_rules('password', 'Password', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
					if ($this->form_validation->run() == TRUE) {
						$EmailSettingsPost = $this->input->post();
						//pr($EmailSettingsPost);exit;
						$EmailSettingsPost['uuid'] = generate_uuid('emas_');
						$EmailSettingsPost['reference_uuid'] = $reference_uuid;
						$EmailSettingsPost = array_merge($EmailSettingsPost, $this->cData);
						$result = $this->email_settings->insertEmailAccountSettings($EmailSettingsPost);
						if ($result['status'] == 'true') {
							$this->session->set_flashdata("success_msg", "Email Account Setting is created successfully ..!!");
							redirect('client/EmailAccountSettings/view/' . $reference_uuid);
						} else {
							$this->session->set_flashdata("error_msg", "Some thing went wrong while inserting email account setting data");
							redirect('client/EmailAccountSettings/view/' . $reference_uuid);
						}
					} else {
						$data['getEmailSettingsEdit'] = $this->email_settings->getEmailSettingsEdit($reference_uuid);
						$data['reference_uuid'] = $reference_uuid;
						$data['file'] = 'client_email_account_settings/update_form';
						$this->load->view('template/front_template', $data);
					}
				} else {
					$data['getEmailSettingsEdit'] = $this->email_settings->getEmailSettingsEdit($reference_uuid);
					$data['reference_uuid'] = $reference_uuid;
					$data['file'] = 'client_email_account_settings/update_form';
					$this->load->view('template/front_template', $data);
				}
			}				
        } else {
            redirect('unauthorized');
        }
    }
	
	public function ajax_checkUniqueUsername() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
		//$reference_uuid = trim($this->input->post('reference_uuid'));
        $value = trim($this->input->post('value'));
		$deleted_field = 'is_deleted';
		//$client_id= 'reference_uuid';
		$array = array($field => $value, $deleted_field => '0');
        $recordCount = $this->db->get_where($table, $array)->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

}

?>