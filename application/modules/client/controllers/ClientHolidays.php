<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ClientHolidays extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_holidays");
        $this->load->library('form_validation');
    }

    public function view($reference_uuid = NULL) {
        //$reference_split = explode("_", $reference_uuid);
        //$reference_prefix = $reference_split[0];
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'client/ClientHolidays/view/' . $reference_uuid;
        $config['total_rows'] = $this->Model_holidays->getClientHolidays($reference_uuid, $search);
        $config['uri_segment'] = 5;
        $config['display_pages'] = TRUE;
        $config['enable_query_strings'] = true;
        if ($_GET) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
        }
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data['getClientHolidays'] = $this->Model_holidays->getClientHolidays($reference_uuid, $search, $config['per_page'], $page);
        $data['hiddenURL'] = 'client/ClientHolidays/view/' . $reference_uuid;
        //$data['reference_prefix'] = $reference_prefix;
        //pr($data['clientLoctions']);exit;
		$data['clientName'] = $this->Model_holidays->getClientName($reference_uuid);
		if($data['clientName']['status'] == 'true'){
			$data['client_title'] = $data['clientName']['resultSet']['client_title'];
			$data['client_type'] = $data['clientName']['resultSet']['client_type'];
		}
        $data['reference_uuid'] = $reference_uuid;
        $data['search'] = $search;
		$data['file'] = 'client_holidays/view_form';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $client_holidays_uuid = $_POST['client_holidays_uuid'];
        if ($client_holidays_uuid != "") {
            $Status = $this->Model_holidays->updateHolidaysStatus($status, $client_holidays_uuid);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Holiday is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Holiday is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function create($reference_uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'add');
        if ($roleAccess == 'Y') {
			
            if ($this->input->post()){
				$this->form_validation->set_rules('holiday_date', 'Holiday Date', 'required');
				$this->form_validation->set_rules('holiday_occasion', 'Holiday Occasion', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $HolidayPost = $this->input->post();
					//pr($HolidayPost);exit;
					$HolidayPost['client_holidays_uuid'] = generate_uuid('clho_');
					$HolidayPost['client_uuid'] = $reference_uuid;
					$dob = isset($HolidayPost['holiday_date'])?$HolidayPost['holiday_date']:'';
					$HolidayPost['holiday_year'] = date("Y", strtotime($dob));
					$HolidayPost['holiday_date'] = date("Y-m-d", strtotime($dob));
					$HolidayPost['holiday_occasion'] = $HolidayPost['holiday_occasion'];
					$HolidayPost = array_merge($HolidayPost, $this->cData);
					$result = $this->Model_holidays->insertHolidays($HolidayPost);
                    if ($result['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "Holiday is created successfully ..!!");
                        redirect('client/ClientHolidays/view/' . $reference_uuid);
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong while inserting phone data");
                        redirect('client/ClientHolidays/create/' . $reference_uuid);
                    }
                } else {
					//$data['getEscalationIds'] = $this->Model_escalations->getEscalationGroupsIds($reference_uuid);
                    $data['clientName'] = $this->Model_holidays->getClientName($reference_uuid);
					if($data['clientName']['status'] == 'true'){
						$data['client_title'] = $data['clientName']['resultSet']['client_title'];
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					}
					
                    //$data['getEscalaltionGroups'] = $this->Model_escalations->getEscalaltionGroups();
                    //$data['getContactGroups'] = $this->Model_escalations->getContactGroups($reference_uuid);
                    $data['reference_uuid'] = $reference_uuid;
                    $data['file'] = 'client_holidays/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				//$data['getEscalationIds'] = $this->Model_escalations->getEscalationGroupsIds($reference_uuid);
                $data['clientName'] = $this->Model_holidays->getClientName($reference_uuid);
				if($data['clientName']['status'] == 'true'){
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
				}
				//$data['getEscalaltionGroups'] = $this->Model_escalations->getEscalaltionGroups();
               // $data['getContactGroups'] = $this->Model_escalations->getContactGroups($reference_uuid);
                $data['reference_uuid'] = $reference_uuid;
                $data['file'] = 'client_holidays/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($reference_uuid = NULL, $client_holidays_uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            $getStatus = $this->Model_holidays->isExistsClientHolidays($client_holidays_uuid);
			if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
					$this->form_validation->set_rules('holiday_date', 'Holiday Date', 'required');
					$this->form_validation->set_rules('holiday_occasion', 'Holiday Occasion', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
					if ($this->form_validation->run() == TRUE) {
						$HolidayPost = $this->input->post();
						//pr($HolidayPost);exit;
						$dob = isset($HolidayPost['holiday_date'])?$HolidayPost['holiday_date']:'';
						$HolidayPost['holiday_year'] = date("Y", strtotime($dob));
						$HolidayPost['holiday_date'] = date("Y-m-d", strtotime($dob));
						$HolidayPost['holiday_occasion'] = $HolidayPost['holiday_occasion'];
						$HolidayPost = array_merge($HolidayPost, $this->uData);
						$updateStatus = $this->Model_holidays->updateHolidays($HolidayPost,$client_holidays_uuid);
                        if ($updateStatus['status'] == 'true') {
                           $this->session->set_flashdata("success_msg", "Holiday is Updated successfully ..!!");
                            redirect('client/ClientHolidays/view/'.$reference_uuid);
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/ClientHolidays/edit/'.$reference_uuid.'/'.$client_holidays_uuid);
						}
                    } else {
						$data['getHolidays'] = $this->Model_holidays->getHolidaysEdit($client_holidays_uuid);
						$data['clientName'] = $this->Model_holidays->getClientName($reference_uuid);
						if($data['clientName']['status'] == 'true'){
							$data['client_title'] = $data['clientName']['resultSet']['client_title'];
							$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						}
                        $data['reference_uuid'] = $reference_uuid;
                        $data['client_holidays_uuid'] = $client_holidays_uuid;
                        $data['file'] = 'client_holidays/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['getHolidays'] = $this->Model_holidays->getHolidaysEdit($client_holidays_uuid);
                    $data['clientName'] = $this->Model_holidays->getClientName($reference_uuid);
					if($data['clientName']['status'] == 'true'){
						$data['client_title'] = $data['clientName']['resultSet']['client_title'];
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					}
                    $data['reference_uuid'] = $reference_uuid;
					$data['client_holidays_uuid'] = $client_holidays_uuid;
                    $data['file'] = 'client_holidays/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('client/ClientHolidays');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function deleteHolidays($client_holidays_uuid = "") {
        $data['getHolidays'] = $this->Model_holidays->getHolidaysEdit($client_holidays_uuid);
		$reference_uuid = $data['getHolidays']['resultSet']->client_uuid;
		if ($client_holidays_uuid != "") {
            $deleteHoliday = $this->Model_holidays->isDeleteHoliday($client_holidays_uuid);
            if ($deleteHoliday['status'] == 'true') {
                $this->session->set_flashdata("success_msg", "Holiday is deleted successfully..!!");
                redirect('client/ClientHolidays/view/'.$reference_uuid);
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('client/ClientHolidays/view/'.$reference_uuid);
            }
        } else {
            redirect('client/ClientHolidays/view/'.$reference_uuid);
        }
    }
	
	public function ajax_checkUniqueDate() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $uuid = $this->input->post('uuid');
        $value = date("Y-m-d", strtotime($this->input->post('value')));
		$deleted_field = 'is_deleted';
		$array = array($field => $value, $deleted_field => '0');
		$array = (isset($uuid) && $uuid != "")?array_merge($array,array('client_holidays_uuid!='=>$uuid)):$array;
        $recordCount = $this->db->get_where($table, $array)->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	
/* 	public function ajax_checkUniqueOccasion() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $date = date("Y-m-d", strtotime($this->input->post('date')));
		$deleted_field = 'is_deleted';
        $recordCount = $this->db->get_where($table, array($field => $value, $field => $date, $deleted_field => '0'))->num_rows();
		echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    } */
	

}

?>