<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locations extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_locations");
        $this->load->library('form_validation');
    }

    public function view($referrence_uuid = NULL) {
		//$referrence_uuid  = decode_url($referrence_uuid);
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		$config['base_url'] = base_url() . 'client/locations/view/'.$referrence_uuid;
        $config['total_rows'] = $this->Model_locations->getLocationsPagination($referrence_uuid,$search);
		$config['uri_segment'] = 5;		      
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;      
        $data['clientLoctions'] = $this->Model_locations->getLocationsPagination($referrence_uuid,$search, $config['per_page'], $page);
		//pr($data['clientLoctions']);exit;
		$data['hiddenURL'] = 'client/locations/view/'.$referrence_uuid;
		//pr($data['clientLoctions']);exit;
		$data['clientName'] = $this->Model_locations->getClientNameuuid($referrence_uuid);
		$data['client_title'] = $data['clientName']['resultSet']['client_title'];
		$data['client_type'] = $data['clientName']['resultSet']['client_type'];
		$data['referrence_uuid'] =$referrence_uuid;
        $data['search'] = $search;
		$data['file'] = 'locations/view_form';
        $this->load->view('template/front_template', $data);
    }
    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = $_POST['id'];
        if ($id != "") {
            $Status = $this->Model_locations->updateStatus($status, $id);
			//pr($Status);exit;
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Location is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Location is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }	
	public function delete($client_id= "",$postId = "") {
		//echo $postId;
		$client_id  = decode_url($client_id);
		//echo $client_id;
        $postId = decode_url($postId);
		//echo $postId;exit;

        if ($postId != "") {
            $deleteRole = $this->Model_locations->isDeleteLocation($postId);
			//pr($deleteRole);exit;
            if ($deleteRole['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "Location is deleted successfully..!!");
                redirect('client/locations/view/'.encode_url($client_id));
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('client/locations/view/'.encode_url($client_id));
            }
        } else {
            redirect('client/locations/view/'.encode_url($client_id));
        }
    }

    public function create($referrence_uuid = NULL) {

		//pr($referrence_uuid);exit;
		//$referrence_uuid = decode_url($referrence_uuid);
       $roleAccess = helper_fetchPermission('51', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('location_name', 'Location Name', 'required');
                $this->form_validation->set_rules('location_short_name', 'Location Short Name', 'required');
                $this->form_validation->set_rules('location_description', 'Location Description', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $locationPost = $this->input->post();
					$locationData  = array();
					$locationAddress = array();
					$location_uuid = generate_uuid('loca_');
					
					$locationData['location_uuid'] = $location_uuid;					
                                        $locationData['referrence_uuid'] = $referrence_uuid;
                                        $locationData = array_merge($locationData, $this->cData);					
					$locationData['location_name'] = $locationPost['location_name'];
					$locationData['location_short_name'] = $locationPost['location_short_name'];
					$locationData['location_erp_number'] = $locationPost['location_erp_number'];
					$locationData['location_crm_number'] = $locationPost['location_crm_number'];
					$locationData['location_description'] = $locationPost['location_description'];
					$locationData['location_domain'] = $locationPost['location_domain'];
					$locationData['location_salesperson_code'] = $locationPost['location_salesperson_code'];
					$locationData['location_site_id'] = $locationPost['location_site_id'];					
					
                    $resultData = $this->Model_locations->insertLocation($locationData);
					//pr($resultData);exit;
	                if (!empty($resultData)) {
						for( $i=0;$i<count($locationPost['city']);$i++){
							$locationAddress[$i]['city'] = $locationPost['city'][$i];
							$locationAddress[$i]['district'] = $locationPost['district'][$i];
							$locationAddress[$i]['state'] = $locationPost['state'][$i];
							$locationAddress[$i]['country'] = $locationPost['country'][$i];
							$locationAddress[$i]['postal_code'] = $locationPost['postal_code'][$i];
							$locationAddress[$i]['house_no'] = $locationPost['house_no'][$i];
							$locationAddress[$i]['address_type'] = $locationPost['address_type'][$i];
							$locationAddress[$i]['referrence_uuid'] = $locationData['location_uuid'] ;							
							$locationAddress[$i]['street'] = $locationPost['street'][$i];
							$locationAddress[$i]['full_address'] = $locationPost['full_address'][$i];
							$locationAddress[$i]['logitude'] = $locationPost['logitude'][$i];
							$locationAddress[$i]['latitude'] = $locationPost['latitude'][$i];
							$locationAddress[$i]['created_on'] = $this->current_date;
							$locationAddress[$i]['created_by'] = $this->current_user;
							//$clientAddress[$i]['company_id'] = $this->getCompanyId;
							$locationAddress[$i]['address_uuid'] = generate_uuid('addr_');
							if( $clientPost['is_primary'] == $i){
								$locationAddress[$i]['is_primary'] = 1;	
							}else{
								$locationAddress[$i]['is_primary'] = 0;	
							}
							
							
						}	
						$resultDataAddress = $this->Model_locations->insertAddress($locationAddress);						
                        $this->session->set_flashdata("success_msg", "Location is created successfully ..!!");
                        redirect('client/locations/view/'.$referrence_uuid);
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('client/locations/create/'.$referrence_uuid);
                    }
                } else {
					$data['clientName'] = $this->Model_locations->getClientNameuuid($referrence_uuid);
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					$data['address_types'] = $this->Model_locations->getAddressTypes();
					$data['referrence_uuid'] = $referrence_uuid;
                    $data['file'] = 'locations/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				$data['clientName'] = $this->Model_locations->getClientNameuuid($referrence_uuid);
				$data['client_title'] = $data['clientName']['resultSet']['client_title'];
				$data['client_type'] = $data['clientName']['resultSet']['client_type'];
				$data['address_types'] = $this->Model_locations->getAddressTypes();
				$data['referrence_uuid'] = $referrence_uuid;
                $data['file'] = 'locations/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }	

    public function edit($referrence_uuid = NULL,$postId = NULL) {
		//exit;
        $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            //$postId = decode_url($postId);
            //$getStatus = $this->Model_gateway->isExitGateway($postId);
			//if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
                    $locationPost = $this->input->post();
                    $this->form_validation->set_rules('location_name', 'Location Name', 'required');
                    $this->form_validation->set_rules('location_short_name', 'Locatin Short Name', 'required');
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
						$locationData  = array();
						$locationAddress = array();
						$locationAddressup = array();
						$locationData = array_merge($locationData, $this->uData);					
						$locationData['location_name'] = $locationPost['location_name'];
						$locationData['location_short_name'] = $locationPost['location_short_name'];
						$locationData['location_erp_number'] = $locationPost['location_erp_number'];
						$locationData['location_crm_number'] = $locationPost['location_crm_number'];
						$locationData['location_description'] = $locationPost['location_description'];
						$locationData['location_domain'] = $locationPost['location_domain'];
						$locationData['location_salesperson_code'] = $locationPost['location_salesperson_code'];
						$locationData['location_site_id'] = $locationPost['location_site_id'];							
						
                        $updateStatus = $this->Model_locations->updateLocation($locationData, $postId);
						//pr($updateStatus);exit;
                        if ($updateStatus['status'] == 'true') {
								for( $i=0;$i<count($locationPost['city']);$i++){
									if(isset($locationPost['address_uuid'][$i])){
										$locationAddressup[$i]['city'] = $locationPost['city'][$i];
										$locationAddressup[$i]['district'] = $locationPost['district'][$i];
										$locationAddressup[$i]['state'] = $locationPost['state'][$i];
										$locationAddressup[$i]['country'] = $locationPost['country'][$i];
										$locationAddressup[$i]['postal_code'] = $locationPost['postal_code'][$i];
										$locationAddressup[$i]['house_no'] = $locationPost['house_no'][$i];
										$locationAddressup[$i]['address_type'] = $locationPost['address_type'][$i];
										$locationAddressup[$i]['referrence_uuid'] = $postId;
										$locationAddressup[$i]['address_uuid'] = $locationPost['address_uuid'][$i];										
										$locationAddressup[$i]['street'] = $locationPost['street'][$i];
										$locationAddressup[$i]['full_address'] = $locationPost['full_address'][$i];
										$locationAddressup[$i]['logitude'] = $locationPost['logitude'][$i];
										$locationAddressup[$i]['latitude'] = $locationPost['latitude'][$i];
										$locationAddressup[$i]['updated_on'] = $this->current_date;
										$locationAddressup[$i]['updated_by'] = $this->current_user;
																	
										if( $locationPost['is_primary'] == $i){
											$locationAddressup[$i]['is_primary'] = 1;	
										}else{
											$locationAddressup[$i]['is_primary'] = 0;	
										}
									}else{
										$locationAddress[$i]['city'] = $locationPost['city'][$i];
										$locationAddress[$i]['district'] = $locationPost['district'][$i];
										$locationAddress[$i]['state'] = $locationPost['state'][$i];
										$locationAddress[$i]['country'] = $locationPost['country'][$i];
										$locationAddress[$i]['postal_code'] = $locationPost['postal_code'][$i];
										$locationAddress[$i]['house_no'] = $locationPost['house_no'][$i];
										$locationAddress[$i]['address_type'] = $locationPost['address_type'][$i];
										$locationAddress[$i]['referrence_uuid'] = $postId;							
										$locationAddress[$i]['street'] = $locationPost['street'][$i];
										$locationAddress[$i]['full_address'] = $locationPost['full_address'][$i];
										$locationAddress[$i]['logitude'] = $locationPost['logitude'][$i];
										$locationAddress[$i]['latitude'] = $locationPost['latitude'][$i];
										$locationAddress[$i]['created_on'] = $this->current_date;
										$locationAddress[$i]['created_by'] = $this->current_user;
										//$clientAddress[$i]['company_id'] = $this->getCompanyId;
										$locationAddress[$i]['address_uuid'] = generate_uuid('addr_');
										if( $locationPost['is_primary'] == $i){
											$locationAddress[$i]['is_primary'] = 1;	
										}else{
											$locationAddress[$i]['is_primary'] = 0;	
										}										
									}
									
									
								}	
								//pr($clientAddressup);
								//pr($clientAddress);//exit;
								if(count($locationAddress)>0){
									$resultDataAddress = $this->Model_locations->insertAddress($locationAddress);
									
								}
								if(count($locationAddressup)>0){
									
									$resultDataAddressup = $this->Model_locations->updateAddress($locationAddressup);
								}							
							
                            $this->session->set_flashdata("success_msg", "Location is Updated successfully ..!!");
                            redirect('client/locations/view/'.$referrence_uuid);
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/locations/edit/'.$referrence_uuid.'/'.$postId);
                        }
                    } else {
						$data['clientName'] = $this->Model_locations->getClientNameuuid($referrence_uuid);
						$data['client_title'] = $data['clientName']['resultSet']['client_title'];
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						$data['referrence_uuid'] = $referrence_uuid;
                        $data['getClient'] = $this->Model_locations->getLocationEdit($postId);
						//pr($data['getClient']);exit;
						$data['address_types'] = $this->Model_locations->getAddressTypes();
						$data['getClientAddress'] = $this->Model_locations->getLocationAddress($postId);
                        $data['file'] = 'locations/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['clientName'] = $this->Model_locations->getClientNameuuid($referrence_uuid);
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					$data['referrence_uuid'] = $referrence_uuid;
					
                    $data['getClient'] = $this->Model_locations->getLocationEdit($postId);
				
					$data['address_types'] = $this->Model_locations->getAddressTypes();
					$data['getClientAddress'] = $this->Model_locations->getLocationAddress($postId);
                    $data['file'] = 'locations/update_form';
                    $this->load->view('template/front_template', $data);
                }

        } else {
            redirect('unauthorized');
        }
    }

	
}
?>