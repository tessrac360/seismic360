<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gateway extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_gateway");
        $this->load->library('form_validation');
    }

    public function index() {
/*         $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            $this->getGateway();
        } else {
            redirect('unauthorized');
        } */
    }

    public function getGateway($client_id = NULL) {
		$client_id  = decode_url($client_id);
		//echo $client_id;exit;
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'client/gateway/getgateway/'.encode_url($client_id );
        $config['total_rows'] = $this->Model_gateway->getGatewayPagination($search, $client_id);
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['uri_segment'] = 5;
        $config['display_pages'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

        $data['gateway'] = $this->Model_gateway->getGatewayPagination($search, $client_id, $config['per_page'], $page);
		$data['clientName'] = $this->Model_gateway->getClientName($client_id);
		//pr($data['gateway']);exit;
        $data['client_id'] = $client_id;
        $data['hiddenURL'] = 'client/gateway/searchdata/'.encode_url($client_id );
        $data['file'] = 'gateway/view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }

    public function searchdata($client_id = NULL) {
        $search = $this->input->get_post('sd');
		$client_id  = decode_url($client_id);
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'client/gateway/searchData/'.encode_url($client_id );
        $config['total_rows'] = count($this->Model_gateway->getGatewaySearchData($search,$client_id ));
        $config['uri_segment'] = 5;
        $config['display_pages'] = TRUE;
//$config['first_url'] = base_url() . "client/searchdata/0?sd=" . $search;
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$data['client_id'] = $client_id;
        $data['gateway'] = $this->Model_gateway->getGatewaySearchData($search, $client_id , $config['per_page'], $page);
		$data['clientName'] = $this->Model_gateway->getClientName($client_id);
        $data['search'] = $search;
        $data['hiddenURL'] = 'client/gateway/searchData/'.encode_url($client_id );
        $data['file'] = 'gateway/view_form';
        $this->load->view('template/front_template', $data);
    }

    public function create($client_id = NULL) {

		//pr($client_id);exit;
		$client_id = decode_url($client_id);
        //$level = (isset($_GET['lvl'])) ? decode_url($_GET['lvl']) : 1;
       $roleAccess = helper_fetchPermission('51', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('title', 'Gateway Name', 'required');
                $this->form_validation->set_rules('ip_address', 'IP Address', 'required|is_unique[client_gateways.ip_address]');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $gatewayPost = $this->input->post();
                    $gatewayPost['client_id'] = $client_id;
                    $gatewayPost = array_merge($gatewayPost, $this->cData);
                    $resultData = $this->Model_gateway->insertGateway($gatewayPost);
					//pr($resultData);exit;
	                if (!empty($resultData)) {
                        $this->session->set_flashdata("success_msg", "Gateway is created successfully ..!!");
                        redirect('client/gateway/getGateway/'.encode_url($client_id));
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('client/gateway/create/'.encode_url($client_id));
                    }
                } else {
					$data['clientName'] = $this->Model_gateway->getClientName($client_id);
                    $data['pageTitle'] = 'Your p44age title';
                    $data['file'] = 'gateway/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				$data['clientName'] = $this->Model_gateway->getClientName($client_id);
                $data['pageTitle'] = 'Your p44age title';
                $data['file'] = 'gateway/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($client_id = NULL,$postId = NULL) {
		$client_id = decode_url($client_id);
        $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId);
            $getStatus = $this->Model_gateway->isExitGateway($postId);
			if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
                    $postGateway = $this->input->post();
                    $this->form_validation->set_rules('title', 'Gateway Name', 'required');
                    $this->form_validation->set_rules('ip_address', 'IP Address', 'required');
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
                        $postGateway = array_merge($postGateway, $this->uData); //pr($postUsers);exit;
                        $updateStatus = $this->Model_gateway->updateGateway($postGateway, $postId);
						//pr($updateStatus);exit;
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "Gateway is Updated successfully ..!!");
                            redirect('client/gateway/getGateway/'.encode_url($client_id));
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/gateway/edit');
                        }
                    } else {
						$data['clientName'] = $this->Model_gateway->getClientName($client_id);
                        $data['getGateway'] = $this->Model_gateway->getGatewayEdit($postId);
                        $data['pageTitle'] = 'Your page title';
                        $data['file'] = 'gateway/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['clientName'] = $this->Model_gateway->getClientName($client_id);
                    $data['getGateway'] = $this->Model_gateway->getGatewayEdit($postId);
                    $data['pageTitle'] = 'Your page title';
                    $data['file'] = 'gateway/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('/users');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);


        if ($id != "") {
            $Status = $this->Model_gateway->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Gateway is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Gateway is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }
	
	public function delete($client_id = NULL, $postId = "") {

        $postId = decode_url($postId);

        if ($postId != "") {
            $deleteRole = $this->Model_gateway->isDeleteGateway($postId);
			//pr($deleteRole);exit;
            if ($deleteRole['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "Gateway is deleted successfully..!!");
                redirect('client/gateway/getGateway/'.$client_id);
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('client/gateway/getGateway/'.$client_id);
            }
        } else {
            redirect('client/gateway/getGateway/'.$client_id);
        }
    }
}
?>