<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ClientSystemCredentials extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_system_credentials");
        $this->load->library('form_validation');
    }

    public function view($reference_uuid = NULL) {
        $reference_split = explode("_", $reference_uuid);
        $reference_prefix = $reference_split[0];
		//echo $reference_prefix;exit;
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'client/ClientSystemCredentials/view/' . $reference_uuid;
        $config['total_rows'] = $this->Model_system_credentials->getSysCredentials($reference_uuid, $search);
        $config['uri_segment'] = 5;
        $config['display_pages'] = TRUE;
        $config['enable_query_strings'] = true;
        if ($_GET) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
        }
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data['getSysCredentials'] = $this->Model_system_credentials->getSysCredentials($reference_uuid, $search, $config['per_page'], $page);
        $data['hiddenURL'] = 'client/ClientSystemCredentials/view/' . $reference_uuid;
        //pr($data['clientLoctions']);exit;
		if($reference_prefix =="clie"){
			$data['clientName'] = $this->Model_system_credentials->getClientName($reference_uuid);
			if($data['clientName']['status'] == 'true'){
				$data['title'] = $data['clientName']['resultSet']['client_title'];
				$data['client_type'] = $data['clientName']['resultSet']['client_type'];
				$data['reference'] = "client";
			}		
		}elseif($reference_prefix =="ediv"){
			$data['Name'] = $this->Model_system_credentials->getDeviceName($reference_uuid);
			if($data['Name']['status'] == 'true'){
				$data['title'] = $data['Name']['resultSet']['device_name'];
				$data['clientName'] = $this->Model_system_credentials->getClientName($data['Name']['resultSet']['client_uuid']);
				$data['client_type'] = $data['clientName']['resultSet']['client_type'];
				$data['reference'] = "devices";
			}		
		}elseif($reference_prefix =="eecg"){
				$data['Name'] = $this->Model_system_credentials->getGatewayName($reference_uuid);
				if($data['Name']['status'] == 'true'){
					$data['title'] = $data['Name']['resultSet']['title'];
					$data['clientName'] = $this->Model_system_credentials->getClientName($data['Name']['resultSet']['client_id'],"id");
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					$data['reference'] = "gateways";
				}		
		}

        $data['reference_uuid'] = $reference_uuid;
        $data['search'] = $search;
		$data['file'] = 'client_system_credentials/view_form';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $credentials_uuid = $_POST['credentials_uuid'];
        if ($credentials_uuid != "") {
            $Status = $this->Model_system_credentials->updateSysCredentialStatus($status, $credentials_uuid);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'System Credential is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'System Credential is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function create($reference_uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'add');
        if ($roleAccess == 'Y') {
			$reference_split = explode("_", $reference_uuid);
			$reference_prefix = $reference_split[0];
			if($reference_prefix =="clie"){
				$data['clientName'] = $this->Model_system_credentials->getClientName($reference_uuid);
				if($data['clientName']['status'] == 'true'){
					$data['title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					$data['reference'] = "client";
				}		
			}elseif($reference_prefix =="ediv"){
				$data['Name'] = $this->Model_system_credentials->getDeviceName($reference_uuid);
				if($data['Name']['status'] == 'true'){
					$data['title'] = $data['Name']['resultSet']['device_name'];
					$data['clientName'] = $this->Model_system_credentials->getClientName($data['Name']['resultSet']['client_uuid']);
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					$data['reference'] = "devices";
				}		
			}elseif($reference_prefix =="eecg"){
				$data['Name'] = $this->Model_system_credentials->getGatewayName($reference_uuid);
				if($data['Name']['status'] == 'true'){
					$data['title'] = $data['Name']['resultSet']['title'];
					$data['clientName'] = $this->Model_system_credentials->getClientName($data['Name']['resultSet']['client_id'],"id");
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					$data['reference'] = "gateways";
				}		
			}		
            if ($this->input->post()){
				$this->form_validation->set_rules('login_account_type', 'Login Account Type', 'required');
				$this->form_validation->set_rules('username', 'Username', 'required');
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('port', 'Port', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $SysCredentialsPost = $this->input->post();
					//pr($SysCredentialsPost);exit;
					$SysCredentialsPost['credentials_uuid'] = generate_uuid('sycr_');
					$SysCredentialsPost['referrence_uuid'] = $reference_uuid;
					$SysCredentialsPost = array_merge($SysCredentialsPost, $this->cData);
					$result = $this->Model_system_credentials->insertSysCredentials($SysCredentialsPost);
					//pr($result);exit;
					if($SysCredentialsPost['is_primary'] !=''){
						//pr($SysCredentialsPost['is_primary']);exit;
							$this->ajax_updatePrimarySysCredential($reference_uuid, $SysCredentialsPost['credentials_uuid']);
					}
                    if ($result['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "System Credential is created successfully ..!!");
                        redirect('client/ClientSystemCredentials/view/' . $reference_uuid);
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong while inserting phone data");
                        redirect('client/ClientSystemCredentials/create/' . $reference_uuid);
                    }
                } else {
					$data['isPrimary'] = $this->Model_system_credentials->isPrimarySysCredential($reference_uuid);
					$data['getLoginAccountTypes'] = $this->Model_system_credentials->getLoginAccountTypes();
                    $data['reference_uuid'] = $reference_uuid;
                    $data['file'] = 'client_system_credentials/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				$data['isPrimary'] = $this->Model_system_credentials->isPrimarySysCredential($reference_uuid);
				$data['getLoginAccountTypes'] = $this->Model_system_credentials->getLoginAccountTypes();
                $data['reference_uuid'] = $reference_uuid;
                $data['file'] = 'client_system_credentials/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($reference_uuid = NULL, $credentials_uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            $getStatus = $this->Model_system_credentials->isExistsSysCredential($credentials_uuid);
			if ($getStatus['status'] == 'true') {
				$reference_split = explode("_", $reference_uuid);
				$reference_prefix = $reference_split[0];
				if($reference_prefix =="clie"){
					$data['clientName'] = $this->Model_system_credentials->getClientName($reference_uuid);
					if($data['clientName']['status'] == 'true'){
						$data['title'] = $data['clientName']['resultSet']['client_title'];
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						$data['reference'] = "client";
					}		
				}elseif($reference_prefix =="ediv"){
					$data['Name'] = $this->Model_system_credentials->getDeviceName($reference_uuid);
					if($data['Name']['status'] == 'true'){
						$data['title'] = $data['Name']['resultSet']['device_name'];
						$data['clientName'] = $this->Model_system_credentials->getClientName($data['Name']['resultSet']['client_uuid']);
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						$data['reference'] = "devices";
					}		
				}elseif($reference_prefix =="eecg"){
					$data['Name'] = $this->Model_system_credentials->getGatewayName($reference_uuid);
					if($data['Name']['status'] == 'true'){
						$data['title'] = $data['Name']['resultSet']['title'];
						$data['clientName'] = $this->Model_system_credentials->getClientName($data['Name']['resultSet']['client_id'],"id");
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						$data['reference'] = "gateways";
					}		
				}
                if ($this->input->post()) {
					$this->form_validation->set_rules('login_account_type', 'Login Account Type', 'required');
					$this->form_validation->set_rules('username', 'Username', 'required');
					$this->form_validation->set_rules('password', 'Password', 'required');
					$this->form_validation->set_rules('port', 'Port', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
					if ($this->form_validation->run() == TRUE) {
						$SysCredentialsPost = $this->input->post();
						$SysCredentialsPost = array_merge($SysCredentialsPost, $this->uData);
						$updateStatus = $this->Model_system_credentials->updateSysCredentials($SysCredentialsPost,$credentials_uuid);
						if($SysCredentialsPost['is_primary'] !=''){
							$this->ajax_updatePrimarySysCredential($reference_uuid, $credentials_uuid);
						}
                        if ($updateStatus['status'] == 'true') {
                           $this->session->set_flashdata("success_msg", "System Credential is Updated successfully ..!!");
                            redirect('client/ClientSystemCredentials/view/'.$reference_uuid);
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/ClientSystemCredentials/edit/'.$reference_uuid.'/'.$credentials_uuid);
						}
                    } else {
						$data['getSysCredentials'] = $this->Model_system_credentials->getSysCredentialsEdit($credentials_uuid);
						$data['getLoginAccountTypes'] = $this->Model_system_credentials->getLoginAccountTypes();
                        $data['reference_uuid'] = $reference_uuid;
                        $data['credentials_uuid'] = $credentials_uuid;
                        $data['file'] = 'client_system_credentials/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['getSysCredentials'] = $this->Model_system_credentials->getSysCredentialsEdit($credentials_uuid);
					$data['getLoginAccountTypes'] = $this->Model_system_credentials->getLoginAccountTypes();
                    $data['reference_uuid'] = $reference_uuid;
					$data['credentials_uuid'] = $credentials_uuid;
                    $data['file'] = 'client_system_credentials/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('client/ClientSystemCredentials');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function deleteSysCredential($credentials_uuid = "") {
        $data['getSysCredentials'] = $this->Model_system_credentials->getSysCredentialsEdit($credentials_uuid);
		$reference_uuid = $data['getSysCredentials']['resultSet']->referrence_uuid;
		if ($credentials_uuid != "") {
            $delete = $this->Model_system_credentials->isDeleteSysCredential($credentials_uuid);
            if ($delete['status'] == 'true') {
                $this->session->set_flashdata("success_msg", "System Credential is deleted successfully..!!");
                redirect('client/ClientSystemCredentials/view/'.$reference_uuid);
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('client/ClientSystemCredentials/view/'.$reference_uuid);
            }
        } else {
            redirect('client/ClientSystemCredentials/view/'.$reference_uuid);
        }
    }
	
	public function ajax_checkUniqueDate() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $uuid = $this->input->post('uuid');
        $value = date("Y-m-d", strtotime($this->input->post('value')));
		$deleted_field = 'is_deleted';
		$array = array($field => $value, $deleted_field => '0');
		$array = (isset($uuid) && $uuid != "")?array_merge($array,array('client_holidays_uuid!='=>$uuid)):$array;
        $recordCount = $this->db->get_where($table, $array)->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	
	public function ajax_updatePrimarySysCredential($reference_uuid="", $credentials_uuid="") {
        //$client_uuid = $this->input->post('client_uuid');
        //$contact_uuid = $this->input->post('contact_uuid');
        $updatePrimarySysCredential = $this->Model_system_credentials->updateExistingPrimarySysCredential($reference_uuid,$credentials_uuid);
		//pr($updatePrimarySysCredential);exit;
        if ($updatePrimarySysCredential['status'] == 'true') {
            $this->session->set_flashdata("success_msg", "Previous System Credential is updated successfully..!!");
            redirect('client/ClientSystemCredentials/view/'.$reference_uuid);
        } else {
            $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
            redirect('client/ClientSystemCredentials/create/' . $reference_uuid);
        }
    }
	

}

?>