<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ClientEscalations extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_escalations");
        $this->load->library('form_validation');
    }

    public function view($reference_uuid = NULL) {
        //$reference_split = explode("_", $reference_uuid);
        //$reference_prefix = $reference_split[0];
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'client/ClientEscalations/view/' . $reference_uuid;
        $config['total_rows'] = $this->Model_escalations->getEscalationsAssignGroup($reference_uuid, $search);
        $config['uri_segment'] = 5;
        $config['display_pages'] = TRUE;
        $config['enable_query_strings'] = true;
        if ($_GET) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
        }
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data['escalationsAssignGroup'] = $this->Model_escalations->getEscalationsAssignGroup($reference_uuid, $search, $config['per_page'], $page);
        $data['hiddenURL'] = 'client/ClientEscalations/view/' . $reference_uuid;
        //$data['reference_prefix'] = $reference_prefix;
        //pr($data['clientLoctions']);exit;
		$data['clientName'] = $this->Model_escalations->getClientName($reference_uuid);
		$data['client_title'] = $data['clientName']['resultSet']['client_title'];
		$data['client_type'] = $data['clientName']['resultSet']['client_type'];
        $data['reference_uuid'] = $reference_uuid;
        $data['search'] = $search;
		$data['file'] = 'client_escalations/view_form';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $escalations_assign_group_uuid = $_POST['escalations_assign_group_uuid'];
        $escalation_group_uuid = $_POST['escalation_group_uuid'];
        if ($escalations_assign_group_uuid != "") {
            $Status = $this->Model_escalations->updateEscalationStatus($status, $escalation_group_uuid);
            $Status = $this->Model_escalations->updateAssignGroupStatus($status, $escalations_assign_group_uuid);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Escalation is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Escalation is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function create($reference_uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'add');
        if ($roleAccess == 'Y') {
			
            if ($this->input->post()){
				$this->form_validation->set_rules('escalation_group_uuid', 'Escalation Group', 'required');
				$this->form_validation->set_rules('contact_uuid[]', 'Contact Group', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $EscalationPost = $this->input->post();
					//pr($EscalationPost);exit;
					$EscalationMapping = array();
					
					foreach($EscalationPost['contact_uuid'] as $value){
						$EscalationMapping['escalations_uuid'] = generate_uuid('cles_');
						$EscalationMapping['reference_uuid'] = $reference_uuid;
						$EscalationMapping['escalation_group_uuid'] = $EscalationPost['escalation_group_uuid'];
						$EscalationMapping['contact_uuid'] = $value;
						$EscalationMapping = array_merge($EscalationMapping, $this->cData);
						$result = $this->Model_escalations->insertEscalations($EscalationMapping);
					}
					
					$EscalationAssignGroup = array();
					
					$EscalationAssignGroup['escalations_assign_group_uuid'] = generate_uuid('esas_');
					$EscalationAssignGroup['reference_uuid'] = $reference_uuid;
					$EscalationAssignGroup['escalation_group_uuid'] = $EscalationPost['escalation_group_uuid'];
					$EscalationAssignGroup = array_merge($EscalationAssignGroup, $this->cData);					
					$result = $this->Model_escalations->insertAssignGroup($EscalationAssignGroup);
                    if ($result['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "Group is created successfully ..!!");
                        redirect('client/ClientEscalations/view/' . $reference_uuid);
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong while inserting phone data");
                        redirect('client/ClientEscalations/create/' . $reference_uuid);
                    }
                } else {
					$data['getEscalationIds'] = $this->Model_escalations->getEscalationGroupsIds($reference_uuid);
                    $data['clientName'] = $this->Model_escalations->getClientName($reference_uuid);
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
                    $data['getEscalaltionGroups'] = $this->Model_escalations->getEscalaltionGroups();
                    $data['getContactGroups'] = $this->Model_escalations->getContactGroups($reference_uuid);
                    $data['reference_uuid'] = $reference_uuid;
                    //$data['pageTitle'] = 'ClientEscalations-create';
                    $data['file'] = 'client_escalations/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				$data['getEscalationIds'] = $this->Model_escalations->getEscalationGroupsIds($reference_uuid);
                $data['clientName'] = $this->Model_escalations->getClientName($reference_uuid);
				$data['client_title'] = $data['clientName']['resultSet']['client_title'];
				$data['client_type'] = $data['clientName']['resultSet']['client_type'];
				$data['getEscalaltionGroups'] = $this->Model_escalations->getEscalaltionGroups();
                $data['getContactGroups'] = $this->Model_escalations->getContactGroups($reference_uuid);
                $data['reference_uuid'] = $reference_uuid;
                //$data['pageTitle'] = 'ClientEscalations-create';
                $data['file'] = 'client_escalations/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($reference_uuid = NULL, $escalations_assign_group_uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            $getStatus = $this->Model_escalations->isExistsEscalationGroupAssign($escalations_assign_group_uuid);
			$data['getEscalationsAssignGroupEdit'] = $this->Model_escalations->getEscalationsAssignGroupEdit($escalations_assign_group_uuid);
			$escalation_group_uuid = $data['getEscalationsAssignGroupEdit']['resultSet']->escalation_group_uuid;
			$data['getGroupName'] = $this->Model_escalations->getEscalationGroupName($escalation_group_uuid);
			//pr($data['getGroupName']);exit;
            if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
					//$this->form_validation->set_rules('escalation_group_uuid', 'Escalation Group', 'required');
					$this->form_validation->set_rules('contact_uuid[]', 'Contact Group', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
					if ($this->form_validation->run() == TRUE) {
						$EscalationPost = $this->input->post();
						//pr($EscalationPost);exit;
						unset($EscalationPost['escalation_group_name']);
						$EscalationMapping = array();
						$this->Model_escalations->deleteContactGroupsIds($escalation_group_uuid);
						foreach($EscalationPost['contact_uuid'] as $value){
							$EscalationMapping['escalations_uuid'] = generate_uuid('cles_');
							$EscalationMapping['reference_uuid'] = $reference_uuid;
							$EscalationMapping['escalation_group_uuid'] = $EscalationPost['escalation_group_uuid'];
							$EscalationMapping['contact_uuid'] = $value;
							$EscalationMapping = array_merge($EscalationMapping, $this->uData);
							$updateStatus = $this->Model_escalations->insertEscalations($EscalationMapping);
						}

                        if ($updateStatus['status'] == 'true') {
                           $this->session->set_flashdata("success_msg", "Group is Updated successfully ..!!");
                            redirect('client/ClientEscalations/view/'.$reference_uuid);
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/ClientEscalations/edit/'.$reference_uuid.'/'.$escalations_assign_group_uuid);
						}
                    } else {
						$data['getEscalaltionGroups'] = $this->Model_escalations->getEscalaltionGroups();
						$data['getContactGroups'] = $this->Model_escalations->getContactGroups($reference_uuid);
						$data['clientName'] = $this->Model_escalations->getClientName($reference_uuid);
						$data['client_title'] = $data['clientName']['resultSet']['client_title'];
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						//$data['getEscalationGroupsIds'] = $this->Model_escalations->getEscalationGroupsIds();
						$data['getContactGroupsIds'] = $this->Model_escalations->getContactGroupsIds($reference_uuid,$escalation_group_uuid);
                        $data['reference_uuid'] = $reference_uuid;
                        //$data['pageTitle'] = 'ClientEscalations-edit';
                        $data['file'] = 'client_escalations/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['getEscalaltionGroups'] = $this->Model_escalations->getEscalaltionGroups();
                    $data['getContactGroups'] = $this->Model_escalations->getContactGroups($reference_uuid);
                    $data['clientName'] = $this->Model_escalations->getClientName($reference_uuid);
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
                    //$data['getEscalationGroupsIds'] = $this->Model_escalations->getEscalationGroupsIds();
					$data['getContactGroupsIds'] = $this->Model_escalations->getContactGroupsIds($reference_uuid,$escalation_group_uuid);
                    $data['reference_uuid'] = $reference_uuid;
                    //$data['pageTitle'] = 'ClientEscalations-edit';
                    $data['file'] = 'client_escalations/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('client/ClientEscalations');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function deleteEscalationGroup($escalations_assign_group_uuid = "") {
        $data['getEscalationsAssignGroupEdit'] = $this->Model_escalations->getEscalationsAssignGroupEdit($escalations_assign_group_uuid);
		$reference_uuid = $data['getEscalationsAssignGroupEdit']['resultSet']->reference_uuid;
		$escalation_group_uuid = $data['getEscalationsAssignGroupEdit']['resultSet']->escalation_group_uuid;
        if ($escalations_assign_group_uuid != "") {
            $deleteEscalationGroup = $this->Model_escalations->isDeleteEscalationGroup($escalation_group_uuid);
            $deleteAssignGroup = $this->Model_escalations->isDeleteAssignGroup($escalations_assign_group_uuid);
            if ($deleteAssignGroup['status'] == 'true') {
                $this->session->set_flashdata("success_msg", "Group is deleted successfully..!!");
                redirect('client/ClientEscalations/view/'.$reference_uuid);
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('client/ClientEscalations/view/'.$reference_uuid);
            }
        } else {
            redirect('client/ClientEscalations/view/'.$reference_uuid);
        }
    }

}

?>