<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_category");
        $this->load->library('form_validation');
    }

    /* Start Create Category */

    public function list_category() {
        $roleAccess = helper_fetchPermission('80', 'view');
        if ($roleAccess == 'Y') {

            $search = $this->input->get_post('sd');
            $this->load->library('pagination');
            $config['per_page'] = PAGINATION_PERPAGE;
            $config['base_url'] = base_url() . 'client/category/list_category/';
            $config['total_rows'] = $this->Model_category->getDeviceCategory($search);
            //pr($config['total_rows']);exit;
            $config['uri_segment'] = 4;
            $config['display_pages'] = TRUE;
            $config['enable_query_strings'] = true;
            if ($_GET) {
                $config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
            }
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['user_groups'] = $this->Model_category->getDeviceCategory($search, $config['per_page'], $page);
            //pr($data['queue']);exit;
            $data['hiddenURL'] = 'client/category/list_category/';
            //pr($data['clientTitles']);exit;
            $data['file'] = 'category/list_category';
            $data['search'] = $search;
            $this->load->view('template/front_template', $data);
        } else {
            redirect('unauthorized');
        }
    }

    public function create_category() {
        $roleAccess = helper_fetchPermission('80', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $resultSet = $this->Model_category->createDeviceCategory();
                if ($resultSet['status'] == 'true') {
                    $this->session->set_flashdata("success_msg", "Device Category is created successfully ..!!");
                    redirect('client/category/list_category');
                }
            } else {
                $data['assignGroup'] = $this->Model_category->getAssignedGroup();
                $data['file'] = 'category/create_category';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function update_category($devId = "") {

        $roleAccess = helper_fetchPermission('80', 'edit');
        if ($roleAccess == 'Y') {
            $devId = decode_url($devId);
            if ($this->input->post()) {
                $resultSet = $this->Model_category->updateDeviceCategory($devId);
                if ($resultSet['status'] == 'true') {
                    $this->session->set_flashdata("success_msg", "Device Category is Updated successfully ..!!");
                    redirect('client/category/list_category');
                }
            } else {
                $data['assignGroup'] = $this->Model_category->getAssignedGroup();
                $data['deviceDetails'] = $this->Model_category->fetchDeviceCategoryByID($devId);
                //pr($data);exit;
                $data['file'] = 'category/update_category';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function ajax_changeStatusCategory() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Model_category->updateStatusCategory($status, $id);
            if ($Status['status'] == 'true') {

                if ($status == 'true') {
                    $return['message'] = 'Device Category is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Device Category  is Deactivated successfully';
                }
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    /* End Create Category */
    /* Start Create sub-category */

    public function list_subcategory() {
        $roleAccess = helper_fetchPermission('81', 'view');
        if ($roleAccess == 'Y') {
            $search = $this->input->get_post('sd');
            $this->load->library('pagination');
            $config['per_page'] = PAGINATION_PERPAGE;
            $config['base_url'] = base_url() . 'client/category/list_subcategory/';
            $config['total_rows'] = $this->Model_category->getDeviceSubCategory($search);
            //pr($config['total_rows']);exit;
            $config['uri_segment'] = 4;
            $config['display_pages'] = TRUE;
            $config['enable_query_strings'] = true;
            if ($_GET) {
                $config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
            }
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['user_groups'] = $this->Model_category->getDeviceSubCategory($search, $config['per_page'], $page);
            //pr($data['queue']);exit;
            $data['hiddenURL'] = 'client/category/list_subcategory/';
            //pr($data['clientTitles']);exit;
            $data['file'] = 'category/list_subcategory';
            $data['search'] = $search;
            $this->load->view('template/front_template', $data);
        } else {
            redirect('unauthorized');
        }
    }

    public function create_subcategory() {
        $roleAccess = helper_fetchPermission('81', 'add');
        if ($roleAccess == 'Y') {

            if ($this->input->post()) {
                $resultSet = $this->Model_category->createDeviceSubCategory();
                if ($resultSet['status'] == 'true') {
                    $this->session->set_flashdata("success_msg", "Device Sub-category is created successfully ..!!");
                    redirect('client/category/list_subcategory');
                }
            } else {
                $data['category'] = $this->Model_category->getCategoryList();
                $data['file'] = 'category/create_subcategory';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function update_subcategory($devId = "") {
        $roleAccess = helper_fetchPermission('81', 'edit');
        if ($roleAccess == 'Y') {
            $devId = decode_url($devId);
            if ($this->input->post()) {
                $resultSet = $this->Model_category->updateDeviceSubCategory($devId);
                if ($resultSet['status'] == 'true') {
                    $this->session->set_flashdata("success_msg", "Device Sub-category is Updated successfully ..!!");
                    redirect('client/category/list_subcategory');
                }
            } else {
                $data['category'] = $this->Model_category->getCategoryList();
                $data['deviceDetails'] = $this->Model_category->fetchDeviceSubCategoryByID($devId);
                //pr($data);exit;
                $data['file'] = 'category/update_subcategory';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function ajax_changeStatusSubCategory() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Model_category->updateStatusSubCategory($status, $id);
            if ($Status['status'] == 'true') {

                if ($status == 'true') {
                    $return['message'] = 'Device Sub-category is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Device Sub-category is Deactivated successfully';
                }
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    /* End Create sub-category */
	
	public function ajax_checkUniqueCategoryName() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = trim($this->input->post('value'));
        $recordCount = $this->db->get_where($table, array($field => $value))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	
	public function ajax_checkUniqueSubCategoryName() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = trim($this->input->post('value'));
        $device_sub_category_id = $this->input->post('device_sub_category_id');
		$category_ids = explode("#",$device_sub_category_id);
		$category_id = $category_ids[0];
        $recordCount = $this->db->get_where($table, array($field => $value,'device_category_id' =>$category_id))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
}

?>