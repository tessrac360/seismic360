<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ClientShn extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->library('form_validation');
        $this->load->model('model_shn','shn');
    }

    public function index($reference_uuid = NULL) {
       if($reference_uuid)
	   {
		$data['file'] = 'client_shn/view_form';	 
		$data['clientId'] = $reference_uuid;	 
		$data['clientData'] = $this->shn->getAllClients($reference_uuid);
		$data['client_title'] = $data['clientData']['resultSet']['client_title'];
		$data['client_type'] = $data['clientData']['resultSet']['client_type'];		
		$this->form_validation->set_rules('shn_desc', 'Descrpition', 'required');			
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');		
		if ($this->form_validation->run() == TRUE){
			$client_id = $_POST['client'];
			$y = $_FILES['userfile']['name'];
			$file_name = "";
			if ($y != '')
			{
				$z = $this->do_upload($y);
				
				if(($z['success'] == 1) && isset($z['file_name']) && !empty($z['file_name']))
				{
					$file_name = $z['file_name'];
					if($_POST['old_file'])
					{
						unlink('uploads/client_shn/'.$_POST['old_file']);
					}
				}
			}else if($_POST['old_file'])
			{
				$file_name = $_POST['old_file'];
			}
			
			$update_client = array(
				"client_shn_link" => $_POST['shn_desc'],
				"client_doc_url" => $file_name,
			);
			$update = $this->shn->updateClient($update_client,array('client_uuid'=>$client_id));
			if($update)
				{
					redirect('client/ClientShn/index/'.$client_id);
				}
		}
		else
		{			
			$this->load->view('template/front_template', $data);
		} 
	   }
		
		
    }
	
	
	public function do_upload($d)
	{
			$this->load->library('upload');
			$config['upload_path']          = './uploads/client_shn/';
			$config['allowed_types']        = '*';
			$config['max_size']             = '*';
			$config['max_width']            = '*';
			$config['max_height']           = '*';
			$config['encrypt_name'] 		= false;

			
			$this->upload->initialize($config); 

			if ($this->upload->do_upload('userfile'))
			{
				$data = $this->upload->data();
				 $upload_data['file_name'] =  $data['file_name'];
				 $upload_data['success'] = 1;
				 return $upload_data;
			}
			else
			{
				$upload_data['errors'] = $this->upload->display_errors( '<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>',' </div> ');
				$upload_data['success'] = 0;
				return $upload_data;
			}
	}
	
	public function updateClient()
	{
		$clientId = $_POST['clientId'];
		$file = $_POST['file'];
		if(isset($clientId,$file) && !empty($clientId) && !empty($file))
		{
			//$filePath = base_url('/uploads/client_shn/'.$file);
			unlink('uploads/client_shn/'.$file);
			$update_client = array(
				"client_doc_url" => '',
			);
			$update = $this->shn->updateClient($update_client,array('client_uuid'=>$clientId));
			if($update)
				{
					echo 1;
				}else
				{
					echo 0;
				}
			
		}
		else
		{
			echo 0;
		}
	}
	
	public function download_files($fileName) {
		$this->load->helper('download');
		$file_path = file_get_contents(base_url().'uploads/client_shn/'.$fileName); 
		 force_download($fileName, $file_path);
	}


 

  

 


	

}

?>