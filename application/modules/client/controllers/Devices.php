<?php

error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Devices extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_devices");
        $this->load->model("gateways/Model_gateways", "gateways");
        $this->load->library('form_validation');
    }

    public function view($referrence_uuid = NULL) {
		$referrence_uuid  = decode_url($referrence_uuid);
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		$config['base_url'] = base_url() . 'client/devices/view/'.encode_url($referrence_uuid);
        $config['total_rows'] = $this->Model_devices->getDevicesPagination($referrence_uuid,$search);
		$config['uri_segment'] = 5;		      
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;      
        $data['clientLoctions'] = $this->Model_devices->getDevicesPagination($referrence_uuid,$search, $config['per_page'], $page);
		$data['hiddenURL'] = 'client/devices/view/'.encode_url($referrence_uuid);
		//pr($data['clientLoctions']);exit;
		$data['clientName'] = $this->Model_devices->getClientName($referrence_uuid);
		$data['client_title'] = $data['clientName']['resultSet']['client_title'];
		$data['client_type'] = $data['clientName']['resultSet']['client_type'];
        $data['file'] = 'devices/view_form';
		$data['referrence_uuid'] =$referrence_uuid;
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }
    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = $_POST['id'];
		//echo $id;
        if ($id != "") {
            $Status = $this->Model_devices->updateStatus($status, $id);
			//pr($Status);exit;
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Device is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Device is Deactivated successfully';
                }
				$return['status'] = 'true';
				$deviceDetails = $this->Model_devices->getDeviceDetails($id);
				if($deviceDetails['status'] == 'true'){
					if($deviceDetails['resultSet']['gateway_type']== 1){
						$x = $this->set_cfg_template_host_services($deviceDetails['resultSet']['gateway_uuid']);
						if($x == "fail"){
							$return['status'] = 'false';
							$return['message'] = 'Nagios server not connecting';
						}
					}
				}				

                
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }
    public function ajax_location() {
        $gateway_uuid = $_POST['gateway_id'];
            $Status = $this->Model_devices->getGatewayLocation($gateway_uuid);
			//pr($Status);exit;
            if ($Status['status'] == 'true') {
                $return['location'] = $Status['resultSet']['gateway_location'];
				
				
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        
        echo $json = json_encode($return);
        die();
    }
    public function ajax_sub_categories() {
        $device_category_uuid = $_POST['device_category_uuid'];
            $Status = $this->Model_devices->getDeviceSubcategories($device_category_uuid);
			//pr($Status);exit;
			$subcategories_select = '<option value=""></option>';
            if ($Status['status'] == 'true') {
                				
				if ($Status['status'] == 'true') {
					foreach ($Status['resultSet'] as $value) {
						
					   $subcategories_select.='<option value="'.$value['id'].'#'.$value['uuid'].'">'.$value['subcategory'].'</option>';   
					}
				}
				$return['subcategories'] = $subcategories_select;
				
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
				$subcategories_select = '<option value=""></option>';
				$return['subcategories'] = $subcategories_select;
            }
        
        echo $json = json_encode($return);
        die();
    }	
    public function ajax_service_groups() {
		//exit;
		$gateway_type_uuid ="";
		$device_category_id ="";
		if(isset($_POST['device_category_id']) && $_POST['device_category_id']!=''){
			$device_category_id = $_POST['device_category_id'];
		}
		if(isset($_POST['gateway_type_uuid']) && $_POST['gateway_type_uuid']!=''){
			$gateway_type_uuid = $_POST['gateway_type_uuid'];
		}
		$servicegroup_select = '<option value=""></option>';
		if($gateway_type_uuid !="" && $device_category_id !=""){
            $Status = $this->Model_devices->getDeviceCategoryServiceGroups($device_category_id,$gateway_type_uuid);
			//pr($Status);exit;
			
            if ($Status['status'] == 'true') {
                				
				if ($Status['status'] == 'true') {
					foreach ($Status['resultSet'] as $value) {
						
						$selected = "";
						if($value['default_group'] ==1 ){
							$selected = "selected";
						}
					   $servicegroup_select.='<option value="'.$value['service_group_uuid'].'"  '.$selected.'>'.$value['service_group_name'].'</option>';   
					}
				}
				$return['servicegroups'] = $servicegroup_select;
				
                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';

				$return['servicegroups'] = $servicegroup_select;
            }			
		}else{
			$return['status'] = 'false';
			$servicegroup_select = '<option value=""></option>';		
		}
        echo $json = json_encode($return);
        die();
    }	
	
	public function delete($client_id= "",$postId = "",$gateway_uuid ="") {
		//echo $postId;
		$client_id  = decode_url($client_id);
		//echo $client_id;
		//echo $postId;exit;

        if ($postId != "") {
            $deleteRole = $this->Model_devices->deleteDevice($postId);
			
			//pr($deleteRole);exit;
            if ($deleteRole['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "Location is deleted successfully..!!");
				$deviceDetails = $this->Model_devices->getDeviceDetails($postId);
				if($deviceDetails['status'] == 'true'){
					if($deviceDetails['resultSet']['gateway_type']== 1){
						$x = $this->set_cfg_template_host_services($deviceDetails['resultSet']['gateway_uuid']);
						if($x == 'fail'){
							$this->session->set_flashdata("error_msg", "Nagios Server not connecting");
						}
					}
				}				
                redirect('client/devices/view/'.encode_url($client_id));
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('client/devices/view/'.encode_url($client_id));
            }
        } else {
            redirect('client/devices/view/'.encode_url($client_id));
        }
    }

    public function create($referrence_uuid = NULL) {

		//pr($referrence_uuid);exit;
		$referrence_uuid = decode_url($referrence_uuid);
		$data['clientdetails'] = $this->Model_devices->getClientName($referrence_uuid);
       $roleAccess = helper_fetchPermission('51', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('gateway_id', 'Gateway', 'required');
                $this->form_validation->set_rules('device_category_id', 'Device Category', 'required');
                //$this->form_validation->set_rules('device_location', 'Location', 'required');
                $this->form_validation->set_rules('device_name', 'Device Name', 'required');
                $this->form_validation->set_rules('address', 'Device Address', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
					
                    $devicePost = $this->input->post();
					//pr($devicePost);exit;
					
					
					$deviceData  = array();
					$device_uuid = generate_uuid('ediv_');					
					$deviceData['uuid'] = $device_uuid;					
					$gateway_post = explode("#",$devicePost['gateway_id']);

					$deviceData['gateway_id'] = $gateway_post[2];
					$gateway_type = $gateway_post[1];
					$deviceData['gateway_uuid'] = $gateway_post[0];
									
					$device_category_id_post = explode("#",$devicePost['device_category_id']);											
											
					$deviceData['device_category_id'] = $device_category_id_post[0];
					$deviceData['device_category_uuid'] = $device_category_id_post[1];	
					
					$device_sub_category_id_post = explode("#",$devicePost['uuid']);						
					$deviceData['device_sub_category'] = $device_sub_category_id_post[0];
					$deviceData['device_sub_category_uuid'] = $device_sub_category_id_post[1];	

					$device_parent_id_post = explode("#",$devicePost['device_parent_id']);						
					$deviceData['device_parent_id'] = $device_parent_id_post[0];
					$deviceData['device_parent_uuid'] = $device_parent_id_post[1];	
									
					$deviceData['client_id'] = $referrence_uuid;
					$deviceData['client_uuid'] = $data['clientdetails']['resultSet']['client_uuid'];
														
					$deviceData['device_name'] = $devicePost['device_name'];
					if(isset($devicePost['device_template_id']) && $devicePost['device_template_id']!=""){
						$deviceData['device_template_id'] = $devicePost['device_template_id'];
					}
					$deviceData['address'] = $devicePost['address'];
					//$deviceData['alias'] = $divicePost['alias'];
					$deviceData['device_description'] = $devicePost['device_description'];
					$deviceData['device_mac_address'] = $devicePost['device_mac_address'];
					$deviceData['device_serial_number'] = $devicePost['device_serial_number'];
					$deviceData['device_adr_building'] = $devicePost['device_adr_building'];
					$deviceData['device_adr_room'] = $devicePost['device_adr_room'];					
					$deviceData['device_adr_rack'] = $devicePost['device_adr_rack'];					
					$deviceData['service_group_uuid'] = $devicePost['service_group_uuid'];					
					$deviceData['device_model_number'] = $devicePost['device_model_number'];					
					$deviceData['device_manufacturer'] = $devicePost['device_manufacturer'];					
					$deviceData['device_location'] = $devicePost['device_location'];
					
				
					//exit; 					
					$deviceData = array_merge($deviceData, $this->cData);
					
					//pr($deviceData);
					$resultData = $this->Model_devices->insertDevice($deviceData);
				
					//exit;
					//pr($resultData);exit;
	                if (!empty($resultData)) {
						$this->session->set_flashdata("success_msg", "Device is created successfully ..!!");
						if($gateway_type == 1){
							$x =$this->set_cfg_template_host_services($deviceData['gateway_uuid']);
							if($x == 'fail'){
								$this->session->set_flashdata("error_msg", "Nagios Server not connecting");
							}	
						}
						//$resultDataAddress = $this->Model_locations->insertAddress($locationAddress);						
                        
                        redirect('client/devices/view/'.encode_url($referrence_uuid));
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('client/devices/create/'.encode_url($referrence_uuid));
                    }
                } else {
					
					//pr($data['clientdetails']);exit;
					//$data['address_types'] = $this->Model_locations->getAddressTypes();
					$data['categories'] = $this->Model_devices->getDeviceCategories();	
					
					$data['deviceslist'] = $this->Model_devices->getDevices($referrence_uuid);
					$data['clientLocations'] = $this->Model_devices->getClientLocations($data['clientdetails']['resultSet']['client_uuid']);
					$data['getways'] = $this->Model_devices->getGateways($referrence_uuid,$data['clientdetails']['resultSet']['parent_client_id']);
					$data['deviceTemplates'] = $this->Model_devices->getDeviceTemplates($referrence_uuid);	
					//$data['priorities'] = $this->Model_devices->getPriorities();
					$data['clientName'] = $this->Model_devices->getClientName($referrence_uuid);
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];					
					$data['referrence_uuid'] = $referrence_uuid;
                    $data['file'] = 'devices/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				//$data['clientdetails'] = $this->Model_devices->getClientName($referrence_uuid);				
				//$data['address_types'] = $this->Model_locations->getAddressTypes();
				$data['categories'] = $this->Model_devices->getDeviceCategories();	

				$data['deviceslist'] = $this->Model_devices->getDevices($referrence_uuid);
				$data['clientLocations'] = $this->Model_devices->getClientLocations($data['clientdetails']['resultSet']['client_uuid']);
				$data['getways'] = $this->Model_devices->getGateways($referrence_uuid,$data['clientdetails']['resultSet']['parent_client_id']);
				$data['deviceTemplates'] = $this->Model_devices->getDeviceTemplates($referrence_uuid);	
				$data['clientName'] = $this->Model_devices->getClientName($referrence_uuid);
				$data['client_title'] = $data['clientName']['resultSet']['client_title'];
				$data['client_type'] = $data['clientName']['resultSet']['client_type'];
				//$data['priorities'] = $this->Model_devices->getPriorities();				
				$data['referrence_uuid'] = $referrence_uuid;
                $data['file'] = 'devices/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }	


    public function edit($referrence_uuid = NULL,$postId = NULL) {
		//pr($referrence_uuid);exit;
		$referrence_uuid = decode_url($referrence_uuid);
		$data['clientdetails'] = $this->Model_devices->getClientName($referrence_uuid);
       $roleAccess = helper_fetchPermission('51', 'edit');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('gateway_id', 'Gateway', 'required');
                $this->form_validation->set_rules('device_category_id', 'Device Category', 'required');
                //$this->form_validation->set_rules('device_location', 'Location', 'required');
                $this->form_validation->set_rules('device_name', 'Device Name', 'required');
                $this->form_validation->set_rules('address', 'Device Address', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
					
                    $devicePost = $this->input->post();
					pr($devicePost);//exit;
					
					
					$deviceData  = array();				
					$gateway_post = explode("#",$devicePost['gateway_id']);

					$deviceData['gateway_id'] = $gateway_post[2];
					$gateway_type = $gateway_post[1];
					$deviceData['gateway_uuid'] = $gateway_post[0];
									
					$device_category_id_post = explode("#",$devicePost['device_category_id']);											
											
					$deviceData['device_category_id'] = $device_category_id_post[0];
					$deviceData['device_category_uuid'] = $device_category_id_post[1];	
					
					$device_sub_category_id_post = explode("#",$devicePost['uuid']);						
					$deviceData['device_sub_category'] = $device_sub_category_id_post[0];
					$deviceData['device_sub_category_uuid'] = $device_sub_category_id_post[1];	

					$device_parent_id_post = explode("#",$devicePost['device_parent_id']);						
					$deviceData['device_parent_id'] = $device_parent_id_post[0];
					$deviceData['device_parent_uuid'] = $device_parent_id_post[1];	
									
					$deviceData['client_id'] = $referrence_uuid;
					$deviceData['client_uuid'] = $data['clientdetails']['resultSet']['client_uuid'];
														
					$deviceData['device_name'] = $devicePost['device_name'];
					if(isset($devicePost['device_template_id']) && $devicePost['device_template_id']!=""){
						$deviceData['device_template_id'] = $devicePost['device_template_id'];
					}
					$deviceData['address'] = $devicePost['address'];
					//$deviceData['alias'] = $divicePost['alias'];
					$deviceData['device_description'] = $devicePost['device_description'];
					$deviceData['device_mac_address'] = $devicePost['device_mac_address'];
					$deviceData['device_serial_number'] = $devicePost['device_serial_number'];
					$deviceData['device_adr_building'] = $devicePost['device_adr_building'];
					$deviceData['device_adr_room'] = $devicePost['device_adr_room'];					
					$deviceData['device_adr_rack'] = $devicePost['device_adr_rack'];					
					$deviceData['service_group_uuid'] = $devicePost['service_group_uuid'];					
					$deviceData['device_model_number'] = $devicePost['device_model_number'];					
					$deviceData['device_manufacturer'] = $devicePost['device_manufacturer'];					
					$deviceData['device_location'] = $devicePost['device_location'];
					
				
					//exit; 					
					$deviceData = array_merge($deviceData, $this->uData);
					
					//pr($deviceData);
					$resultData = $this->Model_devices->updateDevice($deviceData,$postId);
				
					//exit;
					//pr($resultData);exit;
	                if (!empty($resultData)) {
						$this->session->set_flashdata("success_msg", "Device is updated successfully ..!!");
						if($gateway_type == 1){
							$x = $this->set_cfg_template_host_services($deviceData['gateway_uuid']);
							if($x == 'fail'){
								$this->session->set_flashdata("error_msg", "Nagios server not connecting");
								
							}
						}
						//$resultDataAddress = $this->Model_locations->insertAddress($locationAddress);						
                       
                        redirect('client/devices/view/'.encode_url($referrence_uuid));
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('client/devices/edit/'.encode_url($referrence_uuid)."/".$postId);
                    }
                } else {
					
					//pr($data['clientdetails']);exit;
					//$data['address_types'] = $this->Model_locations->getAddressTypes();
					$data['deviceDetails'] = $this->Model_devices->getDeviceDetails($postId);
					//pr($data['deviceDetails']);exit;
					$data['categories'] = $this->Model_devices->getDeviceCategories();	
					
					$data['deviceslist'] = $this->Model_devices->getDevices($referrence_uuid);
					$data['clientLocations'] = $this->Model_devices->getClientLocations($data['clientdetails']['resultSet']['client_uuid']);
					$data['getways'] = $this->Model_devices->getGateways($referrence_uuid,$data['clientdetails']['resultSet']['parent_client_id']);
					$data['deviceTemplates'] = $this->Model_devices->getDeviceTemplates($referrence_uuid);	
					//$data['priorities'] = $this->Model_devices->getPriorities();
					$data['clientName'] = $this->Model_devices->getClientName($referrence_uuid);
					$data['seviceGroups'] = $this->Model_devices->getDeviceCategoryServiceGroups($data['deviceDetails']['resultSet']['device_category_id'],$data['deviceDetails']['resultSet']['gateway_type']);
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];					
					$data['referrence_uuid'] = $referrence_uuid;
                    $data['file'] = 'devices/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				//$data['clientdetails'] = $this->Model_devices->getClientName($referrence_uuid);				
				//$data['address_types'] = $this->Model_locations->getAddressTypes();
				$data['deviceDetails'] = $this->Model_devices->getDeviceDetails($postId);
				//pr($data['deviceDetails']);exit;
				$data['categories'] = $this->Model_devices->getDeviceCategories();	

				$data['deviceslist'] = $this->Model_devices->getDevices($referrence_uuid);
				$data['clientLocations'] = $this->Model_devices->getClientLocations($data['clientdetails']['resultSet']['client_uuid']);
				
				$data['getways'] = $this->Model_devices->getGateways($referrence_uuid,$data['clientdetails']['resultSet']['parent_client_id']);
				$data['seviceGroups'] = $this->Model_devices->getDeviceCategoryServiceGroups($data['deviceDetails']['resultSet']['device_category_id'],$data['deviceDetails']['resultSet']['gateway_type']);
				$data['deviceTemplates'] = $this->Model_devices->getDeviceTemplates($referrence_uuid);	
				$data['clientName'] = $this->Model_devices->getClientName($referrence_uuid);
				$data['client_title'] = $data['clientName']['resultSet']['client_title'];
				$data['client_type'] = $data['clientName']['resultSet']['client_type'];
				//$data['priorities'] = $this->Model_devices->getPriorities();				
				$data['referrence_uuid'] = $referrence_uuid;
                $data['file'] = 'devices/update_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }
	
	
	public function set_cfg_template_host_services($gateway_uuid ="") {
		
		$res = $this->gateways->getGatewayEdit($gateway_uuid,"uuid");	
		if ($res['status'] == 'true') {
			$socket = fsockopen($res['resultSet']->ip_address, "5560", $errno, $errstr);
			if($socket) 
			{ 
				
				$context = new ZMQContext();
				//  Socket to talk to server
				$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
				$requester->connect("tcp://".$res['resultSet']->ip_address.":5560");
				$devices = $this->Model_devices->getGatewayDevices($gateway_uuid);
				$devicesTemplates = $this->Model_devices->getGatewayDevicesTemplate($gateway_uuid);
				$servicegroups =array();
				foreach($devices['resultSet'] as $value){							
					$servicegroups[] = $value['service_group_uuid'];
				}
				$servicesTemplates = $this->Model_devices->getServicesTemplatesGateways($servicegroups);					

				//echo $config_template_data;exit;													
				
				$file_path = '/usr/local/nagios/etc/objects/templates.cfg';
				$config_template_data ="";
				$config_template_data .= "define contact{\n";
				$config_template_data .= "\tname\tgeneric-contact\n";
				$config_template_data .= "\tservice_notification_period\t24x7\n";
				$config_template_data .= "\thost_notification_period \t24x7\n";
				$config_template_data .= "\tservice_notification_options\tw,u,c,r,f,s\n";
				$config_template_data .= "\thost_notification_options\td,u,r,f,s\n";
				$config_template_data .= "\tservice_notification_commands\tnotify-service-by-email\n";
				$config_template_data .= "\thost_notification_commands\tnotify-host-by-email\n";
				$config_template_data .= "\tregister\t0\n";
				$config_template_data .= "\t}\n";		
				if ($devicesTemplates['status'] == 'true') {	
					foreach($devicesTemplates['resultSet'] as $HostData){
						$config_template_data .= "define host{\n";
						$config_template_data .= "\tname\t".$HostData['name']."\n";
						$host_selected = unserialize($HostData['selected']);
						if($host_selected){
							foreach($host_selected as $key=>$value){
								
								$config_template_data .= "\t".$key."\t".$value."\n";	
							}
						}
						$config_template_data .= "\tregister\t0\n";
						$config_template_data .= "\t}\n";				
						
					}
				}
				//pr($ServicesData);exit;
				if ($servicesTemplates['status'] == 'true') {	
					foreach($servicesTemplates['resultSet'] as $ServicesData){

						$config_template_data .= "define service{\n";
						$config_template_data .= "\tname\t".$ServicesData['name']."\n";
						$service_selected = unserialize($ServicesData['selected']);
						if($service_selected){
							foreach($service_selected as $key=>$value){					
								$config_template_data .= "\t".$key."\t".$value."\n";	
							}	
						}
						$config_template_data .= "\tregister\t0\n";
						$config_template_data .= "\t}\n";				
						
					}
				}
				
				
				if($res['resultSet']->sync_data_or_not){
					$send_req = array("msg"=>"edit cfg file","data"=>$config_template_data,"file_path"=>$file_path);  
				}else{
					$send_req = array("msg"=>"Adding Hosts","data"=>$config_template_data,"file_path"=>$file_path);				
				}
				$send_json_req = json_encode($send_req);
				if($config_template_data != ""){
					$requester->send($send_json_req);
					echo $reply = $requester->recv();
				}
				
				$file_path = '/usr/local/nagios/etc/objects/all.cfg';
				$config_template_data_host_services ="";
				if ($devices['status'] == 'true') {	
					foreach($devices['resultSet'] as $HostData){
						$config_template_data_host_services .= "define host{\n";
						$config_template_data_host_services .= "\tuse\t".$HostData['name']."\n";
						$config_template_data_host_services .= "\thost_name\t".$HostData['device_name']."\n";
						$config_template_data_host_services .= "\talias\t".$HostData['device_name']."\n";
						$config_template_data_host_services .= "\taddress\t".$HostData['address']."\n";
						/* $host_selected = unserialize($HostData['selected_values']);
						foreach($host_selected as $key=>$value){
							
							$config_template_data_host_services .= "\t".$key."\t".$value."\n";	
						} */
						$config_template_data_host_services .= "\t}\n";				
						
					}
				}
				//pr($ServicesData);exit;
				
				if ($devices['status'] == 'true') {	
					foreach($devices['resultSet'] as $value){
						$servicegroups[0] =	$value['service_group_uuid'];
						$services = $this->Model_devices->getServicesGateways($servicegroups);
						if ($services['status'] == 'true') {	
							foreach($services['resultSet'] as $ServicesData){

								$config_template_data_host_services .= "define service{\n";
								$config_template_data_host_services .= "\tuse\t".$ServicesData['name']."\n";
								$config_template_data_host_services .= "\thost_name\t".$value['device_name']."\n";
								$config_template_data_host_services .= "\tservice_description\t".$ServicesData['service_description']."\n";
								$config_template_data_host_services .= "\tcheck_command\t".$ServicesData['check_command_with_arg']."\n";
								/* $service_selected = unserialize($ServicesData['selected_values']);
								if($service_selected){
									foreach($service_selected as $key=>$value){					
										$config_template_data_host_services .= "\t".$key."\t".$value."\n";	
									}	
								} */
								$config_template_data_host_services .= "\t}\n";				
								
							}
						}
					}
				}
				if($res['resultSet']->sync_data_or_not){
					$send_req = array("msg"=>"edit cfg file","data"=>$config_template_data_host_services,"file_path"=>$file_path);
				}else{
					$send_req = array("msg"=>"Adding Hosts","data"=>$config_template_data_host_services,"file_path"=>$file_path);			
				}
				$send_json_req = json_encode($send_req);
				if($config_template_data_host_services != ""){
					$requester->send($send_json_req);  				
					echo $reply = $requester->recv();
				}
				$staus = "success";
				return $staus; 
			} 
			else 
			{ 
				$staus = "fail";
				return $staus; 
			} 
	
	
		}			
		
		
	}
		
}
?>