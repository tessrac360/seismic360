<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contracts extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_contracts","contr");
        $this->load->model("client/Model_client","client");
        $this->load->model("Model_holidays");
        $this->load->library('form_validation');
    }

    public function view($reference_uuid = NULL) {
        //$reference_split = explode("_", $reference_uuid);
        //$reference_prefix = $reference_split[0];
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'client/contracts/view/' . $reference_uuid;
        $config['total_rows'] = $this->contr->getClientContracts($reference_uuid, $search);
        $clientDetails = $this->client->getClientDetailsByUuid($reference_uuid);
		if ($clientDetails['status'] == 'true') {
			$data['client_type'] =  $clientDetails['resultSet']['client_type'];
		} else {
			$data['client_type'] = "";
		}
		//echo $data['client_type'];exit;
        $config['uri_segment'] = 5;
        $config['display_pages'] = TRUE;
        $config['enable_query_strings'] = true;
        if ($_GET) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
        }
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data['getClientContracts'] = $this->contr->getClientContracts($reference_uuid, $search, $config['per_page'], $page);
        $data['hiddenURL'] = 'client/contracts/view/' . $reference_uuid;
        //$data['reference_prefix'] = $reference_prefix;
        //pr($data['clientLoctions']);exit;
		$data['clientName'] = $this->contr->getClientName($reference_uuid);
        $data['reference_uuid'] = $reference_uuid;
        $data['search'] = $search;
		$data['file'] = 'contracts/view_form';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $client_holidays_uuid = $_POST['client_holidays_uuid'];
        if ($client_holidays_uuid != "") {
            $Status = $this->Model_holidays->updateHolidaysStatus($status, $client_holidays_uuid);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Holiday is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Holiday is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }
	public function changeStatus($reference_uuid = NULL, $uuid= NULL) {
		
		$status = $this->contr->getContractActive($reference_uuid);
		//pr($status);exit;
		if ($status['status'] == 'false') {
			$result = $this->contr->getContractDetails($uuid);			
			//pr($result);exit;
			if ($result['status'] == 'true') {
				if($result['resultSet']['contract_type'] =="BOH"){
					//echo $result['resultSet']['validity_duration'];
					$update_array = array();
					$update_array['contact_validity_start_date'] = date('Y-m-d');
					$update_array['validity_status'] = "Active";
					$update_array['activated_on'] = date('Y-m-d');
					$update_array['contact_validity_end_date'] = date('Y-m-d', strtotime("+".$result['resultSet']['validity_duration']." days"));
					//$update_array['expired_on'] = date('Y-m-d', strtotime("+".$result['resultSet']['validity_duration']." days"));
					$updateStatus = $this->contr->updateContractStatus($update_array,$uuid);
					//echo date('Y-m-d')."<br>";
					//echo date('Y-m-d', strtotime("+".$result['resultSet']['validity_duration']." days"));
					$this->session->set_flashdata("success_msg", "Contract Activated successfully ..!!");
					redirect('client/contracts/view/' . $reference_uuid);					
				}elseif($result['resultSet']['contract_type'] =="FTE"){
					//echo $result['resultSet']['validity_duration'];
					$update_array = array();
					$update_array['contact_validity_start_date'] = date('Y-m-d');
					$update_array['validity_status'] = "Active";
					$update_array['activated_on'] = date('Y-m-d');
					$update_array['contact_validity_end_date'] = date('Y-m-d', strtotime("+".$result['resultSet']['contract_period']." days"));
					//$update_array['expired_on'] = date('Y-m-d', strtotime("+".$result['resultSet']['validity_duration']." days"));
					$updateStatus = $this->contr->updateContractStatus($update_array,$uuid);
					//echo date('Y-m-d')."<br>";
					//echo date('Y-m-d', strtotime("+".$result['resultSet']['validity_duration']." days"));
					$this->session->set_flashdata("success_msg", "Contract Activated successfully ..!!");
					redirect('client/contracts/view/' . $reference_uuid);					
				}

			}
			//exit;

		}else{
			$this->session->set_flashdata("error_msg", "Already Some Active Contract Running");
			redirect('client/contracts/view/' . $reference_uuid);			
		}
		
	}

    public function create($reference_uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'add');		
        if ($roleAccess == 'Y') {
			$clientDetails = $this->client->getClientDetailsByUuid($reference_uuid);
			if ($clientDetails['status'] == 'true') {
				$data['client_type'] =  $clientDetails['resultSet']['client_type'];
			} else {
				$data['client_type'] = "";
			}			
			
            if ($this->input->post()){
				$this->form_validation->set_rules('contract_type_uuid', 'Contract Type', 'required');
				//$this->form_validation->set_rules('validity_uuid', 'Validity Period', 'required');
				$this->form_validation->set_rules('trigger_alert_expiry', 'Expiry Alert', 'required');
				//$this->form_validation->set_rules('radioselection', 'Activaton Options', 'required');
				
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $contractPost = $this->input->post();
					//pr($contractPost);exit;
					$contractData = array();																				
					if($contractPost['contract_type_uuid'] =='cotp_1cb1a5d2-0892-5972-bc39-430729b55e35'){
						$contractData['reference_uuid'] = $reference_uuid;						
						$contractData['contract_type_uuid'] = $contractPost['contract_type_uuid'];
						
						if(isset($contractPost['validity_uuid']) && $contractPost['validity_uuid']!="") {
							$contractData['validity_uuid'] = $contractPost['validity_uuid'];
						}else{
							$contractData['validity_uuid'] = "";
						}						
						if(isset($contractPost['contract_period_uuid']) && $contractPost['contract_period_uuid']!="") {
							$contractData['contract_period_uuid'] = $contractPost['contract_period_uuid'];
						}else{
							$contractData['contract_period_uuid'] = "";
						}					
						$result_validity = $this->contr->getValidityDuration($contractPost['validity_uuid']);
						
						echo $validity_duration = $result_validity['resultSet']['validity_duration']."<br>";
						$result_contract = $this->contr->getContractPeriodDuration($contractPost['contract_period_uuid']);
						echo $contract_duration = $result_contract['resultSet']['contract_period']."<br>";
						echo $number_of_contracts = round($contract_duration/$validity_duration);
						if(isset($contractPost['trigger_alert_expiry']) && $contractPost['trigger_alert_expiry']!="") {
							$contractData['trigger_alert_expiry'] = $contractPost['trigger_alert_expiry'];
						}else{
							$contractData['trigger_alert_expiry'] =0;
						}
						if(isset($contractPost['purchased_BOH']) && $contractPost['purchased_BOH']!="") {
							$contractData['purchased_BOH'] = $contractPost['purchased_BOH']*3600;
						}else{
							$contractData['purchased_BOH'] =0;
						}
						if(isset($contractPost['trigger_alert_BOH']) && $contractPost['trigger_alert_BOH']!="") {
							$contractData['trigger_alert_BOH'] = $contractPost['trigger_alert_BOH']*3600;
						}else{
							$contractData['trigger_alert_BOH'] =0;
						}
						if(isset($contractPost['resource_count']) && $contractPost['resource_count']!="") {
							$contractData['resource_count'] = $contractPost['resource_count'];
						}else{
							$contractData['resource_count'] =0;
						}						
										
						$contractData['purchased_on'] = $this->current_date;
						/* if($contractPost['radioselection'] ==1){						
							$contractData['validity_status'] = 'New';
						}elseif($contractPost['radioselection'] ==2){
							$contractData['validity_status'] = 'New';
							$contractData['auto_renew'] = '1';						
						}elseif($contractPost['radioselection'] ==3){
							$contractData['validity_status'] = 'New';
							$contractData['activate_scheduled_on'] =  $contractPost['activate_scheduled_on'] ;						
						}	 */
						$contractData['validity_status'] = 'New';
						//pr($contractData);exit;
						
						for($i=0;$i<$number_of_contracts;$i++){
							$contractData['uuid'] = generate_uuid('ectr_');					
							
							$contractData = array_merge($contractData, $this->cData);
							$result = $this->contr->insertContract($contractData);
							if ($result['status'] == 'true') {	
								$contract_id = $result['lastId'];
								$transcation_id = "CT-".str_pad($contract_id, 15, '0', STR_PAD_LEFT);
								$transcation_update = array("transcation-id"=>$transcation_id);
								$updateStatus = $this->contr->updateContractTransactionId($transcation_update,$contract_id);							
							}
						}
						
												
					}elseif($contractPost['contract_type_uuid'] =='cotp_4e4cb9c5-1d8f-5e56-adb7-f14e03f75dce'){


						$contractData['reference_uuid'] = $reference_uuid;						
						$contractData['contract_type_uuid'] = $contractPost['contract_type_uuid'];
												
						if(isset($contractPost['contract_period_uuid']) && $contractPost['contract_period_uuid']!="") {
							$contractData['contract_period_uuid'] = $contractPost['contract_period_uuid'];
						}else{
							$contractData['contract_period_uuid'] = "";
						}					
						if(isset($contractPost['trigger_alert_expiry']) && $contractPost['trigger_alert_expiry']!="") {
							$contractData['trigger_alert_expiry'] = $contractPost['trigger_alert_expiry'];
						}else{
							$contractData['trigger_alert_expiry'] =0;
						}

						if(isset($contractPost['resource_count']) && $contractPost['resource_count']!="") {
							$contractData['resource_count'] = $contractPost['resource_count'];
						}else{
							$contractData['resource_count'] =0;
						}						
										
						$contractData['purchased_on'] = $this->current_date;
						/* if($contractPost['radioselection'] ==1){						
							$contractData['validity_status'] = 'New';
						}elseif($contractPost['radioselection'] ==2){
							$contractData['validity_status'] = 'New';
							$contractData['auto_renew'] = '1';						
						}elseif($contractPost['radioselection'] ==3){
							$contractData['validity_status'] = 'New';
							$contractData['activate_scheduled_on'] =  $contractPost['activate_scheduled_on'] ;						
						} */						

						$contractData['validity_status'] = 'New';
						$contractData['uuid'] = generate_uuid('ectr_');					
						
						$contractData = array_merge($contractData, $this->cData);
						$result = $this->contr->insertContract($contractData);
						if ($result['status'] == 'true') {	
							$contract_id = $result['lastId'];
							$transcation_id = "CT-".str_pad($contract_id, 15, '0', STR_PAD_LEFT);
							$transcation_update = array("transcation-id"=>$transcation_id);
							$updateStatus = $this->contr->updateContractTransactionId($transcation_update,$contract_id);							
						}					
					}
					
                    if ($result['status'] == 'true') {						
                        $this->session->set_flashdata("success_msg", "Contract is created successfully ..!!");
                        redirect('client/contracts/view/' . $reference_uuid);
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('client/contracts/create/' . $reference_uuid);
                    }
                } else {
					//$data['getEscalationIds'] = $this->Model_escalations->getEscalationGroupsIds($reference_uuid);
                    $data['clientName'] = $this->contr->getClientName($reference_uuid);
					$data['contractTypes'] = $this->contr->getContractTypes();
					$data['validities'] = $this->contr->getValidities();
					$data['contractperiods'] = $this->contr->getContractPeriod();
                    //$data['getEscalaltionGroups'] = $this->Model_escalations->getEscalaltionGroups();
                    //$data['getContactGroups'] = $this->Model_escalations->getContactGroups($reference_uuid);
                    $data['reference_uuid'] = $reference_uuid;
                    $data['file'] = 'contracts/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				//$data['getEscalationIds'] = $this->Model_escalations->getEscalationGroupsIds($reference_uuid);
                $data['clientName'] = $this->contr->getClientName($reference_uuid);
				$data['contractTypes'] = $this->contr->getContractTypes();
				$data['validities'] = $this->contr->getValidities();
				$data['contractperiods'] = $this->contr->getContractPeriod();
				//$data['getEscalaltionGroups'] = $this->Model_escalations->getEscalaltionGroups();
               // $data['getContactGroups'] = $this->Model_escalations->getContactGroups($reference_uuid);
                $data['reference_uuid'] = $reference_uuid;
                $data['file'] = 'contracts/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }
	public function topup($reference_uuid = NULL, $uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'add');
        if ($roleAccess == 'Y') {
			$clientDetails = $this->client->getClientDetailsByUuid($reference_uuid);
			if ($clientDetails['status'] == 'true') {
				$data['client_type'] =  $clientDetails['resultSet']['client_type'];
			} else {
				$data['client_type'] = "";
			}				
			
            if ($this->input->post()){
				$this->form_validation->set_rules('BOH_topup_hours', 'BOH Topup Hours', 'required');
				
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $contractPost = $this->input->post();
					//pr($contractPost);exit;
					$contractData = array();
					if(isset($contractPost['BOH_topup_hours']) && $contractPost['BOH_topup_hours']!="") {
						$contractData['BOH_topup_hours'] = $contractPost['BOH_topup_hours']*3600;
					}else{
						$contractData['BOH_topup_hours'] =0;
					}					
					$contractData['reference_uuid'] = $reference_uuid;						
					$contractData['contract_type_uuid'] = 'cotp_ce2e9465-adcc-54d9-94be-56e7a4085c3e';									
					$contractData['purchased_on'] = $this->current_date;										
					$contractData['uuid'] = generate_uuid('ectr_');										
					$contractData = array_merge($contractData, $this->cData);
					$result = $this->contr->insertContract($contractData);
												
                    if ($result['status'] == 'true') {
						$contract_id = $result['lastId'];												
						$transcation_id = "CT-".str_pad($contract_id, 15, '0', STR_PAD_LEFT);
						$transcation_update = array("transcation-id"=>$transcation_id);
						$updateStatus = $this->contr->updateContractTransactionId($transcation_update,$contract_id);	
						$result = $this->contr->getContractDetails($uuid);
						$boh_topup = $result['resultSet']['BOH_topup_hours'];
						$boh_topup = $boh_topup+$contractData['BOH_topup_hours'];	
						$transcation_update = array("BOH_topup_hours"=>$boh_topup);
						$updateStatus = $this->contr->updateContractBOHtopup($transcation_update,$uuid);						
                        $this->session->set_flashdata("success_msg", "BOH Topup hours added successfully ..!!");
                        redirect('client/contracts/view/' . $reference_uuid);
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('client/contracts/create/' . $reference_uuid);
                    }
                } else {
					//$data['getEscalationIds'] = $this->Model_escalations->getEscalationGroupsIds($reference_uuid);
                    $data['clientName'] = $this->contr->getClientName($reference_uuid);
					$data['contractTypes'] = $this->contr->getContractTypes();
					$data['validities'] = $this->contr->getValidities();
					$data['contractperiods'] = $this->contr->getContractPeriod();
                    //$data['getEscalaltionGroups'] = $this->Model_escalations->getEscalaltionGroups();
                    //$data['getContactGroups'] = $this->Model_escalations->getContactGroups($reference_uuid);
                    $data['reference_uuid'] = $reference_uuid;
                    $data['file'] = 'client_holidays/create_form_topup';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				//$data['getEscalationIds'] = $this->Model_escalations->getEscalationGroupsIds($reference_uuid);
                $data['clientName'] = $this->contr->getClientName($reference_uuid);
				$data['contractTypes'] = $this->contr->getContractTypes();
				$data['validities'] = $this->contr->getValidities();
				$data['contractperiods'] = $this->contr->getContractPeriod();
				//$data['getEscalaltionGroups'] = $this->Model_escalations->getEscalaltionGroups();
               // $data['getContactGroups'] = $this->Model_escalations->getContactGroups($reference_uuid);
                $data['reference_uuid'] = $reference_uuid;
                $data['file'] = 'contracts/create_form_topup';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }		
	}

    public function edit($reference_uuid = NULL, $client_holidays_uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            $getStatus = $this->Model_holidays->isExistsClientHolidays($client_holidays_uuid);
			if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
					$this->form_validation->set_rules('holiday_date', 'Holiday Date', 'required');
					$this->form_validation->set_rules('holiday_occasion', 'Holiday Occasion', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
					if ($this->form_validation->run() == TRUE) {
						$HolidayPost = $this->input->post();
						//pr($HolidayPost);exit;
						$dob = isset($HolidayPost['holiday_date'])?$HolidayPost['holiday_date']:'';
						$HolidayPost['holiday_year'] = date("Y", strtotime($dob));
						$HolidayPost['holiday_date'] = date("Y-m-d", strtotime($dob));
						$HolidayPost['holiday_occasion'] = $HolidayPost['holiday_occasion'];
						$HolidayPost = array_merge($HolidayPost, $this->uData);
						$updateStatus = $this->Model_holidays->updateHolidays($HolidayPost,$client_holidays_uuid);
                        if ($updateStatus['status'] == 'true') {
                           $this->session->set_flashdata("success_msg", "Holiday is Updated successfully ..!!");
                            redirect('client/ClientHolidays/view/'.$reference_uuid);
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/ClientHolidays/edit/'.$reference_uuid.'/'.$client_holidays_uuid);
						}
                    } else {
						$data['getHolidays'] = $this->Model_holidays->getHolidaysEdit($client_holidays_uuid);
						$data['clientName'] = $this->Model_holidays->getClientName($reference_uuid);
                        $data['reference_uuid'] = $reference_uuid;
                        $data['file'] = 'client_holidays/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['getHolidays'] = $this->Model_holidays->getHolidaysEdit($client_holidays_uuid);
                    $data['clientName'] = $this->Model_holidays->getClientName($reference_uuid);
                    $data['reference_uuid'] = $reference_uuid;
                    $data['file'] = 'client_holidays/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('client/ClientHolidays');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function deleteContract($uuid = "") {
        $data['getHolidays'] = $this->Model_holidays->getHolidaysEdit($client_holidays_uuid);
		$reference_uuid = $data['getHolidays']['resultSet']->client_uuid;
		if ($client_holidays_uuid != "") {
            $deleteHoliday = $this->Model_holidays->isDeleteHoliday($client_holidays_uuid);
            if ($deleteHoliday['status'] == 'true') {
                $this->session->set_flashdata("success_msg", "Holiday is deleted successfully..!!");
                redirect('client/ClientHolidays/view/'.$reference_uuid);
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('client/ClientHolidays/view/'.$reference_uuid);
            }
        } else {
            redirect('client/ClientHolidays/view/'.$reference_uuid);
        }
    }
	
	public function ajax_getValidityDuration() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
		$deleted_field = 'is_deleted';
        $recordCount = $this->db->get_where($table, array($field => $value, $deleted_field => '0'))->row_array();
		//pr($recordCount);exit;
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
			$status['status'] = 'true';
            $status['status'] = $recordCount['validity_duration'];
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	
/* 	public function ajax_checkUniqueOccasion() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $date = date("Y-m-d", strtotime($this->input->post('date')));
		$deleted_field = 'is_deleted';
        $recordCount = $this->db->get_where($table, array($field => $value, $field => $date, $deleted_field => '0'))->num_rows();
		echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    } */
	
	public function ajax_contract_type_list(){
		$contract_type = $_POST['contract_type'];
		$data['contract_type'] = $contract_type;		
		//$data['file'] = 'ajax_actiontabs';
		$data['validities'] = $this->contr->getValidities();
		$data['contractperiods'] = $this->contr->getContractPeriod();		
		$this->load->view('contracts/ajax_contract_type_list', $data);
	}	
	
	public function autoExpiryAndActive(){		
		
		$totalClients = $this->client->getAllClients();
		//pr($totalClients);exit;
		if ($totalClients['status'] == 'true') {
			foreach($totalClients['resultSet'] as $value){
				$getClientContracts = $this->contr->getClientContractsActive($value['client_uuid']);
				echo $value['client_uuid']."<br>";
				if ($getClientContracts['status'] == 'true') {
					//pr($getClientContracts);exit;
					echo $getClientContracts['resultSet']['transcation-id']."<br>";
					echo $getClientContracts['resultSet']['uuid']."<br>";
					if($getClientContracts['resultSet']['contact_validity_end_date']<= date('Y-m-d')){
						
						
						echo "expired"."<br>";
						echo date('Y-m-d')."<br>";
						$result = $this->contr->setExpiry($getClientContracts['resultSet']['uuid']);
						
						$result = $this->contr->getContractNew($value['client_uuid']);
						//echo pr($result);exit;
						/* Select next active contract in assending order */		
						//pr($result);exit;
						if ($result['status'] == 'true') {
							if($result['resultSet']['contract_type'] =="BOH"){
								//echo $result['resultSet']['validity_duration'];
								$update_array = array();
								$update_array['contact_validity_start_date'] = date('Y-m-d');
								$update_array['validity_status'] = "Active";
								$update_array['activated_on'] = date('Y-m-d');
								$update_array['contact_validity_end_date'] = date('Y-m-d', strtotime("+".$result['resultSet']['validity_duration']." days"));
								//$update_array['expired_on'] = date('Y-m-d', strtotime("+".$result['resultSet']['validity_duration']." days"));
								$updateStatus = $this->contr->updateContractStatus($update_array,$result['resultSet']['uuid']);
								//echo date('Y-m-d')."<br>";
								//echo date('Y-m-d', strtotime("+".$result['resultSet']['validity_duration']." days"));				
							}elseif($result['resultSet']['contract_type'] =="FTE"){
								/* //echo $result['resultSet']['validity_duration'];
								$update_array = array();
								$update_array['contact_validity_start_date'] = date('Y-m-d');
								$update_array['validity_status'] = "Active";
								$update_array['activated_on'] = date('Y-m-d');
								$update_array['contact_validity_end_date'] = date('Y-m-d', strtotime("+".$result['resultSet']['contract_period']." days"));
								//$update_array['expired_on'] = date('Y-m-d', strtotime("+".$result['resultSet']['validity_duration']." days"));
								$updateStatus = $this->contr->updateContractStatus($update_array,$uuid); */					
							}

						}
						/* Select next active contract in assending order */
					}
					
				}					
				
				
			}
			
		}
	}	

}

?>