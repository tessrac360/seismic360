<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ContractValidations extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->userId = $this->session->userdata('id');
		$this->client_id = $this->session->userdata('client_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model("Model_contract_validity");
        //$this->load->model("client/Model_client");
        //$this->load->model("location/Model_location");
        $this->load->library('form_validation');
    }

    public function index() {
        $roleAccess = helper_fetchPermission('86', 'view');
        if ($roleAccess == 'Y') {
            $search = $this->input->get_post('sd');
			$this->load->library('pagination');
			$config['per_page'] = PAGINATION_PERPAGE;
			$config['base_url'] = base_url() . 'client/ContractValidations/index/';
			$config['total_rows'] = $this->Model_contract_validity->getContractValidations($search);
			//pr($config['total_rows']);exit;
			$config['uri_segment'] = 4;		      
			$config['display_pages'] = TRUE;
			$config['enable_query_strings'] = true;
			if($_GET){
				$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
			}		
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;      
			$data['contractValidations'] = $this->Model_contract_validity->getContractValidations($search, $config['per_page'], $page);
			//pr($data['queue']);exit;
			$data['hiddenURL'] = 'client/ContractValidations/index/';
			//pr($data['clientTitles']);exit;
			$data['file'] = 'client_contract_validity/view_form';
			$data['search'] = $search;
			$this->load->view('template/front_template', $data);
			
         } else {
            redirect('unauthorized');
        } 
    }

    public function create() {
        $roleAccess = helper_fetchPermission('86', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $ContractPost = $this->input->post();
				//pr($ContractPost);exit;
                $this->form_validation->set_rules('validity_type', 'Validity Type', 'required');
                $this->form_validation->set_rules('validity_duration', 'Validity Duration', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $ContractPost['uuid'] = generate_uuid('ecva_');
                    $ContractPost = array_merge($ContractPost, $this->cData);
                    $resultData = $this->Model_contract_validity->insertContractValidity($ContractPost);
					if($ContractPost['is_default'] !=''){
							$this->ajax_updateDefaultContract($ContractPost['uuid']);
						}
                    if ($resultData['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "Contract Validation is created successfully ..!!");
                        redirect('client/ContractValidations');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('client/ContractValidations/create');
                    }
                } else {
					$data['isDefault'] = $this->Model_contract_validity->isDefaultContractValidity();
                    $data['file'] = 'client_contract_validity/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				$data['isDefault'] = $this->Model_contract_validity->isDefaultContractValidity();
                $data['file'] = 'client_contract_validity/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        } 
    }

    public function edit($postId = NULL) {
        $roleAccess = helper_fetchPermission('86', 'edit');
        if ($roleAccess == 'Y') {
                if ($this->input->post()) {
                    $ContractPost = $this->input->post();
					//pr($ContractPost);exit;
					$this->form_validation->set_rules('validity_type', 'Validity Type', 'required');
					$this->form_validation->set_rules('validity_duration', 'Validity Duration', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
					if ($this->form_validation->run() == TRUE) {
						$ContractPost = array_merge($ContractPost, $this->cData);
						$updateStatus = $this->Model_contract_validity->updateContractValidity($ContractPost, $postId);
						if($ContractPost['is_default'] !=''){
							$this->ajax_updateDefaultContract($postId);
						}
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "Contract Validation is Updated successfully ..!!");
                            redirect('client/ContractValidations');
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/ContractValidations/edit/' . $postId);
                        }
                    } else {
						$data['getContractValidity'] = $this->Model_contract_validity->getContractValidityEdit($postId);
						$data['uuid'] = $postId;
						$data['file'] = 'client_contract_validity/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['getContractValidity'] = $this->Model_contract_validity->getContractValidityEdit($postId);
					$data['uuid'] = $postId;
                    $data['file'] = 'client_contract_validity/update_form';
                    $this->load->view('template/front_template', $data);
                }
            
        } else {
            redirect('unauthorized');
        }
    }

	 public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = $_POST['id'];
        if ($id != "") {
            $Status = $this->Model_contract_validity->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                
                if ($status == 'true') {
                    $return['message'] = 'Contract Validation is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Contract Validation is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }
	
	
	public function ajax_checkUniqueValidityType() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = trim($this->input->post('value'));
		$deleted_field = 'is_deleted';
		$recordCount = $this->db->get_where($table, array($field => $value,$deleted_field => '0'))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	
	public function ajax_checkUniqueValidityDuration() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = trim($this->input->post('value'));
		$deleted_field = 'is_deleted';
		$recordCount = $this->db->get_where($table, array($field => $value,$deleted_field => '0'))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	
	public function deleteContractValidity($postId = "") {
        if ($postId != "") {
            $delete = $this->Model_contract_validity->isDeleteContractValidity($postId);
			//pr($delete);exit;
            if ($delete['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "Contract Validation is deleted successfully..!!");
                redirect('client/ContractValidations');
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('client/ContractValidations');;
            }
        } else {
            redirect('client/ContractValidations');
        }
    }
	
	public function ajax_updateDefaultContract($uuid="") {
        //$client_uuid = $this->input->post('client_uuid');
        //$contact_uuid = $this->input->post('contact_uuid');
        $updateDefaultContract = $this->Model_contract_validity->updateExistingDefaultContract($uuid);
        //pr($updateContactPrimary);exit;
        if ($updateDefaultContract['status'] == 'true') {
            $this->session->set_flashdata("success_msg", "Previous Contract is updated successfully..!!");
            redirect('client/ContractValidations/');
        } else {
            $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
            redirect('client/ContractValidations/create/' . $uuid);
        }
    }
}
