<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contacts extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->current_date = $this->config->item('datetime');
        $this->current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $this->current_date, 'created_by' => $this->current_user);
        $this->uData = array('updated_on' => $this->current_date, 'updated_by' => $this->current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_contacts");
        $this->load->library('form_validation');
    }

    public function bulk_upload_contact($reference_uuid = NULL) {
        if ($this->input->post()) {
            $this->load->library("excel");
            $file_info = pathinfo($_FILES["userfile"]["name"]);
            $file_directory = "uploads/";
            $new_file_name = date("d-m-Y ") . rand(000000, 999999) . "." . $file_info["extension"];
            $file_type = PHPExcel_IOFactory::identify($_FILES['userfile']['tmp_name']);
            $objReader = PHPExcel_IOFactory::createReader($file_type);
            $objPHPExcel = $objReader->load($_FILES['userfile']['tmp_name']);


            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                echo "<br>" . $worksheetTitle;
                if ($worksheetTitle != 'config') {
                    echo '<br><table border="1"><tr>';
                    for ($row = 1; $row <= $highestRow; ++$row) {
                        echo '<tr>';
                        for ($col = 0; $col < $highestColumnIndex; ++$col) {
                            $cell = $worksheet->getCellByColumnAndRow($col, $row);
                            $val = $cell->getValue();
                            echo '<td>' . $val . '</td>';
                        }
                        echo '</tr>';
                    }
                }
                echo '</table>';
            }
            exit;
        } else {
            $data['file'] = 'contacts/bulk_upload_contact';
            $this->load->view('template/front_template', $data);
        }
    }

    public function view($reference_uuid = NULL) {
        $reference_split = explode("_", $reference_uuid);
        $reference_prefix = $reference_split[0];
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'client/contacts/view/' . $reference_uuid;
        $config['total_rows'] = $this->Model_contacts->getContactsPagination($reference_prefix, $reference_uuid, $search);
        $config['uri_segment'] = 5;
        $config['display_pages'] = TRUE;
        $config['enable_query_strings'] = true;
        if ($_GET) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
        }
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data['clientContacts'] = $this->Model_contacts->getContactsPagination($reference_prefix, $reference_uuid, $search, $config['per_page'], $page);
        $data['hiddenURL'] = 'client/contacts/view/' . $reference_uuid;
        $data['reference_prefix'] = $reference_prefix;
        //pr($data['clientLoctions']);exit;
		$data['clientName'] = $this->Model_contacts->getClientName($reference_uuid);
		if($data['clientName']['status'] == 'true'){
			$data['client_title'] = $data['clientName']['resultSet']['client_title'];
			$data['client_type'] = $data['clientName']['resultSet']['client_type'];
		}
        $data['reference_uuid'] = $reference_uuid;
        $data['search'] = $search;
		$data['file'] = 'contacts/view_form';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $contact_uuid = $_POST['contact_uuid'];
        if ($contact_uuid != "") {
            $Status = $this->Model_contacts->updateStatus($status, $contact_uuid);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Contact is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Contact is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function create($reference_uuid = NULL) {
        //$data['primaryContact'] = $this->Model_contacts->isPrimaryContactExists();
        //pr($data['primaryContact']);exit;
        $roleAccess = helper_fetchPermission('51', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()){
				$this->form_validation->set_rules('contact_name', 'Contact Name', 'required');
                $this->form_validation->set_rules('first_name', 'First Name', 'required');
                $this->form_validation->set_rules('last_name', 'Last Name', 'required');
                /* $this->form_validation->set_rules('full_name', 'Full Name', 'required');
                $this->form_validation->set_rules('sex', 'Gender', 'required');
                $this->form_validation->set_rules('dob', 'DOB', 'required');
                $this->form_validation->set_rules('is_primary', 'Is Primary', 'required'); */
                $this->form_validation->set_rules('phone_number[]', 'Phone Number', 'required');
               /*  $this->form_validation->set_rules('phone_area_code[]', 'Phone Area Code', 'required');
                $this->form_validation->set_rules('phone_country_code[]', 'Phone Country Code', 'required');
                $this->form_validation->set_rules('pn_category[]', 'Phone Category', 'required');
                $this->form_validation->set_rules('pn_type[]', 'Phone Type', 'required'); */
                $this->form_validation->set_rules('phone_primary', 'Phone Primary', 'required');
                $this->form_validation->set_rules('email_address[]', 'Email Address', 'required');
                //$this->form_validation->set_rules('email_category[]', 'Email Category', 'required');
                $this->form_validation->set_rules('email_primary', 'Email Primary', 'required');
                /* $this->form_validation->set_rules('city[]', 'City', 'required');
                $this->form_validation->set_rules('district[]', 'District', 'required');
                $this->form_validation->set_rules('state[]', 'State', 'required');
                $this->form_validation->set_rules('country[]', 'Country', 'required');
                $this->form_validation->set_rules('postal_code[]', 'Postal Code', 'required');
                $this->form_validation->set_rules('house_no[]', 'House Number', 'required');
                $this->form_validation->set_rules('address_type[]', 'Address Type', 'required');
                $this->form_validation->set_rules('street[]', 'Street', 'required');
                $this->form_validation->set_rules('full_address[]', 'Full Address', 'required');
                $this->form_validation->set_rules('logitude[]', 'Logitude', 'required');
                $this->form_validation->set_rules('latitude[]', 'Latitude', 'required');
                $this->form_validation->set_rules('address_primary', 'Address Primary', 'required'); */
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $contactPost = $this->input->post();
                   // pr($contactPost);exit;
                    $ContactData = array();
                    $contact_uuid = generate_uuid('cont_');
                    $ContactData['contact_uuid'] = $contact_uuid;
                    $ContactData['reference_uuid'] = $reference_uuid;
                    $ContactData['contact_name'] = $contactPost['contact_name'];
                    $ContactData['first_name'] = $contactPost['first_name'];
                    $ContactData['second_name'] = $contactPost['second_name'];
                    $ContactData['last_name'] = $contactPost['last_name'];
                    $ContactData['full_name'] = $contactPost['full_name'];
                    $ContactData['sex'] = isset($contactPost['sex'])?$contactPost['sex']:'';
                    $dob = isset($contactPost['dob'])?$contactPost['dob']:'';
                    $ContactData['dob'] = date("Y-m-d", strtotime($dob));
                    $ContactData = array_merge($ContactData, $this->cData);
                    $resultContactData = $this->Model_contacts->insertContacts($ContactData);
                    //pr($resultData);exit;
                    if ($resultContactData['status'] == 'true') {
                        $PhoneData = array();
                        $EmailData = array();
                        $AddressData = array();
						
                        for ($i = 0; $i < count($contactPost['phone_number']); $i++) {
                            $PhoneData[$i]['phone_uuid'] = generate_uuid('phon_');
                            $PhoneData[$i]['referrence_uuid'] = $contact_uuid;
                            $PhoneData[$i]['phone_number'] = $contactPost['phone_number'][$i];
                            $PhoneData[$i]['phone_area_code'] = $contactPost['phone_area_code'][$i];
                            $PhoneData[$i]['phone_country_code'] = $contactPost['phone_country_code'][$i];
                            $PhoneData[$i]['pn_category'] = $contactPost['pn_category'][$i];
                            $PhoneData[$i]['pn_type'] = $contactPost['pn_type'][$i];
                            if ($contactPost['phone_primary'] == $i) {
                                $PhoneData[$i]['is_primary'] = 1;
                            } else {
                                $PhoneData[$i]['is_primary'] = 0;
                            }
                            $PhoneData[$i]['created_on'] = $this->current_date;
                            $PhoneData[$i]['created_by'] = $this->current_user;
                        }
                        $resultPhoneData = $this->Model_contacts->insertPhones($PhoneData);

                        for ($i = 0; $i < count($contactPost['email_address']); $i++) {
                            $EmailData[$i]['email_uuid'] = generate_uuid('emai_');
                            $EmailData[$i]['reference_uuid'] = $contact_uuid;
                            $EmailData[$i]['email_address'] = $contactPost['email_address'][$i];
                            $EmailData[$i]['email_category'] = $contactPost['email_category'][$i];
                            if ($contactPost['email_primary'] == $i) {
                                $EmailData[$i]['is_primary'] = 1;
                            } else {
                                $EmailData[$i]['is_primary'] = 0;
                            }
                            $EmailData[$i]['created_on'] = $this->current_date;
                            $EmailData[$i]['created_by'] = $this->current_user;
                        }
                        $resultEmailData = $this->Model_contacts->insertEmails($EmailData);
                        //pr($resultEmailData);exit;	
						if($contactPost['city'][0] != ''){
							for ($i = 0; $i < count($contactPost['city']); $i++) {
								$AddressData[$i]['address_uuid'] = generate_uuid('addr_');
								$AddressData[$i]['referrence_uuid'] = $contact_uuid;
								$AddressData[$i]['house_no'] = $contactPost['house_no'][$i];
								$AddressData[$i]['street'] = $contactPost['street'][$i];
								$AddressData[$i]['city'] = $contactPost['city'][$i];
								$AddressData[$i]['district'] = $contactPost['district'][$i];
								$AddressData[$i]['state'] = $contactPost['state'][$i];
								$AddressData[$i]['country'] = $contactPost['country'][$i];
								$AddressData[$i]['postal_code'] = $contactPost['postal_code'][$i];
								$AddressData[$i]['full_address'] = $contactPost['full_address'][$i];
								$AddressData[$i]['address_type'] = $contactPost['address_type'][$i];
								$AddressData[$i]['logitude'] = $contactPost['logitude'][$i];
								$AddressData[$i]['latitude'] = $contactPost['latitude'][$i];
								$AddressData[$i]['created_on'] = $this->current_date;
								$AddressData[$i]['created_by'] = $this->current_user;
								if ($contactPost['address_primary'] == $i) {
									$AddressData[$i]['is_primary'] = 1;
								} else {
									$AddressData[$i]['is_primary'] = 0;
								}
							}
							$resultDataAddress = $this->Model_contacts->insertAddress($AddressData);
						}
						if($contactPost['is_primary'] !=''){
							$this->ajax_updatePrimaryContact($reference_uuid,$contact_uuid);
						}


                        $this->session->set_flashdata("success_msg", "Contact is created successfully ..!!");
                        redirect('client/contacts/view/' . $reference_uuid);
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong while inserting phone data");
                        redirect('client/contacts/create/' . $reference_uuid);
                    }
                } else {
                    $data['clientName'] = $this->Model_contacts->getClientName($reference_uuid);
					if($data['clientName']['status'] == 'true'){
						$data['client_title'] = $data['clientName']['resultSet']['client_title'];
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					}
                    $data['primaryContact'] = $this->Model_contacts->isPrimaryContactExists($reference_uuid);
                    $data['address_types'] = $this->Model_contacts->getAddressTypes();
                    $data['reference_uuid'] = $reference_uuid;
                    $data['file'] = 'contacts/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['clientName'] = $this->Model_contacts->getClientName($reference_uuid);
				if($data['clientName']['status'] == 'true'){
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
				}
                $data['primaryContact'] = $this->Model_contacts->isPrimaryContactExists($reference_uuid);
                $data['address_types'] = $this->Model_contacts->getAddressTypes();
                $data['reference_uuid'] = $reference_uuid;
                $data['file'] = 'contacts/create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($reference_uuid = NULL, $contact_uuid = NULL) {
        $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            $getStatus = $this->Model_contacts->isExitContact($contact_uuid);
            if ($getStatus['status'] == 'true') {
                if ($this->input->post()) {
                    $contactPost = $this->input->post();
                    //pr($contactPost);exit;
                    $this->form_validation->set_rules('contact_name', 'Contact Name', 'required');
                    $this->form_validation->set_rules('first_name', 'First Name', 'required');
                    $this->form_validation->set_rules('last_name', 'Last Name', 'required');
                    /* $this->form_validation->set_rules('full_name', 'Full Name', 'required');
                    $this->form_validation->set_rules('sex', 'Gender', 'required');
                    $this->form_validation->set_rules('dob', 'DOB', 'required');
                    $this->form_validation->set_rules('is_primary', 'Is Primary', 'required'); */
                    $this->form_validation->set_rules('phone_number[]', 'Phone Number', 'required');
                    /* $this->form_validation->set_rules('phone_area_code[]', 'Phone Area Code', 'required');
                    $this->form_validation->set_rules('phone_country_code[]', 'Phone Country Code', 'required');
                    $this->form_validation->set_rules('pn_category[]', 'Phone Category', 'required');
                    $this->form_validation->set_rules('pn_type[]', 'Phone Type', 'required'); */
                    $this->form_validation->set_rules('phone_primary', 'Phone Primary', 'required');
                    $this->form_validation->set_rules('email_address[]', 'Email Address', 'required');
                    //$this->form_validation->set_rules('email_category[]', 'Email Category', 'required');
                    $this->form_validation->set_rules('email_primary', 'Email Primary', 'required');
                    /* $this->form_validation->set_rules('city[]', 'City', 'required');
                    $this->form_validation->set_rules('district[]', 'District', 'required');
                    $this->form_validation->set_rules('state[]', 'State', 'required');
                    $this->form_validation->set_rules('country[]', 'Country', 'required');
                    $this->form_validation->set_rules('postal_code[]', 'Postal Code', 'required');
                    $this->form_validation->set_rules('house_no[]', 'House Number', 'required');
                    $this->form_validation->set_rules('address_type[]', 'Address Type', 'required');
                    $this->form_validation->set_rules('street[]', 'Street', 'required');
                    $this->form_validation->set_rules('full_address[]', 'Full Address', 'required');
                    $this->form_validation->set_rules('logitude[]', 'Logitude', 'required');
                    $this->form_validation->set_rules('latitude[]', 'Latitude', 'required');
                    $this->form_validation->set_rules('address_primary', 'Address Primary', 'required'); */
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {

                        $ContactData = array();

                        $ContactData['contact_name'] = $contactPost['contact_name'];
                        $ContactData['first_name'] = $contactPost['first_name'];
                        $ContactData['second_name'] = $contactPost['second_name'];
                        $ContactData['last_name'] = $contactPost['last_name'];
                        $ContactData['full_name'] = $contactPost['full_name'];
                        $ContactData['sex'] = $contactPost['sex'];
                        $dob = $contactPost['dob'];
                        $ContactData['dob'] = date("Y-m-d", strtotime($dob));
                        $ContactData['is_primary'] = $contactPost['is_primary'];
                        $ContactData = array_merge($ContactData, $this->uData);
                        $updateStatus = $this->Model_contacts->updateContacts($ContactData, $contact_uuid);
                        if ($updateStatus['status'] == 'true') {
                            $clientPhones = array();
                            $clientPhonesup = array();
                            $data['getContact'] = $this->Model_contacts->getContactsEdit($contact_uuid);
                            for ($i = 0; $i < count($contactPost['phone_number']); $i++) {
                                if (isset($contactPost['phone_uuid'][$i])) {
                                    //pr($contactPost['phone_uuid'][$i]);
                                    $clientPhonesup[$i]['phone_uuid'] = $contactPost['phone_uuid'][$i];
                                    $clientPhonesup[$i]['referrence_uuid'] = $data['getContact']['resultSet']->contact_uuid;
                                    $clientPhonesup[$i]['phone_number'] = $contactPost['phone_number'][$i];
                                    $clientPhonesup[$i]['phone_area_code'] = $contactPost['phone_area_code'][$i];
                                    $clientPhonesup[$i]['phone_country_code'] = $contactPost['phone_country_code'][$i];
                                    $clientPhonesup[$i]['pn_category'] = $contactPost['pn_category'][$i];
                                    $clientPhonesup[$i]['pn_type'] = $contactPost['pn_type'][$i];
                                    if ($contactPost['phone_primary'] == $i) {
                                        $clientPhonesup[$i]['is_primary'] = 1;
                                    } else {
                                        $clientPhonesup[$i]['is_primary'] = 0;
                                    }
                                    $clientPhonesup[$i]['created_on'] = $this->current_date;
                                    $clientPhonesup[$i]['created_by'] = $this->current_user;
                                } else {
                                    $clientPhones[$i]['phone_uuid'] = generate_uuid('phon_');
                                    $clientPhones[$i]['referrence_uuid'] = $data['getContact']['resultSet']->contact_uuid;
                                    $clientPhones[$i]['phone_number'] = $contactPost['phone_number'][$i];
                                    $clientPhones[$i]['phone_area_code'] = $contactPost['phone_area_code'][$i];
                                    $clientPhones[$i]['phone_country_code'] = $contactPost['phone_country_code'][$i];
                                    $clientPhones[$i]['pn_category'] = $contactPost['pn_category'][$i];
                                    $clientPhones[$i]['pn_type'] = $contactPost['pn_type'][$i];
                                    if ($contactPost['phone_primary'] == $i) {
                                        $clientPhones[$i]['is_primary'] = 1;
                                    } else {
                                        $clientPhones[$i]['is_primary'] = 0;
                                    }
                                    $clientPhones[$i]['created_on'] = $this->current_date;
                                    $clientPhones[$i]['created_by'] = $this->current_user;
                                }
                            }

                            //pr($clientPhonesup);
                            //pr($clientPhones);exit;

                            if (count($clientPhones) > 0) {
                                $resultDataPhones = $this->Model_contacts->insertPhones($clientPhones);
                            }
                            if (count($clientPhonesup) > 0) {
                                $resultDataPhonesup = $this->Model_contacts->updatePhones($clientPhonesup);
                            }

                            $clientEmails = array();
                            $clientEmailsup = array();
                            for ($i = 0; $i < count($contactPost['email_address']); $i++) {
                                if (isset($contactPost['email_uuid'][$i])) {
                                    $clientEmailsup[$i]['email_uuid'] = $contactPost['email_uuid'][$i];
                                    $clientEmailsup[$i]['reference_uuid'] = $data['getContact']['resultSet']->contact_uuid;
                                    $clientEmailsup[$i]['email_address'] = $contactPost['email_address'][$i];
                                    $clientEmailsup[$i]['email_category'] = $contactPost['email_category'][$i];
                                    if ($contactPost['email_primary'] == $i) {
                                        $clientEmailsup[$i]['is_primary'] = 1;
                                    } else {
                                        $clientEmailsup[$i]['is_primary'] = 0;
                                    }
                                    $clientEmailsup[$i]['created_on'] = $this->current_date;
                                    $clientEmailsup[$i]['created_by'] = $this->current_user;
                                } else {
                                    $clientEmails[$i]['email_uuid'] = generate_uuid('emai_');
                                    $clientEmails[$i]['reference_uuid'] = $data['getContact']['resultSet']->contact_uuid;
                                    $clientEmails[$i]['email_address'] = $contactPost['email_address'][$i];
                                    $clientEmails[$i]['email_category'] = $contactPost['email_category'][$i];
                                    if ($contactPost['email_primary'] == $i) {
                                        $clientEmails[$i]['is_primary'] = 1;
                                    } else {
                                        $clientEmails[$i]['is_primary'] = 0;
                                    }
                                    $clientEmails[$i]['created_on'] = $this->current_date;
                                    $clientEmails[$i]['created_by'] = $this->current_user;
                                }
                            }

                            //pr($clientEmailsup);
                            //pr($clientEmails);exit;

                            if (count($clientEmails) > 0) {
                                $resultDataEmails = $this->Model_contacts->insertEmails($clientEmails);
                            }
                            if (count($clientEmailsup) > 0) {
                                $resultDataEmailsup = $this->Model_contacts->updateEmails($clientEmailsup);
                            }

                            $contactAddress = array();
                            $contactAddressup = array();
                            for ($i = 0; $i < count($contactPost['city']); $i++) {
                                if (isset($contactPost['address_uuid'][$i])) {
                                    $contactAddressup[$i]['city'] = $contactPost['city'][$i];
                                    $contactAddressup[$i]['district'] = $contactPost['district'][$i];
                                    $contactAddressup[$i]['state'] = $contactPost['state'][$i];
                                    $contactAddressup[$i]['country'] = $contactPost['country'][$i];
                                    $contactAddressup[$i]['postal_code'] = $contactPost['postal_code'][$i];
                                    $contactAddressup[$i]['house_no'] = $contactPost['house_no'][$i];
                                    $contactAddressup[$i]['address_type'] = $contactPost['address_type'][$i];
                                    $contactAddressup[$i]['referrence_uuid'] = $data['getContact']['resultSet']->contact_uuid;
                                    $contactAddressup[$i]['address_uuid'] = $contactPost['address_uuid'][$i];

                                    $contactAddressup[$i]['street'] = $contactPost['street'][$i];
                                    $contactAddressup[$i]['full_address'] = $contactPost['full_address'][$i];
                                    $contactAddressup[$i]['logitude'] = $contactPost['logitude'][$i];
                                    $contactAddressup[$i]['latitude'] = $contactPost['latitude'][$i];
                                    $contactAddressup[$i]['updated_on'] = $this->current_date;
                                    $contactAddressup[$i]['updated_by'] = $this->current_user;

                                    if ($contactPost['address_primary'] == $i) {
                                        $contactAddressup[$i]['is_primary'] = 1;
                                    } else {
                                        $contactAddressup[$i]['is_primary'] = 0;
                                    }
                                } else {
                                    $contactAddress[$i]['city'] = $contactPost['city'][$i];
                                    $contactAddress[$i]['district'] = $contactPost['district'][$i];
                                    $contactAddress[$i]['state'] = $contactPost['state'][$i];
                                    $contactAddress[$i]['country'] = $contactPost['country'][$i];
                                    $contactAddress[$i]['postal_code'] = $contactPost['postal_code'][$i];
                                    $contactAddress[$i]['house_no'] = $contactPost['house_no'][$i];
                                    $contactAddress[$i]['address_type'] = $contactPost['address_type'][$i];
                                    $contactAddress[$i]['referrence_uuid'] = $data['getContact']['resultSet']->contact_uuid;
                                    $contactAddress[$i]['street'] = $contactPost['street'][$i];
                                    $contactAddress[$i]['full_address'] = $contactPost['full_address'][$i];
                                    $contactAddress[$i]['logitude'] = $contactPost['logitude'][$i];
                                    $contactAddress[$i]['latitude'] = $contactPost['latitude'][$i];
                                    $contactAddress[$i]['created_on'] = $this->current_date;
                                    $contactAddress[$i]['created_by'] = $this->current_user;
                                    //$contactAddress[$i]['company_id'] = $this->getCompanyId;
                                    $contactAddress[$i]['address_uuid'] = generate_uuid('addr_');
                                    if ($contactPost['address_primary'] == $i) {
                                        $contactAddress[$i]['is_primary'] = 1;
                                    } else {
                                        $contactAddress[$i]['is_primary'] = 0;
                                    }
                                }
                            }
                            //pr($contactAddressup);
                            //pr($contactAddress);exit;
                            if (count($contactAddress) > 0) {
                                $resultDataAddress = $this->Model_contacts->insertAddress($contactAddress);
                            }
                            if (count($contactAddressup) > 0) {

                                $resultDataAddressup = $this->Model_contacts->updateAddress($contactAddressup);
                            }
							if($contactPost['is_primary'] !=''){
								$this->ajax_updatePrimaryContact($reference_uuid,$contact_uuid);
							}
                            $this->session->set_flashdata("success_msg", "Contact is Updated successfully ..!!");
                            redirect('client/contacts/view/' . $reference_uuid);
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/contacts/edit'. $reference_uuid.'/'.$contact_uuid);
                        }
                    } else {
                        // $data['user_type'] = $this->user_type;
                        $data['getContact'] = $this->Model_contacts->getContactsEdit($contact_uuid);
                        $data['clientName'] = $this->Model_contacts->getClientName($reference_uuid);
						if($data['clientName']['status'] == 'true'){
							$data['client_title'] = $data['clientName']['resultSet']['client_title'];
							$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						}
                        $data['getClientPhones'] = $this->Model_contacts->getClientPhones($data['getContact']['resultSet']->contact_uuid);
                        $data['getClientEmails'] = $this->Model_contacts->getClientEmails($data['getContact']['resultSet']->contact_uuid);
                        $data['getClientAddress'] = $this->Model_contacts->getClientAddress($data['getContact']['resultSet']->contact_uuid);
                        $data['primaryContact'] = $this->Model_contacts->isPrimaryContactExists();
                        $data['address_types'] = $this->Model_contacts->getAddressTypes();
                        $data['reference_uuid'] = $reference_uuid;
                        $data['file'] = 'contacts/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
                    //$data['user_type'] = $this->user_type;
                    $data['getContact'] = $this->Model_contacts->getContactsEdit($contact_uuid);
                    $data['clientName'] = $this->Model_contacts->getClientName($reference_uuid);
					if($data['clientName']['status'] == 'true'){
						$data['client_title'] = $data['clientName']['resultSet']['client_title'];
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					}
                    $data['getClientPhones'] = $this->Model_contacts->getClientPhones($data['getContact']['resultSet']->contact_uuid);
                    $data['getClientEmails'] = $this->Model_contacts->getClientEmails($data['getContact']['resultSet']->contact_uuid);
                    $data['getClientAddress'] = $this->Model_contacts->getClientAddress($data['getContact']['resultSet']->contact_uuid);
                    $data['primaryContact'] = $this->Model_contacts->isPrimaryContactExists();
                    $data['address_types'] = $this->Model_contacts->getAddressTypes();
                    $data['reference_uuid'] = $reference_uuid;
                    $data['file'] = 'contacts/update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                redirect('client/contacts');
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function ajaxPhoneRemove() {
        $phone_uuid = $_POST['phone_uuid'];
        $this->Model_contacts->deleteClientPhones($phone_uuid);
    }

    public function ajaxEmailRemove() {
        $email_uuid = $_POST['email_uuid'];
        $this->Model_contacts->deleteClientEmails($email_uuid);
    }

    public function ajaxAddressRemove() {
        $address_uuid = $_POST['address_uuid'];
        $this->Model_contacts->deleteClientAddress($address_uuid);
    }

    public function deleteContact($contactId = "") {
        $data['getContact'] = $this->Model_contacts->getContactsEdit($contactId);
        $client_uuid = $data['getContact']['resultSet']->reference_uuid;

        if ($contactId != "") {
            $deleteRole = $this->Model_contacts->isDeleteContact($contactId);
            if ($deleteRole['status'] == 'true') {
                $this->session->set_flashdata("success_msg", "Contact is deleted successfully..!!");
                redirect('client/contacts/view/' . $client_uuid);
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('client/contacts/view/' . $client_uuid);
            }
        } else {
            redirect('client/contacts/view/' . $client_uuid);
        }
    }

    public function ajax_updatePrimaryContact($reference_uuid="",$contact_uuid="") {
        //$client_uuid = $this->input->post('client_uuid');
        //$contact_uuid = $this->input->post('contact_uuid');
        $updateContactPrimary = $this->Model_contacts->updateExistingContactPrimary($reference_uuid, $contact_uuid);
        //pr($updateContactPrimary);exit;
        if ($updateContactPrimary['status'] == 'true') {
            $this->session->set_flashdata("success_msg", "Previous Contact is updated successfully..!!");
            redirect('client/contacts/view/' . $reference_uuid);
        } else {
            $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
            redirect('client/contacts/create/' . $reference_uuid);
        }
    }

    public function ajax_checkUniquePhone() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
		$phone_country_code = $this->input->post('phone_country_code');
        $deleted_field = 'is_deleted';
        $recordCount = $this->db->get_where($table, array($field => $value,'phone_country_code' =>$phone_country_code, $deleted_field => '0'))->num_rows();
        //echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

    public function ajax_checkUniqueEmail() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        $deleted_field = 'is_deleted';
        $recordCount = $this->db->get_where($table, array($field => $value, $deleted_field => '0'))->num_rows();
        //echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }

}

?>