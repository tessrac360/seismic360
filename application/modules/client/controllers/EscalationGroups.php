<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EscalationGroups extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->userId = $this->session->userdata('id');
		$this->client_id = $this->session->userdata('client_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model("Model_escalation_groups");
        //$this->load->model("client/Model_client");
        //$this->load->model("location/Model_location");
        $this->load->library('form_validation');
    }

    public function index() {
        $roleAccess = helper_fetchPermission('51', 'view');
        if ($roleAccess == 'Y') {
            $search = $this->input->get_post('sd');
			$this->load->library('pagination');
			$config['per_page'] = PAGINATION_PERPAGE;
			$config['base_url'] = base_url() . 'client/EscalationGroups/index/';
			$config['total_rows'] = $this->Model_escalation_groups->getEscalationGroups($search);
			//pr($config['total_rows']);exit;
			$config['uri_segment'] = 4;		      
			$config['display_pages'] = TRUE;
			$config['enable_query_strings'] = true;
			if($_GET){
				$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
			}		
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;      
			$data['escalation_groups'] = $this->Model_escalation_groups->getEscalationGroups($search, $config['per_page'], $page);
			//pr($data['queue']);exit;
			$data['hiddenURL'] = 'client/EscalationGroups/index/';
			//pr($data['clientTitles']);exit;
			$data['file'] = 'escalation_groups/view_form';
			$data['search'] = $search;
			$this->load->view('template/front_template', $data);
			
         } else {
            redirect('unauthorized');
        } 
    }

    public function create() {
        $roleAccess = helper_fetchPermission('51', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $escalationPost = $this->input->post();
                $this->form_validation->set_rules('escalation_group_name', 'Group Name', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $escalationPost['escalation_group_uuid'] = generate_uuid('esgr_');
                    $escalationPost = array_merge($escalationPost, $this->cData);
                    $resultData = $this->Model_escalation_groups->insertEscalationGroup($escalationPost);
                    if ($resultData['status'] == 'true') {
                        $this->session->set_flashdata("success_msg", "Escalation Group is created successfully ..!!");
                        redirect('client/EscalationGroups');
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
                        redirect('client/EscalationGroups/create');
                    }
                } else {
                    $data['file'] = 'escalation_groups/create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['file'] = 'escalation_groups/create_form';
                //pr($data['client']);
                //exit;
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        } 
    }

    public function edit($postId = NULL) {
        $roleAccess = helper_fetchPermission('51', 'edit');
        if ($roleAccess == 'Y') {
                if ($this->input->post()) {
                    $escalationPost = $this->input->post();
                    $this->form_validation->set_rules('escalation_group_name', 'Group Name', 'required');
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
						$escalationPost = array_merge($escalationPost, $this->uData);
						$updateStatus = $this->Model_escalation_groups->updateEscalationGroup($escalationPost, $postId);
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "Escalation Group is Updated successfully ..!!");
                            redirect('client/EscalationGroups');
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
                            redirect('client/EscalationGroups/edit/' . $postId);
                        }
                    } else {
						$data['getEscalationGroupByID'] = $this->Model_escalation_groups->getEscalationGroupEdit($postId);
						$data['file'] = 'escalation_groups/update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					$data['getEscalationGroupByID'] = $this->Model_escalation_groups->getEscalationGroupEdit($postId);
                    $data['file'] = 'escalation_groups/update_form';
                    $this->load->view('template/front_template', $data);
                }
            
        } else {
            redirect('unauthorized');
        }
    }

	 public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = $_POST['id'];
        if ($id != "") {
            $Status = $this->Model_escalation_groups->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                
                if ($status == 'true') {
                    $return['message'] = 'Escalation Group is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Escalation Group is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }
	
	
	public function ajax_checkUniqueGroupName() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
		$deleted_field = 'is_deleted';
        $recordCount = $this->db->get_where($table, array($field => $value, $deleted_field => '0'))->num_rows();
		//echo $this->db->last_query();exit;
        if ($recordCount != '0') {
            $status['status'] = 'true';
        } else {
            $status['status'] = 'false';
        }
        echo json_encode($status);
        exit;
    }
	
	public function deleteEscalationGroup($postId = "") {
        if ($postId != "") {
            $deleteGroup = $this->Model_escalation_groups->isDeleteEscaltionGroup($postId);
			//pr($deleteGroup);exit;
            if ($deleteGroup['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "EscalationGroup is deleted successfully..!!");
                redirect('client/EscalationGroups');
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('client/EscalationGroups');;
            }
        } else {
            redirect('client/EscalationGroups');
        }
    }
}
