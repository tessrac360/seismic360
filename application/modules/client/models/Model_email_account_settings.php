<?php

class Model_email_account_settings extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->email_settings = 'email_account_settings';
        $this->client = 'client';
        $this->devices = 'devices';
        $this->client_gateways = 'client_gateways';
        $this->tablename = 'client';
        $this->system_credentials = 'system_credentials';
        $this->login_account_types = 'login_account_types';
        $this->login_account_types = 'login_account_types';
        $this->users = 'users';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }
	
	public function insertEmailAccountSettings($postData = array()){
		$this->db->insert($this->email_settings, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id>0) {
			$return['status'] = 'true';
			$return['lastInsertId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
	
	public function isExistsEmailAccountSetting($reference_uuid) {
        $this->db->select('uuid');
        $this->db->from($this->email_settings);
        $this->db->where('reference_uuid', $reference_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function updateEmailAccountSettings($postData = array(), $reference_uuid) {
		$this->db->where('reference_uuid', $reference_uuid);
        $update_status = $this->db->update($this->email_settings, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

	
	public function getEmailSettingsEdit($reference_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->email_settings);
        $this->db->where('reference_uuid', $reference_uuid);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

	public function getClientName($client_uuid = "",$type = "") {
        $this->db->select('client_title,client_type');
        $this->db->from('client');
		if($type !=""){
			$this->db->where('id', $client_uuid);
		}else{
			$this->db->where('client_uuid', $client_uuid);
		}
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

}

?>