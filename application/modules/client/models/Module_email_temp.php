<?php

class Module_email_temp extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->setting = 'setting';
        $this->client = 'client';
        $this->macro = 'macro';
        $this->company_id = $this->session->userdata('company_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->client_id = $this->session->userdata('client_id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $this->cData = array('created_on' => $current_date, 'created_by' => $this->userId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $this->userId);
    }

    public function isCheckEmailTempExists($clientId = ""){
        $this->db->select('temp_uuid,slug');
        $this->db->from('setting');
        $this->db->where('client_private_emails', 'N');
        $this->db->where('Is_partner_access', 'Y');
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $key => $value) {
                $this->db->select('mapped_id');
                $this->db->from('client_email_template_mapping');
                $this->db->where('client_uuid', $clientId);
                $this->db->where('template_slug', $value->slug);
                //$this->db->where('temp_uuid', $value->temp_uuid);
                $queryMapping = $this->db->get();
                //echo $this->db->last_query();
                if($queryMapping->num_rows()==0){
                    $postData['temp_uuid'] = $value->temp_uuid;
                    $postData['client_uuid'] = $clientId;
                    $postData['template_slug'] = $value->slug;
                    $postData['email_action'] = 'A';
                    $postData['uuid'] =  generate_uuid('emap_');
                    $postData = array_merge($postData, $this->cData);  
                    $this->db->insert('client_email_template_mapping', $postData); 
                }
            }
        }
exit;
        
    }


    public function insertEmail($postData = array()) {       
        $postData['company_id'] = $this->getCompanyId;
        //pr($postData);exit;
        $this->db->insert($this->setting, $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateClientWiseEmail($postData = array(), $tempId) {
        $postData['temp_uuid'] =  generate_uuid('email_');
        unset($postData['isedit']);
        unset($postData['temp_uuid_user']);
       // pr($postData);exit;
        $this->db->insert($this->setting, $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $postUpdateData['is_edit'] = 'Y';
            $postUpdateData['temp_uuid'] =   $postData['temp_uuid'];
            $this->db->where('uuid', $tempId);
            $update_status = $this->db->update('client_email_template_mapping', $postUpdateData);
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateEmail($postData = array(), $tempId) {        
        unset($postData['isedit']);
        $emil_uuid = $postData['temp_uuid_user'];
        unset($postData['temp_uuid_user']);                                   
        $this->db->where('temp_uuid', $emil_uuid);
        $update_status = $this->db->update('setting', $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getSettingPagination($limit = "", $start = "") {
        $this->db->select('sett.*,CONCAT(user.first_name, " ", user.last_name) AS username');
        $this->db->from($this->setting . ' as sett');
        $this->db->join('users as user', 'user.id = sett.created_by');
        //$this->db->where('sett.company_id', $this->getCompanyId);
        $this->db->where('sett.type', '1');
        $this->db->order_by('sett.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data =$query->result();            
        } else {
            $data = array();
        }
        return $data;
    }

    public function getSettingSearchData($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {
        $this->db->select('sett.*,CONCAT(user.first_name, " ", user.last_name) AS username');
        $this->db->from($this->setting . ' as sett');
        $this->db->join('users as user', 'user.id = sett.created_by');
        $this->db->where('sett.company_id', $this->getCompanyId);
        $this->db->where('sett.type', '1');
        $this->db->order_by('sett.id', "desc");
        if ($search != "") {
             $this->db->group_start();
            $this->db->like('sett.subject', $search);
            $this->db->or_like('sett.slug', $search);
             $this->db->group_end();
        }
       
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $client = explode(',', $row->client);
                $getClientName = $this->getClientName($client);
                if ($getClientName['status'] == 'true') {
                    $row->client = $getClientName['resultSet'];
                }
                $data[] = $row;
            }
        } else {
            $data = array();
        }

        return $data;
    }

    public function getClientName($clientId = "") {
        $this->db->select('client_title');
        $this->db->from('client');
        $this->db->where_in('id', $clientId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            $rows = implode(", ", array_column($data, 'client_title'));
            $return['status'] = 'true';
            $return['resultSet'] = $rows;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getEmailSettingsEdit($postId = "") {
        $this->db->select('sett.*,tempmap.is_edit');
        $this->db->from('client_email_template_mapping as tempmap');
        $this->db->join('setting as sett', 'sett.temp_uuid = tempmap.temp_uuid');
        $this->db->where('tempmap.uuid', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            $this->db->where('id', $id);
            $this->db->update($this->setting, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

    public function getSettingSMSPagination($limit = "", $start = "") {
        $this->db->select('sett.*,CONCAT(user.first_name, " ", user.last_name) AS username');
        $this->db->from($this->setting . ' as sett');
        $this->db->join('users as user', 'user.id = sett.created_by');
        $this->db->where('sett.company_id', $this->getCompanyId);
        $this->db->where('sett.type', '2');
        $this->db->order_by('sett.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $client = explode(',', $row->client);
                $getClientName = $this->getClientName($client);
                if ($getClientName['status'] == 'true') {
                    $row->client = $getClientName['resultSet'];
                }
                $data[] = $row;
            }
        } else {
            $data = array();
        }
        //pr($data);exit;
        return $data;
    }
    
     public function getSettingSearchDataSMS($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {
        $this->db->select('sett.*,CONCAT(user.first_name, " ", user.last_name) AS username');
        $this->db->from($this->setting . ' as sett');
        $this->db->join('users as user', 'user.id = sett.created_by');
        $this->db->where('sett.company_id', $this->getCompanyId);
        $this->db->where('sett.type', '2');
        $this->db->order_by('sett.id', "desc");
        if ($search != "") {
             $this->db->group_start();
            $this->db->like('sett.subject', $search);
            $this->db->or_like('sett.slug', $search);
             $this->db->group_end();
        }
       
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $client = explode(',', $row->client);
                $getClientName = $this->getClientName($client);
                if ($getClientName['status'] == 'true') {
                    $row->client = $getClientName['resultSet'];
                }
                $data[] = $row;
            }
        } else {
            $data = array();
        }

        return $data;
    }
	
	public function getMacroInfo()
	{
		$this->db->select('*');
		$this->db->from($this->macro);
		$res = $this->db->get()->result_array();
		return $res;
	}

}

?>