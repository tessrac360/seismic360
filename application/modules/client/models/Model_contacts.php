<?php

class Model_contacts extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->contacts = 'contacts';
        $this->client = 'client';
        $this->tablename = 'client';
		$this->location = 'client_locations';
		$this->phone_numbers = 'phone_numbers';
		$this->emails = 'contact_emails';
		$this->address = 'address';
        $this->users = 'users';
		$this->address_types = 'address_types';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }
	
	public function getContactsPagination($reference_prefix = '',$reference_uuid = '',$search = '', $limit = 0, $start = 0) {
		//$client_id =array('1','2','3','4','5','6','7','8','9','10');
		if($reference_prefix == "loca"){
			$this->db->select('con.*,loc.location_name');
		}elseif($reference_prefix == "clie"){
			$this->db->select('con.*,cl.client_title');	
		}
        $this->db->from($this->contacts.' as con');
		if($reference_prefix == "loca"){
			$this->db->join($this->location.' as loc', 'loc.location_uuid = con.reference_uuid');
		}elseif($reference_prefix == "clie"){
			$this->db->join($this->client.' as cl', 'cl.client_uuid = con.reference_uuid');
		}		
		
		$this->db->where('con.reference_uuid', $reference_uuid);
		$this->db->where('con.is_deleted','0');		
		if($reference_prefix == "clie"){
			if ($search != "") {
				$this->db->like('con.contact_name', $search);
				$this->db->or_like('con.full_name', $search);
				$this->db->or_like('cl.client_title', $search);
			}		
		}elseif($reference_prefix == "loca"){
			if ($search != "") {
				$this->db->like('con.contact_name', $search);
				$this->db->or_like('con.full_name', $search);
				$this->db->or_like('loc.location_name', $search);
			}	
		}
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $gateway->result_array();
    }

	public function updateStatus($status = NULL, $contact_uuid = NULL) {
        if ($contact_uuid != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('contact_uuid', $contact_uuid);
            $this->db->update($this->contacts, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	
    public function isExitContact($contact_uuid) {
        $this->db->select('contact_uuid');
        $this->db->from($this->contacts);
        $this->db->where('contact_uuid', $contact_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function insertContacts($postData = array()){
		$this->db->insert($this->contacts, $postData);
		$rows = $this->db->affected_rows();
		//pr($rows);exit;
		/* $insert_id = $this->db->insert_id();
		pr($insert_id);exit; */
		if ($rows>0) {
			$return['status'] = 'true';
			$return['contactRows'] = $rows;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }

    public function updateContacts($postData = array(), $id) {
		$this->db->where('contact_uuid', $id);
        $update_status = $this->db->update($this->contacts, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getContactsEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->contacts);
        $this->db->where('contact_uuid', $postId);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
    
	/* public function isDeleteLocation($postId) {
        if ($postId != "") { 
			$data = array('is_deleted'=>1);
			$this->db->where('location_uuid', $postId);
            $this->db->update($this->client_locations, $data);
			//echo $this->db->last_query();exit;
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }	
    	
	
	public function isPrimaryContactExist(){
		$this->db->select('*');
        $this->db->from($this->contacts);
        $this->db->where('is_primary', 1);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->result_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
		
	} */
	
	public function getClientName($client_uuid = "") {
        $this->db->select('client_title,client_type');
        $this->db->from('client');
        $this->db->where('client_uuid', $client_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getClientPhones($contact_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->phone_numbers);
        $this->db->where('referrence_uuid', $contact_uuid);
		$this->db->where('is_deleted', '0');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
	
	public function getClientEmails($contact_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->emails);
        $this->db->where('reference_uuid', $contact_uuid);
		$this->db->where('is_deleted', '0');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
	
	public function getClientAddress($contact_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->address);
        $this->db->where('referrence_uuid', $contact_uuid);
		$this->db->where('is_deleted', '0');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
	
	public function insertPhones($data = array()) {
        $this->db->insert_batch($this->phone_numbers, $data);
		//echo $this->db->last_query();exit;
    }
	
	public function updatePhones($data = array()) {
		$this->db->update_batch($this->phone_numbers, $data, 'phone_uuid'); 
		//echo $this->db->last_query();exit;
    }

	public function insertEmails($data=array()) {
        $this->db->insert_batch($this->emails, $data);
		//echo $this->db->last_query();exit;
    }
	
	public function updateEmails($data=array()) {
        $this->db->update_batch($this->emails, $data, 'email_uuid');
		//echo $this->db->last_query();exit;
    }

	public function insertAddress($data = array()) {
        $this->db->insert_batch($this->address, $data);
		//echo $this->db->last_query();exit;
    }
	
	public function updateAddress($data = array()) {
		$this->db->update_batch($this->address, $data, 'address_uuid'); 
		//echo $this->db->last_query();exit;
    }
	
	public function deleteClientPhones($phone_uuid = NULL) {
        //$data['client'] = $client_id;
        $this->db->where('phone_uuid', $phone_uuid);
		$data = array("is_deleted"=>"1");
        $update_status = $this->db->update($this->phone_numbers, $data);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }
	
	public function deleteClientEmails($email_uuid = NULL) {
        //$data['client'] = $client_id;
        $this->db->where('email_uuid', $email_uuid);
		$data = array("is_deleted"=>"1");
        $update_status = $this->db->update($this->emails, $data);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }
	
	public function deleteClientAddress($address_uuid = NULL) {
        //$data['client'] = $client_id;
        $this->db->where('address_uuid', $address_uuid);
		$data = array("is_deleted"=>"1");
        $update_status = $this->db->update($this->address, $data);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }	
	
	public function isDeleteContact($contactId) {
        if ($contactId != "") {
            $data = array('is_deleted' => '1');
            $this->db->where('contact_uuid', $contactId);
            $this->db->update($this->contacts, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getAddressTypes($client_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->address_types);
		$this->db->where('is_deleted', '0');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
			$return['status'] = 'true';
			$return['resultSet'] = $query->result_array();
        } else {
			$return['status'] = 'false';
        }
		return $return;
    }
	
	public function isPrimaryContactExists($reference_uuid="") {
        $this->db->select('*');
        $this->db->from($this->contacts);
		$this->db->where('reference_uuid', $reference_uuid);
        $this->db->where('is_primary', 1);
        $this->db->where('is_deleted', 0);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function updateExistingContactPrimary($reference_uuid="",$contact_uuid="") {
		$postData = array("is_primary" => "0");
		$this->db->where('reference_uuid', $reference_uuid);
        $update_status = $this->db->update($this->contacts, $postData);
		//echo $this->db->last_query();
		$postDataSet = array("is_primary" => "1");
		$this->db->where('contact_uuid', $contact_uuid);
        $update_status = $this->db->update($this->contacts, $postDataSet);		
        //echo $this->db->last_query();exit; 
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
}

?>