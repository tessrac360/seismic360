<?php

class Model_system_credentials extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->contacts = 'contacts';
        $this->client = 'client';
        $this->devices = 'devices';
        $this->client_gateways = 'client_gateways';
        $this->tablename = 'client';
        $this->system_credentials = 'system_credentials';
        $this->login_account_types = 'login_account_types';
        $this->login_account_types = 'login_account_types';
        $this->users = 'users';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }
	
	public function getSysCredentials($reference_uuid = '',$search = '', $limit = 0, $start = 0) {
		$this->db->select('sys.*,log.login_account_type');	
        $this->db->from($this->system_credentials.' as sys');
        $this->db->join($this->login_account_types.' as log', 'log.credentials_uuid = sys.login_account_type');
		$this->db->where('sys.referrence_uuid', $reference_uuid);
		$this->db->where('sys.is_deleted','0');		
		$this->db->group_by('sys.credentials_uuid');		
		if ($search != "") {
			$this->db->like('log.login_account_type', $search);
			$this->db->or_like('sys.username', $search);
			$this->db->or_like('sys.port', $search);
		}		

        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $credentials = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $credentials->num_rows();
        }
        //pr($credentials->result_array());exit;
        return $credentials->result_array();
    }
	
	public function insertSysCredentials($postData = array()){
		$this->db->insert($this->system_credentials, $postData);
		$rows = $this->db->affected_rows();
		//pr($rows);exit;
		/* $insert_id = $this->db->insert_id();
		pr($insert_id);exit; */
		if ($rows>0) {
			$return['status'] = 'true';
			$return['contactRows'] = $rows;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
	
	public function isExistsSysCredential($credentials_uuid) {
        $this->db->select('credentials_uuid');
        $this->db->from($this->system_credentials);
        $this->db->where('credentials_uuid', $credentials_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function updateSysCredentials($postData = array(), $credentials_uuid) {
		$this->db->where('credentials_uuid', $credentials_uuid);
        $update_status = $this->db->update($this->system_credentials, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

	
	public function getSysCredentialsEdit($credentials_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->system_credentials);
        $this->db->where('credentials_uuid', $credentials_uuid);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

	public function updateSysCredentialStatus($status = NULL, $credentials_uuid = NULL) {
        if ($credentials_uuid != "") {
            if ($status == 'true') {
                $data = array('is_disabled' => '0');
            } else if ($status == 'false') {
                $data = array('is_disabled' => '1');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('credentials_uuid', $credentials_uuid);
            $this->db->update($this->system_credentials, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

	public function getClientName($client_uuid = "",$type = "") {
        $this->db->select('client_title,client_type');
        $this->db->from('client');
		if($type !=""){
			$this->db->where('id', $client_uuid);
		}else{
			$this->db->where('client_uuid', $client_uuid);
		}
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	public function getDeviceName($uuid = "") {
        $this->db->select('device_name,client_uuid');
        $this->db->from($this->devices);
        $this->db->where('uuid', $uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	public function getGatewayName($gateway_uuid = "") {
        $this->db->select('title,client_id');
        $this->db->from($this->client_gateways);
        $this->db->where('gateway_uuid', $gateway_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }		
	public function isDeleteSysCredential($credentials_uuid) {
        if ($credentials_uuid != "") {
            $data = array('is_deleted' => '1');
            $this->db->where('credentials_uuid', $credentials_uuid);
            $this->db->update($this->system_credentials, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function isPrimarySysCredential($reference_uuid="") {
        $this->db->select('*');
        $this->db->from($this->system_credentials);
		$this->db->where('referrence_uuid', $reference_uuid);
        $this->db->where('is_primary', 1);
        $this->db->where('is_deleted', 0);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function updateExistingPrimarySysCredential($reference_uuid="",$credentials_uuid="") {
		$postData = array("is_primary" => "0");
		$this->db->where('referrence_uuid', $reference_uuid);
        $update_status = $this->db->update($this->system_credentials, $postData);
		//echo $this->db->last_query();
		$postDataSet = array("is_primary" => "1");
		$this->db->where('credentials_uuid', $credentials_uuid);
        $update_status = $this->db->update($this->system_credentials, $postDataSet);		
     // echo $this->db->last_query();exit; 
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getLoginAccountTypes(){
		$this->db->select('*');
        $this->db->from($this->login_account_types);
        $this->db->where('is_deleted', 0);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->result_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
		
	}

}

?>