<?php

class Model_contracts extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->contacts = 'contacts';
        $this->client = 'client';
        $this->tablename = 'client';
        $this->contracts = 'contracts';
        $this->contract_types = 'contract_types';
        $this->contract_period = 'contract_period';
        $this->contract_validity = 'contract_validity';
        $this->client_holidays = 'client_holidays';
        $this->users = 'users';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

	/* Get Cleint Contracts*/
	public function getClientContracts($reference_uuid = '',$search = '', $limit = 0, $start = 0) {
		$this->db->select('clcon.*,cl.client_title,cont.contract_type,conv.validity_type');	
        $this->db->from($this->contracts.' as clcon');
		$this->db->join($this->client.' as cl', 'cl.client_uuid = clcon.reference_uuid','LEFT');
		$this->db->join($this->contract_types.' as cont', 'cont.uuid = clcon.contract_type_uuid','LEFT');
		$this->db->join($this->contract_validity.' as conv', 'conv.uuid = clcon.validity_uuid','LEFT');
		$this->db->where('clcon.reference_uuid', $reference_uuid);
		$this->db->where('clcon.is_deleted','0');
		
		if ($search != "") {
			$this->db->where('(cl.client_title LIKE \'%'.$search.'%\' OR clcon.transcation-id LIKE \'%'.$search.'%\' OR cont.contract_type LIKE \'%'.$search.'%\' OR conv.validity_type LIKE \'%'.$search.'%\')');		;;
			//$this->db->like('cl.client_title', $search);
			//$this->db->or_like('clcon.transcation-id', $search);
			//$this->db->or_like('cont.contract_type', $search);
			//$this->db->or_like('conv.validity_type', $search);
		}		

        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
		//echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $result->num_rows();
        }
        //pr($holidayList->result_array());exit;
        return $result->result_array();
    }
	/* Get Cleint Contracts*/
	
	public function insertHolidays($postData = array()){
		$this->db->insert($this->client_holidays, $postData);
		$rows = $this->db->affected_rows();
		//pr($rows);exit;
		/* $insert_id = $this->db->insert_id();
		pr($insert_id);exit; */
		if ($rows>0) {
			$return['status'] = 'true';
			$return['contactRows'] = $rows;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
	
	public function isExistsClientHolidays($client_holidays_uuid) {
        $this->db->select('client_holidays_uuid');
        $this->db->from($this->client_holidays);
        $this->db->where('client_holidays_uuid', $client_holidays_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function updateHolidays($postData = array(), $client_holidays_uuid) {
		$this->db->where('client_holidays_uuid', $client_holidays_uuid);
        $update_status = $this->db->update($this->client_holidays, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }


	public function updateHolidaysStatus($status = NULL, $client_holidays_uuid = NULL) {
        if ($client_holidays_uuid != "") {
            if ($status == 'true') {
                $data = array('is_disabled' => '1');
            } else if ($status == 'false') {
                $data = array('is_disabled' => '0');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('client_holidays_uuid', $client_holidays_uuid);
            $this->db->update($this->client_holidays, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

	
	public function isDeleteHoliday($client_holidays_uuid) {
        if ($client_holidays_uuid != "") {
            $data = array('is_deleted' => '1');
            $this->db->where('client_holidays_uuid', $client_holidays_uuid);
            $this->db->update($this->client_holidays, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	/* Get Contract Types*/
	public function getContractTypes() {
		$this->db->select('uuid,contract_type,is_default');
		$this->db->from($this->contract_types);
		$this->db->where('is_disabled','0');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	/* Get Contract Types*/
	
	/* Get Validities*/
	public function getValidities() {
		$this->db->select('*');
		$this->db->from($this->contract_validity);
		$this->db->where('is_disabled','0');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	/* Get Validities*/	
	
	/* Getting Client name*/
	public function getClientName($client_uuid = "") {
        $this->db->select('client_title');
        $this->db->from('client');
        $this->db->where('client_uuid', $client_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	/* Getting Client name*/	
	
	/* Insert Contract*/	
	public function insertContract($postData = array()){
		$this->db->insert($this->contracts, $postData);
		$rows = $this->db->affected_rows();
		//pr($rows);exit;
		$insert_id = $this->db->insert_id();
		//pr($insert_id);exit; 
		if ($rows>0) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
	/* Insert Contract*/
	
	/* Update Contract Transaction Id*/
	public function updateContractTransactionId($postData = array(), $id) {
		$this->db->where('id', $id);
        $update_status = $this->db->update($this->contracts, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	/* Update Contract  Transaction Id*/

	/* Contract Details */
	public function getContractDetails($uuid = "") {
        $this->db->select('clcon.*,cont.contract_type,conv.validity_type,conv.validity_duration,conp.contract_period,');
        $this->db->from($this->contracts.' as clcon');
		$this->db->join($this->contract_types.' as cont', 'cont.uuid = clcon.contract_type_uuid','LEFT');
		$this->db->join($this->contract_validity.' as conv', 'conv.uuid = clcon.validity_uuid','LEFT');
		$this->db->join($this->contract_period.' as conp', 'conp.uuid = clcon.contract_period_uuid','LEFT');
        $this->db->where('clcon.uuid', $uuid);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
		
        return $return;
    }
	/* Contract Details */	
	

	public function getContractActive($reference_uuid = "") {
        $this->db->select('clcon.*,cont.contract_type,conv.validity_type,conv.validity_duration');
        $this->db->from($this->contracts.' as clcon');
		$this->db->join($this->contract_types.' as cont', 'cont.uuid = clcon.contract_type_uuid','LEFT');
		$this->db->join($this->contract_validity.' as conv', 'conv.uuid = clcon.validity_uuid','LEFT');
        $this->db->where('clcon.reference_uuid', $reference_uuid);
        $this->db->where('clcon.validity_status', 'Active');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            //$return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
		
        return $return;
    }
	/* Contract Details */		

	/* Update Contract Status*/
	public function updateContractStatus($postData = array(), $uuid ="") {
		$this->db->where('uuid', $uuid);
        $update_status = $this->db->update($this->contracts, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	/* Update Contract Status*/	
	
	/* Get Contract Periods*/
	public function getContractPeriod() {
		$this->db->select('*');
		$this->db->from($this->contract_period);
		$this->db->where('is_disabled','0');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	/* Get Contract Periods*/		


	/* getValidityDuration */
	public function getValidityDuration($uuid = "") {
        $this->db->select('validity_duration');
        $this->db->from($this->contract_validity);
        $this->db->where('uuid', $uuid);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
		
        return $return;
    }
	/* getValidityDuration */	

	/* getValidityDuration */
	public function getContractPeriodDuration($uuid = "") {
        $this->db->select('contract_period');
        $this->db->from($this->contract_period);
        $this->db->where('uuid', $uuid);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
		
        return $return;
    }
	/* getValidityDuration */

	/* Update Contract Transaction Id*/
	public function updateContractBOHtopup($postData = array(), $uuid) {
		$this->db->where('uuid', $uuid);
        $update_status = $this->db->update($this->contracts, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	/* Update Contract  Transaction Id*/



	/* Get Client Contracts Active*/
	public function getClientContractsActive($reference_uuid = '') {
		$this->db->select('clcon.*,cl.client_title,cont.contract_type,conv.validity_type');	
        $this->db->from($this->contracts.' as clcon');
		$this->db->join($this->client.' as cl', 'cl.client_uuid = clcon.reference_uuid','LEFT');
		$this->db->join($this->contract_types.' as cont', 'cont.uuid = clcon.contract_type_uuid','LEFT');
		$this->db->join($this->contract_validity.' as conv', 'conv.uuid = clcon.validity_uuid','LEFT');
		$this->db->where('clcon.reference_uuid', $reference_uuid);
		$this->db->where('clcon.validity_status', 'Active');
		$this->db->where('clcon.is_deleted','0');	

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	/* Get Client Contracts Active*/	
	

	/* Set Contract Expiry*/
	public function setExpiry($uuid) {
		$postData = array("validity_status"=>"Expired");
		$this->db->where('uuid', $uuid);
        $update_status = $this->db->update($this->contracts, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	/* Set Contract Expiry*/
	
	/* Next New Contract  */
	public function getContractNew($reference_uuid = "") {
        $this->db->select('clcon.*,cont.contract_type,conv.validity_type,conv.validity_duration,conp.contract_period,');
        $this->db->from($this->contracts.' as clcon');
		$this->db->join($this->contract_types.' as cont', 'cont.uuid = clcon.contract_type_uuid','LEFT');
		$this->db->join($this->contract_validity.' as conv', 'conv.uuid = clcon.validity_uuid','LEFT');
		$this->db->join($this->contract_period.' as conp', 'conp.uuid = clcon.contract_period_uuid','LEFT');
		$this->db->where('clcon.reference_uuid',$reference_uuid);
		$this->db->where('clcon.validity_status','New');
		$this->db->limit(1);
		$this->db->order_by('id', "asc");
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
		
        return $return;
    }
	/* Next New Contract */		
	
}

?>