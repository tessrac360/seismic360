<?php

class Model_shn extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $devices, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->client = 'client';
        $current_date = $this->config->item('datetime');		
    }
	
	public function getAllClients($id){
		
        $this->db->select('id,client_title,client_type,client_uuid,client_shn_link,client_doc_url');
        $this->db->from('client');
		$this->db->where('status', 'Y');
		$this->db->where('client_uuid', $id);
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	/* public function save_ticket($data)
	{
		$this->db->insert($this->tickets,$data);
		return $this->db->insert_id();
	}*/
	
	public function updateClient($data,$con)
	{
		return $this->db->update($this->client,$data,$con);
	}
	 
}

?>