<?php

class Model_locations extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->client_locations = 'client_locations';
        $this->client = 'client';
        $this->tablename = 'client';
        $this->users = 'users';
		$this->address = 'address';
		$this->address_types = 'address_types';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function updateLocation($postData = array(), $location_uuid) {
		$this->db->where('location_uuid', $location_uuid);
        $update_status = $this->db->update($this->client_locations, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getLocationEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->client_locations);
        $this->db->where('location_uuid', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

    public function getLocationsPagination($client_id = '',$search = '', $limit = 0, $start = 0) {
		//$client_id =array('1','2','3','4','5','6','7','8','9','10');
        $this->db->select('clilo.*,cl.client_title');
        $this->db->from($this->client_locations.' as clilo');	
		$this->db->join($this->client.' as cl', 'cl.client_uuid = clilo.referrence_uuid');
		$this->db->where('clilo.referrence_uuid', $client_id);
		$this->db->where('clilo.is_deleted','0');		
		
        if ($search != "") {
            $this->db->like('clilo.location_name', $search);
			$this->db->or_like('clilo.location_short_name', $search);
			$this->db->or_like('cl.client_title', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $gateway->result_array();
    }
    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('is_disabled' => '0');
            } else if ($status == 'false') {
                $data = array('is_disabled' => '1');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('location_uuid', $id);
            $this->db->update($this->client_locations, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	public function isDeleteLocation($postId) {
        if ($postId != "") { 
			$data = array('is_deleted'=>1);
			$this->db->where('location_uuid', $postId);
            $this->db->update($this->client_locations, $data);
			//echo $this->db->last_query();exit;
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }	
    public function insertLocation($postData = array()){
		$this->db->insert($this->client_locations, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }	
	public function getClientNameuuid($referrence_uuid = "") {
        $this->db->select('client_title,client_type');
        $this->db->from('client');
        $this->db->where('client_uuid', $referrence_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
    function insertAddress($data) {
        $this->db->insert_batch($this->address, $data);
		//echo $this->db->last_query();exit;
    }
    function updateAddress($data) {
		$this->db->update_batch($this->address, $data, 'address_uuid'); 
		//echo $this->db->last_query();exit;
    }	

	public function getAddressTypes($client_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->address_types);
		$this->db->where('is_deleted', '0');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
			$return['status'] = 'true';
			$return['resultSet'] = $query->result_array();
        } else {
			$return['status'] = 'false';
        }
		return $return;
    }	
	public function getLocationAddress($client_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->address);
        $this->db->where('referrence_uuid', $client_uuid);
		$this->db->where('is_deleted', '0');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
			
            return $query->result_array();
        } else {
			
            return array();
        }
    }	
}

?>