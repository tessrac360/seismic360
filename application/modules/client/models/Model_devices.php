<?php

class Model_devices extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->client_locations = 'client_locations';
        $this->devices = 'devices';
        $this->client = 'client';
        $this->tablename = 'client';
        $this->users = 'users';
		$this->address = 'address';
		$this->client_gateways = 'client_gateways';
		$this->address_types = 'address_types';
		$this->devices_templates = 'devices_templates';
		$this->devices = 'devices';
		$this->device_categories = 'device_categories';
		$this->device_sub_categories = 'device_sub_categories';
		$this->client_device_categories = 'client_device_categories';
		$this->eventedge_service_groups = 'eventedge_service_groups';
		$this->skill_set = 'skill_set';			
		$this->client_gateways = 'client_gateways';			
		$this->grouped_services = 'grouped_services';			
		$this->services_templates = 'services_templates';			
		$this->services = 'services';			
        $current_date = $this->config->item('datetime');		
		
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

	
    public function getDevicesPagination($client_id = '',$search = '', $limit = 0, $start = 0) {
		//$client_id =array('1','2','3','4','5','6','7','8','9','10');
        $this->db->select('dev.*,cl.client_title,clg.gateway_type');
        $this->db->from($this->devices.' as dev');	
		$this->db->join($this->client_gateways.' as clg', 'clg.gateway_uuid = dev.gateway_uuid');
		$this->db->join($this->client.' as cl', 'cl.id = dev.client_id');
		$this->db->where('dev.client_id', $client_id);
		$this->db->where('dev.is_deleted','N');		
		$this->db->where('dev.device_name !=','NA');		
		
        if ($search != "") {
			$this->db->where('(dev.device_name LIKE \'%'.$search.'%\' OR dev.address LIKE \'%'.$search.'%\' OR cl.client_title LIKE \'%'.$search.'%\')');		
            //$this->db->like('dev.device_name', $search);
			//$this->db->or_like('dev.address', $search);
			//$this->db->or_like('cl.client_title', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $gateway->result_array();
    }
	

	
    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('uuid', $id);
            $this->db->update($this->devices, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	public function deleteDevice($postId) {
        if ($postId != "") { 
			$data = array('is_deleted'=>'Y');
			$this->db->where('uuid', $postId);
            $this->db->update($this->devices, $data);
			//echo $this->db->last_query();exit;
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }	


    public function insertDevice($postData = array()){
		$this->db->insert($this->devices, $postData);
		$insert_id = $this->db->insert_id();
		//echo $this->db->last_query();exit;
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
    public function updateDevice($postData = array(), $uuid) {
		$this->db->where('uuid', $uuid);
        $update_status = $this->db->update($this->devices, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }	
	
	public function getClientName($referrence_uuid = "") {
        $this->db->select('parent_client_id,client_title,client_uuid,client_type');
        $this->db->from('client');
        $this->db->where('id', $referrence_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	
	public function getDeviceDetails($postId = "") {
				
        $this->db->select('dev.*,clg.gateway_type');
        $this->db->from($this->devices.' as dev');
		$this->db->join($this->client_gateways.' as clg', 'clg.gateway_uuid = dev.gateway_uuid');
        $this->db->where('dev.uuid', $postId);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }	

	
	
	
	public function getDeviceCategories() {
		$this->db->select('id,category,device_category_uuid');
		$this->db->from($this->device_categories);
		$this->db->where('status','Y');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }

	public function getDevices($client_id = NULL) {
		$this->db->select('*');
		$this->db->from($this->devices);
		$this->db->where('client_id',$client_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	
	public function getClientLocations($client_id = NULL) {
		$this->db->select('*');
		$this->db->from($this->client_locations);
		$this->db->where('referrence_uuid',$client_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }	
	public function getDeviceTemplates() {
		$this->db->select('*');
		$this->db->from($this->devices_templates);
		//$this->db->where('client_id',$client_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }	
	public function getGateways($client_id = NULL, $parent_client_id = NULL) {
		$this->db->select('*');
		$this->db->from($this->client_gateways);
		$this->db->where('client_id',$client_id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$this->db->select('*');
			$this->db->from($this->client_gateways);
			$this->db->where('client_id',$parent_client_id);
			$query = $this->db->get();
			//echo $this->db->last_query();exit;
			if ($query->num_rows() > 0) {
				$return['status'] = true;
				$return['resultSet'] = $query->result_array();					
			} else {					
				$return['status'] = false;
				$return['msg'] = 'Records not found';
			}
		}
		//pr($return);exit;
		return $return;
    }	
	public function getPriorities() {
		$this->db->select('*');
		$this->db->from($this->skill_set);
		$this->db->where('status','Y');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	/* Getting location of gateway*/
	public function getGatewayLocation($gateway_uuid =NULL) {
		
		$this->db->select('gateway_location');
		$this->db->from($this->client_gateways);
		$this->db->where('gateway_uuid',$gateway_uuid );
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->row_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }
	/* Getting location of gateway*/	
	
	/* Getting Sub-categories of device*/
	public function getDeviceSubcategories($device_category_uuid = NULL) {
		$this->db->select('*');
		$this->db->from($this->device_sub_categories);
		$this->db->where('device_category_uuid',$device_category_uuid);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }	
	/* Getting Sub-categories of device*/
	
	/* Getting  service groups of device category*/
	public function getDeviceCategoryServiceGroups($device_category_id = NULL,$gateway_type_uuid = NULL) {
		$this->db->select('*');
		$this->db->from($this->eventedge_service_groups);
		$this->db->where('device_category_id',$device_category_id);
		$this->db->where('gateway_type_uuid',$gateway_type_uuid);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
    }	
	/* Getting  service groups of device category*/	
	
	/* */
    public function getGatewayDetails($gateway_id = "") {
        $this->db->select('id,title,ip_address,client_id,gateway_type');
        $this->db->from($this->tablename_client_gateways);
        $this->db->where('id', $gateway_id);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = true;
            $return['resultSet'] = $query->row_array();
        } else {
            $return['status'] = false;
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }	
		
	
	/* */
	
	
	/* getGatewayDevices*/
	public function getGatewayDevices($gateway_uuid = NULL) {

        $this->db->select('dev.device_name,dev.address,dev.service_group_uuid,devt.name');
        $this->db->from($this->devices.' as dev');	
		$this->db->join($this->devices_templates.' as devt', 'devt.device_template_id = dev.device_template_id');
		$this->db->where('dev.gateway_uuid', $gateway_uuid);
		$this->db->where('dev.is_deleted','N');
		$this->db->where('dev.status','Y');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
		
    }		
	/* getGatewayDevices */


	/* getGatewayDevices Templates*/
	public function getGatewayDevicesTemplate($gateway_uuid = NULL) {

        $this->db->select('devt.name,devt.selected_values as selected');
        $this->db->from($this->devices.' as dev');	
		$this->db->join($this->devices_templates.' as devt', 'devt.device_template_id = dev.device_template_id');
		$this->db->where('dev.gateway_uuid', $gateway_uuid);
		$this->db->group_by('dev.device_template_id'); 
		$this->db->where('dev.is_deleted','N');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
		
    }		
	/* getGatewayDevices  Templates*/
	
	/* getServicesAndTemplates*/
	public function getServicesGateways($servicegroups = array()) {

        $this->db->select('devs.service_description,devs.check_command_with_arg,devst.name');
        $this->db->from($this->grouped_services.' as gps');	
		$this->db->join($this->services.' as devs', 'devs.service_id = gps.service_id');
		$this->db->join($this->services_templates.' as devst', 'devst.service_template_id = gps.service_template_id');
		$this->db->where_in('gps.service_group_uuid', $servicegroups);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
		
    }		
	/* getServicesAndTemplates */
	
	/* getServicesAndTemplates*/
	public function getServicesTemplatesGateways($servicegroups = array()) {

        $this->db->select('devst.service_template_id,devst.name,devst.selected_values as selected');
        $this->db->from($this->grouped_services.' as gps');	
		$this->db->join($this->services_templates.' as devst', 'devst.service_template_id = gps.service_template_id');	
		$this->db->where_in('gps.service_group_uuid', $servicegroups);
		$this->db->group_by('gps.service_template_id'); 
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			$return['status'] = true;
			$return['resultSet'] = $query->result_array();
		} else {
			$return['status'] = false;
			$return['msg'] = 'Records not found';
		}
		//pr($return);exit;
		return $return;
		
    }		
	/* getServicesAndTemplates */	
}

?>