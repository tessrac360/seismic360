<?php

class Model_holidays extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->contacts = 'contacts';
        $this->client = 'client';
        $this->tablename = 'client';
        $this->client_holidays = 'client_holidays';
        $this->users = 'users';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }
	
	public function getClientHolidays($reference_uuid = '',$search = '', $limit = 0, $start = 0) {
		$this->db->select('clhol.*,cl.client_title');	
        $this->db->from($this->client_holidays.' as clhol');
		$this->db->join($this->client.' as cl', 'cl.client_uuid = clhol.client_uuid');
		$this->db->where('clhol.client_uuid', $reference_uuid);
		$this->db->where('clhol.is_deleted','0');		
		$this->db->group_by('clhol.client_holidays_uuid');		
		if ($search != "") {
			$this->db->like('cl.client_title', $search);
			$this->db->or_like('clhol.holiday_year', $search);
			$this->db->or_like('clhol.holiday_date', $search);
			$this->db->or_like('clhol.holiday_occasion', $search);
		}		

        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $holidayList = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $holidayList->num_rows();
        }
        //pr($holidayList->result_array());exit;
        return $holidayList->result_array();
    }
	
	public function insertHolidays($postData = array()){
		$this->db->insert($this->client_holidays, $postData);
		$rows = $this->db->affected_rows();
		//pr($rows);exit;
		/* $insert_id = $this->db->insert_id();
		pr($insert_id);exit; */
		if ($rows>0) {
			$return['status'] = 'true';
			$return['contactRows'] = $rows;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
	
	public function isExistsClientHolidays($client_holidays_uuid) {
        $this->db->select('client_holidays_uuid');
        $this->db->from($this->client_holidays);
        $this->db->where('client_holidays_uuid', $client_holidays_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function updateHolidays($postData = array(), $client_holidays_uuid) {
		$this->db->where('client_holidays_uuid', $client_holidays_uuid);
        $update_status = $this->db->update($this->client_holidays, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

	
	public function getHolidaysEdit($client_holidays_uuid = "") {
        $this->db->select('*');
        $this->db->from($this->client_holidays);
        $this->db->where('client_holidays_uuid', $client_holidays_uuid);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

	public function updateHolidaysStatus($status = NULL, $client_holidays_uuid = NULL) {
        if ($client_holidays_uuid != "") {
            if ($status == 'true') {
                $data = array('is_disabled' => '0');
            } else if ($status == 'false') {
                $data = array('is_disabled' => '1');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('client_holidays_uuid', $client_holidays_uuid);
            $this->db->update($this->client_holidays, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

	public function getClientName($client_uuid = "") {
        $this->db->select('client_title,client_type');
        $this->db->from('client');
        $this->db->where('client_uuid', $client_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function isDeleteHoliday($client_holidays_uuid) {
        if ($client_holidays_uuid != "") {
            $data = array('is_deleted' => '1');
            $this->db->where('client_holidays_uuid', $client_holidays_uuid);
            $this->db->update($this->client_holidays, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

}

?>