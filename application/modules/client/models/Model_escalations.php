<?php

class Model_escalations extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->contacts = 'contacts';
        $this->client = 'client';
        $this->tablename = 'client';
        $this->client_escalations = 'client_escalations';
        $this->escalations_assign_group = 'escalations_assign_group';
        $this->escalation_groups = 'escalation_groups';
		$this->location = 'client_locations';
		$this->phone_numbers = 'phone_numbers';
		$this->emails = 'contact_emails';
		$this->address = 'address';
        $this->users = 'users';
		$this->address_types = 'address_types';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }
	
	public function getEscalationsAssignGroup($reference_uuid = '',$search = '', $limit = 0, $start = 0) {
		$this->db->select('eag.*,cl.client_title,eg.escalation_group_name');	
        $this->db->from($this->escalations_assign_group.' as eag');
		$this->db->join($this->client.' as cl', 'cl.client_uuid = eag.reference_uuid');
		$this->db->join($this->escalation_groups.' as eg', 'eg.escalation_group_uuid = eag.escalation_group_uuid');
		//$this->db->join($this->contacts.' as con', 'con.contact_uuid = ce.contact_uuid');
		$this->db->where('eag.reference_uuid', $reference_uuid);
		$this->db->where('eag.is_deleted','0');		
		//$this->db->group_by('ce.escaltion_group_uuid');		
		if ($search != "") {
			$this->db->like('cl.client_title', $search);
			$this->db->or_like('eg.escalation_group_name', $search);
			//$this->db->or_like('con.contact_name', $search);
		}		

        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $assignGroup = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $assignGroup->num_rows();
        }
        //pr($assignGroup->result_array());exit;
        return $assignGroup->result_array();
    }
	
	public function getEscalaltionGroups(){
		$this->db->select('*');
        $this->db->from($this->escalation_groups);
        //$this->db->where('contact_uuid', $postId);
		$this->db->where('is_deleted','0');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
		
	}
	
	public function getContactGroups($postId){
		$this->db->select('*');
        $this->db->from($this->contacts);
        $this->db->where('reference_uuid', $postId);
		$this->db->where('is_deleted','0');
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
		
	}

	public function updateAssignGroupStatus($status = NULL, $escalations_assign_group_uuid = NULL) {
        if ($escalations_assign_group_uuid != "") {
            if ($status == 'true') {
                $data = array('is_disabled' => '1');
            } else if ($status == 'false') {
                $data = array('is_disabled' => '0');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('escalations_assign_group_uuid', $escalations_assign_group_uuid);
            $this->db->update($this->escalations_assign_group, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	
	public function updateEscalationStatus($status = NULL, $escalation_group_uuid = NULL) {
        if ($escalation_group_uuid != "") {
            if ($status == 'true') {
                $data = array('is_disabled' => '1');
            } else if ($status == 'false') {
                $data = array('is_disabled' => '0');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('escalation_group_uuid', $escalation_group_uuid);
            $this->db->update($this->client_escalations, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	
    public function isExistsEscalationGroupAssign($escalations_assign_group_uuid) {
        $this->db->select('escalations_assign_group_uuid');
        $this->db->from($this->escalations_assign_group);
        $this->db->where('escalations_assign_group_uuid', $escalations_assign_group_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function insertAssignGroup($postData = array()){
		$this->db->insert($this->escalations_assign_group, $postData);
		$rows = $this->db->affected_rows();
		//pr($rows);exit;
		/* $insert_id = $this->db->insert_id();
		pr($insert_id);exit; */
		if ($rows>0) {
			$return['status'] = 'true';
			$return['contactRows'] = $rows;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
	
	public function insertEscalations($postData = array()){
		$this->db->insert($this->client_escalations, $postData);
		$rows = $this->db->affected_rows();
		//pr($rows);exit;
		/* $insert_id = $this->db->insert_id();
		pr($insert_id);exit; */
		if ($rows>0) {
			$return['status'] = 'true';
			$return['contactRows'] = $rows;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }

    public function getEscalationsAssignGroupEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->escalations_assign_group);
        $this->db->where('escalations_assign_group_uuid', $postId);
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

	public function getClientName($client_uuid = "") {
        $this->db->select('client_title,client_type');
        $this->db->from('client');
        $this->db->where('client_uuid', $client_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

	public function getEscalationGroupName($escalation_group_uuid) {
        $this->db->select('*');
		$this->db->where('escalation_group_uuid',$escalation_group_uuid);
        $query = $this->db->get($this->escalation_groups);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    } 
	
	public function getContactGroupsIds($reference_uuid,$escalation_group_uuid) {
        $this->db->select('contact_uuid');
		$this->db->where('reference_uuid', $reference_uuid);
		$this->db->where('escalation_group_uuid', $escalation_group_uuid);
		$this->db->where('is_deleted','0');
        $query = $this->db->get($this->client_escalations);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            //echo "ddddddd";
            $data = $query->result_array();
            $rows = implode(",", array_column($data, 'contact_uuid'));
            $data = explode(',', $rows);
        } else {
            $data = array();
        }

        return $data;
    }
	
	public function deleteContactGroupsIds($postId) {
        $this->db->where('escalation_group_uuid', $postId);
        $this->db->where('is_deleted', 0);
        $this->db->delete($this->client_escalations);
    }

	
	public function isDeleteEscalationGroup($escalation_group_uuid) {
        if ($escalation_group_uuid != "") {
            $data = array('is_deleted' => '1');
            $this->db->where('escalation_group_uuid', $escalation_group_uuid);
            $this->db->update($this->client_escalations, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function isDeleteAssignGroup($escalations_assign_group_uuid) {
        if ($escalations_assign_group_uuid != "") {
            $data = array('is_deleted' => '1');
            $this->db->where('escalations_assign_group_uuid', $escalations_assign_group_uuid);
            $this->db->update($this->escalations_assign_group, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getEscalationGroupsIds($reference_uuid="") {
        $this->db->select('escalation_group_uuid');
        $this->db->from($this->escalations_assign_group);
		$this->db->where('reference_uuid', $reference_uuid);
        $this->db->where('is_deleted', 0);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
			$data = $query->result_array();
            $rows = implode(",", array_column($data, 'escalation_group_uuid'));
            $data = explode(',', $rows);

        } else {
            $data = array();
        }
        return $data;

    }

}

?>