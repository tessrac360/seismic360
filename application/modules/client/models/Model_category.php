<?php

class Model_category extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    private $finall;

    public function __construct() {
        parent::__construct();
        $this->tablename = 'client';
        $this->users = 'users';
        $this->temp_role = 'temp_role';
        $this->temp_role_permission = 'temp_role_permission';
        $this->role = 'role';
        $this->role_permission = 'role_permission';
        $this->master_columns = 'event_column';
        $this->rename_columns = 'event_column_rename';
        $this->company_id = $this->session->userdata('company_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->device_categories = 'device_categories';
        $this->device_sub_categories = 'device_sub_categories';
    }

    public function getAssignedGroup() {
        $this->db->select('a_id,group_name');
        $this->db->from('assign_group');
        $this->db->where('active_status', 'Y');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function createDeviceCategory() {
        $catpost = $this->input->post();
        $catpost['device_category_uuid'] = generate_uuid('dcat_');
        $catpost = array_merge($catpost, $this->cData);
        if ($this->db->insert($this->device_categories, $catpost)) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getDeviceCategory($search = '', $limit = 0, $start = 0) {
        $this->db->select("devcat.id,devcat.category,devcat.status,");
        $this->db->from('device_categories as devcat');
        if ($search != "") {            
            $this->db->or_like('devcat.category', $search);
        }
        $this->db->order_by('devcat.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($limit == 0 && $start == 0) {
            return $query->num_rows();
        }
        return $query->result_array();
    }

    public function fetchDeviceCategoryByID($devId = "") {
        $this->db->select('*');
        $this->db->from($this->device_categories);

        $this->db->where('id', $devId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->row();
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateDeviceCategory($divcatId = "") {
        $this->db->where('id', $divcatId);
        $catpost = $this->input->post();
        $catpost = array_merge($catpost, $this->uData);
        $update_status = $this->db->update($this->device_categories, $catpost);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function updateStatusCategory($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('id', $id);
            $this->db->update($this->device_categories, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

    /* Device sub category Start */

    public function getCategoryList() {
        $this->db->select('id,device_category_uuid,category');
        $this->db->from($this->device_categories);
        $this->db->where('status', 'Y');
        $this->db->where('device_category_uuid !=', '');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function createDeviceSubCategory() {
        $catpost = $this->input->post();
        $catid = explode('#', $catpost['device_sub_category_id']);
        $catpost['device_category_uuid'] = $catid[1];
        $catpost['device_category_id'] = $catid[0];
        unset($catpost['device_sub_category_id']);
        $catpost['uuid'] = generate_uuid('dsca_');
        $catpost = array_merge($catpost, $this->cData);

        if ($this->db->insert($this->device_sub_categories, $catpost)) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getDeviceSubCategory($search = '', $limit = 0, $start = 0) {
        $this->db->select("devsubcat.id,devsubcat.subcategory,devsubcat.status,devcat.category");
        $this->db->from('device_sub_categories as devsubcat');
        $this->db->join('device_categories as devcat', 'devcat.id = devsubcat.device_category_id');
        if ($search != "") {
            $this->db->like('devsubcat.subcategory', $search);
            $this->db->or_like('devcat.category', $search);
        }
        $this->db->order_by('devsubcat.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($limit == 0 && $start == 0) {
            return $query->num_rows();
        }
        return $query->result_array();
    }

    public function fetchDeviceSubCategoryByID($devId = "") {
        $this->db->select('*');
        $this->db->from($this->device_sub_categories);
        $this->db->where('id', $devId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->row();
            $return['status'] = 'true';
            $return['resultSet'] = $data;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateDeviceSubCategory($divcatId = "") {
        $this->db->where('id', $divcatId);
        $catpost = $this->input->post();
        $catid = explode('#', $catpost['device_sub_category_id']);
        $catpost['device_category_uuid'] = $catid[1];
        $catpost['device_category_id'] = $catid[0];
        unset($catpost['device_sub_category_id']);
        
       // pr($catpost);exit;
        
        $catpost = array_merge($catpost, $this->uData);
        $update_status = $this->db->update($this->device_sub_categories, $catpost);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function updateStatusSubCategory($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('id', $id);
            $this->db->update($this->device_sub_categories, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

}

?>