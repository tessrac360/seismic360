<?php

class Model_client extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'client';
        $this->users = 'users';
        $this->company_id = $this->session->userdata('company_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function getActiveClient() {
        //pr($_SESSION);
        $this->db->select('client');
        $this->db->from($this->users);
        $this->db->where('active_status', 'Y');
        $query = $this->db->get();
//        echo $this->db->last_query();
//        exit;
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            $rows = implode(",", array_column($data, 'id'));
            $return['status'] = 'true';
            $return['resultSet'] = $rows;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            $this->db->where('id', $id);
            $this->db->update($this->tablename, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

    public function getAllClientId() {
        $this->db->select('id');
        $this->db->from($this->tablename);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            $rows = implode(",", array_column($data, 'id'));
            $return['status'] = 'true';
            $return['resultSet'] = $rows;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function isExitCleint($id) {
        $this->db->select('id');
        $this->db->from($this->tablename);
        $this->db->where('id', $id);
        if ($this->company_id != 0) {
            $this->db->where('company_id', $this->getCompanyId);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    private function updateClientIDStatusY($clientID = "") {
        $data['client'] = $clientID;
        $this->db->where('chkallclient', 'Y');
        $this->db->update('users', $data);
    }

    public function insertClient($postData = array()) {
        $postData['parent_client_id'] = $this->session->userdata('client_id');
        $postData['role_id'] = $this->roleId;
        $this->db->insert($this->tablename, $postData);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $return['status'] = 'true';
            $return['lastId'] = $insert_id;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateClient($postData = array(), $id) {
        $this->db->where('id', $id);
        $update_status = $this->db->update($this->tablename, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    public function getClientPagination($limit = "", $start = "") {
        $this->db->select('cl.*, CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from($this->tablename.' as cl');
        $this->db->join('users as usr', 'usr.id = cl.created_by');        
        $this->db->where('cl.company_id', $this->getCompanyId);
        $this->db->where('cl.parent_client_id', $this->session->userdata('client_id'));
        $this->db->order_by('cl.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        } //pr($data);exit;
        return $data;
        
       
    }

    /*
     * Objective: Pagination of Role moudle with Searching
     * parameters: Null
     * Return: Null
     */

    public function getUserSearchData($search, $limit = "", $start = "", $order_by = "id desc", $order_type = "") {
        $this->db->select('cl.*, CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from($this->tablename.' as cl');
        $this->db->join('users as usr', 'usr.id = cl.created_by');        
        $this->db->where('cl.company_id', $this->getCompanyId);
        $this->db->where('cl.parent_client_id', $this->session->userdata('client_id'));
        if ($search != "") {
            $this->db->like('cl.client_title', $search);
        }
        $this->db->order_by('cl.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getClientEdit($postId = "") {
        $this->db->select('id,client_title');
        $this->db->from($this->tablename);
        $this->db->where('id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

    public function getClientTreeView($parent_id = "", $access = 0) { // 1:top level 0:low level
        $this->db->select('id,client_title,parent_client_id');
        $this->db->from($this->tablename);
        if ($access == 1) {
            $this->db->where('id', $parent_id);
        } else {
            $this->db->where('parent_client_id', $parent_id);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

}

?>