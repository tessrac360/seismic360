<?php

class Model_contract_period extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'contract_period';
        $this->company_id = $this->session->userdata('company_id');
        $this->client_id = $this->session->userdata('client_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function updateStatus($status = NULL, $id = NULL) {
        //echo $status .' ---- '.$id;//exit;
        if ($id != "") {
            if ($status == 'true') {
                $data = array('is_disabled' => '0');
            } else if ($status == 'false') {
                $data = array('is_disabled' => '1');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('uuid', $id);
            $this->db->update($this->tablename, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

	public function getContractValidations($search = '', $limit = 0, $start = 0) {
        $this->db->select("cv.*,CONCAT_WS(' ', uc.first_name,uc.last_name) as username");
        $this->db->from('contract_period as cv');
       // $this->db->join('client as cl','cl.id = eg.client_id');
        $this->db->join('users as uc','uc.id = cv.created_by');
		$this->db->where('cv.is_deleted','0');
		$this->db->order_by('cv.uuid', "desc");
		if ($search != "") {
            $this->db->like('cv.contract_period_type', $search);
            $this->db->or_like('cv.contract_period', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        if ($limit == 0 && $start == 0) {
            return $query->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $query->result_array();
    }

    public function insertContractPeriod($postData = array()) {
        $this->db->insert('contract_period', $postData);
        $rows = $this->db->affected_rows();
		//pr($rows);exit;
		/* $insert_id = $this->db->insert_id();
		pr($insert_id);exit; */
		if ($rows>0) {
			$return['status'] = 'true';
			$return['rowsInserted'] = $rows;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }

    public function updateContractPeriod($postData = array(), $id) {
        $this->db->where('uuid', $id);
        $update_status = $this->db->update($this->tablename, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }
	
	
	public function getContractValidityEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $this->db->where('uuid', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
	
	public function isDeleteContractValidity($postId = "") {
		$this->db->where('uuid', $postId);
		$data = array("is_deleted"=>"1");
        $update_status = $this->db->update($this->tablename, $data);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }
	
	public function isDefaultContractPeriod() {
        $this->db->select('*');
        $this->db->from($this->tablename);
		//$this->db->where('reference_uuid', $reference_uuid);
        $this->db->where('is_default', 1);
        $this->db->where('is_deleted', 0);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function updateExistingDefaultContract($uuid="") {
		$postData = array("is_default" => "0");
        $update_status = $this->db->update($this->tablename, $postData);
		//echo $this->db->last_query();
		$postDataSet = array("is_default" => "1");
		$this->db->where('uuid', $uuid);
        $update_status = $this->db->update($this->tablename, $postDataSet);		
        //echo $this->db->last_query();exit; 
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    
}

?>