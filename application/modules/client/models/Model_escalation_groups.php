<?php

class Model_escalation_groups extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $company_id, $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'escalation_groups';
        $this->company_id = $this->session->userdata('company_id');
        $this->client_id = $this->session->userdata('client_id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
        $this->roleId = $this->session->userdata('role_id');
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function updateStatus($status = NULL, $id = NULL) {
        //echo $status .' ---- '.$id;//exit;
        if ($id != "") {
            if ($status == 'true') {
                $data = array('is_disabled' => '1');
            } else if ($status == 'false') {
                $data = array('is_disabled' => '0');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('escalation_group_uuid', $id);
            $this->db->update($this->tablename, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

	public function getEscalationGroups($search = '', $limit = 0, $start = 0) {
        $this->db->select("eg.*,CONCAT_WS(' ', uc.first_name,uc.last_name) as username");
        $this->db->from('escalation_groups as eg');
       // $this->db->join('client as cl','cl.id = eg.client_id');
        $this->db->join('users as uc','uc.id = eg.created_by','left');
		$this->db->where('eg.is_deleted','0');
		$this->db->order_by('eg.escalation_group_uuid', "desc");
		if ($search != "") {
            $this->db->like('eg.escalation_group_name', $search);
        }		
        //$this->db->order_by('id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($limit == 0 && $start == 0) {
            return $query->num_rows();
        }
        //pr($gateway->result_array());exit;
        return $query->result_array();
    }

    public function insertEscalationGroup($postData = array()) {
        $this->db->insert('escalation_groups', $postData);
        $rows = $this->db->affected_rows();
		//pr($rows);exit;
		/* $insert_id = $this->db->insert_id();
		pr($insert_id);exit; */
		if ($rows>0) {
			$return['status'] = 'true';
			$return['rowsInserted'] = $rows;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }

    public function updateEscalationGroup($postData = array(), $id) {
        $this->db->where('escalation_group_uuid', $id);
        $update_status = $this->db->update($this->tablename, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }
	
	
	public function getEscalationGroupEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $this->db->where('escalation_group_uuid', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }
	
	public function isDeleteEscaltionGroup($postId = "") {
		$this->db->where('escalation_group_uuid', $postId);
		$data = array("is_deleted"=>"1");
        $update_status = $this->db->update($this->tablename, $data);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }

        return $return;
    }

    
}

?>