<?php

class Model_gateway extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->gateway = 'client_gateways';
        $this->tablename = 'client';
        $this->users = 'users';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    public function isExitGateway($id) {
        $this->db->select('id');
        $this->db->from($this->gateway);
        $this->db->where('id', $id);
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function insertGateway($postData = array()){
		$this->db->insert($this->gateway, $postData);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }

    public function updateGateway($postData = array(), $id) {
		$this->db->where('id', $id);
        $update_status = $this->db->update($this->gateway, $postData);
//        echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getGatewayPagination($search = '', $client_id = 0, $limit = 0, $start = 0) {
        $this->db->select('g.*, CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from($this->gateway . ' as g');
        $this->db->join('users as usr', 'usr.id = g.created_by');
        $this->db->where('g.client_id', $client_id);
		$this->db->where('g.is_deleted', 'N');
        $this->db->order_by('g.title', "asc");
        if ($start >= 0 and $limit > 0 and $search == '') {
            $this->db->limit($limit, $start);
        }
        $gateway = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $gateway->num_rows();
        }
       //pr($allClients);exit;
        return $gateway->result_array();
    }


    public function getGatewaySearchData($search, $client_id = "",$limit = "", $start = "", $order_by = "id desc", $order_type = "") {
        $this->db->select('g.*, CONCAT(usr.first_name, " ", usr.last_name) AS name');
        $this->db->from($this->gateway . ' as g');
        $this->db->join('users as usr', 'usr.id = g.created_by');
        $this->db->where('g.client_id', $client_id);
        if ($search != "") {
            $this->db->like('g.title', $search);
        }
        $this->db->order_by('g.id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        } else {
            $data = array();
        }
		//pr($data);exit;

        return $data;
    }

    public function getGatewayEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->gateway);
        $this->db->where('id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

	public function isDeleteGateway($postId) {
        if ($postId != "") {
            $data = array('is_deleted' => 'Y');
            $this->db->where('id', $postId);
            $this->db->update($this->gateway, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('id', $id);
            $this->db->update($this->gateway, $data);
            //echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	public function getClientName($clientId = "") {
        $this->db->select('client_title');
        $this->db->from('client');
        $this->db->where('id', $clientId);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }	
}

?>