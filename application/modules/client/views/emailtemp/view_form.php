<?php
$AccessAdd = helper_fetchPermission('54', 'add');
$AccessEdit = helper_fetchPermission('54', 'edit');
$AccessStatus = helper_fetchPermission('54', 'active');
?>

<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Email Template Settings</span>
                </div>  
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Settings</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo base_url('settings') ?>">Email Template</a>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>List</span>                            
                        </li>
                    </ul>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-9">
                            <?php if ($AccessAdd == 'Y') { ?> 
                                <div class="btn-group">
                                    <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('settings/create') ?>"> Add Email Template
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>

                     
                    </div>
                </div>
                <table class="table" id="table">
                    <thead>
                        <tr>                            
                            <th> Subject </th>                           
                            <th> Slug Key </th>
                         
                                <th> Status </th>
                          
                           
                                <th> Action </th>
                           

                        </tr>
                    </thead>
      
                   
                </table>
            </div>           
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //alert("hiii");
        var url_string = document.URL;
        var url = new URL(url_string);
        var partner_id = url.searchParams.get("cli");  
        var table = $('#table').DataTable({
            "language": {
                "infoFiltered": ""
            },
            "pageLength": 50,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('client/email_temp/ajax_emailtemp') ?>",
                "type": "POST",
                "data": function (data) {  
                    data.partner_id = partner_id;                  
                }
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [1, 6, 7, 8], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
        });



    });
</script>
