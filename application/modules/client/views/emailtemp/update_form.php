<?php
//pr($getEmailSettings); 
$getResult = $getEmailSettings['resultSet'];
//pr($getEmailSettings);
?>

<!-- END PAGE HEADER-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Edit Email Template</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>settings</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo base_url('settings') ?>">Email Template</a>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>Edit</span>                            
                        </li>
                    </ul>                                                        
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" name="frmSettings" id="frmSettings" method="post" action="" autocomplete="off" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                           
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input name="slug" type="hidden" value="<?php echo ($getResult->slug) ? $getResult->slug : ''; ?>">
                                    <input name="isedit"  type="hidden" value="<?php echo ($getResult->is_edit) ? $getResult->is_edit : ''; ?>">
                                    <input name="temp_uuid_user"  type="hidden" value="<?php echo ($getResult->temp_uuid) ? $getResult->temp_uuid : ''; ?>">
                                    <input class="form-control" id="template_name" name="template_name" value="<?php echo ($getResult->subject) ? $getResult->template_name : ''; ?>" type="text">
                                    <label for="form_control_1">Template Name</label>
<!--                                    <span class="help-block">Enter Template Name...</span>-->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="subject" value="<?php echo ($getResult->subject) ? $getResult->subject : ''; ?>" name="subject" type="text">
                                    <label for="form_control_1">Subject<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
                            <div class="col-md-12">
                                <?php
                                $name = "";
                                foreach ($clientMacro as $macro) {
                                    $name .= "<strong>" . $macro["slug"] . ": " . "</strong>" . $macro["description"] . "<br/>";
                                }
                                ?>
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <label for="form_control_1">Mail Content
                                        <a class="btn btn-qstn pop" data-container="body" data-toggle="popover" data-placement="right" data-content="<?php echo $name; ?>"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                    <textarea name="content" id="content"><?php echo ($getResult->content) ? $getResult->content : ''; ?> </textarea>

                                </div>
                            </div>

                        </div>
                        <div class="form-actions noborder">                                             
                            <button type="submit" class="btn green">Edit & Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('settings'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() . "public/" ?>js/form/form_settings.js" type="text/javascript"></script>