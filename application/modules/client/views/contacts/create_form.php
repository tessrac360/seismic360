
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>css/intlTelInput.css"/>
<?php 
//pr($address_types);exit;

$address_types_select = '<select class="form-control" id="address_type" name="address_type[]">';
if ($address_types['status'] == 'true') {
	$address_types_select.='<option value=""></option>';
		
	foreach ($address_types['resultSet'] as $value) {
		
		$address_types_select.='<option value="'.$value['address_type_uuid'].'">'.$value['address_type'].'</option>';  

	}
}				   
$address_types_select.='</select>';
//echo $address_types_select;exit;	
?>

<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Contacts</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if(isset($client_type) && !empty($client_type) && $client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>
					<li>
						<a href="<?php echo base_url('client/contacts/view/'.$reference_uuid) ?>">Contacts</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light  padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmContacts" id="frmContacts" method="post" action="">
                    <div class="form-body">
					<!--<h4 class="form-section">Person Info</h4>-->
                        <div class="row"> 
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="first_name" name="first_name" type="text" data-validation="required" data-validation-error-msg="Please enter first name" autocomplete="off">
                                    <label for="form_control_1">First Name<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="second_name" name="second_name" type="text">
                                    <label for="form_control_1">Middle Name</label>                                               
                                </div>
                            </div>
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="last_name" name="last_name" type="text" data-validation="required" data-validation-error-msg="Please enter last name" autocomplete="off">
                                    <label for="form_control_1">Last Name<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control edited" id="full_name" name="full_name" type="hidden">
                                    <input class="form-control" id="contact_name" name="contact_name" type="text" data-validation="required" data-validation-error-msg="Please enter contact name">
                                    <label for="form_control_1">Contact Name<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
							
							<div class="col-md-3">
								<div>					
									<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
									<div class="controls ">
										<div class="form-group form-md-line-input form-md-floating-label" style="padding-top:5px;">
											<label class="control-label col-md-4">Gender</label>
											<label class="radio-inline">
												<input type="radio" name="sex" value="Male"/><span></span> Male
											</label> 
											<label class="radio-inline">
												<input type="radio" name="sex" value="Female"/><span></span>  Female
											</label> 
											<label class="radio-inline">
												<input type="radio" name="sex" value="NA"/> <span></span> NA
											</label> 
										</div>
									</div>
								</div>
                            </div>
							
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  type="text" class="form-control date-picker dob" id="dob" name="dob" value="" readonly>
                                    <label for="form_control_1">Date of Birth</label>                                 
                                </div>
                            </div>
							<?php if($primaryContact['status']== 'true'){?>
							<div class="col-md-3">
								<div id="div_id_stock_1_service">					
									<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
									<div class="controls ">
										<div class="radio">
											<label><input type="radio" class="isPrimaryContact" name="is_primary" value="1"/>Is Primary</label>
										</div> 
									</div>
								</div>
							</div>
							<?php }else{?>
							<div class="col-md-3">
								<div id="div_id_stock_1_service">					
									<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
									<div class="controls ">
										<div class="radio">
											<label><input type="radio" class="is_primary" name="is_primary" value="1" checked="checked" />Is Primary</label>
										</div> 
									</div>
								</div>
							</div>
							<?php } ?>
                        </div>
						
						<div class="panel-group">
							<div class="panel panel-default custom-panel">
								<div class="panel-heading client-panel-heading">
									<h3 class="sub-head">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
											<span>Phones</span>
											<i class="more-less glyphicon glyphicon-minus pull-right"></i>
										</a>
									</h3>
								</div>
								<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
									<div class="panel-body">
										<div class="phone_input_fields_wrap wrapping">
											<div class="phonecountdivs">
												<div class="fieldRow clearfix">
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input type="hidden" name="phone_country_code[]" class ="phone_country_code" value="1" />
															<input class="form-control phone_number" id="phone_number" name="phone_number[]" type="text" data-validation="number" data-validation-error-msg="Please enter only numeric values" tableName="phone_numbers" tableField="phone_number" />
															<label for="form_control_1" class="label-phone-number">Phone Number <span class="required" aria-required="true">*</span></label>                                     
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control" id="phone_area_code" 
															name="phone_area_code[]" type="text">
															<label for="form_control_1">Phone Area Code</label>                                               
														</div>
													</div>
													
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<select class="form-control edited" name="pn_category[]" id="	pn_category">
																<option value="work">Work</option>
																<option value="home">Home</option>
																<option value="personal">Personal</option>
															</select>
															<label for="form_control_1">Phone Category<span class="required" aria-required="true">*</span></label>
														</div>
													</div>	
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<select class="form-control edited" name="pn_type[]" id="pn_type">
																<option value="Landline">Landline</option>
																<option value="Mobile">Mobile</option>
																<option value="SIP">SIP</option>
															</select>
															<label for="form_control_1">Phone Type<span class="required" aria-required="true">*</span></label>
														</div>
													</div>	
													<div class="col-md-2">
														<div id="div_id_stock_1_service">					
															<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
															<div class="controls ">
																<div class="radio">
																	<label><input type="radio" class="phone_primary" name="phone_primary" value="0" data-validation="required" data-validation-error-msg="Please select a primary phone" />Is Primary <span class="required" aria-required="true">*</span></label>
																</div> 
															</div>
														</div>
													</div>
												</div>
											</div>	
										</div>
									</div>
									<div class="panel-footer">
										<button class="add_phone_field_button btn blue btn-outline mt-repeater-add pull-right">Add More Phones</button>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="panel-group">
							<div class="panel panel-default custom-panel">
								<div class="panel-heading client-panel-heading">
									<h3 class="sub-head">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel2" aria-expanded="true">
											<span>Emails</span> <i class="more-less glyphicon glyphicon-minus pull-right"></i>
										</a>
									</h3>
								</div>
								<div id="panel2" class="panel-collapse collapse in" aria-expanded="true" style="">
									<div class="panel-body">
										<div class="email_input_fields_wrap wrapping">
											<div class="emailcountdivs">
												<div class="fieldRow clearfix">
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control email_address" id="email_address" name="email_address[]" type="text" data-validation="email" data-validation-error-msg="Please enter valid email" tableName="contact_emails" tableField="email_address">
															<label for="form_control_1">Email Address<span class="required" aria-required="true">*</span></label>                                               
														</div>
													</div>
													
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<select class="form-control edited" name="email_category[]" id="email_category">
																<option value="work">Work</option>
																<option value="home">Home</option>
																<option value="personal">Personal</option>
															</select>
															<label for="form_control_1">Email Category<span class="required" aria-required="true">*</span></label>
														</div>
													</div>	
													<div class="col-md-2">
														<div id="div_id_stock_1_service">					
															<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
															<div class="controls ">
																<div class="radio">
																	<label><input type="radio" class="email_primary" name="email_primary" value="0" data-validation="required" data-validation-error-msg="Please select a primary email" />Is Primary <span class="required" aria-required="true">*</span></label>
																</div> 
															</div>
														</div>
													</div>
												</div>
											</div>	
										</div>
									</div>
									<div class="panel-footer">
										<button class="add_email_field_button btn blue btn-outline mt-repeater-add pull-right">Add More Emails</button>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="panel-group">
							<div class="panel panel-default custom-panel">
								<div class="panel-heading client-panel-heading">
									<h3 class="sub-head">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel3" aria-expanded="true">
											<span>Address</span> <i class="more-less glyphicon glyphicon-minus pull-right"></i>
										</a>
									</h3>
								</div>
								<div id="panel3" class="panel-collapse collapse in" aria-expanded="true" style="">
									<div class="panel-body">
										<div class="input_fields_wrap wrapping">
											<div class="countdivs">
												<div class="fieldRow clearfix">
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control" id="city" name="city[]" type="text">
															<label for="form_control_1">City</label>                                               
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control" id="district" name="district[]" type="text">
															<label for="form_control_1">District</label>                                               
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control" id="state" name="state[]" type="text">
															<label for="form_control_1">State</label>                                 
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control" id="country" name="country[]" type="text">
															<label for="form_control_1">Country</label>                                 
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control" id="postal_code" name="postal_code[]" type="text" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values">
															<label for="form_control_1">Postal Code</label>                                 
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control" id="house_no" name="house_no[]" type="text">
															<label for="form_control_1">House Number</label>                                 
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<?php echo $address_types_select;?>
															<label for="id_stock_1_unit" class="control-label requiredField">Address Type</label>
														</div>
													</div>
													
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control" id="street" name="street[]" type="text">
															<label for="form_control_1">Street</label>                                 
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<textarea class="form-control" id="full_address" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter full address"></textarea>
															<label for="form_control_1">Full Address</label>                  
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control" id="logitude" name="logitude[]" type="text" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values">
															<label for="form_control_1">Longitude</label>                                 
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group form-md-line-input form-md-floating-label">
															<input class="form-control" id="latitude" name="latitude[]" type="text" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values">
															<label for="form_control_1">Latitude</label>                                 
														</div>
													</div>
													<div class="col-md-2">
														<div id="div_id_stock_1_service">					
															<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
															<div class="controls ">
																<div class="radio">
																	<label><input type="radio" class="address_primary" name="address_primary" value="0" checked="checked"/>Is Primary</label>
																</div> 
															</div>
														</div>
													</div>
												</div>
											</div>	
										</div>
									</div>
									<div class="panel-footer">
										<button class="add_field_button btn blue btn-outline mt-repeater-add pull-right">Add More Address</button>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/contacts/view/'.$reference_uuid); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>

<script src="<?php echo base_url() . "public/" ?>js/form/form_client_contacts.js" type="text/javascript"></script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAXkP7Z7L54odTOQJshVFVvQSt-FkAd77I&sensor=false&libraries=places"></script>  
 <script type="text/javascript">
   		var i=0;
		google.maps.event.addDomListener(window, 'load', function () {
			$('.locationtext').each(function(){
				//alert($(this).attr('name'));
			var x = $(this).attr('name');
			var places = new google.maps.places.Autocomplete(this);
			google.maps.event.addListener(places, 'place_changed', function () {
				var yy = x.replace("[text-input]", "");
				alert(yy);
				var place = places.getPlace();
				//var name = place.name;
				//var name = $(this).closest('.mt-repeater-item').find('.nameval').val();
				
				//alert(name);
												
			});
			});			
		}); 
	</script>
    <script type="text/javascript">

$(document).ready(function() {


	
	var max_fields      = 10; //maximum input boxes allowed
    var phone_wrapper         = $(".phone_input_fields_wrap"); //Fields wrapper
	
    var add_phone_button      = $(".add_phone_field_button"); //Add button ID
	var px = 0;
    px = $('.phonecountdivs').length; //initlal text box count
	//alert(px);
	
	
	$(add_phone_button).click(function(e){ //on add input button click
        e.preventDefault();
        $(phone_wrapper).append('<div class="phonecountdivs"><div class="fieldRow clearfix"><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input type="hidden" name="phone_country_code[]" class ="phone_country_code" value="1"/><input class="form-control phone_number" id="phone_number" name="phone_number[]" type="text" data-validation="number" data-validation-error-msg="Please enter only numeric values" tableName="phone_numbers" tableField="phone_number"/><label for="form_control_1" class="label-phone-number">Phone Number <span class="required" aria-required="true">*</span></label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="phone_area_code" name="phone_area_code[]" type="text"><label for="form_control_1">Phone Area Code</label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><select class="form-control edited" name="pn_category[]" id="pn_category"><option value="work">Work</option><option value="home">Home</option><option value="personal">Personal</option></select><label for="form_control_1">Phone Category<span class="required" aria-required="true">*</span></label></div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><select class="form-control edited" name="pn_type[]" id="pn_type"><option value="Landline">Landline</option><option value="Mobile">Mobile</option><option value="SIP">SIP</option></select><label for="form_control_1">Phone Type<span class="required" aria-required="true">*</span></label></div></div><div class="col-md-2"><div id="div_id_stock_1_service"><label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label><div class="controls "><div class="radio"><label><input type="radio" class="phone_primary" name="phone_primary" value="'+px+'" data-validation="required" data-validation-error-msg="Please select a primary phone"/>Is Primary <span class="required" aria-required="true">*</span></label></div></div></div></div><div class="col-md-1"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_phone_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i> Remove</a> </div></div></div></div></div>'); //adding form
		px++;
		recall();
    });
   
   $(phone_wrapper).on("click",".remove_phone_field", function(e){ //user click on remove button
        e.preventDefault(); 
		$(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove();
		px--;
		var y=0;
		$('.phone_primary').each(function(i, obj) {
			//alert(y);
			$(this).val(y);
			y++;
			//alert(val);

		});		
		//alert(x);
    })
   
	
	
	var email_wrapper         = $(".email_input_fields_wrap"); //Fields wrapper
    var add_email_button      = $(".add_email_field_button"); //Add button ID
	var ex = 0;
	ex = $('.emailcountdivs').length; //initlal text box count
	
    $(add_email_button).click(function(e){ //on add input button click
        e.preventDefault();
        $(email_wrapper).append('<div class="emailcountdivs"><div class="fieldRow clearfix"><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control email_address" id="email_address" name="email_address[]" type="text" data-validation="email" data-validation-error-msg="Please enter valid email" tableName="contact_emails" tableField="email_address"><label for="form_control_1">Email Address<span class="required" aria-required="true">*</span></label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><select class="form-control edited" name="email_category[]" id="email_category"><option value="work">Work</option><option value="home">Home</option><option value="personal">Personal</option></select><label for="form_control_1">Email Category<span class="required" aria-required="true">*</span></label></div></div><div class="col-md-2"><div id="div_id_stock_1_service"><label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label><div class="controls "><div class="radio"><label><input type="radio" class="email_primary" name="email_primary" value="'+ex+'" data-validation="required" data-validation-error-msg="Please select a primary email"/>Is Primary <span class="required" aria-required="true">*</span></label></div></div></div></div><div class="col-md-1"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_email_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i> Remove</a> </div></div></div></div></div>');
		ex++;	//adding form
		recall();			
    });
	
	$(email_wrapper).on("click",".remove_email_field", function(e){ //user click on remove button
        e.preventDefault(); 
		$(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove();
		ex--;
		var y=0;
		$('.email_primary').each(function(i, obj) {
			//alert(y);
			$(this).val(y);
			y++;
			//alert(val);

		});		
		//alert(x);
    })
	

    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

	var address_types_select = '<?php echo $address_types_select;?>';
	var x = 0;
	x = $('.countdivs').length; //initlal text box count
	
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        $(wrapper).append('<div class="countdivs"><div class="fieldRow clearfix"><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="city" name="city[]" type="text"><label for="form_control_1">City</label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="district" name="district[]" type="text"><label for="form_control_1">District</label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="state" name="state[]" type="text"><label for="form_control_1">State</label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="country" name="country[]" type="text"><label for="form_control_1">Country</label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="postal_code" name="postal_code[]" type="text" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values"><label for="form_control_1">Postal Code</label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="house_no" name="house_no[]" type="text"><label for="form_control_1">House Number</label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"> '+address_types_select+' <label for="id_stock_1_unit" class="control-label requiredField">Address Type</label></div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="street" name="street[]" type="text"><label for="form_control_1">Street</label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><textarea class="form-control" id="full_address" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter full address"></textarea><label for="form_control_1">Full Address</label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="logitude" name="logitude[]" type="text" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values"><label for="form_control_1">Longitude</label> </div></div><div class="col-md-3"><div class="form-group form-md-line-input form-md-floating-label"><input class="form-control" id="latitude" name="latitude[]" type="text" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values"><label for="form_control_1">Latitude</label> </div></div><div class="col-md-2"><div id="div_id_stock_1_service"><label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label><div class="controls "><div class="radio"><label><input type="radio" class="address_primary" name="address_primary" value="'+x+'"/>Is Primary</label></div></div></div></div><div class="col-md-1"> <div id="div_id_stock_1_service" class="form-group"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i> Remove</a> </div></div></div></div></div>'); 
		x++;//adding form
		recall();
    });
	
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove button
	
        e.preventDefault(); 
		$(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove(); 
		x--;
		var y=0;
		$('.address_primary').each(function(i, obj) {
			//alert(y);
			$(this).val(y);
			y++;
			//alert(val);

		});		
		//alert(x);
    })
	
		
    
});
</script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/intlTelInput.js"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });

function recall()
{
    $(".phone_number").intlTelInput({
      // allowDropdown: true,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: "body",
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      //hiddenInput: "full_number[]",
       initialCountry: "us",
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
       separateDialCode: true,
      utilsScript: '<?php echo base_url() . "public/" ?>js/utils.js'
    });
	 $('.country').click(function(){
		var y = $(this).attr('data-dial-code');
		//alert(y);
		var phonethis = $(this);
		$(this).parent().parent().parent().parent().find('.phone_country_code').val(y);
		var value = $(this).parent().parent().parent().find('.phone_number').val();
		var table = $(this).parent().parent().parent().find('.phone_number').attr('tableName');
		var field = $(this).parent().parent().parent().find('.phone_number').attr('tableField');
		var baseURL = $('#baseURL').val();
		var phone_country_code = y;
		//alert(phone_country_code);
        //var old_email = $('#old_email').val();
		//alert(value);
        var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/contacts/ajax_checkUniquePhone',
            dataType: 'json',
            data: {table: table, field: field, value: value,phone_country_code :phone_country_code},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                    //$('#' + field).val('');
                    phonethis.parent().parent().parent().find('.phone_number').val('');
                    phonethis.parent().parent().parent().find('.phone_number').focus();
                } else {

                }
            }
        });
	});	
	$.validate();
	 
}
$('#panel1').on('show.bs.collapse hidden.bs.collapse', function (e) {
	if (e.type == 'hidden') {
		$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
		$(window).bind('resize', function () {
			$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
			//alert('resized');
		});
	} else {
		$('.dataTables_scrollBody').css({'height': '210px'});
	}
	$(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');


});
$('#panel2').on('show.bs.collapse hidden.bs.collapse', function (e) {
	if (e.type == 'hidden') {
		$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
		$(window).bind('resize', function () {
			$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
			//alert('resized');
		});
	} else {
		$('.dataTables_scrollBody').css({'height': '210px'});
	}
	$(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');


});
$('#panel3').on('show.bs.collapse hidden.bs.collapse', function (e) {
	if (e.type == 'hidden') {
		$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
		$(window).bind('resize', function () {
			$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
			//alert('resized');
		});
	} else {
		$('.dataTables_scrollBody').css({'height': '210px'});
	}
	$(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');


});
</script>
<script>
$(function(){
	//$(".dob").keydown(function(e){ e.preventDefault(); });
    $('.dob').datepicker({
        dateFormat: 'yyyy-mm-dd',
        //startDate: '-100y',
		endDate: '-10y',
		//endDate: '+0d',
        autoclose: true
    });
});
</script>
<script>
    $(".phone_number").intlTelInput({
       //allowDropdown: true,
      //autoHideDialCode: true,
      //autoPlaceholder: "off",
      // dropdownContainer: "body",
      // excludeCountries: ["us"],
      //formatOnDisplay: true,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      //hiddenInput: "full_number",
       initialCountry: "us",
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
       separateDialCode: true,
      utilsScript: '<?php echo base_url() . "public/" ?>js/utils.js'
    });
	
	 $('.country').click(function(){
		var y = $(this).attr('data-dial-code');
		//var country = $(this).attr('data-country-code');
		//alert(country);
		var phonethis = $(this);
		$(this).parent().parent().parent().parent().find('.phone_country_code').val(y);
		var value = $(this).parent().parent().parent().find('.phone_number').val();
		var table = $(this).parent().parent().parent().find('.phone_number').attr('tableName');
		var field = $(this).parent().parent().parent().find('.phone_number').attr('tableField');
		var baseURL = $('#baseURL').val();
		var phone_country_code = y;
		//alert(phone_country_code);
        //var old_email = $('#old_email').val();
		//alert(value);
        var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/contacts/ajax_checkUniquePhone',
            dataType: 'json',
            data: {table: table, field: field, value: value,phone_country_code :phone_country_code},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                    //$('#' + field).val('');
                    phonethis.parent().parent().parent().find('.phone_number').val('');
                    phonethis.parent().parent().parent().find('.phone_number').focus();
                } else {

                }
            }
        });
		
	});
</script>
<style>
.intl-tel-input.separate-dial-code .selected-flag{background-color:transparent !important;}
.intl-tel-input.separate-dial-code .selected-flag:focus, .intl-tel-input.allow-dropdown .flag-container:focus, .intl-tel-input.separate-dial-code .flag-container:focus{outline:0 !Important;}
</style>