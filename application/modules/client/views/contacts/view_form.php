<?php
$accessAdd = helper_fetchPermission('51', 'add');
$accessEdit = helper_fetchPermission('51', 'edit');
$accessStatus = helper_fetchPermission('51', 'active');
$accessDelete = helper_fetchPermission('51', 'delete');
?>
<!-- END PAGE HEADER-->
<div class="row">
    <div>
	
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Contacts</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					 <li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if(isset($client_type) && !empty($client_type) && $client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>						
					<li>
						<span>Contacts</span>   
						<i class="fa fa-angle-right"></i>						
					</li>
										<li>
						<span>List</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
						<?php if ($accessAdd == 'Y') { ?> 
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('client/contacts/create/'.$reference_uuid); ?>"> Add New Contact
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
						<?php } ?>
                        </div>
                        <div class="col-md-3">

                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover order-column" id="role">
                    <thead>
                        <tr>
                            <th style="width:20%; text-align:left;"> Contact Name </th>
							<th style="width:20%; text-align:left;"> Contact Full Name </th>
							
							<?php if($reference_prefix == "loca"){ ?> 
								<th style="width:20%; text-align:left;"> Location Name </th>
							<?php }elseif($reference_prefix == "clie"){ ?> 
								<th style="width:20%; text-align:left;"> Client Name </th>
							<?php }	 ?> 						
							<?php if ($accessStatus == 'Y') { ?> 
								<th  style="width:8%; text-align:left;"> Status </th>
							<?php } ?>
							<?php if ($accessEdit == 'Y') { ?> 
                            <th  style="width:15%; text-align:left;"> Action </th> 
							<?php } ?>							
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($clientContacts)) {
                            foreach ($clientContacts as $value) {
                              //  pr($value);
								//if($value['is_deleted'] == 'N'){
                                ?>
                                <tr>
                                    <td> <?php echo $value['contact_name']; ?> </td> 
									<td> <?php echo $value['full_name']; ?> </td> 
									
									<?php if($reference_prefix == "loca"){ ?> 
										<td> <?php echo $value['location_name']; ?> </td>
									<?php }elseif($reference_prefix == "clie"){ ?> 
										<td> <?php echo $value['client_title']; ?> </td>
									<?php }	 ?> 									
									
									<?php if ($accessStatus == 'Y') { ?>  
                                    <td>
                                        <input type="checkbox" <?php
                                        if ($value['status'] == 'Y') {
                                            echo 'checked';
                                        }
                                        ?>  id="onoffactiontoggle" class="onoffactiontoggle" contact_uuid="<?php echo $value['contact_uuid']; ?>" >
                                    </td>
									<?php } ?>
                                    <?php if ($accessEdit == 'Y' || $accessDelete == 'Y') { ?>
                                    <td>
                                        <a href="<?php echo base_url() . 'client/contacts/edit/'.$reference_uuid.'/'. $value['contact_uuid']; ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>  
                                        <a href="javascript:void(0);"  contactId="<?php echo $value['contact_uuid'];?>" class="btn btn-xs red isDeleteClientContacts" onlick=''>
											<i class="fa fa-trash"></i>
                                        </a>
                                    </td>
									<?php } ?>
                                </tr> 
                                <?php
								//}
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9"> No Record Found </td>
                            </tr> 
<?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client_contacts.js" type="text/javascript"></script>
