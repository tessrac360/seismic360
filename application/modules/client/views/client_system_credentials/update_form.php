<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<?php if($reference =="client"){?>	
					<?php if($client_type != ""){ 
							if($client_type == "P"){
					?>
					<span class="caption-subject font-green-steel bold uppercase">Edit Partner Credentials</span>
					<?php }else{ ?>
					<span class="caption-subject font-green-steel bold uppercase">Edit Client Credentials</span>										
					<?php }}?>
				<?php }elseif($reference =="devices"){?>
					<span class="caption-subject font-green-steel bold uppercase">Edit Device Credentials</span>
				<?php }elseif($reference =="gateways"){?>
					<span class="caption-subject font-green-steel bold uppercase">Edit Gateway Credentials</span>
				<?php }?>
				<span style="color:black;"><?php if(isset($title) && !empty($title)){
						echo "(".$title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Account Mgmt</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if(isset($client_type) && !empty($client_type) && $client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>
					<li>
					<?php if($reference =="client"){?>	
						<span>Client Credentials</span>   
					<?php }elseif($reference =="devices"){?>
						<span>Device Credentials</span> 
					<?php }elseif($reference =="gateways"){?>
						<span>Gateway Credentials</span> 
					<?php }?>
					
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Edit</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light  padd0">
		
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmSysCredential" id="frmSysCredential" method="post" action="">
                    <div class="form-body">
					<!--<h4 class="form-section">Person Info</h4>-->
                        <div class="row"> 
						
							<div class="col-md-4">
								<div class="form-group form-md-line-input form-md-floating-label">
									<select class="form-control edited" name="login_account_type" id="login_account_type">
									<option value="">--Select--</option>
									<?php if($getLoginAccountTypes['status'] == 'true'){
										foreach($getLoginAccountTypes['resultSet'] as $value){?>
										<option value="<?php echo $value['credentials_uuid'];?>" <?= ($getSysCredentials['resultSet']->login_account_type == $value['credentials_uuid']) ? 'selected' : ''; ?>><?php echo $value['login_account_type']; ?></option>
										<?php }
									}?>
									</select>
									<label for="form_control_1">Login Account Type<span class="required" aria-required="true">*</span></label>
								</div>
							</div>
							
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" id="username" name="username" autocomplete="off" data-validation="required" data-validation-error-msg="Please enter username" tableName="system_credentials" tableField="username" value="<?php echo ($getSysCredentials['resultSet']->username) ? $getSysCredentials['resultSet']->username : ''; ?>">
                                    <label for="form_control_1">Username<span class="required" aria-required="true">*</span></label>                                 
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="password" class="form-control" id="password" name="password" autocomplete="off" data-validation="required" data-validation-error-msg="Please enter password" value="<?php echo ($getSysCredentials['resultSet']->password) ? $getSysCredentials['resultSet']->password : ''; ?>">
                                    <label for="form_control_1">Password<span class="required" aria-required="true">*</span></label>                                 
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="port" name="port" type="text" data-validation="required" data-validation-error-msg="Please enter port" tableName="system_credentials" tableField="port" value="<?php echo ($getSysCredentials['resultSet']->port) ? $getSysCredentials['resultSet']->port : ''; ?>">
                                    <label for="form_control_1">Port<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
							<div class="col-md-3">
								<div id="div_id_stock_1_service">					
									<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
									<div class="controls ">
										<div class="radio">
											<label><input type="radio" class="isPrimary" name="is_primary" value="1"  <?php if($getSysCredentials['resultSet']->is_primary == '1'){ echo 'checked="checked"'  ; echo " disabled"; }?>/>Is Primary</label>
										</div> 
									</div>
								</div>
							</div>
                        </div>
						<div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/ClientSystemCredentials/view/'.$reference_uuid); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
			
        </div>
    </div>
</div>
<style>
.portlet.light.padd0 {
    overflow: unset;
}
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>

<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_system_credentials.js" type="text/javascript"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });
</script>