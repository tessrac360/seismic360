<?php
$accessAdd = helper_fetchPermission('51', 'add');
$accessEdit = helper_fetchPermission('51', 'edit');
$accessStatus = helper_fetchPermission('51', 'active');
$accessDelete = helper_fetchPermission('51', 'delete');
?>
<!-- END PAGE HEADER-->
<div class="row">
    <div>
	
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<?php if($reference =="client"){?>	
				<?php if($client_type != ""){ 
							if($client_type == "P"){
					?>
					<span class="caption-subject font-green-steel bold uppercase">Partner Credentials</span>
					<?php }else{ ?>
					<span class="caption-subject font-green-steel bold uppercase">Client Credentials</span>										
					<?php }}?>				 
				<?php }elseif($reference =="devices"){?>
					<span class="caption-subject font-green-steel bold uppercase">Device Credentials</span>
				<?php }elseif($reference =="gateways"){?>
					<span class="caption-subject font-green-steel bold uppercase">Gateway Credentials</span>
				<?php }?>			
				
				<span style="color:black;"><?php if(isset($title) && !empty($title)){
						echo "(".$title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					 <li>
						<i class="fa fa-cog"></i>
						<span>Account Mgmt</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if(isset($client_type) && !empty($client_type) && $client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>						
					<li>
					<?php if($reference =="client"){?>	
						<span>Client Credentials</span>   
					<?php }elseif($reference =="devices"){?>
						<span>Device Credentials</span> 
					<?php }elseif($reference =="gateways"){?>
						<span>Gateway Credentials</span> 
					<?php }?>
						<i class="fa fa-angle-right"></i>						
					</li>
										<li>
						<span>List</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
						<?php if ($accessAdd == 'Y') { ?> 
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('client/ClientSystemCredentials/create/'.$reference_uuid); ?>"> Add New Credential
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
						<?php } ?>
                        </div>
                        <div class="col-md-3">

                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover order-column" id="role">
                    <thead>
                        <tr>
                            <th style="width:20%; text-align:left;"> Login Account Type </th>
                            <th style="width:20%; text-align:left;"> Username </th>
                            <th style="width:20%; text-align:left;"> Port </th>
							<?php if ($accessStatus == 'Y') { ?> 
							<th  style="width:8%; text-align:left;"> Status </th>
							<?php } ?>
							<?php if ($accessEdit == 'Y') { ?> 
                            <th  style="width:15%; text-align:left;"> Action </th> 
							<?php } ?>							
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($getSysCredentials)) {
                            foreach ($getSysCredentials as $value) {
                              // pr($value);
								//if($value['is_deleted'] == 'N'){
                                ?>
                                <tr>
                                    <td> <?php echo $value['login_account_type']; ?> </td>
                                    <td> <?php echo $value['username']; ?> </td> 
                                    <td> <?php echo $value['port']; ?> </td> 
	
									<?php if ($accessStatus == 'Y') { ?>  
                                    <td>
                                        <input type="checkbox" <?php
                                        if ($value['is_disabled'] == '0') {
                                            echo 'checked';
                                        }
                                        ?>  id="onoffactiontoggle" class="onoffactiontoggle" credentials_uuid="<?php echo $value['credentials_uuid']; ?>">
                                    </td>
									<?php } ?>
                                    <?php if ($accessEdit == 'Y' || $accessDelete == 'Y') { ?>
                                    <td>
                                        <a href="<?php echo base_url() . 'client/ClientSystemCredentials/edit/'.$reference_uuid.'/'. $value['credentials_uuid']; ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>  
                                        <a href="javascript:void(0);"  credentials_uuid="<?php echo $value['credentials_uuid'];?>" class="btn btn-xs red isDelete" onlick=''>
											<i class="fa fa-trash"></i>
                                        </a>
                                    </td>
									<?php } ?>
                                </tr> 
                                <?php
								//}
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9"> No Record Found </td>
                            </tr> 
<?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_system_credentials.js" type="text/javascript"></script>
