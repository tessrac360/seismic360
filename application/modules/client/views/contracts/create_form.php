<?php //pr($getEscalationIds['resultSet']);exit; ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.css">
<style>
.marb15{margin-bottom:15px;}
.padl12{padding-left:12px;}
</style>
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Contract</span>
				<span style="color:black;"><?php if(isset($clientName['resultSet']['client_title']) && !empty($clientName['resultSet']['client_title'])){
						echo " (".$clientName['resultSet']['client_title'].")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Account Mgmt</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_type != ""){ 
							if($client_type == "P"){
					?>
					<li>					
						<a href="<?php echo base_url('partner') ?>">Partner</a>                            
						<i class="fa fa-angle-right"></i>
					</li>
					<?php }else{ ?>
					<li>					
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>															
					<?php }}?>
					<li>
						<span>Contracts</span>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light  padd0">
		
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmHolidays" id="frmHolidays" method="post" action="">
                    <div class="form-body">
					<!--<h4 class="form-section">Person Info</h4>-->
                        <div class="row marb15"> 
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="contract_type_uuid" name="contract_type_uuid" >

                                        <?php
                                        if ($contractTypes['status'] == 'true') {
                                            foreach ($contractTypes['resultSet'] as $value){
												if($value['contract_type']!="BOH_Topup"){
                                                ?>
                                               <option value="<?php echo $value['uuid']; ?>" <?php if($value['is_default'] ==1)echo "selected" ?>><?php echo $value['contract_type']; ?></option>  
                                                <?php
                                            }
											}
                                        }
                                        ?> 										

										
                                    </select> 								
                                    <label for="form_control_1">Contract Type</label>                                               
                                </div>
                            </div>	
                        </div>
						
						<div class="panel-group marb15" id="accordion">
							<div class="panel panel-default custom-panel"  id="contract_type_div">
								<div class="panel-heading client-panel-heading">
									<h3 class="sub-head">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
											<span>BOH</span> <i class="more-less glyphicon pull-right glyphicon-minus"></i>
										</a>
									</h3>
								</div>
								<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
									<div class="panel-body">
										<div class="portlet-body padl12">   
											<div class="col-md-3">
												 <div class="form-group form-md-line-input form-md-floating-label">
												
												 <input type="text" name="purchased_BOH" data-validation="required" data-validation-error-msg="Please enter B0H" class="textinput form-control city">
												  <label for="form_control_1">BOH</label>
												 </div>

										   </div>

									
											<div class="col-md-3">
												<div id="div_id_stock_1_service">

												<div class="form-group form-md-line-input form-md-floating-label">
												<select class="form-control" id="validity_uuid" name="validity_uuid" >

													<?php
													if ($validities['status'] == 'true') {
														foreach ($validities['resultSet'] as $value) {
															?>
														   <option value="<?php echo $value['uuid']; ?>" <?php if($value['is_default'] ==1)echo "selected" ?>><?php echo $value['validity_type']; ?></option>  
															<?php
														}
													}
													?> 
												</select> 
												<label for="id_stock_1_product" class="control-label requiredField">Validity Period<span class="asteriskField"></span></label>
												</div>

												</div>
											</div>
											<div class="col-md-3">
												<div id="div_id_stock_1_service">

												<div class="form-group form-md-line-input form-md-floating-label">
												<select class="form-control" id="contract_period_uuid" name="contract_period_uuid" >

													<?php
													if ($contractperiods['status'] == 'true') {
														foreach ($contractperiods['resultSet'] as $value) {
															?>
														   <option value="<?php echo $value['uuid']; ?>" <?php if($value['is_default'] ==1)echo "selected" ?>><?php echo $value['contract_period_type']; ?></option>  
															<?php
														}
													}
													?> 
												</select> 
												<label for="id_stock_1_product" class="control-label requiredField">Contract Period<span class="asteriskField"></span></label>
												</div>

												</div>
											</div>											
										   <div class="col-md-3">
												<div id="div_id_stock_1_service">

												<div class="form-group form-md-line-input form-md-floating-label">

												<input type="text" name="trigger_alert_BOH" value="10" class="textinput form-control state" data-validation="required" data-validation-error-msg="Please enter minimum BOH Alert">
												<label for="id_stock_1_product" class="control-label requiredField">Minimum BOH Alert<span class="asteriskField"></span></label>
												</div>
												</div>
										   </div>												
											<div class="col-md-3">
												<div class="form-group form-md-line-input form-md-floating-label">
												<input type="text" name="trigger_alert_expiry"  value="10"  id="trigger_alert_expiry" tableName="contract_validity" tableField="uuid" data-validation="required" data-validation-error-msg="Please enter expiry alert" class="textinput form-control alert">
												<label for="form_control_1">Expiry Alert</label>
												</div>
											</div>	
																	

										</div> 
									</div>
								</div>
							</div>
						</div>
						
						<!--
						<div class="panel-group marb15" id="accordion">
							<div class="panel panel-default custom-panel">
								<div class="panel-heading client-panel-heading">
									<h3 class="sub-head">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
											<span>Activation</span> <i class="more-less glyphicon pull-right glyphicon-minus"></i>
										</a>
									</h3>
								</div>
								<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
									<div class="panel-body">
										<div class="portlet-body padl12">                                
											<div class="col-md-4 col-sm-6">

												
												<div id="div_id_stock_1_service">					
													
													<div class="controls ">
														<div class="radio">
															<div class="mt-radio-inline">
																<label class="mt-radio">
																	<input type="radio" class="radioselection" name="radioselection" value="1" checked >None
																	<span></span>
																</label>
																<label class="mt-radio">
																	<input type="radio" class="radioselection" name="radioselection" value="2"> Auto Renew
																	<span></span>
																</label>
																<label class="mt-radio">
																	<input type="radio" class="radioselection" name="radioselection" id ="sheduleradio" value="3"> Schedule
																	<span></span>
																</label>											
															</div>
														</div> 
													</div>
												</div>
								
											</div>
																																
											<div class="col-md-8 col-sm-6">
												<div class="col-md-3">
												  <div id="div_id_stock_1_service">
													 
													 <div class="form-group form-md-line-input form-md-floating-label" id="sheduleblock">
													 
													 </div>
													 
												  </div>
											   </div>
											</div>
										</div> 
									</div>
								</div>
							</div>
						</div>-->
						
						<div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; 
							
							<a href="<?php echo base_url('client/contracts/view/'.$reference_uuid); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
			
        </div>
    </div>
</div>
<style>
.portlet.light.padd0 {
    overflow: unset;
}
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>

<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client_contracts.js" type="text/javascript"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });
</script>
<script>

$(function(){
	$(".activate_scheduled_on").keydown(function(e){ e.preventDefault(); });
    $('.activate_scheduled_on').datepicker({
        dateFormat: 'yyyy-mm-dd',
        //startDate: '-100y',
		//endDate: '-10y',
		//endDate: '+0d',
        //autoclose: true
    });	

});

$('#contract_type_uuid').on('change', function () {
	var contract_type = $("#contract_type_uuid option:selected").text();
	var baseURL = $('#baseURL').val();
	$.ajax({
		type: 'POST',
		url: baseURL + 'client/contracts/ajax_contract_type_list',
		data: {'contract_type': contract_type},
			//$("#service_group_uuid").removeClass('edited');
				success: function (response) {
					$('#contract_type_div').html(response);

				}

		
	});	

});
$('.radioselection').on('change', function () {
	var activate_scheduled_on ='<input type="text" name="activate_scheduled_on" id="activate_scheduled_on" class="textinput form-control activate_scheduled_on" data-validation="required" data-validation-error-msg="Please enter schedule time"><label for="id_stock_1_product" class="control-label">Schedule Date<span class="asteriskField"></span></label>';
	
	var val =$("input[name='radioselection']:checked").val();	
	if(val ==3){
		$('#sheduleblock').html(activate_scheduled_on);	
		$(".activate_scheduled_on").keydown(function(e){ e.preventDefault(); });
		$('.activate_scheduled_on').datepicker({
			dateFormat: 'yyyy-mm-dd',
			//startDate: '-100y',
			//endDate: '-10y',
			//endDate: '+0d',
			//autoclose: true
			 startDate: new Date() 
		});	
	}else{
		$('#sheduleblock').html('');	
	}
	//alert(val);
});	
</script>