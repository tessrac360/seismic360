<?php
$accessAdd = helper_fetchPermission('51', 'add');
$accessEdit = helper_fetchPermission('51', 'edit');
$accessStatus = helper_fetchPermission('51', 'active');
$accessDelete = helper_fetchPermission('51', 'delete');
?>

<style>
.status_btn .btn{width:62px !important;}
</style>

<!-- END PAGE HEADER-->
<div class="row">
    <div>
	
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase"><?php echo ($client_type == "P")?"Partner":"Client"?> Contracts</span>
				<span style="color:black;"><?php if(isset($clientName['resultSet']['client_title']) && !empty($clientName['resultSet']['client_title'])){
						echo " (".$clientName['resultSet']['client_title'].")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					 <li>
						<i class="fa fa-cog"></i>
						<span>Account Mgmt</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_type != ""){ 
							if($client_type == "P"){
					?>
					<li>					
						<a href="<?php echo base_url('partner') ?>">Partner</a>                            
						<i class="fa fa-angle-right"></i>
					</li>
					<?php }else{ ?>
					<li>					
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>															
					<?php }}?>
					<li>
						<span>Contracts</span> 
						<i class="fa fa-angle-right"></i>						
					</li>
										<li>
						<span>List</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
						<?php if ($accessAdd == 'Y') { ?> 
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('client/contracts/create/'.$reference_uuid); ?>"> Add New Contract
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
						<?php } ?>
                        </div>
                        <div class="col-md-3">

                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover order-column" id="role">
                    <thead>
                        <tr>
                            <th> Contract Number </th>
                            <th> Contract Type </th>
                            <th> Validity Type </th>
							<th> BOH (hours) </th>
							<th> Consumed BOH (hours) </th>                          
                            <th> BOH Topup (hours) </th>
							<th> Purchased On </th>
							<?php if ($accessStatus == 'Y') { ?> 
							<th> Status </th>
							<?php } ?>
							<?php if ($accessEdit == 'Y' && 1==1) { ?> 
                            <th> Actions </th> 
							<?php } ?>							
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($getClientContracts)) {
                            foreach ($getClientContracts as $value) {
                              // pr($value);
								//if($value['is_deleted'] == 'N'){
                                ?>
                                <tr>
                                    <td> <?php echo $value['transcation-id']; ?> </td> 
                                    <td> <?php echo $value['contract_type']; ?> </td>
                                    <td> <?php echo $value['contract_type']=='BOH'?$value['validity_type']:"NA"; ?> </td>
                                    <td> <?php echo $value['contract_type']=='BOH'?round($value['purchased_BOH']/3600,4):"NA"; ?> </td>
                                    <td> <?php echo $value['contract_type']=='BOH'?round($value['consumed_BOH']/3600,4):"NA"; ?> </td>
                                    <td> <?php echo ($value['contract_type']=='BOH' || $value['contract_type']=='BOH_Topup' )?round($value['BOH_topup_hours']/3600,4):"NA"; ?> </td>
                                    <td> <?php echo $value['purchased_on']; ?> </td> 
                                   
                                    <td class="status_btn"> 
									<?php if($value['contract_type'] != 'BOH Topup'){if($value['validity_status'] =='New'){?>
									<a href="javascript:void(0);" class="btn btn-xs  blue"> New </a>
									<?php }elseif($value['validity_status'] =='Expired'){?>
									<a href="javascript:void(0);" class="btn btn-xs  red"> Expired </a> 
									<?php }elseif($value['validity_status'] =='Active'){?>
									
									<a href="javascript:void(0);" class="btn btn-xs btn-success"> Active </a>
									<?php }}?>
									
																														 
									</td> 
									<td>
									<?php if($value['validity_status'] =='New'){?>
									<a href="<?php echo base_url() . 'client/contracts/changestatus/'. $reference_uuid.'/'. $value['uuid']; ?>" class="btn btn-xs  blue"> Activate </a>
									<?php }elseif($value['validity_status'] =='Active'){?>

									<?php if($value['contract_type'] =='BOH'){?>
									<a href="<?php echo base_url() . 'client/contracts/topup/'. $reference_uuid.'/'. $value['uuid']; ?>" class="btn btn-xs  blue"> Topup </a>
									<?php }?>
									<?php }?>									
									</td> 
	
                                    <?php 
									if(1==2){
									if ($accessEdit == 'Y' || $accessDelete == 'Y') { ?>
                                    <td>
                                        <a href="<?php echo base_url() . 'client/contracts/edit/'.$reference_uuid.'/'. $value['uuid']; ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>  
                                        <a href="javascript:void(0);"  uuid="<?php echo $value['uuid'];?>" class="btn btn-xs red isDelete" onlick=''>
											<i class="fa fa-trash"></i>
                                        </a>
                                    </td>
									<?php }} ?>
                                </tr> 
                                <?php
								//}
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9"> No Record Found </td>
                            </tr> 
<?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client_holidays.js" type="text/javascript"></script>
<script>
	$('.onoffactiontoggle').bootstrapToggle({
        on: 'Active',
        off: 'New',
        size: 'mini',
        onstyle: 'success',
        offstyle: 'danger'
    });
        return loaded();
    }
</script>

