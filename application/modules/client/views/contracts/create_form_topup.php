<?php //pr($getEscalationIds['resultSet']);exit; ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.css">
<style>
.marb15{margin-bottom:15px;}
.padl12{padding-left:12px;}
</style>
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Contract</span>
				<span style="color:black;"><?php if(isset($clientName['resultSet']['client_title']) && !empty($clientName['resultSet']['client_title'])){
						echo " (".$clientName['resultSet']['client_title'].")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Account Mgmt</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_type != ""){ 
							if($client_type == "P"){
					?>
					<li>					
						<a href="<?php echo base_url('partner') ?>">Partner</a>                            
						<i class="fa fa-angle-right"></i>
					</li>
					<?php }else{ ?>
					<li>					
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>															
					<?php }}?>
					<li>
						<span>Contracts</span>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light  padd0">
		
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmHolidays" id="frmHolidays" method="post" action="">
                    <div class="form-body">
					<!--<h4 class="form-section">Person Info</h4>-->
						
						<div class="panel-group marb15" id="accordion">
							<div class="panel panel-default custom-panel"  id="contract_type_div">
								<div class="panel-heading client-panel-heading">
									<h3 class="sub-head">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
											<span>BOH Topup</span> <i class="more-less glyphicon pull-right glyphicon-minus"></i>
										</a>
									</h3>
								</div>
								<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
									<div class="panel-body">
										<div class="portlet-body padl12">   
											<div class="col-md-3">
												 <div class="form-group form-md-line-input form-md-floating-label">
												
												 <input type="text" name="BOH_topup_hours" data-validation="required" data-validation-error-msg="Please enter BOH topup hours" class="textinput form-control city">
												  <label for="form_control_1">BOH Topup Hours</label>
												 </div>

										   </div>																	
										</div> 
									</div>
								</div>
							</div>
						</div>
																		
						<div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/contracts/view/'.$reference_uuid); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
			
        </div>
    </div>
</div>
<style>
.portlet.light.padd0 {
    overflow: unset;
}
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>

<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client_contracts.js" type="text/javascript"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });
</script>
<script>

$(function(){
	$(".activate_scheduled_on").keydown(function(e){ e.preventDefault(); });
    $('.activate_scheduled_on').datepicker({
        dateFormat: 'yyyy-mm-dd',
        //startDate: '-100y',
		//endDate: '-10y',
		//endDate: '+0d',
        //autoclose: true
    });	

});

$('#contract_type_uuid').on('change', function () {
	var contract_type = $("#contract_type_uuid option:selected").text();
	var baseURL = $('#baseURL').val();
	$.ajax({
		type: 'POST',
		url: baseURL + 'client/contracts/ajax_contract_type_list',
		data: {'contract_type': contract_type},
			//$("#service_group_uuid").removeClass('edited');
				success: function (response) {
					$('#contract_type_div').html(response);

				}

		
	});	

});
$('.radioselection').on('change', function () {
	var activate_scheduled_on ='<input type="text" name="activate_scheduled_on" id="activate_scheduled_on" class="textinput form-control activate_scheduled_on" data-validation="required" data-validation-error-msg="Please enter schedule time"><label for="id_stock_1_product" class="control-label">Schedule Date<span class="asteriskField"></span></label>';
	
	var val =$("input[name='radioselection']:checked").val();	
	if(val ==3){
		$('#sheduleblock').html(activate_scheduled_on);	
		$(".activate_scheduled_on").keydown(function(e){ e.preventDefault(); });
		$('.activate_scheduled_on').datepicker({
			dateFormat: 'yyyy-mm-dd',
			//startDate: '-100y',
			//endDate: '-10y',
			//endDate: '+0d',
			//autoclose: true
			 startDate: new Date() 
		});	
	}else{
		$('#sheduleblock').html('');	
	}
	alert(val);
});	
</script>