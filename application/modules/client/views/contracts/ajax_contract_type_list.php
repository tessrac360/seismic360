<?php if($contract_type == 'BOH'){?>
<div class="panel-heading client-panel-heading">
	<h3 class="sub-head">
		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
			<span>BOH</span> <i class="more-less glyphicon pull-right glyphicon-minus"></i>
		</a>
	</h3>
</div>
<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
	<div class="panel-body">
		<div class="portlet-body padl12">   
			<div class="col-md-3">
				 <div class="form-group form-md-line-input form-md-floating-label">
				
				 <input type="text" name="purchased_BOH" data-validation="required" data-validation-error-msg="Please enter B0H" class="textinput form-control city">
				  <label for="form_control_1">BOH</label>
				 </div>

		   </div>

		
			<div class="col-md-3">
				<div id="div_id_stock_1_service">

				<div class="form-group form-md-line-input form-md-floating-label">
				<select class="form-control edited" id="validity_uuid" name="validity_uuid" >

				<?php
				if ($validities['status'] == 'true') {
				foreach ($validities['resultSet'] as $value) {
				?>
				<option value="<?php echo $value['uuid']; ?>" <?php if($value['is_default'] ==1)echo "selected" ?>><?php echo $value['validity_type']; ?></option>  
				<?php
				}
				}
				?> 
				</select> 
				<label for="id_stock_1_product" class="control-label requiredField">Validity Period<span class="asteriskField"></span></label>
				</div>

				</div>
			</div>
			<div class="col-md-3">
				<div id="div_id_stock_1_service">

				<div class="form-group form-md-line-input form-md-floating-label">
				<select class="form-control edited" id="contract_period_uuid" name="contract_period_uuid" >

					<?php
					if ($contractperiods['status'] == 'true') {
						foreach ($contractperiods['resultSet'] as $value) {
							?>
						   <option value="<?php echo $value['uuid']; ?>" <?php if($value['is_default'] ==1)echo "selected" ?>><?php echo $value['contract_period_type']; ?></option>  
							<?php
						}
					}
					?> 
				</select> 
				<label for="id_stock_1_product" class="control-label requiredField">Contract Period<span class="asteriskField"></span></label>
				</div>

				</div>
			</div>			
		   <div class="col-md-3">
				<div id="div_id_stock_1_service">

				<div class="form-group form-md-line-input form-md-floating-label">

				<input type="text" name="trigger_alert_BOH" value="10"  class="textinput form-control state edited" data-validation="required" data-validation-error-msg="Please enter minimum BOH Alert">
				<label for="id_stock_1_product" class="control-label requiredField">Minimum BOH Alert<span class="asteriskField"></span></label>
				</div>
				</div>
		   </div>			
			<div class="col-md-3">
				<div class="form-group form-md-line-input form-md-floating-label">
				<input type="text"  value="10" name="trigger_alert_expiry" id="trigger_alert_expiry" data-validation="required" data-validation-error-msg="Please enter expiry alert" class="textinput form-control city edited">
				<label for="form_control_1">Expiry Alert</label>
				</div>
			</div>	


		</div> 
	</div>
</div>
<?php }else{?>
<div class="panel-heading client-panel-heading">
	<h3 class="sub-head">
		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
			<span>FTE</span> <i class="more-less glyphicon pull-right glyphicon-minus"></i>
		</a>
	</h3>
</div>
<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
	<div class="panel-body">
		<div class="portlet-body padl12">  
			<div class="col-md-3">
			 <div class="form-group form-md-line-input form-md-floating-label">

			 <input type="text" name="resource_count" data-validation="required" data-validation-error-msg="Please enter resource count" class="textinput form-control city">
			  <label for="form_control_1">Resource Count
			</label>
			 </div>

			</div>		
			<div class="col-md-3">
				<div id="div_id_stock_1_service">

				<div class="form-group form-md-line-input form-md-floating-label">
				<select class="form-control edited" id="contract_period_uuid" name="contract_period_uuid" >

					<?php
					if ($contractperiods['status'] == 'true') {
						foreach ($contractperiods['resultSet'] as $value) {
							?>
						   <option value="<?php echo $value['uuid']; ?>" <?php if($value['is_default'] ==1)echo "selected" ?>><?php echo $value['contract_period_type']; ?></option>  
							<?php
						}
					}
					?> 
				</select> 
				<label for="id_stock_1_product" class="control-label requiredField">Contract Period<span class="asteriskField"></span></label>
				</div>

				</div>
			</div>						
			<div class="col-md-3">
				<div class="form-group form-md-line-input form-md-floating-label">
				<input type="text" name="trigger_alert_expiry" value="10"  id="trigger_alert_expiry" data-validation="required" data-validation-error-msg="Please enter expiry alert" class="textinput form-control city edited">
				<label for="form_control_1">Expiry Alert</label>
				</div>
			</div>

		</div> 
	</div>
</div>
						



<?php }?>