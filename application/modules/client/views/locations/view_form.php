
<!-- END PAGE HEADER-->
<div class="row">
    <div>
	
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Locations</span>
				<span class="caption-subject"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					 <li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>						
					<li>
						<span>Locations</span> 
						<i class="fa fa-angle-right"></i>						
					</li>
					<li>
						<span>List</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('client/locations/create/'. $referrence_uuid); ?>"> Add New Location
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">

                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover order-column" id="role">
                    <thead>
                        <tr>
                            <th style="width:20%; text-align:left;"> Location Name </th>
							  <th style="width:20%; text-align:left;"> Location Short Name </th>
							  <th style="width:20%; text-align:left;"> Client Name </th>
                            <th  style="width:8%; text-align:left;"> Status </th>
                            <th  style="width:15%; text-align:left;"> Action </th>  
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($clientLoctions)) {
                            foreach ($clientLoctions as $value) {
                                //  pr($value);
								//if($value['is_deleted'] == 'N'){
                                ?>
                                <tr>
                                    <td> <?php echo $value['location_name']; ?> </td> 
									<td> <?php echo $value['location_short_name']; ?> </td> 
									<td> <?php echo $value['client_title']; ?> </td> 									
							                                   
                                    <td>
                                        <input type="checkbox" <?php
                                        if (!$value['is_disabled']) {
                                            echo 'checked';
                                        }
                                        ?>  id="onoffactiontoggle" class="onoffactiontoggle" myval="<?php echo $value['location_uuid']; ?>">
                                    </td>
									
                                    <td>
                                        <a href="<?php echo base_url() . 'client/locations/edit/'.$referrence_uuid.'/'. $value['location_uuid']; ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>  
                                         <!--<a href="<?php echo base_url() . 'client/locations/delete/'. $referrence_uuid.'/'.$value['location_uuid']; ?>"  userId="<?php echo $value['location_uuid'];?>" class="btn btn-xs red isdeleteAdminUsers" onlick=''>
                                                    <i class="fa fa-trash"></i>
                                        </a>-->
										<a  class="btn btn-xs  blue"  data-toggle="tooltip" title="Add Contacts" href="<?php echo base_url() . 'client/contacts/view/' . $value['location_uuid'] ;?>">
										<i class="fa fa-phone" aria-hidden="true" ></i>
										</a>										
 										
                                    </td>
                                </tr> 
                                <?php
								//}
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9"> No Record Found </td>
                            </tr> 
<?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client_locations.js" type="text/javascript"></script>
