<?php 
//pr($referrence_uuid);exit;

$address_types_select = '<select class="select form-control address_type edited" id="id_stock_1_unit" name="address_type[]" data-validation="required" data-validation-error-msg="Please select address type">';
if ($address_types['status'] == 'true') {
	foreach ($address_types['resultSet'] as $value) {
		
	   $address_types_select.='<option value="'.$value['address_type_uuid'].'">'.$value['address_type'].'</option>';  

	}
}				   
$address_types_select.='</select>';
//echo $address_types_select;exit;	
?>
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Edit Client</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>
					<li>
						<a href="<?php echo base_url('client/locations/view/'.$referrence_uuid) ?>">Locations</a>                            
						<i class="fa fa-angle-right"></i>
					</li>					
					<li>
						<span>Edit</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmClient" id="frmClient" method="post" action="">
                    <div class="form-body">
                        <div class="row"> 

                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="location_name" name="location_name" type="text" data-validation="required" data-validation-error-msg="Please enter location name" value="<?php echo ($getClient['resultSet']->location_name) ? $getClient['resultSet']->location_name : ''; ?>">
                                    <label for="form_control_1">Location Name</label>                                             
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="location_short_name" name="location_short_name" type="text" data-validation="required" data-validation-error-msg="Please enter location short name" value="<?php echo ($getClient['resultSet']->location_short_name) ? $getClient['resultSet']->location_short_name : ''; ?>">
                                    <label for="form_control_1">Location Short Name</label>                                               
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="location_erp_number" name="location_erp_number" type="text" value="<?php echo ($getClient['resultSet']->location_erp_number) ? $getClient['resultSet']->location_erp_number : ''; ?>">
                                    <label for="form_control_1">Location ERP Number</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="location_crm_number" name="location_crm_number" type="text" value="<?php echo ($getClient['resultSet']->location_crm_number) ? $getClient['resultSet']->location_crm_number : ''; ?>">
                                    <label for="form_control_1">Location CRM Number</label>                                               
                                </div>
                            </div>                            
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <textarea class="form-control" id="location_description" name="location_description" >
									<?php echo ($getClient['resultSet']->location_description) ? $getClient['resultSet']->location_description : ''; ?>
									</textarea>
                                    <label for="form_control_1">Location Description</label>                                               
                                </div>
                            </div>                            
                           
                     

                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="location_domain" name="location_domain" type="text" value="<?php echo ($getClient['resultSet']->location_domain) ? $getClient['resultSet']->location_domain : ''; ?>">
                                    <label for="form_control_1">Location Domain</label>                                               
                                </div>
                            </div>
							
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="location_salesperson_code" name="location_salesperson_code" type="text" value="<?php echo ($getClient['resultSet']->location_salesperson_code) ? $getClient['resultSet']->location_salesperson_code : ''; ?>">
                                    <label for="form_control_1">Location Salesperson Code</label>                                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="location_site_id" name="location_site_id" type="text" value="<?php echo ($getClient['resultSet']->location_site_id) ? $getClient['resultSet']->location_site_id : ''; ?>">
                                    <label for="form_control_1">Location Site Id</label>                                               
                                </div>
                            </div>
						


                    </div>						
							<!--<div class="col-sm-3">
								<label class="mt-checkbox mt-checkbox-outline" style="margin-top: 10px;">
								<input type="checkbox" name ="is_subclients" value="1" > No Subclients
								<span></span>
								</label>
							</div>-->


						<div class="panel-group" id="accordion">
							<div class="panel panel-default custom-panel">
								<div class="panel-heading client-panel-heading">
									<h3 class="sub-head">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
											<span>Address</span> <i class="more-less glyphicon glyphicon-minus pull-right"></i>
										</a>
									</h3>
								</div>
								<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
									<div class="panel-body">
										<div class="portlet-body pad0">                                
											<div class="input_fields_wrap wrapping">
							
											<?php
											if (!empty($getClientAddress)) {
												$i=0;
												foreach ($getClientAddress as $value) {
													
											?>

												<div  class="countdivs">
												<hr/>
												<div class="fieldRow clearfix">
												
												   <div class="col-md-3">
														 <div class="form-group form-md-line-input form-md-floating-label">
														
														 <input type="text" name="city[]" class="textinput form-control city"  value ="<?php if(isset($value['city'])){ echo $value['city'];}?>"/>
														  <label for="form_control_1">City</label>
														 </div>

												   </div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
														 <input type="text" name="district[]"  class="textinput form-control district" value ="<?php if(isset($value['district'])){ echo $value['district'];}?>"/>
														 <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField"></span></label>
														 </div>
														 
													  </div>
												   </div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														
														 <div class="form-group form-md-line-input form-md-floating-label">
														 
														 <input type="text" name="state[]"  class="textinput form-control state" value ="<?php if(isset($value['state'])){ echo $value['state'];}?>"/>
														  <label for="id_stock_1_product" class="control-label requiredField">State<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>	
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
														 <input type="text" name="country[]"  class="textinput form-control country" value ="<?php if(isset($value['country'])){ echo $value['country'];}?>"/>
														 <label for="id_stock_1_product" class="control-label">Country<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
														 <input type="text" name="postal_code[]"  class="textinput form-control postal_code" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values" value ="<?php if(isset($value['postal_code'])){ echo $value['postal_code'];}?>"/>
														 <label for="id_stock_1_product" class="control-label requiredField">Postal Code<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>
													<div class="col-md-3">
													  <div id="div_id_stock_1_service">
													
														 <div class="form-group form-md-line-input form-md-floating-label">
														 <input type="text" name="house_no[]"  class="textinput form-control house_no" value ="<?php if(isset($value['house_no'])){ echo $value['house_no'];}?>"/>
															 <label for="id_stock_1_product" class="control-label requiredField">House Number<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>

												   <div class="col-md-3">
													  <div id="div_id_stock_1_unit">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
															<select class="select form-control address_type edited" id="id_stock_1_unit" name="address_type[]">
															<?php
															if ($address_types['status'] == 'true') {
																foreach ($address_types['resultSet'] as $value1) {
																	?>
																   <option value="<?php echo $value1['address_type_uuid']; ?>" <?php if($value1['address_type_uuid'] == $value['address_type']){echo "selected";}?>><?php echo $value1['address_type']; ?></option>  
																	<?php
																}
															}
															?> 
															</select>
															<label for="id_stock_1_unit" class="control-label requiredField">Address Type<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>								   
													<div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
														 
														 <input type="text" name="street[]"  class="textinput form-control street" value ="<?php if(isset($value['street'])){ echo $value['street'];}?>"/>
														 <label for="id_stock_1_product" class="control-label requiredField">Street<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>	
													<div class="clearfix"></div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														
														 <div class="form-group form-md-line-input form-md-floating-label">
														 <textarea class="form-control full_address" rows="2" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter full address"><?php if(isset($value['full_address'])){ echo $value['full_address'];}?></textarea>
														  <label for="id_stock_1_product" class="control-label requiredField">Full Address<span class="asteriskField"></span></label>
														 </div>
													  </div>
												   </div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label">
														 
														 <input type="text" name="logitude[]"  class="textinput form-control logitude" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values" value ="<?php if(isset($value['logitude'])){ echo $value['logitude'];}?>"/>
														 <label for="id_stock_1_product" class="control-label requiredField">Longitude</label>
														 </div>
													  </div>
												   </div>
												   <div class="col-md-3">
													  <div id="div_id_stock_1_service">
														 
														 <div class="form-group form-md-line-input form-md-floating-label"><input type="text" name="latitude[]"  class="textinput form-control latitude" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values" value ="<?php if(isset($value['latitude'])){ echo $value['latitude'];}?>"/>
														 <label for="id_stock_1_product" class="control-label requiredField">Latitude</label>
														 </div>
													  </div>
												   </div>
												   <div class="col-md-2">
													  <div id="div_id_stock_1_service">										
														<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
														<div class="controls ">
														<div class="radio">
															<label><input type="radio" class="is_primary" name="is_primary" data-validation="required" data-validation-error-msg="Please select primary address" value="<?php echo $i;?>" <?php if(isset($value['is_primary']) && $value['is_primary'] ==1){ echo "checked";}?>/>Is Primary</label>
														</div> 
														 </div>
													  </div>
												   </div>
												<?php if($i >0){ ?>
												  <div class="col-md-1">
													 <div id="div_id_stock_1_service">
														<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
														<div class="controls "><a href="#" class="remove_field btn btn-danger mt-repeater-delete" address_uuid="<?php if(isset($value['address_uuid'])){ echo $value['address_uuid'];}?>"><i class="fa fa-close"  ></i>     Remove</a> </div>
													 </div>
												  </div>

												<?php }?>

												</div>
												
												</div>
												<input type="hidden" name="address_uuid[]"  class="textinput form-control" value ="<?php if(isset($value['address_uuid'])){ echo $value['address_uuid'];}?>" />
											<?php $i++;}}?>
								
													</div>
												<div>
													
												</div>
											</div> 
									</div>
								<div class="panel-footer">
									<button class="add_field_button btn blue btn-outline mt-repeater-add pull-right">Add More Address</button><div class="clearfix"></div>
								</div>
								</div>
							</div>
						</div>

                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/locations/view/'.$referrence_uuid); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>
    <script type="text/javascript">

$(document).ready(function() {
	var baseURL = $('#baseURL').val();
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
	var address_types_select = '<?php echo $address_types_select;?>'; //adddress types selectbox
    var x = 0;
    x = $('.countdivs').length; //initlal text box count
	
	
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
		//alert(x);
        if(x < max_fields){ //max input box allowed
			//alert(x);
            //x++; //text box increment
        }
            $(wrapper).append('<div class="countdivs"> <hr/> <div class="fieldRow clearfix"> <div class="col-md-3"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="city[]"   class="textinput form-control city"/> <label for="form_control_1">City</label> </div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="district[]" class="textinput form-control district"/> <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="state[]" class="textinput form-control state"/> <label for="id_stock_1_product" class="control-label requiredField">State<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="country[]" class="textinput form-control country"/> <label for="id_stock_1_product" class="control-label requiredField">Country<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="postal_code[]" class="textinput form-control postal_code" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values"/> <label for="id_stock_1_product" class="control-label requiredField">Postal Code<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="house_no[]" class="textinput form-control house_no"/> <label for="id_stock_1_product" class="control-label requiredField">House Number<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_unit"> <div class="form-group form-md-line-input form-md-floating-label"> '+address_types_select+' <label for="id_stock_1_unit" class="control-label requiredField">Address Type<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="street[]" class="textinput form-control street"/> <label for="id_stock_1_product" class="control-label requiredField">Street<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <textarea class="form-control full_address" rows="2" name="full_address[]" data-validation="required" data-validation-error-msg="Please enter full address"></textarea> <label for="id_stock_1_product" class="control-label requiredField">Full Address<span class="asteriskField"></span></label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"> <input type="text" name="logitude[]" class="textinput form-control logitude" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values"/> <label for="id_stock_1_product" class="control-label requiredField">Longitude</label> </div></div></div><div class="col-md-3"> <div id="div_id_stock_1_service"> <div class="form-group form-md-line-input form-md-floating-label"><input type="text" name="latitude[]" class="textinput form-control latitude" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values"/> <label for="id_stock_1_product" class="control-label requiredField">Latitude</label> </div></div></div><div class="col-md-2"> <div id="div_id_stock_1_service"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "> <div class="radio"><label><input type="radio" class="is_primary" name="is_primary" value="'+x+'" data-validation="required" data-validation-error-msg="Please select primary address">Is Primary</label></div></div></div></div><div class="col-md-1"> <div id="div_id_stock_1_service"> <label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label> <div class="controls "><a href="#" class="remove_field btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i> Remove</a> </div></div></div></div></div>'); //adding form
			x++;
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove button
		
		var address_uuid = $(this).attr('address_uuid');
			if(address_uuid != undefined){
				//alert(address_uuid);
				e.preventDefault(); $(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove(); x--;				
				$.ajax({
					type: 'POST',
					//dataType: 'json',
					url: baseURL + 'client/ajaxAddressRemove',
					data: {'address_uuid': address_uuid},
					success: function (response) {
						//$('#actiontabs').html(response);
					}
				}).done(function() {
					alert('ok');

				});	
			}else{
				//alert('notdefined');
				e.preventDefault(); $(this).parent('div').parent('div').parent('div').parent('div').parent('div').remove(); x--;			
			}
			var y=0;
			$('.is_primary').each(function(i, obj) {
				//alert(y);
				$(this).val(y);
				y++;
				//alert(val);

			});				
		//alert(x);
    });
});

</script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });

function recall()
{
	 $.validate();
}
$('#panel1').on('show.bs.collapse hidden.bs.collapse', function (e) {
	if (e.type == 'hidden') {
		$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
		$(window).bind('resize', function () {
			$('.dataTables_scrollBody').css({'height': (($(window).height()) - 195) + 'px'});
			//alert('resized');
		});
	} else {
		$('.dataTables_scrollBody').css({'height': '210px'});
	}
	$(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');


});
</script>