<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script> 
  <style>
  .btn-submit{margin-right:5px; margin-top:5px;}
  </style>
</head>
<body>
<div class="page-head">
	<!-- BEGIN PAGE TITLE -->
	<div class="page-title">
		<span class="caption-subject font-green-steel bold uppercase">Special handling notes</span>
		<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
	</div>
	<div class="page-toolbar">
		<ul class="page-breadcrumb breadcrumb custom-bread">
			<li>
				<i class="fa fa-cog"></i>
				<span>Managements</span>  
				<i class="fa fa-angle-right"></i>
			</li>
			<?php if($client_type == 'C'){
					?>
					<li>
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>	
				<?php }else{?>
					<li>
						<a href="<?php echo base_url('partner') ?>">Partner</a>                            
						<i class="fa fa-angle-right"></i>
					</li>
				<?php }?>						
			<li>
				<span>SHN</span>   
				<i class="fa fa-angle-right"></i>						
			</li>
								<li>
				<span>View</span>                            
			</li>
		</ul>
	</div>
	<!--<div class="page-title">
								
		<a href="" class="btn btn-xs  blue"></a>
																			</div>-->
	
	<div class="clearfix"></div>
</div>
<div class="manual_incident">
  <form action="" method="POST" enctype="multipart/form-data">	
	<div class="col-md-12">	
		<div class="form-group">
			<label for="comment" class="control-label col-md-12">Description:</label>
			<div class="col-md-12">
				<textarea class="form-control" rows="5" id="comment" name="shn_desc"><?php echo (isset($clientData['resultSet']['client_shn_link']))?$clientData['resultSet']['client_shn_link']:''; ?></textarea>
			</div>
		</div>
	</div>
	<div class="col-md-12">	
		<div class="form-group">
			<div class="col-md-12">
				<input class="form-control input-md" id="inputlg" type="file" name="userfile"  style="margin-top:10px;" value="">
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<input type="hidden" name="client" value = "<?php echo $clientId; ?>">
	<input type="hidden" name="old_file" value = "<?php if(isset($clientData['resultSet']['client_doc_url']) && !empty($clientData['resultSet']['client_doc_url'])) {echo $clientData['resultSet']['client_doc_url'];}?>">
	<div class="col-md-12"><button type="submit" class="btn btn-default btn-submit pull-right">Submit</button></div>
  </form>
  <?php if(isset($clientData['resultSet']['client_doc_url']) && !empty($clientData['resultSet']['client_doc_url'])) {?>
  <div class="col-md-12 remove_file">	
		<div class="form-group">
			<div class="col-md-12">
  <button type="button" class="btn btn-info" onclick="downloadFile('<?php echo $clientData['resultSet']['client_doc_url']; ?>')"><?php echo $clientData['resultSet']['client_doc_url']; ?></button> <button type="button" class="btn btn-danger" onclick="removeFile('<?php echo $clientId; ?>','<?php echo $clientData['resultSet']['client_doc_url']; ?>')">X</button>
  </div>
		</div>
	</div>
  

  <?php } ?>
  
</div>

<script>
 CKEDITOR.replace( 'shn_desc' );
 var Url = "<?php echo base_url('client/ClientShn/updateClient'); ?>";	
 function removeFile(id,val)
 {
	 if (confirm("Are you Sure?") == true) {
			var form_data = {clientId:id,file:val}
			$.ajax({
				url:Url,
				type: 'POST',
				data:form_data,
				success: function(response){
					if(response == 1)
					{
						$('.remove_file').remove();
					}else
					{
						alert("Please try again due to slow server response");
					}
				},
				
			});
		}
		else{
			return false;			
		}	
 }
 
 var DownloadUrl = "<?php echo base_url('client/ClientShn/download_files/'); ?>";	
 function downloadFile(val)
 {
	 if (confirm("Do you want to download the file?") == true) {
			var form_data = {file:val}
			var win = window.open(DownloadUrl+val, '_blank');			
		} 
		else{
			return false;			
		}	
 }
</script>

</body>
</html>
