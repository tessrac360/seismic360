<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Edit Gateway</span>
				<span class="caption-subject"><?php if($clientName['resultSet']['client_title']){
						echo " (".$clientName['resultSet']['client_title'].")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				<ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>	
					<li>
						<a href="<?php //echo base_url('client') ?>">Gateway</a>                            
						<i class="fa fa-angle-right"></i>
					</li>							
					<li>
						<span>Edit</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmClient" id="frmClient" method="post" action="">
                    <div class="form-body">
                        <div class="row">                                                                            
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="title" name="title" type="text" value="<?php echo ($getGateway['resultSet']->title) ? $getGateway['resultSet']->title : ''; ?>">
                                    <label for="form_control_1">Gateway Name</label>                                               
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="ip_address" name="ip_address" type="text" value="<?php echo ($getGateway['resultSet']->ip_address) ? $getGateway['resultSet']->ip_address : ''; ?>">
                                    <label for="form_control_1">IP Address</label>                                               
                                </div>
                            </div>
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/gateway'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>