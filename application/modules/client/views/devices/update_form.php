<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-ipaddress-master/bootstrap-ipaddress.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-ipaddress-master/bootstrap-ipaddress.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<style>
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
	margin:10px;
}
.accordion::after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}
.accordion.active::after {
    content: "\2212";
}

.accordion.active, .accordion:hover {
    background-color: #ddd; 
}

div.panel {
    padding: 0 18px;
    display: none;
    background-color: white;
}
</style>
<style>
.input-group input + span.error{
	position: absolute;
    right: 0;
    z-index: 9;
    text-align: center;
    top: 34px;
    background: #af6d6d;
    color: #fff;
    padding: 2px 6px;
    width: 65%;
    font-size: 12px;
}
.input-group input + span.error:before {
    content: '';
    position: absolute;
    border-style: solid;
    border-width:0 5px 5px;
    border-color: #af6d6d transparent;
    display: block;
    width: 0;
    z-index: 1;
    top: -5px;
    right: 55px;
}
</style>
<?php 
$locations_select = '<option value=""></option>';
if ($clientLocations['status'] == 'true') {
	foreach ($clientLocations['resultSet'] as $value) {
		
	   $locations_select.='<option value="'.$value['location_uuid'].'">'.$value['location_name'].'</option>';   
	}
}				   
?>
<div class="row">
    <div>
	
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Update Device</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>
					<li>
					   <a href="<?php echo base_url('client/devices/view/'.encode_url($referrence_uuid)) ?>">Devices</a>                           
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Update</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
				
				<!-- Device Form -->
                <form role="form" name="frmHost" id="frmHost" method="post" action="" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption font-dark"style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px;">
                                    <span class="caption-subject bold uppercase"> Device Information</span>
                                </div>
                            </div>
							<div class="clearfix"></div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="gateway_id" name="gateway_id"  data-validation="required" data-validation-error-msg="Please select gateway" autocomplete="off">
										<option value=""></option>
                                        <?php
                                        if ($getways['status'] == 'true') {
                                            foreach ($getways['resultSet'] as $value) {
												if($value['title'] !='NA'){
                                                ?>
                                               <option value="<?php echo $value['gateway_uuid']."#".$value['gateway_type']."#".$value['id']; ?>" <?php if($deviceDetails['resultSet']['gateway_uuid'] == $value['gateway_uuid'] ){echo "selected";}?>><?php echo $value['title']; ?></option>  
                                                <?php
												}
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Gateways</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control edited" id="device_category_id" name="device_category_id"  data-validation="required" data-validation-error-msg="Please select category" autocomplete="off">
										<option value=""></option>
                                        <?php
                                        if ($categories['status'] == 'true') {
                                            foreach ($categories['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['id']."#".$value['device_category_uuid']; ?>"  <?php if($deviceDetails['resultSet']['device_category_uuid'] == $value['device_category_uuid'] ){echo "selected";}?>><?php echo $value['category']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Device Category</label>
                                </div>
                            </div>
							
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control edited" id="uuid" name="uuid" >
										<option value=""></option>

                                    </select>                                    
                                    <label for="form_control_1">Device Sub-category</label>
                                </div>
                            </div>							
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="device_parent_id" name="device_parent_id" >
										<option value=""></option>
                                        <?php
                                        if ($deviceslist['status'] == 'true') {
                                            foreach ($deviceslist['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['device_id']."#".$value['uuid']; ?>"  <?php if($deviceDetails['resultSet']['uuid'] == $value['uuid'] ){echo "selected";}?>><?php echo $value['device_name']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Device Parent</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="device_location" name="device_location" >
										<option value=""></option>
                                        <?php
                                        if ($clientLocations['status'] == 'true') {
                                            foreach ($clientLocations['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['location_uuid']; ?>"  <?php if($deviceDetails['resultSet']['device_location'] == $value['location_uuid'] ){echo "selected";}?>><?php echo $value['location_name']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Location</label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="service_group_uuid" name="service_group_uuid" data-validation="required" data-validation-error-msg="Please select service group" autocomplete="off">
										<option value=""></option>
										<?php
                                        if ($seviceGroups['status'] == 'true') {
                                            foreach ($seviceGroups['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['service_group_uuid']; ?>"  <?php if($deviceDetails['resultSet']['service_group_uuid'] == $value['service_group_uuid'] ){echo "selected";}?>><?php echo $value['service_group_name']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                   
                                    <label for="form_control_1">Service Group</label>
                                </div>
                            </div>
							
                            <div class="col-md-3" style="display:none" id ="div_device_template">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="device_template_id" name="device_template_id" >
										<option value=""></option>
                                        <?php
                                        if ($deviceTemplates['status'] == 'true') {
                                            foreach ($deviceTemplates['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['device_template_id']; ?>"  <?php if($deviceDetails['resultSet']['device_template_id'] == $value['device_template_id'] ){echo "selected";}?>><?php echo $value['name']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Device Templates</label>
                                </div>
                            </div>							
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="device_name" name="device_name" value= "<?php echo isset($deviceDetails['resultSet']['device_name'])?$deviceDetails['resultSet']['device_name']:"";?>" type="text"  data-validation="required" data-validation-error-msg="Please enter device name" autocomplete="off">
                                    <label for="form_control_1">Device Name<span class="required" aria-required="true" >*</span></label>  
                                    <!--<span id="errmsg" class="error" style="display: none;"></span>-->
                                </div>
                            </div>
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label" id="IpDemo">
                                    <input class="form-control" id="address" name="address"  value= "<?php echo isset($deviceDetails['resultSet']['address'])?$deviceDetails['resultSet']['address']:"";?>" type="text"  data-validation="required" data-validation-error-msg="Please enter device  address" autocomplete="off">
                                    <label for="form_control_1">Device Address<span class="required"  aria-required="true">*</span></label>                                               
                                </div>
                            </div>	
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  class="form-control" id="alias" name="alias"  value= "<?php echo isset($deviceDetails['resultSet']['device_name'])?$deviceDetails['resultSet']['device_name']:"";?>"  type="text" >
                                    <label for="form_control_1">Device Alias Name<span class="required" aria-required="true"></span></label>                                               
                                </div>
                            </div>	

							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <textarea  class="form-control" id="device_description" name="device_description"  ><?php echo isset($deviceDetails['resultSet']['device_description'])?$deviceDetails['resultSet']['device_description']:"";?></textarea>
                                    <label for="form_control_1">Device Description<span class="required" aria-required="true"></span></label>                                               
                                </div>
                            </div>							
							
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  class="form-control" id="device_mac_address" name="device_mac_address"  value= "<?php echo isset($deviceDetails['resultSet']['device_mac_address'])?$deviceDetails['resultSet']['device_mac_address']:"";?>"  type="text" >
                                    <label for="form_control_1">Device Mac Address<span class="required" aria-required="true"></span></label>                                               
                                </div>
                            </div>

							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  class="form-control" id="device_serial_number" name="device_serial_number"  value= "<?php echo isset($deviceDetails['resultSet']['device_serial_number'])?$deviceDetails['resultSet']['device_serial_number']:"";?>"  type="text" >
                                    <label for="form_control_1">Device Serial Number<span class="required" aria-required="true"></span></label>                                               
                                </div>
                            </div>
							<div class="clearfix"></div>

							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  class="form-control" id="device_adr_building" name="device_adr_building" type="text"  value= "<?php echo isset($deviceDetails['resultSet']['device_adr_building'])?$deviceDetails['resultSet']['device_adr_building']:"";?>" >
                                    <label for="form_control_1">Device Address Building<span class="required" aria-required="true"></span></label>                                               
                                </div>
                            </div>

							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  class="form-control" id="device_adr_room" name="device_adr_room" type="text"  value= "<?php echo isset($deviceDetails['resultSet']['device_adr_room'])?$deviceDetails['resultSet']['device_adr_room']:"";?>" >
                                    <label for="form_control_1">Device Address Room<span class="required" aria-required="true"></span></label>                                               
                                </div>
                            </div>

							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  class="form-control" id="device_adr_rack" name="device_adr_rack" type="text"  value= "<?php echo isset($deviceDetails['resultSet']['device_adr_rack'])?$deviceDetails['resultSet']['device_adr_rack']:"";?>" >
                                    <label for="form_control_1">Device Address Rack<span class="required" aria-required="true"></span></label>                                               
                                </div>
                            </div>

							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  class="form-control" id="device_model_number" name="device_model_number" type="text"  value= "<?php echo isset($deviceDetails['resultSet']['device_model_number'])?$deviceDetails['resultSet']['device_model_number']:"";?>" >
                                    <label for="form_control_1">Device Model Number<span class="required" aria-required="true"></span></label>                                               
                                </div>
                            </div>

							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  class="form-control" id="device_manufacturer" name="device_manufacturer" type="text"  value= "<?php echo isset($deviceDetails['resultSet']['device_manufacturer'])?$deviceDetails['resultSet']['device_manufacturer']:"";?>" >
                                    <label for="form_control_1">Device Manufacturer<span class="required" aria-required="true"></span></label>                                               
                                </div>
                            </div>							
														
							<div class="clearfix"></div>
											
							
							<?php if(1==2){?>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="priority_id" name="priority_id" >
										<option value=""></option>
                                        <?php
                                        if ($priorities['status'] == 'true') {
                                            foreach ($priorities['resultSet'] as $value) {
                                                ?>
                                               <option value="<?php echo $value['skill_id']; ?>"><?php echo $value['title']; ?></option>  
                                                <?php
                                            }
                                        }
                                        ?> 
                                    </select>                                    
                                    <label for="form_control_1">Device Priority</label>
                                </div>
                            </div>	
							<?php }?>
							
							<div class="clearfix"></div>
							

							
						
						<div class="col-md-12" style="margin-top: 10px;margin-bottom:0px;">	
						<div id="host_template_list"></div>
						</div>
						 <div class="col-md-8 " style="margin-top: 10px;">
                        <div class="form-actions noborder">
                            <input type="hidden" name="type" value="service_from" >
						
                            <button type="submit" class="btn green">Save</button>


                        </div>	
						</div>						
                        </div>

                    </div>
                </form>
				
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script>
$(function() {
// setup validate
$.validate();
});
$(document).ready(function () {
     //$('#IpDemo').ipaddress();
     $('input[name=address]').ipaddress();
     $('#IpDemo').ipaddress();
	 var locations_select = '<?php echo $locations_select;?>';
	 //alert(locations_select);
	 	 
		$('#gateway_id').on('change', function () {
			var value = this.value;
			//var old_email = $('#old_email').val();
			var split = value.split("#");
			var v2 = split[1];
			var v1 = split[0];

			if( v2 == 1){
				$("#div_device_template").show();
			}else{
				$("#div_device_template").hide();	
			}
			var baseURL = $('#baseURL').val();
			$.ajax({
				type: 'POST',
				url: baseURL + 'client/devices/ajax_location',
				dataType: 'json',
				data: {'gateway_id': v1},
				success: function (response) {
					if (response.status == 'true') {
						$("#device_location").find('option').remove().end().append(locations_select);
						$("#device_location > option").each(function() {				
							if(response.location == $(this).val()){
								//alert(this.value);
								//alert(response.location);
								$( "#device_location" ).addClass( "edited" );
								$(this).attr('selected', 'selected');

							}
						});
						//swal("", response.message, "success");
						
					} else if (response.status == 'false') {
						$("#device_location").find('option').remove().end().append(locations_select);
					}
				}
			});	

		});	
}); 


$('#device_category_id').on('change', function () {
	var value = this.value;
	//var old_email = $('#old_email').val();
	var split = value.split("#");
	var split1 = $('#gateway_id').val().split("#");
	var device_category_uuid = split[1];
	var device_category_id = split[0];
	var gateway_type_uuid = split1[1];
	//alert(gateway_type_uuid);
	

	var baseURL = $('#baseURL').val();
	$.ajax({
		type: 'POST',
		url: baseURL + 'client/devices/ajax_sub_categories',
		dataType: 'json',
		data: {'device_category_uuid': device_category_uuid},
		success: function (response) {
			if (response.status == 'true') {
				$("#uuid").find('option').remove().end().append(response.subcategories);

				//swal("", response.message, "success");
				
			} else if (response.status == 'false') {
				$("#uuid").find('option').remove().end().append(response.subcategories);
			}
		}
	});

	$.ajax({
		type: 'POST',
		url: baseURL + 'client/devices/ajax_service_groups',
		dataType: 'json',
		data: {'device_category_id': device_category_id,'gateway_type_uuid':gateway_type_uuid},
		success: function (response) {
			$("#service_group_uuid").removeClass('edited');
			if (response.status == 'true') {
				$("#service_group_uuid").find('option').remove().end().append(response.servicegroups);

				//swal("", response.message, "success");
				
			} else if (response.status == 'false') {
				$("#service_group_uuid").find('option').remove().end().append(response.servicegroups);
			}
			$("#service_group_uuid").addClass('edited');
		}
	});	

});	

 
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}

</script>