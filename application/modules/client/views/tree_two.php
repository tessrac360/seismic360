<link rel=StyleSheet href="<?php echo base_url() . "public/" ?>treeview/jquery-simplefolders/main.css" type="text/css" media=screen>
<script src="<?php echo base_url() . "public/" ?>treeview/jquery-simplefolders/main.js"></script>
 <ul class=tree> 
    <li><?php echo $clientData['client_title'] ?>        
        <?php if (!empty($clientData['children'])) { ?>
         <div class="expander" ></div>
            <?php
            echo childTree($clientData['children']);
            ?>
        <?php } ?>
    </li>              
</ul>
<?php
function childTree($treeArray = array()) { ?>
    <ul>
        <?php foreach ($treeArray as $value) {
            ?>
            <li><?php echo $value['client_title'] ?>
                <?php
                if (!empty($value['children'])) {
                    echo '<div class=expander></div>';
                    echo childTree($value['children']);
                }
                ?>        
            </li>             
        <?php }
        ?>
    </ul>
<?php }
?>
