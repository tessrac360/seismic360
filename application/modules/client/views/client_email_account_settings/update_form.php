<div class="tabbable-custom hover-up" style="padding:0 5px;">
	<ul class="nav nav-tabs ">
		<li class="active">
			<a href="#Email Account" data-toggle="tab" aria-expanded="false"> Email Account Settings </a>
		</li>
		<li class="">
			<a href="#Email Template" data-toggle="tab" aria-expanded="false"> Email Template </a>
		</li>
	</ul>
<div class="tab-pane active row" id="Email Account">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Email Account Settings</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Account Mgmt</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('Partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>
					<li>
						<a href="<?php echo base_url('client/EmailAccountSettings/view/'.$reference_uuid) ?>">Email Settings</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Edit</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light  padd0">
		
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmEmailSettings" id="frmEmailSettings" method="post" action="">
                    <div class="form-body">
					<!--<h4 class="form-section">Person Info</h4>-->
                        <div class="row"> 
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control outgoing_host" id="outgoing_host" name="outgoing_host" type="text" data-validation="required" data-validation-error-msg="Please enter outgoing host" reference_uuid=" <?php echo $reference_uuid;?>" value="<?php if($getEmailSettingsEdit['status'] == 'true')echo ($getEmailSettingsEdit['resultSet']->outgoing_host) ? $getEmailSettingsEdit['resultSet']->outgoing_host : ''; ?>">
                                    <label for="form_control_1">Outgoing Host<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="outgoing_port" name="outgoing_port" type="text" data-validation="number" data-validation-error-msg="Please enter only numeric values" value="<?php if($getEmailSettingsEdit['status'] == 'true')echo ($getEmailSettingsEdit['resultSet']->outgoing_port) ? $getEmailSettingsEdit['resultSet']->outgoing_port : ''; ?>">
                                    <label for="form_control_1">Outgoing Port<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control incoming_host" id="incoming_host" name="incoming_host" type="text" data-validation="required" data-validation-error-msg="Please enter incoming host" reference_uuid=" <?php echo $reference_uuid;?>" value="<?php if($getEmailSettingsEdit['status'] == 'true')echo ($getEmailSettingsEdit['resultSet']->incoming_host) ? $getEmailSettingsEdit['resultSet']->incoming_host : ''; ?>">
                                    <label for="form_control_1">Incoming Host<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="incoming_port" name="incoming_port" type="text" data-validation="number" data-validation-error-msg="Please enter only numeric values" value="<?php if($getEmailSettingsEdit['status'] == 'true')echo ($getEmailSettingsEdit['resultSet']->incoming_port) ? $getEmailSettingsEdit['resultSet']->incoming_port : ''; ?>">
                                    <label for="form_control_1">Incoming Port<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control username" id="username" name="username" type="text" data-validation="required" data-validation-error-msg="Please enter username" tableName="email_account_settings" tableField="username" reference_uuid=" <?php echo $reference_uuid;?>" value="<?php if($getEmailSettingsEdit['status'] == 'true')echo ($getEmailSettingsEdit['resultSet']->username) ? $getEmailSettingsEdit['resultSet']->username : ''; ?>">
                                    <label for="form_control_1">Username<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="password" name="password" type="password" data-validation="required" data-validation-error-msg="Please enter password" value="<?php if($getEmailSettingsEdit['status'] == 'true')echo ($getEmailSettingsEdit['resultSet']->password) ? $getEmailSettingsEdit['resultSet']->password : ''; ?>">
                                    <label for="form_control_1">Password<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
                        </div>
						<div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/EmailAccountSettings/view/'.$reference_uuid); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
			
        </div>
    </div>
</div>
</div>
<style>
.portlet.light.padd0 {
    overflow: unset;
}
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>

<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client_email_settings.js" type="text/javascript"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });
</script>
