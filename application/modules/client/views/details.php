<?php error_reporting(0);?>
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Client Details</span>
			</div>
			<div class="page-toolbar">
				<ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('client') ?>">Client</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>List</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="portlet-body" >
                                    <div class="tabbable tabbable-tabdrop">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab1" data-toggle="tab">Client Details</a>
                                            </li>
                                            <li>
                                                <a href="#tab2" data-toggle="tab">Locations</a>
                                            </li>
                                            <li>
                                                <a href="#tab3" data-toggle="tab">Gateways</a>
                                            </li>
                                            <li>
                                                <a href="#tab4" data-toggle="tab">Devices</a>
                                            </li>	
                                            <li>
                                                <a href="#tab4" data-toggle="tab">Users</a>
                                            </li>											

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1">
                                                <p> I'm in Section 1. </p>
                                            </div>
                                            <div class="tab-pane" id="tab2">

													<!-- END PAGE HEADER-->
													<div class="row">
														<div>																																									<!-- BEGIN EXAMPLE TABLE PORTLET-->
															<div class="portlet light padd0">
																<div class="portlet-body padd-top0">
																	<div class="table-toolbar">
																		<div class="row">
																			<div class="col-md-9">
																				<div class="btn-group">
																					<a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('severities/create/'); ?>"> Add New Location
																						<i class="fa fa-plus"></i>
																					</a>
																				</div>
																			</div>
																			<div class="col-md-3">

																				<div class="btn-group pull-right">
																					<div class="input-group">
																						<input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
																						<input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
																						<span class="input-group-btn">
																							<button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<table class="table table-striped table-bordered table-hover order-column" id="role">
																		<thead>
																			<tr>
																				<th style="width:20%; text-align:left;"> Location Name </th>
																				<th  style="width:8%; text-align:left;"> Status </th>
																				<th  style="width:15%; text-align:left;"> Action </th>  
																			</tr>
																		</thead>
																		<tbody>
																			<?php
																			if (!empty($severities)) {
																				foreach ($severities as $value) {
																					//  pr($value);
																					//if($value['is_deleted'] == 'N'){
																					?>
																					<tr>
																						<td> <?php echo $value['severity']; ?> </td>     
																												   
																						<td>
																							<input type="checkbox" <?php
																							if ($value['status'] == 'Y') {
																								echo 'checked';
																							}
																							?>  id="onoffactiontoggle" class="onoffactiontoggle" myval="<?php echo encode_url($value['id']); ?>">
																						</td>
																						
																						<td>
																							<a href="<?php echo base_url() . 'severities/edit/'. encode_url($value['id']); ?>" class="btn btn-xs  blue">
																								<i class="fa fa-edit"></i>
																							</a>  
																							 <a href="<?php echo base_url() . 'severities/delete/'. encode_url($value['id']); ?>"  userId="<?php echo encode_url($value['id']);?>" class="btn btn-xs red isdeleteAdminUsers" onlick=''>
																										<i class="fa fa-trash"></i>
																									</a>
																							
																							
																						</td>
																					</tr> 
																					<?php
																					//}
																				}
																			} else {
																				?>
																				<tr>
																					<td colspan="9"> No Record Found </td>
																				</tr> 
													<?php } ?>
																		</tbody>
																	</table>
																</div>
																<div class="box-footer clearfix">
																	<ul class="pagination pagination-sm no-margin pull-right">
																		<?php //echo $this->pagination->create_links(); ?>
																	</ul>
																</div>
															</div>
														</div>
														</div>
														<!-- END EXAMPLE TABLE PORTLET-->


                                            </div>
                                            <div class="tab-pane" id="tab3">
                                                <p> Howdy, I'm in Section 3. </p>
                                            </div>
                                            <div class="tab-pane" id="tab4">
                                                <p> Howdy, I'm in Section 4. </p>
                                            </div>
                                            <div class="tab-pane" id="tab5">
                                                <p> Howdy, I'm in Section 5. </p>
                                            </div>												

                                        </div>
                                    </div>
									</div>
									</div>
									</div>

<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js" type="text/javascript"></script>
