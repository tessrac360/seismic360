
<!-- END PAGE HEADER-->

<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Create Device Category</span>
            </div>
            <div class="page-toolbar">
                <ul class="page-breadcrumb breadcrumb custom-bread">
                    <li>
                            <i class="fa fa-cog"></i>
                            <span>Account Mgmt</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                           <a href="<?php echo base_url('client/category/list_category')   ?>">Device Category</a>                       
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>Create</span>                            
                        </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmdevcategory" id="frmdevcategory" method="post" action="">
                    <div class="form-body">
                        <div class="row"> 
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control category" id="category" name="category" tableName='device_categories' tableField='category' type="text">
                                    <label for="form_control_1"> Category </label>                                               

                                </div>
                            </div>
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <!--<a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>

<script src="<?php echo base_url() . "public/" ?>js/form/form_device_cat_subcat.js" type="text/javascript"></script>






