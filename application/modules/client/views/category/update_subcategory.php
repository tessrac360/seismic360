
<!-- END PAGE HEADER-->
<?php if ($deviceDetails['status']=='true') { 
    $return = $deviceDetails['resultSet'];
    ?>
<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Create Device Category</span>
            </div>
            <div class="page-toolbar">
                <ul class="page-breadcrumb breadcrumb custom-bread">
                     <li>
                            <i class="fa fa-cog"></i>
                            <span>Account Mgmt</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                           <a href="<?php echo base_url('client/category/list_subcategory')   ?>">Device Sub-category</a>                       
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>Update</span>                            
                        </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmdevsubcategory" id="frmdevsubcategory" method="post" action="">
                    <div class="form-body">
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="device_sub_category_id" name="device_sub_category_id" >
                                        <option value=""></option> 
                                        <?php
                                        if ($category['status'] == 'true') {
                                            foreach ($category['resultSet'] as $value) {
                                                ?>
                                                <option <?php echo ($return->device_category_id == $value->id)?'selected':'';?> value="<?php echo $value->id.'#'. $value->device_category_uuid ?>"><?php echo $value->category ?></option>
                                                
                                                <?php
                                            }
                                        }
                                        ?>

                                    </select> 								
                                    <label for="form_control_1">Category</label>                                               
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control subcategory" id="subcategory" name="subcategory" tableName='device_sub_categories' tableField='subcategory' type="text" value="<?php echo ($return->subcategory)?$return->subcategory:'';?>">
                                    <label for="form_control_1">Sub Category </label>                                               
                                </div>
                            </div>
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <!--<a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>

<script src="<?php echo base_url() . "public/" ?>js/form/form_device_cat_subcat.js" type="text/javascript"></script>
