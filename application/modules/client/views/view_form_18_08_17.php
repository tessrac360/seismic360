<?php
$accessAdd = helper_fetchPermission('51', 'add');
$accessEdit = helper_fetchPermission('51', 'edit');
$accessStatus = helper_fetchPermission('51', 'active');
$accessDelete = helper_fetchPermission('51', 'delete');
?>
<style>
    .toggle.btn-xs {
        width: 66px !important;
        height: 21px !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('.tree').treegrid();
        $('.tree').treegrid('collapseAll');
    });
</script>
<link rel="stylesheet" href="<?php echo base_url() . "public/" ?>css/jquery.treegrid.css">
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Client</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Managements</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo base_url('client') ?>">Client</a>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>List</span>                            
                        </li>
                    </ul>


                </div>
            </div>
            <?php if ($subclients['resultSet']->is_subclients == 0) { ?>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-9">
                                <?php
                                if ($accessAdd == 'Y') {
                                    if ($client_id == 1) {
                                        ?> 
                                        <div class="btn-group">
                                            <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('client/create') ?>"> Add New Client
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-3">
                                <div class="btn-group pull-right">
                                    <div class="input-group">
                                        <input placeholder="Search...." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                        <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                        <span class="input-group-btn">
                                            <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover tree" id="role">
                        <tr>
                            <th> Client Name </th>
                            <th> Created By </th>
                            <?php if ($accessStatus == 'Y') { ?> 
                                <th style="text-align: center;"> Status </th><?php } ?>
                            <?php if ($accessEdit == 'Y') { ?> 
                                <th> Action </th>  <?php } ?>
                            <th> </th>
                        </tr>
                        <?php
                        if (!empty($client)) {
                            for ($i = 0; $i < count($client); $i++) {
                                //pr($client);
                                ?>
                                <tr class="treegrid-<?php echo $client[$i]['id']; ?>
                                <?php
                                if ($client_id != 1) {
                                    if ($i > 0) {
                                        ?>
                                            treegrid-parent-<?php
                                            echo $client[$i]['parent_client_id'];
                                        }
                                    } else {
                                        ?>
                                        <?php if ($client_id != $client[$i]['parent_client_id']) { ?>
                                            treegrid-parent-<?php
                                            echo $client[$i]['parent_client_id'];
                                        }
                                        ?>

                                    <?php } ?>
                                    " 
                                    >
                                    <td><?php echo $client[$i]['client_title'] ?></td><td><?php echo $client[$i]['name'] ?></td>
                                    <?php if ($accessStatus == 'Y') {
                                        ?> 
                                        <td align="center">
                                            <?php if ($i != 0) { ?>
                                                <div class="status_active_<?php echo $client[$i]['id']; ?>">
                                                    <?php if ($client[$i]['status'] == 'Y') { ?>
                                                        <a title="Active"  href="javascript:void(0);" class="client_status" myval="<?php echo $client[$i]['id']; ?>" status="N"> <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i> </a>
                                                    <?php } else { ?>
                                                        <a title="De-activate"  href="javascript:void(0);" class="client_status" myval="<?php echo $client[$i]['id']; ?>" status="Y"> <i class="fa fa-times-circle-o fa-lg" aria-hidden="true"></i> </a>
                                                    <?php } ?> 
                                                </div>


                                            <?php } elseif ($client_id == 1) { ?>

                                                <div class="status_active_<?php echo $client[$i]['id']; ?>">
                                                    <?php if ($client[$i]['status'] == 'Y') { ?>
                                                        <a title="Active" href="javascript:void(0);" class="client_status" myval="<?php echo $client[$i]['id']; ?>" status="N"> <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i> </a>
                                                    <?php } else { ?>
                                                        <a title="De-activate"  href="javascript:void(0);" class="client_status" myval="<?php echo $client[$i]['id']; ?>" status="Y"> <i class="fa fa-times-circle-o fa-lg" aria-hidden="true"></i> </a>
                                                    <?php } ?> 
                                                </div>

                                            <?php } ?>
                                        </td>
                                    <?php } ?>
                                    <?php if ($accessEdit == 'Y') { ?> 
                                        <td>
                                            <?php if ($i != 0) { ?>
                                                <a title="Edit" href="<?php echo base_url() . 'client/edit/' . encode_url($client[$i]['id']); ?>" class="btn btn-xs  blue">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            <?php if($client[$i]['is_delete_manually'] <=date("Y-m-d") && $client[$i]['is_delete_manually'] != '0000-00-00') { ?>
                                            <a title="Delete" href="javascript:void(0);"  clientId="<?php echo encode_url($client[$i]['id']); ?>" class="btn btn-xs red isdelete hideClientDelete_<?php echo $client[$i]['id']; ?>">
                                                                        <i class="fa fa-trash"></i>
                                                                    </a>
                                           <?php } ?>
                                                
                                            <?php } elseif ($client_id == 1) { ?>
                                                <a title="Edit" href="<?php echo base_url() . 'client/edit/' . encode_url($client[$i]['id']); ?>" class="btn btn-xs  blue">
                                                    <i class="fa fa-edit"></i>
                                                </a>	
                                                <?php if($client[$i]['is_delete_manually']<=date("Y-m-d") && $client[$i]['is_delete_manually'] != '0000-00-00') { ?>
                                            <a title="Delete" href="javascript:void(0);"  clientId="<?php echo encode_url($client[$i]['id']); ?>" class="btn btn-xs red isdelete hideClientDelete_<?php echo $client[$i]['id']; ?>">
                                                                        <i class="fa fa-trash"></i>
                                                                    </a>
                                           <?php } ?>
                                            <?php } ?>
                                            
                                             <?php
                                            if ($client[$i]['id'] != $client[$i]['primary_client_id']) {
                                                if ($i != 0) {
                                                    ?>
                                                    <a title="Move" href="<?php echo base_url() . 'client/moveclient/' . encode_url($client[$i]['id']) . "/" . encode_url($client[$i]['parent_client_id']); ?>"  userId="<?php echo encode_url($client[$i]['id']); ?>" class="glyphicon glyphicon-move">

                                                    </a>
                                                <?php } elseif ($client_id == 1) { ?>
                                                    <a title="Move" href="<?php echo base_url() . 'client/moveclient/' . encode_url($client[$i]['id']) . "/" . encode_url($client[$i]['parent_client_id']); ?>"  userId="<?php echo encode_url($client[$i]['id']); ?>" class="glyphicon glyphicon-move">

                                                    </a>													
                                                <?php }
                                            } ?>
                                        </td>
                                    <?php } ?>	
                                        

                                    <td>

                                        <a href="<?php echo base_url() . 'client/create/?cli=' . encode_url($client[$i]['id']) . '&lvl=' . encode_url($client[$i]['level']); ?>">

                                            <button class="btn blue" style=" padding: 2px 2px;font-size:12px;" id="addsubclient" type="button">Add Subclient</button>

                                        </a>	
                                        <a href="<?php echo base_url() . 'nagiosmanagements/nagios_config/' . encode_url($client[$i]['id']) ; ?>">

                                            <button class="btn blue" style=" padding: 2px 2px;font-size:12px;" id="addsubclient" type="button">Manage Config Files</button>
											

                                        </a>										

                                    </td>										
                                </tr>		
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="4"> No Record Found </td>
                            </tr> 
                        <?php } ?>

                    </table>



                </div>
            <?php } else { ?>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-12">
                                Subclients Restricted.
                            </div>
                        </div>
                    </div>
                </div>						
            <?php } ?>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">

                    <?php
                    if ($search == '') {
                        echo $this->pagination->create_links();
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<style type="text/css">

    </style>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.treegrid.js" type="text/javascript"></script>


