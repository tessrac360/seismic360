<div class="row">
    <div>
		<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<span class="caption-subject font-green-steel bold uppercase">Clients Bulk Upload</span>
			<span style="color:black;"><?php if(isset($clientName['resultSet']['client_title'])){
					echo " (".$clientName['resultSet']['client_title'].")";
				} ?>
			</span>
		</div>
		<div class="page-toolbar">
			 <ul class="page-breadcrumb breadcrumb custom-bread">
				<li>
					<i class="fa fa-cog"></i>
					<span>Managements</span>  
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="<?php echo base_url('client') ?>">Client</a>                            
					<i class="fa fa-angle-right"></i>
				</li>						
				<li>
					<span>Clients Bulk Upload </span>                            
				</li>
			</ul>
		</div>
		<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmClient" id="frmClient" method="post" action="" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <label for="form_control_1">1) Column name must be client title</label>                                               
                                </div>
                            </div>	
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <label for="form_control_1">2) Uploaded file must be in csv format</label>                                               
                                </div>
                            </div>							
							<div class="col-md-12">
							<div class="form-actions noborder">
								<input type="file" name="userfile" ><br><br>
								<input type="submit" name="submit" value="UPLOAD" class="btn btn-primary">
								&nbsp; &nbsp; <a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>
							</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
	</div>
</div>
