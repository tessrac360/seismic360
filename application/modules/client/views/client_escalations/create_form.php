<?php //pr($getEscalationIds['resultSet']);exit; ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.css">
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Contacts</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Account Mgmt</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>
					<li>
						<a href="<?php echo base_url('client/ClientEscalations/view/'.$reference_uuid) ?>">Escalations</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light  padd0">
		
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmEscalations" id="frmEscalations" method="post" action="">
                    <div class="form-body">
					<!--<h4 class="form-section">Person Info</h4>-->
                        <div class="row"> 
							<div class="col-md-12" style="padding: 10px 0 10px 15px">
								<label class="control-label col-md-3" for="inputWarning">Escalation Groups:<span class="required" aria-required="true">*</span></label>
								
								<select class="form-control escalation_group_uuid" name="escalation_group_uuid" id="escalation_group_uuid">
								   <option value="">Select options</option>
								
								<?php
								if ($getEscalaltionGroups['status'] == 'true') {
									foreach ($getEscalaltionGroups['resultSet'] as $value) {
										//pr($value);exit;
										?>
										<option value="<?php echo $value['escalation_group_uuid']; ?>"   <?php if (in_array($value['escalation_group_uuid'], $getEscalationIds)) echo 'disabled'; ?>><?php echo $value['escalation_group_name']; ?></option>  
										<?php
									}
									
								}
								?> 
								
								</select>
							
								<div class="col-sm-12" id="assign_error"></div>
                            </div>
							<div class="col-md-12" style="padding: 10px 0 10px 15px">
								<label class="control-label col-md-3" for="inputWarning">Contacts:<span class="required" aria-required="true">*</span></label>
								<select class="form-control contact_uuid sd" name="contact_uuid[]" id="contact_uuid" multiple="multiple" >
									
								<?php
								if ($getContactGroups['status'] == 'true') {
									foreach ($getContactGroups['resultSet'] as $value) {
										?>
										<option value="<?php echo $value['contact_uuid']; ?>"><?php echo $value['contact_name']; ?></option>  
										<?php
									}
								}
								?> 
								</select>
								<div class="col-sm-12" id="assign_error"></div>
                            </div>							
							
                        </div>
						<div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/ClientEscalations/view/'.$reference_uuid); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
			
        </div>
    </div>
</div>
<style>
.portlet.light.padd0 {
    overflow: unset;
}
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script>
$(':checkbox[readonly]').click(function(){ return false; });
(function($) {
     $(function() {
        $('.escalation_group_uuid').fSelect();
        $('.contact_uuid').fSelect();
       
    }); 
})(jQuery);
</script> 

<script src="<?php echo base_url() . "public/" ?>js/form/form_client_escalations.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.js" type="text/javascript"></script>	
