<?php //pr($getContactGroupsIds);?>
<?php //pr($getGroupName['resultSet']);?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.css">
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Edit Contacts</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Account Mgmt</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>
					<li>
						<a href="<?php echo base_url('client/ClientEscalations/view/'.$reference_uuid) ?>">Escalations</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Edit</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light  padd0">
			<?php
            if ($getEscalationsAssignGroupEdit['status'] = 'true') {
               
                $getResult = $getEscalationsAssignGroupEdit['resultSet'];
                //pr($getResult);exit;
                ?>
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmUpdateEscalations" id="frmUpdateEscalations" method="post" action="">
                    <div class="form-body">
					<!--<h4 class="form-section">Person Info</h4>-->
                        <div class="row"> 
							<!--<div class="col-md-12" style="padding: 10px 0 10px 15px">
								<span class="caption-subject  uppercase col-md-3" > Escalation Group  : </span> &nbsp; &nbsp;
								<select class="form-control escalation_group_uuid sd" name="escalation_group_uuid" id="escalation_group_uuid">
								   <option value="">Select options</option>
								<?php
								if ($getEscalaltionGroups['status'] == 'true') {
									foreach ($getEscalaltionGroups['resultSet'] as $value) {
										//pr($value);
										?>
										<option value="<?php echo $value['escalation_group_uuid']; ?>" <?= ($getResult->escalation_group_uuid == $value['escalation_group_uuid']) ? 'selected' : ''; ?>><?php echo $value['escalation_group_name']; ?></option> 
										<?php
										
										
									}
								}
								?> 
								</select>
								<div class="col-sm-12" id="assign_error"></div>
                            </div>-->
							
							<div class="col-md-12" style="padding: 10px 15px 10px 10px">	
								<div class="form-group">
									<label class="control-label col-md-3" for="inputWarning">Escalation Groups:<span class="required" aria-required="true">*</span></label>
									<div class="col-md-2">
										<input class="form-control" type="hidden" id="escalation_group_uuid" name="escalation_group_uuid" value="<?php echo $getGroupName['resultSet']['escalation_group_uuid']?>">
										<input class="form-control" type="text" id="escalation_group_name" name="escalation_group_name" value="<?php echo ($getGroupName['resultSet']['escalation_group_name']) ? $getGroupName['resultSet']['escalation_group_name'] : ''; ?>" readonly>
										
									</div>
								</div>	
							</div>	
							<div class="col-md-12" style="padding: 10px 1px 10px 10px">
								<label class="control-label col-md-3" for="inputWarning">Contacts:<span class="required" aria-required="true">*</span></label>
								<select class="form-control contact_uuid sd" name="contact_uuid[]" id="contact_uuid" multiple="multiple">
									
								<?php
								if ($getContactGroups['status'] == 'true') {
									foreach ($getContactGroups['resultSet'] as $value) {
										?>
										<option value="<?php echo $value['contact_uuid']; ?>" <?php if (in_array($value['contact_uuid'], $getContactGroupsIds)) echo 'selected'; ?> id="<?php echo $value['contact_uuid']; ?>"><?php echo $value['contact_name']; ?></option>  
										<?php
									}
								}
								?> 
								</select>
								<div class="col-sm-12" id="assign_error"></div>
                            </div>							
							
                        </div>
						<div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/ClientEscalations/view/'.$reference_uuid); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
			<?php } ?>
        </div>
    </div>
</div>
<style>
.portlet.light.padd0 {
    overflow: unset;
}

    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script>
$(':checkbox[readonly]').click(function(){ return false; });
(function($) {
     $(function() {
        $('.escalation_group_uuid').fSelect();
        $('.contact_uuid').fSelect();
       
    }); 
})(jQuery);
</script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client_escalations.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.js" type="text/javascript"></script>	
