<!--<script src="<?php echo base_url() . "public/" ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>-->
<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>
<!--<script type="text/javascript" src="<?php echo base_url() . "public/" ?>treeview/treejs/js/jquery.tree.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>treeview/treejs/css/jquery.tree.css"/>-->

<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Edit Group</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Account Mgmt</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                           <a href="<?php echo base_url('client/EscalationGroups') ?>">Escalation Group</a>                           
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>Create</span>                            
                        </li>
                    </ul>                                                        
                </div>
            </div>
            <div class="portlet-body form">

                <form role="form" name="frmEscalationGroup" id="frmEscalationGroup" method="post" action="" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase"> Basic Information</span>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control escalation_group_name" id="escalation_group_name" name="escalation_group_name" type="text" tableName='escalation_groups' tableField='escalation_group_name' value ="<?php echo ($getEscalationGroupByID['resultSet']->escalation_group_name) ? $getEscalationGroupByID['resultSet']->escalation_group_name : ''; ?> ">
                                    <label for="form_control_1">Group Name<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
                        </div>   
                        <div class="form-actions noborder">
                            
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/EscalationGroups'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(':checkbox[readonly]').click(function(){ return false; });
</script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_escalation_group.js" type="text/javascript"></script>