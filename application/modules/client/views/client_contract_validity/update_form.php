
<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>


<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Edit Contract Validity</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Account Mgmt</span>  
                            <i class="fa fa-angle-right"></i>
                        </li> 
						
                        <li>
                           <a href="<?php echo base_url('client/ContractValidations') ?>">Contract Validity</a>                           
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>Edit</span>                            
                        </li>
                    </ul>                                                        
                </div>
            </div>
            <div class="portlet-body form">

                <form role="form" name="frmContractValidity" id="frmContractValidity" method="post" action="" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase"> Basic Information</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control validity_type" id="validity_type" name="validity_type" type="text" tableName='contract_validity' tableField='validity_type' data-validation="required" data-validation-error-msg="Please enter validity type" uuid="<?php echo $uuid;?>" value="<?php echo ($getContractValidity['resultSet']->validity_type) ? $getContractValidity['resultSet']->validity_type : ''; ?>">
                                    <label for="form_control_1">Validity Type<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control validity_duration" id="validity_duration" name="validity_duration" type="text" tableName='contract_validity' tableField='validity_duration' data-validation="number" data-validation-error-msg="Please enter numeric values(Ex- No.of Days)" value="<?php echo ($getContractValidity['resultSet']->validity_duration) ? $getContractValidity['resultSet']->validity_duration : ''; ?>">
                                    <label for="form_control_1">Validity Duration<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
							<div class="col-md-3">
								<div id="div_id_stock_1_service">					
									<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
									<div class="controls ">
										<div class="radio">
											<label><input type="radio" class="isDefault" name="is_default" value="1" <?php if ($getContractValidity['resultSet']->is_default == '1'){ echo 'checked="checked"'  ; echo " disabled"; }?>/>Is Default</label>
										</div> 
									</div>
								</div>
							</div>
							
                        </div>   
                        <div class="form-actions noborder">
                            
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/ContractValidations'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(':checkbox[readonly]').click(function(){ return false; });
</script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_contract_validity.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });
</script>