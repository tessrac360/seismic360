
<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>


<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Create Contract Period</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Account Mgmt</span>  
                            <i class="fa fa-angle-right"></i>
                        </li> 
						
                        <li>
                           <a href="<?php echo base_url('client/contractperiod') ?>">Contract Period</a>                           
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>Create</span>                            
                        </li>
                    </ul>                                                        
                </div>
            </div>
            <div class="portlet-body form">

                <form role="form" name="frmContractValidity" id="frmContractValidity" method="post" action="" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase"> Basic Information</span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control contract_period_type" id="contract_period_type" name="contract_period_type" type="text" tableName='contract_period' tableField='contract_period_type' data-validation="required" data-validation-error-msg="Please enter Period Type">
                                    <label for="form_control_1">Contract Period Type<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control contract_period" id="contract_period" name="contract_period" type="text" tableName='contract_period' tableField='contract_period' data-validation="number" data-validation-error-msg="Please enter numeric values(Ex- No.of Days)">
                                    <label for="form_control_1">Contract Duration<span class="required" aria-required="true">*</span></label>
<!--                                    <span class="help-block">Enter your name...</span>-->
                                </div>
                            </div>
							<?php if($isDefault['status']== 'true'){?>
							<div class="col-md-3">
								<div id="div_id_stock_1_service">					
									<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
									<div class="controls ">
										<div class="radio">
											<label><input type="radio" class="isDefault" name="is_default" value="1"/>Is Default</label>
										</div> 
									</div>
								</div>
							</div>
							<?php }else{?>
							<div class="col-md-4">
								<div id="div_id_stock_1_service">					
									<label for="id_stock_1_product" class="control-label requiredField">&nbsp;</label>
									<div class="controls ">
										<div class="radio">
											<label><input type="radio" class="isDefault" name="is_default" value="1" checked="checked" />Is Default</label>
										</div> 
									</div>
								</div>
							</div>
							<?php }?>
                        </div>   
                        <div class="form-actions noborder">
                            
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/Contractperiod'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(':checkbox[readonly]').click(function(){ return false; });
</script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_contract_validity.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });
</script>