<?php //pr($getEscalationIds['resultSet']);exit; ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>assets/global/scripts/fselect/fSelect.css">
<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Holidays</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Account Mgmt</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }?>
					<li>
						<a href="<?php echo base_url('client/ClientHolidays/view/'.$reference_uuid) ?>">Holidays</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light  padd0">
		
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmHolidays" id="frmHolidays" method="post" action="">
                    <div class="form-body">
					<!--<h4 class="form-section">Person Info</h4>-->
                        <div class="row"> 
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input  type="text" class="form-control date-picker holiday date" id="holiday_date" name="holiday_date" autocomplete="off" value="" data-validation="required" data-validation-error-msg="Please enter holiday occasion" tableName="client_holidays" tableField="holiday_date" >
                                    <label for="form_control_1">Holiday Date<span class="required" aria-required="true">*</span></label>                                 
                                </div>
                            </div>
							<div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input class="form-control" id="holiday_occasion" name="holiday_occasion" type="text" data-validation="required" data-validation-error-msg="Please enter holiday occasion" tableName="client_holidays" tableField="holiday_occasion">
                                    <label for="form_control_1">Holiday Occasion<span class="required" aria-required="true">*</span></label>                                               
                                </div>
                            </div>
                        </div>
						<div class="form-actions noborder">
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client/ClientHolidays/view/'.$reference_uuid); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
			
        </div>
    </div>
</div>
<style>
.portlet.light.padd0 {
    overflow: unset;
}
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>

<script src="<?php echo base_url() . "public/" ?>js/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client_holidays.js" type="text/javascript"></script>
<script>
  $(function() {
    // setup validate
    $.validate();
  });
</script>
<script>
$(function(){
	$(".date").keydown(function(e){ e.preventDefault(); });
    $('.date').datepicker({
        dateFormat: 'yyyy-mm-dd',
        //startDate: '-100y',
		//endDate: '-10y',
		//endDate: '+0d',
        //autoclose: true
    });
});
</script>