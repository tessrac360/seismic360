<?php

class Model_sla extends CI_Model {
	
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */
    
    public function __construct() {
        parent::__construct();
        $this->eventsla = 'events_sla';
        $this->incidentsla = 'incident_sla';
        $this->skills = 'skill_set';
        $this->tablename = 'client';
        $this->days = 'days';
        $this->users = 'users';
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
    }

    // public function isExitGateway($id) {
        // $this->db->select('id');
        // $this->db->from($this->eventsla);
        // $this->db->where('id', $id);
        // $query = $this->db->get();
       //echo $this->db->last_query();exit;
        // if ($query->num_rows() > 0) {
            // $return['status'] = 'true';
        // } else {
            // $return['status'] = 'false';
        // }
        // return $return;
    // }

    public function insertSla($postData = array()){
		$this->db->insert($this->eventsla, $postData);
		//echo $this->db->last_query();exit;
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
	
	public function insertIncSla($postData = array()){
		$this->db->insert($this->incidentsla, $postData);
		//echo $this->db->last_query();exit;
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$return['status'] = 'true';
			$return['lastId'] = $insert_id;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }

    public function updateSla($postData = array(), $id) {
		$this->db->where('sla_id', $id);
        $update_status = $this->db->update($this->eventsla, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function updateIncSla($postData = array(), $id) {
		$this->db->where('sla_id', $id);
        $update_status = $this->db->update($this->incidentsla, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function getSlaPagination($search = '', $client_ids = 0, $limit = 0, $start = 0) {
        $this->db->select('g.*, cl.client_title');
        $this->db->from($this->eventsla . ' as g');
		$this->db->join('client as cl', 'cl.id = g.client_id');
        $this->db->where_in('g.client_id', $client_ids);
        $this->db->order_by('g.sla_type', "asc");
        if ($start >= 0 and $limit > 0 and $search == '') {
            $this->db->limit($limit, $start);
        }
        $sla = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $sla->num_rows();
        }
       //pr($allClients);exit;
        return $sla->result_array();
    }

    public function getIncSlaPagination($search = '', $client_ids = 0, $limit = 0, $start = 0) {
        $this->db->select('g.*, cl.client_title');
        $this->db->from($this->incidentsla . ' as g');
		$this->db->join('client as cl', 'cl.id = g.client_id');
        $this->db->where_in('g.client_id', $client_ids);
        $this->db->order_by('g.sla_type', "asc");
        if ($start >= 0 and $limit > 0 and $search == '') {
            $this->db->limit($limit, $start);
        }
        $sla = $this->db->get();
        //echo $this->db->last_query();exit;		
        if ($limit == 0 && $start == 0) {
            return $sla->num_rows();
        }
       //pr($allClients);exit;
        return $sla->result_array();
    }


    public function getSlaSearchData($search, $client_ids = "",$limit = "", $start = "", $order_by = "sla_id desc", $order_type = "") {
        $this->db->select('g.*');
        $this->db->from($this->eventsla . ' as g');
		//$this->db->join('', $this->days, $working_day);
        $this->db->where_in('g.client_id', $client_ids);
        if ($search != "") {
            $this->db->like('g.sla_type', $search);
        }
        $this->db->order_by('g.sla_id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        } else {
            $data = array();
        }
		//pr($data);exit;

        return $data;
    }

    public function getIncSlaSearchData($search, $client_ids = "",$limit = "", $start = "", $order_by = "sla_id desc", $order_type = "") {
        $this->db->select('g.*');
        $this->db->from($this->incidentsla . ' as g');
		//$this->db->join('', $this->days, $working_day);
        $this->db->where_in('g.client_id', $client_ids);
        if ($search != "") {
            $this->db->like('g.sla_type', $search);
        }
        $this->db->order_by('g.sla_id', "desc");
        if ($start >= 0 and $limit > 0) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        } else {
            $data = array();
        }
		//pr($data);exit;

        return $data;
    }

    public function getSlaEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->eventsla);
        $this->db->where('sla_id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

    public function getIncSlaEdit($postId = "") {
        $this->db->select('*');
        $this->db->from($this->incidentsla);
        $this->db->where('sla_id', $postId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->row();
        } else {
            $return['status'] = 'false';
            $return['msg'] = 'Records not found';
        }
        //pr($return);exit;
        return $return;
    }

	public function isDeleteSla($postId) {
        if ($postId != "") {
            $data = array('status' => 'Y');
            $this->db->where('sla_id', $postId);
            $this->db->update($this->eventsla, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

	public function isIncDeleteSla($postId) {
        if ($postId != "") {
            $data = array('status' => 'Y');
            $this->db->where('sla_id', $postId);
            $this->db->update($this->incidentsla, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('sla_id', $id);
            $this->db->update($this->eventsla, $data);
            echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }	
	
    public function updateIncStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            // pr($data);exit;
            $this->db->where('sla_id', $id);
            $this->db->update($this->incidentsla, $data);
            echo $this->db->last_query();exit;  
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }
	
	public function getClientName($clientId = "") {
        $this->db->select('client_title,client_type');
        $this->db->from('client');
        $this->db->where('id', $clientId);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

	public function getSeverities() {
        $this->db->select('skill_id, title');
        $this->db->from('skill_set');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->result_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
}

?>