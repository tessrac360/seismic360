<?php 

$sla = $getSla['resultSet']->response_sla;
$ev_sla = sprintf('%02d:%02d:%02d', ($sla/3600),($sla/60%60), $sla%60); 


$evsla = $getSla['resultSet']->event_response_sla;
$ev_res_sla = sprintf('%02d:%02d:%02d', ($evsla/3600),($evsla/60%60), $evsla%60); 

$mtrs_sla = $getSla['resultSet']->mtrs_sla;
$ev_mtrs_sla = sprintf('%02d:%02d:%02d', ($mtrs_sla/3600),($mtrs_sla/60%60), $mtrs_sla%60); 
 
$mttr_sla = $getSla['resultSet']->mttr_sla;
$ev_mttr_sla = sprintf('%02d:%02d:%02d', ($mttr_sla/3600),($mttr_sla/60%60), $mttr_sla%60); 

?>

<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Edit Incident SLA</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				<ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_page){
							if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }}?>	
					<li>
						<a href="<?php echo base_url('sla/incident/'.encode_url($client_id)) ?>">SLA</a>                            
						<i class="fa fa-angle-right"></i>
					</li>							
					<li>
						<span>Edit</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light padd0">
            <div class="portlet-body form padd-top0">
                <form role="form" name="frmGateway" id="form" method="post" action="" onsubmit="return validateForm()" >
                    <div class="form-body">
                        <div class="row">                                                                            
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
									<input  class="form-control" type="text" id="admin_name" name="client_name"  value="<?php echo $clientName['resultSet']['client_title'];?>" disabled>
									<input  class="form-control" type="hidden" id="client_id" name="client_id"  value="<?php echo $client_id; ?>" >
                                    
                                    <label for="form_control_1">Client</label>                                               
                                </div>
                            </div>
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control severity" id="priority_id" name="priority_id" tableName="incident_sla" tableField="priority_id" editval="<?php echo $getSla['resultSet']->priority_id;?>" client_id = "<?php echo $client_id; ?>" required>
										<option></option>
										<?php
                                            foreach ($priority['resultSet'] as $values) { ?>
											
											<option value="<?php echo $values['skill_id'];?>" <?php if($getSla['resultSet']->priority_id ==$values['skill_id']) echo "selected"; ?>> <?php echo $values['title'];?></option>
											
										<?php } ?>
									</select> 
									<label for="form_control_1">Severity</label>                                               
                                </div>
                            </div>
							<div id="request_typediv"> 
							<?php if($getSla['resultSet']->priority_id == 4){?>
						
							<div class="col-md-3 ">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="request_type" name="request_type" required>
										<option></option>
										<option value="move" <?php if($getSla['resultSet']->request_type == 'move'){ echo "selected"; } ?> > Move </option>
										<option value="add" <?php if($getSla['resultSet']->request_type == 'add'){ echo "selected"; } ?>> Add </option>
										<option value="change" <?php if($getSla['resultSet']->request_type == 'change'){ echo "selected"; } ?>> Change </option>
										<option value="delete" <?php if($getSla['resultSet']->request_type == 'delete'){ echo "selected"; } ?>> Disconnect/Delete </option>
									</select> 
									<label for="form_control_1">Request Type</label>                                               
                                </div>
                            </div>									
							<?php } ?>
							</div>
							<input class="form-control" id="sla_type" name="sla_type" type="hidden" value="<?php echo ($getSla['resultSet']->sla_type) ? $getSla['resultSet']->sla_type : ''; ?>" required>
                                   
							<input class="form-control" id="days" name="days" type="hidden">
							<input class="form-control" id="hours" name="hours" type="hidden">
							<div class="clearfix"></div>

							<div class="form-group form-md-line-input has-info col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
									</span>
									<input class="form-control" id="event_response_sla" name="event_response_sla" type="text" onchange="validateTime(this);" value="<?php echo $ev_res_sla; ?>" required>
                                    <label for="form_control_1">Event Response SLA</label>
									<span class="help-block"> hr:mm:ss </span>
								</div>
							</div>							
							<div class="form-group form-md-line-input has-info col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
									</span>
									<input class="form-control" id="response_slas" name="response_slas" type="text" onchange="validateTime(this);" value="<?php echo $ev_sla; ?>" required>
                                    <label for="form_control_1">Response SLA</label>
									<span class="help-block"> hr:mm:ss </span>
								</div>
							</div>
							
							<div class="form-group form-md-line-input has-info col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
									</span>
									 <input class="form-control" id="mtrs_slas" name="mtrs_slas" type="text" onchange="validateTime(this);" value="<?php echo $ev_mtrs_sla; ?>" required>
                                    <label for="form_control_1">MTRS SLA</label>
									<span class="help-block"> hr:mm:ss </span>
								</div>
							</div>
							
							<div class="form-group form-md-line-input has-info col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
									</span>
									<input class="form-control" id="mttr_slas" name="mttr_slas" type="text"  onchange="validateTime(this);" value="<?php echo $ev_mttr_sla; ?>" required>
                                    <label for="form_control_1">MTTR SLA</label>
									<span class="help-block"> hr:mm:ss </span>
								</div>
							</div>
									
							
							<div class="col-md-6">                                
									<div class="form-group  form-md-line-input form-md-floating-label">
										<label>Working Days</label>
										<div class="mt-checkbox-list">
											<div class="col-md-4">												
												<label class="mt-checkbox mt-checkbox-outline"> Monday
													<input  name="working_days[]" class="days" type="checkbox" <?php if( strpos($getSla['resultSet']->working_day,"1")!==false) echo "checked"; ?> value=" 1">
													<span></span>
												</label>
												<label class="mt-checkbox mt-checkbox-outline"> Thursday
													<input  name="working_days[]" class="days" type="checkbox" <?php if( strpos($getSla['resultSet']->working_day,"4")!==false) echo "checked"; ?> value=" 4">
													<span></span>
												</label>
											</div>
											<div class="col-md-4">
												<label class="mt-checkbox mt-checkbox-outline"> Tuesday
													<input  name="working_days[]" class="days" type="checkbox" <?php if( strpos($getSla['resultSet']->working_day,"2")!==false) echo "checked"; ?> value=" 2">
													<span></span>
												</label>
												<label class="mt-checkbox mt-checkbox-outline"> Friday
													<input  name="working_days[]" class="days" type="checkbox" <?php if( strpos($getSla['resultSet']->working_day,"5")!==false) echo "checked"; ?> value=" 5">
													<span></span>
												</label>
											</div>
											<div class="col-md-4">
												<label class="mt-checkbox mt-checkbox-outline"> Wednesday
													<input  name="working_days[]" class="days" type="checkbox" <?php if( strpos($getSla['resultSet']->working_day,"3")!==false) echo "checked"; ?> value=" 3">
													<span></span>
												</label>
												<label class="mt-checkbox mt-checkbox-outline"> Saturday
													<input  name="working_days[]" class="days" type="checkbox" <?php if( strpos($getSla['resultSet']->working_day,"6")!==false) echo "checked"; ?> value=" 6">
													<span></span>
												</label>
											</div>
											<div class="col-md-4">										
												<label class="mt-checkbox mt-checkbox-outline"> Sunday
													<input  name="working_days[]" class="days" type="checkbox" <?php if( strpos($getSla['resultSet']->working_day,"7")!==false) echo "checked"; ?> value=" 7" >
													<span></span>
												</label>
											</div>
										</div>
										<div class="col-sm-12" id="day_error"></div>
									</div>																		
                            </div>
							
							<div class="col-md-3">                                
								<div class="form-group form-md-line-input form-md-floating-label">
									
									<label class="col-md-9" for="form_control_1">Working Hours</label>
									<div class="col-md-9">
										<div class="mt-radio-inline">
											<label class="mt-radio">
												<input type="radio" class="postageyes" name="bussiness_hrs" <?php if( strpos($getSla['resultSet']->bussiness_hrs,"1")!==false) echo "checked"; ?> value="1"/> 24 Hours
												<span></span>
											</label>
											<label class="mt-radio">
												<input type="radio" class="postageyes" name="bussiness_hrs" <?php if( strpos($getSla['resultSet']->bussiness_hrs,"0")!==false) echo "checked"; ?> value="0"/> Business Hours
												<span></span>
											</label>											
										</div>
									</div>
								</div>
                            </div>
							
							<div class="col-md-3">
							<div class="form-group form-md-line-input has-info col-md-12">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</span>
									<input class="form-control" id="shift_start_time" name="shift_start_time" type="time" <?php if($getSla['resultSet']->bussiness_hrs == 1){ echo "readonly"; } ?> onchange="validateHhMm(this);" value="<?php echo ($getSla['resultSet']->shift_start_time) ? $getSla['resultSet']->shift_start_time : ''; ?>" required>
                                    <label for="form_control_1">Shift Starts</label>
									<span class="help-block"> hr:mm AM/PM </span>
								</div>
							</div>
							
							<div class="form-group form-md-line-input has-info col-md-12">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</span>
									<input class="form-control" id="shift_end_time" name="shift_end_time" <?php if($getSla['resultSet']->bussiness_hrs == 1){ echo "readonly"; } ?> type="time" onchange="validateHhMm(this);" value="<?php echo ($getSla['resultSet']->shift_end_time) ? $getSla['resultSet']->shift_end_time : ''; ?>" required>
                                    <label for="form_control_1">Shift Ends</label>
									<span class="help-block"> hr:mm AM/PM </span>
								</div>
							</div>
							</div>

                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green" id="submit"  value="Submit">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('sla/incident/'.encode_url($client_id)) ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_eventssla.js" type="text/javascript"></script>


<script>

	// function validateForm() 
	// {		
		// var x = document.forms["form"]["days"].value;		
		// var y = document.querySelectorAll('input[type="checkbox"]:checked').length
		
		// if (x != y) {
			// alert("Days should match!");
			// return false;
		// }
	// }
	
	function validateHhMm(inputField) {
        var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);

        if (isValid) {
            inputField.style.backgroundColor = '#bfa';
			document.getElementById("submit").disabled= false;
			
        } else {
            inputField.style.backgroundColor = '#fba';
			document.getElementById("submit").disabled= true;
        }

        return isValid;
    }
	
	function validateTime(inputField) {
        var isValid = /^([0-9]?[0-9][0-9]):([0-9][0-9])(:[0-9][0-9])?$/.test(inputField.value);

        if (isValid) {
            inputField.style.backgroundColor = '#bfa';
			document.getElementById("submit").disabled= false;
        } else {
            inputField.style.backgroundColor = '#fba';
			document.getElementById("submit").disabled= true;
        }

        return isValid;
    }
</script>
