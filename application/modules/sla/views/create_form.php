
<!-- END PAGE HEADER-->

<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Create Events SLA</span>
				<span class="caption-subject"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_page){
							if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('Partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }}?>
					<li>
						<a href="<?php //echo base_url('client') ?>">Events SLA</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>Create</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <div class="portlet light ">
            <div class="portlet-body form">
                <form role="form" name="frmGateway" id="form" method="post" action="" onsubmit="return validateForm()" >
                    <div class="form-body">
                        <div class="row">          
						
							<input class="form-control" id="days" name="days" type="hidden">
							<input class="form-control" id="hours" name="hours" type="hidden">
							
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
									<input  class="form-control" type="text" id="admin_name" name="client_name"  value="<?php echo $clientName['resultSet']['client_title'];?>" disabled>
									<input  class="form-control" type="hidden" id="client_id" name="client_id"  value="<?php echo $client_id; ?>" >
                                    
                                    <label for="form_control_1">Client</label>                                               
                                </div>
                            </div>
							<div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" id="priority_id"name="priority_id" required>
										<option></option>
										<?php
                                            foreach ($priority['resultSet'] as $values) { ?>
											
											<option value="<?php echo $values['skill_id'];?>"><?php echo $values['title'];?></option>
											
										<?php } ?>
									</select> 
									<label for="form_control_1">Severity</label>                                               
                                </div>
                            </div>
							
							<input class="form-control" id="sla_type" name="sla_type" type="hidden">
                            							
							<div class="form-group form-md-line-input has-info col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</span>
									<input class="form-control" id="shift_start_time" name="shift_start_time" type="time" onchange="validateHhMm(this);" required>
                                    <label for="form_control_1">Shift Starts</label>
									<span class="help-block"> hr:mm AM/PM </span>
								</div>
							</div>
							
							<div class="form-group form-md-line-input has-info col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</span>
									<input class="form-control" id="shift_end_time" name="shift_end_time" type="time" onchange="validateHhMm(this);" required>
                                    <label for="form_control_1">Shift Ends</label>
									<span class="help-block"> hr:mm AM/PM </span>
								</div>
							</div>
							
							<div class="form-group form-md-line-input has-info col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
									</span>
									 <input class="form-control" id="incident_sla" name="incident_sla" type="text" onchange="validateTime(this);" required>
                                    <label for="form_control_1">Incident Generated SLA</label>
									<span class="help-block"> hr:mm </span>
								</div>
							</div>
							
							<div class="form-group form-md-line-input has-info col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
									</span>
									<input class="form-control" id="mtrs_slas" name="mtrs_slas" type="text" onchange="validateTime(this);" required>
                                    <label for="form_control_1">MTRS SLA</label>
									<span class="help-block"> hr:mm </span>
								</div>
							</div>
							
							<div class="form-group form-md-line-input has-info col-md-3">
								<div class="input-group">
									<span class="input-group-addon">
									</span>
									<input class="form-control" id="mttr_slas" name="mttr_slas" type="text"  onchange="validateTime(this);" required>
                                    <label for="form_control_1">MTTR SLA</label>
									<span class="help-block"> hr:mm </span>
								</div>
							</div>
														                                    
							<div class="col-md-6">                                
									<div class="form-group  form-md-line-input form-md-floating-label">
										<label>Working Days</label>
										<div class="mt-checkbox-list">
											<div class="col-md-4">												
												<label class="mt-checkbox mt-checkbox-outline"> Monday
													<input  name="working_days[]" type="checkbox" value=" 1">
													<span></span>
												</label>
												<label class="mt-checkbox mt-checkbox-outline"> Thursday
													<input  name="working_days[]" type="checkbox" value=" 4">
													<span></span>
												</label>
											</div>
											<div class="col-md-4">
												<label class="mt-checkbox mt-checkbox-outline"> Tuesday
													<input  name="working_days[]" type="checkbox" value=" 2">
													<span></span>
												</label>
												<label class="mt-checkbox mt-checkbox-outline"> Friday
													<input  name="working_days[]" type="checkbox" value=" 5">
													<span></span>
												</label>
											</div>
											<div class="col-md-4">
												<label class="mt-checkbox mt-checkbox-outline"> Wednesday
													<input  name="working_days[]" type="checkbox" value=" 3">
													<span></span>
												</label>
												<label class="mt-checkbox mt-checkbox-outline"> Saturday
													<input  name="working_days[]" type="checkbox" value=" 6">
													<span></span>
												</label>
											</div>
											<div class="col-md-4">										
												<label class="mt-checkbox mt-checkbox-outline"> Sunday
													<input  name="working_days[]" type="checkbox" value=" 7" >
													<span></span>
												</label>
											</div>
										</div>
									</div>
                            </div>
							
							
							<div class="col-md-3">                                
								<div class="form-group form-md-line-input form-md-floating-label">
									
									<label class="col-md-9" for="form_control_1">Working Hours</label>
									<div class="col-md-9">
										<div class="mt-radio-inline">
											<label class="mt-radio">
												<input type="radio" name="bussiness_hrs" checked="" value="1"/> 24 Hours
												<span></span>
											</label>
											<label class="mt-radio">
												<input type="radio" name="bussiness_hrs" value="0"/> Bussiness Hours
												<span></span>
											</label>											
										</div>
									</div>
								</div>
                            </div>
							
                        </div>
						
						
                        <div class="form-actions noborder">
                            <button type="submit" class="btn green" id="submit"  value="Submit">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content {
        background-color: #ff9494;
        border-radius: 5px;
    }
    .modal-footer {
        display: none;
    }
</style>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>

<script>

	// function validateForm() 
	// {		
		// var x = document.forms["form"]["days"].value;		
		// var y = document.querySelectorAll('input[type="checkbox"]:checked').length
		
		// if (x != y) {
			// alert("Days should match!");
			// return false;
		// }
	// }
	
	function validateHhMm(inputField) {
        var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);

        if (isValid) {
            inputField.style.backgroundColor = '#bfa';
			document.getElementById("submit").disabled= false;
			
        } else {
            inputField.style.backgroundColor = '#fba';
			document.getElementById("submit").disabled= true;
        }

        return isValid;
    }
	
	function validateTime(inputField) {
        var isValid = /^([0-9]?[0-9][0-9]):([0-9][0-9])(:[0-9][0-9])?$/.test(inputField.value);

        if (isValid) {
            inputField.style.backgroundColor = '#bfa';
			document.getElementById("submit").disabled= false;
        } else {
            inputField.style.backgroundColor = '#fba';
			document.getElementById("submit").disabled= true;
        }

        return isValid;
    }
</script>

<?php

function childTreeDropdown($treeArray = array(), $strlenparent = '') {
    //echo $strlenparent;//exit;
    $strlenparentDot = str_repeat('&nbsp;', $strlenparent);
    ?>   
    <?php
    $child = "";
    foreach ($treeArray as $value) {
        ?>   
        <?php
        if (!empty($value['children'])) {
            $child = count($value['children']);
        }
        ?>
        <option value="<?php echo $value['id']; ?>" <?php if ($child > 0) {
            echo 'class="optionGroup"';
        } ?>><?php echo $strlenparentDot . $value['client_title']; ?></option>
        <?php
        if (!empty($value['children'])) {
            $countlength = $strlenparent + strlen($value['client_title']);
            echo childTreeDropdown($value['children'], $countlength);
        }
        ?>        
        <?php
    }
    ?>
    <?php
}

function childTree($treeArray = array()) {
    ?>
    <ul>
    <?php foreach ($treeArray as $value) {
        ?>
            <li>
                <label class="mt-checkbox mt-checkbox-outline">                                                                    
                    <input type="checkbox" name="subClients[]" value="<?php echo $value['id'] . '-' . $value['parent_client_id']; ?>">
                    <span></span>
                </label>
                <span><?php echo $value['client_title'] ?></span>
                <?php
                if (!empty($value['children'])) {

                    echo childTree($value['children']);
                }
                ?>        
            </li>             
    <?php }
    ?>
    </ul>
<?php }
?>
<style type="text/css">
    .optionGroup {
        font-weight: bold;        
    }
    .optionChild {
        padding-left: 15px;
    }
</style>  
<script>
$(':checkbox[readonly]').click(function(){ return false; });
</script>         
<script src="<?php echo base_url() . "public/" ?>js/form/form_eventssla.js" type="text/javascript"></script>



