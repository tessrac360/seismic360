<!-- END PAGE HEADER-->
<?php
$accessAdd = helper_fetchPermission('13', 'add');
$accessEdit = helper_fetchPermission('13', 'edit');
$accessStatus = helper_fetchPermission('13', 'active');
$accessDelete = helper_fetchPermission('13', 'delete');
?>

<div class="row">
    <div>
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Incidents SLA</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>	
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					<li>
						<i class="fa fa-cog"></i>
						<span>Managements</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<?php if($client_page){
							if($client_type == 'C'){
							?>
							<li>
								<a href="<?php echo base_url('client') ?>">Client</a>                            
								<i class="fa fa-angle-right"></i>
							</li>	
						<?php }else{?>
							<li>
								<a href="<?php echo base_url('partner') ?>">Partner</a>                            
								<i class="fa fa-angle-right"></i>
							</li>
						<?php }}?>
					<li>
						<span>SLA</span>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>List</span>                            
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" href="<?php echo base_url('sla/createincident/'.encode_url($client_id)); ?>"> Add New Incidents SLA
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">

                            <div class="btn-group pull-right">
                                <div class="input-group">
                                    <input placeholder="Search..." id="txt_datatablesearch" title="Search By Operator Site Name" name="table_search" class="form-control" value="<?php echo $search; ?>" type="text">
                                    <input id="hidden_datatableUrl" type="hidden" value="<?php echo $hiddenURL; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn blue" id="btn_datatableSearch" type="button">Search!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover order-column" id="role">
                    <thead>
                        <tr>
                            <th> Client </th>                           
                            <th> SLA Type </th>                           
                            <th> Severity Id </th>
                            <th> Shift Start </th>  
                            <th> Shift End </th>
                            <th> Response SLA </th> 
							<th> MTTR SLA </th>
                            <th> MTRS SLA </th>  
                            <th> Working Days </th>
                            <th> Request Type</th>
                            <th> Action </th>  
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($gateway)) {
                            foreach ($gateway as $value) {
                                //  pr($value);
								//if($value['is_deleted'] == 'N'){
									$a=$value['response_sla'];
									$b=$value['mttr_sla'];
									$c=$value['mtrs_sla'];
                                ?>
                                <tr>
                                    <td> <?php echo $value['client_title']; ?> </td>
                                    <td> <?php echo $value['sla_type']; ?> </td>
                                    <td> <?php echo $value['priority_id']; ?>  </td>
                                    <td> <?php echo $value['shift_start_time']; ?>  </td>
                                    <td> <?php echo $value['shift_end_time']; ?>  </td>
                                    <td> <?php echo sprintf('%02d:%02d:%02d', ($a/3600),($a/60%60), $a%60); ?>  </td>
                                    <td> <?php echo sprintf('%02d:%02d:%02d', ($b/3600),($b/60%60), $b%60); ?>  </td>
                                    <td> <?php echo sprintf('%02d:%02d:%02d', ($c/3600),($c/60%60), $c%60); ?>  </td>
                                    <td> <?php if( strpos($value['working_day'],"1")) echo " Monday, ";
												if( strpos($value['working_day'],"2")) echo " Tuesday, ";
												if( strpos($value['working_day'],"3")) echo " Wednesday, ";
												if( strpos($value['working_day'],"4")) echo " Thursday, ";
												if( strpos($value['working_day'],"5")) echo " Friday, ";
												if( strpos($value['working_day'],"6")) echo " Saturday, ";
												if( strpos($value['working_day'],"7")) echo " Sunday ";?></td>
									<td> <?php echo $value['request_type']; ?>  </td>			
                                    
                                    <td>
                                        <input type="checkbox" <?php
                                        if ($value['status'] == 'Y') {
                                            echo 'checked';
                                        }
                                        ?>  id="onoffactiontoggleinc" class="onoffactiontoggle inc" myval="<?php echo encode_url($value['sla_id']); ?>">
										
										  <a href="<?php echo base_url() . 'sla/editincident/' .encode_url($client_id)."/". encode_url($value['sla_id']); ?>" class="btn btn-xs  blue">
                                            <i class="fa fa-edit"></i>
                                        </a>  
                                         <!--<a href="<?php echo base_url() . 'sla/deleteincident/' .encode_url($client_id)."/". encode_url($value['sla_id']); ?>"  userId="<?php echo encode_url($value['sla_id']);?>" class="btn btn-xs red isdeleteAdminUsers" onlick=''>
                                                    <i class="fa fa-trash"></i>
                                                </a> -->
                                        				
                                   
                                      								
                                    </td>
                                </tr> 
                                <?php
								//}
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="12"> No Record Found </td>
                            </tr> 
<?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_eventssla.js" type="text/javascript"></script>
