<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sla extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("Model_sla");
		$this->load->model('client/Model_client');
        $this->load->library('form_validation');
    }

    public function index($client_id = NULL) {
		//$client_id  = decode_url($client_id);
		//echo $client_id;exit;
		$client_page = true;
		if(is_numeric($client_id)){
		   	$client_page = false;
		}elseif($client_id == NULL){
			$client_page = false;
		}
		if($client_page){
			$client_id  = decode_url($client_id);
			$client_ids[] = $client_id;
			$clientRes = $this->Model_sla->getClientName($client_id);
			$client_titles[$client_id] =$clientRes['resultSet']['client_title'];
			
		}else{
			$data['clients'] = $this->Model_client->getClientsgat($this->client_id);		
			$client_ids =array();
			$client_titles =array();
			for($i=0;$i<count($data['clients']);$i++){
				$client_ids[]= $data['clients'][$i]['id'];
				$client_titles[$data['clients'][$i]['id']]= $data['clients'][$i]['client_title'];
			}			
		}
		//pr($client_ids);exit;		
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		if($client_page){
			$config['base_url'] = base_url() . 'sla/index/'.encode_url($client_id );
		}else{
			$config['base_url'] = base_url() . 'sla/index/';
		}
        $config['total_rows'] = count($this->Model_sla->getSlaSearchData($search,$client_ids ));
		if($client_page){
			$config['uri_segment'] = 4;
		}else{
			$config['uri_segment'] = 3;
		}		
       
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		//$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}
        $this->pagination->initialize($config);
		if($client_page){
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		}else{
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		}		
        

        $data['gateway'] = $this->Model_sla->getSlaPagination($search, $client_ids, $config['per_page'], $page);
		
		//pr($data['gateway']);exit;
        //$data['client_id'] = $client_id;
		if($client_page){
			$data['hiddenURL'] = 'sla/index/'.encode_url($client_id );
		}else{
			$data['hiddenURL'] = 'sla/index/';
		}		
        if($client_page){
			$data['clientName'] = $this->Model_sla->getClientName($client_id);
			$data['client_title'] = $data['clientName']['resultSet']['client_title'];
			$data['client_type'] = $data['clientName']['resultSet']['client_type'];
			$data['client_page'] = true;
			$data['client_id'] = $client_id;
		}else{
			$data['client_page'] = false;
		}
		$data['clientTitles'] = $client_titles;
		//pr($data['clientTitles']);exit;
		$data['pageTitle'] = 'Sla-view';
        $data['file'] = 'view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }
	
	public function incident($client_id = NULL) {
		//$client_id  = decode_url($client_id);
		//echo $client_id;exit;
		$client_page = true;
		if(is_numeric($client_id)){
		   	$client_page = false;
		}elseif($client_id == NULL){
			$client_page = false;
		}
		if($client_page){
			$client_id  = decode_url($client_id);
			$client_ids[] = $client_id;
			$clientRes = $this->Model_sla->getClientName($client_id);
			$client_titles[$client_id] =$clientRes['resultSet']['client_title'];
			
		}else{
			$data['clients'] = $this->Model_client->getClientsgat($this->client_id);		
			$client_ids =array();
			$client_titles =array();
			for($i=0;$i<count($data['clients']);$i++){
				$client_ids[]= $data['clients'][$i]['id'];
				$client_titles[$data['clients'][$i]['id']]= $data['clients'][$i]['client_title'];
			}			
		}
		//pr($client_ids);exit;		
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
		$config['per_page'] = PAGINATION_PERPAGE;
		if($client_page){
			$config['base_url'] = base_url() . 'sla/incident/'.encode_url($client_id );
		}else{
			$config['base_url'] = base_url() . 'sla/incident/';
		}
        $config['total_rows'] = count($this->Model_sla->getIncSlaSearchData($search,$client_ids ));
		if($client_page){
			$config['uri_segment'] = 4;
		}else{
			$config['uri_segment'] = 3;
		}		
       
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		//$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		if($_GET){
			$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
		}
        $this->pagination->initialize($config);
		if($client_page){
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		}else{
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		}		
        

        $data['gateway'] = $this->Model_sla->getIncSlaPagination($search, $client_ids, $config['per_page'], $page);
		
		//pr($data['gateway']);exit;
        //$data['client_id'] = $client_id;
		if($client_page){
			$data['hiddenURL'] = 'sla/incident/'.encode_url($client_id );
		}else{
			$data['hiddenURL'] = 'sla/incident/';
		}		
        if($client_page){
			$data['clientName'] = $this->Model_sla->getClientName($client_id);
			$data['client_title'] = $data['clientName']['resultSet']['client_title'];
			$data['client_type'] = $data['clientName']['resultSet']['client_type'];
			$data['client_page'] = true;
			$data['client_id'] = $client_id;
		}else{
			$data['client_page'] = false;
		}
		$data['clientTitles'] = $client_titles;
		//pr($data['clientTitles']);exit;
        $data['file'] = 'view_form_incidents';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }
	
	public function searchdata($client_id = NULL) {
        
		$data['clients'] = $this->Model_client->getClients($this->client_id);		
		$client_ids =array();
		$client_titles =array();
		for($i=0;$i<count($data['clients']);$i++){
			$client_ids[]= $data['clients'][$i]['id'];
			$client_titles[$data['clients'][$i]['id']]= $data['clients'][$i]['client_title'];
		}
        $this->load->library('pagination');
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['base_url'] = base_url() . 'sla/searchData/';
        $config['total_rows'] = count($this->Model_sla->getSlaSearchData($search,$client_ids ));
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
		$config['enable_query_strings'] = true;
		//$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$config['suffix'] = '?' . http_build_query($_GET, '', "&amp;");
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['sla'] = $this->Model_sla->getSlaSearchData($search, $client_ids , $config['per_page'], $page);
		$data['clientTitles'] = $client_titles;
        $data['search'] = $search;
        $data['hiddenURL'] = 'sla/searchData/';
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }
    public function getSla($client_id = NULL) {
		$client_id  = decode_url($client_id);
		//echo $client_id;exit;
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'client/sla/getsla/'.encode_url($client_id );
        $config['total_rows'] = $this->Model_sla->getSlaPagination($search, $client_id);
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['uri_segment'] = 5;
        $config['display_pages'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

        $data['sla'] = $this->Model_sla->getSlaPagination($search, $client_id, $config['per_page'], $page);
		$data['clientName'] = $this->Model_sla->getClientName($client_id);
		$data['client_title'] = $data['clientName']['resultSet']['client_title'];
		$data['client_type'] = $data['clientName']['resultSet']['client_type'];
		//pr($data['sla']);exit;
        $data['client_id'] = $client_id;
        $data['hiddenURL'] = 'client/sla/searchdata/'.encode_url($client_id );
        $data['file'] = 'sla/view_form';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }

	public function timeDiff($firstTime,$lastTime) 
	{
		$firstTime = strtotime($firstTime);
		$lastTime = strtotime($lastTime);
		$timeDiff = abs($lastTime - $firstTime);
		return $timeDiff;
	}

    public function create($client_id = NULL) {
		
		$client_page = false;
		if($client_id != NULL){
			$client_id = decode_url($client_id);
			$client_page = true;
		}
		//pr($client_id);exit;

        $search = $this->input->get_post('sd');
        //$level = (isset($_GET['lvl'])) ? decode_url($_GET['lvl']) : 1;
		$roleAccess = helper_fetchPermission('62', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
				if(!$client_page){
					$this->form_validation->set_rules('client_id', 'Client', 'required');
				}
                //$this->form_validation->set_rules('sla_type', 'Sla Type', 'required');
                $this->form_validation->set_rules('priority_id', 'Priorities', 'required|is_unique[client_gateways.ip_address]');
				
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $slaPost = $this->input->post();
					if($client_page){
						$slaPost['client_id'] = $client_id;
					}
					
                    $slaPost['working_day'] = (implode(",",$slaPost['working_days']));
					
					$slaPost['days']=count($slaPost['working_days']);					
					$slaPost['hours']=round(($this->timeDiff($slaPost['shift_start_time'],$slaPost['shift_end_time'])/60)/60);
					
					$slaPost['sla_type']="".$slaPost['days']."x".$slaPost['hours'];
                    $slaPost['incident_generated_sla'] = $this->getseconds($slaPost['incident_sla']);
                    $slaPost['mtrs_sla'] = $this->getseconds($slaPost['mtrs_slas']);
                    $slaPost['mttr_sla'] = $this->getseconds($slaPost['mttr_slas']);

					
                    unset($slaPost['working_days']);
                    unset($slaPost['incident_sla']);
                    unset($slaPost['mtrs_slas']);
                    unset($slaPost['mttr_slas']);
					//pr($slaPost);exit;
                    $resultData = $this->Model_sla->insertSla($slaPost);
					//pr($working_day);exit;
	                if (!empty($resultData)) {
                        $this->session->set_flashdata("success_msg", "Sla is created successfully ..!!");                        
						if($client_page){
							redirect('sla/index/'.encode_url($client_id));	
						}else{
							redirect('sla/');
						}
						
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
						if($client_page){
							redirect('sla/create/'.encode_url($client_id));	
						}else{
							redirect('sla/create/');
						}						
                       
                    }
                } else {
					//
					if ($search != '') {
						$tmp = array();
						$cnt = count($data['client']);
						for ($i = 0; $i < $cnt; $i++) {
							if (preg_match("/" . $search . "/i", $data['client'][$i]['client_title'])) {
								$tmp[] = $data['client'][$i];
							}
						}
						$data['client'] = array();
						$data['client'] = $tmp;
					}
					$data['client_id'] = $this->client_id;
					
					$data['subclients'] = $this->Model_client->getClientEdit($this->client_id);
					$data['priority'] = $this->Model_sla->getSeverities();
                    //$data['pageTitle'] = 'Create Events SLA';
                    $data['file'] = 'create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				//$data['clientName'] = $this->Model_gateways->getClientName($client_id);
				$data['client'] = $this->treeClientDropdown();
				if($client_page){
					$data['clientName'] = $this->Model_sla->getClientName($client_id);
					$data['client_title'] = $data['clientName']['resultSet']['client_title'];
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					$data['client_page'] = true;
					$data['client_id'] = $client_id;
				}else{
					$data['client_page'] = false;
				}				
				
                $data['priority'] = $this->Model_sla->getSeverities();
                //$data['pageTitle'] = 'Create Events SLA';
                $data['file'] = 'create_form';
				//pr($data['priority']); die;	
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function createincident($client_id = NULL) {
		
		$client_page = false;
		if($client_id != NULL){
			$client_id = decode_url($client_id);
			$client_page = true;
		}
		//pr($client_id);exit;
		
		$data['clientName'] = $this->Model_sla->getClientName($client_id);
        $search = $this->input->get_post('sd');
        //$level = (isset($_GET['lvl'])) ? decode_url($_GET['lvl']) : 1;
		$roleAccess = helper_fetchPermission('62', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
				$slaPost = $this->input->post();
				if(!$client_page){
					$this->form_validation->set_rules('client_id', 'Client', 'required');
				}
                //$this->form_validation->set_rules('sla_type', 'Sla Type', 'required');
                $this->form_validation->set_rules('priority_id', 'Priorities', 'required|is_unique[client_gateways.ip_address]');
				
				if(!isset($slaPost['working_days']) ||  count($slaPost['working_days']) == 0){
					$this->form_validation->set_rules('working_days', 'Working Days', 'required');	
				}
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    
					//pr($slaPost);//exit;
					if($client_page){
						$slaPost['client_id'] = $client_id;
					}
					
                    $slaPost['working_day'] = (implode(",",$slaPost['working_days']));
					
					$slaPost['days']=count($slaPost['working_days']);					
					$slaPost['hours']= round(($this->timeDiff($slaPost['shift_start_time'],$slaPost['shift_end_time'])/60)/60,2);
					if($slaPost['bussiness_hrs'] == 1){
						$slaPost['sla_type']="24x".$slaPost['days'];	
					}else{
						$slaPost['sla_type']="".$slaPost['hours']."x".$slaPost['days'];
					}
					//$slaPost['sla_type']="".$slaPost['hours']."x".$slaPost['days'];
                    $slaPost['response_sla'] = $this->getseconds($slaPost['response_slas']);
                    $slaPost['event_response_sla'] = $this->getseconds($slaPost['event_response_sla']);
                    $slaPost['mtrs_sla'] = $this->getseconds($slaPost['mtrs_slas']);
                    $slaPost['mttr_sla'] = $this->getseconds($slaPost['mttr_slas']);
					//pr($slaPost);exit;
                    unset($slaPost['working_days']);
                    unset($slaPost['response_slas']);
                    unset($slaPost['mtrs_slas']);
                    unset($slaPost['mttr_slas']);
					//pr($slaPost);exit;
                    $resultData = $this->Model_sla->insertIncSla($slaPost);
					//pr($working_day);exit;
	                if (!empty($resultData)) {
                        $this->session->set_flashdata("success_msg", "Sla is created successfully ..!!");                        
						if($client_page){
							redirect('sla/incident/'.encode_url($client_id));	
						}else{
							redirect('sla/incident');
						}
						
                    } else {
                        $this->session->set_flashdata("error_msg", "Some thing went wrong");
						if($client_page){
							redirect('sla/createincident/'.encode_url($client_id));	
						}else{
							redirect('sla/createincident/');
						}						
                       
                    }
                } else {
					//
					if ($search != '') {
						$tmp = array();
						$cnt = count($data['client']);
						for ($i = 0; $i < $cnt; $i++) {
							if (preg_match("/" . $search . "/i", $data['client'][$i]['client_title'])) {
								$tmp[] = $data['client'][$i];
							}
						}
						$data['client'] = array();
						$data['client'] = $tmp;
					}
					$data['client_id'] = $this->client_id;
					$data['client_page'] = true;
					$data['subclients'] = $this->Model_client->getClientEdit($this->client_id);
					$data['priority'] = $this->Model_sla->getSeverities();
                    $data['pageTitle'] = 'Sla-create';
					$data['client_type'] = $data['clientName']['resultSet']['client_type'];
                    $data['file'] = 'create_form_incident';
                    $this->load->view('template/front_template', $data);
                }
            } else {
				//$data['clientName'] = $this->Model_gateways->getClientName($client_id);
				$data['client'] = $this->treeClientDropdown();
				if($client_page){
					$data['clientName'] = $this->Model_sla->getClientName($client_id);
					if($data['clientName']['status'] == 'true'){
						$data['client_title'] = $data['clientName']['resultSet']['client_title'];
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
					}
					$data['client_page'] = true;
					$data['client_id'] = $client_id;
				}else{
					$data['client_page'] = false;
				}				
				
                $data['priority'] = $this->Model_sla->getSeverities();
                $data['pageTitle'] = 'Sla-create';
                $data['file'] = 'create_form_incident';
				//pr($data['priority']); die;	
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($client_id = NULL,$postId = NULL) {
		$client_page = true;
		if($postId == NULL){
			$postId = $client_id;
			$client_page = false;
		}		
		if($client_page){
			$client_id = decode_url($client_id);
		}	

		//$client_id = decode_url($client_id);
        $roleAccess = helper_fetchPermission('62', 'edit');
		//echo $roleAccess;exit;
        if ($roleAccess == 'Y') {
					
            $postId = decode_url($postId);
                if ($this->input->post()) {
                    $postSla = $this->input->post();
					if(!$client_page){
						$this->form_validation->set_rules('client_id', 'Client', 'required');
					}					
					$this->form_validation->set_rules('sla_type', 'Sla Type', 'required');
					$this->form_validation->set_rules('priority_id', 'Priorities', 'required');
					//pr($postSla);exit;
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
						//pr($postSla);exit;
                        $postSla = array_merge($postSla, $this->uData); //pr($postUsers);exit;
						$postSla['working_day'] = (implode(",",$postSla['working_days']));
						
						$postSla['days']=count($postSla['working_days']);					
						$postSla['hours']=round(($this->timeDiff($postSla['shift_start_time'],$postSla['shift_end_time'])/60)/60);
						
						$postSla['sla_type']="".$postSla['hours']."x".$postSla['days'];
						$postSla['incident_generated_sla'] = $this->getseconds($postSla['incident_sla']);
						$postSla['mtrs_sla'] = $this->getseconds($postSla['mtrs_slas']);
						$postSla['mttr_sla'] = $this->getseconds($postSla['mttr_slas']);
						
						unset($postSla['working_days']);
						unset($postSla['incident_sla']);
						unset($postSla['mtrs_slas']);
						unset($postSla['mttr_slas']);
                        $updateStatus = $this->Model_sla->updateSla($postSla, $postId);
						//pr($postSla);exit;
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "SLA is Updated successfully ..!!");
							if($client_page){
								redirect('sla/index/'.encode_url($client_id));	
							}else{
								redirect('sla/');
							}								
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
							if($client_page){
								redirect('sla/edit/'.encode_url($client_id)."/".encode_url($postId));	
							}else{
								redirect('sla/edit/'.encode_url($postId));
							}							
                        }
                    } else {
						//$data['clientName'] = $this->Model_gateways->getClientName($client_id);
                        $data['getSla'] = $this->Model_sla->getSlaEdit($postId);
						$data['client'] = $this->treeClientDropdown();
						if($client_page){							
							$data['clientName'] = $this->Model_sla->getClientName($client_id);
							$data['client_title'] = $data['clientName']['resultSet']['client_title'];
							$data['client_type'] = $data['clientName']['resultSet']['client_type'];
							$data['client_page'] = true;
							$data['client_id'] = $client_id;
						}else{
							$data['client_page'] = false;
						}						
						$data['priority'] = $this->Model_sla->getSeverities();
                        //$data['pageTitle'] = 'Events SLA edit';
                        $data['file'] = 'update_form';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					//$data['clientName'] = $this->Model_gateways->getClientName($client_id);
                    $data['getSla'] = $this->Model_sla->getSlaEdit($postId);
					$data['client'] = $this->treeClientDropdown();
					if($client_page){						
						$data['clientName'] = $this->Model_sla->getClientName($client_id);
						$data['client_title'] = $data['clientName']['resultSet']['client_title'];
						$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						$data['client_page'] = true;
						$data['client_id'] = $client_id;
					}else{
						$data['client_page'] = false;
					}						
					
					$data['priority'] = $this->Model_sla->getSeverities();
                    //$data['pageTitle'] = 'Your page title';
                    $data['file'] = 'update_form';
					//pr($data); die;
                    $this->load->view('template/front_template', $data);
                }
            
        } else {
            redirect('unauthorized');
        }
    }

    public function editincident($client_id = NULL,$postId = NULL) {
		$client_page = true;
		if($postId == NULL){
			$postId = $client_id;
			$client_page = false;
		}		
		if($client_page){
			$client_id = decode_url($client_id);
		}	

		//$client_id = decode_url($client_id);
        $roleAccess = helper_fetchPermission('62', 'edit');
		//echo $roleAccess;exit;
        if ($roleAccess == 'Y') {
					
            $postId = decode_url($postId);
                if ($this->input->post()) {
                    $postSla = $this->input->post();
					//pr($postSla);exit;
					if(!$client_page){
						$this->form_validation->set_rules('client_id', 'Client', 'required');
					}					
					if(!isset($postSla['working_days']) || count($postSla['working_days']) == 0){
						$this->form_validation->set_rules('working_days', 'Working Days', 'required');	
					}
					$this->form_validation->set_rules('sla_type', 'Sla Type', 'required');
					$this->form_validation->set_rules('priority_id', 'Priorities', 'required');
					//pr($postSla);exit;
                    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                    if ($this->form_validation->run() == TRUE) {
						
                        $postSla = array_merge($postSla, $this->uData); //pr($postUsers);exit;
						$postSla['working_day'] = (implode(",",$postSla['working_days']));
						
						$postSla['days']=count($postSla['working_days']);					
						$postSla['hours']=round(($this->timeDiff($postSla['shift_start_time'],$postSla['shift_end_time'])/60)/60,2);
						$postSla['sla_type']="".$postSla['hours']."x".$postSla['days'];
						if($postSla['bussiness_hrs'] == 1){
							$postSla['sla_type']="24x".$postSla['days'];	
						}else{
							$postSla['sla_type']="".$postSla['hours']."x".$postSla['days'];
						}						
						$postSla['response_sla'] = $this->getseconds($postSla['response_slas']);
						
						$postSla['mtrs_sla'] = $this->getseconds($postSla['mtrs_slas']);
						$postSla['mttr_sla'] = $this->getseconds($postSla['mttr_slas']);
						$postSla['event_response_sla'] = $this->getseconds($postSla['event_response_sla']);
						
						unset($postSla['working_days']);
						unset($postSla['response_slas']);
						unset($postSla['mtrs_slas']);
						unset($postSla['mttr_slas']);
                        $updateStatus = $this->Model_sla->updateIncSla($postSla, $postId);
						//pr($postSla);exit;
                        if ($updateStatus['status'] == 'true') {
                            $this->session->set_flashdata("success_msg", "SLA is Updated successfully ..!!");
							if($client_page){
								redirect('sla/incident/'.encode_url($client_id));	
							}else{
								redirect('sla/incident');
							}								
                        } else {
                            $this->session->set_flashdata("error_msg", "Some thing went wrong");
							if($client_page){
								redirect('sla/editincident/'.encode_url($client_id)."/".encode_url($postId));	
							}else{
								redirect('sla/editincident/'.encode_url($postId));
							}							
                        }
                    } else {
						//$data['clientName'] = $this->Model_gateways->getClientName($client_id);
                        $data['getSla'] = $this->Model_sla->getIncSlaEdit($postId);
						$data['client'] = $this->treeClientDropdown();
						if($client_page){							
							$data['clientName'] = $this->Model_sla->getClientName($client_id);
							if($data['clientName']['status'] == 'true'){
								$data['client_title'] = $data['clientName']['resultSet']['client_title'];
								$data['client_type'] = $data['clientName']['resultSet']['client_type'];
							}
							$data['client_page'] = true;
							$data['client_id'] = $client_id;
						}else{
							$data['client_page'] = false;
						}						
						$data['priority'] = $this->Model_sla->getSeverities();
                        $data['pageTitle'] = 'Sla-edit';
                        $data['file'] = 'update_form_incident';
                        $this->load->view('template/front_template', $data);
                    }
                } else {
					//$data['clientName'] = $this->Model_gateways->getClientName($client_id);
                    $data['getSla'] = $this->Model_sla->getIncSlaEdit($postId);
					$data['client'] = $this->treeClientDropdown();
					if($client_page){						
						$data['clientName'] = $this->Model_sla->getClientName($client_id);
						if($data['clientName']['status'] == 'true'){
							$data['client_title'] = $data['clientName']['resultSet']['client_title'];
							$data['client_type'] = $data['clientName']['resultSet']['client_type'];
						}
						$data['client_page'] = true;
						$data['client_id'] = $client_id;
					}else{
						$data['client_page'] = false;
					}						
					
					$data['priority'] = $this->Model_sla->getSeverities();
                    $data['pageTitle'] = 'Sla-edit';
                    $data['file'] = 'update_form_incident';
					//pr($data); die;
                    $this->load->view('template/front_template', $data);
                }
            
        } else {
            redirect('unauthorized');
        }
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $sla_id = decode_url($_POST['id']);


        if ($sla_id != "") {
            $Status = $this->Model_sla->updateStatus($status, $sla_id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Event SLA is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Event SLA is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }
	
    public function ajax_changeStatusInc() {
        $status = $_POST['status'];
        $sla_id = decode_url($_POST['id']);


        if ($sla_id != "") {
            $Status = $this->Model_sla->updateIncStatus($status, $sla_id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'Incident SLA is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'Incident SLA is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }
	
	public function delete($postId = "") {

        $postId = decode_url($postId);

        if ($postId != "") {
            $deleteRole = $this->Model_sla->isDeleteSla($postId);
			//pr($deleteRole);exit;
            if ($deleteRole['status'] == 'true') {
				$this->session->set_flashdata("success_msg", "Gateway is deleted successfully..!!");
                redirect('sla/');
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('sla/');
            }
        } else {
            redirect('sla/');
        }
    }
	
	public function treeClientDropdown() {
        $parent_client_id = $this->session->userdata('client_id');
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeView($parent_client_id, 1);
        if (!empty($resultSet)) {
            foreach ($resultSet as $value) {
                //  pr($value);
                $client['id'] = $value['id'];
                $client['client_title'] = $value['client_title'];
                $client['parent_client_id'] = $value['parent_client_id'];
                $client['children'] = $this->getClientChildren($value['id']);
                $clients[] = $client;
            }
            return $clients;
        } else {
            return array();
        }
    }
     public function getClientChildren($parent_id) {
        $clients = array();
        $resultSet = $this->Model_client->getClientTreeView($parent_id);
        foreach ($resultSet as $value) {
            $client['id'] = $value['id'];
            $client['client_title'] = $value['client_title'];
            $client['parent_client_id'] = $value['parent_client_id'];
            $client['children'] = $this->getClientChildren($value['id']);
            $clients[] = $client;
        }
        return $clients;
    }
	
	public function getseconds($times)
	{
		$str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "$1:$2", $times);
		sscanf($str_time, "%d:%d", $hours, $minutes);
		$time_seconds = $hours * 3600 + $minutes * 60 ;
		return $time_seconds;
	}
	
	public function ajax_checkUniqueSlaSeverity() {
        $table = $this->input->post('table');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
		if(isset($_POST['editval'])){
			$editval = $_POST['editval'];
		}else{
			$editval = "";
		}  
		if($value != 4){
			if($value != $editval){
				$client_id = $this->input->post('client_id');
				$recordCount = $this->db->get_where($table, array($field => $value, 'client_id'=> $client_id))->num_rows();
				//echo $this->db->last_query();exit;
				if ($recordCount != '0') {
					$status['status'] = 'true';
				} else {
					$status['status'] = 'false';
				}
				echo json_encode($status);
				exit;
			}
		}
    }
}
?>