<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Incidents extends MX_Controller {

   
    public function __construct() {
        parent::__construct();
		//$this->output->enable_profiler(TRUE);
        $current_date = $this->config->item('datetime');
		$this->current_date = $this->config->item('datetime');
		$this->userId = $this->session->userdata('id');		
        $this->load->model('incidents_model','inc');
		$this->load->model("client/Model_client",'cli');

    }
	
	public function index()
	{
		$patners = array();
		$device_ids = array();
		$clientData = array();
		$categoryData = array();
		$userClients = helper_getAccessClientDropDown();
		$urgency = $this->inc->getUrgency();
		$severities = $this->inc->getAllSeverities();
		$skillset = $this->inc->getAllSkillSet();
		if(isset($userClients['resultSet']) && (count($userClients['resultSet'])>0))
		{
			$patners = $this->inc->getAllClients($userClients['resultSet']);
			if(isset($patners['resultSet']) && (count($patners['resultSet'])>0))
			{
				
				foreach($patners['resultSet'] as $client_data)
				{
					array_push($device_ids,$client_data['id']);
					$clientDetais = array(
					'id'=>$client_data['id'],
					'text'=>$client_data['client_title']
					);
					array_push($clientData,$clientDetais);
				}
				$categories = $this->inc->getAllCategory();	

				if(isset($categories['resultSet']) && (count($categories['resultSet'])>0))
				{
					foreach($categories['resultSet'] as $category_data)
					{
						$cateDetais = array(
							'id'=>$category_data['id'],
							'text'=>$category_data['category']
							);
							array_push($categoryData,$cateDetais);
						
					}
					
					
				}				
				
			}
		}
		$data['patners'] = json_encode($clientData);		
		$data['category'] = json_encode($categoryData);
		$data['address_types'] = $this->cli->getAddressTypes();		
		$data['urgency'] = $urgency;		
		$data['severities'] = $severities;		
		$data['skillset'] = $skillset;		
		$data['file'] = 'manuals/view_form';
		$this->form_validation->set_rules('patner', 'Patner', 'required');
		$this->form_validation->set_rules('client', 'Client', 'required');
		$this->form_validation->set_rules('devices', 'Devices', 'required');
		$this->form_validation->set_rules('severity', 'Event Critical', 'required');
		$this->form_validation->set_rules('skill_id', 'Severity', 'required');
		$this->form_validation->set_rules('subject', 'subject', 'required');
		$this->form_validation->set_rules('event_text', 'Description', 'required');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
		if ($this->form_validation->run() == TRUE) {		
			$clientId = $_POST['client'];
			$priorityId = $_POST['urgency'];
			$patnerId = $_POST['patner'];
			$statusCode = (isset($_POST['assign']) && !empty($_POST['assign']))?1:0;
			$getUUIDofClient = $this->inc->getAllClients($clientId);
			$ticket_data = array(
				 'patner_id' => $_POST['patner'], 
				 'subject' => $_POST['subject'], //host_name-alaram_type-Seviority-State
				 'description' => $_POST['event_text'],
				 'queue_id' => 1,
				 'owner_id' =>  $clientId, //Client_id 
				 'requestor' => $_POST['requestor'], 
				 'device_id' => $_POST['devices'], 
				 'service_id' => $_POST['serivce_id'],// from eventedge_services table 
				 'severity_id' => $_POST['severity'], 
				 'skill_id' => $_POST['skill_id'], 
				 'event_text' => $_POST['event_text'], 
				 'client_loaction' => $_POST['clientlocation'], 
				 'status_code' => $statusCode, //state_code 
				 'priority' => $priorityId, 
				 'created_by' => $this->userId, 
				 'created_on' => date('Y-m-d H:i:s'), 
				 'assigned_to' => $_POST['assign'],
				 'due_date' => date("Y-m-d H:i:s"),				 
				 'estimated_time' => date("Y-m-d H:i:s"),				 
				//'due_date' => date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))+$event['sla']), // Get due date hours from skill_set table (created on + sla hours) insert date
				// 'estimated_time' =>  date("Y-m-d H:i:s", strtotime($event['sla']. "sec")), // Get due date hours from skill_set table (created on + sla hours) convert seconds to hours 
				 'client_id' => $clientId, 
				 'client_uuid' => $getUUIDofClient['resultSet'][0]['client_uuid'], 
				 'event_id' => '', //event_id is not applicable for manual incident
				 'ticket_type_id' => 1, //ticket type id by defult it should be incident
				 'ticket_cate_id' => $_POST['category'], 
				 'ticket_sub_cate_id' => $_POST['subcategory'], 
				 'ticket_external_ref' => $_POST['external_ref'], 
				 'vendor_ref' => $_POST['vendor_ref'], 
				 'contract_type' => $_POST['contract'], 
				);
				
				$save_ticket = $this->inc->save_ticket($ticket_data);
				$incident_id = "INC-".str_pad($save_ticket, 6, '0', STR_PAD_LEFT);
					/* Get SLA, MTRS, MTTR starts */
					    $requested_date = strtotime(date("Y-m-d H:i:s"));
					   // $client_id = $clientId;
					    $client_id = $patnerId;
					    $skill_id = isset($_POST['skill_id'])?$_POST['skill_id']:"";
						$url = base_url("webapi/getIncidents/client_id/$client_id/severity_id/$skill_id/date/$requested_date");
						$sla_details = get_sla_details($url);
						$sla = ''; $mtrs = ''; $mttr = '';
						if(isset($sla_details['resultSet']) && !empty($sla_details['resultSet']))
						{
							$sla = $sla_details['resultSet']['slaDueTime'];
							$mtrs = $sla_details['resultSet']['mtrsDueTime'];
							$mttr = $sla_details['resultSet']['mttrDueTime'];
						}
					/* Get SLA, MTRS, MTTR ends */
					$update_ticket_data = array(
											'incident_id'=> $incident_id,
											'status_code'=> $statusCode,
											'sla_due_time'=> $sla,
											'mtrs_due_time'=> $mtrs,
											'mttr_due_time'=> $mttr,
											); 				
				$update_ticket = $this->inc->updateTicket($update_ticket_data,array("ticket_id"=>$save_ticket));
				$insert_activity_log = array(
												"start_time" => date('Y-m-d H:i:s'),
												"notes" => "Manual tickets",
												"ticket_id" => $save_ticket,
												"user_id" => $this->userId,
												"state_status" => $statusCode, 
											);
				$insert_ticket_log = $this->inc->save_ticket_activity($insert_activity_log);
				if($insert_activity_log)
				{
					$this->session->set_flashdata("success_msg", "Manual Ticket Details are created successfully ..!!");
					redirect('event/ticket');
				}
				else
				{
					$this->session->set_flashdata("error_msg", "Manual Ticket Details are not created ");
					redirect('manual/incidents');
				}
		}
		else
		{
			$this->load->view('template/front_template', $data);
		}
	}

	
	public function getAllClientDetails() // from client
	{
		$deviceData = '';		
		$locationsData = '';		
		
		
		if(isset($_POST['client_id']) && !empty($_POST['client_id']))
		{
			$client = $_POST['client_id'];			
			$devices = $this->inc->getOneClientDevices($client);
			
			if(isset($devices['resultSet']) && (count($devices['resultSet'])>0))
			{
				foreach($devices['resultSet'] as $device_data)
				{
					$deviceData .= '<option value='.$device_data['device_id'].'>'.$device_data['device_name'].'</option>';
				}
			}
			else
			{
				$deviceData .= '<option value="0" selected >NA</option>';
			}
			
			//get Locations
			$getUUIDofClient = $this->inc->getAllClients($client);
			if(isset($getUUIDofClient['resultSet']) && (count($getUUIDofClient['resultSet'])>0))
			{
				$getLocations = $this->inc->getOneClientAlllocations($getUUIDofClient['resultSet'][0]['client_uuid']);
				
				if(isset($getLocations['resultSet']) && (count($getLocations['resultSet'])>0))
				{
					foreach($getLocations['resultSet'] as $location_data)
					{
						$locationsData .= '<option value='.$location_data['location_uuid'].'>'.$location_data['location_name'].'</option>';	
					}	
					
				}
				else
				{
					$locationsData .= "<option value='0' selected >NA</option>";	
				}			
			}		
			
		}
		
		$response = array(
			'deviceData' => $deviceData,
			'locationsData' => $locationsData,					
		);		
		echo json_encode($response);
	}
	
	public function getAllClients()
	{
		
		$requesterData = '';
		$contractData = '';		
		$clientData = '';
		$usersData = '';
		
		if(isset($_POST['patner_id']) && !empty($_POST['patner_id']))
		{
			$patner = $_POST['patner_id'];
			$text = $_POST['text'];
			
			
			
			
			
			$getUUIDofClient = $this->inc->getAllClients($patner);
			if(isset($getUUIDofClient['resultSet']) && (count($getUUIDofClient['resultSet'])>0))
			{	
				//get requester
				$getRequester = $this->inc->getAllRequestorsFromClients($getUUIDofClient['resultSet'][0]['client_uuid']);
				
				if(isset($getRequester['resultSet']) && (count($getRequester['resultSet'])>0))
				{
					foreach($getRequester['resultSet'] as $requester_data)
					{
						$requesterData .= '<option value='.$requester_data['contact_uuid'].'>'.$requester_data['contact_name'].'</option>';	
					}				
				}
				else
				{
					$requesterData .= '<option value="0"> NA </option>';	
				}

				//get Contracts
				$getContracts = $this->inc->getContractsByClient($getUUIDofClient['resultSet'][0]['client_uuid']);
				if(isset($getContracts['resultSet']) && (count($getContracts['resultSet'])>0))
				{
									
					if(isset($getContracts['resultSet']) && (count($getContracts['resultSet'])>0))
					{
						foreach($getContracts['resultSet'] as $contract_data)
						{
							$s ='';
							if($contract_data['is_default'] == 1){ $s = "selected";}
							$contractData .= '<option value='.$contract_data['uuid'].' '.$s.'  >'.$contract_data['contract_type'].'</option>';	
						}	
						
					}
					
				}				
			}	
			
			
			
			//get Cleints
			$getClients = helper_getClientsDetails($patner);
			if(isset($getClients['resultSet']) && (count($getClients['resultSet'])>0))
			{
								
				if(isset($getClients['resultSet']) && (count($getClients['resultSet'])>0))
				{
					foreach($getClients['resultSet'] as $client_data)
					{
						$clientData .= '<option value='.$client_data['id'].'>'.$client_data['client_title'].'</option>';	
					}			
					
				}
				
			}
			else			
			{
				$clientData .= '<option value='.$patner.' selected >NA</option>';
			}
			
			
			//get Users
			$getUsers = $this->inc->getUsersListByClientId($patner);
			if(isset($getUsers['resultSet']) && (count($getUsers['resultSet'])>0))
			{
								
				if(isset($getUsers['resultSet']) && (count($getUsers['resultSet'])>0))
				{
					foreach($getUsers['resultSet'] as $user_data)
					{
						$usersData .= '<option value='.$user_data['id'].'>'.$user_data['user'].'</option>';	
					}				
				}
				
			}
			else
			{
				$usersData .= '<option value="0" selected >NA</option>';	
			}
			
			
		}
		
		$response = array(				
			'requesterData' => $requesterData,			
			'clientData' => $clientData,
			'contractData' => $contractData,
			'usersData' => $usersData,	
		);		
		echo json_encode($response);
	}
	
	public function getUsers()
	{
		if(isset($_POST['patner_id'],$_POST['skill']) && !empty($_POST['patner_id']) && !empty($_POST['skill']))
		{
			$patner = $_POST['patner_id'];
			$skill = $_POST['skill'];
			$usersData = '';
			//get Users
			$getUsers = $this->inc->getUsersListByClientId($patner,$skill);
			if(isset($getUsers['resultSet']) && (count($getUsers['resultSet'])>0))
			{
								
				if(isset($getUsers['resultSet']) && (count($getUsers['resultSet'])>0))
				{
					foreach($getUsers['resultSet'] as $user_data)
					{
						$usersData .= '<option value='.$user_data['id'].'>'.$user_data['user'].'</option>';	
					}				
				}
				
			}
			else
			{
				$usersData .= '<option value="0" selected >NA</option>';	
			}
			$response = array(			
			 'usersData' => $usersData,
			);		
			echo json_encode($response);
		}
	}
	
	public function getAllCategories()
	{
		$categoryData = '';
		$categoryId = '';
		$subcategoryData = '';
		$serviceData = '';
		$serviceId = '';
		if(isset($_POST['device_id']) && !empty($_POST['device_id']))
		{
			$device = $_POST['device_id'];
			$categories = $this->inc->getOneDeviceCategories($device);
			$subcategories = $this->inc->getOneDeviceSubCategories($device);
			$services = $this->inc->getOneDeviceServices($device);
			
			if(isset($categories['resultSet']) && (count($categories['resultSet'])>0))
			{
				foreach($categories['resultSet'] as $category_data)
				{	
					$categoryData .= '<option value='.$category_data['id'].'>'.$category_data['category'].'</option>';
				}	
				$categoryId = $category_data['id'];
			}
			if(isset($subcategories['resultSet']) && (count($subcategories['resultSet'])>0))
			{
				foreach($subcategories['resultSet'] as $subcategory_data)
				{
					$subcategoryData .= '<option value='.$subcategory_data['id'].'>'.$subcategory_data['subcategory'].'</option>';					
				}
				
			}
			if(isset($services['resultSet']) && (count($services['resultSet'])>0)) 
			{
				foreach($services['resultSet'] as $service_data)
				{
					$serviceData .= '<option value='.$service_data['service_id'].'>'.$service_data['service_description'].'</option>';
				}
				$serviceId = $service_data['service_id'];
			}
			
		}
		$response = array(
		 'category' => $categoryData,
		 'categoryId' => $categoryId,
		 'serviceId' => $serviceId,
		 'subcategory' => $subcategoryData,
		 'serivces' => $serviceData
		);
		echo json_encode($response);
	}
	
	public function getAllSubCategories()
	{
		$subcategoryData = '';
		if(isset($_POST['cate_id']) && !empty($_POST['cate_id']))
		{
			$cateId = $_POST['cate_id'];			
			$subcategories = $this->inc->getSubCategoriesByCateId($cateId);			
			
			
			if(isset($subcategories['resultSet']) && (count($subcategories['resultSet'])>0))
			{
				foreach($subcategories['resultSet'] as $subcategory_data)
				{
					$subcategoryData .= '<option value='.$subcategory_data['id'].'>'.$subcategory_data['subcategory'].'</option>';					
				}
				
			}
			
			
		}
		$response = array(	
		 'subcategory' => $subcategoryData,		
		);
		echo json_encode($response);
	}
	
	public function siddhu()
	{
		phpinfo();
	}
	
	/* Add Requestor */
    public function submitvalues() {
		//pr($_POST);exit;
        $first_name = $_POST['first_name'];
        $second_name = $_POST['second_name'];
        $last_name = $_POST['last_name'];
        $contact_name = $_POST['contact_name'];
        $phone_number = $_POST['phone_number'];
        $phone_country_code = $_POST['phone_country_code'];
        $email_address = $_POST['email_address'];
        $pn_category = $_POST['pn_category'];
        $email_category = $_POST['email_category'];
        $Client = $_POST['Client'];
		$ContactData = array();
		$EmailData = array();
		$PhoneData = array();
		$data['clientdetails'] = $this->inc->getClientName($Client);	
		
		$contact_uuid = generate_uuid('cont_');
		$ContactData['contact_uuid'] = $contact_uuid;
		$ContactData['reference_uuid'] = $data['clientdetails']['resultSet']['client_uuid'];
		$ContactData['contact_name'] = $contact_name;
		$ContactData['first_name'] = $first_name;
		$ContactData['second_name'] = $second_name;
		$ContactData['last_name'] = $last_name;	
		$resultContactData = $this->inc->insertContacts($ContactData);
		if($_POST['phone_number']!=""){
			$PhoneData['phone_uuid'] = generate_uuid('phon_');
			$PhoneData['referrence_uuid'] = $contact_uuid;
			$PhoneData['phone_number'] = $phone_number;
			$PhoneData['phone_country_code'] = $phone_country_code;
			$PhoneData['pn_category'] = $pn_category;
			$PhoneData['is_primary'] = 1;
			$resultPhoneData = $this->inc->insertPhones($PhoneData);
		}				
		
		if($_POST['email_address']!=""){
			$EmailData['email_uuid'] = generate_uuid('emai_');
			$EmailData['reference_uuid'] = $contact_uuid;
			$EmailData['email_address'] = $email_address;
			$EmailData['email_category'] = $email_category;	
			$EmailData['is_primary'] = 1;
			$resultEmailData = $this->inc->insertEmails($EmailData);
		}
		
           
		if ($resultContactData['status'] == 'true') {
				$requesterData ="";
				$getRequester = $this->inc->getAllRequestorsFromClients($data['clientdetails']['resultSet']['client_uuid']);
				
				if(isset($getRequester['resultSet']) && (count($getRequester['resultSet'])>0))
				{
					
					foreach($getRequester['resultSet'] as $requester_data)
					{
						$selected = "";
						if($requester_data['contact_uuid'] == $contact_uuid)
						{
								$selected = "selected";
						}
						$requesterData .= '<option value='.$requester_data['contact_uuid'].' '.$selected.'>'.$requester_data['contact_name'].'</option>';	
					}	
					
				}							
			$return['status'] = 'true';
			$return['requesterData'] = $requesterData;
		} else {
			$return['status'] = 'false';
		}
        
        echo $json = json_encode($return);
        die();
    }
	/* Add Requestor */	
		/* Add Client */
    public function clientSubmission() {
        		
		$ClientData = array();
		$ClientDevice = array();
		$clientAddress = array();
		$client_id = $_POST['Patner'];
		$res = $this->cli->getClientEdit($client_id);
		//pr($res);exit;
		$level = (isset($res['resultSet']->level)) ? decode_url($res['resultSet']->level) : 1;
		if ($client_id != 1) {
			$ClientData['primary_client_id'] = $res['resultSet']->primary_client_id;
		}		
		//pr($clientPost);exit;				
		$ClientData['parent_client_id'] = $client_id;
		$ClientData['level'] = $level + 1;
		$ClientData['client_title'] = $_POST['client_title'];
		$ClientData['name'] = $_POST['client_display_name'];
		$ClientData['client_type'] = 'C';
		$ClientData['client_uuid'] = generate_uuid('clie_');
		$ClientData['created_on'] = $this->current_date;
		$ClientData['created_by'] = $this->userId;		
		$resultData = $this->cli->insertClient($ClientData);		
		if ($resultData['status'] == 'true') {
			
			$lastInsertId = $resultData['lastId'];
			/* Start
			  Name:Veeru
			  Date: 26/12/17
			  Comment: Fetching client_id and client_uuid and inserting into the  eventedge_devices table
			*/			
			$ClientDevice['uuid'] = generate_uuid('ediv_');
			$ClientDevice['device_name'] = 'NA';
			$ClientDevice['client_id'] = $resultData['lastId'];
			$ClientDevice['client_uuid'] = $ClientData['client_uuid'];
			$ClientDevice['gateway_id'] = '11';
			$ClientDevice['gateway_uuid'] = 'eecg_ea69acfd-c5cd-57f5-ab6a-adc4ee5bf818';
			$ClientDevice['device_category_id'] = '6';
			$ClientDevice['device_category_uuid'] = 'dcat_7da69a77-5a7f-507c-8044-7a6291deef0c';
			$ClientDevice['service_group_uuid'] = 'sgp_98e49ce0-b0be-58bf-a485-fbbc994fafb3';
			$ClientDevice['created_on'] = $this->current_date;
			$ClientDevice['created_by'] = $this->userId;	
			//pr($ClientDevice);exit;
			$DeviceData = $this->cli->insertDevice($ClientDevice);

			/* End
			  Name:Veeru
			  Date: 26/12/17
			  Comment: Fetching client_id and client_uuid and inserting into the  eventedge_devices table
			 */	

			if($_POST['city'] !="" || $_POST['district'] !="" || $_POST['postal_code']!="" || $_POST['full_address']!="" ){
				$clientAddress[0]['city'] = $_POST['city'];
				$clientAddress[0]['district'] = $_POST['district'];
				$clientAddress[0]['state'] = $_POST['state'];
				$clientAddress[0]['country'] = $_POST['country'];
				$clientAddress[0]['postal_code'] = $_POST['postal_code'];
				$clientAddress[0]['house_no'] = $_POST['house_no'];
				$clientAddress[0]['address_type'] = $_POST['address_type'];
				$clientAddress[0]['referrence_uuid'] = $ClientDevice['client_uuid'];
				$clientAddress[0]['street'] = $_POST['street'];
				$clientAddress[0]['full_address'] = $_POST['full_address'];
				//$clientAddress[$i]['logitude'] = $clientPost['logitude'][$i];
				//$clientAddress[$i]['latitude'] = $clientPost['latitude'][$i];

				$clientAddress[0]['created_on'] = $this->current_date;
				$clientAddress[0]['created_by'] = $this->userId;
				//$clientAddress[$i]['company_id'] = $this->getCompanyId;
				$clientAddress[0]['address_uuid'] = generate_uuid('addr_');
				$clientAddress[0]['is_primary'] = 1;

				$resultDataAddress = $this->cli->insertAddress($clientAddress);
			}
			//pr($clientAddress);exit;						
			/*
			Name:Veeru
			Comment: Fetching event_column master data and inserting into the event_column_rename table
			*/
			$columnsInsert = $this->cli->getColumnsInsert($lastInsertId);
			/*
			Name:Veeru
			Comment: Fetching event_column master data and inserting into the event_column_rename table
			*/
			if ($client_id == 1) {
				$primary_client['primary_client_id'] = $lastInsertId;
				$updateStatus = $this->cli->updateClient($primary_client, $lastInsertId);
			}
			/* Start new Log */
			$oldValue="";
			$getNewData = helper_LogFetchRecord('client', 'id', $lastInsertId);
			$newValue = $getNewData['log'];
			$actions = 'CREATE';
			helper_createLogActions('client_log', $lastInsertId, $actions, $oldValue, $newValue);
			/* Start new Log */	




			/* Getting all clients for partner */
			$clientData ="";
			$getClients = helper_getClientsDetails($client_id);
			if(isset($getClients['resultSet']) && (count($getClients['resultSet'])>0))
			{
								
				if(isset($getClients['resultSet']) && (count($getClients['resultSet'])>0))
				{
					foreach($getClients['resultSet'] as $client_data)
					{
					$selected = "";
					if($client_data['id'] == $lastInsertId)
					{
							$selected = "selected";
					}						
						$clientData .= '<option value='.$client_data['id'].' '.$selected.'>'.$client_data['client_title'].'</option>';	
					}			
					
				}
				
			}
			if(isset($client_id) && !empty($client_id))
			{
				$clientData .= '<option value='.$client_id.'>'.$_POST['partner_title'].'</option>';
			}			
						
			$return['status'] = 'true';
			$return['clientData'] = $clientData;
			/* Getting all clients for partner */

			
		}else {
			$return['status'] = 'false';
		}
		echo $json = json_encode($return);
        die();

    }
	/* Add Client */
}
?>