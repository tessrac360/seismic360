 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>css/intlTelInput.css"/>
	<style>
		.manual_incident .select2-container{width:100% !important;    font-size: 13px !important;}
		.manual_incident .form-group { margin-bottom: 15px; min-height: 28px; margin-bottom: 4px;}
		.manual_incident .form-group .form-control { padding: 4px 4px; height: 29px; font-size: 13px !important;color: #333;}
		.manual_incident .select2-selection__placeholder{color: #333 !important;}
		.manual_incident{padding-top:12px;}
		.btn-submit{margin: 5px 6px 10px 0;}
		.cke_top, .cke_bottom{display:none;}
		.control-label span{float:right; margin-right:8px;}
		.cke_contents.cke_reset{height: 250px !important;}
		#basic-addon2{background:#36c6d3; border-color:#36c6d3; color:#fff !important;}
		#basic-addon2 i{color:#fff; position: relative; left: -5px;}
		.intl-tel-input .selected-flag{height:auto !important; top: 4px;}
		.marb10{margin-bottom:10px;}
		.pad7{padding:7px;}
		.form-control{height:28px; padding:3px 6px;font-size:13px;}
		.form-group{margin-bottom:6px;}	
		.manual_incident .input-group { display: flex;}
		@media screen and (min-width:1024px){
			
			.Clients-col .select2-container.select2-container--default, .Requestor .select2-container.select2-container--default{width: 89.5% !important;}
		}		
	</style>
  
</head>
<body>
<?php
//pr($address_types);exit;

$address_types_select = '<select class="select form-control address_type edited" id="id_stock_1_unit" name="address_type" >';
if ($address_types['status'] == 'true') {
    foreach ($address_types['resultSet'] as $value) {

        $address_types_select .= '<option value="' . $value['address_type_uuid'] . '">' . $value['address_type'] . '</option>';
    }
}
$address_types_select .= '</select>';
//echo $address_types_select;exit;	
?>
<div class="page-head" style="margin-bottom: 0px;">
	<!-- BEGIN PAGE TITLE -->
	<div class="page-title">
		<span class="caption-subject font-green-steel bold uppercase">Manual Incident</span>
	</div>
	<!--<div class="page-title">
								
		<a href="" class="btn btn-xs  blue"></a>
																			</div>-->
	
	<div class="clearfix"></div>
</div>
<div class="manual_incident">
  <form action="" method="POST" onSubmit="return checkForm()">
		
	<div class="col-md-4">  
		<div class="form-group">
			<label for="Client" class="control-label col-md-5">Partner<i class="required" aria-required="true">*</i> <span>:</span></label>
			<div class="col-md-7">
				<select class="form-control Patner check_has_error" data-validation="required" id="Patner" onchange="getClients(this.value)" name="patner" >
				 <option></option>
				</select>
			</div>
		</div>
	</div>
	<div class="col-md-4">	
		<div class="form-group">
			<label for="Devices" class="control-label col-md-5"> Clients<i class="required" aria-required="true">*</i> <span>:</span></label>

				<div class="col-md-7 Clients-col">
					<div class="input-group">
				<select class="form-control Clients check_has_error" id="Client" name="client" data-validation="required" onchange="getClientDetails(this.value)">
					<option></option>
				</select>				
						<span class="input-group-addon" id="basic-addon2" data-toggle="modal" data-target="#clientForm"><i class="fa fa-plus" aria-hidden="true"></i></span>
					</div>
				</div>			
		</div>
	</div>
			<div class="col-md-4">
			<div class="form-group">
				<label for="Client" class="control-label col-md-5">Client Location <span>:</span></label>
				<div class="col-md-7">
					<select class="form-control ClientLocation" id="ClientLocation" name="clientlocation">
						<option></option>
					</select>
				</div>
			</div>
			</div>

			<div class="col-md-4">	
			<div class="form-group">
				<label for="Devices" class="control-label col-md-5">Devices <span>:</span></label>
				<div class="col-md-7">
					<select class="form-control Devices" id="Devices" onchange="getCategories(this.value)" name="devices">
					 <option></option>
					</select>
				</div>
			</div>
			</div>
			<div class="col-md-4">	
			<div class="form-group">
				<label for="Devices" class="control-label col-md-5"> Service <span>:</span></label>
				<div class="col-md-7">
					<select class="form-control serivces" id="serivces"  name="serivce_id">
						<option></option>
					</select>
				</div>
			</div>
			</div>
			<div class="col-md-4">	
			<div class="form-group">
				<label for="Devices" class="control-label col-md-5"> Categories <span>:</span></label>
				<div class="col-md-7">
					<select class="form-control Categories" id="Categories" name="category" onchange="getSubCategories(this.value)">
						<option></option>
					</select>
				</div>
			</div>
			</div>
			<div class="col-md-4">	
			<div class="form-group">
				<label for="Devices" class="control-label col-md-5"> Sub-Categories <span>:</span></label>
				<div class="col-md-7">
					<select class="form-control SubCategories" id="SubCategories" name="subcategory">
						<option></option>
					</select>
				</div>
			</div>
			</div>
			
			<div class="col-md-4">	
			<div class="form-group">
				<label for="Devices" class="control-label col-md-5 ">Contract Type <span>:</span></label>
				<div class="col-md-7">
					<select class="form-control contract" id="contract" name="contract">
						<option></option>
					</select>
				</div>
			</div>
			</div>
			<div class="col-md-4">  
			<div class="form-group">
				<label for="sel1" class="control-label col-md-5"> Urgency <span>:</span></label>
				<div class="col-md-7">
				<select class="form-control urgency" id="urgency"  name="urgency">
					<?php if(isset($urgency) && (count($urgency['resultSet']) > 0)){
					foreach($urgency['resultSet'] as $urgency_data){	?>				
					<option value="<?php echo $urgency_data['urgency_id'];?>" <?php if($urgency_data['urgency_id'] == 3){echo "selected";} ?> ><?php echo $urgency_data['name'];?></option>
					<?php }} ?>
				</select>
					
				</div>
			</div>
			</div>

			<div class="col-md-4">	
			<div class="form-group">
				<label for="Devices" class="control-label col-md-5">Requestor<i class="required" aria-required="true">*</i> <span>:</span></label>
				<div class="col-md-7">
					<div class="input-group Requestor">
						<select class="form-control requester check_has_error" id="requester" name="requestor" data-validation="required">
							<option></option>
						</select>					
						<span class="input-group-addon" id="basic-addon2" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus" aria-hidden="true"></i></span>
					</div>
				</div>
			</div>
			</div>

			<div class="col-md-4">	
			<div class="form-group">
				<label for="sel1" class="control-label col-md-5">Event Critical<i class="required" aria-required="true">*</i> <span>:</span></label>
				<div class="col-md-7">
				<select class="form-control severity check_has_error" id="severity" name="severity" data-validation="required">
					<option value="">--Select Critical--</option>	
					<?php if(isset($severities) && (count($severities['resultSet']) > 0)){
						foreach($severities['resultSet'] as $severity_data){
							if($severity_data['id'] != 2){ // for OK status
						?>	 
					 <option value="<?php echo $severity_data['id'];?>"><?php echo $severity_data['severity'];?></option>			
					<?php }}} ?>
					
				</select>
					
				</div>
			</div>
			</div>
			<div class="col-md-4">	
			<div class="form-group">
				<label for="sel1" class="control-label col-md-5"> Severity<i class="required" aria-required="true">*</i> <span>:</span></label>
				<div class="col-md-7">
				<select class="form-control skill_id check_has_error" id="skill_id" name="skill_id" data-validation="required" onchange="changeUsers(this.value)" >
				<option value="">--Select Severity--</option>
					<?php if(isset($skillset) && (count($skillset['resultSet']) > 0)){
						foreach($skillset['resultSet'] as $skillset_data){	?>	 
					
					  <option value="<?php echo $skillset_data['skill_id'];?>"><?php echo $skillset_data['title'];?></option>
					
					<?php }} ?>
					
						
					</select>
				</div>
			</div>
			</div>
			<div class="col-md-4">	
			<div class="form-group">
				<label for="Devices" class="control-label col-md-5">Assign To <span>:</span></label>
				<div class="col-md-7">
					<select class="form-control Users" id="Users" name="assign">
						<option></option>
					</select>
				</div>
			</div>
			</div>
			<div class="col-md-4">	
			<div class="form-group">
				<label for="sel1" class="control-label col-md-5"> External Reference <span>:</span> </label>
				<div class="col-md-7">
				<input type="text" class="form-control" id="external_ref" name="external_ref">
					
				</div>
			</div>
			</div>

			<div class="col-md-4">	
			<div class="form-group">
				<label for="sel1" class="control-label col-md-5"> Vendor Reference <span>:</span></label>
				<div class="col-md-7">
				<input type="text" class="form-control" id="vendor_ref" name="vendor_ref">
					
				</div>
			</div>
			</div>
			
			<div class="clearfix"></div>
	
			<div class="col-md-12">	
				<div class="form-group">
					<label for="sel1" class="control-label col-md-12"> Subject<i class="required" aria-required="true">*</i> </label>
					<div class="col-md-12">
					<input type="text" class="form-control" id="subject" name="subject" data-validation="required" data-validation-error-msg="Subject is required" />						
					</div>
				</div>
			</div>
		
			<div class="col-md-12">	
				<div class="form-group">
					<label for="comment" class="control-label col-md-12"> Description </label>
					<div class="col-md-12">
						<textarea class="form-control comment" rows="5" id="comment" name="event_text" data-validation="required"></textarea>
					</div>
					<span id="commenterror"></span>
				</div>
			</div>
			
			<div class="col-md-12 padl0"><button type="submit" class="btn btn-success btn-submit pull-right">Submit</button></div>
			
	
  </form>
</div>
<!-- Modal -->
<div class="modal fade" id="modalForm" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Requestor Form</h4>
            </div>
            
            <!-- Modal Body -->
			<form role="form" id="requestorform">
            <div class="modal-body">
			<p class="statusMsg"></p>
                
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputName">First Name<span class="required" aria-required="true">*</span></label>                 
								<input class="form-control" id="first_name" name="first_name" type="text" data-validation="required" data-validation-error-msg="Please enter first name">
							</div>
						</div>
						<div class="col-md-6">
							 <div class="form-group">
								<label for="inputName">Middle Name</label>                 
								<input class="form-control" id="second_name" name="second_name" type="text">
							</div>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputName">Last Name<span class="required" aria-required="true">*</span></label>                 
								<input class="form-control" id="last_name" name="last_name" type="text" data-validation="required" data-validation-error-msg="Please enter last name">
							</div>	
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputEmail">Email<span class="required" aria-required="true">*</span></label>
                                <input class="form-control email_address" id="email_address" name="email_address" type="text" placeholder="Enter your email" data-validation="email" data-validation-error-msg="Please enter valid email" tableName="contact_emails" tableField="email_address">

							</div>
						</div>
							   <input class="form-control" id="contact_name" name="contact_name" type="hidden" data-validation="required" data-validation-error-msg="Please enter contact name">

						<div class="clearfix"></div>							
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputEmail">Phone</label>
								<input type="hidden" name="phone_country_code" id ="phone_country_code" class ="phone_country_code" value="1" />
								<input class="form-control phone_number" id="phone_number" name="phone_number" type="text" data-validation="number" data-validation-optional="true" data-validation-error-msg="Please enter only numeric values" tableName="phone_numbers" tableField="phone_number" />
  							

							</div>
						</div>
						
						<div class="clearfix"></div>
												

						<input type="hidden" name="pn_category" id="pn_category" value="work" />
						<input type="hidden" name="email_category"  id="email_category" value="work"/>
						<div class="clearfix"></div>
						
						
					</div>
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary submitBtn" >SUBMIT</button>
			
            </div>
			</form>
        </div>
    </div>
</div>
<!-- Client Form -->
<div class="modal fade" id="clientForm" role="dialog" tabindex="-1">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Client Form</h4>
            </div>
            
            <!-- Modal Body -->
			<form role="form" id="clientReuestForm">
            <div class="modal-body">
			<p class="statusMsg"></p>

					<div class="marb10">
						<div class="col-md-6 padl0">
							<div class="form-group">
								<label for="inputName" class="col-md-5">Client Name<span class="required" aria-required="true">*</span></label>                 
								<div class="col-md-7">
									<input class="form-control" id="client_title" name="client_title" type="text" data-validation="required" data-validation-error-msg="Please enter client name">
								</div>
							</div>
							

						</div>
						<div class="col-md-6 padr0">
							 <div class="form-group">
								<label for="inputName"class="control-label col-md-5">Client Display Name</label>
								<div class="col-md-7">                
									<input class="form-control" id="client_display_name" name="client_display_name" type="text">
								</div>
							</div>
						</div>
						
						<div class="clearfix"></div>						
						
					</div>
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default custom-panel">
                                <div class="panel-heading client-panel-heading">
                                    <h3 class="sub-head">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">
                                            <span>Address</span> <i class="more-less glyphicon glyphicon-minus pull-right"></i>
                                        </a>
                                    </h3>
                                </div>
                                <div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body">
                                        <div class="portlet-body pad0">                                
											<div class="pad7">
												<div class="col-md-3">
													<div class="form-group">
													<label for="form_control_1">City</label>
												  <input type="text" name="city"   class="textinput form-control city" />
														
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
													 <label for="id_stock_1_product" class="control-label requiredField">District<span class="asteriskField"></span></label>
														<input type="text" name="district"  class="textinput form-control district" />
														   
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
													 <label for="id_stock_1_product" class="control-label requiredField">State<span class="asteriskField"></span></label>
														<input type="text" name="state"  class="textinput form-control state" />
														   
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label for="id_stock_1_product" class="control-label">Country<span class="asteriskField"></span></label>
														<input type="text" name="country"  class="textinput form-control country" />
															
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-3">
													<div class="form-group">
													<label for="id_stock_1_product" class="control-label requiredField">Postal Code<span class="asteriskField"></span></label>
														<input type="text" name="postal_code"  class="textinput form-control postal_code" />
															
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
													<label for="id_stock_1_product" class="control-label requiredField">House Number<span class="asteriskField"></span></label>
													<input type="text" name="house_no"  class="textinput form-control house_no" />
															
															
													</div>
												</div>														
												<div class="col-md-3">
													<div class="form-group">
													 <label for="id_stock_1_unit" class="control-label requiredField">Address Type<span class="asteriskField"></span></label>
													<?php echo $address_types_select; ?>
														   
															
													</div>
												</div>														
												<div class="col-md-3">
													<div class="form-group">
													<label for="id_stock_1_product" class="control-label requiredField">Street<span class="asteriskField"></span></label>
													<input type="text" name="street"  class="textinput form-control street" />
															
															
													</div>
												</div>
												
												<div class="col-md-3">
													<div class="form-group">
													<label for="id_stock_1_product" class="control-label requiredField">Full Address<span class="asteriskField"></span></label>
													<textarea class="form-control full_address" rows="2" name="full_address" ></textarea>
																																															  
													</div>
												</div>														
										   </div>
                                        </div> 
                                    </div>

                                </div>
                            </div>
                        </div>					
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary submitBtn" >SUBMIT</button>
            </div>
			</form>
        </div>
    </div>
</div>
<!-- Client Form -->
<script src="<?php echo base_url() . "public/" ?>js/intlTelInput.js"></script>
<script src="<?php echo base_url() . "public/" ?>form-validator/jquery.form-validator.js"></script>
<script>
 $.validate({
      modules: 'security',     
    });
</script>


<script>

var data = <?php echo $patners ?>; 
$(document).ready(function() {
    $('.Patner').select2({
       placeholder: 'Select Partner',
       width: "180px",
       data: data,      
    }); 
	$('.Clients').select2({
       placeholder: 'Select Client',
       width: "180px",
       data: '',      
    });
	
	$('.ClientLocation').select2({
       placeholder: 'Select Client Location',
       width: "180px",
       data: '',      
    });
	
	$('.Devices').select2({
	   placeholder: 'Select Devices',
	   width: "180px",
	   data: '',      
	});
	
	$('.serivces').select2({
	   placeholder: 'Select Service',
	   width: "180px",
	   data: '',      
	});

	$('.Categories').select2({
	   placeholder: 'Select Category',
	   width: "180px",
	   data: <?php echo $category ?>,      
	});
	
	$('.SubCategories').select2({
	   placeholder: 'Select Sub-Category',
	   width: "180px",
	   data: '',      
	});
	
	$('.Users').select2({
	   placeholder: 'Select Engineer',
	   width: "180px",
	   data: '',      
	});
	
	$('.requester').select2({
	   placeholder: 'Select Requestor',
	   width: "180px",
	   data: '',      
	});
	
	$('.contract').select2({
	   placeholder: 'Select Contract',
	   width: "180px",
	   data: '',      
	});
});


function getClients(val)
{
	 $('.Clients').empty().append('<option></option>');
	 $('.Devices').empty().append('<option></option>');
	 $('.ClientLocation').empty().append('<option></option>');
	 $('.Users').empty().append('<option></option>');
	 $('.requester').empty().append('<option></option>');
	 $('.contract').empty().append('<option></option>');
	 var selectedText = $( "#Patner option:selected" ).text();
	var Url = "<?php echo base_url('manual/incidents/getAllClients'); ?>";	
	var form_data = {
				patner_id: val,
				text: selectedText,
			 };
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//console.log(msg);					
					var obj = jQuery.parseJSON(msg);
					console.log(obj);
					$('.Clients').append(obj.clientData).trigger('change'); 						
					$('.requester').append(obj.requesterData).trigger('change');
					$('.contract').append(obj.contractData).trigger('change');
					$('.Users').append(obj.usersData).trigger('change'); 
				},
			});	
		
}

function changeUsers(val)
{
	
	$('.Users').empty().append('<option></option>');
	var partner =  $(".Patner").val();
	var Url = "<?php echo base_url('manual/incidents/getUsers'); ?>";	
	var form_data = {
					patner_id: partner,
					skill: val,					
				 };
	$.ajax({
		url: Url,
		type: 'POST',
		data: form_data,
		success: function(msg) {
			//console.log(msg);					
			var obj = jQuery.parseJSON(msg);					
			$('.Users').append(obj.usersData).trigger('change'); 
		},
	});	
}

function getClientDetails(val)
{
	 $('.Devices').empty().append('<option></option>');
	 $('.ClientLocation').empty().append('<option></option>');
	 //$('.contract').empty().append('<option></option>');
	// $('.Users').empty().append('<option></option>');	 
	var Url = "<?php echo base_url('manual/incidents/getAllClientDetails'); ?>";	
	var form_data = {
				client_id: val,				
			 };
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//console.log(msg);					
					var obj = jQuery.parseJSON(msg);
					console.log(obj);										
					$('.Devices').append(obj.deviceData).trigger('change'); 					
					$('.ClientLocation').append(obj.locationsData).trigger('change');					
					//$('.Users').append(obj.usersData).trigger('change'); 					
				},
			});	
		
}



function getCategories(val)
{
	//$('.Categories').empty().append('<option></option>');
	$('.SubCategories').empty().append('<option></option>');
	var Url = "<?php echo base_url('manual/incidents/getAllCategories'); ?>";	
	var form_data = {
				device_id: val
			 };
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//console.log(msg);
					var obj = jQuery.parseJSON(msg);
					//console.log(obj);
					$('.Categories').val(obj.categoryId); 
					$('.Categories').trigger('change'); 					
					//$('.Categories').append(obj.category).trigger('change'); 					
					$('.SubCategories').append(obj.subcategory).trigger('change'); 					
					$('.serivces').append(obj.serivces).trigger('change');
					$('.serivces').val(obj.serviceId); 
					$('.serivces').trigger('change'); 
									
				},
			});	
		
}

function getSubCategories(val)
{
	$('.SubCategories').empty().append('<option></option>');
	var Url = "<?php echo base_url('manual/incidents/getAllSubCategories'); ?>";	
	var form_data = {
				cate_id: val
			 };
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//console.log(msg);
					var obj = jQuery.parseJSON(msg);				 					
					$('.SubCategories').append(obj.subcategory).trigger('change');						
				},
			});	
		
}

 CKEDITOR.replace( 'event_text' );
</script>

<script>
$("#first_name").keyup(function () {
    $('#contact_name').val($(this).val()+' '+$("#last_name").val());
});
$("#last_name").keyup(function () {
    $('#contact_name').val($("#first_name").val()+' '+$(this).val());
});

$(function() {
			$.validate({
			modules: 'security',
			form: '#requestorform',
			onSuccess: function() {	
					var Client = $('#Patner').val();
					if(Client.trim() == '' ){
					alert('Please select Patner');
					$('#Patner').focus();
					$('#modalForm').modal('hide');
					return false;
					}else{
					$.ajax({
						type:'POST',
						dataType: 'json',
						url:'<?php echo base_url('manual/incidents/submitvalues'); ?>',
						//url:'submit_form.php',
						data:$('#requestorform').serialize()+'&Client='+Client,
						beforeSend: function () {
						   // $('.submitBtn').attr("disabled","disabled");
						   // $('.modal-body').css('opacity', '.5');
						},
						success:function(response){
							if (response.status == 'true') {
							
								document.getElementById("requestorform").reset();
								$('.requester').find('option').remove().end().append(response.requesterData);
								//$('.requester').append(obj.requesterData).trigger('change');
								$('#modalForm').modal('hide');
							}else{
								$('.statusMsg').html('<span style="color:red;">Some problem occurred, please try again.</span>');
							}
							//$('.submitBtn').removeAttr("disabled");
							//$('.modal-body').css('opacity', '');
						}
					});
					}
					return false;
				
			}
			});

	
	});	


$(function() {
			$.validate({
			modules: 'security',
			form: '#clientReuestForm',
			onSuccess: function() {
				    var retValue = true;
					var field = "client_title";
					var parent_client_id = $('#Patner').val();
					var table =  "client";
					var value = $('#client_title').val();
					$.ajax({
							type: 'POST',
							
							url:'<?php echo base_url('client/ajax_checkUniqueClient'); ?>',
							dataType: 'json',
							data: {table: table, field: field, parent_client_id: parent_client_id,value: value},
							async: false,
							success: function (data) {
								if (data.status == 'true') {
									//alert('ok');
									//$('#errmsg').html(value + ' already exists!').show();
					//                    bootbox.alert(value + ' already exists!', function () {
					//                    });
									//swal("Oops!!!", value + " already exists!", "warning");
									//alert(value+' client already exists');
									$('#client_title').val('');
									$('#client_display_name').val('');
									$('#client_title').focus();
									retValue = false;
									//return false;

								} else {

								}
							}
					});	
					
					if( retValue == false){
						return retValue;
					}
					var Patner = $('#Patner').val();
					var partner_title =$("#Patner option:selected").text();
					if(Patner.trim() == '' ){
						alert('Please select patner');
						$('#Patner').focus();
						$('#clientForm').modal('hide');
						return false;
					}else{
						//alert('ok');
					$.ajax({
						type:'POST',
						dataType: 'json',
						url:'<?php echo base_url('manual/incidents/clientSubmission'); ?>',
						//url:'submit_form.php',
						data:$('#clientReuestForm').serialize()+'&Patner='+Patner+'&partner_title='+partner_title,
						beforeSend: function () {
						   // $('.submitBtn').attr("disabled","disabled");
						   // $('.modal-body').css('opacity', '.5');
						},
						success:function(response){
							if (response.status == 'true') {
							
								document.getElementById("clientReuestForm").reset();
								$('.Clients').find('option').remove().end().append(response.clientData);
								//$('.requester').append(obj.requesterData).trigger('change');
								$('#clientForm').modal('hide');
							}else{
								$('.statusMsg').html('<span style="color:red;">Some problem occurred, please try again.</span>');
							}
							//$('.submitBtn').removeAttr("disabled");
							//$('.modal-body').css('opacity', '');
						}
					});
					}
					return false;
				
			}
			});

	
	});
$('#client_title').on("change", function(e){ 

	var field = "client_title";
	var parent_client_id = $('#Patner').val();
	var table =  "client";
	var value = $(this).val();

	$.ajax({
			type: 'POST',
			
			url:'<?php echo base_url('client/ajax_checkUniqueClient'); ?>',
			dataType: 'json',
			data: {table: table, field: field, parent_client_id: parent_client_id,value: value},
			success: function (data) {
				if (data.status == 'true') {
					//$('#errmsg').html(value + ' already exists!').show();
	//                    bootbox.alert(value + ' already exists!', function () {
	//                    });
					//swal("Oops!!!", value + " already exists!", "warning");
					alert(value+' client already exists');
					$('#client_title').val('');
					$('#client_display_name').val('');					
					$('#client_title').focus();

				} else {

				}
			}
	});
	
});
$("#client_title").keyup(function () {
    $('#client_display_name').val($(this).val());
});
</script>
<script>
    $(".phone_number").intlTelInput({
       //allowDropdown: true,
      //autoHideDialCode: true,
      //autoPlaceholder: "off",
      // dropdownContainer: "body",
      // excludeCountries: ["us"],
      //formatOnDisplay: true,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      //hiddenInput: "full_number",
       initialCountry: "us",
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
       separateDialCode: true,
      utilsScript: '<?php echo base_url() . "public/" ?>js/utils.js'
    });
	
	 $('.country').click(function(){
		var y = $(this).attr('data-dial-code');
		//var country = $(this).attr('data-country-code');
		//alert(country);
		var phonethis = $(this);
		$(this).parent().parent().parent().parent().find('.phone_country_code').val(y);
		var value = $(this).parent().parent().parent().find('.phone_number').val();
		var table = $(this).parent().parent().parent().find('.phone_number').attr('tableName');
		var field = $(this).parent().parent().parent().find('.phone_number').attr('tableField');
		var baseURL = $('#baseURL').val();
		var phone_country_code = y;
		//alert(phone_country_code);
        //var old_email = $('#old_email').val();
		//alert(value);
       /*  var id = "";
        $.ajax({
            type: 'POST',
            url: baseURL + 'client/contacts/ajax_checkUniquePhone',
            dataType: 'json',
            data: {table: table, field: field, value: value,phone_country_code :phone_country_code},
            success: function (data) {
                if (data.status == 'true') {
                    //$('#errmsg').html(value + ' already exists!').show();
//                    bootbox.alert(value + ' already exists!', function () {
//                    });
                    swal("Oops!!!", value + " already exists!", "warning");
                    //$('#' + field).val('');
                    phonethis.parent().parent().parent().find('.phone_number').val('');
                    phonethis.parent().parent().parent().find('.phone_number').focus();
                } else {

                }
            }
        }); */
		
	});
	
	
</script>
<style>
.intl-tel-input.separate-dial-code .selected-flag{background-color:transparent !important;}
.intl-tel-input.separate-dial-code .selected-flag:focus, .intl-tel-input.allow-dropdown .flag-container:focus, .intl-tel-input.separate-dial-code .flag-container:focus{outline:0 !Important;}
</style>

<script>
$('.check_has_error').on('change',function(){
		
	if(this.value)
	{
		var id = $(this).parent().children('.form-error').remove();		
		$(this).parent().siblings('.form-error').remove();		
	}	
});

function checkForm()
{
	var editor_val = CKEDITOR.instances.comment.document.getBody().getChild(0).getText();
	
	if(editor_val.length > 1)
	{
		return true;
	}else
	{
		$('#commenterror').html(" <font color='red'>Please enter the description. </font>"); 
		return false;
	}
	
}

$(document).on('change', '.email_address', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	$.ajax({
		type: 'POST',
		url: baseURL + 'client/contacts/ajax_checkUniqueEmail',
		dataType: 'json',
		data: {table: table, field: field, value: value},
		success: function (data) {
			if (data.status == 'true') {
				alert("Email already exists!");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {
				
			}
		}
    });
	
});

$(document).on('change', '.phone_number', function () {	
	var baseURL = $('#baseURL').val();
	var table = $(this).attr('tableName');
	var field = $(this).attr('tableField');
	var value = this.value;
	var phone_country_code = $(this).parent().parent().find('.phone_country_code').val();
	$.ajax({
		type: 'POST',
		url: baseURL + 'client/contacts/ajax_checkUniquePhone',
		dataType: 'json',
		data: {table: table, field: field, value: value,phone_country_code :phone_country_code},
		success: function (data) {
			if (data.status == 'true') {
				alert("Phone number already exists!");
				$('#' + field).val('');
				$('#' + field).focus();
			} else {

			}
		}
	});
	
});

	

</script>


