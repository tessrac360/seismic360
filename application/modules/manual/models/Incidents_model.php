<?php

class Incidents_model extends CI_Model {

    private $tablename, $userId, $roleId, $user_type, $devices, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'temp_role';
        $this->client = 'client';
        $this->contacts = 'contacts';
        $this->devices = 'devices';
        $this->device_categories = 'device_categories';
        $this->device_sub_categories = 'device_sub_categories';
        $this->client_locations = 'client_locations';
        $this->ticket_urgency = 'ticket_urgency';
        $this->severities = 'severities';
        $this->skill_set = 'skill_set';
		$this->getCompanyId = helpler_getCompanyId();
        $this->users = 'users';
        $this->events = 'events';
        $this->tickets = 'tickets';
		$this->ticket_status = 'ticket_status';
        $this->ticket_queue = 'ticket_queue';
        $this->ticket_activity = 'ticket_activity';
        $this->activity_type = 'activity_type';
		$this->phone_numbers = 'phone_numbers';
		$this->emails = 'contact_emails';		
        $this->incident_activity_log = 'incident_activity_log';
        $this->ssh_log = 'ssh_log';       
        $current_date = $this->config->item('datetime');		
    }
	
	//SELECT  urgency_id, name FROM `eventedge_ticket_urgency`
	public function getUrgency(){
		
        $this->db->select('urgency_id,name');
        $this->db->from('ticket_urgency');		
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getAllClients($ids){
	 
       $this->db->select('cnt.id,cnt.client_title,cnt.client_uuid,cn.client_title as parent_name,cn.partner_id');
       $this->db->from('client as cnt');       
       $this->db->join('client as cn', 'cn.id = cnt.parent_client_id');         
       $this->db->where_in('cnt.id', $ids);
		
		
		
		
      /*  $this->db->select('id,client_title,client_uuid');
        $this->db->from('client');
		$this->db->where('status', 'Y');
		$this->db->where_in('id', $ids);*/
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	public function getAllRequestorsFromClients($id){
		
        $this->db->select('contact_uuid,contact_name,reference_uuid');
        $this->db->from('contacts');
		$this->db->where('status', 'Y');
		$this->db->where('reference_uuid', $id);
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getContractsByClient($id){
		
       $query = $this->db->query("SELECT c.uuid, (SELECT contract_type FROM eventedge_contract_types WHERE uuid = c.contract_type_uuid) as contract_type, (SELECT is_default FROM eventedge_contract_types WHERE uuid = c.contract_type_uuid) as is_default  FROM `eventedge_contracts` c WHERE reference_uuid = '{$id}' AND validity_status = 'Active'");	
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getAllSeverities(){
		
        $this->db->select('id,severity');
        $this->db->from('severities');
		$this->db->where('status', 'Y');		
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getAllSkillSet(){
		
        $this->db->select('skill_id,title');
        $this->db->from('skill_set');
		$this->db->where('status', 'Y');		
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	//SELECT   FROM `eventedge_devices` WHERE  client_id IN(57,27) AND status = 'Y'
	public function getAllClientDevices($client_ids){
		
        $this->db->select('device_id, device_name,client_id,device_category_id,device_sub_category');
        $this->db->from('devices');
		$this->db->where('status', 'Y');
		$this->db->where_in('client_id', $client_ids);
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getOneClientDevices($client_id){
		
        $this->db->select('device_id, device_name,client_id,device_category_id,device_sub_category');
        $this->db->from('devices');
		$this->db->where('status', 'Y');
		$this->db->where('client_id', $client_id);
        $query = $this->db->get();       
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	public function getOneDeviceCategories($device_id){
		
        $query = $this->db->query("SELECT id,category FROM `eventedge_device_categories` WHERE status = 'Y' AND  id = (SELECT device_category_id FROM `eventedge_devices` WHERE device_id = {$device_id})");	
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	public function getOneDeviceSubCategories($device_id){
		
        $query = $this->db->query("SELECT id,subcategory FROM `eventedge_device_sub_categories` WHERE status = 'Y' AND id = (SELECT device_sub_category FROM `eventedge_devices` WHERE device_id = {$device_id})");	
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getSubCategoriesByCateId($cate_id){
		
        $query = $this->db->query("SELECT id,subcategory FROM `eventedge_device_sub_categories` WHERE status = 'Y' AND device_category_id = {$cate_id}");	
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getOneDeviceServices($device_id){
		
        $query = $this->db->query("SELECT service_id, service_description FROM `eventedge_services` WHERE service_id  in  (SELECT service_id  FROM `eventedge_grouped_services` WHERE service_group_uuid  =  (SELECT service_group_uuid FROM `eventedge_devices`  WHERE device_id = {$device_id}))");	
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	//SELECT * FROM `eventedge_client_locations` WHERE referrence_uuid IN('clie_3a7fb8d6-2594-5309-97ce-d5a20c4bff60','clie_4f9fea38-ddfa-5fa0-8ff3-9874c8247b28')
	public function getAlllocations($location_ids){
		$this->db->group_start();
		$location_ids = array_chunk($location_ids,25);
        $this->db->select('location_uuid, location_name, referrence_uuid');
        $this->db->from('client_locations');
		foreach($location_ids as $location_id)
		{
			$this->db->where_in('referrence_uuid', $location_id);
		}
		$this->db->group_end();		
        $query = $this->db->get();  
		//echo $this->db->last_query();		
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getOneClientAlllocations($client_id){
		$this->db->select('location_uuid, location_name, referrence_uuid');
        $this->db->from('client_locations');
		$this->db->where('referrence_uuid', $client_id);			
        $query = $this->db->get();  
		//echo $this->db->last_query();		
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	//SELECT id, category FROM `eventedge_device_categories`  WHERE id IN(1,2) AND status = 'Y'
	public function getAllCntDvcCategory($category_ids){
		$this->db->group_start();
		$category_ids = array_chunk($category_ids,25);
        $this->db->select('id, category');
        $this->db->from('device_categories');
		$this->db->where('status', 'Y');
		foreach($category_ids as $category_id)
		{
			$this->db->where_in('id', $category_id);
		}
		$this->db->group_end();
        $query = $this->db->get(); 
		//echo $this->db->last_query();		
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getAllCategory(){
		
        $this->db->select('id, category');
        $this->db->from('device_categories');
		$this->db->where('status', 'Y');
		$query = $this->db->get(); 
		//echo $this->db->last_query();		
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	public function getAllCntDvcSubCategory($subcategory_ids){
		$this->db->group_start();
		$subcategory_ids = array_chunk($subcategory_ids,25);
        $this->db->select('id, subcategory');
        $this->db->from('device_sub_categories');
		$this->db->where('status', 'Y');
		foreach($subcategory_ids as $subcategory_id)
		{
			$this->db->where_in('id', $subcategory_id);
		}
		$this->db->group_end();
        $query = $this->db->get(); 
		//echo $this->db->last_query();		
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	
	public function getUsersListByCompanyId(){
		$this->db->select("id,CONCAT_WS(' ', first_name,last_name,email_id) as user");
        $this->db->from($this->users);
		$this->db->where('client',$this->getCompanyId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        //pr($return);exit;
        return $return;
	}
	
	public function getUsersListByClientId($partner_id = NULL, $skill = NULL){
		$this->db->select("id,CONCAT_WS(' ', first_name,last_name,email_id) as user");
        $this->db->from($this->users);
		$this->db->where('partner_id',$partner_id);
		$this->db->where('is_deleted',"N");
		if($skill != NULL)
		{
			$this->db->where("FIND_IN_SET($skill, skill) !=", 0);
		}
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'true';
            $return['resultSet'] = $query->result_array();
        } else {
            $return['status'] = 'false';
        }
        return $return;
	}
	
	/* public function updateTicketDetails($postData = "", $ticket_id) {
		$this->db->where('ticket_id', $ticket_id);
        $update_status = $this->db->update($this->tickets, $postData);
        //echo $this->db->last_query();exit;
        if ($update_status) {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }  */
	
	
	public function save_ticket($data)
	{
		$this->db->insert($this->tickets,$data);
		return $this->db->insert_id();
	}
	
	public function updateTicket($data,$con)
	{
		return $this->db->update($this->tickets,$data,$con);
	}
	
	public function save_ticket_activity($data)
	{
		$this->db->insert($this->ticket_activity,$data);
		return $this->db->insert_id();
	}
/* Requestor Adding */
	public function getClientName($referrence_uuid = "") {
        $this->db->select('parent_client_id,client_title,client_uuid');
        $this->db->from('client');
        $this->db->where('id', $referrence_uuid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function insertContacts($postData = array()){
		$this->db->insert($this->contacts, $postData);
		$rows = $this->db->affected_rows();
		//pr($rows);exit;
		/* $insert_id = $this->db->insert_id();
		pr($insert_id);exit; */
		if ($rows>0) {
			$return['status'] = 'true';
			$return['contactRows'] = $rows;
		} else {
			$return['status'] = 'false';
		}
		return $return;
    }
	public function insertPhones($data = array()) {
        $this->db->insert($this->phone_numbers, $data);
		//echo $this->db->last_query();exit;
    }
	public function insertEmails($data=array()) {
        $this->db->insert($this->emails, $data);
		//echo $this->db->last_query();exit;
    }	
	/* Requestor Adding */

}

?>