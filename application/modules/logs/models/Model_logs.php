<?php

class Model_logs extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->clientlog = 'client_log';
        $this->users = 'users';
    }

    public function getClient($client_id = 0, $allClients = array(), $type = 0) {
        $this->db->select('cl.id');
        $this->db->from('client as cl');
        if ($type == 0) {
            if ($client_id != 1) {
                $this->db->where('cl.id', $client_id);
            } else {
                $this->db->where('cl.parent_client_id', $client_id);
            }
        } else {
            $this->db->where('cl.parent_client_id', $client_id);
        }
        $this->db->where_not_in('cl.id', 1);
        $clients = $this->db->get();
        foreach ($clients->result_array() as $client) {

            $allClients[] = $client;
            $allClients = $this->getClient($client['id'], $allClients, 1);
        }
        return $allClients;
    }

    public function getClientLog($search = "") {
        $parent_client_id = $this->session->userdata('client_id');
        $clientId = $this->getClient($parent_client_id);
        $clientId = implode(",", array_column($clientId, 'id'));
        $clientId = explode(",", $clientId);
        $this->db->select('cl.action,CONCAT(u.first_name," ", u.last_name) as user,cl.created_on,cl.new_value,cl.old_value');
        $this->db->from('client_log as cl');
        $this->db->join('users as u', 'u.id = cl.created_by');
        if (count($search) > 0) {
            $this->db->where('DATE(cl.created_on) BETWEEN "' . date('Y-m-d', strtotime($search['from_date'])) . '" and "' . date('Y-m-d', strtotime($search['to_date'])) . '"');	
        }
        if ($parent_client_id != 1) {
            $this->db->where_in('cl.client_id', $clientId);
        }
        $this->db->order_by('cl.created_on', "desc");
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $rows = array();
        foreach ($query->result_array() as $log) {
            $data['action'] = $log['action'];
            $data['user'] = $log['user'];
            $data['created_date'] = $log['created_on'];
            $data['new_value'] = unserialize($log['new_value']);
            $data['old_value'] = unserialize($log['old_value']);
            $rows[] = $data;
        }
        return $rows;
    }
	public function getUserLog($search = ""){
		$parent_client_id = $this->session->userdata('client_id');
        $clientId = $this->getClient($parent_client_id);
        $clientId = implode(",", array_column($clientId, 'id'));
        $clientId = explode(",", $clientId);
        $this->db->select('ul.action,CONCAT(u.first_name," ", u.last_name) as user,ul.created_on,ul.new_value,ul.old_value');
        $this->db->from('user_log_manage as ul');
        $this->db->join('users as u', 'u.id = ul.created_by');
        if (count($search) > 0) {
            $this->db->where('DATE(ul.created_on) BETWEEN "' . date('Y-m-d', strtotime($search['from_date'])) . '" and "' . date('Y-m-d', strtotime($search['to_date'])) . '"');	
        }
        if ($parent_client_id != 1) {
            $this->db->where_in('ul.client_id', $clientId);
        }
        $this->db->order_by('ul.created_on', "desc");
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $rows = array();
        foreach ($query->result_array() as $log) {
            $data['action'] = $log['action'];
            $data['user'] = $log['user'];
            $data['created_date'] = $log['created_on'];
            $data['new_value'] = unserialize($log['new_value']);
            $data['old_value'] = unserialize($log['old_value']);
            $rows[] = $data;
        }
        return $rows;
	}

}

?>