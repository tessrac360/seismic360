<?php
$accessAdd = helper_fetchPermission('51', 'add');
$accessEdit = helper_fetchPermission('51', 'edit');
$accessStatus = helper_fetchPermission('51', 'active');
?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Logs</span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Logs</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="">Client Log</a>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>List</span>                            
                        </li>
                    </ul>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row" >
                        <form id="loginform" name="search" class="form-inline" method="post" action="<?php echo base_url('logs/clients/'); ?>" role="form">

                            <div class="btn-group pull-right">
                                <div class="form-group">
                                    <label class="control-label"><strong>Date Range</strong></label>
                                    <div class="input-group input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">

                                        <input id="from_date" type="text" autocomplete="off" class="form-control datepick" value="<?php echo $from_date; ?>" name="from_date">
                                        <span class="input-group-addon"> To </span>
                                        <input id="to_date" type="text" autocomplete="off" class="form-control datepick" value="<?php echo $to_date; ?>" name="to_date"> 
                                    </div>
                                    <!--<input type="submit" name="search" class="btn btn-success" value="Go">-->
                                    <input type="submit" name="search" class="btn btn-info" value="Download Excel">
                                </div>
                            </div>
                        </form>
                    </div>	
                </div>

                <table class="table table-striped table-bordered table-hover tree" id="role">
                    <tr>
                        <th> Action </th>
                        <th> Created By </th>
                        <th> Created On </th>
                        <th> New Client </th>
                        <th> Old Client </th>
                    </tr>

                    <?php
                    if (!empty($client_data)) {
                        foreach ($client_data as $log) {
                            //pr($log);
                    ?>
                    <tr>
						<td><?php echo $log['action'];
							if ($log['action'] == 'STATUS') {
								echo ($log['new_value']['status'] == "N") ? ' <b>(Inactive) </b>' : ' <b>(Active) </b>';
							} ?>
						</td>
                        <td><?php 
							if (!empty($log['user'])) {
								echo $log['user'];
							} ?>
						</td>
                        <td><?php echo date("m/d/Y h:iA", strtotime($log['created_date'])); ?></td>
                        <td><?php
                            if (!empty($log['new_value'])) {
                                echo $log['new_value']['client_title'] . '(' . $log['new_value']['parentClientName'] . ')';
                            }
                            ?>
						</td>
                        <td><?php 
							if (!empty($log['old_value'])) {
									echo $log['old_value']['client_title'] . '(' . $log['old_value']['parentClientName'] . ')';
								}
							?>
						</td>
					</tr>		
						<?php
						}
					}else{
						?>
                    <tr>
                        <td colspan="5"> No Record Found </td>
                    </tr> 
					<?php } ?>

                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <!--<?php //if($search ==''){echo $this->pagination->create_links();}  ?>-->
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_logs.js" type="text/javascript"></script>