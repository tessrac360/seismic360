<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logs extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->client_id = $this->session->userdata('client_id');
        $this->load->model("logs/Model_logs");
        $this->load->model("client/Model_client");
    }


    public function clients() {
		
        $data['from_date'] = (isset($_POST['from_date']) && trim($_POST['from_date']) != "") ? $_POST['from_date'] : date('m/d/Y', strtotime('today - ' . LOG_LAST_RECORDS . ' days'));
        $data['to_date'] = (isset($_POST['to_date']) && trim($_POST['to_date']) != "") ? $_POST['to_date'] : date('m/d/Y');
        $data['client_data'] = $this->Model_logs->getClientLog($data);
		
        if (isset($_POST['search']) && $_POST['search'] == 'Download Excel') {
            $this->downloadClientLog($data['client_data']);
        } else {
            $data['file'] = 'clientlog_form';
            $this->load->view('template/front_template', $data);
        }
    }
	
	public function users() {
		
        $data['from_date'] = (isset($_POST['from_date']) && trim($_POST['from_date']) != "") ? $_POST['from_date'] : date('m/d/Y', strtotime('today - ' . LOG_LAST_RECORDS . ' days'));
        $data['to_date'] = (isset($_POST['to_date']) && trim($_POST['to_date']) != "") ? $_POST['to_date'] : date('m/d/Y');
        $data['user_data'] = $this->Model_logs->getUserLog($data);
        if (isset($_POST['search']) && $_POST['search'] == 'Download Excel') {
            $this->downloadUserLog($data['user_data']);
        } else {
            $data['file'] = 'userlog_form';
            $this->load->view('template/front_template', $data);
        }
    }

    public function downloadClientLog($result) {

	    $this->load->library("excel");
		$heading = array('Action', 'Created By', 'Created On', 'New Client', 'Old Client');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$rowNumberH = 1;
		$colH = 'A';
		foreach ($heading as $h) {
			$objPHPExcel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
			$objPHPExcel->getActiveSheet()->getStyle($colH . $rowNumberH)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension($colH)->setWidth(30);
			$colH++;
		}
		$objPHPExcel->getActiveSheet(0)->setTitle("Log-Reports");
		$maxrow = count($result) + 1;
		$n = 2;
		$c = 0;  /* count for comments having invoices */
		if(is_array($result) && !empty($result) && count($result) > 0)
		{
			foreach ($result as $row) {
				$status = "";
				if($row['action']=='STATUS'){ $status = ($row['new_value']['status'] == "N")?' (Inactive) ':' (Active) ' ;  }
				$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $n, $row['action'].$status , PHPExcel_Cell_DataType::TYPE_STRING);
				if (!empty($row['user'])) {
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $n, $row['user'], PHPExcel_Cell_DataType::TYPE_STRING);
				}
				$objPHPExcel->getActiveSheet()->setCellValue('C' . $n, $row['created_date']);
				if (!empty($row['new_value'])) {
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $n, $row['new_value']['client_title'] . '(' . $row['new_value']['parentClientName'] . ')', PHPExcel_Cell_DataType::TYPE_STRING);
				}
				if (!empty($row['old_value'])) {
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $n, $row['old_value']['client_title'] . '(' . $row['old_value']['parentClientName'] . ')', PHPExcel_Cell_DataType::TYPE_STRING);
				}
				$n++;
			}
		}
		else{
			 $objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
			 $objPHPExcel->getActiveSheet()->setCellValueExplicit('A2', 'No data found' , PHPExcel_Cell_DataType::TYPE_STRING);
		}
		$objPHPExcel->getActiveSheet()->freezePane('A2');
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$objPHPExcel->getActiveSheet()->setAutoFilter('A1:E1');
		$objPHPExcel->getActiveSheet()->getStyle('A1:E' . $maxrow)->applyFromArray($styleArray);

		$objPHPExcel->getActiveSheet()->getStyle('A2:A' . $maxrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		if (ob_get_contents())
			ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Log-Reports-on-' . date('YmdHis') . '.xls"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
		exit();
    }
	
	public function downloadUserLog($result) {

	    $this->load->library("excel");
		$heading = array('Action', 'Created By', 'Created On', 'New Client', 'Old Client');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$rowNumberH = 1;
		$colH = 'A';
		foreach ($heading as $h) {
			$objPHPExcel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
			$objPHPExcel->getActiveSheet()->getStyle($colH . $rowNumberH)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension($colH)->setWidth(30);
			$colH++;
		}
		$objPHPExcel->getActiveSheet(0)->setTitle("Log-Reports");
		$maxrow = count($result) + 1;
		$n = 2;
		$c = 0;  /* count for comments having invoices */
		if(is_array($result) && !empty($result) && count($result) > 0)
		{
			foreach ($result as $row) {
				$status = "";
				if($row['action']=='STATUS'){ $status = ($row['new_value']['active_status'] == "N")?' (Inactive) ':' (Active) ' ;  }
				$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $n, $row['action'].$status , PHPExcel_Cell_DataType::TYPE_STRING);
				if (!empty($row['user'])) {
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $n, $row['user'], PHPExcel_Cell_DataType::TYPE_STRING);
				}
				$objPHPExcel->getActiveSheet()->setCellValue('C' . $n, $row['created_date']);
				if (!empty($row['new_value'])) {
					$objRichText = new PHPExcel_RichText();
					$objBold = $objRichText->createTextRun('User Name: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['new_value']['first_name'] . ' ' . $row['new_value']['last_name'] ."\n");

					$objBold = $objRichText->createTextRun('Phone: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['new_value']['phone']."\n");
					
					$objBold = $objRichText->createTextRun('Email: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['new_value']['email_id']."\n");
					
					$objBold = $objRichText->createTextRun('Role: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['new_value']['roleTitle']."\n");
					
					$objBold = $objRichText->createTextRun('Client: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['new_value']['clientName']."\n");
					
					$objBold = $objRichText->createTextRun('Client Access: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['new_value']['clientAccessTitles']."\n");
					
					$objBold = $objRichText->createTextRun('Domain: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['new_value']['domainTitles']);
										
					$objPHPExcel->getActiveSheet()->getCell('D' . $n)->setValue($objRichText);
				}
				$objPHPExcel->getActiveSheet()->getStyle('D'.$n)->getAlignment()->setWrapText(true);
				if (!empty($row['old_value'])) {
					$objRichText = new PHPExcel_RichText();
					$objBold = $objRichText->createTextRun('User Name: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['old_value']['first_name'] . ' ' . $row['old_value']['last_name'] ."\n");

					$objBold = $objRichText->createTextRun('Phone: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['old_value']['phone']."\n");
					
					$objBold = $objRichText->createTextRun('Email: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['old_value']['email_id']."\n");
					
					$objBold = $objRichText->createTextRun('Role: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['old_value']['roleTitle']."\n");
					
					$objBold = $objRichText->createTextRun('Client: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['old_value']['clientName']."\n");
					
					$objBold = $objRichText->createTextRun('Client Access: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['old_value']['clientAccessTitles']."\n");
					
					$objBold = $objRichText->createTextRun('Domain: ');
					$objBold->getFont()->setBold(true);
					$objRichText->createText($row['old_value']['domainTitles']);
					
					$objPHPExcel->getActiveSheet()->getCell('E' . $n)->setValue($objRichText);		
				
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$n)->getAlignment()->setWrapText(true);
				$n++;
			}
		}
		else{
			 $objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
			 $objPHPExcel->getActiveSheet()->setCellValueExplicit('A2', 'No data found' , PHPExcel_Cell_DataType::TYPE_STRING);
		}
		$objPHPExcel->getActiveSheet()->freezePane('A2');
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$objPHPExcel->getActiveSheet()->setAutoFilter('A1:E1');
		$objPHPExcel->getActiveSheet()->getStyle('A1:E' . $maxrow)->applyFromArray($styleArray);

		$objPHPExcel->getActiveSheet()->getStyle('A2:A' . $maxrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		if (ob_get_contents())
			ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Log-Reports-on-' . date('YmdHis') . '.xls"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
		exit();
    }

}
