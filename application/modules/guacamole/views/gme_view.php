
<!DOCTYPE HTML>
<html>
	<meta http-equiv="content-type" content="text/html" charset="utf-8" />
    <head>
        <!--<title>Guacamole Tutorial</title>-->
		
	<style>
		*{
			margin:0px;
			padding:0px;
			overflow: none;
		}
	</style>
    </head>

    <body>
        <!-- Guacamole -->
        <script type="text/javascript"
            src="<?php echo base_url('public/assets/gme'); ?>/all.min.js"></script>

        <!-- Display -->
        <div id="display"></div>
		<?php
			
			//include_once 'cryptor.php';
			
			
			/* $server = 'http://10.10.32.52:9000/guacamole-tunnel/tunnel';
			// encodeing Message data into json string
			
			
			$node['hostname']= 'localhost';
			$node['port']= '22';
			$node['protocal']= 'ssh';
			$node['username']= 'root';
			$node['password']= 'ltpAS8!';
			$message = json_encode($node); */
			
			
			/* $node['hostname']= '10.10.32.51';
			$node['port']= '22';
			$node['protocal']= 'ssh';
			$node['username']= 'root';
			$node['password']= 'insDevelopment51&';
			$message = json_encode($node);
			//print_r($ssh_details); exit;
			
			$server = 'http://10.10.32.51:8080/guacamole-tunnel/tunnel'; */
			$message = "";
			if(isset($ssh_details) && !empty($ssh_details))
			{
				$server_ip = (isset($ssh_details['address']))?$ssh_details['address']:'10.10.32.51';
				$server_port = (isset($ssh_details['gateway_port']))?$ssh_details['gateway_port']:'22';
				$server = "http://".$server_ip.":".$server_port."/guacamole-tunnel/tunnel";
				$node['hostname']= (isset($ssh_details['gateway_Ipaddress']))?$ssh_details['gateway_Ipaddress']:'';
				$node['port']= (isset($ssh_details['port']))?$ssh_details['port']:'';
				$node['protocal']= 'ssh';
				$node['username']= (isset($ssh_details['username']))?$ssh_details['username']:'';
				$node['password']= (isset($ssh_details['pass']))?$ssh_details['pass']:'';
				$message = json_encode($node);

			} 
			
			
			
			// generating a unique key to be used for encryption
			$client_uuid = '4s4hcgywebjbjht219845wdhh';
			//$key = sha1(mt_rand(10000,99999).time().$client_uuid);
			$key = "04bdaf8742fdf23cc83d60657e847477c235dc12";
			
			// Encrypting the message
			$encrypted = Cryptor::Encrypt($message, $key);
			
		?>
        <!-- Init -->
        <script type="text/javascript"> /* <![CDATA[ */

            // Get display div from document
            var display = document.getElementById("display");
			
			// Instantiate client, using an HTTP tunnel for communications.
            var guac = new Guacamole.Client(
               
			   new Guacamole.HTTPTunnel('<?php echo $server;?>', true)
            );
			//guac.getDisplay().scale(1);
            // Add client to display div
            display.appendChild(guac.getDisplay().getElement());
            //document.body.innerHTML = guac.getDisplay().getElement();
            // Error handler
            guac.onerror = function(error) {
                alert(error);
            };

            // Connect
            guac.connect('token=<?php echo urlencode($encrypted);?>');
			
            // Disconnect on close
            window.onunload = function() {
                guac.disconnect();
            };
			
			// Mouse
            var mouse = new Guacamole.Mouse(guac.getDisplay().getElement());

            mouse.onmousedown = 
            mouse.onmouseup   =
            mouse.onmousemove = function(mouseState) {
                guac.sendMouseState(mouseState);
            };

            // Keyboard
            var keyboard = new Guacamole.Keyboard(document);

            keyboard.onkeydown = function (keysym) {
                guac.sendKeyEvent(1, keysym);
            };

            keyboard.onkeyup = function (keysym) {
                guac.sendKeyEvent(0, keysym);
            };

        /* ]]> */ </script>
		
    </body>

</html>
