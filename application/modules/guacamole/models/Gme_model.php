<?php
class Gme_model extends CI_Model {

    private $tablename;
 
    public function __construct() {
        parent::__construct();
        $this->tablename = 'eventedge_devices';      	
    }
	
	public function get_ssh_login($id)
	{
	 $query = $this->db->query("SELECT uuid,gateway_id, address,
		(select c.username   from eventedge_system_credentials c WHERE c.referrence_uuid = uuid) as username,
		(select  c. password  from eventedge_system_credentials c WHERE c.referrence_uuid = uuid) as pass,
		(select  c. port  from eventedge_system_credentials c WHERE c.referrence_uuid = uuid) as port,
		(select  g. gateway_remote_ip  from eventedge_client_gateways g WHERE g.id = gateway_id) as gateway_Ipaddress,
		(select  g. gateway_remote_port  from eventedge_client_gateways g WHERE g.id = gateway_id) as gateway_port
		FROM `eventedge_devices`  WHERE device_id = {$id}
		 ORDER BY `device_id` DESC");
		if($query->num_rows() > 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	
	
	
	
	


}

?>