<?php  error_reporting(0); $level= $level-1;//echo "<pre>";pr($sub_client);exit;?>
<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url() . "public/" ?>treeview/treejs/js/jquery.tree.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>treeview/treejs/css/jquery.tree.css"/>
<script>
    $(document).ready(function () {
        $('#example-0 div').tree();
    });
</script>
<style>
    .ui-widget.ui-widget-content {
        border: 0px solid #cccccc;
    }
    .ui-widget-content {
        border: 0px solid #0000 !important;
        background: top repeat-x !important;
    }
</style>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Moving Client</span><span> <?php  echo "(". $title.")";?></span>
                </div>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-cog"></i>
                            <span>Managements</span>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo base_url('users') ?>">Clients</a>                            
                            <i class="fa fa-angle-right"></i>
                        </li>						
                        <li>
                            <span>Move</span>                            
                        </li>
                    </ul>                                                        
                </div>
            </div>
            <div class="portlet-body form">

                <form role="form" name="frmUser" id="frmUser" method="post" action="" autocomplete="off">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase"> Moving Childs </span>
                                </div>
                            </div>

                            <div id="groupdiv">
                                <div class="form-group" >
                                    <div class="col-sm-12" style="padding: 20px 0 10px 20px">
                                        <span class="caption-subject  uppercase" > Clients  : </span>
                                        <br>
                                        <div id="example-0" style="margin-top: 10px;">
                                            <div> 
												<?php foreach($sub_clients as $sub_client){?>
                                                <ul> 
                                                    <li>

                                                        <label class="mt-radio mt-radio-outline">                                                            <?php 
															$disable = "";$stopen="";$stclose ="";	
															if($clientMoveParent == $sub_client['id']){ 
																$disable ='readonly="readonly" checked ';
																$stopen = '<strike>';$stclose ='</strike>';
															}
														?>          
                                                            <input id="<?php echo $sub_client['id']; ?>"  type="radio" name="subClients" <?php echo $disable;?> value="<?php echo $sub_client['id'] ; ?>">
                                                            <span></span>
                                                        </label>
                                                        <label for="<?php echo $sub_client['id']; ?>"><?php echo $stopen.$sub_client['client_title'] .$stclose?></label>   
                                                        <?php
														if($sub_client['level'] >= $level){
															$sub_client['children'] = array();
														}
														if (!empty($sub_client['children'])) { ?>       
                                                            <?php
                                                            echo childTree($sub_client['children'],$clientMove,$clientMoveParent,$level);
                                                            ?>
														<?php } ?>
                                                    </li>              
                                                </ul>
												<?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" id="group_id_error"></div>
                                </div>


                            </div>

                        </div>
                        <div class="form-actions noborder">
                            <input type="hidden" name="moveClientId" value="<?php echo encode_url($clientMove); ?>" >
                            <button type="submit" class="btn green">Save</button>
                            &nbsp; &nbsp; <a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


    <?php


function childTree($treeArray = array(),$clientMove="",$clientMoveParent="",$level="") {
	
    ?>
    <ul>
    <?php foreach ($treeArray as $value) {
        ?>
            <li>
                <label class="mt-radio mt-radio-outline">
					<?php 
					$disable = "";$stopen="";$stclose ="";	
					if($clientMoveParent == $value['id']){ 
						$disable ='readonly="readonly" checked ';
						$stopen = '<strike>';$stclose ='</strike>';
					}
					?>
					<input id="<?php echo $value['id']; ?>" type="radio" <?php echo $disable;?>name="subClients" value="<?php echo $value['id']; ?>">
                    <span></span>
                </label>
                <label for="<?php echo $value['id']; ?>"><?php echo $stopen.$value['level']."-".$value['client_title'].$stclose;?></label>
				
                <?php
				if($value['level'] >= $level){
					$value['children'] = array();
				}
                if (!empty($value['children'])) {

                    echo childTree($value['children'],$clientMove,$clientMoveParent,$level);
                }
                ?>        
            </li>             
    <?php }
    ?>
    </ul>
<?php }
?>
<script>
$(':radio[readonly]').click(function(){ return false; });
</script> 
<style type="text/css">
    .optionGroup {
        font-weight: bold;        
    }
    .optionChild {
        padding-left: 15px;
    }
</style>           
