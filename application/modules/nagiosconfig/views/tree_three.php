<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url() . "public/" ?>treeview/treejs/js/jquery.tree.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "public/" ?>treeview/treejs/css/jquery.tree.css"/>
<script>
    $(document).ready(function () {
        $('#example-0 div').tree();
    });
</script>

<?php//  pr($clientData); ?>
<form role="form" name="frmClient" id="frmClient" method="post" action="">
    <div id="example-0">
        <div>           
            <ul> 
                <li><input type="checkbox" name="subClients[]" value="<?php echo $clientData['id'] .'-'. $clientData['parent_client_id']; ?>"><span><?php echo $clientData['client_title'] ?></span>   
                    <?php if (!empty($clientData['children'])) { ?>       
                        <?php
                        echo childTree($clientData['children']);
                        ?>
                    <?php } ?>
                </li>              
            </ul>
        </div>
    </div>
    <?php
    function childTree($treeArray = array()) { ?>
        <ul>
            <?php foreach ($treeArray as $value) {
                ?>
                <li><input type="checkbox" name="subClients[]" value="<?php echo $value['id'] .'-'. $value['parent_client_id'];  ?>"><span><?php echo $value['client_title'] ?></span>

                    <?php
                    if (!empty($value['children'])) {

                        echo childTree($value['children']);
                    }
                    ?>        
                </li>             
            <?php }
            ?>
        </ul>
    <?php }
    ?>

    <div class="form-actions noborder">
        <button type="submit" class="btn green">Save</button>
        &nbsp; &nbsp; <a href="<?php echo base_url('client'); ?>" class="btn default">Cancel</a>
    </div>
</form>