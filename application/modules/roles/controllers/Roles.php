<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Roles extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $this->client_id = $this->session->userdata('client_id');
        $current_user = $this->session->userdata('id');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user);
        $this->load->model('Module_roles');
        $this->load->model('Web_module/Module_model');
        $this->load->model('users/Model_users');
        $this->load->model("client/Model_client");
        $this->load->library('form_validation');
    }

    public function index() {
        $roleAccess = helper_fetchPermission('50', 'view');
        if ($roleAccess == 'Y') {
            $this->getClient();
        } else {
            redirect('unauthorized');
        }
    }

    public function getClient() {
        $search = $this->input->get_post('sd');
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'roles/getclient/';
        $config['total_rows'] = $this->Model_client->getClientPag($search, $this->client_id);
        $config['per_page'] = PAGINATION_PERPAGE;
        $config['uri_segment'] = 3;
        $config['display_pages'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data['client'] = $this->Model_client->getClientPag($search, $this->client_id, $config['per_page'], $page);
        if ($search != '') {
            $tmp = array();
            $cnt = count($data['client']);
            for ($i = 0; $i < $cnt; $i++) {
                if (preg_match("/" . $search . "/i", $data['client'][$i]['client_title'])) {
                    $tmp[] = $data['client'][$i];
                }
            }
            $data['client'] = array();
            $data['client'] = $tmp;
        }
		
        $data['client_id'] = $this->client_id;
        $data['hiddenURL'] = 'roles/getclient/';
        $data['file'] = 'view_form_client';
        $data['search'] = $search;
        $this->load->view('template/front_template', $data);
    }

    public function ajax_changeStatus() {
        $status = $_POST['status'];
        $id = decode_url($_POST['id']);
        if ($id != "") {
            $Status = $this->Module_roles->updateStatus($status, $id);
            if ($Status['status'] == 'true') {
                if ($status == 'true') {
                    $return['message'] = 'User Account is activated successfully';
                } else if ($status == 'false') {
                    $return['message'] = 'User Account is Deactivated successfully';
                }

                $return['status'] = 'true';
            } else {
                $return['status'] = 'false';
                $return['message'] = 'Error in status change';
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Something wents wrong !!';
        }
        echo $json = json_encode($return);
        die();
    }

    public function getrole() {

        $client_id = $this->input->get_post('cli');
        $client_id = decode_url($client_id);
        //echo $client_id;exit;      
        $data['role'] = $this->Module_roles->getRolesPagin($client_id);
		$data['clientName'] = $this->Module_roles->getClientName($client_id);
		if($data['clientName']['status'] == 'true'){
			$data['client_title'] = $data['clientName']['resultSet']['client_title'];
		}
        $data['hiddenURL'] = 'roles/searchData';
        $data['search'] = '';
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }

    public function searchdata() {
        $search = $this->input->get_post('sd');
        if ($search == "") {
            redirect('roles');
        }
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'roles/searchdata/';

        $data['role'] = $this->Module_roles->getSearchData($search);
        $data['search'] = $search;
        $data['hiddenURL'] = 'roles/searchData';
        $data['file'] = 'view_form';
        $this->load->view('template/front_template', $data);
    }

    public function create($client_id = NULL) {
        $client_id = decode_url($client_id);
        $roleAccess = helper_fetchPermission('50', 'add');
        if ($roleAccess == 'Y') {
            if ($this->input->post()) {
                $this->form_validation->set_rules('title', 'Role', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $group = $this->input->post();
                    $creaedby = $this->session->userdata('uid');
                    //$createdDate = date('Y-m-d H:i:s');
                    $title = $this->input->post('title');
                    $checkRole = $this->Module_roles->CheckRoleGroupData($title, $client_id);
                    if ($checkRole['status'] == 'true') {
                        $createRole = $this->Module_roles->createRole($group, $client_id);
                        if ($createRole['status'] == 'true') {
                            $roleId = $createRole['roleid'];

                            for ($i = 0; $i < count($group['roleid']); $i++) {
                                $moduleid = $group['roleid'][$i];

                                if (!empty($group['chkadd_' . $moduleid]) || !empty($group['chkedit_' . $moduleid]) || !empty($group['chkconfig_' . $moduleid]) || !empty($group['chkview_' . $moduleid]) || !empty($group['chkactive_' . $moduleid]) || !empty($group['chkdelete_' . $moduleid]))
                                    if ($group['chkadd_' . $moduleid] != "") {
                                        if ($group['chkadd_' . $moduleid]) {
                                            $chkadd = "Y";
                                        } else {
                                            $chkadd = "N";
                                        }

                                        if ($group['chkedit_' . $moduleid]) {
                                            $chkedit = "Y";
                                        } else {
                                            $chkedit = "N";
                                        }

                                        if ($group['chkview_' . $moduleid]) {
                                            $chkview = "Y";
                                        } else {
                                            $chkview = "N";
                                        }

                                        if ($group['chkactive_' . $moduleid]) {
                                            $chkactive = "Y";
                                        } else {
                                            $chkactive = "N";
                                        }

                                        if ($group['chkdelete_' . $moduleid]) {
                                            $chkdelete = "Y";
                                        } else {
                                            $chkdelete = "N";
                                        }

                                        $permissionDetails[] = array("role_id" => $roleId, 'module_id' => $moduleid, "add" => $chkadd, "edit" => $chkedit, "view" => $chkview, "active" => $chkactive, "delete" => $chkdelete);
                                    }
                            }

                            $CreatePermission = $this->Module_roles->insertPermission($permissionDetails);
                            if ($CreatePermission['status'] == 'true') {
                                $this->session->set_flashdata("success_msg", "Role Created successfully..!!");
                                redirect('roles/getrole?cli=' . encode_url($client_id));
                            } else {
                                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                                redirect('roles/create/' . encode_url($client_id));
                            }
                        } else {
                            $this->session->set_flashdata("error_msg", "Role cannot be inserted");
                            redirect('roles/create/' . encode_url($client_id));
                        }
                    } else {
                        $this->session->set_flashdata("error_msg", "Role title already exists");
                        redirect('roles/create/' . encode_url($client_id));
                    }
                } else {
                    $data['priv_data'] = $this->input->post();
                    $data['getRecords'] = $this->Module_model->getModule($client_id);
                    $data['client'] = $this->Module_roles->getClientName($client_id);
					if($data['client']['status'] == 'true'){
						$data['client_title'] = $data['client']['resultSet']['client_title'];
					}
                    $data['file'] = 'create_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['getRecords'] = $this->Module_model->getModule($client_id);
                $data['client'] = $this->Module_roles->getClientName($client_id);
				if($data['client']['status'] == 'true'){
					$data['client_title'] = $data['client']['resultSet']['client_title'];
				}
                //echo "<pre>";pr($data['client']);exit;
                $data['file'] = 'create_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function edit($postId = NULL, $client_id = NULL) {
        $client_id = decode_url($client_id);
        $roleAccess = helper_fetchPermission('50', 'edit');
        if ($roleAccess == 'Y') {
            $postId = decode_url($postId);
            if ($this->input->post()) {
                $this->form_validation->set_rules('title', 'Role', 'required');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $group = $this->input->post();
                    // pr($group);exit;
                    $creaedby = $this->session->userdata('uid');
                    //$createdDate = date('Y-m-d H:i:s');
                    $title = $this->input->post('title');
                    $hid_title = $this->input->post('hid_title');

                    $checkRole = $this->Module_roles->CheckRoleGroupDataForEdit($title, $hid_title);
                    //pr($checkRole);
                    if ($checkRole['status'] == 'true') {
                        $updateRole = $this->Module_roles->updateRole($group, $postId);
                        if ($updateRole['status'] == 'true') {
                            $isDelete = $this->Module_roles->deleteRoleById($postId);
                            if ($isDelete['status'] == 'true') {
                                $roleId = $postId;

                                for ($i = 0; $i < count($group['roleid']); $i++) {
                                    $moduleid = $group['roleid'][$i];

                                    if (!empty($group['chkadd_' . $moduleid]) || !empty($group['chkedit_' . $moduleid]) || !empty($group['chkconfig_' . $moduleid]) || !empty($group['chkview_' . $moduleid]) || !empty($group['chkactive_' . $moduleid]) || !empty($group['chkdelete_' . $moduleid]))
                                        if ($group['chkadd_' . $moduleid] != "") {
                                            if ($group['chkadd_' . $moduleid]) {
                                                $chkadd = "Y";
                                            } else {
                                                $chkadd = "N";
                                            }

                                            if ($group['chkedit_' . $moduleid]) {
                                                $chkedit = "Y";
                                            } else {
                                                $chkedit = "N";
                                            }

                                            if ($group['chkview_' . $moduleid]) {
                                                $chkview = "Y";
                                            } else {
                                                $chkview = "N";
                                            }

                                            if ($group['chkactive_' . $moduleid]) {
                                                $chkactive = "Y";
                                            } else {
                                                $chkactive = "N";
                                            }

                                            if ($group['chkdelete_' . $moduleid]) {
                                                $chkdelete = "Y";
                                            } else {
                                                $chkdelete = "N";
                                            }

                                            $permissionDetails[] = array("role_id" => $roleId, 'module_id' => $moduleid, "add" => $chkadd, "edit" => $chkedit, "view" => $chkview, "active" => $chkactive, "delete" => $chkdelete);
                                        }
                                }

                                $CreatePermission = $this->Module_roles->insertPermission($permissionDetails);
                                if ($CreatePermission['status'] == 'true') {
                                    $this->session->set_flashdata("success_msg", "Role Updated successfully..!!");
                                    redirect('roles/getrole?cli=' . encode_url($client_id));
                                } else {
                                    $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                                    redirect('roles/edit/' . encode_url($postId));
                                }
                            }
                        } else {
                            $this->session->set_flashdata("error_msg", "Something wents wrongs");
                            redirect('roles/edit/' . encode_url($postId));
                        }
                        exit;
                    } else {
                        $this->session->set_flashdata("error_msg", "Role title already exists");
                        redirect('roles/edit/' . encode_url($postId));
                    }
                } else {
                    $data['priv_data'] = $this->input->post();
                    $data['getRecords'] = $this->Module_model->getModule($client_id);
                    $data['client'] = $this->Module_roles->getClientName($client_id);
					if($data['client']['status'] == 'true'){
						$data['client_title'] = $data['client']['resultSet']['client_title'];
					}
                    $data['fetchRole'] = $this->Module_roles->fetchRoleEdit($postId);
                    $data['file'] = 'update_form';
                    $this->load->view('template/front_template', $data);
                }
            } else {
                $data['getRecords'] = $this->Module_model->getModule($client_id);
                $data['fetchRole'] = $this->Module_roles->fetchRoleEdit($postId);
                $data['client'] = $this->Module_roles->getClientName($client_id);
				if($data['client']['status'] == 'true'){
					$data['client_title'] = $data['client']['resultSet']['client_title'];
				}
                //pr($data);exit;

                $data['file'] = 'update_form';
                $this->load->view('template/front_template', $data);
            }
        } else {
            redirect('unauthorized');
        }
    }

    public function delete($postId = "", $client_id = NULL) {
        $postId = decode_url($postId);
        if ($postId != "") {
            $deleteRole = $this->Module_roles->isDeleteRole($postId);
            if ($deleteRole['status'] == 'true') {
                $this->session->set_flashdata("success_msg", "Role is deleted successfully..!!");
                redirect('roles/getrole?cli=' . $client_id);
            } else {
                $this->session->set_flashdata("error_msg", "SomeThing went worng !!");
                redirect('roles/getrole?cli=' . $client_id);
            }
        } else {
            redirect('roles');
        }
    }

    public function ajax_status() {
        $data['file'] = 'delete_form';
        $this->load->view('template/front_template', $data);
    }
    
    public function getclients() {
        $accessLevelClient = helper_getClientsDetails(55);
        pr($accessLevelClient);
    }

}
