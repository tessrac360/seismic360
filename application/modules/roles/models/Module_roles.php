<?php

class Module_roles extends CI_Model {

    private $tablename, $userId, $userUUID, $roleId, $roleUUID, $user_type, $clientUUID, $company_id = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $this->tablename = 'role';
        $this->client = 'client';
        $this->company_id = $this->session->userdata('company_id');
        $this->user_type = $this->session->userdata('user_type');
        $this->userId = $this->session->userdata('id');
		//$this->userUUID = $this->session->userdata('uuid');
        $this->client_id = $this->session->userdata('client_id');
		//$this->clientUUID = $this->session->userdata('client_uuid');
        $this->roleId = $this->session->userdata('role_id');
		//$this->roleUUID = $this->session->userdata('role_uuid');
        $current_date = $this->config->item('datetime');
        $this->cData = array('created_on' => $current_date, 'created_by' => $this->userId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $this->userId);
    }

    public function updateStatus($status = NULL, $id = NULL) {
        if ($id != "") {
            if ($status == 'true') {
                $data = array('status' => 'Y');
            } else if ($status == 'false') {
                $data = array('status' => 'N');
            }
            $data = array_merge($data, $this->uData);
            $this->db->where('id', $id);
            $this->db->update($this->tablename, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
            $return['message'] = "Something went wrong";
        }
        return $return;
    }

    public function fetchRoleEdit($Id) {
        $this->db->select('id,title,client_id,level');
        $this->db->from($this->tablename);
        $this->db->where('id', $Id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->row();    // For executing single row records            
        }
        //pr($data);exit;
        return $data;
    }

    public function getCompanyId_helper() {
       // pr($_SESSION);
        $data="";
        if ($this->user_type == 'P') {
            $data = $this->client_id;
        } else if ($this->user_type == 'A') {
            $data = $this->client_id;
        } else if ($this->user_type == 'U') {
            $data = $this->company_id;
        }
        return $data;
    }

    /*
     * Objective: insert Permission
     * parameters: Null
     * Return: Null
     */

    public function insertPermission($postPermission) {
        $insertid = $this->db->insert_batch('role_permission', $postPermission);
        if ($insertid != '') {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function fetchPermission($roleID, $moduleID) {
        $this->db->from('role_permission');
        $this->db->where('role_id', $roleID);
        $this->db->where('module_id', $moduleID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data = $row;
            }
        } else {
            $data = array();
        }
        return $data;
    }

    /*
     * Objective: Left side menu show using role_id
     * parameters: Null
     * Return: Null
     */

    public function getPermissionForLogin() {
        ///pr($_SESSION);
        $roleId = $this->session->userdata('role_id');
        $this->db->select('rp.module_id,m.title,m.icons,m.link,m.full_link,m.is_linkshow,m.status,m.primary_menuid,m.menu_level');
        $this->db->from('role_permission as rp');
        $this->db->join('module as m', 'm.id = rp.module_id');
        $this->db->where('rp.role_id', $roleId);
        $this->db->where('m.menu_level', '0');
        $this->db->where('m.status', 'Y');
		$this->db->order_by('m.order_by', "asc");
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                $this->db->select('rp.module_id,m.title,m.icons,m.link,m.full_link,m.is_linkshow,m.status,m.primary_menuid,m.menu_level');
                $this->db->from('role_permission as rp');
                $this->db->join('module as m', 'm.id = rp.module_id');
                $this->db->where('m.primary_menuid', $value->module_id);
                $this->db->where('rp.role_id', $roleId);
                $this->db->where('m.menu_level', '1');
                $this->db->where('m.status', 'Y');
                $querySubCaterory = $this->db->get();
                //echo $this->db->last_query();
                if ($querySubCaterory->num_rows() > 0) {
                    $subcat = array();
                    foreach ($querySubCaterory->result() as $valuesub) {
                        $this->db->select('rp.module_id,m.title,m.icons,m.link,m.full_link,m.is_linkshow,m.status,m.primary_menuid,m.menu_level');
                        $this->db->from('role_permission as rp');
                        $this->db->join('module as m', 'm.id = rp.module_id');
                        $this->db->where('m.primary_menuid', $valuesub->module_id);
                        $this->db->where('rp.role_id', $roleId);
                        $this->db->where('m.menu_level', '2');
                        $this->db->where('m.status', 'Y');
                        $querySubSubCaterory = $this->db->get();
                        if ($querySubSubCaterory->num_rows() > 0) {
                            $subcatlevel3 = array();
                            foreach ($querySubSubCaterory->result() as $valuesubsub) {
                                $subcatlevel3[] = $valuesubsub;
                            }
                            $valuesub->menu_second_level = $subcatlevel3;
                        } else {
                            $valuesub->menu_second_level = array();
                        }

                        $subcat[] = $valuesub;
                    }

                    $value->menu_one_level = $subcat;
                } else {
                    $value->menu_one_level = array();
                }

                $returnSet[] = $value;
            }
            // For executing single row records
            $return['status'] = 'true';
            $return['resultSet'] = $returnSet;
        } else {
            $return['status'] = 'false';
            $return['message'] = 'Records not found';
        }
        return $return;
    }

    public function CheckRoleGroupData($title="",$client_id="") {
        //$createdby = $_SESSION['uid'];
        $return = array();
        $this->db->from('role');
        $this->db->where('title', $title);
        $this->db->where('client_id', $client_id);
        $this->db->where('created_by', $this->userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return['status'] = 'false';
        } else {
            $return['status'] = 'true';
        }
        return $return;
    }

    public function CheckRoleGroupDataForEdit($title, $hidtitle,$client_id="") {
        $getCompanyId = helpler_getCompanyId();
        $return = array();
        $this->db->from('role');
        $this->db->where('title', $title);
        $this->db->where_not_in('title', $hidtitle);
        $this->db->where('client_id', $client_id);
        $this->db->where('company_id', $getCompanyId);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            $return['status'] = 'false';
        } else {
            $return['status'] = 'true';
        }
        return $return;
    }
    
     public function getClient() {   
        $getCompanyId = helpler_getCompanyId();
        $this->db->select('id,client_title');
        $this->db->from('client');
        $this->db->where('status', 'Y');
        $this->db->where('parent_client_id', $this->session->userdata('client_id'));
        //echo $this->company_id;
        //if($this->company_id != 0)
        $this->db->where('company_id', $getCompanyId);
        $query = $this->db->get();  
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->result();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }
	
	public function getClientName($clientId = "") {   
       // $getCompanyId = helpler_getCompanyId();
        $this->db->select('client_title');
        $this->db->from('client');
        $this->db->where('status', 'Y');
        $this->db->where('id', $clientId);
        //echo $this->company_id;
        //if($this->company_id != 0)
       // $this->db->where('company_id', $getCompanyId);
        $query = $this->db->get();  
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $return['resultSet'] = $query->row_array();
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    /*
     * Objective: Role Create
     * parameters: Null
     * Return: Null
     */

    public function createRole($groupPost,$client_id) {
        //pr($groupPost);exit;
        $group['title'] = $groupPost['title'];
        $group['level'] = $groupPost['level'];
        $group['client_id'] = $client_id;
        $group['uuid'] = generate_uuid('role_');
        //$group['role_id'] = $this->roleId;
        $group = array_merge($group, $this->cData);
        $group['company_id'] = helpler_getCompanyId();
        $this->db->insert('role', $group);
        $lastInsertId = $this->db->insert_id();
        if ($lastInsertId != '') {
            $return['status'] = 'true';
            $return['roleid'] = $lastInsertId;
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function updateRole($groupPost, $id) {
        unset($rolePost['hid_title']);
        $group['title'] = $groupPost['title'];
        $group['level'] = $groupPost['level'];
        $group = array_merge($group, $this->uData);
        $group['company_id'] = helpler_getCompanyId();
        $this->db->where('id', $id);
        $getdata = $this->db->update('role', $group);
        if ($getdata != '') {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    public function deleteRoleById($roleId) {
        $this->db->where('role_id', $roleId);
        $deleteStaus = $this->db->delete('role_permission');
        if ($deleteStaus != '') {
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';
        }
        return $return;
    }

    /*
     * Objective: Pagination of Role module
     * parameters: Null
     * Return: Null
     */

    public function getRolesPagin($client_id ="") {
        $getCompanyId = helpler_getCompanyId();
        $this->db->select('role.*,client.client_title,CONCAT(usr.first_name, " ", usr.last_name) AS username');
        $this->db->from($this->tablename.' as role');
        $this->db->join($this->client.' as client', 'role.client_id = client.id');
         $this->db->join('users as usr', 'usr.id = role.created_by'); 
        //$this->db->where('role.company_id', $getCompanyId); 
		$this->db->where('role.client_id', $client_id); 
        $this->db->where('role.is_deleted', 'N');
		$this->db->where('client.status', 'Y');
        $this->db->order_by('role.id', "desc");
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
               //pr($row);
                $row->isUsedRoleByUser = $this->isUsedRoleByUser($row->id,$row->client_id);
                $data[] = $row;
            }
        } else {
            $data = array();
        }
        //pr($data);exit;
        return $data;
    }
    
    private function isUsedRoleByUser($roleId="",$clientId= "") {
        return $recordCount = $this->db->get_where('users', array('role_id' => $roleId,'client'=>$clientId))->num_rows();
    }

    /*
     * Objective: Pagination of Role moudle with Searching
     * parameters: Null
     * Return: Null
     */

    public function getSearchData($search, $order_by = "id desc", $order_type = "") {
        $getCompanyId = helpler_getCompanyId();        
        $this->db->select('ro.*,client.client_title,CONCAT(usr.first_name, " ", usr.last_name) AS username');
        $this->db->from($this->tablename.' as ro');
        $this->db->join($this->client.' as client', 'ro.client_id = client.id');
        $this->db->join('users as usr', 'usr.id = ro.created_by'); 
        $this->db->where('ro.is_deleted', 'N');
        $this->db->where('ro.company_id', $getCompanyId);       
        if ($search != "") {
            $this->db->like('ro.title', $search);
        }        
        $this->db->order_by($order_by, $order_type);

        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
             foreach ($query->result() as $row) {
               //pr($row);
                $row->isUsedRoleByUser = $this->isUsedRoleByUser($row->id,$row->client_id);
                $data[] = $row;
            }
        } else {
            $data = array();
        }
       // pr($data);
        return $data;
    }

    public function fetchMethods($moduleID, $method) {
        $user = $this->session->userdata('id');
        $roleId = $this->session->userdata('role_id');
        $this->db->select('`' . $method . '` as rstatus');
        $this->db->from('role_permission');
        $this->db->where_in('role_id', $roleId);
        $this->db->where('module_id', $moduleID);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data = $row->rstatus;
            }
        } else {
            $data = 'N';
        }
        return $data;
    }
    
    public function isDeleteRole($postId) {
        if ($postId != "") {           
            $data = array('is_deleted'=>'Y');
            $this->db->where('id', $postId);
            $this->db->update($this->tablename, $data);
            $return['status'] = 'true';
        } else {
            $return['status'] = 'false';            
        }
        return $return;
        
    }

}

?>