<?php
$accessAdd = helper_fetchPermission('51', 'add');
$accessEdit = helper_fetchPermission('51', 'edit');
$accessStatus = helper_fetchPermission('51', 'active');
?>
<style>
    .toggle.btn-xs {
        width: 66px !important;
        height: 21px !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('.tree').treegrid();
        $('.tree').treegrid('collapseAll');
    });
</script>
<link rel="stylesheet" href="<?php echo base_url() . "public/" ?>css/jquery.treegrid.css">
<!-- END PAGE HEADER-->
<div class="row">
    <div>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Role</span>
            </div>
            <div class="page-toolbar">
                <ul class="page-breadcrumb breadcrumb custom-bread">
                    <li>
                        <i class="fa fa-cog"></i>
                        <span>Managements</span>  
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo base_url('roles') ?>">Roles</a>                            
                        <i class="fa fa-angle-right"></i>
                    </li>						
                    <li>
                        <span>List</span>                            
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                <table class="table table-striped table-bordered table-hover tree" id="role">

                    <tr>
                        <th> Partner Name </th>
                        <th> Created By </th>

                        <th> Action </th>  
                    </tr>




                    <?php
                    if (!empty($client)) {
                        for ($i = 0; $i < count($client); $i++) {
                            ?>
                            <tr class="treegrid-<?php echo $client[$i]['id']; ?>
                            <?php if ($client_id != 1) {
                                if ($i > 0) { ?>
                                        treegrid-parent-<?php echo $client[$i]['parent_client_id'];
                                }
                            } else { ?>
                                    <?php if ($client_id != $client[$i]['parent_client_id']) { ?>
                                        treegrid-parent-<?php echo $client[$i]['parent_client_id'];
                    } ?>

        <?php } ?>
                                " 
                                >
                                <td><?php echo $client[$i]['client_title'] ?></td><td><?php echo $client[$i]['name'] ?></td>

                                <td>




                                    <a href="<?php echo base_url() . 'roles/getrole/?cli=' . encode_url($client[$i]['id']); ?>">

                                        <button class="btn blue" style=" padding: 2px 2px;font-size:12px;" id="addsubclient" type="button">Manage Roles</button>

                                    </a>												

                                </td>										
                            </tr>		
    <?php
    }
} else {
    ?>
                        <tr>
                            <td colspan="4"> No Record Found </td>
                        </tr> 
<?php } ?>

                </table>



            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">

<?php if ($search == '') {
    echo $this->pagination->create_links();
} ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_client.js" type="text/javascript"></script>
<script src="<?php echo base_url() . "public/" ?>js/jquery.treegrid.js" type="text/javascript"></script>


