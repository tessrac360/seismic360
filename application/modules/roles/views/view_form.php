<?php
$roleAccessAdd = helper_fetchPermission('50', 'add');
$roleAccessEdit = helper_fetchPermission('50', 'edit');
$roleAccessStatus = helper_fetchPermission('50', 'active');
$roleAccessDelete = helper_fetchPermission('50', 'delete');
?>
<!-- END PAGE HEADER-->
<div class="row">
    <div>
        
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<span class="caption-subject font-green-steel bold uppercase">Role</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
			</div>
			<div class="page-toolbar">
				 <ul class="page-breadcrumb breadcrumb custom-bread">
					 <li>
						<i class="fa fa-cog"></i>
						<span>Role & Permission</span>  
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url('roles') ?>">Role</a>                            
						<i class="fa fa-angle-right"></i>
					</li>						
					<li>
						<span>List</span>                            
					</li>
				</ul>
				 <?php if ($roleAccessAdd == 'Y') { ?>
					<a id="sample_editable_1_new" class="btn btn-green" href="<?php echo base_url('roles/create/'. $_GET['cli']) ?>"> Add New Role
						<i class="fa fa-plus"></i>
					</a>
				<?php } ?>
			</div>
			<div class="clearfix"></div>
		</div>
		
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light padd0">
            <div class="portlet-body padd-top0">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="role">
                    <thead>
                        <tr>

                            <th> Client </th>
                            <th> Role Name </th>                             
                            <th> Created By </th>
                            <?php if ($roleAccessStatus == 'Y') { ?> 
                                <th> Status </th>
                            <?php } ?>
                            <?php if ($roleAccessEdit == 'Y' || $roleAccessDelete == 'Y') { ?> 
                                <th> Action </th>
                            <?php } ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($role)) {
                            foreach ($role as $value) {
                                //  pr($value);
                                ?>
                                <tr>                                   
                                    <td> <?php echo $value->client_title; ?>  </td>
                                    <td> <?php echo $value->title; ?>  </td>

                                    <td> <?php echo $value->username; ?>  </td>
                                    <?php
                                    if ($value->id)
                                        if ($roleAccessStatus == 'Y') {
                                            ?> 
                                            <td>
                                                <?php
                                                
                                                
                                                
                                                
                                                if ($value->id != 1) {
                                                    ?>  
                                                
                                                <?php if ($value->isUsedRoleByUser > 0) {  ?> 
                                                <img  src="<?php echo base_url() . 'public/images/active.png' ?>" class="isUsedRoleByUser"/> 
                                                <?php } else { ?> 
                                                        <input type="checkbox" <?php if ($value->status == 'Y') { echo 'checked'; } ?>  
                                                           class="onoffactiontoggle" myval="<?php echo encode_url($value->id); ?>" />
                                                       <?php } ?>
                                                
                                                <?php } ?>
                                                
                                                
                                            </td>
                                        <?php } ?>
                                    <?php if ($roleAccessEdit == 'Y' || $roleAccessDelete == 'Y') { ?> 
                                        <td>
                                            <?php if ($roleAccessEdit == 'Y') { ?> 
                                                <a href="<?php echo base_url() . 'roles/edit/' . encode_url($value->id).'/'.$_GET['cli']; ?>" class="btn btn-xs  blue">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            <?php } ?>
                                            <?php
                                            if ($value->id != 1) {
                                                if ($roleAccessDelete == 'Y') {
                                                    if ($value->isUsedRoleByUser > 0) {
                                                        ?> 
                                                        <a href="javascript:void(0);"  class="btn btn-xs  red isUsedRoleByUser">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a href="javascript:void(0);"  clientId="<?php echo $_GET['cli']; ?>" roleId="<?php echo encode_url($value->id); ?>" class="btn btn-xs red isdelete" onlick=''>
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </td>
                                    <?php } ?>
                                </tr> 
                                <?php
                                //pr($value);
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="5"> No Record Found </td>
                            </tr> 
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?php //echo $this->pagination->create_links(); ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<script src="<?php echo base_url() . "public/" ?>js/form/form_roles.js" type="text/javascript"></script>





