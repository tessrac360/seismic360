<style>
    .mt-checkbox, .mt-radio {
        display: inline-block;
        position: relative;
        padding-left: 30px;
        margin-bottom: 15px;
        cursor: pointer;
        font-size: 14px;
        left: 37%;
        webkit-transition: all .3s;
        -moz-transition: all .3s;
        -ms-transition: all .3s;
        -o-transition: all .3s;
        transition: all .3s;
    }

</style>

<div class="row">
    <div>

        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <span class="caption-subject font-green-steel bold uppercase">Edit Role</span>
				<span style="color:black;"><?php if(isset($client_title) && !empty($client_title)){
						echo "(".$client_title.")";
					} ?>
				</span>
            </div>
            <div class="page-toolbar">
                <ul class="page-breadcrumb breadcrumb custom-bread">
                    <li>
                        <i class="fa fa-cog"></i>
                        <span>Role & Permission</span>  
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php  echo base_url('roles') ?>">Role</a>                            
                        <i class="fa fa-angle-right"></i>
                    </li>						
                    <li>
                        <span>Edit</span>                            
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light  padd0">
            <div class="portlet-body padd-top0">
                <div class="table-toolbar">
                    <div class="row">
                        <form name="frmrole" id="frmrole" method="post">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_control_1">Client Title</label>  
                                    <input class="form-control" readonly=""  type="text" value="<?php echo ($client['resultSet']['client_title']) ? $client['resultSet']['client_title'] : "" ?>">                                    
                                    <!--<span id="errmsg" class="error" style="display: none;"></span>-->
                                </div>
                            </div>
                            <div class="col-md-4" >
                                <div class="form-group">
                                    <label for="form_control_1">Role Name</label>
                                    <input class="form-control input-sm" name="title" placeholder="Role Name" id="form_control_1" type="text" value="<?php echo ($fetchRole->title) ? $fetchRole->title : ''; ?>">
                                    <input  name="hid_title"  type="hidden" value="<?php echo ($fetchRole->title) ? $fetchRole->title : ''; ?>">                                    
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_control_1">Level<span class="required" aria-required="true">*</span></label>
                                    <select class="form-control level" name="level" id="level">
                                        <option value="">Select Level</option>
                                        <option value="1" <?php echo ($fetchRole->level=='1')?'selected':'' ?>>High</option>
                                        <option value="2" <?php echo ($fetchRole->level=='2')?'selected':'' ?>>Medium</option>
                                        <option value="3" <?php echo ($fetchRole->level=='3')?'selected':'' ?>>Low</option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-12">

                                <!-- BEGIN PORTLET-->
                                <label class="error" id="error_module" style="display: none;color: red;font-size: 12px">Please select module permission</label>
                                <div class="box-body"><div class="mt-checkbox-list">
                                        <table class="table table-condensed fixedheader" id="rolemanage" >
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>All</th>
                                                    <th>Add</th>
                                                    <th>Edit</th>
                                                    <th>View</th>
                                                    <th>Status</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <?php
                                            $roleid = $fetchRole->id;
                                            if (!empty($getRecords['resultSet'])) {
                                                foreach ($getRecords['resultSet'] as $role) {
                                                    $getPermission = helper_PermissionAccess($roleid, $role->id);

                                                    if (!empty($getPermission)) {
                                                        if ($role->is_linkshow == 'N') {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo '<strong>' . $role->title . '<strong>'; ?>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <input type="hidden" name="roleid[]" value="<?php echo $role->id; ?>" class="minimal">
                                                                    </div>

                                                                </td>
                                                                <td>
                                                                    <input type="hidden" name="chkadd_<?php echo $role->id; ?>" value="0" class="hid_chkadd_<?php echo $role->id; ?>">
                                                                    <input type="hidden" name="chkedit_<?php echo $role->id; ?>" value="0" class="hid_chkedit_<?php echo $role->id; ?>">
                                                                    <input type="hidden" name="chkactive_<?php echo $role->id; ?>" class="hid_chkactive_<?php echo $role->id; ?>" value="0">
                                                                    <input type="hidden" name="chkdelete_<?php echo $role->id; ?>" class="hid_chkdelete_<?php echo $role->id; ?>" value="0">
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkview_<?php echo $role->id; ?>" value="<?php echo ($getPermission->view == 'Y') ? '1' : '0'; ?>" class="hid_chkview_<?php echo $role->id; ?>">
                                                                            <input type="checkbox" value="chkview_<?php echo $role->id; ?>"  <?php echo ($getPermission->view == 'Y') ? 'checked' : ''; ?> class="minimal chkpermission allstatus chkview_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>

                                                                </td>
                                                                <td>

                                                                </td>
                                                            </tr>
                                                        <?php } else { ?>
                                                            <tr>
                                                                <td><?php echo '<strong>' . $role->title . '<strong>'; ?></td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="checkbox"   id="<?php echo $role->id; ?>" class="minimal allstatus allcheck checkall_<?php echo $role->id; ?>">
                                                                            <input type="hidden"  name="roleid[]" value="<?php echo $role->id; ?>" class="minimal">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkadd_<?php echo $role->id; ?>"  value="<?php echo ($getPermission->add == 'Y') ? '1' : '0'; ?>" class="hid_chkadd_<?php echo $role->id; ?>">
                                                                            <input type="checkbox" value="chkadd_<?php echo $role->id; ?>" <?php echo ($getPermission->add == 'Y') ? 'checked' : ''; ?>  class="minimal chkpermission allstatus chkadd_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkedit_<?php echo $role->id; ?>" value="<?php echo ($getPermission->edit == 'Y') ? '1' : '0'; ?>" class="hid_chkedit_<?php echo $role->id; ?>">
                                                                            <input type="checkbox" value="chkedit_<?php echo $role->id; ?>" <?php echo ($getPermission->edit == 'Y') ? 'checked' : ''; ?>  class="minimal chkpermission allstatus chkedit_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkview_<?php echo $role->id; ?>" value="<?php echo ($getPermission->view == 'Y') ? '1' : '0'; ?>" class="hid_chkview_<?php echo $role->id; ?>">
                                                                            <input type="checkbox" value="chkview_<?php echo $role->id; ?>" <?php echo ($getPermission->view == 'Y') ? 'checked' : ''; ?> class="minimal chkpermission allstatus chkview_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkactive_<?php echo $role->id; ?>" value="<?php echo ($getPermission->active == 'Y') ? '1' : '0'; ?>" class="hid_chkactive_<?php echo $role->id; ?>" >
                                                                            <input type="checkbox" value="chkactive_<?php echo $role->id; ?>" <?php echo ($getPermission->active == 'Y') ? 'checked' : ''; ?> class="minimal chkpermission allstatus chkactive_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkdelete_<?php echo $role->id; ?>" value="<?php echo ($getPermission->delete == 'Y') ? '1' : '0'; ?>" class="hid_chkdelete_<?php echo $role->id; ?>" value="0">
                                                                            <input type="checkbox" value="chkdelete_<?php echo $role->id; ?>" <?php echo ($getPermission->delete == 'Y') ? 'checked' : ''; ?>  class="minimal chkpermission allstatus chkdelete_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                            <script type="text/javascript">
                                                                $(document).ready(function () {
                                                                    if ($('.multi_<?php echo $role->id; ?>:checked').length == $('.multi_<?php echo $role->id; ?>').length) {
                                                                        $(".checkall_<?php echo $role->id; ?>")[0].checked = true; //change "select all" checked status to true
                                                                    }
                                                                });
                                                            </script>
                                                            </tr>
                                                            <?php
                                                        }
                                                    } else {
                                                        if ($role->is_linkshow == 'N') {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo '<strong>' . $role->title . '<strong>'; ?>

                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <input type="hidden" name="roleid[]" value="<?php echo $role->id; ?>" class="minimal">
                                                                    </div>

                                                                </td>
                                                                <td>
                                                                    <input type="hidden" name="chkadd_<?php echo $role->id; ?>" value="0" class="hid_chkadd_<?php echo $role->id; ?>">
                                                                    <input type="hidden" name="chkedit_<?php echo $role->id; ?>" value="0" class="hid_chkedit_<?php echo $role->id; ?>">
                                                                    <input type="hidden" name="chkactive_<?php echo $role->id; ?>" class="hid_chkactive_<?php echo $role->id; ?>" value="0">
                                                                    <input type="hidden" name="chkdelete_<?php echo $role->id; ?>" class="hid_chkdelete_<?php echo $role->id; ?>" value="0">
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkview_<?php echo $role->id; ?>" value="0" class="hid_chkview_<?php echo $role->id; ?>">
                                                                            <input type="checkbox" value="chkview_<?php echo $role->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>

                                                                </td>
                                                                <td>

                                                                </td>
                                                            </tr>
                                                        <?php } else { ?>
                                                            <tr>
                                                                <td><?php echo '<strong>' . $role->title . '<strong>'; ?></td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="checkbox"  id="<?php echo $role->id; ?>" class="minimal allstatus allcheck checkall_<?php echo $role->id; ?>">
                                                                            <input type="hidden" name="roleid[]" value="<?php echo $role->id; ?>" class="minimal">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>



                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkadd_<?php echo $role->id; ?>" value="0" class="hid_chkadd_<?php echo $role->id; ?>">
                                                                            <input type="checkbox" value="chkadd_<?php echo $role->id; ?>" class="minimal chkpermission allstatus chkadd_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkedit_<?php echo $role->id; ?>" value="0" class="hid_chkedit_<?php echo $role->id; ?>">
                                                                            <input type="checkbox" value="chkedit_<?php echo $role->id; ?>"  class="minimal chkpermission allstatus chkedit_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkview_<?php echo $role->id; ?>" value="0" class="hid_chkview_<?php echo $role->id; ?>">
                                                                            <input type="checkbox" value="chkview_<?php echo $role->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkactive_<?php echo $role->id; ?>" class="hid_chkactive_<?php echo $role->id; ?>" value="0">
                                                                            <input type="checkbox" value="chkactive_<?php echo $role->id; ?>"  class="minimal chkpermission allstatus chkactive_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mt-checkbox-list">
                                                                        <label class="mt-checkbox ">
                                                                            <input type="hidden" name="chkdelete_<?php echo $role->id; ?>" class="hid_chkdelete_<?php echo $role->id; ?>" value="0">
                                                                            <input type="checkbox" value="chkdelete_<?php echo $role->id; ?>"   class="minimal chkpermission allstatus chkdelete_<?php echo $role->id; ?> multicheck multi_<?php echo $role->id; ?>">
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <?php
                                                    if (!empty($role->menu_one_level)) {
                                                        foreach ($role->menu_one_level as $menulevelone) {
                                                            $getPermission_one = helper_PermissionAccess($roleid, $menulevelone->id);
                                                            // pr($getPermission_one);
                                                            if (!empty($getPermission_one)) {
                                                                if ($menulevelone->is_linkshow == 'N') {
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $role->title . ' <i class="fa fa-angle-right"> <strong>' . $menulevelone->title . '</strong>'; ?></td>
                                                                        <td>
                                                                            <input type="hidden" name="roleid[]" value="<?php echo $menulevelone->id; ?>" class="minimal">
                                                                        </td>
                                                                        <td>

                                                                        </td>
                                                                        <td>

                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkview_<?php echo $menulevelone->id; ?>" value="<?php echo ($getPermission_one->view == 'Y') ? '1' : '0'; ?>"  class="hid_chkview_<?php echo $menulevelone->id; ?>">
                                                                                    <input type="checkbox" value="chkview_<?php echo $menulevelone->id; ?>" <?php echo ($getPermission_one->view == 'Y') ? 'checked' : ''; ?>  class="minimal chkpermission allstatus chkview_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <input type="hidden" name="chkadd_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkadd_<?php echo $menulevelone->id; ?>">
                                                                            <input type="hidden" name="chkedit_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkedit_<?php echo $menulevelone->id; ?>">
                                                                            <input type="hidden" name="chkactive_<?php echo $menulevelone->id; ?>" class="hid_chkactive_<?php echo $menulevelone->id; ?>" value="0">
                                                                            <input type="hidden" name="chkdelete_<?php echo $menulevelone->id; ?>" class="hid_chkdelete_<?php echo $menulevelone->id; ?>" value="0">
                                                                        </td>
                                                                        <td>

                                                                        </td>
                                                                    </tr>
                                                                <?php } else { ?>
                                                                    <tr>
                                                                        <td><?php echo $role->title . ' <i class="fa fa-angle-right"> <strong>' . $menulevelone->title . '</strong>'; ?></td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="checkbox" id="<?php echo $menulevelone->id; ?>" class="minimal allstatus allcheck checkall_<?php echo $menulevelone->id; ?>">
                                                                                    <input type="hidden" name="roleid[]" value="<?php echo $menulevelone->id; ?>" class="minimal">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkadd_<?php echo $menulevelone->id; ?>"  value="<?php echo ($getPermission_one->add == 'Y') ? '1' : '0'; ?>"  value="0" class="hid_chkadd_<?php echo $menulevelone->id; ?>">
                                                                                    <input type="checkbox" <?php echo ($getPermission_one->add == 'Y') ? 'checked' : ''; ?> value="chkadd_<?php echo $menulevelone->id; ?>" class="minimal chkpermission allstatus chkadd_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkedit_<?php echo $menulevelone->id; ?>" value="<?php echo ($getPermission_one->edit == 'Y') ? '1' : '0'; ?>" class="hid_chkedit_<?php echo $menulevelone->id; ?>">
                                                                                    <input type="checkbox" <?php echo ($getPermission_one->edit == 'Y') ? 'checked' : ''; ?> value="chkedit_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkedit_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkview_<?php echo $menulevelone->id; ?>" value="<?php echo ($getPermission_one->view == 'Y') ? '1' : '0'; ?>" class="hid_chkview_<?php echo $menulevelone->id; ?>">
                                                                                    <input type="checkbox" <?php echo ($getPermission_one->view == 'Y') ? 'checked' : ''; ?> value="chkview_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkactive_<?php echo $menulevelone->id; ?>" value="<?php echo ($getPermission_one->active == 'Y') ? '1' : '0'; ?>" class="hid_chkactive_<?php echo $menulevelone->id; ?>" >
                                                                                    <input type="checkbox" <?php echo ($getPermission_one->active == 'Y') ? 'checked' : ''; ?> value="chkactive_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkactive_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkdelete_<?php echo $menulevelone->id; ?>" value="<?php echo ($getPermission_one->delete == 'Y') ? '1' : '0'; ?>" class="hid_chkdelete_<?php echo $menulevelone->id; ?>" >
                                                                                    <input type="checkbox" <?php echo ($getPermission_one->delete == 'Y') ? 'checked' : ''; ?> value="chkdelete_<?php echo $menulevelone->id; ?>"   class="minimal chkpermission allstatus chkdelete_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    <script type="text/javascript">
                                                                        $(document).ready(function () {
                                                                            if ($('.multi_<?php echo $menulevelone->id; ?>:checked').length == $('.multi_<?php echo $menulevelone->id; ?>').length) {
                                                                                $(".checkall_<?php echo $menulevelone->id; ?>")[0].checked = true; //change "select all" checked status to true
                                                                            }
                                                                        });
                                                                    </script>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            } else {
                                                                if ($menulevelone->is_linkshow == 'N') {
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $role->title . ' <i class="fa fa-angle-right"> <strong>' . $menulevelone->title . '</strong>'; ?></td>
                                                                        <td>
                                                                            <input type="hidden" name="roleid[]" value="<?php echo $menulevelone->id; ?>" class="minimal">
                                                                        </td>
                                                                        <td>

                                                                        </td>
                                                                        <td>

                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkview_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkview_<?php echo $menulevelone->id; ?>">
                                                                                    <input type="checkbox" value="chkview_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <input type="hidden" name="chkadd_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkadd_<?php echo $menulevelone->id; ?>">
                                                                            <input type="hidden" name="chkedit_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkedit_<?php echo $menulevelone->id; ?>">
                                                                            <input type="hidden" name="chkactive_<?php echo $menulevelone->id; ?>" class="hid_chkactive_<?php echo $menulevelone->id; ?>" value="0">
                                                                            <input type="hidden" name="chkdelete_<?php echo $menulevelone->id; ?>" class="hid_chkdelete_<?php echo $menulevelone->id; ?>" value="0">
                                                                        </td>
                                                                        <td>

                                                                        </td>

                                                                    </tr>
                                                                <?php } else { ?>
                                                                    <tr>
                                                                        <td><?php echo $role->title . ' <i class="fa fa-angle-right"> <strong>' . $menulevelone->title . '</strong>'; ?></td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="checkbox" id="<?php echo $menulevelone->id; ?>" class="minimal allstatus allcheck checkall_<?php echo $menulevelone->id; ?>">
                                                                                    <input type="hidden" name="roleid[]" value="<?php echo $menulevelone->id; ?>" class="minimal">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>



                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkadd_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkadd_<?php echo $menulevelone->id; ?>">
                                                                                    <input type="checkbox" value="chkadd_<?php echo $menulevelone->id; ?>" class="minimal chkpermission allstatus chkadd_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkedit_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkedit_<?php echo $menulevelone->id; ?>">
                                                                                    <input type="checkbox" value="chkedit_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkedit_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkview_<?php echo $menulevelone->id; ?>" value="0" class="hid_chkview_<?php echo $menulevelone->id; ?>">
                                                                                    <input type="checkbox" value="chkview_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkactive_<?php echo $menulevelone->id; ?>" class="hid_chkactive_<?php echo $menulevelone->id; ?>" value="0">
                                                                                    <input type="checkbox" value="chkactive_<?php echo $menulevelone->id; ?>"  class="minimal chkpermission allstatus chkactive_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="mt-checkbox-list">
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="hidden" name="chkdelete_<?php echo $menulevelone->id; ?>" class="hid_chkdelete_<?php echo $menulevelone->id; ?>" value="0">
                                                                                    <input type="checkbox" value="chkdelete_<?php echo $menulevelone->id; ?>"   class="minimal chkpermission allstatus chkdelete_<?php echo $menulevelone->id; ?> multicheck multi_<?php echo $menulevelone->id; ?>">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>



                                                            <?php
                                                            if (!empty($menulevelone->menu_second_level)) {
                                                                foreach ($menulevelone->menu_second_level as $menuleveltwo) {
                                                                    $getPermission_two = helper_PermissionAccess($roleid, $menuleveltwo->id);
                                                                    // pr($getPermission_two);
                                                                    if (!empty($getPermission_two)) {
                                                                        ?>
                                                                        <tr>
                                                                            <td><?php echo $role->title . ' <i class="fa fa-angle-right">  ' . $menulevelone->title . ' <i class="fa fa-angle-right"> <strong>' . $menuleveltwo->title . '</strong>'; ?></td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="checkbox" id="<?php echo $menuleveltwo->id; ?>" class="minimal allstatus allcheck checkall_<?php echo $menuleveltwo->id; ?>">
                                                                                        <input type="hidden" name="roleid[]" value="<?php echo $menuleveltwo->id; ?>" class="minimal">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>



                                                                            </td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="hidden" name="chkadd_<?php echo $menuleveltwo->id; ?>" value="<?php echo ($getPermission_two->add == 'Y') ? '1' : '0'; ?>"  value="0" class="hid_chkadd_<?php echo $menuleveltwo->id; ?>">
                                                                                        <input type="checkbox" <?php echo ($getPermission_two->add == 'Y') ? 'checked' : ''; ?>   value="chkadd_<?php echo $menuleveltwo->id; ?>" class="minimal chkpermission allstatus chkadd_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="hidden" name="chkedit_<?php echo $menuleveltwo->id; ?>" value="<?php echo ($getPermission_two->edit == 'Y') ? '1' : '0'; ?>" class="hid_chkedit_<?php echo $menuleveltwo->id; ?>">
                                                                                        <input type="checkbox" <?php echo ($getPermission_two->edit == 'Y') ? 'checked' : ''; ?> value="chkedit_<?php echo $menuleveltwo->id; ?>"  class="minimal chkpermission allstatus chkedit_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="hidden" name="chkview_<?php echo $menuleveltwo->id; ?>" value="<?php echo ($getPermission_two->view == 'Y') ? '1' : '0'; ?>" class="hid_chkview_<?php echo $menuleveltwo->id; ?>">
                                                                                        <input type="checkbox" <?php echo ($getPermission_two->view == 'Y') ? 'checked' : ''; ?> value="chkview_<?php echo $menuleveltwo->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="hidden" name="chkactive_<?php echo $menuleveltwo->id; ?>" value="<?php echo ($getPermission_two->active == 'Y') ? '1' : '0'; ?>" class="hid_chkactive_<?php echo $menuleveltwo->id; ?>" >
                                                                                        <input type="checkbox" <?php echo ($getPermission_two->active == 'Y') ? 'checked' : ''; ?> value="chkactive_<?php echo $menuleveltwo->id; ?>"  class="minimal chkpermission allstatus chkactive_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                                        <span></span> 
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="hidden" name="chkdelete_<?php echo $menuleveltwo->id; ?>" value="<?php echo ($getPermission_two->delete == 'Y') ? '1' : '0'; ?>" class="hid_chkdelete_<?php echo $menuleveltwo->id; ?>" >
                                                                                        <input type="checkbox" <?php echo ($getPermission_two->delete == 'Y') ? 'checked' : ''; ?> value="chkdelete_<?php echo $menuleveltwo->id; ?>"   class="minimal chkpermission allstatus chkdelete_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        <script type="text/javascript">
                                                                            $(document).ready(function () {
                                                                                if ($('.multi_<?php echo $menuleveltwo->id; ?>:checked').length == $('.multi_<?php echo $menuleveltwo->id; ?>').length) {
                                                                                    $(".checkall_<?php echo $menuleveltwo->id; ?>")[0].checked = true; //change "select all" checked status to true
                                                                                }
                                                                            });
                                                                        </script>
                                                                        </tr> 
                                                                    <?php } else { ?>
                                                                        <tr>
                                                                            <td><?php echo $role->title . ' <i class="fa fa-angle-right">' . $menulevelone->title . ' <i class="fa fa-angle-right"> <strong>' . $menuleveltwo->title . '</strong>'; ?></td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="checkbox" id="<?php echo $menuleveltwo->id; ?>" class="minimal allstatus allcheck checkall_<?php echo $menuleveltwo->id; ?>">
                                                                                        <input type="hidden" name="roleid[]" value="<?php echo $menuleveltwo->id; ?>" class="minimal">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>



                                                                            </td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="hidden" name="chkadd_<?php echo $menuleveltwo->id; ?>" value="0" class="hid_chkadd_<?php echo $menuleveltwo->id; ?>">
                                                                                        <input type="checkbox"  value="chkadd_<?php echo $menuleveltwo->id; ?>" class="minimal chkpermission allstatus chkadd_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="hidden" name="chkedit_<?php echo $menuleveltwo->id; ?>" value="0" class="hid_chkedit_<?php echo $menuleveltwo->id; ?>">
                                                                                        <input type="checkbox" value="chkedit_<?php echo $menuleveltwo->id; ?>"  class="minimal chkpermission allstatus chkedit_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="hidden" name="chkview_<?php echo $menuleveltwo->id; ?>" value="0" class="hid_chkview_<?php echo $menuleveltwo->id; ?>">
                                                                                        <input type="checkbox" value="chkview_<?php echo $menuleveltwo->id; ?>"  class="minimal chkpermission allstatus chkview_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="hidden" name="chkactive_<?php echo $menuleveltwo->id; ?>" class="hid_chkactive_<?php echo $menuleveltwo->id; ?>" value="0">
                                                                                        <input type="checkbox" value="chkactive_<?php echo $menuleveltwo->id; ?>"  class="minimal chkpermission allstatus chkactive_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="mt-checkbox-list">
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="hidden" name="chkdelete_<?php echo $menuleveltwo->id; ?>" class="hid_chkdelete_<?php echo $menuleveltwo->id; ?>" value="0">
                                                                                        <input type="checkbox" value="chkdelete_<?php echo $menuleveltwo->id; ?>"   class="minimal chkpermission allstatus chkdelete_<?php echo $menuleveltwo->id; ?> multicheck multi_<?php echo $menuleveltwo->id; ?>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    <?php }
                                                                    ?>

                                                                    <?php
                                                                }
                                                            }
                                                            ?>


                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn green">Save</button>
                                    &nbsp; &nbsp; <a href="<?php echo base_url() . 'roles'; ?>" class="btn default">cancel</a>					
                                </div>
                                <!-- END PORTLET-->
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>























<script src="<?php echo base_url() . "public/" ?>js/form/form_roles.js" type="text/javascript"  type="text/javascript"></script>

<style>
    thead, th {text-align: center; font-size: 12px !important  ;}  

    td {
        color: #333;
        font-size: 12px !important  ;
        text-shadow: none;
    }
</style>