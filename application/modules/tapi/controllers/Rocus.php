<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rocus extends MX_Controller {

   
    public function __construct() {
        parent::__construct();
		//$this->output->enable_profiler(TRUE);
        $current_date = $this->config->item('datetime');
		$this->current_date = $this->config->item('datetime');			
        $this->load->model('api_model','api');
		$this->load->model('manual/incidents_model','inc');

    }
	
	public function index()
	{
		$imap = imap_open("{outlook.office365.com:993/imap/ssl/authuser=soc@rocusnetworks.com}INBOX", 'soc@rocusnetworks.com', 'Strategic210!');

		$emails = imap_fetch_overview($imap, "1660:*", FT_UID);

		foreach ($emails as $overview) {
			
			$data = array();
			$pttr = '/=\?[^ ?]+\?[BQbq]\?/';
			if(preg_match($pttr, $overview->subject)){
				$data['subject']= imap_utf8($overview->subject);
			} else {
				$data['subject']= $overview->subject;
			}
			
			$headers = imap_header($imap, $overview->msgno);
			$fromInfo = $headers->from[0];
			
			$data['fromName'] = (isset($fromInfo->personal))? $fromInfo->personal : "";
			$data['fromAddress'] = (isset($fromInfo->mailbox) && isset($fromInfo->host))? $fromInfo->mailbox . "@" . $fromInfo->host : "";
			$data['mailDate'] = (isset($headers->udate))? $headers->udate : "";
			$mAdr = '';
			if(!empty($data['fromName'])){
				$mAdr = "'" . $data['fromName']."'";
			}
			if(!empty($data['fromAddress'])){
				$mAdr .= " &lt;" . $data['fromAddress']."&gt;";
			}
			
			$strTime = "";
			if(!empty($data['mailDate'])){
					$myTimestamp = $data['mailDate'];
					$dt = new DateTime();
					$dt->setTimezone(new DateTimeZone('IST')); // EST
					$dt->setTimestamp($myTimestamp);
					$strTime = $dt->format('F j, Y @ G:i');
			}
				
			$message=getBody($overview->uid, $imap);
			$data['mailDate'] = (isset($headers->udate))? $headers->udate : "";
			$message=preg_replace('/\r\n|\r|\n/',"<br/>",$message);
			
			
			$output = '<table width="640" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td ><div class="mail-from"><span class="mf-label" style="font-size:12px; font-weight:bold;">From: </span><span class="mf-value" style="font-size:12px;">'. $mAdr.'</span></div></td><td align="right"><div class="mail-date"><span class="md-label" style="font-size:12px; font-weight:bold;">Date: </span><span class="md-value"  style="font-size:12px;">'.$strTime.'</span></div></td></tr></table>';
			$output .= '<div class="mail-seperator"></div>';
			$output .= '<table width="640" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td style="border-bottom:1px solid #ededed; padding-top:6px;padding-bottom:10px;"><div class="mail-subject"><span class="ms-label" style="font-size:13.5px; font-weight:bold;">Subject: </span><span class="ms-value" style="font-size:13.5px;">'.$data['subject'].'</span></div></td></tr></table>';
			$output .= '<div class="mail-seperator"></div>';
			$output .= '<table width="640" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td ><div class="mail-body" style="margin-top:10px;">'.$message.'</div></td></tr></table>';
			$output = '<div class="mail-content" style="padding-top:10px;">'.$output.'</div>';
			
			$data['description'] = $output;
			
			echo $output;
			
		}
		imap_close($imap);			
	}

	function getBody($uid, $imap)
	{
		$body = get_part($imap, $uid, "TEXT/HTML");
		// if HTML body is empty, try getting text body
		if ($body == "") {
			$body = get_part($imap, $uid, "TEXT/PLAIN");
		}
		return $body;
	}

	function get_part($imap, $uid, $mimetype, $structure = false, $partNumber = false)
	{
		if (!$structure) {
			$structure = imap_fetchstructure($imap, $uid, FT_UID);
		}
		if ($structure) {
			if ($mimetype == get_mime_type($structure)) {
				if (!$partNumber) {
					$partNumber = 1;
				}
				$text = imap_fetchbody($imap, $uid, $partNumber, FT_UID);
				switch ($structure->encoding) {
					case 3:
						return imap_base64($text);
					case 4:
						return imap_qprint($text);
					default:
						return $text;
				}
			}

			// multipart
			if ($structure->type == 1) {
				foreach ($structure->parts as $index => $subStruct) {
					$prefix = "";
					if ($partNumber) {
						$prefix = $partNumber . ".";
					}
					$data = get_part($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));
					if ($data) {
						return $data;
					}
				}
			}
		}
		return false;
	}

	function get_mime_type($structure)
	{
		$primaryMimetype = ["TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER"];

		if ($structure->subtype) {
			return $primaryMimetype[(int)$structure->type] . "/" . $structure->subtype;
		}
	 return "TEXT/PLAIN";
	}
		

}
?>