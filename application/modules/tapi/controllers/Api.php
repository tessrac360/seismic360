<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends MX_Controller {

   
    public function __construct() {
        parent::__construct();
		//$this->output->enable_profiler(TRUE);
        $current_date = $this->config->item('datetime');
		$this->current_date = $this->config->item('datetime');			
        $this->load->model('api_model','api');
		$this->load->model('manual/incidents_model','inc');

    }
	
	public function index()
	{
		//pr($_POST); 
		$success = array();
		$errors = array();
		//Required
		$client_key = (isset($_POST['client_key']))?$_POST['client_key']:'';
		$requestor = (isset($_POST['requestor']))?$_POST['requestor']:'';
		$eventCritical = (isset($_POST['event_critical']))?$_POST['event_critical']:'';
		$subject = (isset($_POST['subject']))?$_POST['subject']:'';
		$desc = (isset($_POST['desc']))?$_POST['desc']:'';
		
		//optional
		$client_location = (isset($_POST['client_location']))?$_POST['client_location']:'';
		$device = (isset($_POST['device']))?$_POST['device']:'';
		$service = (isset($_POST['service']))?$_POST['service']:'';
		$category = (isset($_POST['category']))?$_POST['category']:'';// not 
		$subcategory = (isset($_POST['subcategory']))?$_POST['subcategory']:'';// not
		$urgency = (isset($_POST['urgency']))?$_POST['urgency']:'';
		$assign_to = (isset($_POST['assign_to']))?$_POST['assign_to']:'';//not
		$external_ref = (isset($_POST['external_ref']))?$_POST['external_ref']:'';
		$vendor_ref = (isset($_POST['vendor_ref']))?$_POST['vendor_ref']:'';//not
		$statusCode = (isset($_POST['assign_to']) && !empty($_POST['assign_to']))?1:0;
		$skill_id = (isset($_POST['skill_id']) && !empty($_POST['skill_id']))?$_POST['skill_id']:'';
		$cate_id = (isset($_POST['cate_id']) && !empty($_POST['cate_id']))?$_POST['cate_id']:0;
		$sub_cate_id = (isset($_POST['sub_cate_id']) && !empty($_POST['sub_cate_id']))?$_POST['sub_cate_id']:0;
		
		$success['status_code'] = $statusCode;
		$success['assign_to'] = $assign_to;
		
		// required
		if(isset($client_key) && !empty($client_key))
		{
			$clientUuid = "";
			$partner_id = "";
			$clientData = $this->api->getClient($client_key);
			if($clientData['status'] && isset($clientData['resultSet']['id']))
			{
				if($clientData['resultSet']['client_type'] == 'P')
				{
					$success['partner_id'] = $clientData['resultSet']['id'];
					$success['client_id'] = $clientData['resultSet']['id'];
				}
				else
				{
					$success['partner_id'] = $clientData['resultSet']['partner_id'];
					$success['client_id'] = $clientData['resultSet']['id'];
				}
				
				$clientUuid = $clientData['resultSet']['client_uuid'];
				$success['client_uuid'] = $clientUuid;				
								
			}else
			{
				$errors['client_key'] = "Invalid client key";
				$success['client_uuid'] = "";	
				$success['partner_id'] = "";	
			}
			
			
		}else
		{
			$errors['requestor'] = "Client key is required";
		}
		
		
		
		if(isset($requestor) && !empty($requestor))
		{
			if (!filter_var($requestor, FILTER_VALIDATE_EMAIL)) {
				$errors['requestor'] = "Invalid email format in requestor"; 
			}
			else
			{
				$requestorData = $this->api->getRequestor($requestor);
				if($requestorData['status'])
				{
					$success['requestor'] = $requestorData['resultSet']['reference_uuid'];
				}else
				{
					$errors['requestor'] = "Invalid Requestor";
				}
			}
			
		}else
		{
			$errors['requestor'] = "Requestor is required";
		}
		
		
		if(isset($eventCritical) && !empty($eventCritical))
		{
			if (!is_string($eventCritical)) {
				$errors['eventCritical'] = "Invalid event critical format"; 
			}
			else
			{
				$eventCriticalData = $this->api->getSeverities($eventCritical);
				if($eventCriticalData['status'] && isset($eventCriticalData['resultSet']['id']))
				{
					$success['eventCritical'] = $eventCriticalData['resultSet']['id'];
				}else
				{
					$errors['eventCritical'] = "Invalid event critical";
				}
			}
			
		}else
		{
			$errors['eventCritical'] = "Event critical is required";
		}
		
		
		if(isset($subject) && !empty($subject))
		{
			$success['subject'] = $subject;				
		}else
		{
			$errors['subject'] = "Subject is required";
		}
		
		if(isset($desc) && !empty($desc))
		{
			$success['desc'] = $desc;				
		}else
		{
			$errors['desc'] = "Descripiton is required";
		}
		
		
		
		//optional
		if(isset($client_location,$clientUuid) && !empty($client_location) && !empty($clientUuid))
		{
			if (!is_string($client_location)) {
				$errors['client_location'] = "Invalid location format"; 
			}
			else
			{
				$locationData = $this->api->getLocation($client_location,$clientUuid);
				if($locationData['status'] && isset($locationData['resultSet']['location_uuid']))
				{
					$success['client_location'] = $locationData['resultSet']['location_uuid'];
				}else
				{
					$errors['client_location'] = "Invalid client Location";
				}
			}
			
		}
		
		
		$success['device'] = "";
		$deviceId = "";
		if(isset($device,$clientUuid) && !empty($device) && !empty($clientUuid))
		{
			if (!is_string($device)) {
				$errors['device'] = "Invalid device format"; 
			}
			else
			{
				$deviceData = $this->api->getDevice($device,$clientUuid);
				if($deviceData['status'] && isset($deviceData['resultSet']['device_id']))
				{
					$success['device'] = $deviceData['resultSet']['device_id'];
					$deviceId = $deviceData['resultSet']['device_id'];
				}else
				{
					$success['device'] = 0;
					//$errors['device'] = "Invalid client device";
				}
			}
			
		}
		
		$success['service'] = "";
		if(isset($deviceId,$service) && !empty($deviceId) && !empty($service))
		{
			if (!is_string($service)) {
				$errors['service'] = "Invalid service format"; 
			}
			else
			{
				$serviceData = $this->api->getOneDeviceServices($deviceId,$service);
				if($serviceData['status'] && isset($serviceData['resultSet']))
				{
					$success['service'] = $serviceData['resultSet']['service_id'];
				}else
				{
					$errors['service'] = "Invalid service for this device ".$device;
				}
			}
			
		}
		
		$success['urgency'] = "";
		if(isset($urgency) && !empty($urgency))
		{
			if (!is_string($urgency)) {
				$errors['urgency'] = "Invalid urgency format"; 
			}
			else
			{
				$urgencyData = $this->api->getUrgencyByName($urgency);
				if($urgencyData['status'] && isset($urgencyData['resultSet']['urgency_id']))
				{
					$success['urgency'] = $urgencyData['resultSet']['urgency_id'];
				}else
				{
					$errors['urgency'] = "Invalid urgency";
				}
			}
			
		}

		$success['skill'] = "";
		if(isset($skill_id) && !empty($skill_id))
		{
			if (!is_string($skill_id)) {
				$errors['skill'] = "Invalid skill format"; 
			}
			else
			{
				$skillData = $this->api->getSkillByName($skill_id);
				if($skillData['status'] && isset($skillData['resultSet']['skill_id']))
				{
					$success['skill'] = $skillData['resultSet']['skill_id'];
				}else
				{
					$errors['skill'] = "Invalid skill";
				}
			}

		}
		
		$success['external_ref'] = "";
		if(isset($external_ref) && !empty($external_ref))
		{
			$success['external_ref'] = $external_ref;				
		}
		
		//get contract type, which is in active for this client
		if(isset($clientUuid) && !empty($clientUuid))
		{
			//$clientUuid = 'clie_04229954-3f4d-5a42-91ca-0c632e65879a';
			$contractData = $this->api->getContract($clientUuid);
				if($contractData['status'] && isset($contractData['resultSet']['uuid']))
				{
					$success['contract'] = $contractData['resultSet']['uuid'];
				}else
				{
					$success['contract'] = "";
				}				
		}
		
		//pr($success); 
		//pr($errors); 
		if(isset($errors) && !empty($errors))
		{
			$response = array(
			'status' => 'failed',
			'errors' => $errors,
			);
			echo json_encode($response);
		}
		else
		{
			$id = $this->createIncdient($success);
			$response = array(
			'status' => 'success',
			'ticket_id' => $id,
			);
			echo json_encode($response);
		}
		
	}
	
	
	function createIncdient($response = array())
	{
		//pr($response);
		$ticket_data = array(
				 'patner_id' => $response['partner_id'],
				 'subject' => $response['subject'], 
				 'description' => $response['desc'],
				 'queue_id' => 1,
				 'owner_id' =>  $response['client_id'], //Client_id 
				 'requestor' => $response['requestor'], 
				 'device_id' => $response['device'], 
				 'service_id' => '',// from eventedge_services table #change 
				 'severity_id' => $response['eventCritical'], 
				 'skill_id' => isset($response['skill'])?$response['skill']:'', #change severity
				 'event_text' => $response['desc'], 
				 'client_loaction' => $response['client_location'], 
				 'status_code' => $response['status_code'], //state_code 
				 'priority' => $response['urgency'],
				 'created_by' => 1, //created by defult 1 System genrate ticket refering to user table
				 'created_on' => date('Y-m-d H:i:s'), 
				 'assigned_to' =>  $response['assign_to'], 
				 'due_date' => date("Y-m-d H:i:s"),				 
				 'estimated_time' => date("Y-m-d H:i:s"),				 
				//'due_date' => date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))+$event['sla']), // Get due date hours from skill_set table (created on + sla hours) insert date
				// 'estimated_time' =>  date("Y-m-d H:i:s", strtotime($event['sla']. "sec")), // Get due date hours from skill_set table (created on + sla hours) convert seconds to hours 
				 'client_id' => $response['client_id'], 
				 'client_uuid' => $response['client_uuid'], 
				 'event_id' => '', //event_id is not applicable for manual incident
				 'ticket_type_id' => 1, //ticket type id by defult it should be incident
				 'ticket_cate_id' => isset($response['cate_id'])?$response['cate_id']:0, 
				 'ticket_sub_cate_id' => isset($response['sub_cate_id'])?$response['sub_cate_id']:0, 
				 'ticket_external_ref' => $response['external_ref'], 
				 'vendor_ref' => '', 
				 'contract_type' => $response['contract'], 
				);
				
				$save_ticket = $this->inc->save_ticket($ticket_data);
				$incident_id = "INC-".str_pad($save_ticket, 6, '0', STR_PAD_LEFT);
					/* Get SLA, MTRS, MTTR starts */
					    $requested_date = strtotime(date("Y-m-d H:i:s"));
					   // $client_id = $clientId;
					    $client_id = $response['partner_id'];
					    $skill_id =  isset($response['skill'])?$response['skill']:'';
						$url = base_url("webapi/getIncidents/client_id/$client_id/severity_id/$skill_id/date/$requested_date");
						$sla_details = get_sla_details($url);
						$sla = ""; $mtrs = ""; $mttr = "";
						if(isset($skill_id,$sla_details['resultSet']) && !empty($sla_details['resultSet']))
						{
							$sla = $sla_details['resultSet']['slaDueTime'];
							$mtrs = $sla_details['resultSet']['mtrsDueTime'];
							$mttr = $sla_details['resultSet']['mttrDueTime'];
						}						
					/* Get SLA, MTRS, MTTR ends */
					$update_ticket_data = array(
											'incident_id'=> $incident_id,
											'status_code'=> $response['status_code'],
											'sla_due_time'=> $sla,
											'mtrs_due_time'=> $mtrs,
											'mttr_due_time'=> $mttr,
											); 				
				$update_ticket = $this->inc->updateTicket($update_ticket_data,array("ticket_id"=>$save_ticket));
				$insert_activity_log = array(
												"start_time" => date('Y-m-d H:i:s'),
												"notes" => "Manual tickets",
												"ticket_id" => $save_ticket,
												"user_id" => 1,
												"state_status" => $response['status_code'], 
											);
				$insert_ticket_log = $this->inc->save_ticket_activity($insert_activity_log);
				return $incident_id;
				
	}
		

}
?>