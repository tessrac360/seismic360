    <script src="<?php echo base_url() . "public/" ?>js/form/form_dashboardevent.js" type="text/javascript"></script>
<?php
if ($getMtrs) {
    $hours_mtrs = round($getMtrs[0]['mtrs'] / (60 * 60), 2);
}



?>
<style>
    a:hover{text-decoration: none;}
    .remove-pad-right{padding-right:0;}
    .remove-pad-left{padding-left: 0;}
    .dashboard-stat2{padding: 6px 9px 5px;}
    .dashboard-stat2 .display .number small{font-size: 14px;font-weight: 800;text-transform: capitalize;}
    .dashboard-stat2{margin-bottom: 5px; padding:7px 9px 5.5px;}
    .dashboard-stat2 .display{margin-bottom: 4.5px;}
    .dashboard-stat{margin-bottom: 5px;}
    .amcharts-title{color:#000; fill:black;}

    .dashboard-stat.dashboard-stat-v2 .visual {
        padding-top: 0px;
        margin-bottom: 0;
    }
    .dashboard-stat .details{padding-right: 0;}
    .dashboard-stat .details .number{font-size: 22px;padding-top:0px;}
    .dashboard-stat .details .desc{font-size: 13px;}
    .dashboard-stat.yellow-gold {background-color: #E87E04;}
    .dashboard-stat.blue-cham .details .number, .dashboard-stat.blue-cham .details .desc{color: #fff;}    
    .dashboard-stat.blue-cham .visual>i { opacity: .1; filter: alpha(opacity=10);}    
    .dashboard-stat.blue-cham {background-color: #2C3E50;}
    .dashboard-stat .visual{height: 63px;}
    .dashboard-stat .details{position: relative;}
    .dashboard-stat .details .desc{position: absolute; left: 28px; font-weight:600;}
    .dashboard-stat.green{background-color:#24a286;}

    .dashboard-stat2 hr{margin: 6px 0 0 0;}
    .dashboard-stat2 h3 { margin-top: 0px; margin-bottom: 2px; font-size:31px; font-weight:600;}
    .dashboard-stat2 .progress-info .status{text-transform: capitalize;}
    .dashboard-stat2 .display.green small{color:#2ab4c0;}
    .dashboard-stat2 .display.red  small{color:#f36a5a;}
    .dashboard-stat2 .display.blue  small{color:#5c9bd1;}
    .dashboard-stat2 .display.green h3{color:#2ab4c0 !important;position: relative; z-index: 2; }
    .dashboard-stat2 .display.red  h3{color:#f36a5a !important; position: relative; z-index: 2; }
    .dashboard-stat2 .display.blue  h3{color:#5c9bd1 !important;position: relative; z-index: 2; }
    .mttr_blocks .dashboard-stat2{overflow:hidden; position:relative;    display: block;}
    .mttr_blocks .dashboard-stat2 i{position: absolute; font-size: 100px !important;     color: rgba(0, 0, 0, 0.05) !important; top: 7px; right: -16px;}
    .table-scrollable{margin:0 !important;}
    .events_Live_status .table>tbody>tr>td{padding: 7.9px 6px 9px 6px;}
    .events_Live_status .table>tbody>tr>td a{font-weight:600;}
    .events_Live_status .table>thead>tr>th{font-weight:600;}


    .events_Live_status .table-advance td.highlight div.Critical { border-left: 2px solid #e7505a; }
    .events_Live_status .table-advance td.highlight div.Major { border-left: 2px solid #8e44ad; }
    .events_Live_status .table-advance td.highlight div.Warning { border-left: 2px solid #e87e04; }
    .events_Live_status .table-advance td.highlight div.Minor { border-left: 2px solid #24a286; }
    .events_Live_status .table-advance td.highlight div.Unknown { border-left: 2px solid #3598dc; }

    .span-padd{padding:0px 4px; color:#fff; min-width:30px; text-align:center; display: inline-block; position: relative; top: 1px;}
    .critical-span-bg{background:#e7505a;}
    .major-span-bg{background:#8e44ad;}
    .warning-span-bg{background:#e87e04;}
    .minor-span-bg{background:#24a286;}
    .unknown-span-bg{background:#3598dc;}

    .table-advance div.Critical, .table-advance div.Major, .table-advance div.Warning, .table-advance div.Minor, .table-advance div.Unknown {
        position: absolute; margin-top: -5px; float: left; width: 2px; height: 30px; margin-right: 20px!important; }
    .portlet { margin-bottom: 6px;}
    .portlet.light>.portlet-title { min-height: 33px; margin-bottom: 0;}

    .dashboard-stat .visual { height: 69px;}
    .dashboard-stat .details .desc{bottom:auto;}
    .portlet.light>.portlet-title>.caption>.caption-subject {
        font-size: 14px;
        text-transform: capitalize !important;
        font-weight: 600 !important;
    }

    .portlet.light>.portlet-title>.nav-tabs>li>a { padding: 0 13px 9px;}
    .tab-content{height:auto !important;}
    .portlet.light { padding: 5px 10px 10px 10px;}

    #mttr .amcharts-legend-marker, #mtrs .amcharts-legend-marker{d: path('M -4.5 4.5 L 4.5 4.5 L 4.5 -4.5 L -4.5 -4.5 Z');}

    .page-title {
        background: rgba(204, 204, 204, 0.22);
        padding: 6px 9px;
        margin: 0 0 10px;
    }.page-title{font-size: 14px; font-weight: 600 !important; letter-spacing:-0.5px !important ;}	
    .table-advance tr td.highlight:first-child a {margin-left: 6px;}
    .remove-pad-right .portlet.light>.portlet-title>.actions, .remove-pad-left .portlet.light>.portlet-title>.actions{display:none;}
</style>
<div class="full-height-content full-height-content-scrollable">
    <h1 class="page-title">Dashboard</h1>
    <div style="position: absolute; top: 8px; right:8px;font-size: 12px;">
        <div class="page-toolbar form-inline" style="padding-top:0;">
            Period : 
            <select class="form-control" style="height: 23px; padding: 0; font-size: 11px;">
                <option>MTD</option>
				<option>YTD</option>
            </select>
        </div>
    </div>
    <div class="full-height-content-body">
        <div class="row">

            <div class="col-md-10 parent remove-pad-right">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 mttr_blocks ">
                        <div class="dashboard-stat2">
                            <div class="display green">
                                <div class="number">
                                    <small>MTTR</small>
                                </div>
                                <div class="icon">
                                    <h3 class="font-green-sharp">
                                        <span><?php echo '1.82';
//                                            $secondsInAMinute = 60;
//                                            $secondsInAnHour = 60 * $secondsInAMinute;
//                                            $secondsInADay = 24 * $secondsInAnHour;
//// extract days								
//											ini_set("precision", 12);
//                                            $avgmt = round($getMttr[0]['mttr'] / $secondsInADay, 2);
//                                            
//											//print_r($getMttr);
//                                            echo $avgmt;
                                            ?></span>
                                    </h3>
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="progress-info">
                                <hr />
                                <div class="status">
                                    <div class="status-title"> Target </div>
                                    <div class="status-number"> 2 Days </div>
                                </div>
                            </div>
                        </div>


                        <div class="dashboard-stat2 ">
                            <div class="display red">
                                <div class="number">
                                    <small>MTRS</small>
                                </div>
                                <div class="icon">
                                    <h3 class="font-green-sharp">
                                        <span><?php echo '0.97'
//                                            $secondsInAMinute = 60;
//                                            $secondsInAnHour = 60 * $secondsInAMinute;
//                                            $secondsInADay = 24 * $secondsInAnHour;
//                                            // extract days
//                                            $avgmt = round($getMtrs[0]['mtrs'] / $secondsInAnHour, 2);
//                                            echo $avgmt;
                                            ?></span>
                                    </h3>
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="progress-info">
                                <hr />
                                <div class="status">
                                    <div class="status-title"> Target </div>
                                    <div class="status-number"> 1 Hr </div>
                                </div>
                            </div>
                        </div>

                        <div class="dashboard-stat2 ">
                            <div class="display blue">
                                <div class="number">
                                    <small class="">SLA</small>
                                </div>
                                <div class="icon">
                                    <h3 class="font-green-sharp">
                                        <span><?php echo '98' //echo $getSla; ?>%</span>
                                    </h3>
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="progress-info">
                                <hr />
                                <div class="status">
                                    <div class="status-title"> Target </div>
                                    <div class="status-number"> 95% </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-9 col-md-6 col-xs-12 col-sm-12 remove-pad-left">
                        <div class="row">
                            <div class="col-md-6 remove-pad-left remove-pad-right">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject bold uppercase font-dark">Open Cases: Live</span>														
                                        </div>
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                                <i class="icon-trash"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body events_Live_status">
                                        <div class="table-scrollable">
                                            <table class="Open-Events-Status-Live table table-striped table-bordered table-advance table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <i class="fa fa-bell" aria-hidden="true"></i> Incidents </th>
                                                        <th class="text-center">
                                                            <i class="fa fa-check-circle" aria-hidden="true"></i> Count </th>
                                                        <th class="text-center">
                                                            <i class="fa fa-clock-o" aria-hidden="true"></i> Age <small>(hh:mm)</small> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 remove-pad-right">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject bold uppercase font-dark">Breached Cases: Live</span>														
                                        </div>
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                                <i class="icon-trash"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body  events_Live_status">
                                        <div class="table-scrollable">
                                            <table class="breached-events-live table table-striped table-bordered table-advance table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <i class="fa fa-bell" aria-hidden="true"></i> Incidents </th>
                                                        <th class="text-center">
                                                            <i class="fa fa-check-circle" aria-hidden="true"></i> Count </th>
                                                        <th class="text-center">
                                                            <i class="fa fa-clock-o" aria-hidden="true"></i> Age <small>(hh:mm)</small> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12col-xs-12 col-sm-12">
                        <div class="portlet light" style="padding-top: 5px;">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase font-dark">Events Trend <span style="display: none" id="chartMothlyReportHour">/ Hour</span></span>
                                </div>
                                <div class="actions" id="chartMothlyReportdiv" style="display: none">
                                    <a class="btn btn-circle btn-icon-only btn-default" id="chartMothlyReportclosed" href="#">
                                        <i class="fa fa-times "></i>  
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body  events_Live_status" style="position:relative;">
                                <div class="row">
                                    <div id="chartMothlyReport" style="height:195px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <div class="col-md-2 parent remove-pad-left">
                <div class="row">
                    <div class="col-md-12 remove-pad-left">
                        <a class="dashboard-stat dashboard-stat-v2 red" style="background:#333366;" href="<?php echo base_url() . "/event/ticket?status_id=0" ?>">
                            <div class="visual">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number"> 
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets['New'])) ? $getTickets['New'] : "0" ?>"><?php echo (isset($getTickets['New'])) ? $getTickets['New'] : "0" ?></span></div>
                                <div class="desc"> New </div>
                            </div>
                        </a>					
                        <a class="dashboard-stat dashboard-stat-v2 blue" href="<?php echo base_url() . "/event/ticket?status_id=7" ?>">
                            <div class="visual">
                                <i class="fa fa-bar-chart-o"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="
                                          <?php echo (isset($getTickets['Closed'])) ? $getTickets['Closed'] : "0" ?>"><?php echo (isset($getTickets['Closed'])) ? $getTickets['Closed'] : "0" ?></span></div>
                                     <div class="desc"> Closed </div>
                            </div>
                        </a>

                        <a class="dashboard-stat dashboard-stat-v2 yellow-gold" href="<?php echo base_url() . "/event/ticket?status_id=2" ?>">
                            <div class="visual">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo "3";// (isset($getTickets['Inprogress'])) ? $getTickets['Inprogress'] : "0" ?>"><?php echo (isset($getTickets['Inprogress'])) ? $getTickets['Inprogress'] : "0" ?></span></div>
                                <div class="desc"> In-Progress </div>
                            </div>
                        </a>

                        <a class="dashboard-stat dashboard-stat-v2 red" href="<?php echo base_url() . "/event/ticket?status_id=3" ?>">
                            <div class="visual">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number"> 
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets['Pending Customer'])) ? $getTickets['Pending Customer'] : "0" ?>"><?php echo (isset($getTickets['Pending Customer'])) ? $getTickets['Pending Customer'] : "0" ?></span></div>
                                <div class="desc"> Pending Customer </div>
                            </div>
                        </a>

                        <a class="dashboard-stat dashboard-stat-v2 purple" href="<?php echo base_url() . "/event/ticket?status_id=8" ?>">
                            <div class="visual">
                                <i class="fa fa-bar-chart-o"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets['Pending Vendor'])) ? $getTickets['Pending Vendor'] : "0" ?>"><?php echo (isset($getTickets['Pending Vendor'])) ? $getTickets['Pending Vendor'] : "0" ?></span>
                                </div>
                                <div class="desc"> Pending Vendor </div>
                            </div>
                        </a>

                        <a class="dashboard-stat dashboard-stat-v2 green" href="<?php echo base_url() . "/event/ticket?status_id=9" ?>">
                            <div class="visual">
                                <i class="fa fa-download" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets['Pending Work Order'])) ? $getTickets['Pending Work Order'] : "0" ?>"><?php echo (isset($getTickets['Pending Work Order'])) ? $getTickets['Pending Work Order'] : "0" ?></span>
                                </div>
                                <div class="desc"> Pending Work Order </div>
                            </div>
                        </a>

                        <a class="dashboard-stat dashboard-stat-v2 yellow-gold" style="background:#353535;" href="<?php echo base_url() . "/event/ticket?status_id=5" ?>">
                            <div class="visual">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup" data-value="<?php echo (isset($getTickets['Resolved'])) ? $getTickets['Resolved'] : "0" ?>"><?php echo (isset($getTickets['Resolved'])) ? $getTickets['Resolved'] : "0" ?></span></div>
                                <div class="desc"> Resolved </div>
                            </div>
                        </a>


                    </div>
                </div>
            </div>

            <div class="right_block col-md-3" style="display:none;">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">Revenue</span>
                        </div>

                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-cloud-upload"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-wrench"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-trash"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="close fa fa-close"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

