<?php

//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends MX_Controller {

    var $cData; /* created user details (Array type) */
    var $uData, $user_type;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();
        $current_date = $this->config->item('datetime');
        $current_user = $this->session->userdata('id');
        $this->getCompanyId = helpler_getCompanyId();
        $this->accessLevelClient = helper_getAccessLevelClient();
        $this->user_type = $this->session->userdata('user_type');
        $this->cData = array('created_on' => $current_date, 'created_by' => $current_user, 'company_id' => $this->getCompanyId);
        $this->uData = array('updated_on' => $current_date, 'updated_by' => $current_user, 'company_id' => $this->getCompanyId);
        $this->client_id = $this->session->userdata('client_id');
        $this->load->library('form_validation');
        $this->load->model("Model_dashboard");
    }

    public function index121() {
        $data['getTickets'] = $this->Model_dashboard->getTicketsCount();
        $data['getAlarms'] = $this->Model_dashboard->getTotalAlarms();
        //$data['getBreached'] = $this->Model_dashboard->getBreached();
        $data['getSla'] = $this->Model_dashboard->getSla();
        $data['getMtrs'] = $this->Model_dashboard->getMtrs();
        // 
        $data['getMttr'] = $this->Model_dashboard->getMttr(); //pr($data); die;
        // $data['getDretched'] = $this->Model_dashboard->getDretched();
        //pr($data);exit;
        $data['file'] = 'event';
        $this->load->view('template/front_template', $data);
    }

    public function ajax_chartDaywise_event() {
        $datetime = $_POST['datetime']; //exit;        
        $resultSet = $this->Model_dashboard->getChartCurrentDate($datetime);
        echo $json = json_encode($resultSet);
        die();
    }

    public function ajax_chartMonthwise_event() {
        $resultSet = $this->Model_dashboard->getChartCurrentMonth();
        echo $json = json_encode($resultSet);
        die();
    }

    public function ajax_acknowledgmentStatus_event() {
        $resultSet = $this->Model_dashboard->getChartAcknowledgmentStatus();
        echo $json = json_encode($resultSet);
        die();
    }

    public function ajax_open_events_status_live_event() {
        $data['getEvents'] = $this->Model_dashboard->getOpenEventsStatusLive();
        $data['getTasks'] = $this->Model_dashboard->getTasksCount();
        //pr($data['getEvents']);exit;
        $this->load->view('events/open_events_status_live', $data);
    }

    public function ajax_breached_events_live_event() {
        $data['getBreached'] = $this->Model_dashboard->getBreachedEventsLive();
        $data['getTasks'] = $this->Model_dashboard->getTasksCount();
        $this->load->view('events/breached_events_live', $data);
    }

    public function chart() {
        echo $this->Model_dashboard->countAgeing();
    }

    public function timeCalculate() {
        $beginday = date("Y-m-01");
        $lastday = date("2018-11-30");
        $nr_work_days = $this->getWorkingDays($beginday, $lastday);
        pr($nr_work_days);
    }

    function getWorkingDays($startDate, $endDate) {
        $begin = strtotime($startDate);
        $end = strtotime($endDate);
        if ($begin > $end) {
            echo "startdate is in the future! <br />";

            return 0;
        } else {
            $no_days = 0;
            $weekends = 0;
            $excludeDate = array();
            while ($begin <= $end) {
                $no_days++; // no of days in the given interval
                $what_day = date("N", $begin);
                if ($what_day <= 5) { // 6 and 7 are weekend days
                    $excludeDate[] = date('Y-m-d', $begin) . '<br>';
                    $weekends++;
                };

                $begin += 86400; // +1 day
            };

            rsort($excludeDate);

            return $excludeDate[3];
        }
    }

    /* Start Incident dashboard module */

    public function incident() {
           $data['file'] = 'incident';
        //$data['MTTRSeverity4'] = $this->Model_dashboard->MTTRSeverityMACD(4);
//        $data['MTTRSeverity1'] = $this->Model_dashboard->MTTRSeverity(1);
//        $data['MTTRSeverity2'] = $this->Model_dashboard->MTTRSeverity(2);
//        $data['MTTRSeverity3'] = $this->Model_dashboard->MTTRSeverity(3);
//        $data['MTTRSeverity4'] = $this->Model_dashboard->MTTRSeverityMACD(4);
//        $data['MTRSSeverity1'] = $this->Model_dashboard->MTRSSeverity(1);
//        $data['MTRSSeverity2'] = $this->Model_dashboard->MTRSSeverity(2);
//        $data['MTRSSeverity3'] = $this->Model_dashboard->MTRSSeverity(3);
//        $data['MTRSSeverity4'] = $this->Model_dashboard->MTRSSeverityMACD(4);
//        $data['getTickets'] = $this->Model_dashboard->getTicketsCount();
//        $data['getMTTR'] = $this->Model_dashboard->getMTTRincident();
//        $data['getMTRS'] = $this->Model_dashboard->getMTRSincident();
//        $data['getSLA'] = $this->Model_dashboard->getSLAincident();
//        $data['getAlarmtypes'] = $this->Model_dashboard->getAlartypeCount();

        $data['incidentOpenLive'] = $this->Model_dashboard->IncidentOpenLive();
        $data['MacdOpenLive'] = $this->Model_dashboard->MacdOpenLive();
        
        
        $data['incidentBreachedLive'] = $this->Model_dashboard->IncidentBreachedLive();
        $data['MacdBreachedLive'] = $this->Model_dashboard->MacdBreachedLive();
         //pr($data['getEvents']);exit;
        $data['getTickets'] = $this->Model_dashboard->getTicketsCount();
        // $data['getTotalClients'] = $this->Model_dashboard->getTotalClients();

		$data['gettop5priority'] = $this->Model_dashboard->getTop5priority();

		//pr($data['gettop5priority']);exit;
        $data['countAgeingBucket'] = $this->Model_dashboard->getAgeBucket();
        $data['countLastUpdatedBucket'] = $this->Model_dashboard->getLastUpdatedBucket();

        //pr($data);exit;
        $this->load->view('template/front_template', $data);
    }

    public function ajax_totalCountIncident() {
        $getTickets = $this->Model_dashboard->getTicketsCount();        
        if (!empty($getTickets)) {            
           echo $html = '<a class="dashboard-stat dashboard-stat-v2 red" style="background:#5e5eab;" href="'.base_url() .'event/ticket?status_id='. base64_encode(0) .'">
                            <div class="visual">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            </div>
                            <div class="details">
                                <div class="number"> 
                                    <span data-counter="counterup" data-value="'.(isset($getTickets[0])) ? $getTickets[0] : "0" .'">'. (isset($getTickets[0])) ? $getTickets[0] : "0" .'</span></div>
                                <div class="desc"> New </div>
                            </div>
                        </a>';
        }
        exit;
    }

    public function ajax_ageBucket() {
        $data['countAgeingBucket'] = $this->Model_dashboard->getAgeBucket();
    }

    public function ajax_LastUpdatedBucket() {
        $data['countLastUpdatedBucket'] = $this->Model_dashboard->getLastUpdatedBucket();
    }
    
    
    

    /* End Incident dashboard module */
}
