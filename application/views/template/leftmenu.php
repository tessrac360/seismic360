<?php
$menuArr = helper_MasterMenu();
//pr($menuArr);
$routing = & load_class('Router');
$class = $routing->fetch_class();
$method = $routing->fetch_method();
$webLink = $class . '/' . $method;
$level = helper_getDashBoardStatus($_SESSION['role_id']);
//pr($menuArr);
?>
<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-compact page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
    <?php
    if ($menuArr['status'] == 'true') {
        foreach ($menuArr['resultSet'] as $topLevelMenu) {
            //pr($topLevelMenu);
            ?>
            <li class="nav-item" >
                <?php
                if ($topLevelMenu->title == "Dashboard") {                    
                        ?>
                        <a href="<?php echo base_url($level); ?>" class="nav-link nav-toggle">
                            <i class="<?php echo $topLevelMenu->icons; ?>"></i>
                            <span class="title">Dashboard</span>
                        </a>
                   
                    <?php
                } else {
                    ?>




                    <a href="<?php echo ($topLevelMenu->is_linkshow == 'Y') ? base_url($topLevelMenu->link) : 'javascript:void(0);' ?>" class="nav-link nav-toggle">
                        <i class="<?php echo $topLevelMenu->icons; ?>"></i>
                        <span class="title"><?php echo $topLevelMenu->title; ?></span>
                        <span class="arrow"></span>

                        <?php if ($webLink == $topLevelMenu->full_link) { ?>
                            <span class="selected"></span>
                            <span class="arrow"></span>
                        <?php } ?>
                    </a>
                <?php } ?>
                <?php if (!empty($topLevelMenu->menu_one_level)) { ?>
                    <ul class="sub-menu">
                        <?php
                        foreach ($topLevelMenu->menu_one_level as $menulevelone) {
                            // pr($menulevelone);
                            ?>
                            <li class="nav-item">
                                <a href="<?php echo ($menulevelone->is_linkshow == 'Y') ? base_url($menulevelone->link) : 'javascript:void(0);' ?>" class="nav-link nav-toggle">
                                    <i class="<?php echo $menulevelone->icons; ?>"></i>
                                    <span class="title"><?php echo $menulevelone->title; ?></span>
                                    <?php if ($webLink == $menulevelone->full_link) { ?>
                                        <span class="selected"></span> 
                                    <?php } ?>
                                    <?php if (!empty($menulevelone->menu_second_level)) { ?> 
                                        <span class="arrow"></span>
                                    <?php } ?>
                                </a>
                                <?php if (!empty($menulevelone->menu_second_level)) { ?>
                                    <ul class="sub-menu">
                                        <?php foreach ($menulevelone->menu_second_level as $menuleveltwo) { ?>

                                            <li class="nav-item">
                                                <a href="<?php echo ($menuleveltwo->is_linkshow == 'Y') ? base_url($menuleveltwo->link) : 'javascript:void(0);' ?>" class="nav-link" > <i class="<?php echo $menuleveltwo->icons; ?>"></i> <?php echo $menuleveltwo->title; ?>  </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </li>
            <?php
        }
    }
    ?>

</ul>