<div class="container-fluid">
    <div>
        <div class="page-footer-inner pull-right"> Copyright &copy; 2017-<?php echo date('Y')?> Seismic LLC. All rights reserved.
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
    </div>
</div>




