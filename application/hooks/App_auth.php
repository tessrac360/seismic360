<?php

class App_auth {

    private $CI;

    public function index() {
        $CI = &get_instance();
        $RequireLogin = array();
        $permissions = array();
        $baseURL = $GLOBALS['CFG']->config['base_url'];
        $RequireLogin['webapi']['client'] = true;
        $RequireLogin['webapi']['masterkey'] = true;
        $RequireLogin['webapi']['getSLAtime'] = true;
        $RequireLogin['webapi']['getIncidents'] = true;
        $RequireLogin['auth']['login'] = true;
        $RequireLogin['auth']['do_checkmail'] = true;
        $RequireLogin['auth']['sessionout'] = true;
        $RequireLogin['auth']['do_login'] = true;
        $RequireLogin['auth']['forgot_password'] = true;
        $RequireLogin['auth']['reset_password'] = true;
        $RequireLogin['auth']['do_resetPassword'] = true;
        $RequireLogin['auto_tickets']['generate_tickets'] = true;
        $RequireLogin['auto_tickets']['macd_manual'] = true;
        $RequireLogin['cron_job']['auto_assign_incidents'] = true;
        $RequireLogin['cron_job']['cronSuppressedActive'] = true;
        $RequireLogin['api']['index'] = true;
        $RequireLogin['macd']['client'] = true;
        $RequireLogin['macd']['client_title'] = true;
        $RequireLogin['macd']['client_devices'] = true;
        $RequireLogin['macd']['client_data'] = true;
        $RequireLogin['macd']['user_login'] = true;
        $RequireLogin['macd']['getMacdDetailsOfUser'] = true;
        $RequireLogin['macd']['bringCounts'] = true;
        $RequireLogin['macd']['updateIndts'] = true;
        $RequireLogin['macd']['updateMacd'] = true;
        $RequireLogin['macd']['getMacdDetailsById'] = true;
        $RequireLogin['macd']['updateIndtsSingle'] = true;
        $RequireLogin['macd']['updateMacdSingle'] = true;
        $RequireLogin['macd']['macdActivity'] = true;

        $RequireLogin['webapi']['test'] = true;
        $RequireLogin['webapi']['createMacd'] = true;
        $RequireLogin['webapi']['macdStatusChange'] = true;
        $RequireLogin['webapi']['createTicket'] = true;
        $RequireLogin['webapi']['assignEngineer'] = true;
        $RequireLogin['webapi']['updateStatasForTask'] = true;
        $RequireLogin['webapi']['updateStatasForTaskResolved'] = true;
        $RequireLogin['webapi']['updateStatasForTaskClosed'] = true;
        $routing = & load_class('Router');
        $class = $routing->fetch_class();
        $method = $routing->fetch_method();
        if (!empty($RequireLogin[$class][$method]) && ($class != 'macd')) {
            if (isset($_SESSION['id']) && !empty($RequireLogin[$class][$method])) {
                redirect(site_url('dashboard/incident'));
            }
            return true;
        } else if (!empty($RequireLogin[$class][$method]) && ($class == 'macd')) {
            /* if (isset($_SESSION['id']) && !empty($RequireLogin[$class][$method])) {
              redirect(site_url('dashboard'));
              } */
            return true;
        } else {
            if (!$_SESSION['id']) {
                redirect(site_url('login'));
                exit;
            }
        }
    }

    /* public function menuTab() {
      $CI = &get_instance();
      $RequireLogin = array();
      $permissions = array();
      $baseURL = $GLOBALS['CFG']->config['base_url'];
      //$RequireLogin['users']['dashboard'] = 'Dash Boards';
      //$RequireLogin['users']['index'] = 'Users';
      //$RequireLogin['roles']['index'] = 'Role';
      // $RequireLogin['client']['index'] = 'Client';
      //$RequireLogin['event']['index'] = 'Event';
      $routing = & load_class('Router');
      $class = $routing->fetch_class();
      $method = $routing->fetch_method();
      if (!empty($_SESSION['id'])) {
      if (!empty($RequireLogin[$class][$method])) {
      $MenuName = $RequireLogin[$class][$method];
      $url = $class . '/' . $method;
      $value = $this->insertMenuTemp($url, $MenuName);
      if ($value == 'EXCEED') {
      ?>
      <script type="text/javascript">
      alert('Please Remove some menu');
      </script>
      <?php

      }
      }
      }
      } */

    public function menuTabOLD() {
        $CI = &get_instance();
        $RequireLogin = array();
        $permissions = array();
        $baseURL = $GLOBALS['CFG']->config['base_url'];
        $RequireLogin['users']['dashboard'] = true;
        $RequireLogin['users']['index'] = true;
        $RequireLogin['roles']['index'] = true;
        $routing = & load_class('Router');
        $class = $routing->fetch_class();
        $method = $routing->fetch_method();

        if (!empty($RequireLogin[$class][$method])) {
            $url = $class . '/' . $method;
            $value = $this->insertMenuTemp($url);
            if ($value == 'EXCEED') {
                ?>
                <script type="text/javascript">
                    alert('Please Remove some menu');
                </script>  
                <?php

            }
        }
    }

    private function insertMenuTemp($url, $MenuName) {
        $CI = &get_instance();
        $CI->load->model('Menus');
        return $CI->Menus->InsertMenus($url, $MenuName);
    }

//    private function isloginCheck() {
//        $CI = &get_instance();
//        $CI->load->model('Users/Model_users');
//        return $CI->Model_users->getSuperAdminId_helper();
//    }
}
