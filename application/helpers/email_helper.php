<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function helperEmailContent($slugName = '', $pclientId = "") {
    $ci = get_instance();
    $ci->load->model('settings/Email_temp'); //echo "hiii";exit;
    return $ci->Email_temp->getEmailContent($slugName, $pclientId);
}

function helperReplayEmailID($slugName = '', $clientId = "") {
    $ci = get_instance();
    $ci->load->model('settings/Email_temp');
    return $ci->Email_temp->getReplayEmailID($slugName, $clientId);
}

//function headerhtml() {
//    $html = '<html lang="en">
//    <head>
//        <meta charset="utf-8">
//        <meta http-equiv="X-UA-Compatible" content="IE=edge">
//        <meta name="viewport" content="width=device-width, initial-scale=1">
//        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
//        <title>Eventedge</title>
//    </head>
//    <body style="font-family:Arial;">
//        <table width="700" cellpadding="0" cellspacing="0" align="center" style="margin-top:20px;margin-bottom:20px">
//            <thead>
//                <tr style="color: #333;background-color: #f5f5f5;">
//                    <td style="text-align:left;border:1px solid #d7d7d7;border-top-left-radius:10px;border-top-right-radius:10px;">
//                        <a href="#"><img src="' . base_url() . 'public/images/logo.png" border="0" alt="Eventegde" style="width:150px;padding: 12px 0 10px 30px;"></a>
//                        <a href="#"><img src="' . base_url() . 'public/images/li.png"  style="float:right;padding:15px 30px 0px 10px;"border="0" alt="li"></a>
//                        <a href="#"><img src="' . base_url() . 'public/images/tw.png"  style="float:right;padding:15px 0px 0px 0px;"border="0" alt="tw"></a>						
//                        <a href="#"><img src="' . base_url() . 'public/images/fb.png"  style="float:right;padding:15px 10px 0px 10px;"border="0" alt="fb"></a>
//                    </td>
//                </tr>
//            </thead>
//            <tbody>
//                <tr>
//                    <td style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7">
//                        <table style="border:1px solid #d7d7d7;margin:30px;" width="640" cellpadding="0" cellspacing="0" align="center">
//                            <tbody>
//                                <tr>
//                                    <td>';
//    return $html;
//}
//
//function footerhtml() {
//    $html = '</td>
//                                </tr>
//                            </tbody>
//                        </tab   le>		
//                    </td>
//                </tr>
//                <tr>
//                    <td style="border:1px solid #d7d7d7;background-color:#efefef;padding:15px;border-bottom-left-radius:10px;border-bottom-right-radius:10px;">
//                        <table width="630" border="0" cellpadding="0" cellspacing="0">
//                            <tbody>
//                                <tr>
//                                    <td style="font-size:11px;line-height:16px;font-weight:normal;text-align:center;">
//                                        Note: Do not reply to this email. Contact us with any queries by visiting our website at:
//                                        <a href="#" style="font-weight:bold;color:#245498;" target="">Eventedge</a>
//
//                                    </td>
//                                </tr>
//                            </tbody>
//                        </table>
//                    </td>
//                </tr>
//            </tbody>
//        </table>    
//    </body>
//</html>';
//    return $html;
//}


function headerhtml() {
    $html = '<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Seismic360</title>
    </head>
    <body style="font-family:Helvetica Neue, Helvetica Neue, Helvetica, Arial, sans-serif;">
        <table width="700" cellpadding="0" cellspacing="0" align="center" style="margin-top:20px;margin-bottom:20px">
            <thead>
                <tr style="color: #333;background-color: #17C4BB;">
                    <td style="text-align:left;border:1px solid #d7d7d7;">
                        <a href="#"><img src="http://sdk.eventedge.in/EventEdgeImage/logo-default.png" border="0" alt="Seismic360" style="width:150px;padding: 12px 0 10px 30px;"></a>
                        
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;padding: 15px; font-size:13px;">';
    return $html;
}

function footerhtml() {
    $html = '</td>
                </tr>
                <tr>
                    <td style="border:1px solid #d7d7d7;background-color:#17C4BB;padding:15px;">
                        <table width="630" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td style="font-size:11px;line-height:16px;font-weight:normal;text-align:center; color:#fff;">
                                        Note: Do not reply to this email. Contact us with any queries by visiting our website at:
                                        <a href="http://s360.seismic360.com" style="font-weight:bold;color:#fff;" target="">Seismic360</a>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>    
    
</body></html>';
    return $html;
}

function helperSendMail($to, $from, $subject, $message, $name) {
    $CI = & get_instance();
    $CI->load->library('email');
    if ($to != '' && $from != '') {
        $CI->email->set_newline("\r\n");
        $CI->email->from($from, $name);       
        $CI->email->reply_to($from);
        $CI->email->to($to);
        //$CI->email->cc('veeru89.k@gmail.com,venkataprasadbuddala@gmail.com');
        //$CI->email->bcc('another@another-example.com');
        $CI->email->subject($subject);
        $CI->email->message($message);
        $CI->email->send();
        return true;
    }
    return false;
}

function tempSubject($msg = "", $setVal = array()) {
    if (strstr($msg, "[MACD_ID]")) {
        $msg = str_replace("[MACD_ID]", $setVal->macd_id, $msg);
    }

    if (strstr($msg, "[TASK_ID]")) {
        $msg = str_replace("[TASK_ID]", $setVal->task_id, $msg);
    }
    
    if (strstr($msg, "[STATUS]")) {
        $msg = str_replace("[STATUS]", $setVal->status, $msg);
    }
    
    if (strstr($msg, "[NAME_OF_ENGINEER]")) {
        $msg = str_replace("[NAME_OF_ENGINEER]", $setVal->name, $msg);
    }
    
    return $msg;
}

function tempContent($msg = "", $setVal = array(), $LINK = "") {

    if (strstr($msg, "[NAME]")) {
        $msg = str_replace("[NAME]", $setVal->name, $msg);
    }

    if (strstr($msg, "[EMAIL]")) {
        $msg = str_replace("[EMAIL]", $setVal->email, $msg);
    }

    if (strstr($msg, "[PASSWORD]")) {
        $msg = str_replace("[PASSWORD]", $setVal->password, $msg);
    }

    if (strstr($msg, "[REQUESTER_NAME]")) {
        $msg = str_replace("[REQUESTER_NAME]", $setVal->requester_name, $msg);
    }

    if (strstr($msg, "[REQUEST_TYPE]")) {
        $msg = str_replace("[REQUEST_TYPE]", $setVal->request_type, $msg);
    }

    if (strstr($msg, "[MACD_ID]")) {
        $msg = str_replace("[MACD_ID]", $setVal->macd_id, $msg);
    }

    if (strstr($msg, "[TASK_ID]")) {
        $msg = str_replace("[TASK_ID]", $setVal->task_id, $msg);
    }

    if (strstr($msg, "[STATUS]")) {
        $msg = str_replace("[STATUS]", $setVal->status, $msg);
    }

    if (strstr($msg, "[LINK]")) {
        $msg = str_replace("[LINK]", $LINK, $msg);
    }

    $html = headerhtml();
    $html .= $msg;
    $html .= footerhtml();
    return $html;
}
