<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function helper_LogFetchRecord($tableName="", $key="", $value="") {
    $ci = get_instance();
    $ci->load->model('settings/Model_log');
    return $ci->Model_log->LogFetchRecord($tableName, $key, $value);
}

function helper_createLogActions($logTableName="", $clientId="", $actions="", $oldValue="", $newValue="") {
    $ci = get_instance();
    $ci->load->model('settings/models/Model_log');
    return $ci->Model_log->createLogActions($logTableName, $clientId, $actions, $oldValue, $newValue);
}



function helper_LogFetchRecordUser($tableName="", $key="", $value="") {
    $ci = get_instance();
    $ci->load->model('settings/Model_log');
    return $ci->Model_log->LogFetchRecordUser($tableName, $key, $value);
}
