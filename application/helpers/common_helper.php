<?php
/* Purpose  : file create for manage common functionality
  Author / created by  :  emp id 1523/ @ketan
  dated                :  27/04/2017
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
function HRToSec($hours_time){
  sscanf($hours_time, "%d:%d", $hours, $minutes);
  return  $hours * 3600 + $minutes * 60 ;
}

function secToHR($seconds) {
  $hours = floor($seconds / 3600);
  $minutes = floor(($seconds / 60) % 60); 
  return "$hours:$minutes";
}


function wordlimit($value, $noOfchar) {
    $wordwrap = wordwrap($value, $noOfchar, true);
    if (strlen($value) > $noOfchar) {
        $return = substr($value, 0, $noOfchar) . '....';     
    } else {
        $return = $wordwrap;
    }
    return $return;
}

function encode_url($string, $key = "", $url_safe = TRUE) {
    if ($key == null || $key == "") {
        $key = "tyz_mydefaulturlencryption";
    }
    $CI = & get_instance();
    $ret = $CI->encrypt->encode($string, $key);

    if ($url_safe) {
        $ret = strtr(
                $ret, array(
            '+' => '.',
            '=' => '-',
            '/' => '~'
                )
        );
    }

    return $ret;
}

function decode_url($string, $key = "") {
    if ($key == null || $key == "") {
        $key = "tyz_mydefaulturlencryption";
    }
    $CI = & get_instance();
    $string = strtr(
            $string, array(
        '.' => '+',
        '-' => '=',
        '~' => '/'
            )
    );

    return $CI->encrypt->decode($string, $key);
}

function pr($array) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function helper_getUserName() {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getUserName();
}

function helper_clientName() {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getClientName();
}

function helper_PermissionAccess($roleID, $moduleID) {
    $ci = get_instance();
    $ci->load->model('Roles/Module_roles');
    return $ci->Module_roles->fetchPermission($roleID, $moduleID);
}

function helper_PermissionAccessTemp($roleID, $moduleID) {
    $ci = get_instance();
    $ci->load->model('Roles/Module_temproles');
    return $ci->Module_temproles->fetchPermission($roleID, $moduleID);
}

function helper_getNoOfGroup() {
    $ci = get_instance();
    $ci->load->model('auth/Auth_model');
    return $ci->Auth_model->isGroupCount();
}

function helper_getUserDefaultGroup() {
    $ci = get_instance();
    $ci->load->model('auth/Auth_model');
    return $ci->Auth_model->getDefaultGroup();
}

function helper_getActiveClient() {
    $ci = get_instance();
    $ci->load->model('client/Model_client');
    return $ci->Model_client->getActiveClient();
}

function helper_getUserLocationAllocate() {
    $ci = get_instance();
    $ci->load->model('auth/Auth_model');
    return $ci->Auth_model->getGroupLocationAllocate();
}

function helper_isInArray($array, $key, $key_value) {
    $within_array = 'false';
    foreach ($array as $k => $v) {

        if (is_array($v)) {
            $within_array = helper_isInArray($v, $key, $key_value);
            if ($within_array == 'true') {
                break;
            }
        } else {
            if ($v == $key_value && $k == $key) {
                $within_array = 'true';
                break;
            }
        }
    }
    return $within_array;
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function helper_LocationTab() {
    $routing = & load_class('Router');
    $class = $routing->fetch_class();
    $method = $routing->fetch_method();
    $url = $class . '/' . $method;
    $returnSet = helper_getUserLocationAllocate();
    if ($returnSet['status'] == 'true')
        foreach ($returnSet['resultSet'] as $key => $value) {
            ?>
            <li class="<?php
            if ($url === $value['url']) {
                echo 'active';
            }
            ?>">
                <a href="<?php echo base_url($value['url']); ?>"> <?php echo $value['location'] ?> </a>
            </li>
            <?php
        }
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function helper_getDomain() {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getDomain();
}

function helper_getSkill() {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getSkill();
}

function helper_getAccessLevelClient() {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getAccessLevelClient();
}

function helper_getAccessOnlyClient() {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getAccessOnlyClient();
}


function helper_getAccessClientDropDown() {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getAccessClientDropDown();
}


function helper_getAccessPartnerClient() {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getAccessClientPartner();
}


/* Generate UUID Starts */
function generate_uuid($prefix = NULL)
{
	if(isset($prefix) && !empty($prefix)){
		$ci = get_instance();
		$ci->load->library('UUID');
		$key = $prefix.UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', generateRandomString());
	}else
	{
		$ci = get_instance();
		$ci->load->library('UUID');
		$key = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', generateRandomString());
	}
	
	return $key;
	
}

function generateRandomString($length = 32) {
		return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
	}
	
function createSecretKey($salt =""){
	
	// generating a unique key to be used for encryption
	if($salt == ""){
		
		$salt =  substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,41);
	}
	return sha1(mt_rand(10000,99999).time().$salt);
}

	
/* Generate UUID ENDs */
/* Get SLA, MTRS, MTTR  starts*/
function get_sla_details($url)
{
	$username = 'inspiredge';
	$password = 'inspiredge@123';
	$context = stream_context_create(array(
	'http' => array(
	'header' => "Authorization: Basic " . base64_encode("$username:$password")
	)
	));
	$data = file_get_contents($url, false, $context);
	$array = json_decode($data, true);
	return $array;
}
/* Get SLA, MTRS, MTTR  ends*/

//Key: Id or uuid
//value : userid or urseruuid
function helper_getUserInformation($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getUserInformation($key,$value);
}

function helper_getDashBoardStatus($roleId="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getDashBoardStatus($roleId);
}

function helper_getLevelOfUser($roleId="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_users');
    return $ci->Model_users->getLevelOfUser($roleId);
}

function helper_getClientsDetails($partnerId = "") {
   $ci = get_instance();
   $ci->load->model('Users/Model_users');
   return $ci->Model_users->getClientsDetails($partnerId);
}

## SIDDHU's CODE Don't touch it ;-) ##

function helper_getClientInformation($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_indivisualrecords','indivisual');
    return $ci->indivisual->getClientInformation($key,$value);
}

function helper_getClientLocationInfo($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_indivisualrecords','indivisual');
    return $ci->indivisual->getClientLocationInformation($key,$value);
}

function helper_getIncidentStausName($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_indivisualrecords','indivisual');
    return $ci->indivisual->getIncidentStausName($key,$value);
}

function helper_getdevicesInfo($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_indivisualrecords','indivisual');
    return $ci->indivisual->getdevicesInfo($key,$value);
}

function helper_getservicesInfo($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_indivisualrecords','indivisual');
    return $ci->indivisual->getservicesInfo($key,$value);
}

function helper_getDeviceCateInfo($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_indivisualrecords','indivisual');
    return $ci->indivisual->getDeviceCateInfo($key,$value);
}

function helper_getDeviceSubCateInfo($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_indivisualrecords','indivisual');
    return $ci->indivisual->getDeviceSubCateInfo($key,$value);
}

function helper_getUrgencyInfo($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_indivisualrecords','indivisual');
    return $ci->indivisual->getUrgencyInfo($key,$value);
}

function helper_getSeveritiesInfo($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_indivisualrecords','indivisual');
    return $ci->indivisual->getSeveritiesInfo($key,$value);
}

function helper_getSkillsInfo($key="",$value="") {
    $ci = get_instance();
    $ci->load->model('Users/Model_indivisualrecords','indivisual');
    return $ci->indivisual->getSkillsInfo($key,$value);
}
## SIDDHU's CODE Don't touch it ;-) END ##





