<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function event_hostName($host_name) {
    if ($host_name['status'] == 'true') {
        ?>
        <div class="chkdropdown filter-pos">
            <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
            <ul class="custom_filter">
                <?php foreach ($host_name['resultSet'] as $value) { ?>
                    <li>
                        <input type="checkbox" class="device_name" id="cdd1" name="device_name[]" value="<?php echo $value->device_id; ?>">
                        <label for="cdd1"><?php echo $value->device_name; ?></label>
                    </li>
                <?php } ?>


            </ul>
        </div>
        <?php
    }
}

function event_service($service) {
    if ($service['status'] == 'true') {
        ?>
        <div class="chkdropdown filter-pos">
            <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
            <ul class="custom_filter">
                <?php foreach ($service['resultSet'] as $value) { ?>
                    <li>
                        <input type="checkbox" class="service" id="cdd1" name="service[]" value="<?php echo $value->service_id; ?>">
                        <label for="cdd1"><?php echo $value->service_description; ?></label>
                    </li>
                <?php } ?>


            </ul>
        </div>
        <?php
    }
}

function event_severity($severity) {
    if ($severity['status'] == 'true') {
        ?>
        <div class="chkdropdown filter-pos">
            <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
            <ul class="custom_filter">
                <?php foreach ($severity['resultSet'] as $value) { ?>
                    <li>
                        <input class="severity" type="checkbox" id="cdd1" name="severity[]" value="<?php echo $value->id; ?>">
                        <label for="cdd1"><?php echo $value->severity; ?></label>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php
    }
}

function event_state($state) {
    if ($state['status'] == 'true') {
        ?>
        <div class="chkdropdown filter-pos">
            <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
            <ul class="custom_filter">
                <?php foreach ($state['resultSet'] as $value) { ?>
                    <li>
                        <input class="state" type="checkbox" id="cdd1" name="state[]" value="<?php echo $value->id; ?>">
                        <label for="cdd1"><?php echo $value->event_state; ?></label>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php
    }
}

function event_tools($sub_client) {
    //pr($sub_client);exit;
     if ($sub_client['status'] == 'true') {
        ?>
        <div class="chkdropdown filter-pos">
            <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
            <ul class="custom_filter">
                <?php foreach ($sub_client['resultSet'] as $value) {
                    ?>
                    <li>
                        <input type="checkbox"  class="tools" id="cdd1" name="tools[]" value="<?php echo $value->id; ?>">
                        <label for="cdd1"><?php echo $value->title; ?></label>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php
    }
    
}

function event_clients($sub_client) {
    //pr($sub_client);exit;
     if ($sub_client['status'] == 'true') {
        ?>
        <div class="chkdropdown filter-pos">
            <i class="fa fa-filter dropdown-toggle" data-toggle="dropdown"></i>
            <ul class="custom_filter">
                <?php foreach ($sub_client['resultSet'] as $value) { ?>
                    <li>
                        <input type="checkbox"  class="subClients" id="cdd1" name="subClients[]" value="<?php echo $value->id; ?>">
                        <label for="cdd1"><?php echo $value->client_title; ?></label>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php
    }
    
}



function childTreeEvent($treeArray = array()) {
    ?>
    <ul class="drop">
        <?php
        foreach ($treeArray as $value) {
            ?>
            <li><label><input type="checkbox" class="subClients subOption"  name="subClients[]" value="<?php echo $value['id']; ?>"> <?php echo $value['client_title'] ?></label>
                    <?php
                    if (!empty($value['children'])) {

                        echo childTreeEvent($value['children']);
                    }
                    ?>        
            </li>             
        <?php }
        ?>
    </ul>
    <?php
}



