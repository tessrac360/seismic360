<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menus extends CI_Model {

    private $getCompanyId = "";
    var $cData; /* created user details (Array type) */
    var $uData;  /* updated user details (Array type) */

    public function __construct() {
        parent::__construct();

        $CI = &get_instance();
        $this->getCompanyId = helpler_getCompanyId();
        $this->userId = $this->session->userdata('id');
        $this->db2 = $CI->load->database('eventedgesqlite', TRUE);
    }
    
    public function deleteMenu($menuID="") {
        $this->db2->where('id', $menuID);
        if($this->db2->delete('menu')){
            $data['status'] = 'true';            
        }else{
            $data['status'] = 'false';
        }
        return $data;
    }

    public function getMenusDashboards() {
        $this->db2->select('id,url,menuname');
        $this->db2->from('menu');
        $this->db2->where('company_id', $this->getCompanyId);
        $this->db2->where('user_id', $this->userId);
        $query = $this->db2->get();
        if($query->num_rows()>0){
            $data['status'] = 'true';
            $data['resultSet'] = $query->result();
        }else{
            $data['status'] = 'false';
        }
        return $data;
    }
    
    public function fetchMenusCount() {
        $this->db2->from('menu');
        $this->db2->where('company_id', $this->getCompanyId);
        $this->db2->where('user_id', $this->userId);
        $query = $this->db2->get();
        return $query->num_rows();
    }

    public function InsertMenus($url="",$menuName="") {
        $countMenu = $this->fetchMenusCount();
        if ($countMenu >= MENU_LIMIT) {
            return "EXCEED";
        } else {
            $this->db2->from('menu');
            $this->db2->where('company_id', $this->getCompanyId);
            $this->db2->where('user_id', $this->userId);
            $this->db2->where('url', $url);
            $query = $this->db2->get();
            if ($query->num_rows() == 0) {

                $postmenu = array(
                    'company_id' => $this->getCompanyId,
                    'user_id' => $this->userId,
                    'menuname' => $menuName,
                    'url' => $url
                );
                $this->db2->insert('menu', $postmenu);
                return 'INSERT';
            }
        }
    }

}
