<?php
session_start();
if(!isset($_SESSION['is_logged_in']) && empty($_SESSION['is_logged_in'])) {
	$_SESSION = array();
	session_destroy();
	header('Location: index.php');  
}
include_once('ajax_url.php');
date_default_timezone_set('Asia/Kolkata');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>MACD</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content=""
            name="description" />
        <meta content="" name="author:Siddhartha" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="./assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- END GLOBAL MANDATORY STYLES -->
       
        <link href="./assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
       
        <link href="./assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
		<link href="./assets/toster/build/toastr.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
		 <link href="./assets/datapicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    <!-- END HEAD -->
 <style> .required:before { content:"*"; color:red } 
 .custom_box .form-control {
height: 33px;
background: rgba(244, 244, 244, 0.6);
}

#loading-img {
    background: url(http://preloaders.net/preloaders/360/Velocity.gif) center center no-repeat;
    height: 100%;
    z-index: 20;
}

.overlay {
    background: #e9e9e9;
    display: none;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    opacity: 0.5;
}
 </style>
 </head>
    <body onload="getdata_onload()">
        <?php include("header.php"); ?>
		<div class="container pad0">
		<div class="overlay">
			<div id="loading-img"></div>
		</div>
			<div class="custom_box" id="replace_msg">
			<form method="post" action="" id="be_registration_form">
			<h4>Requestor Information: <span id="title"></span></h4>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label required">Requested By :</label>
						<div class="col-sm-8 padr0">
							<input type="text" class="form-control" data-validation="required" name="requested_by" value="<?php echo $_SESSION['name']; ?>"  readonly />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label required">Request Date :</label>
						<div class="col-sm-8 padr0">
							<input type="text" name="requested_date" class="form-control datepicker" id="datepicker" data-validation="required"
                    value="<?php echo date('m/d/Y H:i'); ?>"  />
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-6" style="display:none;">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label">Requested For :</label>
						<div class="col-sm-8 padr0">
							<input type="text" class="form-control" name="requested_for"  />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label required">Work Phone :</label>
						<div class="col-sm-8 padr0">
							<input type="text" name="work_phone"  class="form-control" data-validation="required" value="<?php echo $_SESSION['phone']; ?>" />
						</div>
					</div>
				</div>
				
					<div class="col-md-6">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label required">Request Type</label>
						<div class="col-sm-8 padr0">
							<select class="form-control" onchange="getInfo(this.value)" name="request_type" data-validation="required" data-validation-error-msg="Please select a request type">
								<option value="">Select Type</option>
								<option value="Move">Move</option>
								<option value="Add">Add</option>
								<option value="Change">Change</option>
								<option value="Delete">Disconnect / Delete</option>								
							</select>
						</div>
					</div>
				</div>
							
				
				
				
				
			</div>
			<div class="clearfix"></div>
			<div class="row">
			<div class="clearfix"></div>
			
				
				
				
				
				
			</div>
			
			<span class="unique_change"></span>
			
			
			<div class="col-md-12 pad0">
				<div class="form-group">
					<label for="firstname" class="col-sm-5 padlr0 control-label">Comments</label>
					<div class="col-sm-12 pad0">
						<textarea name="comments" cols="150" rows="5" style="width:100%;"></textarea>
					</div>
				</div>
			</div>
			
			<div class="col-md-12">
				<p>Fields with <span class="required"></span> are mandatory.</p>
			</div>
			
			<input type="hidden" id="client_id" name="client_id" value="<?php echo $_SESSION["client"]; ?>"/>
			<input type="hidden" id="key" name="key" value="<?php echo $_SESSION["client_uuid"]; ?>"/>
			<input type="hidden" id="contacts"  value="" />
			<input type="hidden" id="client_devices"  value="" />
			<input type="hidden" id="uid" name="uid" value="<?php echo $_SESSION["uid"]; ?>" />
			<input type="hidden" id="approver" name="approver" value="<?php echo $_SESSION["reporting_manager_id"]; ?>" />
			<input type="hidden" id="contract_id" name="contract_id" value="<?php echo $_SESSION["contract_id"]; ?>" />
			<?php $today_date = date('m/d/Y'); $plus_date = date('m/d/Y', strtotime($today_date . ' +5 day')); ?>
			<input type="hidden" id="plus_5_date"  value="<?php echo $plus_date;  ?>" />
			<div class="footer-bottom">
				<button type="submit" id="hide_button" class="btn btn-primary pull-right" style="margin-left:10px;">Submit</button> &nbsp; &nbsp; &nbsp;&nbsp;
				<button type="Reset" class="btn btn-primary pull-right">Reset</button>
				<div class="clearfix"></div>
			</div>
			</form>
		</div>
		
		</div>
       
 <?php include("footer.php"); ?>

