<div class="header_block">
			<div class="container pad0">
				<div class="row">
					<div class="col-md-6">
						<div><a href="inbox.php"><img src="./assets/layouts/layout/img/logo.png" class="logo" /></a></div>
					</div>
					<div class="col-md-6">
						<ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="./assets/layouts/layout/img/avatar.png">
                                    <span class="username username-hide-on-mobile"> <?php echo $_SESSION['name']; ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
					</div>
				</div>
			</div>
		</div>