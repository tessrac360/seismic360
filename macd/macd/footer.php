		 <div class="copyright"> 2017 © Seismic LLC </div>
	  <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
		<script src="./assets/global/plugins/respond.min.js"></script>
		<script src="./assets/global/plugins/excanvas.min.js"></script> 
		<script src="./assets/global/plugins/ie8.fix.min.js"></script> 
		<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="./assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
		
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="./assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="./assets/js/self.js" type="text/javascript"></script>
		<script src="./assets/toster/toastr.js"></script>
		<script src="./assets/validations/form-validator/jquery.form-validator.js"></script>
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/1.4.5/numeral.min.js"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
		
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="./assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <!-- END Dashboard Plugins -->
		
		
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="./assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
       
		

        
    </body>
	
	<script>
	  $(document).ready(function() {
		  
		  //Command: toastr["success"]("My name is Inigo Montoya. You killed my father. Prepare to die!")

			toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": true,
			  "progressBar": true,
			  "rtl": false,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": 300,
			  "hideDuration": 1000,
			  "timeOut": 3000,
			  "extendedTimeOut": 1000,
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
	  });
	</script>	
 <script src="./assets/datapicker/onejquery.datetimepicker.full.js"></script>


    <script>

        $('.datepicker').datetimepicker({
            format: 'Y-m-d H:i',
            mask: false,
            dayOfWeekStart: 1,
            lang: 'en',
            timepicker: true,
            showTimePicker: true,
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false,
            minDate: 0,
            step: 5
        });	
		
		
		function call_dateTime()
		{
			$('.datepicker').datetimepicker({
				format: 'Y-m-d H:i',
				mask: false,
				dayOfWeekStart: 1,
				lang: 'en',
				timepicker: true,
				showTimePicker: true,
				scrollMonth: false,
				scrollTime: false,
				scrollInput: false,
				minDate: 0,
				step: 5
			});	
		}
	</script>

</html>