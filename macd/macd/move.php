<?php
session_start();
if(!isset($_SESSION['is_logged_in']) && empty($_SESSION['is_logged_in'])) {
	$_SESSION = array();
	session_destroy();
	header('Location: index.php');  
}
if(isset($_GET['id']) && !empty($_GET['id']) && !is_numeric($_GET['id']))
{
	die("Invalid URL");
}
include_once('ajax_url.php');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>MACD</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="#1 selling multi-purpose bootstrap admin theme sold in themeforest marketplace packed with angularjs, material design, rtl support with over thausands of templates and ui elements and plugins to power any type of web applications including saas and admin dashboards. Preview page of Theme #1 for statistics, charts, recent events and reports"
            name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="./assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- END GLOBAL MANDATORY STYLES -->
       
        <link href="./assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
       
        <link href="./assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
		<link href="./assets/toster/build/toastr.css" rel="stylesheet" type="text/css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="./assets/js/move.js" type="text/javascript"></script> 		
        <style>
		.activity-table{max-height: 196px;min-height: 196px; overflow:auto;}
		</style>

 </head>
    <body onload="data_details_by_id(<?php echo $_GET['id'];?>)">
        <?php include("header.php"); ?>
		<div class="container pad0">
			<div class="custom_box" id="replace_msg">
			<form method="post" action="" id="be_registration_form">
			<h4>Requestor Information: <span id="title"></span></h4>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label">Requested By :</label>
						<div class="col-sm-8 padr0">
							<input type="text" class="form-control" data-validation="required" id ="requested_by" name="requested_by" readonly />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label">Request Date :</label>
						<div class="col-sm-8 padr0">
							<input type="text" name="requested_date" class="form-control" id="requested_date" data-validation="date"
                   data-validation-format="mm/dd/yyyy" readonly />
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-6" style="display:none;">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label">Requested For :</label>
						<div class="col-sm-8 padr0">
							<input type="text" class="form-control" name="requested_for" id="requested_for" readonly />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label">Work Phone :</label>
						<div class="col-sm-8 padr0">
							<input type="text" name="work_phone"  class="form-control" id="work_phone" data-validation="required" readonly />
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label">Request Type</label>
						<div class="col-sm-8 padr0">
							<input type="text" class="form-control" name="requested_for" id="request_type" readonly />
						</div>
					</div>
				</div>
				<div class="clearfix"></div>				
				
				
				
				
			</div>
			<div class="clearfix"></div>
			<div class="row">
			<div class="clearfix"></div>
							
				<div class="col-md-6">
					<div class="form-group">
						<label for="firstname" class="col-sm-4 padlr0 control-label">Is voice mail required</label>
						<div class="col-sm-8 padr0">
						<input type="text" class="form-control" name="voice_mail" id="voice_mail" readonly />
						</div>
					</div>
				</div>
				
				
				
			</div>
			
			<span class="unique_change"></span>
			<!--  <div class="col-md-12"> <div class=" sub-content"> </div> </div> -->
  
  <div class="clearfix"></div>
  <h4 class="mart20">In Details: </h4> 
  <div class="row">
   <!--Move Start -->
   <span id="move_class"> 
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">First Name : </label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="firstname_from" id="firstname_from" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Last Name :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="lastname_from" id="lastname_from" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label" style="letter-spacing:-0.6px;">Phone/Extension :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="phone" name="phone" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Move from Building/Floor/Post location:</label> 
     <div class="col-sm-12 padlr0"> 
	 <input type="text" class="form-control" id ="location_from" name="location_from" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly />      
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label" data-validation="required" data-validation-error-msg="Please select a Location">Move from cubicle:</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id="cubicle_from" name="cubicle_from" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Move to Building/Floor/Post location: </label> 
     <div class="col-sm-12 padlr0"> 
	  <input type="text" class="form-control" id ="location_to" name="location_to" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly />     
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Move to cubicle:</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="cubicle_to" name="cubicle_to" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Device Type :</label> 
     <div class="col-sm-12 padlr0">
		<input type="text" class="form-control" id ="device_type" name="device_type" data-validation="required" readonly />	 
       
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address (for IP Device Type) :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="mac_address" name="mac_address" data-validation="required" /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Dialing capabilities :</label> 
     <div class="col-sm-12 padlr0">
	<input type="text" class="form-control" id ="dialing_capabilities" name="dialing_capabilities" data-validation="required" readonly />      
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Required By Date :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="required_by" id="required_by" readonly  /> 
     </div>
    </div>
   </div>
   </span>
   <!--Move End -->


	<!--Add Start -->
	<span id="add_class"> 
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">First Name : </label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="firstname_from" id="firstname_from" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Last Name :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="lastname_from" id="lastname_from" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label" style="letter-spacing:-0.6px;">Phone/Extension :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="phone" name="phone" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   
   <div class="col-md-4 "> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Move from Building/Floor/Post location:</label> 
     <div class="col-sm-12 padlr0"> 
	 <input type="text" class="form-control" id ="location_from" name="location_from" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly />      
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label" data-validation="required" data-validation-error-msg="Please select a Location">Move from cubicle:</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id="cubicle_from" name="cubicle_from" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Device Type :</label> 
     <div class="col-sm-12 padlr0">
		<input type="text" class="form-control" id ="device_type" name="device_type" data-validation="required" readonly /></div>
    </div>
   </div>   
   
   <div class="clearfix"></div>  
   
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address (for IP Device Type) :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="mac_address" name="mac_address" data-validation="required" readonly disabled /> 
     </div>
    </div>
   </div>
   
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Dialing capabilities :</label> 
     <div class="col-sm-12 padlr0">
	<input type="text" class="form-control" id ="dialing_capabilities" name="dialing_capabilities" data-validation="required" readonly />      
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Required By Date :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="required_by" id="required_by" readonly  /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   </span>
	<!--Add End -->
   
	<!--Change START-->
	<span id="change_class"> 
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">First Name : </label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="firstname_from" id="firstname_from" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Last Name :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="lastname_from" id="lastname_from" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label" style="letter-spacing:-0.6px;">Phone/Extension :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="phone" name="phone" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   <div class="col-md-4 "> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Building/Floor/Post location:</label> 
     <div class="col-sm-12 padlr0"> 
	 <input type="text" class="form-control" id ="location_from" name="location_from" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly />      
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   <!--Name Change START-->
   <span id="name_change_id">
    <div class="col-md-4 "> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Change Request :</label> 
     <div class="col-sm-12 padlr0"> 
	 <input type="text" class="form-control" id = "change_request" name="change_request" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly />      
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label" data-validation="required" data-validation-error-msg="Please select a Location">First Name(To):</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id="first_name_to" name="first_name_to" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Last Name(To):</label> 
     <div class="col-sm-12 padlr0">
		<input type="text" class="form-control" id ="last_name_to" name="last_name_to" data-validation="required" readonly /></div>
    </div>
   </div>   
   
   <div class="clearfix"></div> 
   
   <div class="col-md-4 "> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Device Type(From) :</label> 
     <div class="col-sm-12 padlr0">
		<input type="text" class="form-control" id ="device_type" name="device_type" data-validation="required" readonly /></div>
    </div>
   </div>
   
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address (for IP Device Type) :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="mac_address" name="mac_address" data-validation="required" /> 
     </div>
    </div>
   </div>  
  
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Required By Date :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="required_by" id="required_by" readonly  /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   
	
   </span>
   <!--Name Change ends-->
   
   
    <!--Feature Change START-->
   <span id="feature_id">
    <div class="col-md-4 "> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Change Request :</label> 
     <div class="col-sm-12 padlr0"> 
	 <input type="text" class="form-control" id = "change_request" name="change_request" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly />      
     </div>
    </div>
   </div>
   
  <div class="col-md-12">
	  <div class="form-group">
		  <label for="comments" class="col-sm-4 padlr0 control-label">Which feature would you like to add or device type :</label> <div class="col-sm-12 padr0">
		  <textarea name="feature_like" id="feature_like" cols="120" rows="4" readonly></textarea>
		  </div>
	  </div>
  </div>
   
   <div class="clearfix"></div> 
   
   <div class="col-md-4 "> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Device Type(From) :</label> 
     <div class="col-sm-12 padlr0">
		<input type="text" class="form-control" id ="device_type" name="device_type" data-validation="required" readonly /></div>
    </div>
   </div>
   
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address (for IP Device Type) :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="mac_address" name="mac_address" data-validation="required" /> 
     </div>
    </div>
   </div>  
  
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Required By Date :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="required_by" id="required_by" readonly  /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   </span>
   <!--Feature Change ends-->
   
   
    <!--Device Change START-->
   <span id="device_change_id">
    <div class="col-md-4 "> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Change Request :</label> 
     <div class="col-sm-12 padlr0"> 
	 <input type="text" class="form-control" id = "change_request" name="change_request" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly />      
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Device Type(From) :</label> 
     <div class="col-sm-12 padlr0">
		<input type="text" class="form-control" id ="device_type" name="device_type" data-validation="required" readonly /></div>
    </div>
   </div>
   
    <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Device Type(To) :</label> 
     <div class="col-sm-12 padlr0">
		<input type="text" class="form-control" id ="device_type_to" name="device_type_to" data-validation="required" readonly /></div>
    </div>
   </div>
      
   
   <div class="clearfix"></div> 
   
  
   
   
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address From(for IP Device Type) :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="mac_address" name="mac_address" data-validation="required" readonly /> 
     </div>
    </div>
   </div>

	<div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address To(for IP Device Type) :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="mac_address_to" name="mac_address_to" data-validation="required" readonly /> 
     </div>
    </div>
   </div>   
  
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Required By Date :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="required_by" id="required_by" readonly  /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>	
   </span>
   <!--Device Change ends-->
   
   
   
     <!--Dailing Capabilites START-->
   <span id="dailing_id">
    <div class="col-md-4 "> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Change Request :</label> 
     <div class="col-sm-12 padlr0"> 
	 <input type="text" class="form-control" id = "change_request" name="change_request" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly />      
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Dialing capabilities(From) :</label> 
     <div class="col-sm-12 padlr0">
	<input type="text" class="form-control" id ="dialing_capabilities" name="dialing_capabilities" data-validation="required" readonly />      
     </div>
    </div>
   </div>
   
    <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Dialing capabilities(To) :</label> 
     <div class="col-sm-12 padlr0">
	<input type="text" class="form-control" id ="dialing_capabilities_to" name="dialing_capabilities_to" data-validation="required" readonly />      
     </div>
    </div>
   </div>
      
   
   <div class="clearfix"></div> 
   
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Device Type(From) :</label> 
     <div class="col-sm-12 padlr0">
		<input type="text" class="form-control" id ="device_type" name="device_type" data-validation="required" readonly /></div>
    </div>
   </div>
  
   
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address From(for IP Device Type) :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="mac_address" name="mac_address" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Required By Date :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="required_by" id="required_by" readonly  /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>	
   </span>
   <!--Dailing Capabilites ends-->
   
   
   
   
   
   
   </span>
	<!--Change END-->
	
	<!--DELETED START-->
	<span id="delete_class"> 
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">First Name : </label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="firstname_from" id="firstname_from" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Last Name :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="lastname_from" id="lastname_from" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label" style="letter-spacing:-0.6px;">Phone/Extension :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="phone" name="phone" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Move to Building/Floor/Post location: </label> 
     <div class="col-sm-12 padlr0"> 
	  <input type="text" class="form-control" id ="location_to" name="location_to" data-validation="number" data-validation-error-msg="Please enter a valid number" readonly />     
     </div>
    </div>
   </div>
   
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Move to cubicle:</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="cubicle_to" name="cubicle_to" data-validation="required" readonly /> 
     </div>
    </div>
   </div>
   
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Device Type :</label> 
     <div class="col-sm-12 padlr0">
		<input type="text" class="form-control" id ="device_type" name="device_type" data-validation="required" readonly /></div>
    </div>
   </div>   
   
   <div class="clearfix"></div>  
   
   <div class="col-md-4"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address (for IP Device Type) :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" id ="mac_address" name="mac_address" data-validation="required" /> 
     </div>
    </div>
   </div>
   
      
   <div class="col-md-4 padl0"> 
    <div class="form-group"> 
     <label for="firstname" class="col-sm-12 padlr0 control-label">Required By Date :</label> 
     <div class="col-sm-12 padlr0"> 
      <input type="text" class="form-control" name="required_by" id="required_by" readonly  /> 
     </div>
    </div>
   </div>
   <div class="clearfix"></div>
   </span>
	<!--DELETED END-->   
   
   
   
   <div class="clearfix"></div> <br/>
   <div class="col-md-12 pad10">
		<div class="form-group">
			<label for="firstname">Comments: </label>
			 <span id = "comments"></span>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	
	<div class="inbox-header " style="position:relative;display:none;">		<!--Time been hold-->
		<?php if($_SESSION["reporting_manager_id"] == 0){ ?>
		<div class="col-md-4 pad10 show_if_not_approve">
			<div class="form-group">
				<label for="firstname" class="col-sm-5 padlr0 control-label">Notes</label>
				<div class="col-sm-12 pad0">
					<textarea name="notes" cols="150" id ="notes" rows="10" style="width:100%;"></textarea>
				</div>
			</div>
		</div>
		<input type="hidden" id="reporting_manager_id" value="<?php echo $_SESSION["uid"]; ?>">
		<?php } ?>
		
		<div class="col-md-8">
			<div class="form-group">
				<label for="firstname" class="col-sm-5 padlr0 control-label">MACD Activity</label>
				<div class="col-sm-12 pad0 activity-table">
					<table class="table table-striped table-bordered table table-hover" id="tblGrid">   
						<thead style="background:#17c4bb;">
							<tr>
								<th style="color:#fff;">Activity No</th>
								<th style="color:#fff;">Activity Type</th>
								<th style="color:#fff;">Start Time</th>
								<th style="color:#fff;">End Time</th>
							</tr>
						</thead>
						<tbody class="bold-text">
																		
							<tr>
								<td colspan="4"> No Record Found </td>
							</tr> 
							
																	  
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
	
	<?php if($_SESSION["is_approval"] == "Y"){ ?>
	<div class="col-md-12 show_if_not_approve">
		<div class="btn-group input-actions pull-right">
			<a class="btn btn-sm blue btn-outline sbold" onclick="updated_check(2);" href="javascript:" data-toggle="dropdown" style="margin-right: 3px;"> Reject </a>
			<a class="btn btn-sm blue btn-outline sbold" onclick="approve_check(1);" data-toggle="dropdown" style="margin-right: 3px;"> Approve </a>
			<a class="btn btn-sm blue btn-outline sbold" onclick="updated_check(3);" href="#" data-toggle="dropdown"> Cancel </a>
		</div>
	</div>
	<?php } ?>
	
  </div>
			
			
			
			
			
			<input type="hidden" id="macd" name="macd" value="<?php echo $_GET["id"]; ?>"/>
			<input type="hidden" id="uid" name="uid" value="<?php echo $_SESSION["uid"]; ?>" />
			
			
			</form>
		</div>
		
		</div>
		<script src="./assets/toster/toastr.js"></script>
		<script>
	  $(document).ready(function() {
		  
		  //Command: toastr["success"]("My name is Inigo Montoya. You killed my father. Prepare to die!")

			toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": true,
			  "progressBar": true,
			  "rtl": false,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": 300,
			  "hideDuration": 1000,
			  "timeOut": 3000,
			  "extendedTimeOut": 1000,
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
	  });
	  
	$(document).ready(function(){
		 if( sessionStorage.getItem("status") == "Approved") {
			toastr.success("Approved Successfully");
		}else if( sessionStorage.getItem("status") == "Rejected") {
			toastr.success("Rejected Successfully");
		}else if( sessionStorage.getItem("status") == "Cancelled") {
			toastr.success("Cancelled Successfully");
		}
		sessionStorage.removeItem("status");
		sessionStorage.clear();
	});
	  
	  
	</script>	
		