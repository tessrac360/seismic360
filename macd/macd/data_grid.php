<?php
session_start();
if(!isset($_SESSION['is_logged_in']) && empty($_SESSION['is_logged_in'])) {
	$_SESSION = array();
	session_destroy();
	header('Location: index.php');  
}
include_once('ajax_url.php');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Form</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="#1 selling multi-purpose bootstrap admin theme sold in themeforest marketplace packed with angularjs, material design, rtl support with over thausands of templates and ui elements and plugins to power any type of web applications including saas and admin dashboards. Preview page of Theme #1 for statistics, charts, recent events and reports"
            name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="./assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
		
		<!-- BEGIN PAGE LEVEL PLUGINS -->
     
        <!-- END PAGE LEVEL PLUGINS -->
		
		<link href="./assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="./assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="./assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
		<link href="./assets/toster/build/toastr.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="./assets/ajax/css/jquery.dataTables.css">
        <!-- END THEME LAYOUT STYLES -->
		 <style>
            .dataTables_scrollHeadInner{padding-left: 0 !important;}
            table thead tr th, table tbody tr td{white-space: nowrap;}
				
.disabled span{background:#e4e4e4 !important;}
.dataTables_wrapper .dataTables_paginate .paginate_button{padding: 0.3em 0.5em !important;}
        </style>
	</head>
    <body onload="getMacdData_onload(<?php echo $_SESSION["uid"]; ?>)">
	
		 <?php include("header.php"); ?>
        
		<div class="container pad0 mart15">
			<div class="inbox">
				<div class="row">
					<div class="col-md-2 padro">
						<div class="inbox-sidebar">
							<a href="home.php" data-title="Compose" class="btn red compose-btn btn-block">
								<i class="fa fa-edit"></i> New MACD Request </a>
							<ul class="inbox-nav">
								<li>
									<a href="inbox.php" data-type="inbox" data-title="Inbox"> All
										<span class="badge badge-success" id="all">0</span>
									</a>
								</li>
								<li <?php if($_GET['sts'] == 0){?> class="active" <?php } ?>>
									<a href="javascript:data_grid('<?php echo $_SESSION["uid"]; ?>',0,<?php echo $_SESSION["reporting_manager_id"]; ?>);" data-type="important" data-title="Inbox"> Pending 
										<span class="badge badge-success" id="pending">0</span>
									</a>
								</li>
								<li <?php if($_GET['sts'] == 1){?> class="active" <?php } ?>>
									<a href="javascript:data_grid('<?php echo $_SESSION["uid"]; ?>',1,<?php echo $_SESSION["reporting_manager_id"]; ?>);" data-type="sent" data-title="Sent"> Approved 
										<span class="badge badge-danger" id="approved">0</span>
									</a>
								</li>
								<li <?php if($_GET['sts'] == 2){?> class="active" <?php } ?>>
									<a href="javascript:data_grid('<?php echo $_SESSION["uid"]; ?>',2,<?php echo $_SESSION["reporting_manager_id"]; ?>);" data-type="draft" data-title="Draft"> Rejected
										<span class="badge badge-danger" id="rejected">0</span>
									</a>
								</li>
								
								<li <?php if($_GET['sts'] == 3){?> class="active" <?php } ?>>
									<a href="javascript:data_grid('<?php echo $_SESSION["uid"]; ?>',3,<?php echo $_SESSION["reporting_manager_id"]; ?>);" data-title="Trash"> Cancelled
										<span class="badge badge-info" id="trash">0</span>
									</a>
								</li>
							</ul>
							<ul class="inbox-contacts">
								<li class="divider margin-bottom-30"></li>
								<li>
									<a href="javascript:;">
										<img class="contact-pic" src="./assets/layouts/layout/img/avatar.png">
										<span class="contact-name"> <?php echo $_SESSION['name']; ?> </span>
										<span class="contact-status bg-green"></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-10 padl5">
						<div class="inbox-body">
							<div class="inbox-header">
								<h1 class="pull-left">MACD Inbox</h1>
								<?php if($_SESSION["is_approval"] == "Y"){ ?>
								<div class="btn-group input-actions">
									<a class="btn btn-sm blue btn-outline sbold" onclick="updated_check(2);" href="javascript:" data-toggle="dropdown"> Reject </a>
									<a class="btn btn-sm blue btn-outline sbold" onclick="approve_check(1);" href="javascript:approve_check(1);" data-toggle="dropdown"> Approve </a>
									<a class="btn btn-sm blue btn-outline sbold" onclick="updated_check(3);" href="javascript:approve_check(3);" data-toggle="dropdown"> Cancel </a>
								</div>
								<?php } ?>
							</div>
							<div class="inbox-content" style="">
								<table id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table table-hover dataTable no-footer" width="100%">
									<thead>
										<tr width="20">
										<?php if($_SESSION["reporting_manager_id"] == 0){ ?>
											<th class="nosort"> <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
													<input type="checkbox" class="mail-checkbox md-check1">
													<span></span>
												</label>
											</th>
											<?php } ?>
											<th> Request ID </th>
											<th> Subject </th>
											<th> Request Status </th>
											<th> Submitted On </th>
											<th> Requested By </th>
											<th> Task ID </th>
											<th> Task Status </th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="uid" name="uid" value="<?php echo json_encode($_SESSION["uid"]); ?>" />
       <?php //include("footer.php"); ?>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
		<script src="./assets/global/plugins/respond.min.js"></script>
		<script src="./assets/global/plugins/excanvas.min.js"></script> 
		<script src="./assets/global/plugins/ie8.fix.min.js"></script> 
		<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
		<script src="./assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="./assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->

		<!-- BEGIN PAGE LEVEL PLUGINS -->
		
		<!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="./assets/global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="./assets/pages/scripts/table-datatables-fixedheader.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="./assets/layouts/layout/scripts/layout.js" type="text/javascript"></script>
		<script src="./assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
		
        <script src="./assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="./assets/toster/toastr.js"></script>
		

         <!-- END Dashboard Plugins -->
  

        <script>


            $(function ()
            {
                $('.inbox-body, .inbox-sidebar').css({'height': (($(window).height()) - 126) + 'px'});
                $(window).bind('resize', function () {
                    $('.inbox-body, .inbox-sidebar').css({'height': (($(window).height()) - 126) + 'px'});
                    //alert('resized');
                });

              $('.dataTables_scrollBody').css({'height': (($(window).height()) - 260) + 'px'});
                $(window).bind('resize', function () {
                    $('.dataTables_scrollBody').css({'height': (($(window).height()) - 260) + 'px'});
                    //alert('resized');
                });
            });
        </script>
		
		
		<script>
	  $(document).ready(function() {
		  
		  //Command: toastr["success"]("My name is Inigo Montoya. You killed my father. Prepare to die!")

			toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": true,
			  "progressBar": true,
			  "rtl": false,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": 300,
			  "hideDuration": 1000,
			  "timeOut": 3000,
			  "extendedTimeOut": 1000,
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
	  });
	  
	  var get = "<?php echo (isset($_GET['id']))?$_GET['id']:''; ?>";
	  
	  if(get == "post")
	  {
		if (window.performance) {
		  console.info("window.performance work's fine on this browser");
		}
		  if (performance.navigation.type == 1) {
			console.info( "This page is reloaded" );
		  } else {
			console.info( "This page is not reloaded");
			toastr.success("Successfully created Request");
			//toastr.success("Welcome "+"<?php echo $_SESSION["name"]; ?>");
		  } 
	  }
	  
	</script>	
		
	<!-- ajax start -->
	
	<script type="text/javascript" language="javascript" src="./assets/ajax/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" >
			$(document).ready(function() {
				$.fn.dataTableExt.sErrMode = 'throw';
				 var col = 6;
				<?php if($_SESSION["reporting_manager_id"] == 0){ ?>
				 var col = 7;
				<?php } ?>
				var uid = <?php echo $_SESSION["uid"]; ?>;
				var sts = <?php echo $_GET["sts"]; ?>;
				var mng = <?php echo $_GET["mng"]; ?>;
				var dataTable = $('#employee-grid').DataTable( {
					"processing": true,
					"serverSide": true,
					"ajax":{
						url : ajax_url+"event/macd/getMacdDetailsOfUser?id="+uid+"&sts="+sts+"&mng="+mng, // json datasource
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".employee-grid-error").html("");
							$("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="'+col+'">No data found in the server</th></tr></tbody>');
							$("#employee-grid_processing").css("display","none");
							
						}
					},
					"order": [[ 1, "desc" ]],
					'aoColumnDefs': [{
					   'bSortable': false,
					   'aTargets': 'nosort'
					}]
				} );
			} );
		</script>
	<!-- ajax end --> 
    		<script src="./assets/js/inbox.js" type="text/javascript"></script>  
			
    </body>

</html>