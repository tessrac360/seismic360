var Dashboard = function() {

    return {
        initMorisCharts: function() {
            new Morris.Line({
			// ID of the element in which to draw the chart.
			element: 'morris_chart_1',
			// Chart data records -- each entry in this array corresponds to a point on
			// the chart.
			data: [
				{ y: '2006', a: 100, b: 90, c: 95 },
				{ y: '2007', a: 75,  b: 65, c: 75 },
				{ y: '2008', a: 50,  b: 40, c: 55 },
				{ y: '2009', a: 75,  b: 65, c: 55 },
				{ y: '2010', a: 50,  b: 40, c: 45 },
				{ y: '2011', a: 75,  b: 65, c: 85 },
				{ y: '2012', a: 100, b: 90, c: 95 },
				{ y: '2013', a: 100, b: 90, c: 95 },
				{ y: '2014', a: 100, b: 90, c: 95 },
				{ y: '2015', a: 100, b: 90, c: 95 }
			],
			// The name of the data record attribute that contains x-values.
			xkey: 'y',
			// A list of names of data record attributes that contain y-values.
			ykeys: ['a', 'b' , 'c'],
			// Labels for the ykeys -- will be displayed when you hover over the
			// chart.
			labels: ['Agent', 'Team', 'Department']
		  });
			
			new Morris.Bar({
				element: 'morris_chart_3',
				data: [
				  { y: 'QA', a: 100, b: 90,c:95 },
				  { y: 'Compliance', a: 75,  b: 65,c:80 }
				],
				xkey: 'y',
				ykeys: ['a','b','c'],
				labels: ['Agent', 'Team','Department']
			  });
			
        },

        init: function() {
            this.initMorisCharts();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {
        Dashboard.init(); // init metronic core componets
    });
}