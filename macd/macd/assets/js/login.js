function login_call()
{
	var user = $("#user").val();
	var pwd = $("#password").val();
	var key = $("#key").val();
	var Url = ajax_url+"event/macd/user_login"; // if codeigniter base_url+controller+method
	var id = $('#id').val();
	var val = $('#val').val();
	var form_data = {
				user: user,
				pwd: pwd,
				key: key,
			 };
			 
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//console.log(msg);
					var parsedData = JSON.parse(msg);
					if(parsedData['status'] == true){
						console.log(parsedData['data']);
						var response_data = {
							client:parsedData['data']['client'],
							client_uuid:parsedData['data']['client_uuid'],
							is_approval:parsedData['data']['is_approval'],
							name:parsedData['data']['name'],
							reporting_manager_id:parsedData['data']['reporting_manager_id'],
							uid:parsedData['data']['uid'],
							phone:parsedData['data']['phone'],
							contract_id:parsedData['data']['contract_id'],
						};
						// start
						$.ajax({
							url: 'test.php',
							type: 'POST',
							data: response_data,
							success: function(msg) {
								if(msg == 1){
									window.location = "inbox.php";	
								}else{
									 toastr.error("Invalid login details or User doesn't Have Access.");
								}							  						 
							},				
							error: function (textStatus, errorThrown) {
								console.log(textStatus);
								console.log(errorThrown);
							}
						});
						// end
					}else
					{
						toastr.error(parsedData['error']);
					}				
				},				
				error: function (textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
	 return false;	
}




