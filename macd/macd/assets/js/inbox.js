		
function getMacdData_onload(val)
{
	
	var Url = ajax_url+"event/macd/bringCounts";
	var form_data = {
				id: val,				
			 };		 
			 
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//console.log(msg);
					var parsedData = JSON.parse(msg);
					if(parsedData['status'] == true){
						//console.log(parsedData['all']);						
						$('#all').text(parsedData['all']);
						$('#pending').text(parsedData['pending']);
						$('#approved').text(parsedData['approved']);
						$('#rejected').text(parsedData['rejected']);
						$('#trash').text(parsedData['trash']);	
											
					 } 				
				},				
				error: function (textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				}
			});	
}



function data_grid(uid,val,mng)
{
	window.location = "data_grid.php?id="+uid+"&sts="+val+"&mng="+mng;
	
}

function data_details_by_id(macd)
{
	window.location = "move.php?id="+macd;
	
}

					  
				
function approve_check(val)
{
	var matches = [];
	$(".checked:checked").each(function() {
		matches.push(this.value);
	});
	if(matches == "")
	{
		alert("Please Select Atleast one MACD");
		return false;
	}
	var Url = ajax_url+"event/macd/updateIndts";
	var form_data = {
				id: val,
				macd_id: matches,
			 };		 
			 
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				async:true, 
				success: function(msg) {
					console.log(msg);
					if (typeof(Storage) !== "undefined") {
						sessionStorage.setItem("status","Approved");    
					} 					
					location.reload(); 			 				
				},				
				error: function (textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				}
			});	
}


function updated_check(val)
{
	
	var matches = [];
	$(".checked:checked").each(function() {
		matches.push(this.value);
	});
    if(matches == "")
	{
		alert("Please Select Atleast one MACD");
		return false;
	}
	if(val == 2)
	{
		var info = "Reject";
	}else
	{
		var info = "Cancelled";
	}
	if(confirm("Do you want "+info+" this MACD?"))
	{
			var Url = ajax_url+"event/macd/updateMacd";
			var form_data = {
						id: val,
						macd_id: matches,
					 };		 
					 
					$.ajax({
						url: Url,
						type: 'POST',
						data: form_data,
						success: function(msg) {
							if (typeof(Storage) !== "undefined") {
								if(val == 2)
								{
									sessionStorage.setItem("status","Rejected");
								}else
								{
									sessionStorage.setItem("status","Cancelled");
								}
									
							} 	
							
						 location.reload();
											
						},				
						error: function (textStatus, errorThrown) {
							console.log(textStatus);
							console.log(errorThrown);
						}
					});	 
	}else
	{
		return false;
	}
	
}








