 $(function() {
    // setup validate
    $.validate({
    modules: 'security',
    form: '#be_registration_form',
    onSuccess: function() {
        // Do ajax stuff here
	$(':input[type="submit"]').prop('disabled', true);
	 $(".overlay").show();
	 var updateUrl = ajax_url+"event/macd/client_data";
	  var str = $("form").serializeArray();
	  	$.ajax({  
			type: "POST",  
			url: updateUrl,  
			data: str,  
			success: function(value) {
				var parsedData = JSON.parse(value);
				if(parsedData['status'] == true){
					if (typeof(Storage) !== "undefined") {
						sessionStorage.setItem("status","MACD_CREATED");    
					} 					
					window.location = "inbox.php";					
				}else
				{
					alert("Something went wrong Please try again!");
				}				
			}
		}); 
        return false; // preventing the form submit
    }
});
	/*  $( "#datepicker" ).datepicker({ minDate: 0});
	 $( "#datepicker1" ).datepicker({ minDate: 0});
	 $( ".datepicker2" ).datepicker({ minDate: 0}); */
  });


			
function getdata_onload()
{
	
	var updateUrl = ajax_url+"event/macd/client";
	var updateUrl2 = ajax_url+"event/macd/client_title";
	var updateUrl3 = ajax_url+"event/macd/client_devices";
	var form_data = {
				id: $('#key').val(),
			 };
			 
			 
			$.ajax({
				url: updateUrl,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					var parsedData = JSON.parse(msg);
					if(parsedData['status'] == true){
						//console.log(parsedData['data']);
						$('#contacts').val(msg);
						/* var toAppend = '';
						$.each(parsedData['data'],function(i,o){
						   console.log(i,o);
						   console.log(i,o.location_name);
						  toAppend += '<option value='+o.location_uuid+'>'+o.location_name+'</option>';
						}); 
						$('#location_from').append(toAppend);	
						$('#location_to').append(toAppend);	 */

						
					}				
				},				
				error: function (textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
			
			
			$.ajax({
				url: updateUrl2,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					var parsedData = JSON.parse(msg);
					if(parsedData['status'] == true){
						//console.log(parsedData['data']);
						$('#title').text(parsedData['data']);										
					}				
				},				
				error: function (textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
			
			
			$.ajax({
				url: updateUrl3,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					var parsedData = JSON.parse(msg);
					if(parsedData['status'] == true){
						$('#client_devices').val(msg);									
					}				
				},				
				error: function (textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
	
}

					  
				




function getInfo(val){
	
	var plus_5_date = $("#plus_5_date").val();
	if(val == 'Move'){ // move
		 $(".unique_change").replaceWith(' <span class="unique_change">   <div class="col-md-12"> <div class=" sub-content"> Moves might not require new hardware, however if you require a new device, you must choose \'Add\' at \'Request type\', then choose your device type.  </div></div><div class="clearfix"></div><h4 class="mart20">Please fill the following fields for \'Move\' </h4> <div class="row"> <div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">First Name : </label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="firstname_from" data-validation="required"/> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Last Name :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="lastname_from" data-validation="required"/> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required" style="letter-spacing:-0.6px;">Phone/Extension :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="phone" data-validation="number" data-validation-error-msg="Please enter a valid number"/> </div></div></div><div class="clearfix"></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Move from Building/Floor/Post location:</label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a Location" name="location_from" id="location_from"> <option value="">Select Location</option> </select> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required" data-validation="required" data-validation-error-msg="Please select a Location">Move from cubicle:</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="cubicle_from" data-validation="required"/> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Move to Building/Floor/Post location: </label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a Location" name="location_to" id="location_to"> <option value="">Select Location</option> </select> </div></div></div><div class="clearfix"></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Move to cubicle:</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="cubicle_to" data-validation="required"/> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Device Type :</label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a device type" name="device_type" id="device_type"> <option value="">Select Type</option> </select> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">MAC Address (for IP Device Type) :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="mac_address" data-validation="required"/> </div></div></div><div class="clearfix"></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Dialing capabilities :</label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a dialing capability" name="dialing_capabilities"> <option value="">Select Type</option> <option value="1">Internal999 only</option> <option value="2">Option 1 plus LocalToll Free</option> <option value="3">Option 2 plus LocalToll</option> <option value="4">Option 3 plus Long Distance</option> <option value="5">Option 4 plus International</option> </select> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Required By Date :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control datepicker" name="required_by" id="datepicker2" placeholder="" data-validation="required"  value=""/> </div></div></div><div class="col-md-4 padl0"><div class="form-group"><label for="firstname" class="col-sm-12 padlr0 control-label required">Is voice mail required</label><div class="col-sm-12 padlr0"><select class="form-control" name="voice_mail" data-validation="required" data-validation-error-msg="Please select voice mail required or not"><option value="">Select Type</option><option value="yes">Yes</option><option value="no">No</option></select></div></div></div><div class="clearfix"></div></div></span>');
		 
		 call_dateTime();
	
		
		
		$contacts = $('#contacts').val();
		var parsedData = JSON.parse($contacts);
		var toAppend = '';
			$.each(parsedData['data'],function(i,o){
				//console.log(i,o);
				//console.log(i,o.location_name);
				toAppend += '<option value='+o.location_uuid+'>'+o.contact_name+ '(' +o.location_name+ ')</option>';
			});	
		$('#location_from').append(toAppend);
		$('#location_to').append(toAppend);
		
		
		$client_devices = $('#client_devices').val();
		var parsedData = JSON.parse($client_devices);
		var toAppend = '';
			$.each(parsedData['data'],function(i,o){
				//console.log(i,o);
				//console.log(i,o.location_name);
				toAppend += '<option value='+o.device_id+'>' +o.device_name+ '</option>';
			});	
		$('#device_type').append(toAppend);
				
		 
		
	}else if(val == 'Add') 
	{ //add
		 $(".unique_change").replaceWith('<span class="unique_change"><div class="col-md-12"> <div class=" sub-content"> Add request involves associating an existing phone or configuring a new phone. </div></div><div class="clearfix"></div><h4 class="mart20">Please fill the following fields for \'Add\'</h4> <div class="row"> <div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">First Name : </label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="firstname_from" data-validation="required"/> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Last Name :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="lastname_from" data-validation="required"/> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label" style="letter-spacing:-0.6px;">Phone/Extension :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="phone"/> </div></div></div><div class="clearfix"></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Building/Floor/Post location: </label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a Location" name="location_from" id="location_from"> <option value="">Select Location</option> </select> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Cubicle/office identifier :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="cubicle_from" data-validation="required"/> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Device Type :</label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a device type" name="device_type" id="device_type"> <option value="">Select Type</option> </select> </div></div></div><div class="clearfix"></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address (for IP Device Type) :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="mac_address"/> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label">Dialing capabilities :</label> <div class="col-sm-12 padlr0"> <select class="form-control" name="dialing_capabilities"><option value="">Select Type</option><option value="1">Internal999 only</option><option value="2">Option 1 plus LocalToll Free</option><option value="3">Option 2 plus LocalToll</option><option value="4">Option 3 plus Long Distance</option><option value="5">Option 4 plus International</option> </select> </div></div></div><div class="col-md-4 padl0"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Required By Date:</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control datepicker" name="required_by" placeholder="" id="datepicker2" data-validation="required"  value=""/> </div></div></div><div class="col-md-4"><div class="form-group"><label for="firstname" class="col-sm-12 padlr0 control-label required">Is voice mail required</label><div class="col-sm-12 padlr0"><select class="form-control" name="voice_mail" data-validation="required" data-validation-error-msg="Please select voice mail required or not"><option value="">Select Type</option><option value="yes">Yes</option><option value="no">No</option></select></div></div></div><div class="clearfix"></div></div> </span>');
		call_dateTime();
		$contacts = $('#contacts').val();
		var parsedData = JSON.parse($contacts);
		var toAppend = '';
			$.each(parsedData['data'],function(i,o){
				//console.log(i,o);
				//console.log(i,o.location_name);
				toAppend += '<option value='+o.location_uuid+'>'+o.contact_name+ '(' +o.location_name+ ')</option>';
			});	
		$('#location_from').append(toAppend);
		
		$client_devices = $('#client_devices').val();
		var parsedData = JSON.parse($client_devices);
		var toAppend = '';
			$.each(parsedData['data'],function(i,o){
				//console.log(i,o);
				//console.log(i,o.location_name);
				toAppend += '<option value='+o.device_id+'>' +o.device_name+ '</option>';
			});	
		$('#device_type').append(toAppend);
		
		
	}
	else if(val == 'Change')
	{ // change
		 $(".unique_change").replaceWith('<span class="unique_change"> <div class="col-md-12"> <div class=" sub-content"> First choose one of the \'Change Request\' type and proceed further </div></div><div class="clearfix"> </div><h4 class="mart20">Please fill the following fields for \'Change\' </h4> <div class="row"> <div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">First Name : </label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="firstname_from" data-validation="required"/> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Last Name : </label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="lastname_from" data-validation="required"/> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required" style="letter-spacing:-0.6px;">Phone/Extension : </label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="phone" data-validation="number" data-validation-error-msg="Please enter a valid number"/> </div></div></div><div class="clearfix"> </div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Building/Floor/Post location: </label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a Location" name="location_from" id="location_from"> <option value="">Select Location </option> </select> </div></div></div>  <div class="col-md-4 "> <div class="form-group"> <label for="firstname" class="col-sm-12 control-label required" data-validation="required" data-validation-error-msg="Please select a Location"> Cubicle/office Identifier:</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="cubicle_from" data-validation="required"/> </div></div></div> <div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Change Request : </label> <div class="col-sm-12 padlr0"> <select class="form-control" name="change_request" onchange="bringRelateFileds(this.value)" data-validation="required"> <option value="">Select Type </option> <option value="name_change">Name Change </option> <option value="feature_change">Feature Change </option> <option value="device_change">Device Change </option> <option value="dialing_capabilities_change">Dialing capabilities change </option> </select> </div></div></div><span id="unique_change_request"> </span> <div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Required by Date: </label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control datepicker" name="required_by" id="datepicker2" placeholder="" data-validation="required" value=""/> </div></div></div><div class="col-md-4"><div class="form-group"><label for="firstname" class="col-sm-12 padlr0 control-label required">Is voice mail required</label><div class="col-sm-12 padlr0"><select class="form-control" name="voice_mail" data-validation="required" data-validation-error-msg="Please select voice mail required or not"><option value="">Select Type</option><option value="yes">Yes</option><option value="no">No</option></select></div></div></div><div class="clearfix"> </div></div></span>');
		call_dateTime();
		 $contacts = $('#contacts').val();
		var parsedData = JSON.parse($contacts);
		var toAppend = '';
			$.each(parsedData['data'],function(i,o){
				//console.log(i,o);
				//console.log(i,o.location_name);
				toAppend += '<option value='+o.location_uuid+'>'+o.contact_name+ '(' +o.location_name+ ')</option>';
			});	
		$('#location_from').append(toAppend);
	}
	else if(val == 'Delete') 
	{ //Disconnect
		 $(".unique_change").replaceWith('<span class="unique_change"> <div class="col-md-12"> <div class=" sub-content"> Disconnect/Delete request will involve removing the phone and line. If you want to remove the name association from the phone , specify clearly in the comments.</div></div><div class="clearfix"></div><h4 class="mart20">Please fill the following changes for \'Delete\'</h4> <div class="row"> <div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">First Name : </label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="firstname_from" data-validation="required"/> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Last Name :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="lastname_from" data-validation="required"/> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required" style="letter-spacing:-0.6px;">Phone/Extension :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="phone" data-validation="number"/> </div></div></div><div class="clearfix"></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Building/Floor/Post location : </label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a Location" name="location_to" id="location_to"> <option value="">Select Location</option> </select> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Cubicle/office identifier :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="cubicle_to" data-validation="required"/> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Device Type :</label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a device type" name="device_type" id="device_type"> <option value="">Select Type</option> </select> </div></div></div><div class="clearfix"></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address (for IP Device Type) :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="mac_address"/> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required required">Required By Date:</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control datepicker" name="required_by" id="datepicker2" placeholder="" data-validation="required"  value=""/> </div></div></div><div class="clearfix"></div></div> </span>');
		
		 call_dateTime();
		 $contacts = $('#contacts').val();
		var parsedData = JSON.parse($contacts);
		var toAppend = '';
			$.each(parsedData['data'],function(i,o){
				//console.log(i,o);
				//console.log(i,o.location_name);
				toAppend += '<option value='+o.location_uuid+'>'+o.contact_name+ '(' +o.location_name+ ')</option>';
			});	
		$('#location_to').append(toAppend); 
		$client_devices = $('#client_devices').val();
		var parsedData = JSON.parse($client_devices);
		var toAppend = '';
			$.each(parsedData['data'],function(i,o){
				//console.log(i,o);
				//console.log(i,o.location_name);
				toAppend += '<option value='+o.device_id+'>' +o.device_name+ '</option>';
			});	
		$('#device_type').append(toAppend);
		
		
	}else
	{
		 $(".unique_change").replaceWith('<span class="unique_change"></span>');
	}
   
	/* var x = document.getElementById("plus_5_date").value;
	document.getElementById("datepicker2").innerHTML = x;  */
   
}



function details_submit()
{
	/*  var updateUrl = "http://10.10.32.52/eventedge2/event/macd/client_data";
	  var str = $("form").serializeArray();
	  console.log(str);
		$.ajax({  
			type: "POST",  
			url: updateUrl,  
			data: str,  
			success: function(value) {
				alert(value);				
			}
		});  */
}


function bringRelateFileds(val)
{
	if(val == 'name_change')
	{
		$("#unique_change_request").replaceWith('<span id="unique_change_request">   <div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">First Name(To) : </label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="firstname_to" data-validation="required"/> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Last Name(To) :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="lastname_to" data-validation="required"/> </div></div></div><div class="clearfix"></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Device Type(From) :</label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a device type" name="device_type" id="device_type"> <option value="">Select Type</option> </select> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address (for IP Device Type) :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="mac_address"/> </div></div></div> </span>');	
		
	}else if(val == 'feature_change'){
		$("#unique_change_request").replaceWith('<span id="unique_change_request">  <div class="col-md-12"><div class="form-group"> <label for="comments" class="col-sm-4 padlr0 control-label">Which feature would you like to add or device type :</label> <div class="col-sm-12 padr0"><textarea name="feature_like" cols="120" rows="4"></textarea></div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Device Type(From) :</label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a device type" name="device_type" id="device_type"> <option value="">Select Type</option> </select> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address (for IP Device Type) :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="mac_address"/> </div></div></div> </span>');	
		
	}else if(val == 'device_change'){
		$("#unique_change_request").replaceWith('<span id="unique_change_request">  <div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Device Type(From) :</label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a device type" name="device_type" id="device_type"> <option value="">Select Type</option> </select> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Device Type(To) :</label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a device type" name="device_type_to" id="device_type_to"> <option value="">Select Type</option> </select> </div></div></div><div class="clearfix"></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address From (for IP Device Type) :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="mac_address"/> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address To (for IP Device Type) :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="mac_address_to"/> </div></div></div> </span>');	
		
	}else if(val == 'dialing_capabilities_change'){
		$("#unique_change_request").replaceWith('<span id="unique_change_request">  <div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label">Dialing capabilities(From) :</label> <div class="col-sm-12 padlr0"> <select class="form-control" name="dialing_capabilities"> <option value="">Select Type</option> <option value="1">Internal999 only</option> <option value="2">Option 1 plus LocalToll Free</option> <option value="3">Option 2 plus LocalToll</option> <option value="4">Option 3 plus Long Distance</option> <option value="5">Option 4 plus International</option> </select> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label">Dialing capabilities(To) :</label> <div class="col-sm-12 padlr0"> <select class="form-control" name="dialing_capabilities_to"> <option value="">Select Type</option> <option value="1">Internal999 only</option> <option value="2">Option 1 plus LocalToll Free</option> <option value="3">Option 2 plus LocalToll</option> <option value="4">Option 3 plus Long Distance</option> <option value="5">Option 4 plus International</option> </select> </div></div></div><div class="clearfix"></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label required">Device Type(From) :</label> <div class="col-sm-12 padlr0"> <select class="form-control" data-validation="required" data-validation-error-msg="Please select a device type" name="device_type" id="device_type"> <option value="">Select Type</option> </select> </div></div></div><div class="col-md-4"> <div class="form-group"> <label for="firstname" class="col-sm-12 padlr0 control-label">MAC Address (for IP Device Type) :</label> <div class="col-sm-12 padlr0"> <input type="text" class="form-control" name="mac_address"/> </div></div></div> </span>');		
		
	}else
	{
		$("#unique_change_request").replaceWith('<span id="unique_change_request">  </span>');
	}
	
		$client_devices = $('#client_devices').val();
		var parsedData = JSON.parse($client_devices);
		var toAppend = '';
			$.each(parsedData['data'],function(i,o){
				//console.log(i,o);
				//console.log(i,o.location_name);
				toAppend += '<option value='+o.device_id+'>' +o.device_name+ '</option>';
			});	
		$('#device_type').append(toAppend);
		$('#device_type_to').append(toAppend);
}



function login_call()
{
	var user = $("#user").val();
	alert(user);
	return false;
}


