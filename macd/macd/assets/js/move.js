function data_details_by_id(macd)
{
	var Url = ajax_url+"event/macd/getMacdDetailsById";
	var form_data = {
				id: macd,				
			 };		 
			 
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					
					var parsedData = JSON.parse(msg);
					if(parsedData['status'] == true){
						console.log(parsedData['data']);
						if(parsedData['data']['request_type'] == "Add")
						{
							$("#move_class").empty();
							$("#delete_class").empty();
							$("#change_class").empty();
						}else if(parsedData['data']['request_type'] == "Move")
						{
							$("#add_class").empty();
							$("#delete_class").empty();
							$("#change_class").empty();
						}
						else if(parsedData['data']['request_type'] == "Delete")
						{
							$("#add_class").empty();
							$("#move_class").empty();
							$("#change_class").empty();
						}else if(parsedData['data']['request_type'] == "Change")
						{
							$("#add_class").empty();
							$("#move_class").empty();
							$("#delete_class").empty();
							
							if(parsedData['data']['change_request'] == 'name_change')
							{
								$("#feature_id").empty();
								$("#device_change_id").empty();
								$("#dailing_id").empty();
							}
							if(parsedData['data']['change_request'] == 'feature_change')
							{
								$("#name_change_id").empty();
								$("#device_change_id").empty();
								$("#dailing_id").empty();
							}
							if(parsedData['data']['change_request'] == 'device_change')
							{
								$("#name_change_id").empty();
								$("#feature_id").empty();
								$("#dailing_id").empty();
							}
							if(parsedData['data']['change_request'] == 'dialing_capabilities_change')
							{
								$("#name_change_id").empty();
								$("#feature_id").empty();
								$("#device_change_id").empty();
							}
						}
						
						if(parsedData['data']['macd_status'] != 0)
						{
							$(".show_if_not_approve").remove();
						}
						
						
						$('#title').text(parsedData['data']['client']);
						$('#requested_by').val(parsedData['data']['requested_by']);
						$('#requested_date').val(parsedData['data']['requested_date_name']);
						$('#requested_for').val(parsedData['data']['requested_for']);
						$('#work_phone').val(parsedData['data']['work_phone']);
						$('#request_type').val(parsedData['data']['request_type']);
						$('#voice_mail').val(parsedData['data']['voice_mail']);
						$('#comments').text(parsedData['data']['comments']);
						$('#firstname_from').val(parsedData['data']['firstname_from']);
						$('#lastname_from').val(parsedData['data']['lastname_from']);
						$('#phone').val(parsedData['data']['phone']);						
						$('#location_from').val(parsedData['data']['location_from']);
						$('#cubicle_from').val(parsedData['data']['cubicle_from']);						
						$('#location_to').val(parsedData['data']['location_to']);
						$('#cubicle_to').val(parsedData['data']['cubicle_to']);							
						$('#device_type').val(parsedData['data']['device']);						
						$('#mac_address').val(parsedData['data']['mac_address']);
						$('#dialing_capabilities').val(parsedData['data']['dialing_capabilities_name']);
						$('#dialing_capabilities_to').val(parsedData['data']['dialing_capabilities_name_to']);
						$('#required_by').val(parsedData['data']['required_by_date_formet']);
						$('#cubicle_to').text(parsedData['data']['cubicle_to']);
						$('#first_name_to').val(parsedData['data']['firstname_to']);
						$('#last_name_to').val(parsedData['data']['lastname_to']);
						$('#change_request').val(parsedData['data']['change_request']);
						$('#feature_like').val(parsedData['data']['feature_like']);
						$('#device_type_to').val(parsedData['data']['device_to']);
						$('#mac_address_to').val(parsedData['data']['mac_address_to']);
 	
											
					 } 				
				},				
				error: function (textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
			
			
	var macd_url = ajax_url+"event/macd/macdActivity";
	var form_data = {
				id: macd,				
			 };		 
			 
			$.ajax({
				url: macd_url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					//alert(msg);
					/* var parsedData = JSON.parse(msg);
					if(parsedData['status'] == true){
						
					} 	 */			
				},				
				error: function (textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
			
	
}



	




function approve_check(val)
{	
	var macd = $("#macd").val();	
	var notes = $("#notes").val();
	var reporting_manager_id = $("#reporting_manager_id").val();

	
	var Url = ajax_url+"event/macd/updateIndtsSingle";
	var form_data = {
				id: val,
				macd_id: macd,
				notes: notes,
				reporting_manager_id: reporting_manager_id,
			 };		 
			 
			$.ajax({
				url: Url,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					if (typeof(Storage) !== "undefined") {
						sessionStorage.setItem("status","Approved");    
					} 					
					window.location.replace("inbox.php");	 				
				},				
				error: function (textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				}
			});	
}


function updated_check(val)
{
	
	var macd = $("#macd").val();
	var notes = $("#notes").val();
	var reporting_manager_id = $("#reporting_manager_id").val();
	if(val == 2)
	{
		var info = "Reject";
	}else
	{
		var info = "Cancel";
	}
	if(confirm("Do you want "+info+" this MACD?"))
	{
		var Url = ajax_url+"event/macd/updateMacdSingle";
		var form_data = {
					id: val,
					macd_id: macd,
					notes: notes,
					reporting_manager_id: reporting_manager_id,
				 };		 
				 
				$.ajax({
					url: Url,
					type: 'POST',
					data: form_data,
					success: function(msg) {
						if (typeof(Storage) !== "undefined") {
							if(val == 2)
							{
								sessionStorage.setItem("status","Rejected");
							}else
							{
								sessionStorage.setItem("status","Cancelled");
							}
								
						} 					
						window.location.replace("inbox.php");		 				
					},				
						error: function (textStatus, errorThrown) {
						console.log(textStatus);
						console.log(errorThrown);
					}
				});	 
	}else
	{
		return false;
	}
	
}




				
			