<?php
//include_once("Db.class.php");
/**
 *  SLA - A simple sl,mtrs and mttr return class 
 * @author		B V V S Prasad		
 * @version      0.1
 *
 */

class Sla
{  
    /**
     *   Default Constructor 
     *
     *	1. Instantiate Log class.
     *	2. Connect to database.
     *	3. Creates the parameter array.
     */
    public function __construct()
    {
		$this->con = new Db();
    }
    
    public function getSLAtime_get($clientId = NULL,$priority_id = NULL ,$date = NULL) {
        //$clientId = $this->get('client_id');
        //$priority_id = $this->get('priority_id');
        $timestamp = date('Y-m-d H:i', $date);
        if ($clientId != "" && $priority_id != "" && $timestamp != "") {
            //$this->db->select('shift_start_time,shift_end_time,incident_generated_sla,mtrs_sla,mttr_sla,working_day,hours,days');
            //$this->db->from('events_sla');
            //$this->db->where('client_id', $clientId);
            //$this->db->where('priority_id', $priority_id);           
            //$query = $this->db->get();
			
			$row   =  $this->con->query("SELECT shift_start_time,shift_end_time,incident_generated_sla,mtrs_sla,mttr_sla,working_day,hours,days FROM eventedge_events_sla WHERE client_id = :client_id AND priority_id = :priority_id  ",
			array(
			"client_id"=>$clientId,
			"priority_id"=>$priority_id,			
			)			
			);					
			$rowcount_val = count($row);			
            //echo $this->db->last_query();
            if ($rowcount_val > 0) {
                $newArray = array();
                $eventDate = strtotime(date('Y-m-d', strtotime($timestamp)));
                $eventTime = $this->HRToSec(date('H:i', strtotime($timestamp)));
                $shift_start_time = $this->HRToSec(date('H:i', strtotime($row[0]['shift_start_time'])));
                $shift_end_time = $this->HRToSec(date('H:i', strtotime($row[0]['shift_end_time'])));
                $weekdays = explode(",", $row[0]['working_day']);
                $holidays = array();
                $sla_time = $row[0]['incident_generated_sla'];
                $mtrs_time = $row[0]['mtrs_sla'];
                $mttr_sla = $row[0]['mttr_sla'];
                $sla = $this->calculateSLADueDate($eventDate, $eventTime, $weekdays, $shift_start_time, $shift_end_time, $sla_time, $holidays);
                $datetime['slaDueTime'] = date('Y-m-d H:i', $sla);
                $mtrs = $this->calculateSLADueDate($eventDate, $eventTime, $weekdays, $shift_start_time, $shift_end_time, $mtrs_time, $holidays);
                $datetime['mtrsDueTime'] = date('Y-m-d H:i', $mtrs);
                $mttr = $this->calculateSLADueDate($eventDate, $eventTime, $weekdays, $shift_start_time, $shift_end_time, $mttr_sla, $holidays);
                $datetime['mttrDueTime'] = date('Y-m-d H:i', $mttr);
                $return['status'] = 'true';
                $return['resultSet'] = $datetime;                
            } else {
                $return['status'] = 'false';
                $return['message'] = "Something went wrong";
            }
        } else {
            $return['status'] = 'false';
            $return['message'] = 'please enter valid Input';
        } 
		return $return;
		
        //$this->response($return, 200);
    }

    public function calculateSLADueDate($eventStartDay, $eventStartTime, $weekdays = array(), $slaStartTime, $slaEndTime, $slaDuration, $holidays = array()) {

        //echo $eventStartDay;exit;
        /*
          The function calculates a due date for an event or ticket by considering business hours
          and excluding non working days and holidays
         */

        $DueDate = $eventStartDay; // Default SLA Due date
        $SLAperiod = $slaDuration;

        // echo "<br/> SLA for the day '" . date('Y-m-d', $DueDate) ."'\t Day of the week : ". date('N', $DueDate); // for debug

        /* checks if an event day is in holiday list or not */
        $isHoliday = false;
        $hDayCount = sizeof($holidays);
        if ($hDayCount > 0) {

            $isHoliday = $this->isHoliday($holidays, $DueDate);
        }

        /* check if an event day falls within working days and not a holiday. */

        if ((in_array(date('N', $DueDate), $weekdays)) && (!($isHoliday))) {


            if (($eventStartTime >= $slaStartTime) && ($eventStartTime <= $slaEndTime)) {

                /* if event time falls with in business hours */
                $duration = $slaEndTime - $eventStartTime;
                if ($SLAperiod <= $duration) {

                    $DueDate += $eventStartTime + $SLAperiod;
                    $SLAperiod = 0;
                } else {

                    $DueDate += 86400 + $slaStartTime;
                    $SLAperiod -= $duration;
                }

                // echo "<br/> SLA1 : " . ($slaDuration - $SLAperiod) ; // for debug
            } else if ($eventStartTime < $slaStartTime) {

                /* if event time is before start of business hours */

                $DueDate += $slaStartTime;
                $duration = $slaEndTime - $slaStartTime;
                if ($SLAperiod <= $duration) {

                    $DueDate += $SLAperiod;
                    $SLAperiod = 0;
                } else {

                    $DueDate += 86400;
                    $SLAperiod -= $duration;
                }

                //echo "<br/> SLA2 :" . ($slaDuration - $SLAperiod ) ; // for debug
            } else if ($eventStartTime > $slaEndTime) {

                /* if event time is after end of business hours */
                $DueDate += 86400 + $slaStartTime;

                //echo "<br/> SLA3 :" . ($slaDuration - $SLAperiod)  ; // for debug
            }
        } else { /* runs if event time not within business hours or it's a holiday */

            $DueDate += 86400 + $slaStartTime;
            //echo "<br/> SLA4 :" . ($slaDuration - $SLAperiod ) ; // for debug
        }


        while ($SLAperiod > 0) {

            /* checks if an due date day is in holiday list or not */
            $isHoliday = false;
            if ($hDayCount > 0) {

                $isHoliday = $this->isHoliday($holidays, $DueDate);
            }

            /*  check if an Due date falls within working days and not a holiday. */
            if ((in_array(date('N', $DueDate), $weekdays)) && (!($isHoliday))) {

                $duration = $slaEndTime - $slaStartTime;
                if ($SLAperiod <= $duration) {

                    $DueDate += $SLAperiod;
                    $SLAperiod = 0;
                } else {

                    $DueDate += 86400;
                    $SLAperiod -= $duration;
                }
                //echo "<br/> SLA6 :" . ($slaDuration - $SLAperiod)  ; // for debug
            } else { /* if Due date not within business days or it's a holiday */

                $DueDate += 86400;
                //echo "<br/> SLA7 :" . ($slaDuration - $SLAperiod)  ; // for debug
            }
        }

        return $DueDate;
    }

    public function isHoliday($holidays, $DueDate) {

        /* Checks if a given date is holiday list or not */

        $flag_Holiday = false;
        $tempDay = strtotime(date('Y-m-d', $DueDate));

        if (in_array($tempDay, $holidays)) {

            //echo "<br/> Excluding holiday :" . date('Y-m-d H:i', $tempDay) ; // for debug
            $flag_Holiday = true;
        }
        return $flag_Holiday;
    }
	public function HRToSec($hours_time){
	sscanf($hours_time, "%d:%d", $hours, $minutes);
	return  $hours * 3600 + $minutes * 60 ;
	}
}
?>
