<?php

/* * *******************************************
 * #### Imap_parser.php v0.0.1 ####
 * IMAP mailbox parser using PHP.
 * Return Array or JSON.
 * Coded by @bachors 2016.
 * https://github.com/bachors/Imap_parser.php
 * Updates will be posted to this site.

  - data:
  - email:
  - hostname
  - username
  - password
  - pagination:
  - sort
  - limit
  - offset

  - result:
  - status
  - email
  - count
  - inbox:
  - id
  - subject
  - from
  - email
  - date
  - message
  - image
  - pagination
  - sort
  - limit
  - offset
  - back
  - next

 * ******************************************* */

class Imap_parser {

    function inbox($data) {
		

        $result = array();

        $imap = imap_open($data['email']['hostname'], $data['email']['username'], $data['email']['password']) or die('Cannot connect to yourdomain.com: ' . imap_last_error());

        if ($imap) {

            $result['status'] = 'success';
            $result['email'] = $data['email']['username'];

            $read = imap_search($imap, 'ALL');


            if ($data['pagination']['sort'] == 'DESC') {
                rsort($read);
            }

            $num = count($read);

            $result['count'] = $num;

            $stop = $data['pagination']['limit'] + $data['pagination']['offset'];

            if ($stop > $num) {
                $stop = $num;
            }

            for ($i = $data['pagination']['offset']; $i < $stop; $i++) {
                $message ="";
                //print_r($read[$i]);
                $overview = imap_fetch_overview($imap, $read[$i], 0);
                $message = imap_body($imap, $read[$i], 0);
				
                $header = imap_headerinfo($imap, $read[$i], 0);

                /* get mesage body */
                $message = imap_qprint(imap_body($imap, $read[$i], 2));
                //$message = imap_body($imap, $read[$i],2);
                $mail = $header->from[0]->mailbox . '@' . $header->from[0]->host;
                
                $aDataTableDetailHTML = array();
                $aDataTableHeaderHTML = array();
                $htmlContent = preg_replace('/td/', 'th', $message, 16);
                $DOM = new DOMDocument();
                libxml_use_internal_errors(true);
                $DOM->loadHTML($htmlContent);
                $Header = $DOM->getElementsByTagName('th');
                $Detail = $DOM->getElementsByTagName('td');
                //#Get header name of the table
                foreach ($Header as $NodeHeader) {
                    $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
                }

                #Get row data/detail table without header name as key
                $k = 0;
                $j = 0;
                foreach ($Detail as $sNodeDetail) {
                    $aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
                    $k = $k + 1;
                    $j = $k % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
                }
//
//
//                echo '<pre>';
//                print_r($aDataTableDetailHTML);
//                echo '</pre>';
				if (strpos($overview[0]->subject, '(') !== false) {
					$subj1 = strstr($overview[0]->subject, '(', true);
				}
				else
				{
					$subj1 = $overview[0]->subject;
				}
				$subj1 = strstr($overview[0]->subject, '(', true);
				$subj2 = str_replace("Server","",$subj1);
				$subjectfinal=str_replace("Condition:","",$subj2);
				

                $result['inbox'][] = array(
                    'id' => $read[$i],
                    'subject' => trim($subjectfinal),
                    'from' => $overview[0]->from,
                    'email' => $mail,
                    'date' => $overview[0]->date,
                    'message'=>$aDataTableDetailHTML,
                );
				
				
				
                $result['pagination'] = array(
                    'sort' => $data['pagination']['sort'],
                    'limit' => $data['pagination']['limit'],
                    'offset' => array(
                        'back' => ($data['pagination']['offset'] == 0 ? null : $data['pagination']['offset'] - $data['pagination']['limit']),
                        'next' => ($data['pagination']['offset'] < $num ? $data['pagination']['offset'] + $data['pagination']['limit'] : null)
                    )
                );
            }

            imap_close($imap);
        } else {
            $result['status'] = 'error';
        }

        return $result;
    }
    function inbox_vistara($data) {

        $result = array();

        $imap = imap_open($data['email']['hostname'], $data['email']['username'], $data['email']['password']) or die('Cannot connect to yourdomain.com: ' . imap_last_error());

        if ($imap) {

            $result['status'] = 'success';
            $result['email'] = $data['email']['username'];

            $read = imap_search($imap, 'ALL');


            if ($data['pagination']['sort'] == 'DESC') {
                rsort($read);
            }

            $num = count($read);

            $result['count'] = $num;

            $stop = $data['pagination']['limit'] + $data['pagination']['offset'];

            if ($stop > $num) {
                $stop = $num;
            }

            for ($i = $data['pagination']['offset']; $i < $stop; $i++) {
                $message = "";
                //print_r($read[$i]);
                $overview = imap_fetch_overview($imap, $read[$i], 0);
                $message = imap_body($imap, $read[$i], 0);
                $header = imap_headerinfo($imap, $read[$i], 0);

                /* get mesage body */
                 $message = imap_qprint(imap_body($imap, $read[$i], 2));
                    //$output2 = decode_qprint($2s2d);           
              // $message = imap_body($imap, $read[$i], 2);
               //$message = imap_fetchbody($imap, $read[$i], 1.2);

                //$message = imap_body($imap, $read[$i],2);
                $mail = $header->from[0]->mailbox . '@' . $header->from[0]->host;

                $aDataTableHeaderHTML=array();
                $pos = strpos($message, '</table>');
                $outputcontent = substr($message, $pos);

                $htmlContent = preg_replace('/td/', 'th', $outputcontent);              
                $DOM = new DOMDocument();
                libxml_use_internal_errors(true);
                $DOM->loadHTML($htmlContent);

                $Header = $DOM->getElementsByTagName('th');               
                
               // $tr = $DOM->getElementsByTagName('tr');
                //$aDataTableHeaderHTML = '';
                //#Get header name of the table
                foreach ($Header as $NodeHeader) {
                    $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
                }
                

                $final = array();
                $j = 0;
                for ($j = 0; $j < sizeof($aDataTableHeaderHTML); $j += 2) {
                    $final[$aDataTableHeaderHTML[$j]] = $aDataTableHeaderHTML[$j + 1];
                }

                $result['inbox'][] = array(
                    'id' => $read[$i],
                    'subject' => strip_tags($overview[0]->subject),
                    'from' => $overview[0]->from,
                    'email' => $mail,
                    'date' => $overview[0]->date,
                    'message'=>$final,
                );

                $result['pagination'] = array(
                    'sort' => $data['pagination']['sort'],
                    'limit' => $data['pagination']['limit'],
                    'offset' => array(
                        'back' => ($data['pagination']['offset'] == 0 ? null : $data['pagination']['offset'] - $data['pagination']['limit']),
                        'next' => ($data['pagination']['offset'] < $num ? $data['pagination']['offset'] + $data['pagination']['limit'] : null)
                    )
                );
            }

            imap_close($imap);
        } else {
            $result['status'] = 'error';
        }

        return $result;
    }	

}

?>
