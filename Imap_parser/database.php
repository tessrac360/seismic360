<?php

// This class uses PDO extension for PHP
// code in this class is compatible with PHP 5.1 and above


class db {
		
	
	private $oConn = null;
	
	
	function __construct(){
       
		require_once('DB_config.php');
		if ($this->is_db_driver($db_type)){
		
			return  $this->connect($db_hostname, $db_username, $db_password, $db_database, $db_port, $db_type);
			
		} else {
			
			//throw new Exception('Unknown database "'.$db_type.'". Drivers for this database type may be not installed.');
		}
		
	}
	
	// Checks whether a specified database driver plug in/ library installed or not
	
	private function is_db_driver($db_type){
		
		foreach(PDO::getAvailableDrivers() as $driver){
			
			if ($driver == $db_type){
				
				return true;
			} 
		}
		return false;
	}
	
 
  
	// Open a new connection to the database
	
	private function connect($hostname, $username, $password, $database, $port, $dbtype)
	{
		if ($port != '')
		{
			$hostname .= ':'.$port;
		}
		
		$this->oConn = new PDO("$dbtype:host=$hostname;dbname=$database", $username, $password);
		$this->oConn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return true;
	}
	
	
	// Closes the connection to database
	
	private function close(){
		
		$this->oConn = null;
		
	}

	
	
		
	// executes insert, update, delete and other unprepared statement
	public function execute( $query,$type){
		//echo $query . "<br />";
		
		 $this->oConn->exec($query);
		 if($type == 'I'){
			return $this->oConn->lastInsertId();
		 }
		 return false;
	}
		
		
	
	// returns the data from a database. unprepared statement
	public function select( $query){
		
		
		return $this->oConn->query($query,PDO::FETCH_ASSOC);
	}
	
	
	
	// prepare a SQL statement for execution
	
	public function prepare( $query){
		
		
		return $this->oConn->prepare($query);
	}

	
	
	// begins a transaction
	
	public function beginTransaction(){
		
		
		return $this->oConn->beginTransaction();
	}
	
	// commits a transaction
	
	public function commit(){
		
		
		return $this->oConn->commit();
	}
	
	
	// rolls back a transaction
	
	public function rollback(){
		
		
		return $this->oConn->rollBack();
	}
	
	// this function is called before object using this class is detroyed.
	function __destruct() {
       
	   $this->close();
	}
	
}
?>
